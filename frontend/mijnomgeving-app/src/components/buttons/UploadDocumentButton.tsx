import { FC, useState } from 'react'
import { useNotify, useTranslate } from 'react-admin'
import { Button, Tooltip } from '@mui/material'
import UploadFileIcon from '@mui/icons-material/UploadFile'
import { CaseRecord } from '../../pages/cases/Cases.show'
import { memoryToken } from '../../providers/dataProvider'
import { getCasesUrl } from '../../AppUrls'

const validMediaTypes = [
  'image/bmp',
  'image/jpeg',
  'image/x-png',
  'image/png',
  'image/gif',
  'image/svg+xml',
  'image/tiff',
  'application/msword',
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  'application/vnd.ms-excel',
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  'application/pdf',
  'application/json',
  'application/xml',
  'application/vnd.ms-powerpoint',
  'application/zip',
  'text/csv',
  'text/plain',
  'audio/mpeg',
  'audio/wav',
  'audio/webm',
  'video/mp4',
  'video/mpeg'
]
const maxSizeInBytes = 50000000 // 50mb

interface Props {
  record: CaseRecord | undefined
}

export const UploadDocumentButton: FC<Props> = ({ record }) => {
  const translate = useTranslate()
  const notify = useNotify()
  const [isUploading, setIsUploading] = useState(false)

  const checkIfFileIsValid = (file: File): { valid: boolean, error?: string } => {
    if (!validMediaTypes.includes(file.type))
      return { valid: false, error: 'warnings.unsupportedMimeType' }
    if (maxSizeInBytes && file.size > maxSizeInBytes)
      return { valid: false, error: 'warnings.fileSizeToBig' }
    return { valid: true }
  }

  const handleFileUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (!event.target.files || !event.target.files[0]) return

    const file = event.target.files[0]
    const { valid, error } = checkIfFileIsValid(file)
    if (!valid && error) {
      notify(translate(error), { type: 'warning' })
      return
    }
    setIsUploading(true)

    const headers = new Headers()
    headers.append('organization-url', window.origin)
    if (memoryToken) headers.append('Authorization', `Bearer ${memoryToken}`)

    const data = new FormData()
    data.append('file', file)
    data.append('case_uuid', `${record?.id || ''}`)

    fetch(`${getCasesUrl()}/documents`, { method: 'POST', body: data, headers })
      .then(response => {
        const ok = response.status === 204
        notify(ok ? 'cases.fileUploaded' : 'warnings.uploadFailed', { type: ok ? 'info' : 'warning' })
      })
      .catch((e: Error) => notify(e.message, { type: 'warning' }))
      .finally(() => setIsUploading(false))
  }

  return (
    <Tooltip title={translate(isUploading ? 'cases.isUploading' : 'cases.uploadInstructions')} >
      <span>
        <Button
          component="label"
          variant="outlined"
          startIcon={<UploadFileIcon />}
          disabled={isUploading}
        >
          {translate('cases.uploadButton')}
          <input type="file" hidden onChange={handleFileUpload} />
        </Button>
      </span>
    </Tooltip>
  )
}