import { FC, useState } from 'react'
import { useNotify, useTranslate, Identifier } from 'react-admin'
import { Button } from '@mui/material'
import { memoryToken } from '../../providers/dataProvider'
import { getCasesUrl } from '../../AppUrls'

interface Props {
  id: Identifier
  name: string
}

export const DownloadFileButton: FC<Props> = ({ id, name }) => {
  const translate = useTranslate()
  const notify = useNotify()
  const [isDownloading, setIsDownloading] = useState(false)

  const onClickHandler = () => {
    const headers = new Headers()
    headers.append('organization-url', window.origin)
    if (memoryToken) headers.append('Authorization', `Bearer ${memoryToken}`)

    setIsDownloading(true)
    fetch(`${getCasesUrl()}/documents/download/${id}`, { method: 'GET', headers })
      .then(response => {
        setIsDownloading(false)
        if (response.status !== 200) return notify(translate('warnings.downloadFailed'), { type: 'warning' })
        return response.blob().then(blob => {
          const downloadUrl = window.URL.createObjectURL(blob)
          const element = document.createElement('a')
          element.href = downloadUrl
          element.download = name
          element.click()
        })
      })
      .catch((error: Error) => notify(error.message, { type: 'warning' }))
  }

  return (
    <Button disabled={isDownloading} onClick={onClickHandler} >
      { translate('cases.downloadButton') }
    </Button>
  )
}