import { ArrowBack } from '@mui/icons-material'
import { useEffect, useState } from 'react'
import { Button } from 'react-admin'
import { Link } from 'react-router-dom'

interface BackButtonProps {
  label?: string,
  to?: string,
}

export const BackButton = (props: BackButtonProps): JSX.Element => {
  const { label, to } = props
  const [mountHistoryLength, setMountHistoryLength] = useState<number>(0)

  useEffect(() => setMountHistoryLength(window.history.length), [])
  const onClick = () => !to && window.history.go(mountHistoryLength - window.history.length - 1)

  return (
    <Button
      variant="outlined"
      onClick={onClick}
      label={label}
      component={ to ? Link : undefined }
      to={ to ? { pathname: to } : undefined }
      sx={theme => ({ marginRight: theme.spacing(1) })}
    >
      <ArrowBack />
    </Button>
  )
}
