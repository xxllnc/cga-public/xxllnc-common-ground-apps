import { FC } from 'react'
import { List, Datagrid, TextField } from 'react-admin'
import { Link } from 'react-router-dom'
import './PersonalInquiriesList.css'

export const PersonalInquiriesList: FC = () => (
  <>
    <List exporter={false}>
      <Datagrid bulkActionButtons={false}>
        <TextField label="Organisatie" source="displayName"></TextField>
        <TextField label="Resultaat" source="summary"></TextField>
        <TextField label="Datum" source="date"></TextField>
      </Datagrid>
    </List>
    <Link className="linkToLogin" to="/login">
      Terug
    </Link>
  </>
)
