import { FC } from 'react'
import { useGetIdentity, SimpleForm, TextInput } from 'react-admin'
import { Navigate } from 'react-router'
import { Link } from 'react-router-dom'
import { IngeschrevenPersoonHal } from '../../types/generatedPersonsTypes'
import Typography from '@mui/material/Typography'
import './EditPersonalDetails.css'

const createUserIdentityRecord = (identity: IngeschrevenPersoonHal) =>
  identity
    ? {
      voornamen: identity.naam?.voornamen,
      geslachtsnaam: identity.naam?.geslachtsnaam,
      geslachtsaanduiding: identity.geslachtsaanduiding,
      geboortedatum: identity.geboorte?.datum?.datum,
      geboorteplaats: identity.geboorte?.plaats?.omschrijving,
      woonplaats: identity.verblijfplaats?.woonplaats,
      straatnaam: identity.verblijfplaats?.straat,
      nummeraanduiding:
        identity.verblijfplaats?.nummeraanduidingIdentificatie,
      postcode: identity.verblijfplaats?.postcode,
    }
    : {}

export const EditPersonalDetails: FC = () => {
  const { identity, isLoading } = useGetIdentity()
  const record = createUserIdentityRecord(identity as IngeschrevenPersoonHal)

  if (!window.location.pathname.includes('identity')) {
    return <Navigate to="/identity"></Navigate>
  } else if (isLoading) {
    return null
  } else {
    return (
      <>
        <SimpleForm
          className="personalDetails"
          toolbar={false}
          defaultValues={record}
        >
          <Typography variant="h3" id="personalDetailsTitle">
            Identiteit
          </Typography>
          <hr className="line" />
          {Object.keys(record).map((key) => (
            <span className="personalDetailsFieldContainer" key={key}>
              <label className="personalDetailsLabel" htmlFor={key}>
                {key}
              </label>
              <TextInput label={false} disabled source={key}></TextInput>
            </span>
          ))}
          <hr className="line" />
          <Link className="linkToList" to="/auditlog">
            Welke aangesloten organisaties maken gebruik van mijn gegevens?
          </Link>
        </SimpleForm>
      </>
    )
  }
}
