import * as React from 'react'
import { Card, CardContent, Typography } from '@mui/material'
import { Title } from 'react-admin'

export default () => (
    <Card>
        <Title title="xxllnx mijn omgevingen" />
        <CardContent>
          <Typography variant="h6">Mijn omgevingen</Typography>
          <Typography variant="body2">
            Welkom in mijn omgevingen
          </Typography>
        </CardContent>
    </Card>
)