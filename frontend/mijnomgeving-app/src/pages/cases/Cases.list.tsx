import { FC } from 'react'
import {
  useLocale,
  Datagrid,
  DateField,
  ListProps,
  TextField,
  SimpleList,
  useTranslate,
  RaRecord,
  ListBase,
  Pagination
} from 'react-admin'
import { useAuthorized } from 'xxllnc-react-components'
import { Theme, useMediaQuery, Card, Typography, Divider } from '@mui/material'

const CaseListWithCustomTitle: FC<ListProps> = ({ children, ...props }) => {
  const translate = useTranslate()

  return (
    <ListBase {...props}>
      <Typography variant="h5" sx={{ margin: '20px 10px 10px 10px' }}>{ translate('cases.openCases') }</Typography>
      <Divider sx={{ marginBottom: '20px' }} />
      <Card>
          {children}
      </Card>
      <Pagination />
    </ListBase>
  )
}

const CasesList: FC<ListProps> = (props) => {
  useAuthorized()

  const translate = useTranslate()
  const isXSmall = useMediaQuery<Theme>(theme => theme.breakpoints.down('md'))
  const locale = useLocale()

  return (
    <CaseListWithCustomTitle {...props}
      title="cases.title"
      exporter={false}
      empty={false}
    >
      {isXSmall ? (
        <SimpleList
          linkType="show"
          hasBulkActions={false}
          primaryText={(record: RaRecord) => `${record.id ?? ''} `}
          secondaryText={(record: RaRecord) => `${record.omschrijving}`}
          tertiaryText={(record: RaRecord) =>
            `${translate('cases.registratiedatum')}: ${record.registratiedatum ?
              new Date((record as unknown as { registratiedatum: Date }).registratiedatum).toLocaleDateString() : '-'}`
          }
        />
      ) : (
        <Datagrid bulkActionButtons={false} rowClick="show">
          <TextField label={'cases.id'} source="identificatie" sortable={false} />
          <TextField label={'cases.omschrijving'} source="omschrijving" sortable={false} />
          <TextField label={'cases.status'} source="status_naam" sortable={false} />
          <TextField label={'cases.zaaktype'} source="zaaktype_naam" sortable={false} />
          <DateField
            label={'cases.registratiedatum'}
            source="registratiedatum"
            locales={locale}
            sortable={false}
          />
        </Datagrid>
      )}
    </CaseListWithCustomTitle>
  )
}

export default CasesList
