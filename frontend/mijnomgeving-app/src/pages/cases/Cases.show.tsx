import { FC } from 'react'
import {
  useNotify,
  useRefresh,
  useRedirect,
  useShowController,
  RecordContextProvider,
  SimpleShowLayout,
  TextField,
  DateField,
  ReferenceManyField,
  Datagrid,
  useTranslate,
  RaRecord,
  FunctionField
} from 'react-admin'
import { Divider, Grid, Box, Typography, Card } from '@mui/material'
import { UploadDocumentButton, BackButton, DownloadFileButton } from '../../components/buttons'
import { fileSize } from '../../utils'

export interface CaseRecord extends RaRecord {
  omschrijving: string
}

export interface DocumentRecord extends RaRecord {
  attributes: {
    name: string
    filesize: number
    mimetype: string
  }
}

const CasesShow: FC = () => {
  const translate = useTranslate()
  const notify = useNotify()
  const refresh = useRefresh()
  const redirect = useRedirect()

  const onError = (error) => {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      notify(`Could not load cases: ${error.message}`, { type: 'warning' })
      redirect('/cases')
      refresh()
  }

  const {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      error,
      isLoading,
      record
  } = useShowController<CaseRecord>({ queryOptions: { onError } })

  const customTitle = () =>
    `Zaak ${record?.identificatie || ''} - ${record?.omschrijving || ''}`

  if (isLoading) {
      return <div></div>
  }
  if (error) {
      return <div>{translate('warnings.casesError')}</div>
  }
  return (
    <RecordContextProvider value={record}>
      <Grid container sx={{ margin: '10px' }}>
        <Grid item xs={12} md={9}>
          <Typography variant="h5">{ customTitle() }</Typography>
        </Grid>
        <Grid item xs={12} md={3}>
          <Box display="flex" justifyContent="flex-end">
            <Typography variant="h6">{ record?.status_naam || '' }</Typography>
          </Box>
        </Grid>
      </Grid>
      <Divider sx={{ marginBottom: '20px' }}/>
      <Card>
        <SimpleShowLayout sx={{ WebkitFlex: 'unset' }}>
          <TextField label={'cases.zaaktype'} source="zaaktype_naam" />
          <TextField label={'cases.omschrijving'} source="omschrijving" />
          <DateField label={'cases.registratiedatum'} source="registratiedatum" />
          <DateField label={'cases.einddatum'} source="einddatum" />
        </SimpleShowLayout>
        <SimpleShowLayout sx={{ WebkitFlex: 'unset' }}>
          <ReferenceManyField
            label="Documents"
            reference="documents"
            target="case_uuid"
          >
            <Datagrid bulkActionButtons={false}>
              <TextField label="Naam" source="attributes.name" />
              <TextField label="Type" source="attributes.mimetype" />
              <FunctionField
                label="Grootte"
                render={(r: DocumentRecord) => fileSize(r.attributes.filesize)}
              />
              <FunctionField
                label="Download"
                render={(r: DocumentRecord) => <DownloadFileButton id={r.id} name={r.attributes.name} />}
              />
            </Datagrid>
          </ReferenceManyField>
        </SimpleShowLayout>
        <Grid container sx={{ margin: '10px' }}>
          <Grid item xs={12} >
            <Typography variant="h5">{ translate('cases.uploadDocument') }</Typography>
          </Grid>
          <Grid item xs={12}>
          <Typography variant="body1">{ translate('cases.uploadDocumentDescription') }</Typography>
          </Grid>
          <Grid item xs={12}>
            <UploadDocumentButton record={record} />
          </Grid>
        </Grid>
      </Card>
      <Grid container sx={{ margin: '10px' }}>
        <Grid item xs={12}>
          <Box display="flex" justifyContent="flex-end">
            <BackButton label="Back" to="/cases" />
          </Box>
        </Grid>
      </Grid>
    </RecordContextProvider>
  )
}

export default CasesShow