import WorkOutlineIcon from '@mui/icons-material/WorkOutline'
import CasesList from './Cases.list'
import CasesShow from './Cases.show'

const Cases = {
  name: 'cases',
  list: CasesList,
  show: CasesShow,
  icon: WorkOutlineIcon,
  options: {
    label: 'cases'
  }
}

export default Cases
