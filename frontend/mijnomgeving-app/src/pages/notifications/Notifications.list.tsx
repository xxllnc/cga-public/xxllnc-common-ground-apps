import { FC, useEffect, useState } from 'react'
import { Title, useGetIdentity } from 'react-admin'
import { NovuProvider, useNotifications } from '@novu/notification-center'
import {
  Paper,
  Box,
  Typography,
  TableContainer,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  IconButton,
  Collapse
} from '@mui/material'
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import { getEnvVariable } from '../../getEnvVariable'

interface Notification {
  _id: string
  seen: boolean
  content: any
  payload: {
    sender?: string
    subject?: string
    message?: string
  },
  createdAt: string
}

const Notifications: FC = () => {
  const { notifications, refetch, markAsSeen } = useNotifications()

  useEffect(() => {
    void refetch()
  }, [])

  const Row: FC<Notification> = ({ _id, seen, content, payload, createdAt }) => {
    const [open, setOpen] = useState(false)

    const onRowClick = () => {
      setOpen(!open)
      void markAsSeen(_id)
    }

    const subject = `${content}`.replace(/(<([^>]+)>)/ig, '')

    return (
      <>
        <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
          <TableCell>
            <IconButton
              aria-label="expand row"
              size="small"
              onClick={onRowClick}
            >
              {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
            </IconButton>
          </TableCell>
          <TableCell component="th" scope="row">
            { !seen ? <b>{ payload?.sender }</b> : <i>{ payload?.sender }</i> }
          </TableCell>
          <TableCell align="right">
            { !seen ? <b>{ subject }</b> : <i>{ subject }</i> }
          </TableCell>
          <TableCell align="right">
            { !seen ? <b>{ createdAt }</b> : <i>{ createdAt }</i> }
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
            <Collapse in={open} timeout="auto" unmountOnExit>
              <Box sx={{ margin: 1 }}>
                <Typography variant="h6" gutterBottom component="div">
                  Bericht details
                </Typography>
                <Typography variant="body2" gutterBottom component="div">
                  { payload?.message || ''}
                </Typography>
              </Box>
            </Collapse>
          </TableCell>
        </TableRow>
      </>
    )
  }

  return (
    <Paper elevation={0} sx={{ margin: '20px', background: 'unset' }}>
      <Title title="Notifications" />
      <Typography variant="h5">Mijn Berichten</Typography>
      <TableContainer component={Paper} sx={{ marginTop: '10px' }}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
            <TableCell></TableCell>
              <TableCell>Afzender</TableCell>
              <TableCell align="right">Onderwerp</TableCell>
              <TableCell align="right">Ontvangen</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            { notifications.map((notification: Notification) => (
              <Row
                // eslint-disable-next-line no-underscore-dangle
                key={notification._id}
                { ...notification }
              />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  )
}

export const NotificationsList = () => {
  const { identity } = useGetIdentity()

  return !identity?.aNummer ? <p>Loading...</p> : (
  <NovuProvider
    socketUrl="https://novu-ws.vcap.me"
    backendUrl="https://novu-api.vcap.me"
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    subscriberId={identity?.aNummer || ''}
    applicationIdentifier={getEnvVariable('REACT_APP_NOVU_APP_ID')}
    >
    <Notifications />
  </NovuProvider>
)}