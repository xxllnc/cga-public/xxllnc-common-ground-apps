import PostAddIcon from '@mui/icons-material/PostAdd'
import { NotificationsList } from './Notifications.list'

const Notifications = {
  name: 'notifications',
  list: NotificationsList,
  icon: PostAddIcon,
  options: {
    label: 'notifications'
  }
}

export default Notifications
