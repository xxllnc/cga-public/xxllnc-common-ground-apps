import { useLogin } from 'react-admin'
import styles from './login.module.css'
import digiDLogo from '../../assets/img/digid.gif'
import logo from '../../assets/img/logo-xxllnc.svg'
const Login = () => {
  const login = useLogin()

  return (
    <div className={styles.topContainer}>
      <img
        className={styles.xxllncLogo}
        src={logo}
        alt="xxllnc logo"
      />
      <div className={styles.mainContainer}>
        <div className={styles.infoContainer}>
          <h2>Inloggen voor uw MijnOmgeving</h2>
          <p className={styles.paragraph}>
            Voor het door u gekozen onderdeel is verificatie van uw identiteit
            vereist. Bij Gemeente Hof van Twente kunt u inloggen met DigiD.
            Voortaan kunt u met DigiD naar steeds meer overheidsinstellingen op
            internet. Wanneer u op de button van DigiD klikt, wordt u
            doorgestuurd naar de omgeving van DigiD. Nadat u bent ingelogd,
            wordt u automatisch teruggestuurd naar het door u gekozen onderdeel.
          </p>
          <div className={styles.loginContainer}>
            <img
              className={styles.digidLogoLarge}
              src={digiDLogo}
              alt="DigiD logo"
            />
            <a
              className={styles.loginLink}
              onClick={(event) => {
                event.preventDefault()
                void login({})
              }}
            >
              Inloggen met DigiD
            </a>
          </div>
        </div>
        <div className={styles.explanationContainer}>
          <div className={styles.flexContainer}>
            <img
              className={styles.digidLogoSmall}
              src={digiDLogo}
              alt="DigiD logo"
            />
            <div>
              <h3 className={styles.noMargingNoPadding}>Wat is DigiD?</h3>
              <p className={styles.paragraph}>
                DigiD staat voor Digitale Identiteit; het is een
                gemeenschappelijk systeem waarmee de overheid op internet uw
                identiteit kan verifiëren. Met uw DigiD kunt u bij steeds meer
                overheidsinstellingen terecht.
              </p>
            </div>
          </div>
          <div>
            <p className={styles.paragraph}>
              Nog geen DigiD inlogcode? Ga naar{' '}
              <a target="_blank" rel="noopener" href="http://www.digid.nl">
                http://www.digid.nl
              </a>{' '}
              om direct een DigiD inlogcode aan te vragen.
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Login
