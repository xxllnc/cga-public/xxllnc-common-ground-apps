export const getEnvVariable = (name: string) => {
  const result =
    // eslint-disable-next-line
    ((window as any)?.env?.[String(name)] ??
      process.env[String(name)] ??
      '') as string

  if (!result) {
    console.warn(
      `Missing environment variable ${name}. You have to set in in .env or yml files`
    )
  }

  return result
}
