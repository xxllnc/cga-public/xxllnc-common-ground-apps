/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

/**
 * An enumeration.
 */
export enum AuditLogChangeTypes {
  Insert = 'insert',
  Update = 'update',
  Delete = 'delete',
}

export interface AuditLogCreate {
  /**
   * Identifier of the app this event is coming from.
   * @format uuid
   */
  appId: string

  /** The unique identifier of the user. */
  userId?: string

  /**
   * The unique identifier of the organization of the user.
   * @format uuid
   */
  organizationId: string

  /**
   * Identifier of the app instance this event is coming from.
   * @format uuid
   */
  appInstanceId?: string

  /** Human readable name of the logged in user that triggered the event. If the system triggered the event, the name of the system. */
  displayName: string

  /**
   * The date / time of the event of the audit log item.
   * @format date-time
   */
  timestamp: string

  /** The change type of the audit log item. */
  changeType: AuditLogChangeTypes

  /** The summary of the audit log item. */
  summary: string

  /** Url to retrieve (GET) the resource concerning audit log item. */
  resourceUrl?: string

  /** The content of the change concerning this auditlog. */
  content?: object
}

export interface AuditLogOut {
  /**
   * Identifier of the app this event is coming from.
   * @format uuid
   */
  appId: string

  /** The unique identifier of the user. */
  userId?: string

  /**
   * The unique identifier of the organization of the user.
   * @format uuid
   */
  organizationId: string

  /**
   * Identifier of the app instance this event is coming from.
   * @format uuid
   */
  appInstanceId?: string

  /** Human readable name of the logged in user that triggered the event. If the system triggered the event, the name of the system. */
  displayName: string

  /**
   * The date / time of the event of the audit log item.
   * @format date-time
   */
  timestamp: string

  /** The change type of the audit log item. */
  changeType: AuditLogChangeTypes

  /** The summary of the audit log item. */
  summary: string

  /** Url to retrieve (GET) the resource concerning audit log item. */
  resourceUrl?: string

  /** The content of the change concerning this auditlog. */
  content?: object

  /**
   * Unique identifier of the audit log item.
   * @format uuid
   */
  id: string
}

export interface HTTPValidationError {
  /** Detail */
  detail?: ValidationError[]
}

export interface ValidationError {
  /** Location */
  loc: (string | number)[]

  /** Message */
  msg: string

  /** Error Type */
  type: string
}
