import { Auth0ContextInterface } from '@auth0/auth0-react/src/auth0-context'
import { UserIdentity } from 'ra-core'
import { getEnvVariable } from './getEnvVariable'
import { IngeschrevenPersoonHal } from './types/generatedPersonsTypes'

let lastStatus = 200
let identityPromise: Promise<UserIdentity>

const accessTokenPromises = {
  CGA: null as unknown as Promise<string>,
  ECO: null as unknown as Promise<string>,
}

export const authHeader = (token: string) => ({
  headers: {
    'authorization': `Bearer ${token}`,
    'organization-url': window.origin
  },
})

export const getAccessToken = (
  auth0: Auth0ContextInterface,
  audience: keyof typeof accessTokenPromises
): Promise<string> => {
  // eslint-disable-next-line security/detect-object-injection
  accessTokenPromises[audience] =
    // eslint-disable-next-line
    accessTokenPromises[audience] ||
    auth0.getAccessTokenSilently({
      audience: getEnvVariable(`REACT_APP_AUTH0_${audience}_API_AUDIENCE`),
    })

  // eslint-disable-next-line security/detect-object-injection
  return accessTokenPromises[audience]
}

export const authProvider = (
  auth0: Auth0ContextInterface,
  returnTo: string
) => ({
  login: () => {
    void auth0.loginWithRedirect()
    return Promise.reject()
  },
  logout: () => {
    if (auth0.isAuthenticated) {
      auth0.logout({ federated: false, returnTo })
      return Promise.reject()
    }

    return Promise.resolve()
  },
  checkError: (error: { status: number }) => {
    const { status } = error
    lastStatus = status

    if (status === 401) {
      auth0.isAuthenticated && auth0.logout()
      return Promise.reject()
    } else {
      return Promise.resolve()
    }
  },
  checkAuth: () =>
    auth0.isAuthenticated ? Promise.resolve() : Promise.reject(),
  getPermissions: () => Promise.resolve({ authorized: lastStatus !== 403 }),
  getIdentity: (): Promise<UserIdentity> => {
    identityPromise =
      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      identityPromise ||
      getAccessToken(auth0, 'CGA')
        .then((token) =>
          fetch(
            `/persons/api/v1/persons/${auth0.user?.name}`,
            authHeader(token)
          )
            .then((r) => r.json())
            .then((resp: IngeschrevenPersoonHal) => ({
              id: auth0.user?.name ?? '',
              fullName: `${resp?.naam?.voornamen} ${resp?.naam?.geslachtsnaam}`,
              ...resp,
            }))
        )
        .catch((err) => {
          console.warn(err)

          // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
          if (err && err.error === 'login_required') {
            auth0.logout({ federated: false, returnTo })
          }
        })

    return identityPromise
  },
})
