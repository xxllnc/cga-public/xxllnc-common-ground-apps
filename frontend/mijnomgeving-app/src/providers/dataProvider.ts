/* eslint-disable @typescript-eslint/no-unsafe-argument */
import { Auth0ContextInterface } from '@auth0/auth0-react/src/auth0-context'
import simpleRestProvider from 'ra-data-simple-rest'
import { fetchUtils } from 'react-admin'
import { getCasesUrl, getMessagesUrl, getUrl } from '../AppUrls'
import { getAccessToken } from '../auth'
import { mock } from '../utils'
import mockDataProvider from './dataProvider.mock'

export let memoryToken: string

interface Response { headers: Headers, json: any, body: string, status: number }
const getHttpClient = (auth0: Auth0ContextInterface) =>
  (url: string, options: RequestInit = {}): Promise<Response> =>
    getAccessToken(auth0, 'CGA').then((token) => {
      if (!token)
        return Promise.reject(process.env?.NODE_ENV === 'development' ? 'No user Token' : '')

      memoryToken = token
      console.log({ memoryToken })
      const user = { authenticated: true, token: `Bearer ${token}` }
      const headers = new Headers({ 'organization-url': window.origin, ...options?.headers })

      return fetchUtils.fetchJson(url, { ...options, user, headers })
    }).catch((error) => Promise.reject(error))

export default (auth0: Auth0ContextInterface) => {
  if (mock()) return mockDataProvider

  const getProviderForResource = (resource: string) => {
    switch (resource) {
      case 'documents': return casesProvider
      case 'cases': return casesProvider
      case 'messages': return messagesProvider
      default: return auditlogProvider
    }
  }

  const casesProvider = simpleRestProvider(getCasesUrl(), getHttpClient(auth0))
  const messagesProvider = simpleRestProvider(getMessagesUrl(), getHttpClient(auth0))
  const auditlogProvider = simpleRestProvider(getUrl(), getHttpClient(auth0))

  return {
    ...auditlogProvider,
    getList: (resource: string, params: any) => getProviderForResource(resource).getList(resource, params),
    getMany: (resource: string, params: any) => getProviderForResource(resource).getMany(resource, params),
    getManyReference: (resource: string, params: any) => getProviderForResource(resource).getManyReference(resource, params),
    getOne: (resource: string, params: any) => getProviderForResource(resource).getOne(resource, params),
  }
}