/* eslint-disable */
import {
  CreateParams,
  DeleteManyParams,
  DeleteParams,
  GetListParams,
  GetManyParams,
  GetManyReferenceParams,
  GetOneParams,
  UpdateManyParams,
  UpdateParams,
  DataProvider,
  fetchUtils
} from 'react-admin'
import { getUrl } from '../AppUrls'
import mocks from '../mocks'

interface Options extends RequestInit {
  user?: {
    authenticated?: boolean;
    token?: string;
  };
}

const HttpClient = (url: string, options: Options = {}) => fetchUtils.fetchJson(url, options)

const mockDataProvider: DataProvider = {
  getList: async (resource: string, params: GetListParams) => mocks.mockProvider.getList(resource, params),
  getOne: (resource: string, params: GetOneParams) => mocks.mockProvider.getOne(resource, params),
  create: (resource: string, params: CreateParams) => mocks.mockProvider.create(resource, params),
  delete: (resource: string, params: DeleteParams) => mocks.mockProvider.delete(resource, params),
  getMany: (resource: string, params: GetManyParams) => mocks.mockProvider.getMany(resource, params),
  getManyReference: (resource: string, params: GetManyReferenceParams) => HttpClient(`${getUrl()}/${resource}/${params.id}`).then(({ json }) => ({ data: json, total: json.length })),
  update: (resource: string, params: UpdateParams) => HttpClient(`${getUrl()}/${resource}/${params.id}`).then(response => ({ data: response.json })),
  updateMany: (resource: string, params: UpdateManyParams) => HttpClient(`${getUrl()}/${resource}/${JSON.stringify(params.ids)}`).then(response => ({ data: response.json })),
  deleteMany: (resource: string, params: DeleteManyParams) => HttpClient(`${getUrl()}/${resource}/${JSON.stringify(params.ids)}`).then(response => ({ data: response.json }))
}

export default mockDataProvider
