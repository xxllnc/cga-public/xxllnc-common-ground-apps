export const Loader = () => (
  <div className={'loading'}>
    <p>{'Bezig met laden.'}</p>
  </div>
)
