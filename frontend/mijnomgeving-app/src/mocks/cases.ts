import { Case } from '../types/cases'

const Cases: Case[] = [
  {
    id: '1',
    title: 'Test title 1',
  },
  {
    id: '2',
    title: 'Test title 2',
  },
  {
    id: '3',
    title: 'Test title 3',
  },
]

export default Cases