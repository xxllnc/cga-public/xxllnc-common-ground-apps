export const getUrl = (): string => window.location.origin + '/persons/api/v1'
export const getCasesUrl = (): string => window.location.origin + '/mijnomgeving/api/v1'
export const getMessagesUrl = (): string => window.location.origin + '/messages/api/v1'