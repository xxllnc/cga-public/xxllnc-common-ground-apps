const kb = 1000
const mb = 1000000
const gb = 1000000000

const round = (value: number, precision: number): number => {
  const multiplier = Math.pow(10, precision || 0)
  return Math.round(value * multiplier) / multiplier
}

export const fileSize = (bytes: number): string => {
  if (bytes > gb) return `${round(bytes / gb, 1)} GB`
  if (bytes > mb) return `${round(bytes / mb, 1)} MB`
  if (bytes > kb) return `${round(bytes / kb, 0)} KB`
  return `${bytes} B`
}