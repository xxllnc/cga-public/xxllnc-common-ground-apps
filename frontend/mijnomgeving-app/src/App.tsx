import { FC } from 'react'
import { Auth0Provider, useAuth0 } from '@auth0/auth0-react'
import { Admin, Resource, Layout, LayoutProps, CustomRoutes } from 'react-admin'
import { BrowserRouter, Route } from 'react-router-dom'
import LoginPage from './pages/login/Login'
import { PersonalInquiriesList } from './pages/identity/PersonalInquiriesList'
import { EditPersonalDetails } from './pages/identity/EditPersonalDetails'
import { Menu } from './layout/Menu'
import { AppBar } from './layout/AppBar'
import polyglotI18nProvider from 'ra-i18n-polyglot'
import { authProvider } from './auth'
import { i18nDutch } from './i18n'
import { Loader } from './Loader'
import DataProvider from './providers/dataProvider'
import { getEnvVariable } from './getEnvVariable'
import dashboard from './pages/dashboard'
import Notifications from './pages/notifications'
import Cases from './pages/cases'

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore
const i18nProvider = polyglotI18nProvider(() => i18nDutch, 'nl', {
  allowMissing: true,
})

const AppLayout: FC<LayoutProps> = (props) => (
  <Layout {...props} appBar={AppBar} menu={Menu}></Layout>
)

const AdminWrapper: FC = () => {
  const auth0 = useAuth0()
  const authProviderInstance = authProvider(
    auth0,
    `${window.location.origin}/mijnomgeving/login`
  )

  return auth0.isLoading ? (
    <Loader />
  ) : (
    <BrowserRouter basename="/mijnomgeving">
      <Admin
        i18nProvider={i18nProvider}
        loginPage={LoginPage}
        layout={AppLayout}
        authProvider={authProviderInstance}
        disableTelemetry
        dashboard={dashboard}
        dataProvider={DataProvider(auth0)}
      >
        <Resource name={Notifications.name} icon={Notifications.icon} list={Notifications.list} />
        <Resource name={Cases.name} icon={Cases.icon} list={Cases.list} show={Cases.show}/>
        <Resource name="auditlog" list={PersonalInquiriesList} />
        <Resource name="documents" />
        <CustomRoutes>
          <Route path="/identity" element={<EditPersonalDetails />} />
        </CustomRoutes>
      </Admin>
    </BrowserRouter>
  )
}

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const App = () => (
  <Auth0Provider
    domain={getEnvVariable('REACT_APP_AUTH0_DOMAIN')}
    clientId={getEnvVariable('REACT_APP_AUTH0_CLIENT_ID')}
    audience={getEnvVariable('REACT_APP_AUTH0_AUDIENCE')}
    redirectUri={`${window.location.origin}/mijnomgeving`}
    connection="DigiD-MijnOmgeving"
  >
    <AdminWrapper />
  </Auth0Provider>
)

export default App
