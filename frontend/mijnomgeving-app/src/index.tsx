import { lazy, Suspense } from 'react'
import { Loader } from './Loader'
import ReactDOM from 'react-dom'

const App = lazy(() => import('./App'))

ReactDOM.render(
  <Suspense fallback={<Loader />}>
    <App />
  </Suspense>,
  document.getElementById('root')
)
