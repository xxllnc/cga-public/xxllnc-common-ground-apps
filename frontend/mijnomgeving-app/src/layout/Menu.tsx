import { FC } from 'react'
import { Menu as RaMenu, MenuItemLink, MenuProps } from 'react-admin'

export const Menu: FC<MenuProps> = (props) => (
  <RaMenu {...props}>
    <MenuItemLink to="/" primaryText="Home" />
    <MenuItemLink to="/notifications" primaryText="Berichten" />
    <MenuItemLink to="/cases" primaryText="Zaken" />
    <MenuItemLink to="/identity" primaryText="Identiteit" />
  </RaMenu>
)
