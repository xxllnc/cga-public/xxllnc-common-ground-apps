import { FC } from 'react'
import { AppBar as RaBar, AppBarProps } from 'react-admin'
import Typography from '@mui/material/Typography'
import './theme.css'

export const AppBar: FC<AppBarProps> = (props) => (
  <RaBar {...props}>
    <Typography variant="h6" id="react-admin-title1">
      xxllnc
    </Typography>
    <Typography variant="h6" id="react-admin-title2">
      MijnOmgeving
    </Typography>
  </RaBar>
)
