
#####################################################################
# App pipeline template:
# 
# To use one of these template make sure that the following 
# environment variables are set:
# 
# - APP_PATH:         The path to the app.
#                     example: frontend/contactmomenten-app
# - CACHE_KEY_PREFIX: prefix that is used for the cache key
#                     example: cm
# 
# If needed set needs and dependencies in the job it self
#####################################################################


#####################################################################
# Rules template:
# This template is used by the other templates
#####################################################################
.app_rules_template:
  rules:
    - if: '($CI_PIPELINE_SOURCE == "merge_request_event" || $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH) && $CI_PIPELINE_SOURCE != "schedule"'
      changes:
        - "frontend/app-template.gitlab-ci.yml"
        - "${APP_PATH}/**/*"

#####################################################################
# Basic job template:
# This template is used by the other templates
#####################################################################
.app_basic_job_template: &app_basic_job
  extends: .app_rules_template
  dependencies: [] # no artifacts needed, so set dependencies to empty array
  cache:
    - key:
        files:
          - ${APP_PATH}/yarn.lock
        prefix: ${CACHE_KEY_PREFIX}
      paths:
        - ${APP_PATH}/node_modules
      policy: pull

#####################################################################
# App yarn install template:
#####################################################################
.app_install_template:
  stage: install
  extends: .app_rules_template
  needs: []
  script:
    - cd ${APP_PATH}
    - yarn install --cache-folder .yarn-cache --frozen-lockfile
  cache:
    - key:
        files:
          - ${APP_PATH}/yarn.lock
        prefix: ${CACHE_KEY_PREFIX}
      paths:
        - ${APP_PATH}/node_modules
      policy: pull-push
    - key:
        files:
          - ${APP_PATH}/yarn.lock
        prefix: ${CACHE_KEY_PREFIX}_yc
      paths:
        - ${APP_PATH}/.yarn-cache/
      policy: pull-push

#####################################################################
# App quality-assurance template:
#####################################################################
.app_qa_template:
  stage: quality-assurance
  <<: *app_basic_job
  script:
    - cd ${APP_PATH}
    - yarn license-checker-rseidelsohn --onlyAllow "${ALLOWED_LICENSES_FRONTEND}" --markdown --out licenses.md
  artifacts:
    name: frontend_${CI_COMMIT_REF_SLUG}-${CI_PIPELINE_IID}-licenses
    # expose_as: '${APP_NAME}_licenses'
    paths:
      - ${APP_PATH}/licenses.md
    expire_in: 4 mos

#####################################################################
# App template for the test job:                                    #
#####################################################################
.app_test_template:
  stage: test
  <<: *app_basic_job
  script:
    - cd ${APP_PATH}
    - yarn test:CI:coverage --coverageDirectory=. --passWithNoTests
  artifacts:
    name: frontend_${CI_COMMIT_REF_SLUG}-${CI_PIPELINE_IID}
    expire_in: 2 mos
    reports:
      junit: ${APP_PATH}/junit.xml
      coverage_report:
        coverage_format: cobertura
        path: ${APP_PATH}/cobertura-coverage.xml

#####################################################################
# App linting template:
#####################################################################
.app_lint_template:
  stage: test
  <<: *app_basic_job
  script:
    - cd ${APP_PATH}
    - yarn lint

#####################################################################
# App audit template:
#####################################################################
.app_audit_template:
  stage: audit
  <<: *app_basic_job
  script:
    - cd ${APP_PATH}
    - yarn add -D audit-ci
    - yarn audit-ci --config ./audit-ci.jsonc

#####################################################################
# App build template:
#####################################################################
.app_build_for_container_template:
  stage: build
  <<: *app_basic_job
  script:
    - cd ${APP_PATH}
    - cp ../../security.txt public
    - yarn build
    - cp ../../scripts/create-env-config.sh build/
  cache:
    - key:
        files:
          - ${APP_PATH}/yarn.lock
        prefix: ${CACHE_KEY_PREFIX}
      paths:
        - ${APP_PATH}/node_modules
      policy: pull
    - key:
        files:
          - ${APP_PATH}/yarn.lock
        prefix: ${CACHE_KEY_PREFIX}_build
      paths:
        - ${APP_PATH}/build
      policy: pull-push
  artifacts:
    name: frontend_${CI_COMMIT_REF_SLUG}-${CI_PIPELINE_IID}
    expire_in: 24 hrs
    paths:
      - ${APP_PATH}/build/

#####################################################################
# App template to build and push docker container:                  #
#####################################################################
.app_build_docker_container_template:
  extends: .app_rules_template
  stage: package-app
  image: docker:latest
  variables:
    DOCKER_IMAGE_BASE: "${CI_REGISTRY_IMAGE}/${APP_PATH}" # base url
    DOCKER_IMAGE_VERSION: "${DOCKER_IMAGE_BASE}:${VERSION}"
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule"'
      changes:
        - "frontend/app-template.gitlab-ci.yml"
        - "${APP_PATH}/**/*"
  tags:
    - socket
  script:
    - echo $VERSION
    - cd ${APP_PATH}
    - docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
    - docker build -t ${DOCKER_IMAGE_VERSION} -f Dockerfile.prod .
    - docker tag ${DOCKER_IMAGE_VERSION} ${DOCKER_IMAGE_BASE}:latest
    - docker push ${DOCKER_IMAGE_BASE} --all-tags

#####################################################################
# App to tag the latest container with the version                  #
#####################################################################
.app_tag_latest_container_template:
  stage: tag-latest-containers
  image: docker:latest
  variables:
    DOCKER_IMAGE_BASE: "${CI_REGISTRY_IMAGE}/${APP_PATH}" # base url
  tags:
    - socket
  script:
    - docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
    - docker pull ${DOCKER_IMAGE_BASE}:latest
    - docker tag ${DOCKER_IMAGE_BASE}:latest ${DOCKER_IMAGE_BASE}:$VERSION
    - docker push ${DOCKER_IMAGE_BASE} --all-tags
