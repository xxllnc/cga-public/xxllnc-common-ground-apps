import { FC } from 'react'
import { createTheme, ThemeProvider } from '@material-ui/core'
import { Redirect } from 'react-router'
import { getEnvVariable, LoginPage, theme } from 'xxllnc-react-components'

const debug = getEnvVariable('REACT_APP_DEBUG').toUpperCase() === 'TRUE'
const apiUrl = getEnvVariable('REACT_APP_LOGIN_INFO_URL')
const redirectUri = `${window.location.origin}/terugbelverzoeken#/login`

const LoginWithTheme: FC = () => (
  <ThemeProvider theme={createTheme(theme)}>
    <LoginPage
      scopes={'cga.user cga.admin'}
      redirect={<Redirect to="/" />}
      returnTo={redirectUri}
      debug={debug}
      apiurl={apiUrl}
    />
  </ThemeProvider>
)

export default LoginWithTheme
