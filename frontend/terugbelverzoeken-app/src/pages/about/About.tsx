import { Title } from 'react-admin'
import { LicensesInformation } from 'xxllnc-react-components'
import licenses from '../../licenses.json'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'

const About = (): JSX.Element => (
  <Card>
    <Title title="Over" />
    <CardContent>
      <LicensesInformation licenses={licenses} />
    </CardContent>
  </Card>
)

export default About