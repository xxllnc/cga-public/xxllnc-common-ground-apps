import EmployeeList from './Employees.list'
import EmployeeEdit from './Employees.edit'
import EmployeeCreate from './Employees.create'
import { CustomResourceProps } from '../Resources'

const EmployeeRequests: CustomResourceProps = {
  name: 'employees',
  list: EmployeeList,
  edit: EmployeeEdit,
  create: EmployeeCreate,
  options: {
    label: 'Werknemers',
    scopes: ['cga.admin', 'cga.user']
  }
}

export default EmployeeRequests
