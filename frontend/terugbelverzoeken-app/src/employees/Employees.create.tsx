import {
  CreateProps,
  Create,
  SimpleForm,
  TextInput
} from 'react-admin'
import { HeaderTitle } from 'xxllnc-react-components'

const EmployeeCreate = (props: CreateProps): JSX.Element => (
  <Create title={<HeaderTitle title="Werknemer toevoegen" />}{...props}>
    <SimpleForm>
      <TextInput source="name" />
      <TextInput source="email" />
    </SimpleForm>
  </Create>
)

export default EmployeeCreate
