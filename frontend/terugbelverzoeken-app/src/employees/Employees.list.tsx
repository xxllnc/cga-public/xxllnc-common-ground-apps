import {
  List,
  Datagrid,
  TextField,
  EmailField,
  ListProps,
  EditButton
} from 'react-admin'

const EmployeeList = (props: ListProps): JSX.Element => (
  <List {...props}
    exporter={false}
  >
    <Datagrid rowClick="edit">
      <TextField source="id" />
      <TextField source="name" />
      <EmailField source="email" />
      <EditButton />
    </Datagrid>
  </List>
)

export default EmployeeList
