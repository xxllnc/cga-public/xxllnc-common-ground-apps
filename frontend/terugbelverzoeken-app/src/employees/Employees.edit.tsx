import {
  Edit,
  EditProps,
  SimpleForm,
  TextInput
} from 'react-admin'
import { HeaderTitle, CustomToolbar} from 'xxllnc-react-components'

const EmployeeEdit = (props: EditProps): JSX.Element => (
  <Edit title={<HeaderTitle title="Werknemer %name% aanpassen" />} {...props}>
    <SimpleForm onSubmit={null} toolbar={<CustomToolbar/>}>
      <TextInput disabled source="id" />
      <TextInput source="name" />
      <TextInput source="email" />
    </SimpleForm>
  </Edit>
)

export default EmployeeEdit
