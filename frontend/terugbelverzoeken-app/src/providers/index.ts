import { DataProvider, fetchUtils } from 'ra-core'
import simpleRestProvider from 'ra-data-simple-rest'
import polyglotI18nProvider from 'ra-i18n-polyglot'
import { authProvider, authToken } from 'xxllnc-react-components'
import { getMembersUrl, getUrl } from '../AppUrls'
import { i18nDutch } from '../i18n'
import { mock } from '../utils/mock'
import mockDataProvider from './data'

const i18nProvider = polyglotI18nProvider(
  () => i18nDutch,
  'nl',
  { allowMissing: true }
)

interface Response { headers: Headers, json: any, body: string, status: number }
const httpClient = (url: string, options: RequestInit = {}): Promise<Response> => {

  const token = authToken.getToken() ?? null
  if (!token)
    return Promise.reject(process.env?.NODE_ENV === 'development' ? 'No user Token' : '')

  const user = { authenticated: true, token }
  const headers = new Headers({ 'organization-url': window.origin, ...options?.headers })

  return fetchUtils.fetchJson(url, { ...options, user, headers })
}

const getDataProvider = () => {

  const test = process.env?.NODE_ENV === 'test'
  if (mock())
    console.log('mock is set to true!')

  return !mock() && !test ? myDataProvider : mockDataProvider
}

const normalProvider = simpleRestProvider(getUrl(), httpClient)
const membersProvider = simpleRestProvider(getMembersUrl(), httpClient)

const myDataProvider: DataProvider = {
  ...normalProvider,
  getList: (resource, params) =>
    resource === 'members' ? membersProvider.getList(resource, params) : normalProvider.getList(resource, params),
  getMany: (resource, params) =>
    resource === 'members' ? membersProvider.getMany(resource, params) : normalProvider.getMany(resource, params)
}

const providers = {
  auth: authProvider,
  data: getDataProvider(),
  i18n: i18nProvider,
  url: getUrl()
}

export default providers
