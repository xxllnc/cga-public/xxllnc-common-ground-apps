import { FC } from 'react'
import Admin from './Admin'
import { useHistory, BrowserRouter as Router } from 'react-router-dom'
import { Auth0ProviderWithHistory, getEnvVariable } from 'xxllnc-react-components'

const redirectUri = `${window.location.origin}/terugbelverzoeken#/login`
const scope = 'cga.user cga.admin openid profile'
const getDomain = () => getEnvVariable('REACT_APP_AUTH0_DOMAIN')
const getClientID = () => getEnvVariable('REACT_APP_AUTH0_CLIENT_ID')
const getAudience = () => getEnvVariable('REACT_APP_AUTH0_AUDIENCE')

const Auth: FC = () => {
  const history = useHistory()

  return (
    <Auth0ProviderWithHistory
      history={history}
      redirectUri={redirectUri}
      scope={scope}
      domain={getDomain()}
      clientId={getClientID()}
      audience={getAudience()}
    >
      <Admin />
    </Auth0ProviderWithHistory>
  )
}

const App: FC = () => (
  <Router>
    <Auth />
  </Router>
)

export default App
