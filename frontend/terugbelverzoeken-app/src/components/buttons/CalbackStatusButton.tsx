import { Button, useRecordContext, useUpdate, useGetList } from 'react-admin'
import DoneIcon from '@material-ui/icons/Done'
import LockOpenIcon from '@material-ui/icons/LockOpen'
import { PydanticMainCallbackRequestDetails, Result, Statusses } from '../../types/callbackRequests'

export const CallbackStatusButton = (): JSX.Element | null => {
  const { data: results } = useGetList<Result>('results')
  const record = useRecordContext<PydanticMainCallbackRequestDetails>()
  const isOpen = record?.status === Statusses.Open

  let defaultResult: Result | undefined
  if (results) {
    Object.keys(results).map((key) => {
      if (results[Number(key)].default) {
        defaultResult = results[Number(key)]
      }
    })
  }
  const [reOpen, { loading }] = useUpdate(
    'callbackRequests',
    record?.id,
    {
      ...record,
      status: isOpen ? Statusses.Afgehandeld : Statusses.Open,
      resultId: isOpen ? defaultResult?.id : undefined
    },
    record
  )

  return defaultResult ? (
    <Button label={isOpen ? defaultResult.name : 'Heropen'} onClick={void reOpen} disabled={loading}>
      {isOpen ? <DoneIcon /> : <LockOpenIcon />}
    </Button>
  ) : null
}
