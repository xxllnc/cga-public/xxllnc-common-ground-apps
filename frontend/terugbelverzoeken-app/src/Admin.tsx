import { FC } from 'react'
import { Admin as ReactAdmin } from 'react-admin'
import { useAuth0User, getEnvVariable } from 'xxllnc-react-components'
import { Route } from 'react-router-dom'
import { Layout } from './layout/Layout'
import AboutPage from './pages/about/About'
import LoginPage from './pages/login/Login'
import providers from './providers'
import { resourcesForScopes } from './Resources'

const returnTo = `${window.location.origin}/terugbelverzoeken`
const mock = getEnvVariable('REACT_APP_MOCK_REQUESTS').toUpperCase() === 'TRUE'

const Admin: FC = () => {
  const auth0 = useAuth0User()

  return (
    <ReactAdmin
      customRoutes={[
        <Route
          path="/about"
          component={AboutPage}
        />,
      ]}
      i18nProvider={providers.i18n}
      authProvider={providers.auth(auth0, returnTo, mock)}
      dataProvider={providers.data}
      layout={Layout}
      loginPage={LoginPage}
      disableTelemetry
    >
      {resourcesForScopes}
    </ReactAdmin>
  )
}

export default Admin
