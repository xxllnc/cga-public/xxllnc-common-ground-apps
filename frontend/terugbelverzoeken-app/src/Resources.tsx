import { EditGuesser, ListGuesser, Resource, ResourceProps, ShowGuesser } from 'react-admin'
import { CustomFieldCreate, CustomFieldEdit, CustomFieldList } from 'xxllnc-react-components'
import PostAddIcon from '@material-ui/icons/PostAdd'
import CallbackRequests from './callbackRequests'
import About from './pages/about'

interface Permissions {
  scopes: string[]
}

export interface CustomResourceProps extends ResourceProps {
  options: {
    label: string
    settings?: boolean
    scopes: string[]
  }
}

const availableResources = ({ scopes }: Permissions): JSX.Element[] => [
  <Resource
    name="callbackRequests"
    icon={CallbackRequests.icon}
    list={CallbackRequests.list}
    create={CallbackRequests.create}
    edit={CallbackRequests.edit}
    options={{
      label: 'Terugbelverzoeken',
      settings: false,
      scopes: ['cga.admin', 'cga.user']
    }}
  />,
  <Resource
    name="about"
    icon={About.icon}
    options={{
      label: 'Over',
      settings: true,
      scopes: ['cga.admin', 'cga.user']
    }}
  />,
  <Resource
    name="employees"
    options={{
      label: 'Werknemers',
      settings: false,
      scopes: ['cga.admin', 'cga.user']
    }}
  />,
  <Resource
    name="results"
    list={scopes.some(scope => scope === 'cga.admin') ? ListGuesser : undefined}
    edit={scopes.some(scope => scope === 'cga.admin') ? EditGuesser : undefined}
    show={scopes.some(scope => scope === 'cga.admin') ? ShowGuesser : undefined}
    options={{
      label: 'Resultaten',
      settings: false,
      scopes: ['cga.admin', 'cga.user']
    }}
  />,
  <Resource
    name="customFields"
    icon={scopes.some(scope => scope === 'cga.admin') ? PostAddIcon : undefined}
    list={scopes.some(scope => scope === 'cga.admin') ? CustomFieldList : undefined}
    edit={scopes.some(scope => scope === 'cga.admin') ? CustomFieldEdit : undefined}
    create={scopes.some(scope => scope === 'cga.admin') ? CustomFieldCreate : undefined}
    options={{
      label: 'Eigen velden',
      settings: scopes.some(scope => scope === 'cga.admin') ? true : false,
      scopes: ['cga.admin', 'cga.user']
    }}
  />
]

const fallback = (): JSX.Element[] => [
  <Resource
    name={CallbackRequests.name}
    list={CallbackRequests.list}
    icon={CallbackRequests.icon}
    options={{
      label: 'Terugbelverzoeken'
    }}
  />
]

interface AResource {
  props: CustomResourceProps
}

export const resourcesForScopes = ({ scopes }: Permissions): JSX.Element[] => {
  if (!scopes || scopes.length === 0) return fallback()
  const resources = availableResources({ scopes }).filter((resource: AResource) => resource.props.options.scopes
    .find(scope => scopes.includes(scope)) !== undefined)
  if (!resources || resources.length === 0) return fallback()
  return resources
}
