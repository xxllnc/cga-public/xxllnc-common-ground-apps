import { getEnvVariable } from 'xxllnc-react-components'

export const getUrl = (): string => window.location.origin + '/terugbelverzoeken/api/v1'
export const getMembersUrl = (): string => getEnvVariable('REACT_APP_GEBRUIKERSBEHEER_SERVER_URL')