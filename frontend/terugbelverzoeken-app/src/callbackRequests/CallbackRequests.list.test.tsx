import { DataProviderContext } from 'react-admin'
import { TestContext } from 'ra-test'
import { createTheme, ThemeProvider } from '@material-ui/core'
import { fireEvent, render, screen, waitFor } from '@testing-library/react'
import CallbackRequestsList from './CallbackRequests.list'
import providers from '../providers'

const initialState = {
  admin: {
    resources: {
      callbackRequests: {
        list: {
          ids: [],
          selectedIds: [],
          total: 0
        }
      },
      employees: {
        list: {
          ids: [],
          selectedIds: [],
          total: 0
        }
      },
      results: {
        list: {
          ids: [],
          selectedIds: [],
          total: 0
        }
      }
    }
  }
}

describe('CallbackRequests list test', () => {

  it('should render the list with callback request rows', async () => {
    const baseTheme = createTheme()
    const dataProviderMock = providers.data
    const defaultListProps = {
      basePath: '/callbackRequests',
      resource: 'callbackRequests'
    }

    // Given
    render(
      <DataProviderContext.Provider value={dataProviderMock}>
        <ThemeProvider theme={baseTheme}>
          <TestContext enableReducers initialState={initialState}>
            <CallbackRequestsList {...defaultListProps} />
          </TestContext>
        </ThemeProvider>
      </DataProviderContext.Provider>
    )

    // Disable default filter open
    fireEvent.click(screen.getByRole('button', { name: 'Open' }))
    // We expect 4x the text 'Wouter' in the employee of 4 records
    await waitFor(() => expect(screen.getAllByText('Wouter')).toHaveLength(4))
    // We expect 7x the text Open (6x status of records, 1x in the Aside)
    expect(screen.getAllByText('Open')).toHaveLength(7)
    // We expect one text 'Ondersteuning bij ingebruikneming' in the subject of one record
    expect(screen.getByText('Ondersteuning bij ingebruikneming')).toBeDefined()
    // We expect one text 'Peter Pietenman' in the contact of one record
    expect(screen.getByText('Peter Pietenman')).toBeDefined()
    // We expect 1x the text 'Gebeld'
    await waitFor(() => expect(screen.getAllByText('Gebeld')).toHaveLength(1))
    expect(screen.getAllByText('Niet bereikbaar')).toHaveLength(1)
  })

  it('should render the filtered list after status filter button is clicked', async () => {
    const baseTheme = createTheme()
    const dataProviderMock = providers.data
    const defaultListProps = {
      basePath: '/callbackRequests',
      resource: 'callbackRequests'
    }

    // Given
    render(
      <DataProviderContext.Provider value={dataProviderMock}>
        <ThemeProvider theme={baseTheme}>
          <TestContext enableReducers initialState={initialState}>
            <CallbackRequestsList {...defaultListProps} />
          </TestContext>
        </ThemeProvider>
      </DataProviderContext.Provider>
    )

    // We expect 7x the text 'Open' (6x status of records, 1x in the Aside)
    await waitFor(() => expect(screen.getAllByText('Open')).toHaveLength(7))
    // If we click on the afgehandeld button
    fireEvent.click(screen.getByRole('button', { name: 'Afgehandeld' }))
    // We expect 3x the text 'Afgehandeld' (2x status of records, 1x in the Aside)
    await waitFor(() => expect(screen.getAllByText('Afgehandeld')).toHaveLength(3))
    // We expect only one text 'Open' in the Aside
    await waitFor(() => expect(screen.getAllByText('Open')).toHaveLength(1))
  })

  it('should render the filtered list after result filter button is clicked', async () => {
    const baseTheme = createTheme()
    const dataProviderMock = providers.data
    const defaultListProps = {
      basePath: '/callbackRequests',
      resource: 'callbackRequests'
    }

    // Given
    render(
      <DataProviderContext.Provider value={dataProviderMock}>
        <ThemeProvider theme={baseTheme}>
          <TestContext enableReducers initialState={initialState}>
            <CallbackRequestsList {...defaultListProps} />
          </TestContext>
        </ThemeProvider>
      </DataProviderContext.Provider>
    )

    // Disable default filter open
    fireEvent.click(screen.getByRole('button', { name: 'Open' }))
    // We expect 1x the text 'Afgehandeld' (1x status of records)
    await waitFor(() => expect(screen.getAllByText('Afgehandeld')).toHaveLength(1))
    // If we click on the Afgehandeld button
    fireEvent.click(screen.getByRole('button', { name: 'Afgehandeld' }))
    await waitFor(() => expect(screen.getAllByText('Afgehandeld')).toHaveLength(3))
  })

  it('should render the filtered list after created at today filter button is clicked', async () => {
    const baseTheme = createTheme()
    const dataProviderMock = providers.data
    const defaultListProps = {
      basePath: '/callbackRequests',
      resource: 'callbackRequests'
    }

    // Given
    render(
      <DataProviderContext.Provider value={dataProviderMock}>
        <ThemeProvider theme={baseTheme}>
          <TestContext enableReducers initialState={initialState}>
            <CallbackRequestsList {...defaultListProps} />
          </TestContext>
        </ThemeProvider>
      </DataProviderContext.Provider>
    )

    // If we click on the today button
    fireEvent.click(screen.getByRole('button', { name: 'Vandaag' }))
    // We expect the text 'Datum van'
    await waitFor(() => expect(screen.getByText('Datum van')).toBeDefined())
    // If we click on the last week button
    fireEvent.click(screen.getByRole('button', { name: 'Vorige week' }))
    // We expect the text 'Datum van'
    await waitFor(() => expect(screen.getByText('Datum van')).toBeDefined())
    // We expect the text 'Datum tot'
    await waitFor(() => expect(screen.getByText('Datum tot')).toBeDefined())
  })

})

