import {
  AutocompleteInput,
  Create,
  CreateProps,
  ReferenceInput,
  required,
  SimpleForm,
  TextInput,
  useGetList,
  ReduxState
} from 'react-admin'
import { CustomField } from '../types/callbackRequests'
import { HeaderTitle, CustomFieldInput } from 'xxllnc-react-components'
import { useSelector } from 'react-redux'
import { callbackRequestTransform, CBREmployee } from '../utils/callbackRequestTransform'
import { requiredMessage } from '../utils'

const CallbackRequestsCreate = (props: CreateProps): JSX.Element => {
  const employees = useSelector((state: ReduxState) => state.admin.resources.employees.data)
  const { data: customFields, ids: customFieldIds } =
    useGetList<CustomField>('customFields', undefined, { field: 'sortOrder', order: 'ASC' }, { archived: false })

  return (
    <Create
      {...props}
      title={<HeaderTitle title="Terugbelverzoek toevoegen" />}
      transform={callbackRequestTransform}
    >
      <SimpleForm redirect="list">
        <ReferenceInput
          label="Toewijzen aan"
          source="employee"
          reference="employees"
          validate={required('Een medewerker is verplicht, selecteer een medewerker')}
          sort={{ field: 'name', order: 'ASC' }}
          format={(employee: CBREmployee) => employee?.id}
          parse={(id: number) => employees[`${id}`]}
        >
          <AutocompleteInput optionText="name" fullWidth />
        </ReferenceInput>
        <TextInput label={'Contactpersoon'} source="contact" validate={[required(requiredMessage('de contactpersoon'))]} fullWidth />
        <TextInput label={'Onderwerp'} source="subject" validate={[required(requiredMessage('het onderwerp'))]} fullWidth />
        <TextInput label={'Samenvatting'} source="summary" multiline fullWidth />
        <TextInput label={'Telefoonnummer'} source="phone" fullWidth
          validate={required(requiredMessage('het telefoonnummer'))}
        />
        {
          customFields && customFieldIds.map((id, index) => <CustomFieldInput
            key={id}
            index={index}
            customField={customFields[Number(id)]}
          />
          )
        }
      </SimpleForm>
    </Create>
  )
}

export default CallbackRequestsCreate
