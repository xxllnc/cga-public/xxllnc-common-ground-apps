import { Theme, useMediaQuery } from '@material-ui/core'
import { DateTimeInput, Filter, ListProps, TextInput } from 'react-admin'
import { QuickFilter } from 'xxllnc-react-components'

export const FilterToolbar = (props: ListProps): JSX.Element => {
  const isSmall = useMediaQuery<Theme>(theme => theme.breakpoints.down('sm'))

  return (
    <Filter {...props}>
      <TextInput label="Zoeken" source="q" alwaysOn />
      {isSmall && <QuickFilter
        label="Door mij aangemaakt"
        source="createdByMe"
        defaultValue={true}
      />}
      {isSmall && <QuickFilter
        label="Aan mij toegewezen"
        source="assignedToMe"
        defaultValue={true}
      />}
      {isSmall && <QuickFilter
        label="Open"
        source="status"
        defaultValue="Open"
      />}
      <DateTimeInput source="createdAt_gte" label="Datum van" />
      <DateTimeInput source="createdAt_lte" label="Datum tot" />
    </Filter>
  )
}
