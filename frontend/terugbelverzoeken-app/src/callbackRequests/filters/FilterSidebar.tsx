import { FilterSidebar as RCFilterSideBar, CreatedAtFilter, CreatedByAssignedToMeFilter } from 'xxllnc-react-components'
import { FilterList, FilterListItem } from 'react-admin'
import LockOpenIcon from '@material-ui/icons/LockOpen'
import { Statusses } from '../../types/callbackRequests'

const StatusFilter = (): JSX.Element => (
  <FilterList
    label="Status"
    icon={<LockOpenIcon />}>
    <FilterListItem
      label="Open"
      value={{
        status: Statusses.Open,
      }}
    />
    <FilterListItem
      label="Afgehandeld"
      value={{
        status: Statusses.Afgehandeld,
      }}
    />
  </FilterList>
)

export const FilterSidebar = (): JSX.Element => (
  <RCFilterSideBar>
    <CreatedByAssignedToMeFilter />
    <StatusFilter />
    <CreatedAtFilter />
  </RCFilterSideBar>
)
