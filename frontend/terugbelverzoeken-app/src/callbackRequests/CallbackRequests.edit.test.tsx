import { DataProviderContext } from 'react-admin'
import { TestContext } from 'ra-test'
import { createTheme, ThemeProvider } from '@material-ui/core'
import { fireEvent, render, screen, waitFor } from '@testing-library/react'
import CallbackRequestsEdit from './CallbackRequests.edit'
import providers from '../providers'
import callbackRequestsMock from '../mocks/callbackRequests'
import employees from '../mocks/employees'

const initialState = {
  admin: {
    resources: {
      callbackRequests: {
        props: {
          name: 'callbackRequests',
          options: {},
          hasList: true,
          hasEdit: true,
          hasShow: false,
          hasCreate: true
        },
        data: {
          CB001: callbackRequestsMock[0]
        }
      },
      employees: {
        data: {
          1: employees[0]
        }
      }
    }
  }
}

describe('CallbackRequests edit test', () => {

  it('should render the edit', async () => {
    const baseTheme = createTheme()
    const dataProviderMock = providers.data

    const defaultEditProps = {
      basePath: '/callbackRequests/CB001',
      resource: 'callbackRequests',
      id: 'CB001'
    }

    //Given
    render(
      <DataProviderContext.Provider value={dataProviderMock}>
        <ThemeProvider theme={baseTheme}>
          <TestContext enableReducers initialState={initialState}>
            <CallbackRequestsEdit {...defaultEditProps} />
          </TestContext>
        </ThemeProvider>
      </DataProviderContext.Provider>
    )

    await waitFor(() => expect(screen.getByLabelText('Onderwerp *')).toBeDefined())
    expect(screen.getByLabelText('Samenvatting')).toBeDefined()
  })

  it('should change editAssigned', async () => {
    const baseTheme = createTheme()
    const dataProviderMock = providers.data

    const defaultEditProps = {
      basePath: '/callbackRequests/CB001',
      resource: 'callbackRequests',
      id: 'CB001'
    }

    //Given
    render(
      <DataProviderContext.Provider value={dataProviderMock}>
        <ThemeProvider theme={baseTheme}>
          <TestContext enableReducers initialState={initialState}>
            <CallbackRequestsEdit {...defaultEditProps} />
          </TestContext>
        </ThemeProvider>
      </DataProviderContext.Provider>
    )

    await waitFor(() => expect(screen.getByLabelText('Onderwerp *')).toBeDefined())
    expect(screen.getByLabelText('Huidige toegewezen')).toBeDefined()
    expect(screen.getByLabelText('Aanpassen')).toBeDefined()
    fireEvent.click(screen.getByLabelText('Aanpassen'))
    expect(screen.getByText('Toewijzen aan *')).toBeDefined()
  })
})
