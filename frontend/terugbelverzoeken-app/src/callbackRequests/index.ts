import CallbackRequestsList from './CallbackRequests.list'
import CallbackRequestsCreate from './CallbackRequests.create'
import CallbackRequestsEdit from './CallbackRequests.edit'
import PhoneCallbackIcon from '@material-ui/icons/PhoneCallback'
import { CustomResourceProps } from '../Resources'

const CallbackRequests: CustomResourceProps = {
  name: 'callbackRequests',
  list: CallbackRequestsList,
  create: CallbackRequestsCreate,
  edit: CallbackRequestsEdit,
  icon: PhoneCallbackIcon,
  options: {
    label: 'Terugbelverzoeken',
    settings: false,
    scopes: ['cga.admin', 'cga.user']
  }
}

export default CallbackRequests
