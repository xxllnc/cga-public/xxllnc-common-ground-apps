import { List, ListProps, SimpleList } from 'react-admin'
import { CustomFieldListGuesser, useAuthorized } from 'xxllnc-react-components'
import { Theme, useMediaQuery } from '@material-ui/core'
import { FilterSidebar, FilterToolbar } from './filters'
import { Statusses } from '../types/callbackRequests'

const CallbackRequestsList = (props: ListProps): JSX.Element => {
  useAuthorized(props)
  const isXSmall = useMediaQuery<Theme>(theme => theme.breakpoints.down('xs'))

  return isXSmall ? (
    <List
      {...props}
      aside={<FilterSidebar />}
      filters={<FilterToolbar />}
      bulkActionButtons={false}
      exporter={false}
      perPage={10}
    >
      <SimpleList
        // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
        primaryText={record => `${record.contact} ` }
        // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
        secondaryText={record => `${record.subject}`}
        tertiaryText={record => `${record.id}`}
      />
    </List>
  ) : (
    <CustomFieldListGuesser
      {...props}
      filterDefaultValues={{ status: Statusses.Open, assignedToMe: true }}
      aside={<FilterSidebar />}
      filters={<FilterToolbar />}
      bulkActionButtons={false}
      exporter={false}
      ignoreList={['uuid', 'updatedAt', 'createdAt', 'customFields', 'assignedToId']}
    />
  )
}

export default CallbackRequestsList
