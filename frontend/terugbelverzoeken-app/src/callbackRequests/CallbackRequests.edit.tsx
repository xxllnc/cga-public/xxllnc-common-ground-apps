import { useState, FC } from 'react'
import {
  Edit,
  required,
  TextInput,
  EditProps,
  TopToolbar,
  SimpleForm,
  SelectInput,
  ReferenceInput,
  useRecordContext,
  AutocompleteInput,
  ReduxState,
  BooleanInput
} from 'react-admin'
import { useSelector } from 'react-redux'
import { Grid } from '@material-ui/core'
import { PydanticMainCallbackRequestDetails, Statusses } from '../types/callbackRequests'
import { CallbackStatusButton } from '../components/buttons/CalbackStatusButton'
import { callbackRequestTransform, CBREmployee } from '../utils/callbackRequestTransform'
import { HeaderTitle, CustomFieldInput, RestrictedToolbar } from 'xxllnc-react-components'
import { requiredMessage } from '../utils'

const CustomFields: FC = () => {
  const record = useRecordContext<PydanticMainCallbackRequestDetails>()
  const customFields = record?.customFields ? [...record.customFields] : []

  return (
    <>
      {
        customFields?.map((customField, index) => (
          <CustomFieldInput
            key={customField.id}
            index={index}
            customField={customField}
          />))
      }
    </>
  )
}

const Actions = () => (
  <TopToolbar>
    <CallbackStatusButton />
  </TopToolbar>
)

const CallbackRequestsEdit: FC<EditProps> = (props) => {
  const [editAssigned, setEditAssigned] = useState(false)
  const employees = useSelector((state: ReduxState) => state.admin.resources.employees.data)

  return (
    <Edit
      {...props}
      title={<HeaderTitle title="Terugbelverzoek van %contact% aanpassen" />}
      actions={<Actions />}
      transform={callbackRequestTransform}
    >
      <SimpleForm redirect="list" toolbar={<RestrictedToolbar/>}>
        <Grid container style={{width: '100%'}} spacing={2}>
          <Grid item xs={8} md={10}>
            { editAssigned ? <ReferenceInput
              label="Toewijzen aan"
              source="employee"
              reference="employees"
              validate={required('Een medewerker is verplicht, selecteer een medewerker')}
              sort={{ field: 'name', order: 'ASC' }}
              format={(employee: CBREmployee) => employee?.id}
              parse={(id: number) => employees[`${id}`]}
            >
              <AutocompleteInput optionText="name" fullWidth />
            </ReferenceInput> :
              <TextInput label={'Huidige toegewezen'} source="assignedToName" disabled fullWidth />}
          </Grid>
          <Grid item xs={4} md={2}>
            <BooleanInput
              label={'Aanpassen'}
              source="assign"
              parse={(value: boolean) => {
                setEditAssigned(value)
                return value
              }} />
          </Grid>
        </Grid>
        <TextInput label={'Contactpersoon'} source="contact" validate={required(requiredMessage('de contactpersoon'))} fullWidth />
        <TextInput label={'Onderwerp'} source="subject" validate={[required(requiredMessage('het onderwerp'))]} fullWidth />
        <TextInput label={'Samenvatting'} source="summary" multiline fullWidth />
        <TextInput label={'Telefoon'} source="phone" validate={required(requiredMessage('het telefoonnummer'))} fullWidth />
        <CustomFields />
        <SelectInput
          label={'Huidige status'}
          source="status"
          choices={Object.values(Statusses).map(status => ({ id: status, name: status }))}
        />
        <ReferenceInput
          label="Resultaat"
          source="resultId"
          reference="results"
          sort={{ field: 'name', order: 'ASC' }}
        >
          <SelectInput label={'Resultaat'} optionText="name" fullWidth />
        </ReferenceInput>
      </SimpleForm>
    </Edit>
  )
}

export default CallbackRequestsEdit
