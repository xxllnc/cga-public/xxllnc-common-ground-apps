import { FC } from 'react'
import { LayoutProps, getResources, ReduxState } from 'react-admin'
import { CustomLayout, Resources } from 'xxllnc-react-components'
import { useSelector } from 'react-redux'

export const Layout: FC<LayoutProps> = (props) => {
  const open = useSelector((state: ReduxState) => state.admin.ui.sidebarOpen)
  const resources = useSelector<Record<string, unknown>, Resources[]>(getResources)

  return (
    <CustomLayout {...props} appName='Terugbelverzoeken' resources={resources} open={open} />
  )
}