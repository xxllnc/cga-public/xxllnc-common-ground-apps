import { Record } from 'react-admin'

export interface CBREmployee {
  id: number
  name: string
  email: string
  uuid: string
}

export interface CallbackRequestProps extends Record {
  assign?: boolean
  employee?: CBREmployee
}

export const callbackRequestTransform = (data: CallbackRequestProps): Record => {
  const {assign, employee, ...rest} = data
  if (!employee || assign === false) return data
  return {
    ...rest,
    assignedToId: employee?.id,
    assignedToName: employee?.name
  }
}