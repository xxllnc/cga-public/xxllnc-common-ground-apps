export const requiredMessage = (field: string): string => {
  const message = `${field} is verplicht, vul ${field} in`
  return message.charAt(0).toUpperCase() + message.slice(1)
}