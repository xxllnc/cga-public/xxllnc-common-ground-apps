import { lazy, Suspense } from 'react'
import ReactDOM from 'react-dom/client'
import reportWebVitals from './reportWebVitals'
import './styles.css'

const App = lazy(() => import('./App'))

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
)

root.render(
  <Suspense fallback={
    <div className={'loading'}>
      <p>{'Bezig met laden.'}</p>
    </div>
  }>
    <App />
  </Suspense>
)

reportWebVitals()
