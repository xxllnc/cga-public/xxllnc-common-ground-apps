import { Identifier } from 'ra-core/'
import { Result } from '../types/callbackRequests'

export const removeResult = (id: Identifier): any => {
  const tobeDeleted = results.find(record => String(record.id) === id)
  results = results.filter(record => String(record.id) === id)
  return tobeDeleted
}

let results: Result[] = [
  {
    uuid: '1',
    id: 1,
    name: 'Gebeld',
    default: true
  },
  {
    uuid: '2',
    id: 2,
    name: 'Niet bereikbaar',
    default: false
  }
]

export default results
