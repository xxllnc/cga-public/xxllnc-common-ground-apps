/* eslint-disable max-len */
import { PydanticMainCallbackRequestDetails, CustomFieldTypes, Statusses } from '../types/callbackRequests'

const callbackRequests: PydanticMainCallbackRequestDetails[] = [
  {
    uuid: 'CB001',
    id: 'CB001',
    createdAt: '2021-01-01T12:48:36.252',
    contact: 'Tessa Tester (wouter)',
    subject: 'Vraag over sales',
    summary: 'Kan Peter Moen mevrouw Tester terug bellen?',
    phone: '0123456789',
    status: Statusses.Open,
    resultId: undefined,
    updatedAt: '',
    employeeId: 1,
    customFields: [{
      id: 1,
      sortOrder: 1,
      name: 'Extra telefoonnummer',
      description: 'Een extra telefoon nummer',
      required: false,
      type: CustomFieldTypes.Text,
      value: '0612345678'
    }, {
      id: 2,
      sortOrder: 2,
      name: 'Select',
      description: 'Een select invoer',
      required: false,
      type: CustomFieldTypes.Select,
      options: [{
        id: '1',
        name: 'optie1'
      }, {
        id: '2',
        name: 'optie2'
      }],
    }, {
      id: 3,
      sortOrder: 3,
      name: 'Uitleg',
      description: 'Dit is de uitleg',
      required: false,
      type: CustomFieldTypes.Text,
      value: 'Dit is de uitleg'
    }]
  },
  {
    uuid: 'CB002',
    id: 'CB002',
    createdAt: '2021-02-02T10:37:36.252',
    contact: 'Peter Pietenman',
    subject: 'Ondersteuning bij ingebruikneming',
    summary: 'Pellentesque vel varius magna. Aliquam erat volutpat. Morbi fringilla placerat nunc ut ultrices. Donec felis augue, ultrices vel ex quis, commodo ultrices risus. Nam tincidunt scelerisque enim quis tempor. Suspendisse viverra purus tellus, vel luctus metus gravida a. In quis scelerisque neque.',
    phone: '0123456789',
    status: Statusses.Open,
    resultId: 2,
    updatedAt: '',
    employeeId: 2,
  },
  {
    uuid: 'CB003',
    id: 'CB003',
    createdAt: '2021-03-03T15:39:42.252',
    contact: 'Willem Wever',
    subject: 'Contact na klacht',
    summary: 'Vivamus risus magna, fermentum quis convallis vitae, posuere sit amet neque. Duis rutrum id arcu sed aliquam. Nullam at nisi vitae massa lobortis tempus. Ut posuere pharetra elit tempor consectetur.',
    phone: '0612345678',
    status: Statusses.Afgehandeld,
    resultId: 1,
    updatedAt: '',
    employeeId: 3
  },
  {
    uuid: 'CB004',
    id: 'CB004',
    createdAt: '2021-04-04T14:29:42.252',
    contact: 'Piet pieter',
    subject: 'Contact na klacht',
    summary: 'Vivamus risus magna, fermentum quis convallis vitae, posuere sit amet neque. Duis rutrum id arcu sed aliquam. Nullam at nisi vitae massa lobortis tempus. Ut posuere pharetra elit tempor consectetur.',
    phone: '0612345688',
    status: Statusses.Afgehandeld,
    resultId: undefined,
    updatedAt: '',
    employeeId: 2
  },
  {
    uuid: 'CB005',
    id: 'CB005',
    createdAt: '2021-05-05T12:13:42.252',
    contact: 'Thorsten Schipper',
    subject: 'Contact na klacht',
    summary: 'Vivamus risus magna, fermentum quis convallis vitae, posuere sit amet neque. Duis rutrum id arcu sed aliquam. Nullam at nisi vitae massa lobortis tempus. Ut posuere pharetra elit tempor consectetur.',
    phone: '0612345750',
    status: Statusses.Open,
    resultId: undefined,
    updatedAt: '',
    employeeId: 2
  },
  {
    uuid: 'CB006',
    id: 'CB006',
    createdAt: '2021-05-13T18:46:42.252',
    contact: 'Niels Oomke',
    subject: 'Contact na klacht',
    summary: 'Vivamus risus magna, fermentum quis convallis vitae, posuere sit amet neque. Duis rutrum id arcu sed aliquam. Nullam at nisi vitae massa lobortis tempus. Ut posuere pharetra elit tempor consectetur.',
    phone: '0612345740',
    status: Statusses.Open,
    resultId: undefined,
    updatedAt: '',
    employeeId: 3
  },
  {
    uuid: 'CB007',
    id: 'CB007',
    createdAt: '2021-05-18T09:32:42.252',
    contact: 'Puck Tib',
    subject: 'Contact na klachten',
    summary: 'Vivamus risus magna, fermentum quis convallis vitae, posuere sit amet neque. Duis rutrum id arcu sed aliquam. Nullam at nisi vitae massa lobortis tempus. Ut posuere pharetra elit tempor consectetur.',
    phone: '0612345720',
    status: Statusses.Open,
    resultId: undefined,
    updatedAt: '',
    employeeId: 1
  },
  {
    uuid: 'CB008',
    id: 'CB008',
    createdAt: '2021-05-19T10:48:42.252',
    contact: 'Mario Super',
    subject: 'Contact na klacht',
    summary: 'Vivamus risus magna, fermentum quis convallis vitae, posuere sit amet neque. Duis rutrum id arcu sed aliquam. Nullam at nisi vitae massa lobortis tempus. Ut posuere pharetra elit tempor consectetur.',
    phone: '0612345720',
    status: Statusses.Open,
    resultId: undefined,
    updatedAt: '',
    employeeId: 2
  }
]

export default callbackRequests
