import { CustomField, CustomFieldTypes } from '../types/callbackRequests'

const customFields: CustomField[] = [
  {
    uuid: '1',
    id: 1,
    name: 'Mobiel',
    description: 'Mobiel telefoonnummer veld',
    required: true,
    type: CustomFieldTypes.String,
    sortOrder: 1
  },
  {
    uuid: '2',
    id: 2,
    name: 'Vrije invoer',
    description: 'Een extra invoerveld',
    required: false,
    type: CustomFieldTypes.Text,
    sortOrder: 2
  }
]

export default customFields
