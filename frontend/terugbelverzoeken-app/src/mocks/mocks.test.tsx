import { CreateParams, GetOneParams } from 'react-admin'
import mockDataProvider from '../providers/data'
import customFields from './customFields'
import results from './results'
import employees from './employees'
import callbackRequests from './callbackRequests'
import {  CustomFieldTypes, Statusses } from '../types/callbackRequests'


describe('Mocks test', () => {

  it('mockDataProvider should return the first mockRecource of every recource', async () => {

    const params: GetOneParams = {
      id: '1'
    }

    const paramsCallbackRequest: GetOneParams = {
      id: callbackRequests[0].id
    }

    const callbackRequest = () => Promise.resolve({
      data: callbackRequests[0]
    })

    const customField = () => Promise.resolve({
      data: customFields[0]
    })

    const employee = () => Promise.resolve({
      data: employees[0]
    })

    const result = () => Promise.resolve({
      data: results[0]
    })

    expect(await mockDataProvider.getOne('callbackRequests', paramsCallbackRequest)).toEqual(await callbackRequest())
    expect(await mockDataProvider.getOne('customFields', params)).toEqual(await customField())
    expect(await mockDataProvider.getOne('employees', params)).toEqual(await employee())
    expect(await mockDataProvider.getOne('results', params)).toEqual(await result())
  })

  it('mockDataProvider should create the mockRecources', async () => {
    const callbackRequest = {
      contact: 'Tessa Tester',
      subject: 'Vraag over sales',
      status: Statusses.Open,
      createdAt: '2021-01-01T12:48:36.252'
    }

    const paramsCallbackRequest: CreateParams = {
      data: callbackRequest
    }

    const callbackRequestResult = () => Promise.resolve({
      data: {
        ...callbackRequest,
        id: 'CB009',
        uuid: 'CB009'
      }
    })

    const customField = {
      name: 'Mobiel',
      type: CustomFieldTypes.String,
      sortOrder: 1,
    }

    const paramsCustomFields: CreateParams = {
      data: customField
    }

    const customFieldResult = () => Promise.resolve({
      data: {
        ...customField,
        id: '3',
        uuid: '3'
      }
    })

    const employee = {
      name: 'Irene',
      email: 'Irene@test.nl'
    }

    const paramsEmployees: CreateParams = {
      data: employee
    }

    const employeeResult = () => Promise.resolve({
      data: {
        ...employee,
        id: '7',
        uuid: '7'
      }
    })

    expect(await mockDataProvider.create('callbackRequests', paramsCallbackRequest)).toEqual(await callbackRequestResult())
    expect(await mockDataProvider.create('customFields', paramsCustomFields)).toEqual(await customFieldResult())
    expect(await mockDataProvider.create('employees', paramsEmployees)).toEqual(await employeeResult())
  })

})
