import { Employee } from '../types/callbackRequests'

const employees: Employee[] = [
  {
    uuid: '1',
    id: 1,
    name: 'Stijn',
    email: 'stijn@test.nl'
  },
  {
    uuid: '2',
    id: 2,
    name: 'Wouter',
    email: 'wouter@test.nl'
  },
  {
    uuid: '3',
    id: 3,
    name: 'Niek',
    email: 'niek@test.nl'
  },
  {
    uuid: '4',
    id: 4,
    name: 'Erwin',
    email: 'erwin@test.nl'
  },
  {
    uuid: '5',
    id: 5,
    name: 'Gabrielle',
    email: 'Gabrielle@test.nl'
  },
  {
    uuid: '6',
    id: 6,
    name: 'Irene',
    email: 'Irene@test.nl'
  }
]

export default employees
