import dutchMessages from 'ra-language-dutch'
import {nl} from 'xxllnc-react-components'

export const i18nDutch = {
  ...dutchMessages,
  ...nl,
  contact: 'Contact',
  createdAt: 'Aanmaakdatum',
  employeeId: 'Toegewezen aan',
  id: 'Identificatie',
  phone: 'Telefoon',
  resultId: 'Resultaat',
  status: 'Status',
  subject: 'Onderwerp',
  summary: 'Samenvatting',
  createdByName: 'Aangemaakt door',
  assignedToName: 'Toegewezen aan'
}
