/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface CallbackRequest {
  contact: string
  subject: string
  summary?: string
  phone?: string
  employeeId?: number
  id: string

  /** @format uuid */
  uuid: string
  status: Statusses
  resultId?: number

  /** @format date-time */
  createdAt: string

  /** @format date-time */
  updatedAt?: string
  Uitleg?: string
  "Extra telefoonnummer"?: string
}

export interface CallbackRequestCreateRequest {
  customFields?: CustomFieldsWithValue[]
  contact: string
  subject: string
  summary?: string
  phone?: string
  employeeId?: number
}

export interface CallbackRequestUpdateRequest {
  customFields?: CustomFieldsWithValue[]
  contact: string
  subject: string
  summary?: string
  phone?: string
  employeeId?: number
  status: Statusses
  resultId?: number
}

export interface CustomField {
  /** Integer representing the sort order of the field. */
  sortOrder: number
  name: string
  description?: string
  required?: boolean
  archived?: boolean
  type: CustomFieldTypes
  options?: Options[]
  id: number

  /** @format uuid */
  uuid: string
}

export interface CustomFieldCreate {
  name: string
  description?: string
  required?: boolean
  archived?: boolean
  type: CustomFieldTypes
  options?: Options[]
}

export interface CustomFieldCreateUpdate {
  /** Integer representing the sort order of the field. */
  sortOrder?: number
  name?: string
  description?: string
  required?: boolean
  archived?: boolean
  type?: CustomFieldTypes
  options?: Options[]
}

/**
 * An enumeration.
 */
export enum CustomFieldTypes {
  String = "string",
  Text = "text",
  Select = "select",
}

export interface CustomFieldsWithValue {
  name: string
  description?: string
  required?: boolean
  archived?: boolean
  type: CustomFieldTypes
  options?: Options[]
  id: number
  value?: string
}

export interface Employee {
  name: string
  email: string
  id: number

  /** @format uuid */
  uuid: string
}

export interface EmployeeBase {
  name: string
  email: string
}

export interface EmployeeUpdate {
  name?: string
  email?: string
}

export interface HTTPValidationError {
  detail?: ValidationError[]
}

export interface Options {
  id: string
  name: string
}

export interface Result {
  result: string
  default?: boolean
  id: number

  /** @format uuid */
  uuid: string
}

export interface ResultBase {
  result: string
  default?: boolean
}

/**
 * An enumeration.
 */
export enum Statusses {
  Open = "Open",
  Afgehandeld = "Afgehandeld",
}

export interface ValidationError {
  loc: string[]
  msg: string
  type: string
}

export interface AppSchemasCallbackRequestSchemasCallbackRequestDetails {
  customFields?: CustomFieldsWithValue[]
  contact: string
  subject: string
  summary?: string
  phone?: string
  employeeId?: number
  id: string

  /** @format uuid */
  uuid: string
  status: Statusses
  resultId?: number

  /** @format date-time */
  createdAt: string

  /** @format date-time */
  updatedAt?: string
}

export interface PydanticMainCallbackRequestDetails {
  customFields?: CustomFieldsWithValue[]
  contact: string
  subject: string
  summary?: string
  phone?: string
  employeeId?: number
  id: string

  /** @format uuid */
  uuid: string
  status: Statusses
  resultId?: number

  /** @format date-time */
  createdAt: string

  /** @format date-time */
  updatedAt?: string
  Uitleg?: string
  "Extra telefoonnummer"?: string
}
