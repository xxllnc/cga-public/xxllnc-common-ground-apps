import { StringMap } from 'ra-core'

export const nl: StringMap = {
  label: {
    name: 'Naam',
    description: 'Omschrijving',
    required: 'Verplicht veld',
    type: 'Type',
    options: 'Opties',
    id: 'Identificatie',
    sortOrder: 'Sortering',
    archived: 'Gearchiveerd'
  },
  value: {
    string: 'Tekstveld klein',
    text: 'Tekstveld groot',
    select: 'Enkelvoudige keuze'
  },
  login: {
    login: 'Login',
    loginToContinue: 'Log in om verder te gaan',
    succesfulLogin: 'Je bent succesvol ingelogd!',
    getUserInfo: 'Een moment geduld, we halen nu je gebruikers informatie op.',
    email: 'Email',
    emailHint: 'Vul je emailadres in om door te gaan.',
    hint: 'Log in om verder te gaan',
    loginCompleted: 'Succesvol ingelogd!',
    fetchUserInfo: 'Een moment geduld, we halen je gebruikersinformatie op.',
    incognitoError: 'Authenticatie niet mogelijk in incognito mode',
    unknownOrganizationId: 'Het opgegeven organisatie-id is onbekend'
  },
  warnings: {
    notAuthorized: 'U heeft onvoldoende rechten om deze pagina te bekijken'
  },
  errors: {
    accessDenied: 'Authenticatie mislukt, u heeft geen toegang',
    invalidUserPassword: 'De gebruikersnaam of het wachtwoord is ongeldig',
    mfaInvalidCode: 'De code is verlopen of niet meer geldig',
    mfaRegistrationRequired: 'MFA registratie nodig om in te kunnen loggen',
    mfaRequired: 'MFA nodig om in te kunnen loggen',
    passwordLeaked: 'Wachtwoord is gelekt, verander het wachtwoord',
    PasswordHistoryError: 'Het opgegeven wachtwoord is al eerder gebruikt, kies een ander wachtwoord',
    PasswordStrengthError: 'Het wachtwoord is te zwak kies een sterker wachtwoord',
    tooManyAttempts: 'Het account is geblokkeerd omdat er tevaak is ingelogd met verkeerde gegevens',
    unauthorized: 'U bent niet bevoegd om deze applicatie te gebruiken, uw account is geblokkeerd',
    invalidState: 'Authenticatie mislukt, log opnieuw in',
    unknown: 'Authenticatie mislukt'
  },
  cancel: 'Anulleren',
  submit: 'Verzenden',
  createdByName: 'Aangemaakt door',
  createdAt: 'Aanmaakdatum',
  id: 'Identificatie',
  contact: 'Contactpersoon',
  subject: 'Onderwerp',
  status: 'Status'
}