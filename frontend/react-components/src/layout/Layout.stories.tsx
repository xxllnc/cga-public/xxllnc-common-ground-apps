import { FC } from 'react'
import { Story, Meta } from '@storybook/react'
import { Admin, Resource, ListGuesser, LayoutProps, useResourceDefinitions, useSidebarState } from 'react-admin'
import fakeRestDataProvider from 'ra-data-fakerest'
import DescriptionIcon from '@mui/icons-material/Description'
import { CustomLayout } from './Layout'
import { theme as xxTheme } from './themes'

export default {
  title: 'Layout'
} as Meta

const dataProvider = fakeRestDataProvider(
  {
    someData: [
      {
        id: '1',
        content: 'AAA',
        createdAt: '2021-05-12T00:01:00.000Z',
      },
      {
        id: '2',
        content: 'BBB',
        createdAt: '2021-05-13T00:01:00.000Z',
      }
    ],
    somethingElse: [
      {
        id: 'SE586',
        content: 'CCC',
        createdAt: '2021-06-15T00:01:00.000Z',
      },
      {
        id: 'SE486',
        content: 'DDD',
        createdAt: '2021-06-18T00:01:00.000Z',
      }
    ],
    inSubMenu: [
      {
        id: 'SM533',
        content: 'EEE',
        createdAt: '2022-03-06T00:01:00.000Z',
      },
      {
        id: 'SM59',
        content: 'FFF',
        createdAt: '2022-03-08T00:01:00.000Z',
      }
    ],
    inSubMenu2: [
      {
        id: 'SM534',
        content: 'GGG',
        createdAt: '2022-03-06T00:01:00.000Z',
      },
      {
        id: 'SM60',
        content: 'HHH',
        createdAt: '2022-03-08T00:01:00.000Z',
      }
    ],
  },
  true
)

const Layout: FC<LayoutProps> = (props) => {
  const [open] = useSidebarState() // Replaces useSelector for sidebarOpen
  const resourcesDefinitions = useResourceDefinitions() // Replaces useSelector for getResources
  const resources = Object.keys(resourcesDefinitions).map(name => resourcesDefinitions[String(name)])

  return (
    <CustomLayout {...props} appName="Storybook app" resources={resources} open={open} />
  )
}

export const LayoutStory: Story = () => (
  <Admin
    dataProvider={dataProvider}
    layout={Layout}
    theme={xxTheme}
  >
    <Resource
      name="someData"
      list={ListGuesser}
      icon={DescriptionIcon}
      options={{
        label: 'Some data',
        scopes: ['cga.admin', 'cga.user'],
        settings: false
      }}
    />
    <Resource
      name="somethingElse"
      list={ListGuesser}
      icon={DescriptionIcon}
      options={{
        label: 'Something else',
        scopes: ['cga.admin', 'cga.user'],
        settings: false
      }}
    />
    <Resource
      name="inSubMenu"
      list={ListGuesser}
      icon={DescriptionIcon}
      options={{
        label: 'Element in submenu',
        submenu: true,
        scopes: ['cga.admin', 'cga.user'],
        settings: false
      }}
    />
    <Resource
      name="inSubMenu2"
      list={ListGuesser}
      icon={DescriptionIcon}
      options={{
        label: 'Something else in submenu',
        submenu: true,
        scopes: ['cga.admin', 'cga.user'],
        settings: false
      }}
    />
  </Admin>
)