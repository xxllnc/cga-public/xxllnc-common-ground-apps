import { createTheme, styled } from '@mui/material/styles'
import { FC } from 'react'
import { Layout, LayoutProps, Sidebar as RaSidebar, useSidebarState } from 'react-admin'
import { CustomAppBar } from './AppBar'
import Menu from './Menu'

const material = createTheme()

interface StyleProps {
  sidebarIsOpen?: boolean,
}

const StyledSideBar = styled(RaSidebar)<{ props: StyleProps }>(
  ({ props }) => ({
    '& .RaSidebar-fixed': {
      paddingLeft: props.sidebarIsOpen ? material.spacing(2) : 12,
      paddingRight: props.sidebarIsOpen ? material.spacing(2) : 12,
    }
  })
)

const StyledLayout = styled(Layout)(
  ({ theme }) => ({
    '& .RaLayout-content': {
      'padding': theme.spacing(6),
      'paddingRight': theme.spacing(8),
      'paddingBottom': '60px',
      'position': 'relative',
      [theme.breakpoints.down('sm')]: {
        padding: theme.spacing(2),
      },
      '&::before': {
        left: theme.spacing(3),
        bottom: theme.spacing(3),
        height: '12px',
        width: '50px',
        content: '""',
        position: 'absolute',
        // eslint-disable-next-line max-len
        background: 'url("data:image/svg+xml;utf8,<?xml version="1.0" encoding="utf-8"?><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 175.4 42.2" style="enable-background:new 0 0 175.4 42.2;" xml:space="preserve"><path d="M78.3,40.7h7.2V0.4h-7.2V40.7z M94.8,40.7h7.2V0.4h-7.2V40.7z M64.5,9.4L53.4,20.8l-11-11.4l-4.8,4.9l11.1,11.4L37.6,37.1l4.8,4.9l11.1-11.4l11,11.4l4.8-4.9L58.2,25.7l11.1-11.4L64.5,9.4z M27.2,9.4L16.1,20.8L5.1,9.4l-4.8,4.9l11.1,11.4L0.3,37.1L5.1,42l11.1-11.4l11,11.4l4.8-4.9L20.9,25.7L32,14.3L27.2,9.4z M124.8,10.7c-8.2,0-14.3,5.2-14.3,14.1v15.9h7.2V25c0-4.8,3-7.3,7.1-7.3s7.1,2.6,7.1,7.3v15.8h7.2v-16C139.1,15.8,133,10.7,124.8,10.7z M160.4,34.8c-4.5,0-8.2-3.9-8.2-8.8c0-4.8,3.7-8.8,8.2-8.8c3.4,0,6.3,2.2,7.5,5.3h7.2c-1.3-7-7.1-11.8-14.6-11.8c-8.8,0-15.2,6.5-15.2,15.4c0,8.8,6.4,15.3,15.2,15.3c7.5,0,13.3-4.8,14.6-11.7h-7.2C166.6,32.6,163.7,34.8,160.4,34.8z"/></svg>") no-repeat center center',
        filter: 'invert(85%) sepia(0%) saturate(303%) hue-rotate(317deg) brightness(94%) contrast(110%)',
      }
    }
  })
)

export const CustomLayout: FC<LayoutProps> = (props) => {
  const [open] = useSidebarState()
  const Sidebar = sbProps => (<StyledSideBar props={{ sidebarIsOpen: open }} {...sbProps} size={200} />)

  return (
    <StyledLayout
      {...props}
      appBar={CustomAppBar}
      sidebar={Sidebar}
      menu={menuProps => <Menu {...menuProps}/>}
    />
  )
}

