import ExpandLess from '@mui/icons-material/ExpandLess'
import ExpandMore from '@mui/icons-material/ExpandMore'
import { Collapse, List, ListItemIcon, MenuItem, styled, Tooltip, Typography } from '@mui/material'
import { FC, PropsWithChildren, ReactElement } from 'react'
import { useTranslate } from 'react-admin'
import { xxllncColor } from './themes'

interface StyleProps {
  isOpen?: boolean,
  sidebarIsOpen?: boolean,
}

const StyledMenuItem = styled(MenuItem)<{ props: StyleProps }>(
  ({ theme, props }) => ({
    'backgroundColor': props.isOpen ? xxllncColor.dark80 : 'transparent',
    'borderRadius': '24px',
    'display': 'flex',
    'height': '48px !important',
    'color': props.isOpen ? xxllncColor.light100 : xxllncColor.dark60,
    'paddingLeft': props.sidebarIsOpen ? theme.spacing(2) : theme.spacing(1.5),
    'paddingRight': props.sidebarIsOpen ? theme.spacing(2) : theme.spacing(1.5),
    'marginTop': '0px !important',
    'marginBottom': props.isOpen ? theme.spacing(1.5) : 0,
    '&:hover': {
      backgroundColor: props.isOpen ? xxllncColor.dark100 : 'rgba(0, 0, 0, 0.04)',
    }
  })
)

const StyledListItemIcon = styled(ListItemIcon)<{ props: StyleProps }>(
  ({ theme, props }) => ({
    'fill': props.isOpen ? xxllncColor.light100 : xxllncColor.dark60,
    'minWidth': theme.spacing(5),
    '& .MuiSvgIcon-root': {
      fill: props.isOpen ? xxllncColor.light100 : xxllncColor.dark60,
    }
  })
)


const SubmenuContainer = styled('div')<{ props: StyleProps }>(
  ({ theme, props }) => ({
    'backgroundColor': props.isOpen ? xxllncColor.dark10 : 'transparent',
    'borderRadius': '24px',
    'overflow': 'hidden',
    '& .MuiList-root': {
      'paddingLeft': theme.spacing(.5),
      'paddingRight': theme.spacing(.5),
      'paddingBottom': theme.spacing(.5),
      '& .MuiListItem-button': {
        padding: props.sidebarIsOpen ? theme.spacing(2).toString() + 'px !important' : '9px !important',
      },
    },
    '& .MuiMenuItem-root': {
      height: 40,
      marginBottom: 0,
      marginTop: theme.spacing(1.5),
    }
  })
)

const DropdownContainer = styled('div')({ display: 'flex', marginLeft: 'auto' })

interface Props {
  dense: boolean
  handleToggle: () => void
  icon: ReactElement
  isOpen: boolean
  name: string
  sidebarIsOpen: boolean
}


interface ConditionalWrapperProps {
  condition: boolean
  wrapper: (children: JSX.Element) => JSX.Element
  children: JSX.Element
}
const ConditionalWrapper: FC<ConditionalWrapperProps> = ({ condition, wrapper, children }) =>
  condition ? wrapper(children) : children

interface HeaderProps { isOpen: boolean, header: JSX.Element, title: string }
const Header: FC<HeaderProps> = ({ isOpen, header, title }) => {
  if (isOpen)
    return header

  return (
    <Tooltip title={title} placement="right">
      {header}
    </Tooltip>
  )
}


const SubMenu: FC<PropsWithChildren<Props>> = ({
  handleToggle,
  sidebarIsOpen,
  isOpen,
  name,
  icon,
  children,
  dense,
}) => {
  const props = { isOpen, sidebarIsOpen }
  const translate = useTranslate()

  if (!children)
    return null

  return (
    <SubmenuContainer props={props}>
      <ConditionalWrapper
        condition={sidebarIsOpen || isOpen}
        wrapper={childrenToWrap => <Tooltip title={translate(name)} placement="right">{childrenToWrap}</Tooltip>}
      >
        <StyledMenuItem props={props} dense={dense} onClick={handleToggle}>
          <StyledListItemIcon props={props}>
            {icon}
          </StyledListItemIcon>
          <Typography
            variant="inherit"
            sx={{ color: isOpen ? xxllncColor.light100 : xxllncColor.dark60 }}
          >
            {translate(name)}
          </Typography>
          <DropdownContainer>
            {isOpen ? <ExpandLess /> : <ExpandMore />}
          </DropdownContainer>
        </StyledMenuItem>
      </ConditionalWrapper>
      <Collapse in={isOpen} timeout="auto" unmountOnExit>
        <List
          dense={dense}
          component="div"
          disablePadding
          sx={(theme) => ({
            'transition': 'padding-left 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms',
            '& a': sidebarIsOpen ? ({
              paddingLeft: theme.spacing(4),
              transition: 'padding-left 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms',
            }) : ({
              paddingLeft: theme.spacing(1.5),
              paddingRight: theme.spacing(1.5),
              transition: 'padding-left 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms',
            }),
          })}
        >
          {children}
        </List>
      </Collapse>
    </SubmenuContainer>
  )
}

export default SubMenu
