import SettingsOutlinedIcon from '@mui/icons-material/SettingsOutlined'
import { Box, styled, Typography, useMediaQuery, useTheme } from '@mui/material'
import { createElement, FC, ReactNode, useState } from 'react'
import { MenuItemLink, MenuProps, useResourceDefinitions, useSidebarState, useTranslate } from 'react-admin'
import { getEnvVariable } from '../utils/getEnvVariable'
import SubMenu from './SubMenu'
import { xxllncColor } from './themes'

const getAppName = () => getEnvVariable('REACT_APP_NAME')

const StyledSubMenuItemLink = styled(MenuItemLink)(
  ({ theme }) => ({
    'color': theme.palette.text.secondary,
    '& .RaMenuItemLink-active': {
      'backgroundColor': xxllncColor.light100,
      'color': xxllncColor.dark60,
      '& svg': {
        fill: xxllncColor.dark80,
      }
    },
    '& .RaMenuItemLink-icon': {
      minWidth: theme.spacing(5)
    },
  })
)

const StyledMenuItemLink = styled(MenuItemLink)({
  '& .RaMenuItemLink-active': {
    'backgroundColor': xxllncColor.dark80,
    'color': 'red', // xxllncColor.light100,
    '& svg': {
      fill: xxllncColor.light100,
    },
    '&:hover': {
      color: 'red', // xxllncColor.light100,
      background: xxllncColor.dark100,
    }
  }
})

export interface Resources {
  name: string,
  icon?: string,
  options?: {
    label: string,
    submenu?: string,
    settings?: boolean
  }
}

interface ResourceWithSubMenu extends Resources {
  resources?: string[]
  submenu?: Resources[]
}

export interface ResourceProps {
  resources: Resources[]
}

type MenuName = 'menuSettings'

interface Props extends MenuProps {
  onMenuClick?: () => void
  logout?: ReactNode
}

const Menu: FC<Props> = ({ onMenuClick, logout, dense = false }) => {
  const [open] = useSidebarState()
  const resourcesDefinitions = useResourceDefinitions()
  const resources = Object.keys(resourcesDefinitions).map(name => resourcesDefinitions[`${name}`])
  const translate = useTranslate()
  const theme = useTheme()
  const isXSmall = useMediaQuery(theme.breakpoints.down('sm'))

  const [state, setState] = useState({
    menuSettings: false,
  })

  const handleToggle = (menu: MenuName) => {
    setState(stateItem => ({ ...stateItem, [menu]: !stateItem[`${menu}`] }))
  }

  const getMenuItems = (object: Resources[]) => {
    const values: ResourceWithSubMenu[] = object.filter(resource => resource.icon && !resource.options?.submenu)
    const submenu = object.filter(resource => resource.icon && resource.options?.submenu)

    if (submenu.length > 0)
      values.push({
        name: 'submenu.settings',
        icon: 'DescriptionIcon',
        resources: submenu.map(prop => prop.name),
        submenu: [
          ...submenu
        ]
      })

    return values
  }

  return (
    <Box mt={1} style={{ padding: isXSmall ? 10 : 0 }}>
      {isXSmall &&
        <Typography style={{ marginLeft: 5 }} variant="h6" gutterBottom>
          {translate(getAppName())}
        </Typography>
      }
      {getMenuItems(resources).filter(resource => !resource.options?.settings).map(resource => {
        if (resource.submenu) {
          return (
            <SubMenu
              key={resource.name}
              handleToggle={() => handleToggle('menuSettings')}
              isOpen={state.menuSettings}
              sidebarIsOpen={open}
              name={resource.name}
              icon={<SettingsOutlinedIcon />}
              dense={dense}
            >
              {resource && resource.submenu && resource.submenu.map(item => (
                <StyledSubMenuItemLink
                  dense={dense}
                  key={item.name}
                  leftIcon={createElement(item?.icon ?? '')}
                  onClick={onMenuClick}
                  primaryText={translate(item.options?.label || item.name)}
                  sidebarIsOpen={open}
                  to={`/${item.name}`}
                />
              ))}
            </SubMenu>
          )
        }

        return (
          <StyledMenuItemLink
            sx={(raTheme) => ({ marginBottom: raTheme.spacing(1.5) })}
            dense={dense}
            key={resource.name}
            leftIcon={createElement(resource?.icon ?? '')}
            onClick={onMenuClick}
            primaryText={translate(resource.options?.label || resource.name)}
            sidebarIsOpen={open}
            to={`/${resource.name}`}
          />
        )
      })}
      {isXSmall && logout}
    </Box>
  )
}

export default Menu
