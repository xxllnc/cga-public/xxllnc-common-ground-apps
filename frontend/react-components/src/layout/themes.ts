import { Components, createTheme, PaletteOptions, ThemeOptions } from '@mui/material'
import { defaultTheme } from 'react-admin'

export const material = createTheme()

const opacity = {
  o80: '80%',
  o60: '60%',
  o40: '40%',
  o20: '20%',
  o10: '10%',
}

export const xxllncColor = {
  base: 'rgba(239,239,237,1)',
  dark100: 'rgba(29,29,29,1)',
  dark80: 'rgba(71,71,70,1)',
  dark60: 'rgba(113,113,112,1)',
  dark40: 'rgba(155,155,154,1)',
  dark20: 'rgba(197,197,196,1)',
  dark10: 'rgba(219,219,217,1)',
  opDark80: 'rgba(29,29,29,' + opacity.o80 + ')',
  opDark60: 'rgba(29,29,29,' + opacity.o60 + ')',
  opDark40: 'rgba(29,29,29,' + opacity.o40 + ')',
  opDark20: 'rgba(29,29,29,' + opacity.o20 + ')',
  opDark10: 'rgba(29,29,29,' + opacity.o10 + ')',
  light100: 'rgba(255,255,255,1)',
  light80: 'rgba(252,252,251,1)',
  light60: 'rgba(249,249,248,1)',
  light40: 'rgba(245,245,244,1)',
  light20: 'rgba(242,242,241,1)',
  opLight80: 'rgba(255,255,255,' + opacity.o80 + ')',
  opLight60: 'rgba(255,255,255,' + opacity.o60 + ')',
  opLight40: 'rgba(255,255,255,' + opacity.o40 + ')',
  opLight20: 'rgba(255,255,255,' + opacity.o20 + ')',
  // Blue (xxllnc Bezwaren)
  blue100: 'rgba(30,101,186,1)',
  blue80: 'rgba(72,129,196,1)',
  blue60: 'rgba(114,157,207,1)',
  blue40: 'rgba(155,183,216,1)',
  blue20: 'rgba(197,211,227,1)',
  opBlue80: 'rgba(30,101,186,' + opacity.o80 + ')',
  opBlue60: 'rgba(30,101,186,' + opacity.o60 + ')',
  opBlue40: 'rgba(30,101,186,' + opacity.o40 + ')',
  opBlue20: 'rgba(30,101,186,' + opacity.o20 + ')',
  opBlue10: 'rgba(30,101,186,' + opacity.o10 + ')',
  // Lightblue (xxllnc Bezwaren)
  lightblue100: 'rgba(105,162,255,1)',
  lightblue80: 'rgba(132,178,251,1)',
  lightblue60: 'rgba(159,193,248,1)',
  lightblue40: 'rgba(185,208,244,1)',
  lightblue20: 'rgba(212,223,241,1)',
  opLightblue80: 'rgba(105,162,255,' + opacity.o80 + ')',
  opLightblue60: 'rgba(105,162,255,' + opacity.o60 + ')',
  opLightblue40: 'rgba(105,162,255,' + opacity.o40 + ')',
  opLightblue20: 'rgba(105,162,255,' + opacity.o20 + ')',
  // Green (xxllnc Zaken / Success)
  green100: 'rgba(0,115,77,1)',
  green80: 'rgba(48,140,109,1)',
  green60: 'rgba(96,165,141,1)',
  green40: 'rgba(143,189,173,1)',
  green20: 'rgba(191,214,205,1)',
  opGreen80: 'rgba(0,115,77,' + opacity.o80 + ')',
  opGreen60: 'rgba(0,115,77,' + opacity.o60 + ')',
  opGreen40: 'rgba(0,115,77,' + opacity.o40 + ')',
  opGreen20: 'rgba(0,115,77,' + opacity.o20 + ')',
  opGreen10: 'rgba(0,115,77,' + opacity.o10 + ')',
  // Red (xxllnc Warning)
  red100: 'rgba(194,66,66,1)',
  red80: 'rgba(204,101,100,1)',
  red60: 'rgba(213,136,135,1)',
  red40: 'rgba(221,170,169,1)',
  red20: 'rgba(230,204,203,1)',
  opRed80: 'rgba(194,66,66,' + opacity.o80 + ')',
  opRed60: 'rgba(194,66,66,' + opacity.o60 + ')',
  opRed40: 'rgba(194,66,66,' + opacity.o40 + ')',
  opRed20: 'rgba(194,66,66,' + opacity.o20 + ')',
  opRed10: 'rgba(194,66,66,' + opacity.o10 + ')',
  // Coral
  coral100: 'rgba(226,104,104,1)',
  coral80: 'rgba(229,131,130,1)',
  coral60: 'rgba(232,157,157,1)',
  coral40: 'rgba(234,184,183,1)',
  coral20: 'rgba(236,212,211,1)',
  opCoral80: 'rgba(226,104,104,' + opacity.o80 + ')',
  opCoral60: 'rgba(226,104,104,' + opacity.o60 + ')',
  opCoral40: 'rgba(226,104,104,' + opacity.o40 + ')',
  opCoral20: 'rgba(226,104,104,' + opacity.o20 + ')',
  // Orange (xxllnc Koppelen / Error)
  orange100: 'rgba(232,110,68,1)',
  orange80: 'rgba(234,136,101,1)',
  orange60: 'rgba(235,162,136,1)',
  orange40: 'rgba(236,187,169,1)',
  orange20: 'rgba(237,213,204,1)',
  opOrange80: 'rgba(232,110,68,' + opacity.o80 + ')',
  opOrange60: 'rgba(232,110,68,' + opacity.o60 + ')',
  opOrange40: 'rgba(232,110,68,' + opacity.o40 + ')',
  opOrange20: 'rgba(232,110,68,' + opacity.o20 + ')',
  // Yellow (xxllnc Gegevens)
  yellow100: 'rgba(255,176,62,1)',
  yellow80: 'rgba(252,189,97,1)',
  yellow60: 'rgba(249,202,132,1)',
  yellow40: 'rgba(245,213,167,1)',
  yellow20: 'rgba(242,226,202,1)',
  opYellow80: 'rgba(255,176,62,' + opacity.o80 + ')',
  opYellow60: 'rgba(255,176,62,' + opacity.o60 + ')',
  opYellow40: 'rgba(255,176,62,' + opacity.o40 + ')',
  opYellow20: 'rgba(255,176,62,' + opacity.o20 + ')',
}

const color = {
  white: xxllncColor.light100,
  black: '#2A2E33',
  alto: '#d6d6d6',
  gallery: '#efefef',
  silverChalice: '#afafaf',
  mineShaft: '#393939',
  codGray: '#1a1a1a',
  greenPea: '#1e6844',
  pomegranate: '#e74e18',
  gamboge: '#e4970c',
  veniceBlue: '#0e5186',
  curiousBlue: '#2597e1',
  viking: '#79b6dd',
  nileBlue: '#1a3455',
  greenSmoke: '#a2b56f',
  beautyBush: '#efcbcb',
  tallPoppy: '#bc2f2a',
  primary: {
    main: xxllncColor.dark100,
  },
}

export const border = {
  radius: {
    sm: 4,
    md: 24,
    lg: 50,
  },
  thin: '1px',
  normal: '2px',
  thick: '3px',
  dark: xxllncColor.dark80,
  light: xxllncColor.dark20,
  button: {
    radius: 50,
  },
  input: {
    radius: 12,
  },
  dropdown: {
    radius: 12,
  },
  icon: {
    radius: {
      sm: 8,
      md: 16,
    }
  }
}

const typography = {
  headline: ['Inter', 'Helvetica', 'Arial', 'sans-serif'].join(','),
  body: ['Inter', 'Helvetica', 'Arial', 'sans-serif'].join(','),
  subtitle: ['Inter', 'Helvetica', 'Arial', 'sans-serif'].join(','),
  button: ['Inter', 'Helvetica', 'Arial', 'sans-serif'].join(','),
  overline: ['Inter', 'Helvetica', 'Arial', 'sans-serif'].join(','),
  caption: ['Inter', 'Helvetica', 'Arial', 'sans-serif'].join(','),
}

const button = {
  delete: {
    default: {
      background: 'transparent',
      padding: material.spacing(.75, 2),
      color: xxllncColor.dark100,
      fill: xxllncColor.red100,
    },
    hover: {
      background: 'rgba(194, 67, 66, 0.1)',
    }
  }
}

const raComponentOverrides = {
  RaLayout: {
    styleOverrides: {
      root: {
        'fontFamily': typography.headline,
        'backgroundColor': xxllncColor.base,
        '& .RaLayout-content': {
          backgroundColor: xxllncColor.base,
        },
        '& .RaLayout-appFrame': {
          [material.breakpoints.up('xs')]: {
            marginTop: material.spacing(8),
          },
          [material.breakpoints.down('xs')]: {
            marginTop: material.spacing(7),
          },
        }
      }
    }
  },
  RaSidebarToggleButton: {
    styleOverrides: {
      root: {
        '& RaSidebarToggleButton-menuButtonIconOpen': {
          fill: xxllncColor.dark80,
        },
        '& RaSidebarToggleButton-menuButtonIconClosed': {
          fill: xxllncColor.dark80,
        }
      }
    }
  },
  RaList: {
    styleOverrides: {
      root: {
        '& .RaList-content': {
          borderBottomRightRadius: 'unset',
          borderBottomLeftRadius: 'unset',
          borderTopRightRadius: 'unset',
          borderTopLeftRadius: 'unset',
          backgroundColor: xxllncColor.base,
          marginBottom: 0,
          paddingBottom: 0,
          [material.breakpoints.down('md')]: {
            backgroundColor: xxllncColor.light100
          },
        },
        '& RaList-bulkActionsDisplayed': {
          marginTop: 0,
          backgroundColor: 'unset',
        },
        '& .RaList-main': {
          borderBottomRightRadius: 24,
          borderBottomLeftRadius: 24,
          backgroundColor: 'unset',
        }
      },
      main: {
        borderBottomRightRadius: 24,
        borderBottomLeftRadius: 24,
        backgroundColor: xxllncColor.light100,
      }
    }
  },
  RaSimpleList: {
    styleOverrides: {
      root: {
        backgroundColor: xxllncColor.light100,
      },
    }
  },
  RaShow: {
    styleOverrides: {
      root: {
        [material.breakpoints.up('xl')]: {
          maxWidth: '66.6%',
          marginLeft: 'auto',
          marginRight: 'auto',
          width: '100%',
        },
        '& .MuiToolbar-root': {
          backgroundColor: xxllncColor.light100,
          borderTopLeftRadius: border.radius.md,
          borderTopRightRadius: border.radius.md,
          fontFamily: typography.headline,
          minHeight: 'auto',
          padding: material.spacing(3),
          display: 'flex',
          alignItems: 'center',
        },
        '& .RaShow-noActions': {
          marginTop: 0,
        },
        '& .RaShow-main': {
          '& .show-tab': {
            fontSize: '14px',
            textTransform: 'initial',
            minWidth: 'auto',
          },
        },
        '& .RaShow-card': {
          borderTopLeftRadius: 0,
          borderTopRightRadius: 0,
        },
      }
    }
  },
  RaTabbedShowLayout: {
    styleOverrides: {
      root: {
        '& RaTabbedShowLayout-content': {
          paddingLeft: material.spacing(6),
          paddingRight: material.spacing(6),
          paddingTop: material.spacing(3),
          paddingBottom: material.spacing(3),
        },
      }
    }
  },
  RaSidebar: {
    styleOverrides: {
      root: {
        'height': 'auto',
        '& .RaSidebar-drawerPaper': {
          border: 'none',
        },
        '& .RaSidebar-fixed': {
          'boxSizing': 'border-box',
          'height': 'calc(100vh - 3em)',
          'overflowX': 'hidden',
          'position': 'fixed',
          'width': 'inherit',
          '& .MuiBox-root': {
            marginTop: 0,
          },
          'borderRight': border.thin + ' solid ' + border.light,
          'paddingTop': 12
        },
      },
    }
  },
  RaMenuItemLink: {
    styleOverrides: {
      root: (props: { sidebarIsOpen?: boolean }) => ({
        'borderRadius': border.radius.md,
        'padding': props.sidebarIsOpen ? material.spacing(2) : 12,
        'overflow': props.sidebarIsOpen ? 'unset' : 'hidden',
        'marginBottom': material.spacing(1.5),
        'height': 48,
        '&:hover': {
          color: xxllncColor.light100,
        },
        '&.RaMenuItemLink-active': {
          'backgroundColor': xxllncColor.dark80,
          'color': xxllncColor.light100,
          '& svg': {
            fill: xxllncColor.light100,
          },
          '&:hover': {
            color: xxllncColor.light100,
            background: xxllncColor.dark100,
          }
        },
      })
    },
  },
  RaToolbar: {
    styleOverrides: {
      root: {
        'backgroundColor': xxllncColor.light100,
        'paddingLeft': material.spacing(4),
        'paddingRight': material.spacing(4),
        'paddingBottom': material.spacing(2),
        'display': 'flex',
        'justifyContent': 'flex-end',
        '&.RaToolbar-desktopToolbar': {
          marginTop: 0,
        },
        '&.RaToolbar-defaultToolbar': {
          justifyContent: 'flex-end',
        }
      },
    }
  },
  RaTopToolbar: {
    styleOverrides: {
      root: {
        boxSizing: 'border-box',
        width: '100%',
        display: 'flex',
        justifyContent: 'flex-end',
        flexWrap: 'wrap',
        paddingTop: 0,
        paddingBottom: material.spacing(3.5),
        [material.breakpoints.down('xs')]: {
          backgroundColor: 'unset',
        },
      },
    }
  },
  RaListToolbar: {
    styleOverrides: {
      root: {
        backgroundColor: xxllncColor.base,
        border: 0,
        [material.breakpoints.down('md')]: {
          backgroundColor: xxllncColor.light100,
          borderTopLeftRadius: border.radius.md,
          borderTopRightRadius: border.radius.md,
        },
      },
    }
  },
  RaBulkActionsToolbar: {
    styleOverrides: {
      root: {
        '& .RaBulkActionsToolbar-toolbar': {
          left: 20,
          right: 130
        }
      }
    }
  },
  RaCreate: {
    styleOverrides: {
      root: {
        [material.breakpoints.up('xl')]: {
          maxWidth: '66.6%',
          marginLeft: 'auto',
          marginRight: 'auto',
          width: '100%',
        },
        '& .MuiToolbar-root': {
          backgroundColor: xxllncColor.light100,
          borderTopLeftRadius: border.radius.md,
          borderTopRightRadius: border.radius.md,
          fontFamily: typography.headline,
          minHeight: 'auto',
          padding: material.spacing(3),
          display: 'flex',
          alignItems: 'center',
        },
      },
      main: {
        '& .MuiGrid-item': {
          paddingLeft: material.spacing(1.5),
          paddingRight: material.spacing(1.5),
          boxSizing: 'border-box',
        },
        '& .ra-input': {
          paddingLeft: material.spacing(1.5),
          paddingRight: material.spacing(1.5),
          boxSizing: 'border-box',
        },
        '& .ra-input-undefined': {
          paddingLeft: 0,
          paddingRight: 0,
        },
      },
      card: {
        'borderTopLeftRadius': 0,
        'borderTopRightRadius': 0,
        '& .MuiCardContent-root': {
          'paddingTop': 0,
          'paddingLeft': material.spacing(2.5),
          'paddingRight': material.spacing(2.5),
          '&:first-child': {
            paddingTop: 0,
            paddingLeft: material.spacing(2.5),
            paddingRight: material.spacing(2.5),
          },
        },
      },
    }
  },
  RaEdit: {
    styleOverrides: {
      root: {
        [material.breakpoints.up('xl')]: {
          maxWidth: '66.6%',
          marginLeft: 'auto',
          marginRight: 'auto',
          width: '100%',
        },
        '& .MuiToolbar-root': {
          backgroundColor: xxllncColor.light100,
          borderTopLeftRadius: border.radius.md,
          borderTopRightRadius: border.radius.md,
          fontFamily: typography.headline,
          minHeight: 'auto',
          padding: material.spacing(3),
          display: 'flex',
          alignItems: 'center',
        },
        '& .RaEdit-main': {
          '& .MuiGrid-item': {
            paddingLeft: material.spacing(1.5),
            paddingRight: material.spacing(1.5),
            boxSizing: 'border-box',
          },
          '& .ra-input': {
            paddingLeft: material.spacing(1.5),
            paddingRight: material.spacing(1.5),
            boxSizing: 'border-box',
            margin: 0
          },
          '& .ra-input-undefined': {
            paddingLeft: 0,
            paddingRight: 0,
          },
        },
        '& .RaEdit-card': {
          'borderTopLeftRadius': 0,
          'borderTopRightRadius': 0,
          '& .MuiCardContent-root': {
            paddingTop: 0,
            paddingLeft: material.spacing(2.5),
            paddingRight: material.spacing(2.5),
          },
        },
      }
    }
  },
  RaFormInput: {
    styleOverrides: {
      input: {
        width: '100%',
      },
    }
  },
  RaFileInput: {
    styleOverrides: {
      dropZone: {
        backgroundColor: xxllncColor.light20,
        border: border.normal + ' dashed ' + border.light,
        borderRadius: border.radius.md,
        color: xxllncColor.dark60,
        padding: material.spacing(4),
      },
      preview: {
        padding: '12px',
        paddingLeft: 0,
        width: '48px',
        maxHeight: '48px',
      },
      removeButton: {
        display: 'flex',
        background: xxllncColor.base,
        borderRadius: '24px',
        boxSizing: 'border-box',
        margin: 0,
      },
    }
  },
  RaFileInputPreview: {
    styleOverrides: {
      removeIcon: {
        color: xxllncColor.dark80
      },
    }
  },
  RaLabeled: {
    styleOverrides: {
      root: {
        '& .RaLabeled-label': {
          textTransform: 'uppercase',
          fontSize: '14px',
          lineHeight: '14px',
        },
        '& > span': {
          color: 'currentColor',
          width: '100%',
          border: 0,
          margin: 0,
          display: 'block',
          padding: '8px 0 4px',
          background: 'none',
          boxSizing: 'content-box',
          verticalAlign: 'middle',
          fontSize: 16
        },
        'value': {
          '& .previews': {
            display: 'flex'
          },
        }
      }
    }
  },
  RaButton: {
    styleOverrides: {
      root: {
        textTransform: 'none',
      }
    },
  },
  RaSaveButton: {
    styleOverrides: {
      button: {
        marginLeft: material.spacing(2)
      }
    },
  },
  RaBulkDeleteWithConfirmButton: {
    styleOverrides: {
      deleteButton: {
        'backgroundColor': button.delete.default.background,
        'padding': button.delete.default.padding,
        'color': button.delete.default.color,
        '& .MuiSvgIcon-root': {
          fill: button.delete.default.fill,
        },
        '&:hover': {
          backgroundColor: button.delete.hover.background,
        }
      },
    }
  },
  RaDatagrid: {
    styleOverrides: {
      root: {
        'borderTopRightRadius': 24,
        'borderTopLeftRadius': 24,
        'backgroundColor': xxllncColor.light100,
        'paddingBottom': 16,
        '& .RaDatagrid-headerCell': {
          'backgroundColor': 'transparent',
          'borderBottom': border.normal + ' solid ' + xxllncColor.dark60,
          'color': xxllncColor.dark100,
          'fontSize': 12,
          'fontWeight': 600,
          'padding': material.spacing(1.5),
          '&.MuiTableCell-paddingCheckbox': {
            borderBottomColor: xxllncColor.dark60,
          }
        },
        '& .RaDatagrid-row': {
          '&:hover': {
            '& .column-changeType': {
              '& .MuiSvgIcon-root': {
                backgroundColor: xxllncColor.dark80,
                color: xxllncColor.light100,
              },
            },
          },
        },
        '& .RaDatagrid-rowCell': {
          'borderBottom': border.thin + ' solid ' + border.light,
          'color': xxllncColor.dark80,
          'padding': material.spacing(1.5),
          '&:last-child': {
            padding: material.spacing(1.5),
          }
        }
      }
    }
  },
  RaImageField: {
    styleOverrides: {
      image: {
        margin: 0,
        maxWidth: '100%',
      }
    },
  },
  RaCardContentInner: {
    styleOverrides: {
      root: {
        'padding': 0,
        '&:first-child': {
          padding: 0,
        },
        '&:last-child': {
          [material.breakpoints.down('sm')]: {
            paddingBottom: material.spacing(2),
          },
          padding: 0,
        },
      },
    }
  },
  RaEmpty: {
    styleOverrides: {
      icon: {
        backgroundColor: xxllncColor.light20,
        borderRadius: border.button.radius,
        boxSizing: 'border-box',
        height: material.spacing(5),
        marginBottom: material.spacing(1),
        padding: '6px',
        width: material.spacing(5),
      },
      message: {
        'padding': material.spacing(6),
        'boxSizing': 'border-box',
        'margin': 0,
        '& .MuiTypography-root': {
          fontSize: '14px',
          lineHeight: '20px',
        },
      },
      toolbar: {
        marginTop: 0,
        paddingLeft: material.spacing(6),
        paddingRight: material.spacing(6),
        paddingBottom: material.spacing(6),
      },
    }
  },
  RaFilterForm: {
    styleOverrides: {
      form: {
        [material.breakpoints.down('xs')]: {
          marginTop: 0,
        },
      },
    }
  },
  RaFilterListItem: {
    styleOverrides: {
      root: {
        'display': 'flex',
        'borderRadius': border.button.radius,
        'marginRight': 'auto',
        'marginBottom': material.spacing(.5),
        '& .RaFilterListItem-listItemButton': {
          display: 'flex',
          borderRadius: border.button.radius,
          marginRight: 'auto',
        },
        '&.Mui-selected': {
          background: xxllncColor.dark80,
          color: xxllncColor.light100,
          paddingRight: 6,
        },
        '& svg': {
          fill: xxllncColor.light100,
        },
      }
    }
  },
}

const muiComponentOverrides: Components = {
  // MuiList: {
  //   styleOverrides: {
  //     root: {
  //       'display': 'flex',
  //       'flexFlow': 'column',
  //       '& > div': {
  //         display: 'flex',
  //         borderRadius: border.button.radius,
  //         marginRight: 'auto',
  //         marginBottom: material.spacing(.5),
  //       },
  //       '& > li': {
  //         display: 'flex',
  //         borderRadius: border.button.radius,
  //         marginRight: 'auto',
  //         marginBottom: material.spacing(.5),
  //       },
  //       '& .MuiListItem-button': {
  //         paddingLeft: material.spacing(2).toString() + 'px !important',
  //         borderRadius: border.button.radius,
  //         width: 'auto',
  //       },
  //     },
  //   }
  // },
  // MuiListItem: {
  //   styleOverrides: {
  //     container: {
  //       '& .MuiListItemSecondaryAction-root': {
  //         right: 0,
  //       },
  //       '&:hover': {
  //         '& .Mui-selected': {
  //           background: xxllncColor.opDark10,
  //           color: xxllncColor.dark80,
  //         },
  //         '& svg': {
  //           fill: xxllncColor.dark80,
  //         },
  //       },
  //       '& .Mui-selected': {
  //         background: xxllncColor.dark80,
  //         color: xxllncColor.light100,
  //         paddingRight: '36px',
  //       },
  //       '& svg': {
  //         fill: xxllncColor.light100,
  //       },
  //     },
  //   }
  // },
  MuiTextField: {
    defaultProps: { variant: 'filled' },
    styleOverrides: {
      root: {
        width: '100%',
      },
    }
  },
  MuiPaper: {
    styleOverrides: {
      root: {
        border: 0,
        backgroundClip: 'padding-box',
      },
      elevation1: {
        boxShadow: 'none',
      },
    }
  },
  MuiTabs: {
    styleOverrides: {
      root: {
        paddingLeft: material.spacing(6),
        paddingRight: material.spacing(6),
      },
    }
  },
  MuiButton: {
    styleOverrides: {
      root: {
        color: xxllncColor.dark60,
        borderRadius: border.button.radius,
        minWidth: 'auto',
      },
      startIcon: {
        '&.MuiButton-iconSizeSmall': {
          marginRight: 'unset',
          marginLeft: 'unset'
        }
      },
      contained: {
        backgroundColor: color.white,
        color: color.mineShaft,
        boxShadow: 'none',
      },
      containedPrimary: {
        color: xxllncColor.light100,
        backgroundColor: xxllncColor.dark100
      },
      outlinedPrimary: {
        'border': border.normal + ' solid ' + border.dark,
        '&:hover': {
          border: border.normal + ' solid ' + border.dark,
        },
      },
      textPrimary: {
        color: xxllncColor.dark100,
        textTransform: 'none',
      }
    }
  },
  MuiButtonBase: {
    styleOverrides: {
      root: {
        '&:hover:active::after': {
          content: '""',
          display: 'block',
          width: '100%',
          height: '100%',
          position: 'absolute',
          top: 0,
          right: 0,
          backgroundColor: 'currentColor',
          opacity: 0.3,
          borderRadius: 'inherit',
        }
      }
    }
  },
  MuiCard: {
    styleOverrides: {
      root: {
        marginBottom: material.spacing(2),
      },
    }
  },
  MuiAppBar: {
    styleOverrides: {
      colorSecondary: {
        color: '#808080',
        backgroundColor: xxllncColor.base,
        border: 0,
        borderBottom: border.normal + ' solid ' + border.light,
      },
    }
  },
  MuiLinearProgress: {
    styleOverrides: {
      colorPrimary: {
        backgroundColor: '#f5f5f5',
      },
      barColorPrimary: {
        backgroundColor: color.alto,
      },
    }
  },
  MuiTableCell: {
    styleOverrides: {
      sizeSmall: {
        '&.MuiTableCell-paddingCheckbox': {
          padding: material.spacing(1.5),
          borderBottomColor: border.light,
        },
      },
    }
  },
  MuiFormLabel: {
    styleOverrides: {
      root: {
        '&#q-label': {
          'transform': 'none',
          'height': '40px',
          'display': 'flex',
          'alignItems': 'center',
          'paddingLeft': material.spacing(2),
          '&[data-shrink=true]': {
            display: 'none',
          },
        },
      },
    }
  },
  MuiInputLabel: {
    styleOverrides: {
      root: {
        paddingLeft: material.spacing(1.5)
      }
    }
  },
  MuiTablePagination: {
    styleOverrides: {
      root: {
        backgroundColor: xxllncColor.light100,
        borderBottomRightRadius: 24,
        borderBottomLeftRadius: 24
      }
    }
  },
  MuiFilledInput: {
    styleOverrides: {
      root: {
        'backgroundColor': xxllncColor.light100,
        'borderTopLeftRadius': border.input.radius,
        'borderTopRightRadius': border.input.radius,
        'borderBottomLeftRadius': border.input.radius,
        'borderBottomRightRadius': border.input.radius,
        'border': border.thin + ' solid ' + border.dark,
        '&:before': {
          display: 'none',
        },
        '& svg': {
          fill: xxllncColor.dark80,
        },
      },
      input: {
        borderRadius: border.input.radius,
      },
      underline: {
        '&:after': {
          display: 'none',
        },
      },
    }
  },
  MuiSnackbarContent: {
    styleOverrides: {
      root: {
        border: 'none',
      },
    }
  },
  MuiDivider: {
    styleOverrides: {
      root: {
        display: 'none',
      },
    }
  },
  MuiPopover: {
    styleOverrides: {
      paper: {
        'background': xxllncColor.light80,
        'border': border.thin + ' solid ' + xxllncColor.dark10,
        'borderRadius': border.dropdown.radius,
        'boxShadow': '0px 4px 8px rgba(29, 29, 29, 0.04), 0px 8px 16px rgba(29, 29, 29, 0.02)',
        '& .MuiMenu-list': {
          'boxSizing': 'border-box',
          'padding': material.spacing(2, 1.5),
          '& .MuiMenuItem-root': {
            'borderRadius': border.radius.md,
            'color': xxllncColor.dark80,
            'marginBottom': 0,
            'marginTop': material.spacing(1),
            'width': '100%',
            '& .MuiSvgIcon-root': {
              fill: xxllncColor.dark80,
            },
            '&:hover': {
              'color': xxllncColor.dark100,
              'background': xxllncColor.opDark10,
              '& .MuiSvgIcon-root': {
                fill: xxllncColor.dark100,
              }
            },
          },
          '& .Mui-selected': {
            'background': xxllncColor.dark80,
            'color': xxllncColor.light100,
            '& .MuiSvgIcon-root': {
              fill: xxllncColor.light100,
            },
          },
        },
      },
    }
  },
}

interface ITheme extends ThemeOptions {
  palette?: PaletteOptions & {
    common: {
      [key: string]: string
    }
  }
  sidebar: {
    width: number
    closedWidth: number
  }
  props: {
    MuiButtonBase: {
      disableRipple: boolean
    }
  }
}

export const theme: ITheme = {
  ...defaultTheme,
  typography: {
    fontFamily: typography.body,
    h6: {
      color: xxllncColor.dark100,
      fontSize: 20,
      fontStyle: 'normal',
      fontWeight: 500,
      lineHeight: '28px',
    },
    subtitle1: {
      color: xxllncColor.dark100,
      fontSize: 16,
      fontStyle: 'normal',
      fontWeight: 500,
      lineHeight: '24px',
    },
  },
  palette: {
    mode: 'light' as unknown as 'light',
    common: {
      black: color.black,
      white: color.white,
      inactive: color.alto,
      zakenZaaksysteem: color.greenPea,
      dataKoppel: color.pomegranate,
      dataIntegraties: color.gamboge,
      belastingBezwaren: color.veniceBlue,
      belastingMarktanalyse: color.curiousBlue,
      belastingVoormeldingen: color.viking,
      belasting4: color.nileBlue,
      onderwijs1: color.greenSmoke,
      omgeving1: color.beautyBush,
      omgeving2: color.tallPoppy,
      account: color.silverChalice,
      cloud: color.white,
      login: color.mineShaft,
    },
    primary: {
      main: color.primary.main,
    },
    secondary: {
      light: color.silverChalice,
      main: color.mineShaft,
      dark: color.codGray,
      contrastText: color.white,
    },
    background: {
      default: color.gallery,
    },
    error: {
      main: xxllncColor.red100,
    },
  },
  sidebar: {
    width: 356, // The default value is 240
    closedWidth: 74, // The default value is 55
  },
  shape: {
    borderRadius: border.radius.md,
  },
  components: {
    ...defaultTheme.components,
    ...raComponentOverrides,
    ...muiComponentOverrides,
  },
  props: {
    MuiButtonBase: {
      disableRipple: true,
    },
  },
}
