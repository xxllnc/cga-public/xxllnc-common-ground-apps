import { styled } from '@mui/material/styles'
import Typography from '@mui/material/Typography'
import { createElement, FC, forwardRef } from 'react'
import { AppBar, Logout, MenuItemLink, useResourceDefinitions, UserMenu } from 'react-admin'
import { getEnvVariable } from '../utils/getEnvVariable'
import { Resources } from './Menu'
import { border, xxllncColor } from './themes'

const getAppName = () => getEnvVariable('REACT_APP_NAME')

const Spacer = styled('span')({ flex: 1 })

interface CMenuProps {
  onClick?: any
}

const ConfigurationMenu = forwardRef<any, CMenuProps>(({ onClick }, ref) => {
  const resourcesDefinitions = useResourceDefinitions()
  const resources = Object.keys(resourcesDefinitions).map(name => resourcesDefinitions[`${name}`] as Resources)

  return (
    <>
      {resources.filter(resource => resource.options?.settings)
        .map((resource, index) => (
          <MenuItemLink
            key={resource.name}
            ref={ref}
            to={`/${resource.name}`}
            primaryText={resource.options?.label || resource.name}
            leftIcon={createElement(resource?.icon ?? '')}
            onClick={onClick as () => void}
            sidebarIsOpen
            sx={index === 0 ? { marginTop: 0 } : {}}
          />))
      }
    </>
  )})

const CustomUserMenu: FC = () => (
  <UserMenu>
    <ConfigurationMenu />
    <Logout />
  </UserMenu>
)

export const CustomAppBar: FC = () => (
  <AppBar
    container={'div'}
    elevation={1}
    userMenu={<CustomUserMenu />}
    sx={(theme) => ({
      'borderBottom': border.thin + ' solid ' + border.light,
      '& .RaAppBar-toolbar': {
        [theme.breakpoints.up('xs')]: {
          minHeight: theme.spacing(8),
        },
        [theme.breakpoints.down('sm')]: {
          minHeight: theme.spacing(7),
        }
      }
    })}

  >
    <Typography
      component="span"
      color="textPrimary"
      sx={(raTheme) => ({ fontWeight: 600, marginRight: raTheme.spacing(.5) })}
    >
      { getAppName() }
    </Typography>
    <Typography
      component="span"
      color="textPrimary"
      sx={(raTheme) => ({
        backgroundColor: xxllncColor.dark20,
        borderRadius: border.radius.sm,
        fontSize: 12,
        fontWeight: 600,
        lineHeight: raTheme.spacing(1).toString(),
        marginRight: raTheme.spacing(3),
        marginTop: '2px',
        padding: raTheme.spacing(.5),
      })}
    >
        Lab
    </Typography>
    <Typography
      id="react-admin-title"
      component="span"
      color="textPrimary"
      sx={{ flex: 1, overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' }}
    />
    <Spacer />
  </AppBar>
)
