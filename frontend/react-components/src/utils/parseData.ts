export const parseData = <T>(rawValue?: string): T | null => {
  if (!rawValue) return null
  try {
    return JSON.parse(rawValue) as T
  } catch (_) {
    return null
  }
}
