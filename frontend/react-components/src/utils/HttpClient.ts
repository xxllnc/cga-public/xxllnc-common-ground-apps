import { checkResponse, parseResponse } from './checkResponse'
import { postOptions } from './postOptions'

interface Props {
  method?: 'GET' | 'POST'
  url: string
  headers?: Headers
}

export const optimisticFetch = <T>({ method = 'GET', url, headers }: Props): Promise<T> =>
  fetch(url, { method, headers})
    .then(response => parseResponse<T>(response))
    .catch(_ => ({}) as unknown as T)

export const getCall = <T>(url: string, operation: string): Promise<T> =>
  fetch(`${url}/${operation}`)
    .then(response => checkResponse<T>(response))

export const postCall = async <T>(url: string, data: FormData | string, token: string, operation: string, headers?: Headers): Promise<T> =>
  fetch(`${url}/${operation}`, postOptions(data, token, headers))
    .then(response => checkResponse<T>(response))