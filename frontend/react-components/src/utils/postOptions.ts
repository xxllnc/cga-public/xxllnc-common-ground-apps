const createHeader = (token: string, existing?: Headers): Headers => {
  const headers = existing || new Headers()
  headers.append('Content-Type', 'application/json')
  if (token) headers.append('Authorization', 'Bearer ' + token)
  return headers
}

export const postOptions = (data: FormData | string, token: string, headers?: Headers): RequestInit => ({
  method: 'POST',
  headers: createHeader(token, headers),
  body: data
})

