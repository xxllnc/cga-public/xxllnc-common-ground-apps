/* eslint-disable */
import { ComponentType, createElement } from 'react';

interface InferredType {
  type?: ComponentType;
  component?: ComponentType;
  representation?: (props: any, children: any) => string;
}

class InferredElement {
  constructor(
    private type?: InferredType,
    private props?: any,
    private children?: any
  ) { }

  getElement(props = {}) {
    if (!this.isDefined()) {
      return
    }

    return this.children
      ? createElement(
        // @ts-ignore
        this.type.component,
        { ...this.props, ...props },
        this.children.length > 0
          ? this.children.map((child, index) =>
            child.getElement({ key: index, label: child.props.source })
          )
          : this.children.getElement()
      ) // @ts-ignore
      : createElement(this.type.component, { ...this.props, ...props })
  }

  isDefined() {
    return !!this.type
  }
}

export default InferredElement
