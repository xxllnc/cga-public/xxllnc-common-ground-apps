import { GetListResult, GetOneResult, RaRecord, testDataProvider } from 'react-admin'
import { Statusses } from '../types/callbackRequests'

const defaultMockList: RaRecord[] = [
  {
    uuid: '5586dde0-c9da-4df7-a6fa-568be4c5f6b1',
    id: '1',
    contact: 'contact 1',
    subject: 'subject 1',
    status: Statusses.Open,
    createdByName: 'Test user 1',
    createdAt: '2021-05-12T00:01:00.000Z'
  },
  {
    uuid: '5586dde0-c9da-4df7-a6fa-568be4c5f6b2',
    id: '2',
    contact: 'contact 2',
    subject: 'subject 2',
    status: Statusses.Afgehandeld,
    createdByName: 'Test user 2',
    createdAt: '2021-05-12T00:01:00.000Z'
  }
]

export const dataProviderForStories = (mockList: RaRecord[] = defaultMockList) => (
  testDataProvider({
    getOne: (_, params: { id: string }) =>
      Promise.resolve({ data: mockList.find(element => element.id === (params?.id ?? 1)) } as GetOneResult),
    getList: () => Promise.resolve({ data: mockList, total: mockList.length } as GetListResult)
  })
)