import { FC } from 'react'
import { Auth0Provider } from '@auth0/auth0-react'

interface Auth0ProviderWithHistoryOptions {
  scope: string
  audience: string
  domain: string
  clientId: string
  redirectUri: string
  children?: React.ReactNode
}

export const Auth0ProviderWithHistory: FC<Auth0ProviderWithHistoryOptions> =
  ({ scope, audience, domain, clientId, redirectUri, children }) => (
    <Auth0Provider
      audience={audience}
      scope={scope}
      domain={domain}
      clientId={clientId}
      redirectUri={redirectUri}
      cacheLocation='localstorage'
      useRefreshTokens={true}
    >
      {children}
    </Auth0Provider>
  )