import { Meta, Story } from '@storybook/react'
import { Admin, ListGuesser, Resource } from 'react-admin'
import { BrowserRouter, Navigate, useNavigate } from 'react-router-dom'
import { LoginPage } from '.'
import { dataProviderForStories } from '../../utils'
import { authProvider } from '../authProvider'
import { useAuth0User } from '../hooks/useAuth0User'
import { Auth0ProviderWithHistory } from '../Auth0ProviderWithHistory'
import { theme } from '../../layout'


export default {
  title: 'LoginPage'
} as Meta


const Page = () => (
  <LoginPage
    scopes={'cga.user cga.admin'}
    redirect={<Navigate to="/" />}
    returnTo={'forms'}
    debug={true}
    loginInfoUrl="https://controlpanel.vcap.me/api/v1/login_info"
    audience={'test'}
  />
)


const AdminWrapper: Story = () => {
  const auth0 = useAuth0User()
  return (
    <Admin
      loginPage={Page}
      authProvider={authProvider(auth0, 'test')}
      dataProvider={dataProviderForStories()}
      theme={theme}
    >
      <Resource name="callbackRequests" list={ListGuesser} />
    </Admin>
  )
}

const Auth = () => {
  const navigate = useNavigate()

  return (
    <Auth0ProviderWithHistory
      audience={'test'}
      scope={'cga.user cga.admin openid profile'}
      navigate={navigate}
      redirectUri={'test'}
      domain={'testDomain'}
      clientId={'testClientId'}
    >
      <AdminWrapper />
    </Auth0ProviderWithHistory>
  )
}

export const Example: Story = () => (
  <BrowserRouter>
    <Auth />
  </BrowserRouter>
)
