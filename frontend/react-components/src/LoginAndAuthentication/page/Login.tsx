import { useAuth0, User } from '@auth0/auth0-react'
import ErrorIcon from '@mui/icons-material/Error'
import LockIcon from '@mui/icons-material/Lock'
import { Avatar, Button, Card, CircularProgress, styled, TextField, Typography } from '@mui/material'
import { ChangeEvent, FC, MouseEvent, useEffect, useState } from 'react'
import { useTranslate } from 'react-admin'
import { Navigate } from 'react-router'
import { theme as xxTheme, xxllncColor } from '../../layout'
import { Xxllnc } from '../Icons'
import { authToken as authTokenUtil, organizationFetch } from '../utils'

const StyledHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  justifyContent: 'center',
  lineHeight: 0,
  marginBottom: theme.spacing(4),
  marginTop: theme.spacing(2),
  minWidth: 300,
}))

const StyledLogo = styled(Xxllnc)(({ theme }) => ({
  fill: xxTheme.palette?.common.inactive,
  width: theme.spacing(13),
}))

const Main = styled('div')(({ theme }) => ({
  alignItems: 'center',
  backgroundColor: xxTheme.palette?.common.login,
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  minHeight: '100vh',
  minWidth: 300,
  paddingLeft: theme.spacing(2),
  paddingRight: theme.spacing(2),
}))

const SyledCard = styled(Card)(({ theme }) => ({
  borderRadius: 5,
  boxSizing: 'border-box',
  marginBottom: theme.spacing(9),
  minWidth: 400,
  padding: theme.spacing(5),
}))

const AvatarContainer = styled(Card)(({ theme }) => ({
  display: 'flex',
  justifyContent: 'center',
  marginBottom: theme.spacing(3),
}))

const StyledTextField = styled(TextField)(({ theme }) => ({
  'boxSizing': 'border-box',
  'marginBottom': theme.spacing(3),
  '& .MuiInputLabel-formControl': {
    backgroundColor: xxllncColor.light100,
    transform: 'translate(0, 39%) scale(1)',
    top: '10px'
  },
  '& .MuiInputLabel-shrink': {
    transform: 'translate(-10%, -40%) scale(.88)',
    zIndex: 99,
  },
  '& .Mui-focused': {
    'color': '#007bad',
    '& input': {
      color: 'rgb(45, 51, 58)',
    }
  },
  '& .MuiInput-input': {
    boxSizing: 'border-box',
    height: '100%',
    padding: theme.spacing(1, 1.5),
    width: '100%',
  }
}))

const ErrorContainer = styled('div')(({ theme }) => ({
  'textAlign': 'center',
  'marginBottom': theme.spacing(3),
  '& .MuiTypography-colorError': {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'center',
  },
}))

const StyledButton = styled(Button)(({ theme }) => ({
  'backgroundColor': '#0069D2',
  'borderRadius': 3,
  'color': xxllncColor.light100,
  'fontSize': 16,
  'lineHeight': '16px',
  'padding': theme.spacing(2),
  'textTransform': 'capitalize',
  '&:hover': {
    backgroundColor: '#0059d6',
    boxShadow: 'rgba(0, 0, 0, 0.08) 0px 0px 0px 150px inset',
    transition: 'background-color .25s ease-in-out,box-shadow .25s ease-in-out',
  },
  '& svg': {
    color: xxllncColor.light100,
  }
}))

const LoginPageComponent: FC<Props> = ({ scopes, redirect, returnTo, loginInfoUrl, audience, debug }) => {
  const [authToken, setAuthToken] = useState<string | null>(null)
  const [customError, setCustomError] = useState<string | null>(null)
  const [orgIsLoading, setOrgIsLoading] = useState(false)
  const [email, setEmail] = useState<string>(authTokenUtil.getOrganizationEmail())
  const [orgError, setOrgError] = useState(false)

  const translate = useTranslate()
  const {
    isLoading,
    isAuthenticated,
    error,
    user,
    loginWithRedirect,
    getAccessTokenSilently,
    logout
  } = useAuth0()

  useEffect(() => {
    // TODO: We should switch to error.name, but it's not in the error at this time
    if (error?.message === 'Cannot read property \'organization_uuid\' of undefined') {
      logout({ federated: false, returnTo })
    } else if (error?.message === 'authorization request parameter organization must be an organization id') {
      setCustomError(translate('login.unknownOrganizationId'))
    }
  }, [authToken, error])

  useEffect(() => {
    const existingToken = authTokenUtil.getToken()
    if (existingToken) return setAuthToken(existingToken)
    if (isAuthenticated && user) {
      void (async () => {
        try {
          // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
          const organization = user.org_id ? { organization: user.org_id } : null
          const token = await getAccessTokenSilently({
            ...organization,
            audience,
            scope: scopes
          })
          if (token) {
            authTokenUtil.setToken(token)
            setAuthToken(token)
          }
        } catch (err) {
          setCustomError(translate('login.incognitoError'))
        }
      })()
    }
  }, [isAuthenticated])

  const onChangeInput = (event: ChangeEvent<{ value: unknown }>) => {
    setOrgError(false)
    setEmail(event.target.value as string)
  }

  const onClickLogin = async (event: MouseEvent) => {
    event.preventDefault()

    if (!email) return
    setOrgIsLoading(true)

    const organizationId = authTokenUtil.getOrganizationId()
    if (organizationId === '') {
      await organizationFetch({ loginInfoUrl, email })
        .then(organization => {
          // eslint-disable-next-line camelcase
          loginWithRedirect({ organization: organization.auth0Identifier, login_hint: email })
            .catch((_) => setOrgError(true))
        })
        .catch((_) => setOrgError(true))
        .finally(() => {
          setOrgIsLoading(false)
        })
    } else {
      const auth0Identifier = authTokenUtil.getAuth0Identifier()
      // eslint-disable-next-line camelcase
      loginWithRedirect({ organization: auth0Identifier, login_hint: email })
        .catch((_) => setOrgError(true))
    }
  }

  if (authToken) return <>{redirect}</>
  if (isAuthenticated) return <Main />

  return (
    <Main >
      <StyledHeader>
        <StyledLogo fill={xxTheme.palette?.common.inactive} />
      </StyledHeader>
      <SyledCard >
        <form>
          <AvatarContainer>
            <Avatar sx={theme => ({ backgroundColor: theme.palette.secondary.main })}>
              <LockIcon />
            </Avatar>
          </AvatarContainer>
          {!isAuthenticated &&
            <>
              <Typography
                align="center"
                component="p"
                variant="body2"
                sx={theme => ({
                  color: '#2D333A',
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  marginBottom: theme.spacing(3)
                })}
              >
                {translate('login.hint')}
              </Typography>
              <StyledTextField
                variant="standard"
                error={orgError}
                label={translate('login.email')}
                helperText={orgError ? translate('login.emailHint') : undefined}
                onChange={onChangeInput}
                defaultValue={email}
              />
            </>
          }
          {customError &&
            <ErrorContainer>
              <Typography variant="body2" color="error" component="p">
                <ErrorIcon sx={theme => ({
                  height: 16,
                  marginRight: theme.spacing(.5),
                  width: 16
                })} />
                {customError || translate('ra.auth.sign_in_error')}
              </Typography>
            </ErrorContainer>
          }
          <StyledButton
            variant="contained"
            type="submit"
            // eslint-disable-next-line @typescript-eslint/no-misused-promises
            onClick={onClickLogin}
            disabled={orgIsLoading || isLoading || (user && !authToken && !error)}
            fullWidth
          >
            <Progress
              orgIsLoading={orgIsLoading}
              isLoading={isLoading}
              user={user}
              authToken={authToken}
              error={error}
            />
          </StyledButton>
        </form>
      </SyledCard>
    </Main>
  )
}

interface ProgressProps {
  orgIsLoading: boolean
  isLoading: boolean
  user: User | undefined
  authToken: string | null
  error: Error | undefined
}

const Progress: FC<ProgressProps> = ({ orgIsLoading, isLoading, user, authToken, error }) => {
  const translate = useTranslate()

  if (orgIsLoading || isLoading || (user && !authToken && !error)) {
    return <CircularProgress size={16} thickness={2} />
  } else {
    return <>{translate('ra.auth.sign_in')}</>
  }
}

interface Props {
  scopes: string
  redirect: Partial<typeof Navigate>
  returnTo: string
  loginInfoUrl: string
  audience: string
  debug?: boolean
}

export const LoginPage: FC<Props> = (props) => (<LoginPageComponent {...props} />)