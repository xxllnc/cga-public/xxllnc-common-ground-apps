import { useEffect } from 'react'
import { Auth0ContextInterface } from '@auth0/auth0-react'
import { useAuth0 } from '@auth0/auth0-react'
import { authToken } from '../utils'

export const useAuth0User = (): Auth0ContextInterface => {
  const auth0 = useAuth0()
  const auth0Identifier = authToken.getAuth0Identifier()
  const email = authToken.getOrganizationEmail()
  const { isAuthenticated, loginWithRedirect, isLoading } = auth0
  const token = authToken.getToken()

  useEffect(() => {
    if (!isAuthenticated && token && !isLoading && auth0Identifier) {
      void (async () => {
        // eslint-disable-next-line camelcase
        await loginWithRedirect({organization: auth0Identifier, login_hint: email})
      })()
    }
  }, [isAuthenticated, loginWithRedirect, isLoading, token])

  return auth0
}
