import { Auth0ContextInterface } from '@auth0/auth0-react/src/auth0-context'
import { AuthProvider, UserIdentity } from 'ra-core'
import { parseData } from '../utils'
import { authToken, getCombinedFullName, getScope } from './utils'

let lastStatus = 200

const clearLocalStorage = () => {
  authToken.deleteToken()
  localStorage.removeItem('user')
}

export const authProvider = (auth0: Auth0ContextInterface, returnTo: string, mock = false): AuthProvider => ({
  login: () => Promise.resolve(),
  logout: () => {
    if (auth0.isAuthenticated)
      auth0.logout({ federated: false, returnTo })
    clearLocalStorage()
    return Promise.resolve()
  },
  checkError: (error: { status: number }) => {
    const { status } = error
    lastStatus = status
    if (status === 401) {
      clearLocalStorage()
      if (auth0.isAuthenticated) void auth0.logout()
      return Promise.reject()
    }
    return Promise.resolve()
  },
  checkAuth: async () => {
    if (mock || authToken.getToken())
      return Promise.resolve()
    if (!auth0.isAuthenticated)
      return Promise.reject({ message: false })

    auth0.logout({ federated: false, returnTo })
    return Promise.reject({ message: false })
  },
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
  getPermissions: () => Promise.resolve({ scopes: getScope(mock), authorized: lastStatus !== 403 }),
  getIdentity: (): Promise<UserIdentity> => {
    if (mock) return Promise.resolve({ id: 1, fullName: 'xxllnc - Tessa tester' })
    const unknownUser: UserIdentity = { id: 'unknown', fullName: '' }
    if (!auth0.user) {
      const userFromStorage: UserIdentity =
        parseData<UserIdentity>(localStorage.getItem('user') ?? undefined) ?? unknownUser
      return Promise.resolve(getCombinedFullName(userFromStorage))
    }
    const user: UserIdentity = { id: auth0.user?.sub ?? '', fullName: auth0.user?.nickname ?? '' }
    localStorage.setItem('user', JSON.stringify(user))
    return Promise.resolve(getCombinedFullName(user))
  },
})