import { getEnvVariable } from '../../utils'

export const debug = getEnvVariable('REACT_APP_DEBUG') === 'true' || false
export const mock = (): string | boolean => getEnvVariable('REACT_APP_MOCK_REQUESTS') === 'true' || false
