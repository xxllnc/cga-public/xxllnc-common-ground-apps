import { organizationFetch } from './organizationFetch'

const loginInfo = {
  auth0Identifier: 'org_WxZyvrkltQlvOXLc',
  organizationId: '0aa2beca-3819-4f49-aba2-2631e0bcbfb8'
}
const globalRef: any = global

// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
globalRef.fetch = jest.fn(() =>
  Promise.resolve({
    ok: true,
    status: 200,
    json: () => Promise.resolve([loginInfo]),
  })
)

describe('OrganizationFetch test', () => {

  it('should get the default orgId and uuid', async () => {

    const organization = await organizationFetch({
      loginInfoUrl: 'https://controlpanel.vcap.me/api/v1/login_info', email: 'admin@xxllnc.nl'
    })

    expect(fetch).toHaveBeenCalledTimes(1)
    expect(organization).toMatchObject(loginInfo)
  })

})
