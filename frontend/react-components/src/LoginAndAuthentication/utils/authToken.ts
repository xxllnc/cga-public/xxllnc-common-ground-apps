/* eslint-disable camelcase */
import jwtDecode from 'jwt-decode'
import { getEnvVariable } from '../../utils'

interface Token {
  'https://xxllnc.nl/organization_name': string,
  'https://xxllnc.nl/organization_uuid': string,
  'https://xxllnc.nl/name': string,
  'org_id': string
}


const tokenName = () => getEnvVariable('REACT_APP_AUTH_TOKEN', 'token')
const getToken = (): string | null => localStorage.getItem(tokenName())
const setToken = (token: string): void => {
  localStorage.setItem(tokenName(), `Bearer ${token}`)

  try {
    const decoded = jwtDecode<Token>(token)
    const organizationName = decoded['https://xxllnc.nl/organization_name']
    if (organizationName !== undefined) localStorage.setItem('organization_name', organizationName)
  } catch (_e) {
    // invalid token, do nothing
  }

}
const deleteToken = (): void => localStorage.removeItem(tokenName())

const getTokenProperty = (property: string): string => {
  const token = getToken()
  if (!token) return ''
  const decoded = jwtDecode<Token>(token)
  // eslint-disable-next-line @typescript-eslint/no-unsafe-return, security/detect-object-injection
  return decoded[property]
}

const getOrganizationId = (): string => getTokenProperty('https://xxllnc.nl/organization_uuid')
const getOrganizationName = (): string => getTokenProperty('https://xxllnc.nl/organization_name')
const getOrganizationEmail = (): string => getTokenProperty('https://xxllnc.nl/name')
const getAuth0Identifier = (): string => getTokenProperty('org_id')

export const authToken = {
  getToken,
  setToken,
  deleteToken,
  getOrganizationId,
  getOrganizationName,
  getOrganizationEmail,
  getAuth0Identifier
}
