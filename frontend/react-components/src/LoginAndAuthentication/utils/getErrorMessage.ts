import { OAuthError } from '@auth0/auth0-react'

const isOAuthError = (checkErrorType: Error | OAuthError): checkErrorType is OAuthError =>
  (checkErrorType as OAuthError)?.error !== undefined

// Source https://auth0.com/docs/libraries/common-auth0-library-authentication-errors
const errorMessages: { [name: string]: string } = {
  'access_denied': 'errors.accessDenied',
  'invalid_user_password': 'errors.invalidUserPassword',
  'mfa_invalid_code': 'errors.mfaInvalidCode',
  'mfa_registration_required': 'errors.mfaRegistrationRequired',
  'mfa_required': 'errors.mfaRequired',
  'password_leaked': 'errors.passwordLeaked',
  'PasswordHistoryError': 'errors.PasswordHistoryError',
  'PasswordStrengthError': 'errors.PasswordStrengthError',
  'too_many_attempts': 'errors.tooManyAttempts',
  'unauthorized': 'errors.unauthorized',
  'Invalid state': 'errors.invalidState',
  'unknown': 'errors.unknown'
}

export const getErrorMessage = (authError: Error | OAuthError): string => {

  const name = isOAuthError(authError) ? authError.error : authError?.message ?? 'unknown'

  return errorMessages?.[String(name)] ?? errorMessages.unknown
}
