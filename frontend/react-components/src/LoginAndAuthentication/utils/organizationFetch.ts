import { checkResponse } from '../../utils'

interface Props { loginInfoUrl: string, email: string }

export interface OrganizationBase {
  /** @example org_AEglyXUkuFrbC2Rs */
  auth0Identifier: string
  organizationId: string
}

const NOT_FOUND_MESSAGE = 'Er is een fout opgetreden, de organisatie is niet bekend ' +
  'Neem contact op met de organisatie waar u wilt inloggen of probeer het later opnieuw.'


const isOrg = (object?: OrganizationBase) => object && Object.prototype.hasOwnProperty.call(object, 'auth0Identifier')
  && Object.prototype.hasOwnProperty.call(object, 'organizationId')

export const organizationFetch = async ({ loginInfoUrl, email }: Props): Promise<OrganizationBase> => {

  const url = new URL(loginInfoUrl)
  url.search = new URLSearchParams({ filter: `{"emailAddress":"${email}"}` }).toString()
  const headers = new Headers()
  headers.append('Content-Type', 'application/json')

  return fetch(url, { headers, mode: 'cors' })
    .then(response => checkResponse<OrganizationBase[]>(response))
    .then(response => {
      if (isOrg(response?.[0]))
        return response[0]
      throw new Error(NOT_FOUND_MESSAGE)
    })
}
