import { authToken } from './authToken'


export const getScope = (mock=false): string[] => {
  if (mock)
    return ['cga.admin', 'cga.user']
  try {
    const token = authToken.getToken() || ''
    const parsed = JSON.parse(atob(token.split('.')[1])) as {scope?: string}
    return parsed?.scope?.split(' ') ?? []
  } catch (e) {
    return []
  }
}
