import { waitFor } from '@testing-library/react'
import { authToken } from './authToken'
import { getScope } from './getScope'
import { OrganizationBase } from './organizationFetch'


describe('Utils test', () => {
  // eslint-disable-next-line max-len
  const token = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InlVUlotQWY5eUEwdDVmWGhrRnA3RyJ9.eyJodHRwczovL3h4bGxuYy5ubC9uYW1lIjoiYWRtaW5AZXh4ZWxsZW5jZS5ubCIsImh0dHBzOi8veHhsbG5jLm5sL29yZ2FuaXphdGlvbl91dWlkIjoiMGFhMmJlY2EtMzgxOS00ZjQ5LWFiYTItMjYzMWUwYmNiZmI4IiwiaXNzIjoiaHR0cHM6Ly94eGxsbmMtbG9jYWwuZXUuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDYwYjhiMzg2MjM5ZThiMDA3MDIzMGM5YiIsImF1ZCI6WyJodHRwczovL21pam4udmNhcC5tZS9jZ2EvYXBpIiwiaHR0cHM6Ly94eGxsbmMtbG9jYWwuZXUuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTYzNDcyMzI2MCwiZXhwIjoxNjM0ODA5NjYwLCJhenAiOiI0T1E3UDF1Z0wzTjFSUkNsTUtHY3JZWmtXWGpaMHlWeCIsInNjb3BlIjoib3BlbmlkIHByb2ZpbGUgZW1haWwgY2dhLnVzZXIgY2dhLmFkbWluIiwib3JnX2lkIjoib3JnX1d4Wnl2cmtsdFFsdk9YTGMifQ.xUDDT4AOJNd4yjrgeBRekiXm3cNxrLblzUTZk1LYukiUTmHNPDODXAr8yRESET1_7GLUAamyLMhFjHeeFH5WBLRkFu2DqfS2JmtMgsoly4rNWMxXuMEilC0O3EyVTsGxt3HQeaB8ndfeIBbBNou0zIWtycp6UrlKZN4Av2ZfZMrDAiccI_GdQD4ojF3iME8he9ey7n9JKrYyJdlPO3YYp5P_Y61Pjiz6RcX7G8nKhd_eqZCGtZSK7vzRZcgRZktKR0AsFsCmz8I6giIIBmF8BKmZttVjY4JtGZD_PvU4x1lleGJsk-aXdvahAIIUZ-UQDdVF_JZfAGiraR0Iq-g9Sw'
  const organization: OrganizationBase = {
    organizationId: '0aa2beca-3819-4f49-aba2-2631e0bcbfb8',
    auth0Identifier: 'org_WxZyvrkltQlvOXLc'
  }
  const organizationEmailInToken = 'admin@exxellence.nl'

  it('getScope should give the expected scopes from token', async () => {
    authToken.setToken(token)

    await waitFor(() => expect(authToken.getToken()).toEqual(`Bearer ${token}`))
    expect(getScope()).toEqual(['openid', 'profile', 'email', 'cga.user', 'cga.admin'])
  })

  it('getScope should return empty scope array []', async () => {
    const fakeToken = 'Fake-Token'
    authToken.setToken(fakeToken)

    await waitFor(() => expect(authToken.getToken()).toEqual(`Bearer ${fakeToken}`))
    expect(getScope()).toEqual([])
  })

  it('authToken should delete token', async () => {
    authToken.setToken(token)

    await waitFor(() => expect(authToken.getToken()).toEqual(`Bearer ${token}`))
    authToken.deleteToken()
    expect(authToken.getToken()).toEqual(null)
  })

  it('authToken should contain the organizationId', async () => {
    authToken.setToken(token)
    const organizationId = authToken.getOrganizationId()
    await waitFor(() => expect(organizationId).toEqual(organization.organizationId))
  })

  it('authToken should contain the auth0Identifier', async () => {
    authToken.setToken(token)
    const auth0Identifier = authToken.getAuth0Identifier()
    await waitFor(() => expect(auth0Identifier).toEqual(organization.auth0Identifier))
  })

  it('authToken should contain the organizationEmail', async () => {
    authToken.setToken(token)
    const organizationEmail = authToken.getOrganizationEmail()
    await waitFor(() => expect(organizationEmail).toEqual(organizationEmailInToken))
  })
})