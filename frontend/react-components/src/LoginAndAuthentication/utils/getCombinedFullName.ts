import { UserIdentity as RAUserIdentity } from 'ra-core'

interface UserIdentity extends RAUserIdentity {
  nickname?: string
}

export const getCombinedFullName = (user: UserIdentity): UserIdentity => {
  const organizationName = localStorage.getItem('organization_name')
  if (!organizationName) return user
  const combinedName = `${organizationName}${user.nickname ? ` - ${user.nickname}` : ''}`
  return {
    ...user,
    fullName: combinedName
  }
}