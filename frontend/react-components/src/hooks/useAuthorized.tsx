import { useEffect } from 'react'
import { NotificationType, useNotify, usePermissions } from 'react-admin'

export const useAuthorized = (): void => {
  const notify = useNotify()
  const permissions = usePermissions<{ authorized?: boolean }>()

  useEffect(() => {
    if (permissions?.permissions?.authorized === false)
      notify('warnings.notAuthorized', { type: 'warning' })

  }, [permissions, notify])
}
