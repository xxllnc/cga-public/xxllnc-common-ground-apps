export const useQueryParameters = (): URLSearchParams =>
  new URLSearchParams(window.location.search)