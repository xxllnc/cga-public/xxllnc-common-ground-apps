import get from 'lodash/get'
import { useRecordContext, useTranslate } from 'ra-core'
import { RaRecord } from 'react-admin'

interface ToolbarTitleProps {
  record?: RaRecord,
  source?: string,
  i18n?: string,
  className?: string,
  [key: string]: any,
}

export const ToolbarTitle = ({ record, source, i18n, className, ...props }: ToolbarTitleProps): JSX.Element | null => {
  const translate = useTranslate()
  const getRecord = record ? record : useRecordContext(props)
  // eslint-disable-next-line @typescript-eslint/no-unsafe-call
  const getSource = source ? get(getRecord, source) as string : undefined

  if (i18n && source) {
    return <span className={className}>{translate(i18n, { name: getSource })}</span>
  } if (source) {
    return <span className={className}>{getSource}</span>
  } if (i18n) {
    return <span className={className}>{translate(i18n)}</span>
  } else {
    return null
  }
}