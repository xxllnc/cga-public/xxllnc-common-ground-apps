import { FC } from 'react'
import {
  Toolbar,
  SaveButton,
  DeleteButton,
  usePermissions,
  ToolbarProps,
  SaveButtonProps,
  DeleteButtonProps,
  RaRecord,
  useRecordContext
} from 'react-admin'
import { useFormState } from 'react-hook-form'

interface PermissionsProps {
  permissions: {
    scopes: string[]
  }
}

interface Access {
  scopes?: string[]
  permisson?: (record: RaRecord) => void
}

interface RestrictedToolbarProps extends ToolbarProps {
  saveAccess?: Access
  saveButtonProps?: SaveButtonProps
  deleteAccess?: Access
  deleteButtonProps?: DeleteButtonProps
  hideDeleteButton?: boolean
}

const valueOrDefault = (defaultValue: boolean, value?: boolean): boolean =>
  typeof value === 'undefined' ? defaultValue : value

const grantedAccess = (scopes: string[], access: Access | undefined, record: RaRecord) => {
  if (!access) return true
  if (!access.scopes && (access.permisson?.(record) ?? true)) return true
  return access.scopes?.some((i: string) => scopes?.includes(i)) && (access.permisson?.(record) ?? true)
}

export const RestrictedToolbar: FC<RestrictedToolbarProps> = ({
  saveAccess,
  deleteAccess,
  saveButtonProps,
  deleteButtonProps,
  hideDeleteButton = false,
  ...props
}) => {
  const { permissions } = usePermissions() as PermissionsProps
  const { isValid, isValidating, isSubmitting } = useFormState()
  const record = useRecordContext<RaRecord>()

  const disabled = !valueOrDefault(!isValidating, saveButtonProps?.alwaysEnable)

  if (!record) return null

  return (
    <Toolbar {...props}
      sx={{
        flex: 1,
        display: 'flex',
        justifyContent: 'flex-end'
      }}
    >
      {!hideDeleteButton &&
        <DeleteButton
          sx={{ margin: 5 }}
          record={record}
          disabled={!grantedAccess(permissions?.scopes, deleteAccess, record)}
          {...deleteButtonProps}
        />}
      <SaveButton
        sx={{ margin: 5 }}
        disabled={disabled || !grantedAccess(permissions?.scopes, saveAccess, record)}
        type="submit"
        invalid={!isValid}
        saving={isValidating || isSubmitting}
        {...saveButtonProps}
      />
    </Toolbar>
  )
}
