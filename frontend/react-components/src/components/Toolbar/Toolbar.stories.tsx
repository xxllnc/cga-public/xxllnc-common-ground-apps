import { Meta, Story } from '@storybook/react'
import { ComponentType, FC } from 'react'
import {
  Admin, Edit, LayoutProps, ListGuesser, RaRecord, Resource, SimpleForm, TextInput, useResourceDefinitions, useSidebarState
} from 'react-admin'
import { Statusses } from '../../types/callbackRequests'
import { dataProviderForStories } from '../../utils'
import { RestrictedToolbar } from './RestrictedToolbar'
import { CustomLayout, theme } from '../../layout'

export default {
  title: 'Toolbars'
} as Meta

const Layout: FC<LayoutProps> = (props) => {
  const [open] = useSidebarState() // Replaces useSelector for sidebarOpen
  const resourcesDefinitions = useResourceDefinitions() // Replaces useSelector for getResources
  const resources = Object.keys(resourcesDefinitions).map(name => resourcesDefinitions[String(name)])

  return (
    <CustomLayout {...props} appName="Storybook app" resources={resources} open={open} />
  )
}

const Wrapper: FC<{ edit: ComponentType }> = ({ edit }) => (
  <Admin
    dataProvider={dataProviderForStories()}
    theme={theme}
    layout={Layout}
  >
    <Resource name="callbackRequests" list={ListGuesser} edit={edit} />
  </Admin>
)


const EditWithToolbar = () => (
  <Edit>
    <SimpleForm
      toolbar={<RestrictedToolbar />}>
      <TextInput source="contact" />
    </SimpleForm>
  </Edit>
)

export const Normal: Story = () => (<Wrapper edit={EditWithToolbar} />)


const EditWithAccessToolbar = (props) => (
  <Edit {...props}>
    <SimpleForm
      toolbar={
        <RestrictedToolbar
          saveAccess={{
            scopes: ['test']
          }}
          deleteAccess={{
            scopes: ['test']
          }}
        />}>
      <TextInput source="contact" />
    </SimpleForm>
  </Edit>
)

export const WithScopes: Story = () => (<Wrapper edit={EditWithAccessToolbar} />)

const EditWithPermissionsToolbar = (props) => (
  <Edit {...props}>
    <SimpleForm
      toolbar={
        <RestrictedToolbar
          saveAccess={{
            permisson: ({ status }: RaRecord) => status === Statusses.Open
          }}
        />}>
      <TextInput source="contact" />
    </SimpleForm>
  </Edit>
)

export const WithSavePermission: Story = () => (<Wrapper edit={EditWithPermissionsToolbar} />)


const EditWithButtonProps = (props) => (
  <Edit {...props}>
    <SimpleForm
      toolbar={
        <RestrictedToolbar
          saveButtonProps={{
            variant: 'outlined'
          }}
          deleteButtonProps={{
            mutationMode: 'optimistic'
          }}
        />}>
      <TextInput source="contact" />
    </SimpleForm>
  </Edit>
)

export const WithButtonProps: Story = () => (<Wrapper edit={EditWithButtonProps} />)


const EditWithoutDelete = (props) => (
  <Edit {...props}>
    <SimpleForm toolbar={<RestrictedToolbar hideDeleteButton />}>
      <TextInput source="contact" />
    </SimpleForm>
  </Edit>
)

export const WithoutDeleteButton: Story = () => (<Wrapper edit={EditWithoutDelete} />)
