import { ListActionsProps, TopToolbar } from 'react-admin'
import { createTheme } from '@mui/material/styles'
import makeStyles from '@mui/styles/makeStyles'
import { ActionToolbarHeaderTitle } from '../Header'

const material = createTheme()

const useStyles = makeStyles(
  {
    toolbar: {
      display: 'flex',
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    spacer: {
      flex: 1,
    },
    title: {
      marginRight: 'auto',
      flex: 1,
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      fontSize: 28,
      [material.breakpoints.down('md')]: {
        fontSize: 20,
      },
      lineHeight: '40px',
    },
  },
  { name: 'xxllncListActionsToolbar' }
)

interface ListActionsToolbarProps extends ListActionsProps {
  i18n: string,
  children?: React.ReactNode,
}

export const ListActionsToolbar = ({ i18n, children }: ListActionsToolbarProps): JSX.Element => {
  const classes = useStyles()

  return (
    <TopToolbar className={classes.toolbar}>
      <ActionToolbarHeaderTitle i18n={i18n} className={classes.title} />
      <span className={classes.spacer} />
      {children}
    </TopToolbar>
  )
}
