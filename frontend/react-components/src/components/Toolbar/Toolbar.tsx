import { DeleteButtonProps, RaRecord, SaveButton, SaveButtonProps, Toolbar, ToolbarProps, useRecordContext } from 'react-admin'
import { CustomDeleteButton } from '../Button/DeleteButton'
import { useFormState } from 'react-hook-form'
import { FC } from 'react'

const valueOrDefault = (defaultValue: boolean, value?: boolean): boolean =>
  typeof value === 'undefined' ? defaultValue : value

interface CustomToolbarProps extends ToolbarProps {
  saveButtonProps?: SaveButtonProps
  deleteButtonProps?: DeleteButtonProps
}

export const CustomToolbar: FC<CustomToolbarProps> = ({ saveButtonProps, deleteButtonProps, ...props }) => {

  const { isValid, isValidating, isSubmitting } = useFormState()
  const disabled = !valueOrDefault(!isValidating, saveButtonProps?.alwaysEnable)
  const record = useRecordContext<RaRecord>()

  return (
    <Toolbar {...props}
      sx={{
        display: 'flex',
        justifyContent: 'space-between',
      }}
    >
      <SaveButton
        disabled={disabled}
        invalid={!isValid}
        saving={isValidating || isSubmitting}
        {...saveButtonProps}
      />
      <CustomDeleteButton record={record} {...deleteButtonProps} />
    </Toolbar>
  )
}