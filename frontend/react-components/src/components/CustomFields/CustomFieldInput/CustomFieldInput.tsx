import {
  required,
  TextInput,
  SelectInput,
} from 'react-admin'
import { CustomFieldsWithValue, CustomFieldTypes } from '../../../types/callbackRequests'

const getCustomFieldValidate = (customField: CustomFieldsWithValue): any[] => {
  const isRequired = customField.required
  return isRequired ? [required()] : []
}

const formatCustomField = (input: CustomFieldsWithValue, customField: CustomFieldsWithValue): string => {
  if (input && !input.value) return ''
  return input?.value ? input.value : customField?.value ? customField.value : ''
}

const parseCustomField = (newValue: string, customField: CustomFieldsWithValue) =>
  ({ ...customField, value: newValue })


interface ICustomField {
  customField: CustomFieldsWithValue
  index: number
}

export const CustomFieldInput = ({ customField, index }: ICustomField): JSX.Element => {
  if (customField.type === CustomFieldTypes.Select)
    return <SelectInput
      key={index}
      label={customField.name}
      source={`customFields[${index}]`}
      choices={customField.options}
      optionValue="name"
      format={(input: CustomFieldsWithValue) => formatCustomField(input, customField)}
      parse={(value: string) => parseCustomField(value, customField)}
      validate={getCustomFieldValidate(customField)}
      fullWidth
    />
  return <TextInput
    key={index}
    label={customField.name}
    source={`customFields[${index}]`}
    format={(input: CustomFieldsWithValue) => formatCustomField(input, customField)}
    parse={(value: string) => parseCustomField(value, customField)}
    validate={getCustomFieldValidate(customField)}
    fullWidth
  />

}
