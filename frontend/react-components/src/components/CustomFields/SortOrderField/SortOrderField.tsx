/* eslint-disable */
import PropTypes from 'prop-types'
import { TextFieldProps, useRecordContext, useUpdate, useRefresh } from 'react-admin'
import { IconButton } from '@mui/material'
import { ArrowUpward, ArrowDownward } from '@mui/icons-material'

export const SortOrderField = (props: TextFieldProps): JSX.Element => {
  if (!props.source) return <>
    <IconButton aria-label="down" disabled={true} size="large">
      <ArrowUpward fontSize="small" />
    </IconButton>
    <IconButton aria-label="up" disabled={true} size="large">
      <ArrowDownward fontSize="small" />
    </IconButton>
  </>

  const { source } = props
  const record = useRecordContext(props)
  const refresh = useRefresh()

  const [down, {isLoading : isLoadingDown}] = useUpdate('customFields',
    { id: record.id, data: { sortOrder: Number(record[String(source)]) - 1 }}, { onSuccess: refresh })
  const [up, {isLoading : isLoadingUp} ] = useUpdate('customFields',
    { id: record.id, data: { sortOrder: Number(record[String(source)]) + 1 }}, { onSuccess: refresh })

  return <>
    <IconButton
      aria-label="down"
      disabled={isLoadingDown || isLoadingUp}
      onClick={() => down()}
      size="large"
    >
      <ArrowUpward fontSize="small" />
    </IconButton>
    <IconButton
      aria-label="up"
      disabled={isLoadingDown || isLoadingUp}
      onClick={() => up()}
      size="large"
    >
      <ArrowDownward fontSize="small" />
    </IconButton>
  </>
}

SortOrderField.propTypes = {
  label: PropTypes.string,
  record: PropTypes.object,
  source: PropTypes.string.isRequired
}
