import DescriptionIcon from '@mui/icons-material/Description'
import { Meta, Story } from '@storybook/react'
import { Admin, Resource, useResourceDefinitions, useSidebarState } from 'react-admin'
import { dataProviderForStories } from '../../../utils'
import { CustomFieldListGuesser as ListGuesser } from './CustomFieldListGuesser'
import { CustomLayout } from '../../../layout/Layout'

export default {
  title: 'ListGuesser'
} as Meta

const ListGuesserWithIgnoredFields = (props) => (
  <ListGuesser
    {...props}
    ignoreList={['uuid', 'updateAt', 'createdAt', 'resultId', 'customFields']}
  />
)

export const CustomFieldListGuesser: Story = () => {

  const [open] = useSidebarState()
  const resourcesDefinitions = useResourceDefinitions()
  const resources = Object.keys(resourcesDefinitions).map(name => resourcesDefinitions[String(name)])

  return (
    <Admin
      dataProvider={dataProviderForStories()}
      layout={(props) => <CustomLayout {...props} open={open} resources={resources} appName='Terugbelverzoeken' />}
    >
      <Resource
        name="callbackRequests"
        list={ListGuesserWithIgnoredFields}
        icon={DescriptionIcon}
        options={{
          label: 'Verzoeken',
          scopes: ['cga.admin', 'cga.user']
        }}
      />
    </Admin>
  )
}