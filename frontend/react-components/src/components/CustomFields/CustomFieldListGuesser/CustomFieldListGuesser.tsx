import { Datagrid, List, ListViewProps, RaRecord, TextField, useListContext } from 'react-admin'


const CustomFields = (props: Omit<ListGuesserViewProps, 'children'>) => {

  const { data } = useListContext<RaRecord>()

  const record = data?.[0]
  if (!record)
    return null

  return (
    <Datagrid bulkActionButtons={false} rowClick="edit">
      {Object.keys(record).map((field, index) => {
        if (!props.ignoreList?.includes(field)) {
          return <TextField key={index} label={field} source={field} />
        }
      })}
    </Datagrid>
  )
}

interface ListGuesserViewProps extends ListViewProps {
  ignoreList?: string[]
}

export const CustomFieldListGuesser = (props: Omit<ListGuesserViewProps, 'children'>) => (
  <List emptyWhileLoading exporter={false} {...props}>
    <CustomFields {...props} />
  </List>
)

