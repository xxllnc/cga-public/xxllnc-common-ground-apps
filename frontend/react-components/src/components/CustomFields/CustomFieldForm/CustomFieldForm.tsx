import { RaRecord } from 'ra-core'
import { FC, useEffect, useState } from 'react'
import {
  ArrayInput,
  BooleanInput,
  required,
  SelectInput,
  SimpleForm,
  SimpleFormIterator,
  TextInput,
  useRecordContext,
  useTranslate
} from 'react-admin'
import { CustomFieldTypes } from '../../../types/callbackRequests'

interface WithPropsProps {
  children: any
  [x: string]: any
}

export const WithProps = ({ children, ...props }: WithPropsProps): JSX.Element => (
  // eslint-disable-next-line @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-return
  children(props)
)

export interface CustomFieldRecord extends RaRecord {
  type: string
}

interface CustomFieldProps {
  [x: string]: unknown
}

export const CustomFieldForm: FC<CustomFieldProps> = ({ ...props }) => {

  const record = useRecordContext<CustomFieldRecord>() ?? {}
  const [isList, setIsList] = useState(false)
  const translate = useTranslate()

  useEffect(
    () => setIsList(record.type === CustomFieldTypes.Select),
    [record]
  )

  return (
    <SimpleForm {...props} >
      <TextInput label={'label.name'} source="name" validate={[required()]} fullWidth />
      <TextInput label={'label.description'} source="description" fullWidth />
      <SelectInput
        label={'label.type'}
        source="type"
        choices={Object.values(CustomFieldTypes).map(type => ({ id: type, name: translate(`value.${type}`) }))}
        onChange={(event) =>
          // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
          setIsList(event.target.value === CustomFieldTypes.Select)
        }
      />
      {isList &&
        <ArrayInput label={'label.options'} source="options">
          <SimpleFormIterator>
            <TextInput label={'label.id'} source="id" validate={[required()]} />
            <TextInput label={'label.name'} source="name" validate={[required()]} />
          </SimpleFormIterator>
        </ArrayInput>}
      <BooleanInput label={'label.required'} source="required" />
      <BooleanInput label={'label.archived'} source="archived" />
    </SimpleForm>
  )
}
