import { act, render, screen, waitFor } from '@testing-library/react'
import { AdminContext, ResourceContextProvider } from 'react-admin'
import customFields from '../../../mocks/customFields'
import { dataProviderForStories } from '../../../utils/dataProviderForStories'
import { CustomFieldEdit } from './CustomFieldEdit'


describe('CustomFields edit test', () => {

  it('should render the edit', async () => {

    //Given
    // eslint-disable-next-line @typescript-eslint/await-thenable
    await act(() => {
      render(
        <AdminContext dataProvider={dataProviderForStories(customFields)} >
          <ResourceContextProvider value="customFields">
            <CustomFieldEdit id={1} />
          </ResourceContextProvider>
        </AdminContext>
      )
    })

    await waitFor(() => expect(screen.getByLabelText('label.name *')).toBeDefined())
    await waitFor(() => expect(screen.getByText('label.description')).toBeDefined())

  })
})
