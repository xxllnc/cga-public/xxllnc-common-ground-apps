import { FC } from 'react'
import {
  Edit,
  EditProps
} from 'react-admin'
import { HeaderTitle } from '../../Header'
import { CustomFieldForm } from '../CustomFieldForm'

export const CustomFieldEdit: FC<EditProps> = (props) => (
  <Edit title={<HeaderTitle title="Eigen veld %name% aanpassen" />} {...props}>
    <CustomFieldForm {...props} />
  </Edit>
)