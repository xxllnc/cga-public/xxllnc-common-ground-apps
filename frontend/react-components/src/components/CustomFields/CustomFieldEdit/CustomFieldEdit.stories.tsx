import { Meta, Story } from '@storybook/react'
import { AdminContext, EditProps, ResourceContextProvider } from 'react-admin'
import customFields from '../../../mocks/customFields'
import { dataProviderForStories } from '../../../utils/dataProviderForStories'
import { CustomFieldEdit } from './CustomFieldEdit'


export default {
  title: 'CustomFields/EditForm',
  component: CustomFieldEdit
} as Meta

export const EditForm: Story<EditProps> = () => (
  <AdminContext dataProvider={dataProviderForStories(customFields)} >
    <ResourceContextProvider value="customFields">
      <CustomFieldEdit id={1} />
    </ResourceContextProvider>
  </AdminContext>
)
