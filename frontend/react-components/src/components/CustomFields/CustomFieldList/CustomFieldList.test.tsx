import { render, screen, waitFor } from '@testing-library/react'
import { Admin, Resource } from 'react-admin'
import customFields from '../../../mocks/customFields'
import { dataProviderForStories } from '../../../utils/dataProviderForStories'
import { CustomFieldList } from './CustomFieldList'


describe('CustomFields list test', () => {

  it('should render the list with custom field rows', async () => {

    // Given
    render(
      <Admin dataProvider={dataProviderForStories(customFields)}>
        <Resource name="callbackRequests" list={CustomFieldList} />
      </Admin>
    )


    await waitFor(() => expect(screen.getByText('label.name')).toBeDefined())

    expect(screen.getByText('label.description')).toBeDefined()
    expect(screen.getByText('label.required')).toBeDefined()

    // We expect 'Onderwerp' to be an item in the list
    expect(screen.getByText('Onderwerp')).toBeDefined()
  })
})
