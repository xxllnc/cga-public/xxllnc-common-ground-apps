import { Meta, Story } from '@storybook/react'
import { Admin, Resource } from 'react-admin'
import customFields from '../../../mocks/customFields'
import { dataProviderForStories } from '../../../utils/dataProviderForStories'
import { CustomFieldList } from './CustomFieldList'


export default {
  title: 'CustomFields/List',
  component: CustomFieldList
} as Meta

export const List: Story = () => (
  <Admin dataProvider={dataProviderForStories(customFields)}>
    <Resource name="callbackRequests" list={CustomFieldList} />
  </Admin>
)

