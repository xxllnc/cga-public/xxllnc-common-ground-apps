import {
  BooleanField, Datagrid, EditButton, FunctionField, List, TextField, useTranslate
} from 'react-admin'
import { CustomFieldRecord } from '../CustomFieldForm'
import { SortOrderField } from '../SortOrderField'


export const CustomFieldList = (): JSX.Element => {
  const translate = useTranslate()

  return (
    <List
      bulkActionButtons={false}
      sort={{ field: 'sortOrder', order: 'ASC' }}
      exporter={false}
    >
      <Datagrid>
        <TextField
          label={'label.name'}
          source="name"
          sortable={false}
        />
        <TextField
          label={'label.description'}
          source="description"
          sx={theme => ({
            '& .RaDatagrid-headerCell .RaDatagrid-rowCell': {
              [theme.breakpoints.down('sm')]: { display: 'none' }
            }
          }
          )}
          sortable={false}
        />
        <FunctionField<CustomFieldRecord>
          label={'label.type'}
          sx={theme => ({
            '& .RaDatagrid-headerCell .RaDatagrid-rowCell': {
              [theme.breakpoints.down('sm')]: { display: 'none' }
            }
          }
          )}
          sortable={false}
          render={record => record && translate(`value.${record.type}`)}
        />
        <BooleanField
          label={'label.required'}
          source="required"
          sortable={false}
        />
        <BooleanField
          label={'label.archived'}
          source="archived"
          sortable={false}
        />
        <SortOrderField
          label={'label.sortOrder'}
          source="sortOrder"
          sortable={false}
        />
        <EditButton />
      </Datagrid>
    </List>
  )
}
