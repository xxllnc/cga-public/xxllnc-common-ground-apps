import {
  Create,
  CreateProps
} from 'react-admin'
import { HeaderTitle } from '../../Header'
import { CustomFieldForm, CustomFieldRecord, WithProps } from '../CustomFieldForm'

export const CustomFieldCreate = (props: CreateProps): JSX.Element => (
  <Create title={<HeaderTitle title="Eigen veld toevoegen" />} {...props}>
    <WithProps>{({ record, ...rest }: {record: CustomFieldRecord}) =>
      <CustomFieldForm record={record} {...rest} />}
    </WithProps>
  </Create>
)
