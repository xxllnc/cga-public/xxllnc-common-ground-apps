import { Story, Meta } from '@storybook/react'
import { CustomFieldCreate } from './CustomFieldCreate'
import { createTheme, ThemeProvider, Theme, StyledEngineProvider } from '@mui/material'
import { AdminContext, CreateProps, ResourceContextProvider } from 'react-admin'
import { dataProviderForStories } from '../../../utils/dataProviderForStories'
import customFields from '../../../mocks/customFields'


export default {
  title: 'CustomFields/Create',
  component: CustomFieldCreate
} as Meta

export const Create: Story = () => (
  <AdminContext dataProvider={dataProviderForStories(customFields)} >
    <ResourceContextProvider value="customFields">
      <CustomFieldCreate />
    </ResourceContextProvider>
  </AdminContext>
)


