import { render, screen } from '@testing-library/react'
import { LicensesInformation } from './LicensesInformation'

export const licensesExample = {
  'easy-peasy@3.3.1': {
    licenses: 'MIT',
    repository: 'https://github.com/ctrlplusb/easy-peasy',
    publisher: 'Sean Matheson',
    email: 'sean@ctrlplusb.com'
  },
  '@mui/material@4.11.3': {
    licenses: 'MIT',
    repository: 'https://github.com/mui-org/material-ui',
    publisher: 'Material-UI Team'
  },
  '@mui/icons-material@4.11.2': {
    licenses: 'MIT',
    repository: 'https://github.com/mui-org/material-ui',
    publisher: 'Material-UI Team'
  },
  'dotenv@8.2.0': {
    licenses: 'BSD-2-Clause',
    repository: 'https://github.com/motdotla/dotenv'
  },
  'react@17.0.1': {
    licenses: 'MIT',
    repository: 'https://github.com/facebook/react'
  },
  'react-leaflet@3.1.0': {
    licenses: 'Hippocratic-2.1',
    repository: 'https://github.com/PaulLeCam/react-leaflet',
    publisher: 'Paul Le Cam',
    email: 'paul@ulem.net'
  },
}

describe('Test LicensesInformation component', () => {

  it('should render the LicensesInformation without license file', () => {

    //Given
    render(<LicensesInformation />)

    expect(screen.getByText('Fout, er is geen licentieinformatie gevonden')).toBeDefined()
  })

  it('should render the LicensesInformation with license file', () => {

    //Given
    render(<LicensesInformation licenses={licensesExample} />)

    expect(screen.getByText('react')).toBeDefined()
    expect(screen.getAllByText('Versie: 17.0.1, Licentie: MIT')).toBeDefined()
  })

})
