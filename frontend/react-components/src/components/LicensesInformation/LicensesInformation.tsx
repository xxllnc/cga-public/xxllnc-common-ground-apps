import { ButtonBase, Grid, Typography } from '@mui/material'
import { styled } from '@mui/material/styles'
import React, { useState } from 'react'
import { GetLicenseForTypeWithUsername } from './GetLicenseForTypeWithUsername'

export interface DetailsOfLicense {
  licenses: string
  repository?: string
  licenseUrl?: string
  publisher?: string
}

interface DetailsProps {
  packageName: string
  packageDetails: DetailsOfLicense
}

export interface LicensesInformationProps {
  licenses?: { [key: string]: DetailsOfLicense }
}

const Img = styled('img')({
  minWidth: '40px',
  width: '40px',
  maxWidth: '40px',
  minHeight: ' 40px',
  height: '40px',
  maxHeight: '40px',
  margin: '0 0.8em 0 0',
  objectFit: 'contain'
})

const TextGrid = styled(Grid)`
  text-align: left;
`

const ExtractNameFromGithubUrl = (url?: string): string | null => {
  if (!url) return null
  // eslint-disable-next-line security/detect-unsafe-regex
  const reg = /((https?:\/\/)?(www\.)?github\.com\/)?(@|#!\/)?([A-Za-z0-9_]{1,15})(\/([-a-z]{1,20}))?/i
  const components = reg.exec(url)
  return (components && components.length > 5) ? components[5] : null

}

const Details = ({ packageName, packageDetails }: DetailsProps): JSX.Element => {

  const [showDetails, setShowDetails] = useState(false)

  const rawName = packageName.startsWith('@') ? packageName.substr(1) : packageName
  const [name, version] = rawName.split('@')
  const username = ExtractNameFromGithubUrl(packageDetails.repository) ||
    ExtractNameFromGithubUrl(packageDetails.licenseUrl) || name

  return (
    <>
      <Grid item xs={6}>
        <ButtonBase onClick={() => setShowDetails(!showDetails)}>
          <Grid container justifyContent="flex-start" alignItems="center">
            <TextGrid item>
              <Img alt="" role="presentation" src={`https://github.com/${username}.png`} />
            </TextGrid>
            <TextGrid item>
              <Typography variant="h5">{name}</Typography>
              <Typography variant="body2" color="textSecondary">
                Versie: {version}, Licentie: {packageDetails.licenses}
              </Typography>
            </TextGrid>
          </Grid>
        </ButtonBase>
      </Grid>
      {showDetails &&
        <Grid item xs={12}>
          <article
            role={'presentation'}
            key={'article'}
          >
            <section>
              <header>
                <Typography variant="h4">{'License'}</Typography>
              </header>
              {GetLicenseForTypeWithUsername(
                { username, licenses: packageDetails.licenses }).split('\n')
                .map((item, i) => <p key={i}>{item}</p>)}
            </section>
          </article>
        </Grid>
      }
    </>
  )
}

export const LicensesInformation: React.FC<LicensesInformationProps> = ({ licenses }): JSX.Element => (
  <Grid container spacing={1}>
    <Grid item xs={12}>
      <Typography variant="h3">Licentie informatie</Typography>
    </Grid>
    {licenses ?
      Object.keys(licenses).map(key =>
        <Details key={key} packageName={key} packageDetails={licenses[String(key)]} />
      )
      :
      <Grid item xs={12}>
        <Typography variant="h3">Fout, er is geen licentieinformatie gevonden</Typography>
      </Grid>
    }
  </Grid>
)

