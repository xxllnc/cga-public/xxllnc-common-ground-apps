import get from 'lodash/get'
import { useRecordContext, useTranslate } from 'ra-core'

interface IHeaderTitle {
  title: string
  [key: string]: any
}
export const HeaderTitle = ({ title }: IHeaderTitle): JSX.Element => {

  const record = useRecordContext()

  // Get all the variables as example %name%
  const regex = /%(.*?)%/g
  const matches = title.match(regex)
  const params: string[] = []
  matches?.forEach((match: string) => params.push(match.replaceAll('%', '')))

  // Change all params in title to the values
  params?.forEach((param: string) => {
    const value: string | undefined = param ? record?.[String(param)] as string || undefined : undefined
    if (param && value) title = title.replace(`%${param}%`, value.toLocaleLowerCase())
  })

  return <span>{title}</span>
}

interface IActionToolbarHeaderTitle {
  source?: string,
  i18n: string,
  className?: string,
  [key: string]: unknown,
}

export const ActionToolbarHeaderTitle = ({ source, i18n, className, ...props }: IActionToolbarHeaderTitle): JSX.Element => {
  const translate = useTranslate()
  const record = useRecordContext(props)
  // eslint-disable-next-line @typescript-eslint/no-unsafe-call
  const getSource = source ? get(record, source) as string : null

  return getSource ?
    <span className={className}>{translate(i18n, { name: getSource })}</span> :
    <span className={className}>{translate(i18n)}</span>
}