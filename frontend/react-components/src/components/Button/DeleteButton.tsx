import {
  DeleteButtonProps,
  DeleteWithConfirmButton,
  DeleteWithUndoButton,
  RaRecord,
  useNotify,
  useRedirect,
  useRefresh
} from 'react-admin'

export const CustomDeleteButton = (
  {
    redirect: redirectTo = 'list',
    resource,
    mutationMode,
    record,
    ...rest
  }: DeleteButtonProps<RaRecord>): JSX.Element | null => {
  const notify = useNotify()
  const redirectF = useRedirect()
  const refresh = useRefresh()

  if (!record || record.id == null) {
    return null
  }

  const onSuccess = () => {
    notify(
      'ra.notification.deleted',
      // eslint-disable-next-line camelcase
      { type: 'info', undoable: true, messageArgs: { smart_count: 1 } }
    )
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    redirectF(redirectTo, `/${resource ? resource : ''}`)
    setTimeout(() => refresh(), 0.1)
  }

  return mutationMode === 'undoable' ? (
    <DeleteWithUndoButton
      record={record}
      mutationOptions={{ onSuccess }}
      {...rest} />
  ) : (
    <DeleteWithConfirmButton
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      mutationMode={mutationMode}
      record={record}
      mutationOptions={{ onSuccess }}
      {...rest}
    />
  )
}