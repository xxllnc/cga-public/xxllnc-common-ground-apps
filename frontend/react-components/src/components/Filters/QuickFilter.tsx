import { Chip } from '@mui/material'
import { FC } from 'react'
import { InputProps } from 'react-admin'

interface QuickFilterProps extends InputProps {
  label: string
}

export const QuickFilter: FC<QuickFilterProps> = ({ label }) => (
  <Chip label={label} sx={(theme) => ({ marginBottom: theme.spacing(2) })} />
)