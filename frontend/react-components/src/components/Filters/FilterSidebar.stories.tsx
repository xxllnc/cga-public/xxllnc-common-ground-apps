import { Meta, Story } from '@storybook/react'
import { FC } from 'react'
import { Admin, LayoutProps, Resource, useResourceDefinitions, useSidebarState } from 'react-admin'
import { CustomLayout, theme } from '../../layout'
import { dataProviderForStories } from '../../utils'
import { CustomFieldListGuesser } from '../CustomFields'
import { CreatedAtFilter, CreatedByAssignedToMeFilter } from './Filters'
import { FilterSidebar } from './FilterSidebar'

export default {
  title: 'FilterSidebar'
} as Meta

const TestFilterSidebar: FC = () => (
  <FilterSidebar>
    <CreatedByAssignedToMeFilter showAssignedToMe={false} />
    <CreatedAtFilter />
  </FilterSidebar>
)

const Layout: FC<LayoutProps> = (props) => {
  const [open] = useSidebarState() // Replaces useSelector for sidebarOpen
  const resourcesDefinitions = useResourceDefinitions() // Replaces useSelector for getResources
  const resources = Object.keys(resourcesDefinitions).map(name => resourcesDefinitions[String(name)])

  return (
    <CustomLayout {...props} appName="Storybook app" resources={resources} open={open} />
  )
}

const ListGuesserWithSidebar = (props) => (
  <CustomFieldListGuesser {...props} aside={<TestFilterSidebar />} />
)


export const FilterSidebarStory: Story = () => (
  <Admin dataProvider={dataProviderForStories()} theme={theme} layout={Layout}>
    <Resource name="callbackRequests" list={ListGuesserWithSidebar} />
  </Admin>
)
