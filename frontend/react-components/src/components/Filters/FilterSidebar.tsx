import { Card, CardContent } from '@mui/material'
import { ReactNode } from 'react'

export const FilterSidebar = ({ children }: { children: ReactNode }): JSX.Element => (
  <Card
    sx={{
      display: {
        xs: 'none',
        md: 'block',
      },
      order: -1,
      flex: '0 0 15em',
      mr: 2,
      mt: 8,
      alignSelf: 'flex-start',
    }}
  >
    <CardContent sx={{ pt: 1 }}>
      {children}
    </CardContent>
  </Card>
)
