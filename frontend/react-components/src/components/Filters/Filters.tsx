/* eslint-disable camelcase */
import {
  FilterList,
  FilterListItem,
} from 'react-admin'
import TodayIcon from '@mui/icons-material/Today'
import FilterListIcon from '@mui/icons-material/FilterList'
import {
  startOfToday,
  startOfWeek,
  subWeeks,
  startOfMonth,
  subMonths,
  endOfToday,
  endOfWeek,
  endOfMonth,
} from 'date-fns'

export const CreatedAtFilter = (): JSX.Element => (
  <FilterList label="Periode" icon={<TodayIcon />}>
    <FilterListItem
      label="Vandaag"
      value={{
        createdAt_gte: startOfToday().toISOString(),
        createdAt_lte: endOfToday().toISOString(),
      }}
    />
    <FilterListItem
      label="Deze week"
      value={{
        createdAt_gte: startOfWeek(new Date()).toISOString(),
        createdAt_lte: endOfWeek(new Date()).toISOString(),
      }}
    />
    <FilterListItem
      label="Vorige week"
      value={{
        createdAt_gte: subWeeks(startOfWeek(new Date()), 1).toISOString(),
        createdAt_lte: startOfWeek(new Date()).toISOString(),
      }}
    />
    <FilterListItem
      label="Deze maand"
      value={{
        createdAt_gte: startOfMonth(new Date()).toISOString(),
        createdAt_lte: endOfMonth(new Date()).toISOString(),
      }}
    />
    <FilterListItem
      label="Vorige maand"
      value={{
        createdAt_gte: subMonths(startOfMonth(new Date()), 1).toISOString(),
        createdAt_lte: subMonths(endOfMonth(new Date()), 1).toISOString(),
      }}
    />
    <FilterListItem
      label="Eerder"
      value={{
        createdAt_gte: undefined,
        createdAt_lte: subMonths(endOfMonth(new Date()), 2).toISOString(),
      }}
    />
  </FilterList>)

export const CreatedByAssignedToMeFilter = ({ showAssignedToMe = true }: { showAssignedToMe?: boolean }): JSX.Element => (
  <FilterList label="Filters" icon={<FilterListIcon />}>
    <FilterListItem
      label="Door mij aangemaakt"
      value={{
        createdByMe: true
      }}
    />
    {showAssignedToMe &&
      <FilterListItem
        label="Aan mij toegewezen"
        value={{
          assignedToMe: true
        }}
      />
    }
  </FilterList>
)
