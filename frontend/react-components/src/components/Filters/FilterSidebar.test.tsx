import { Theme } from '@mui/material'
import { render, screen, waitFor } from '@testing-library/react'
import { FC, ReactElement } from 'react'
import { Admin, Datagrid, List, Resource, TextField } from 'react-admin'
import { dataProviderForStories } from '../../utils'
import { CreatedAtFilter, CreatedByAssignedToMeFilter } from './Filters'
import { FilterSidebar } from './FilterSidebar'


const Wrapper: FC<{ aside?: ReactElement }> = ({ aside }) => {
  const ListWithSidebar = () => (
    <List emptyWhileLoading aside={aside}>
      <Datagrid>
        <TextField source="id" />
      </Datagrid>
    </List>
  )

  return (
    <Admin dataProvider={dataProviderForStories()}>
      <Resource name="testFilterSideBar" list={ListWithSidebar} />
    </Admin>
  )
}

describe('FilterSidebar and Filters test', () => {

  it('should render the FilterSideBar with children', async () => {

    const TestFilterSidebar: FC = () => (
      <FilterSidebar>
        <p>child 1</p>
        <p>child 2</p>
      </FilterSidebar>
    )

    // Given
    render(<Wrapper aside={<TestFilterSidebar />} />)

    // Then
    await waitFor(() => expect(screen.getByText('child 1')).toBeDefined())
    expect(screen.getByText('child 2')).toBeDefined()
  })

  it('should render the FilterSideBar with filters in a list context', async () => {

    const defaultListProps = {
      basePath: '/customFields',
      resource: 'customFields'
    }

    const TestFilterSidebar: FC = () => (
      <FilterSidebar {...defaultListProps}>
        <CreatedByAssignedToMeFilter />
        <CreatedAtFilter />
      </FilterSidebar>
    )

    // Given
    render(<Wrapper aside={<TestFilterSidebar />} />)

    // Then
    await waitFor(() => expect(screen.getByText('Door mij aangemaakt')).toBeDefined())
    expect(screen.getByText('Aan mij toegewezen')).toBeDefined()
    expect(screen.getByText('Periode')).toBeDefined()
  })

  it('should render the FilterSideBar with filters in a list context without Assigned to me', async () => {

    const TestFilterSidebar = (): JSX.Element => (
      <FilterSidebar>
        <CreatedByAssignedToMeFilter showAssignedToMe={false} />
        <CreatedAtFilter />
      </FilterSidebar>
    )

    // Given
    render(<Wrapper aside={<TestFilterSidebar />} />)

    await waitFor(() => expect(screen.getByText('Door mij aangemaakt')).toBeDefined())
    expect(screen.findByText('Aan mij toegewezen')).toMatchObject({})
    expect(screen.getByText('Periode')).toBeDefined()
  })

})
