import { createTheme, ThemeProvider, Theme, StyledEngineProvider } from '@mui/material'
import { render, screen } from '@testing-library/react'
import { QuickFilter } from './QuickFilter'

declare module '@mui/styles/defaultTheme' {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface DefaultTheme extends Theme {}
}

describe('QuickFilter test', () => {

  it('should render a chip when quickFilter is added', () => {
    const baseTheme = createTheme()

    // Given
    render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={baseTheme}>
          <QuickFilter label='TestQuickFilter' source='test' defaultValue='test' />
        </ThemeProvider>
      </StyledEngineProvider>
    )

    // We expect the text 'TestQuickFilter' to be on the page
    expect(screen.getByText('TestQuickFilter')).toBeDefined()
  })

})
