/* eslint-disable @typescript-eslint/no-misused-promises */
import { FC, MouseEventHandler, PropsWithChildren } from 'react'
import { Button, useTranslate } from 'react-admin'
// import { Form } from 'react-final-form'
// import { Config } from 'final-form'
import { DialogProps, Dialog, DialogTitle, DialogContent, DialogActions } from '@mui/material'
import { useForm } from 'react-hook-form'
/*
thanks to : https://www.tutorialguruji.com/react-js/how-to-add-a-form-input-on-a-dialog-from-show-view-with-react-admin/
*/

export interface DialogFormProps {
  open: DialogProps['open'],
  loading?: boolean
  // onSubmit: Config['onSubmit'],
  onCancel: MouseEventHandler,
  title?: string,
  submitLabel?: string,
  cancelLabel?: string
}

export const DialogForm: FC<PropsWithChildren<DialogFormProps>> = ({
  open,
  loading,
  // onSubmit,
  onCancel,
  title,
  cancelLabel,
  submitLabel,
  children,
}) => {
  const translate = useTranslate()
  const { register, handleSubmit } = useForm()
  const onSubmit = data => console.log(data)

  // TODO: silence error, provide register to children
  // <input defaultValue="test" {...register("example")} />
  // https://react-hook-form.com/get-started
  // Also, change onSubmit
  return (
    <Dialog
      open={open}
    >
      <form onSubmit={handleSubmit(onSubmit)}>
        {title && (
          <DialogTitle>
            {title}
          </DialogTitle>
        )}
        <DialogContent>
          {children}
        </DialogContent>
        <DialogActions>
          <Button
            label={cancelLabel || translate('cancel')}
            onClick={onCancel}
            disabled={loading}
          />
          <Button
            label={submitLabel || translate('submit')}
            type="submit"
            disabled={loading}
          />
        </DialogActions>
      </form>
    </Dialog>
  )
}

export default DialogForm
