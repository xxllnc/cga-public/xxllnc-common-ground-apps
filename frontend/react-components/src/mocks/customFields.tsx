import { RaRecord } from 'react-admin'
import { CustomField, CustomFieldTypes } from '../types/callbackRequests'

const customFields: RaRecord[] = [
  {
    uuid: '5586dde0-c9da-4df7-a6fa-568be4c5f6bd',
    id: 1,
    name: 'Onderwerp',
    description: 'Invoerveld voor het onderwerp',
    required: true,
    type: CustomFieldTypes.Text,
    sortOrder: 1,
    archived: true
  },
  {
    uuid: '62456c6c-fde4-49d8-a939-c620b7df2f36',
    id: 2,
    name: 'Samenvatting',
    description: 'Invoerveld voor de samenvatting',
    required: false,
    type: CustomFieldTypes.Text,
    sortOrder: 2,
    archived: false
  },
  {
    uuid: '74ca9e88-8d6f-41a3-b29b-cd7dec234f69',
    id: 3,
    name: 'Kanaal',
    description: 'Invoerveld voor het kanaal',
    required: false,
    type: CustomFieldTypes.Text,
    sortOrder: 3,
    archived: false
  }
]

export default customFields