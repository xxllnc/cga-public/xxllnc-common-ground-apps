
import mockDataProvider from './data'

const providers = {
  data: mockDataProvider
}

export default providers
