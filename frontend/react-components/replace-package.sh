#!/bin/bash
# Check if another instance of script is running
script_name=$(basename -- "$0")
pid=$(pidof -x "$script_name" -o $$)
if pidof -x "$script_name" -o $$ >/dev/null;then
   kill -15 $pid
fi
# Get the selected app
CGA_APP=$1

# Build the /dist dir
yarn prepublishOnly
echo -e "Build done"

# Replace dist in the thosen app
# Remove dist
rm -R ../$CGA_APP/src/node_modules/xxllnc-react-components/dist
rm -R ../$CGA_APP/node_modules/xxllnc-react-components/dist

# Copy to app/src/node_modules/xxllnc-react-components/dist
cp -R dist ../$CGA_APP/src/node_modules/xxllnc-react-components/dist
cp -R dist ../$CGA_APP/node_modules/xxllnc-react-components/dist
cp -R package.json ../$CGA_APP/src/node_modules/xxllnc-react-components/package.json
cp -R README.md ../$CGA_APP/src/node_modules/xxllnc-react-components/README.md
cp -R LICENSE ../$CGA_APP/src/node_modules/xxllnc-react-components/LICENSE

echo "Continue Watching \"src\""