import commonjs from '@rollup/plugin-commonjs'
import resolve from '@rollup/plugin-node-resolve'
import typescript from '@rollup/plugin-typescript'
import copy from 'rollup-plugin-copy'
import dts from 'rollup-plugin-dts'
import peerDepsExternal from 'rollup-plugin-peer-deps-external'
import postcss from 'rollup-plugin-postcss'
import { terser } from 'rollup-plugin-terser'
import packageJson from './package.json' assert { type: "json" }

export default [
  {
    input: 'src/index.ts',
    output: [
      {
        file: packageJson.main,
        format: 'cjs',
        sourcemap: true,
        name: 'xxllnc-react-components',
      },
      {
        file: packageJson.module,
        format: 'esm',
        sourcemap: true,
      },
    ],
    external: ['ra-core'],
    plugins: [
      peerDepsExternal(),
      resolve(),
      typescript({
        tsconfig: './tsconfig.json',
        exclude: [
          '**/*.stories.tsx',
          '**/*.test.ts',
          '**/*.test.tsx',
          '**/*.scss',
          '**/mocks/**',
          '**/providers/**'
        ]
      }),
      commonjs(),
      postcss(),
      copy({
        targets: [
          {
            src: ['src/components/**/*.scss', '!**/*.module.scss'],
            dest: 'dist/types/components',
            rename: (name, extension) => `${name}/${name}.${extension}`
          }
        ],
        verbose: true
      }),
      terser(),
    ],
  },
  {
    input: 'dist/esm/index.d.ts',
    output: [{ file: 'dist/index.d.ts', format: 'esm' }],
    external: [/\.scss$/],
    plugins: [dts()],
  },
]
