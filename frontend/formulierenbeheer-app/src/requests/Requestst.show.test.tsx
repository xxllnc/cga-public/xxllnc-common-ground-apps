import { render, screen, waitFor } from '@testing-library/react'
import { AdminContext, ResourceContextProvider } from 'react-admin'
import providers from '../providers'
import RequestsShow from './Requests.show'


describe('Requests show test', () => {

  it('should render the show', async () => {

    const dataProviderMock = providers.data

    //Given
    render(
      <AdminContext dataProvider={dataProviderMock} >
        <ResourceContextProvider value="requests">
          <RequestsShow id={'009b58ae-9a31-4726-ae7c-20c3463fa6ae'} children={[]} />
        </ResourceContextProvider>
      </AdminContext>
    )

    // We expect 'Id*' as required label for one input field
    await waitFor(() => expect(screen.getByText('56b7cfe0-c9c5-4f47-aec5-4ce5e7151482')).toBeDefined())
  })
})
