import { FC } from 'react'
import {
  TabbedShowLayout,
  Tab,
  ShowProps,
  Show,
  SimpleShowLayout,
  TextField,
  DateField,
  SelectField
} from 'react-admin'
import { Grid } from '@mui/material'
import { Status } from '../types/generatedFormTypes'
import RequestDetails from './Request.details'
import { RequestsShowActions } from './Requests.actions'

const RequestsShow: FC<ShowProps> = (props) => (
  <Show
    {...props}
    actions={<RequestsShowActions />}
  >
    <TabbedShowLayout>
      <Tab label="show.details">
        <Grid container>
          <Grid item xs={6}>
            <SimpleShowLayout>
              <TextField label="Id" source="formUuid" />
              <TextField label="requestPage.organization" source="organization" />
              <TextField label="requestPage.submitter" source="submitter" />
              <SelectField label={'requestPage.status'} source="status" choices={[
                { id: Status.ACTIVE, name: 'Actief' },
                { id: Status.INACTIVE, name: 'Inactief' }
              ]} />
              <DateField label="requestPage.date" source="registrationDate" />
              <RequestDetails/>
            </SimpleShowLayout>
          </Grid>
        </Grid>
      </Tab>
    </TabbedShowLayout>
  </Show>
)
export default RequestsShow