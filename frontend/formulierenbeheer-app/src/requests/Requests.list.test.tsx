import { render, screen, waitFor } from '@testing-library/react'
import { AdminContext, ResourceContextProvider } from 'react-admin'
import { useMediaQuery } from '@mui/material'
import providers from '../providers'
import RequestList from './Requests.list'

describe('requests list test', () => {
  it('should render the list with requests', async () => {

    const dataProviderMock = providers.data

    // Given
    render(
      <AdminContext dataProvider={dataProviderMock} >
        <ResourceContextProvider value="requests">
          <RequestList resource='requests'><></></RequestList>
        </ResourceContextProvider>
      </AdminContext>
    )

    await waitFor(() => expect(screen.getAllByText('Actief')).toHaveLength(2))
  })
  it('should render asimple list when the device is small', async () => {
    const mui = { useMediaQuery }
    jest.spyOn(mui, 'useMediaQuery').mockReturnValue(true)
    const dataProviderMock = providers.data

    // Given
    render(
      <AdminContext dataProvider={dataProviderMock} >
        <ResourceContextProvider value="requests">
          <RequestList resource='requests'><></></RequestList>
        </ResourceContextProvider>
      </AdminContext>
    )

    await waitFor(() => expect(screen.getAllByText('Actief')).toHaveLength(2))
  })
})
