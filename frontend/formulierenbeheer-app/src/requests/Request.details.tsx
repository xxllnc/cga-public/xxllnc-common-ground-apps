import { FC } from 'react'
import { Paper, Grid, Theme } from '@mui/material'
import makeStyles from '@mui/styles/makeStyles'
import createStyles from '@mui/styles/createStyles'
import { TextFieldProps, useRecordContext, useTranslate } from 'react-admin'
import { getObjectProperties, getReadableKey } from '../utils'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      color: theme.palette.text.secondary,
    },
    contentGrid: {
      padding: '0 12px!important'
    },
    content: {
      marginBottom: 7
    }
  }),
)

const RequestDetails: FC<TextFieldProps> = (props) => {
  const classes = useStyles()
  const translate = useTranslate()
  const record = useRecordContext()
  const properties = getObjectProperties(record.formInput)

  return properties?.length > 0 ?
    <div className={classes.root}>
      <Grid container spacing={0}>
        <Grid item xs={12}>
          <h5>{translate('requestPage.content')}</h5>
        </Grid>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <Grid container spacing={3}>
              {properties.map(object =>
                <Grid key={object.key} item xs={12} sm={6} md={4} lg={3} className={classes.contentGrid}>
                  <p className={classes.content}>{getReadableKey(object.key) + ': ' + object.value.toString()}</p>
                </Grid>
              )}
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </div> : null
}

export default RequestDetails