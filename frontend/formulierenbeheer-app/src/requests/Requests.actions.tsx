import { FC } from 'react'
import { ActionsToolbar } from '../components'

export const RequestsShowActions: FC = () => (
  <ActionsToolbar source="requests/admin" to="/requests/admin"/>
)
