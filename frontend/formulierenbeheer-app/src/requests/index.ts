import PostAddIcon from '@mui/icons-material/PostAdd'
import { CustomResourceProps } from '../Resources'
import RequestsList from './Requests.list'
import RequestsShow from './Requests.show'

const Requests: CustomResourceProps = {
  name: 'requests/admin',
  list: RequestsList,
  show: RequestsShow,
  icon: PostAddIcon,
  options: {
    label: 'requests',
    scopes: ['cga.admin', 'cga.user']
  }
}

export default Requests
