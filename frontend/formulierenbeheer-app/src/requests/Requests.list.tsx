import { FC } from 'react'
import {
  useLocale,
  Datagrid,
  DateField,
  List,
  ListProps,
  SelectField,
  TextField,
  SimpleList,
  ShowButton,
  useTranslate,
  RaRecord
} from 'react-admin'
import { useAuthorized } from 'xxllnc-react-components'
import { Theme, useMediaQuery } from '@mui/material'
import { Status } from '../types/generatedFormTypes'

const FormList: FC<ListProps> = (props) => {
  useAuthorized()

  const translate = useTranslate()
  const isXSmall = useMediaQuery<Theme>(theme => theme.breakpoints.down('md'))
  const locale = useLocale()

  console.log({ isXSmall })

  return (
    <List {...props}
      title="requestPage.title"
      exporter={false}
      empty={false}
    >
      {isXSmall ? (
        <SimpleList
          hasBulkActions={false}
          primaryText={(record: RaRecord) => `${record.submitter ?? ''} `}
          secondaryText={(record: RaRecord) =>
            `${translate('requestPage.date')}: ${record.registrationDate ?
              new Date((record as unknown as { registrationDate: Date }).registrationDate).toLocaleDateString() : '-'}`
          }
          tertiaryText={(record: RaRecord) => `${record.status}`}
        />
      ) : (
        <Datagrid bulkActionButtons={false}>
          <TextField label={'requestPage.submitter'} source="submitter" />
          <SelectField label={'requestPage.status'} source="status" choices={[
            { id: Status.ACTIVE, name: 'Actief' },
            { id: Status.INACTIVE, name: 'Inactief' }
          ]} />
          <DateField label={'requestPage.date'} source="registrationDate" locales={locale} />
          <ShowButton />
        </Datagrid>
      )}
    </List>
  )
}

export default FormList
