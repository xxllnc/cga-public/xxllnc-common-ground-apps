import { ImageService } from './ImageService'
import { resourcesForScopes } from './Resources'
import actualLogo from './assets/images/logo.jpg'
import App from './App'
import { render, screen } from '@testing-library/react'
import * as components from 'xxllnc-react-components'

describe('Source root test', () => {

  it('should return resources', () => {

    const resources = resourcesForScopes({ scopes: ['cga.admin', 'cga.user'] })

    expect(resources.length).toBe(8)

  })

  it('should return no resources when there are no scopes', () => {

    const resources = resourcesForScopes({ scopes: [] })

    expect(resources.length).toBe(1)

  })

  it('should return no resources when there are unknown scopes', () => {

    const resources = resourcesForScopes({ scopes: ['something_else'] })

    expect(resources.length).toBe(1)

  })

  it('should return ImageService', () => {
    const logo = ImageService.logo

    expect(logo).toBe(actualLogo)

  })

  // TODO: Fix test CGA-3643
  it.skip('should render App', () => {
    jest.spyOn(components, 'Auth0ProviderWithHistory').mockImplementation(({ children }) => <div>{children}</div>)

    render(
      <App />
    )

    expect(screen.getAllByRole('generic')).toHaveLength(2)

  })
})