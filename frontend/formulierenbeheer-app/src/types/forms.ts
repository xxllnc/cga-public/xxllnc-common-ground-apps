import { FormWithDetails as GeneratedForm } from './generatedFormTypes'

export interface FormData {
  title: string
  name?: string
  [key: string]: string | string[] | unknown
  components: {
    key: string
    ignore: boolean
    [key: string]: unknown
  }[]
}

export interface Form extends Omit<GeneratedForm, 'form' | 'config'> {
  form: FormData
  config?: { key: string, value: string }[]
  [key: string]: unknown
}

export interface Id {
  id: string
  uuid: string
}

export interface Layout extends Id {
  name: string
  primaryColour: string
  secondaryColour: string
  letterType: string
  colourLink: string
  logo: string
}

export interface GeneralSetting extends Id {
  name: string
  value: string
  description: string
  regex: string
  settingTypeId: number
  lastModified: string
}

export interface SettingType extends Id {
  name: string
  description: string
}

