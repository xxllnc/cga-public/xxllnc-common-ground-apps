/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface ActiveFormFields {
  /** The slug of the form */
  slug: string;

  /** The name of the form */
  name: string;

  /**
   * The unique uuid of the form instance
   * @format uuid
   */
  id: string;
}

/**
 * Shows the details of a Form, is used in the response of get, put and post
 */
export interface Form {
  /** The config of the form */
  config?: object[];

  /**
   * The unique uuid of the organization
   * @format uuid
   */
  organizationUuid: string;

  /** The name of the organization */
  organizationName: string;

  /** The slug of the form */
  slug: string;

  /** The name of the form */
  name: string;

  /** The status of the form */
  status: Status;

  /** The form of the form */
  rights?: Rights;

  /**
   * The publish date of the form
   * @format date
   */
  publishDate?: string;

  /**
   * The unique uuid of the form instance
   * @format uuid
   */
  id: string;
}

export interface FormAddShareRequest {
  /** The config of the form */
  config?: object[];

  /**
   * The unique uuid of the organization
   * @format uuid
   */
  organizationUuid: string;

  /** The name of the organization */
  organizationName: string;

  /**
   * The unique uuid of the form instance
   * @format uuid
   */
  id: string;

  /** The slug of the form */
  slug?: string;

  /** The name of the form */
  name?: string;

  /** The status of the form */
  status?: Status;

  /** The form of the form */
  rights?: Rights;

  /**
   * The publish date of the form
   * @format date
   */
  publishDate?: string;
}

/**
 * class to validate request input to create form
 */
export interface FormCreateRequest {
  /** The config of the form */
  config?: object[];

  /** The form contents containing the pages */
  form: object;

  /** Indicates if form is public or private */
  private?: boolean;

  /** The name of the form */
  name: string;

  /** The status of the form */
  status: Status;

  /** The form of the form */
  rights?: Rights;

  /**
   * The publish date of the form
   * @format date
   */
  publishDate?: string;
}

export interface FormGetOneResponseDetails {
  /** The form contents containing the pages */
  form: object;

  /** Indicates if form is public or private */
  private?: boolean;

  /** The slug of the form */
  slug: string;

  /** The name of the form */
  name: string;

  /**
   * The unique uuid of the form instance
   * @format uuid
   */
  id: string;

  /** All the organisations the form is shared with */
  shares?: Form[];
}

/**
 * Make fields optional to support partial update (patch)
 */
export interface FormUpdateRequest {
  /** The config of the form */
  config?: object[];

  /** The form contents containing the pages */
  form?: object;

  /** Indicates if form is public or private */
  private?: boolean;

  /** The slug of the form */
  slug?: string;

  /** The name of the form */
  name?: string;

  /** The status of the form */
  status?: Status;

  /** The form of the form */
  rights?: Rights;

  /**
   * The publish date of the form
   * @format date
   */
  publishDate?: string;
}

export interface FormUpdateShareRequest {
  /** The config of the form */
  config?: object[];

  /** The slug of the form */
  slug?: string;

  /** The name of the form */
  name?: string;

  /** The status of the form */
  status?: Status;

  /** The form of the form */
  rights?: Rights;

  /**
   * The publish date of the form
   * @format date
   */
  publishDate?: string;
}

/**
 * Also add the form and public element
 */
export interface FormWithDetails {
  /** The form contents containing the pages */
  form: object;

  /** Indicates if form is public or private */
  private?: boolean;

  /** The config of the form */
  config?: object[];

  /**
   * The unique uuid of the organization
   * @format uuid
   */
  organizationUuid: string;

  /** The name of the organization */
  organizationName: string;

  /** The slug of the form */
  slug: string;

  /** The name of the form */
  name: string;

  /** The status of the form */
  status: Status;

  /** The form of the form */
  rights?: Rights;

  /**
   * The publish date of the form
   * @format date
   */
  publishDate?: string;

  /**
   * The unique uuid of the form instance
   * @format uuid
   */
  id: string;
}

export interface HTTPValidationError {
  /** Detail */
  detail?: ValidationError[];
}

export interface OrganizationReturn {
  /** The name of the organization */
  name: string;

  /**
   * The unique id of the organization
   * @format uuid
   */
  id: string;

  /** The urls of the organization */
  urls: Url[];
}

export interface OrganizationUpdate {
  /** The name of the organization */
  name?: string;
}

/**
 * An enumeration.
 */
export enum Rights {
  VIEWER = "VIEWER",
  EDITOR = "EDITOR",
  OWNER = "OWNER",
}

/**
 * An enumeration.
 */
export enum Status {
  ACTIVE = "ACTIVE",
  INACTIVE = "INACTIVE",
}

export interface Url {
  /** An url of the organization */
  url: string;

  /** if this is the primary url */
  primary: boolean;

  /** the id of the organisatoin */
  organizationId: number;

  /** The unique id of the url */
  id: number;

  /**
   * The unique uuid of the url
   * @format uuid
   */
  uuid: string;
}

export interface UrlUpdate {
  /** An url of the organization */
  url?: string;

  /** if this is the primary url */
  primary?: boolean;

  /** the id of the organisatoin */
  organizationId?: number;
}

export interface ValidationError {
  /** Location */
  loc: string[];

  /** Message */
  msg: string;

  /** Error Type */
  type: string;
}
