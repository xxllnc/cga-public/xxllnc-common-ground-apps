import { FC } from 'react'
import { Admin as ReactAdmin, CustomRoutes } from 'react-admin'
import { Route } from 'react-router-dom'
import { getEnvVariable, theme, useAuth0User, CustomLayout } from 'xxllnc-react-components'
import FormEditor from './forms/formioeditor/FormEditor'
import AboutPage from './pages/about/About'
import LoginPage from './pages/login/Login'
import providers from './providers'
import { resourcesForScopes } from './Resources'

const returnTo = `${window.location.origin}${process.env.PUBLIC_URL}/login`
const mock = () => getEnvVariable('REACT_APP_MOCK_REQUESTS').toUpperCase() === 'TRUE'

// TODO: Remove ts-ignore for theme={theme}
// We need to ignore this line because the version difference of mui in dependencies en ra-admin
// causes a typescript error. When ra-admin upgrades from 5.0.2 to 5.9.2 we can remove the ts-ignore

const Admin: FC = () => {
  const auth0 = useAuth0User()

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  return (<ReactAdmin
    i18nProvider={providers.i18n}
    authProvider={providers.auth(auth0, returnTo, mock())}
    dataProvider={providers.data}
    loginPage={LoginPage}
    layout={CustomLayout}
    disableTelemetry
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    theme={theme}
  >
    {resourcesForScopes}
    <CustomRoutes>
      <Route path='/formioeditor' element={<FormEditor />} />,
      <Route path="/about" element={<AboutPage />} />
    </CustomRoutes>
  </ReactAdmin>
  )
}

export default Admin
