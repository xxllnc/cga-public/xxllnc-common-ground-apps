import { FC } from 'react'
import {
  Create,
  CreateProps,
  DateInput,
  RadioButtonGroupInput,
  SimpleForm,
  TextInput,
  maxLength,
  minLength,
  required,
  useNotify,
  useRefresh,
  useRedirect,
  useGetList,
  SelectInput,
  FormDataConsumer,
  BooleanInput,
  RaRecord,
  useRecordContext
} from 'react-admin'
import { HeaderTitle } from 'xxllnc-react-components'
import formData from './formData'
import { ErrorType, getErrorMessage } from '../utils/getErrorMessage'
import { Form, FormData } from '../types/forms'
import { Status } from '../types/generatedFormTypes'

export interface CreateForm extends RaRecord {
  form?: unknown
  name?: string
  publishDate?: string
  status?: string
  sourceForm?: Form | null
}

const FormCreate: FC<CreateProps> = (props) => {
  const notify = useNotify()
  const refresh = useRefresh()
  const redirect = useRedirect()
  const dateTime = new Date()
  const currentDateString = `${dateTime.getFullYear()}-${dateTime.getMonth() + 1}-${dateTime.getDate()}`

  const { data: forms } = useGetList<Form>('forms')

  const onSuccess = () => {
    notify('Formulier is toegevoegd', { type: 'info' })
    redirect('list')
    refresh()
  }

  const onFailure = (error: unknown) => {
    notify(`Er is een fout opgetreden: ${getErrorMessage(error as ErrorType)}`, { type: 'error' })
  }

  const getSourceForm = (sourceForm: Form | null | undefined, title: string): FormData => {
    if (!sourceForm) return ({ ...formData, title })
    return ({ ...sourceForm.form, title })
  }

  const transform = (form: CreateForm) => {
    if (form.status === 'INACTIVE') delete form.publishDate
    const formContent = form?.form !== undefined ? form : getSourceForm(form.sourceForm, form.name ?? '')
    return ({ ...form, form: form?.form || formContent || '' })
  }

  const FormSource: FC = () => {
    const record = useRecordContext()
    if (record)
      return <p>{`* Clone van formulier "${record.name}". Hernoem het formulier om het op te kunnen slaan.`}</p>

    if (!forms)
      return null

    return forms && <SelectInput
      source="sourceForm"
      label="formSource"
      choices={forms}
      optionValue="id"
      format={(form: Form) => form?.id || ''}
      parse={id => forms.find(form => form.id === id)}
    />
  }

  return (
    <Create
      {...props}
      title={<HeaderTitle title="Formulier toevoegen" />}
      mutationOptions={{
        onSuccess,
        onError: onFailure
      }}
      transform={transform}
    >
      <SimpleForm defaultValues={{ status: Status.INACTIVE }} >
        <TextInput
          label="Naam"
          source="name"
          validate={[required(), minLength(1), maxLength(255)]}
        />
        <FormSource />
        <TextInput
          label="Formulier"
          source="form"
          fullWidth
          multiline
          hidden
        />
        <RadioButtonGroupInput
          label="Status"
          source="status"
          choices={[
            { id: Status.ACTIVE, name: 'Actief' },
            { id: Status.INACTIVE, name: 'Inactief' },
          ]}
        />
        <FormDataConsumer>
          {({ formData: localFormDate, ...rest }: { formData: { status: string } }) =>
            localFormDate.status === Status.ACTIVE &&
            <DateInput
              label="Publicatiedatum"
              source="publishDate"
              defaultValue={currentDateString}
              {...rest}
            />
          }
        </FormDataConsumer>
        <BooleanInput
          label="Privé"
          source="private"
          defaultValue={true}
          fullWidth
          helperText={'Een openbaar formulier kan door andere organisaties ingezien en gekopieerd worden. ' +
            'Zo kunnen we samen eenvoudig formulieren maken. Een openbaar formulier kan alleen door de eigenaar bewerkt worden. ' +
            'Als u andere organisaties schrijf rechten wil geven kunt u het formulier na het aanmaken delen met specifieke organisaties.'}
        />
      </SimpleForm>
    </Create>
  )
}

export default FormCreate
