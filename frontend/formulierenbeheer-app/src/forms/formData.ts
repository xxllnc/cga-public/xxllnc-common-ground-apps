import { FormData } from '../types/forms'

const formData: FormData = {
  display: 'wizard',
  title: 'formtitel',
  type: [
    'form'
  ],
  path: 'formpath',
  components: []
}

export default formData
