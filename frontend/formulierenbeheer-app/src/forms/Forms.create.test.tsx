import { act, fireEvent, render, screen, waitFor } from '@testing-library/react'
import { AdminContext, ResourceContextProvider } from 'react-admin'
import providers from '../providers'
import { Status } from '../types/generatedFormTypes'
import FormCreate from './Forms.create'


describe('Forms create test', () => {

  it('should render the create', async () => {

    const dataProviderMock = providers.data

    //Given
    act(() => {
      render(
        <AdminContext dataProvider={dataProviderMock} >
          <ResourceContextProvider value="forms">
            <FormCreate />
          </ResourceContextProvider>
        </AdminContext>
      )
    })

    await waitFor(() => expect(screen.getByLabelText('Naam *')).toBeDefined())
    await waitFor(() => expect(screen.queryByText('Publicatiedatum')).toBeNull())
    await waitFor(() => expect(screen.getByText('Status')).toBeDefined())

    const inputFieldActive: HTMLInputElement = screen.getByLabelText('Actief')
    act(() => { fireEvent.click(inputFieldActive) })
    await waitFor(() => expect(inputFieldActive.value).toBe(Status.ACTIVE))

    const inputFieldInActive: HTMLInputElement = screen.getByLabelText('Inactief')
    expect(inputFieldInActive.value).toBe(Status.INACTIVE)

  })
})
