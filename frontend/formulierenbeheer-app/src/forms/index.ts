import DescriptionIcon from '@mui/icons-material/Description'
import { CustomResourceProps } from '../Resources'
import FormCreate from './Forms.create'
import FormEdit from './Forms.edit'
import FormList from './Forms.list'

const FormRequests: CustomResourceProps = {
  name: 'forms',
  list: FormList,
  create: FormCreate,
  edit: FormEdit,
  icon: DescriptionIcon,
  options: {
    label: 'forms',
    scopes: ['cga.admin', 'cga.user']
  }
}

export default FormRequests
