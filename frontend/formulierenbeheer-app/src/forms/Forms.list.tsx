import { FC } from 'react'
import {
  useLocaleState,
  Datagrid,
  // DatagridConfigurable,
  DateField,
  List,
  ListProps,
  SelectField,
  TextField,
  SimpleList,
  BulkActionProps,
  BulkDeleteWithConfirmButton,
  FunctionField,
  RaRecord
} from 'react-admin'
import { useAuthorized } from 'xxllnc-react-components'
import { Theme, useMediaQuery } from '@mui/material'
import PublicIcon from '@mui/icons-material/Public'
import { FilterSidebar, FilterToolbar } from './filters'
import { ExportFormsButton } from '../components'
import { FormsActions } from './Forms.actions'
import { Status } from '../types/generatedFormTypes'
import FormListButtons from '../components/button/FormListButtons'

const FormBulkActionButtons = (props: BulkActionProps): JSX.Element => (
  <>
    <ExportFormsButton {...props} />
    <BulkDeleteWithConfirmButton {...props} mutationMode={'undoable'} />
  </>
)

const FormList: FC<ListProps> = (props) => {
  useAuthorized()
  const isXSmall = false // useMediaQuery<Theme>(theme => theme.breakpoints.down('md'))
  const [locale] = useLocaleState()

  return (
    <List {...props}
      title="Formulieren"
      exporter={false}
      aside={<FilterSidebar />}
      filters={<FilterToolbar />}
      filterDefaultValues={{ private: true }}
      sort={{ field: 'name', order: 'ASC' }}
      actions={<FormsActions />}
      empty={false}
    >
      {isXSmall ? (
        <SimpleList
          primaryText={(record: RaRecord) => `${record.name} `}
          secondaryText={(record: RaRecord) =>
            `Publicatiedatum: ${record.publishDate ?
              new Date((record as unknown as { publishDate: Date }).publishDate).toLocaleDateString() : '-'}`
          }
          tertiaryText={(record: RaRecord) => `${record.status}`}
        />
      ) : (
        // <DatagridConfigurable bulkActionButtons={<FormBulkActionButtons />}>
        <Datagrid bulkActionButtons={<FormBulkActionButtons />}>
          <FunctionField
            label=""
            render={(record: RaRecord) =>
              <PublicIcon
                fontSize="small"
                color={(record as unknown as { private: boolean }).private ? 'disabled' : 'primary'}
              />}
          />
          <TextField label={'Naam'} source="name" />
          <TextField label={'Auteur'} source="author" />
          <SelectField label={'Status'} source="status" choices={[
            { id: Status.ACTIVE, name: 'Actief' },
            { id: Status.INACTIVE, name: 'Inactief' }
          ]} />
          <DateField label={'Publicatiedatum'} source="publishDate" locales={locale} />
          <FormListButtons />
        </Datagrid>
        // </DatagridConfigurable>
      )}
    </List>
  )
}

export default FormList
