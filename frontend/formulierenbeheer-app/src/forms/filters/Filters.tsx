/* eslint-disable camelcase */
import {
  FilterList,
  FilterListItem,
} from 'react-admin'
import AssignmentIndIcon from '@mui/icons-material/AssignmentInd'
import TodayIcon from '@mui/icons-material/Today'
import {
  endOfToday,
  startOfToday,
  endOfWeek,
  startOfWeek,
  subWeeks,
  startOfMonth,
  endOfMonth,
  subMonths,
  startOfTomorrow
} from 'date-fns'
import PublicIcon from '@mui/icons-material/Public'
import { Status } from '../../types/generatedFormTypes'

export const PublicFilter = (): JSX.Element => (
  <FilterList
    label="Openbaar"
    icon={<PublicIcon />}>
    <FilterListItem
      label="Openbaar"
      value={{
        private: false,
      }}
    />
    <FilterListItem
      label="Privé"
      value={{
        private: true,
      }}
    />
  </FilterList>
)

export const StatusFilter = (): JSX.Element => (
  <FilterList
    label="Status"
    icon={<AssignmentIndIcon />}>
    <FilterListItem
      label="Actief"
      value={{
        status: Status.ACTIVE,
      }}
    />
    <FilterListItem
      label="Inactief"
      value={{
        status: Status.INACTIVE,
      }}
    />
  </FilterList>
)

export const PublicedAtFilter = (): JSX.Element => (
  <FilterList label="Publicatiedatum" icon={<TodayIcon />}>
    <FilterListItem
      label="Vanaf morgen"
      value={{
        publishedAt_gte: startOfTomorrow().toISOString(),
        publishedAt_lte: undefined,
      }}
    />
    <FilterListItem
      label="Vandaag"
      value={{
        publishedAt_gte: startOfToday().toISOString(),
        publishedAt_lte: endOfToday().toISOString(),
      }}
    />
    <FilterListItem
      label="Deze week"
      value={{
        publishedAt_gte: startOfWeek(new Date()).toISOString(),
        publishedAt_lte: endOfWeek(new Date()).toISOString(),
      }}
    />
    <FilterListItem
      label="Vorige week"
      value={{
        publishedAt_gte: subWeeks(startOfWeek(new Date()), 1).toISOString(),
        publishedAt_lte: startOfWeek(new Date()).toISOString(),
      }}
    />
    <FilterListItem
      label="Deze maand"
      value={{
        publishedAt_gte: startOfMonth(new Date()).toISOString(),
        publishedAt_lte: endOfMonth(new Date()).toISOString(),
      }}
    />
    <FilterListItem
      label="Vorige maand"
      value={{
        publishedAt_gte: subMonths(startOfMonth(new Date()), 1).toISOString(),
        publishedAt_lte: subMonths(endOfMonth(new Date()), 1).toISOString(),
      }}
    />
    <FilterListItem
      label="Eerder"
      value={{
        publishedAt_gte: undefined,
        publishedAt_lte: subMonths(endOfMonth(new Date()), 2).toISOString(),
      }}
    />
  </FilterList>)
