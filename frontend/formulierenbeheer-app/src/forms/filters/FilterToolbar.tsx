import { FC } from 'react'
import { DateTimeInput, Filter, ListProps, BooleanInput } from 'react-admin'
import { Theme, useMediaQuery } from '@mui/material'

export const FilterToolbar: FC<Partial<ListProps>> = (props) => {
  const isSmall = useMediaQuery<Theme>(theme => theme.breakpoints.down('lg'))

  return (
    <Filter {...props}>
      <DateTimeInput source="publishedAt_gte" label="Publicatiedatum van" />
      <DateTimeInput source="publishedAt_lte" label="Publicatiedatum tot" />
      { isSmall &&
        <BooleanInput
          label="Actief"
          source="status"
          format={v => v === 'ACTIVE'}
          parse={v => v ? 'ACTIVE' : 'INACTIVE'}
        />}
      { isSmall && <BooleanInput label="Privé" source="private" />}
    </Filter>
  )
}