import { FC } from 'react'
import { SavedQueriesList , FilterLiveSearch, useTranslate } from 'react-admin'
import { CardContent, Typography } from '@mui/material'
import { SidebarCard } from 'xxllnc-react-components'
import { PublicedAtFilter, StatusFilter, PublicFilter } from './Filters'

export const FilterSidebar: FC = () => {
  const translate = useTranslate()

  return (
    <SidebarCard sx={{ maxWidth: 250 }}>
      <CardContent>
        <Typography variant="h6" component="div">
          { translate('filters') }
        </Typography>
        <FilterLiveSearch source="q" />
        <SavedQueriesList />
        <StatusFilter />
        <PublicFilter />
        <PublicedAtFilter />
      </CardContent>
    </SidebarCard>
  )
}

