import SettingsIcon from '@mui/icons-material/Settings'
import queryString from 'query-string'
import { FC, useEffect, useState } from 'react'
import { Button, Title, useGetOne } from 'react-admin'
import { Link, useLocation } from 'react-router-dom'
import { PreviewFormButton, SaveButton } from '../../components'
import { Form, FormData } from '../../types/forms'
import FormulierenBuilder from './FormulierenBuilder'

interface FormPageQueryProps {
  id: string
  slug: string
  active: string
}

const FormEditor: FC = () => {
  const location = useLocation()
  const { id, slug, active } = queryString.parse(location.search) as unknown as FormPageQueryProps
  const [schema, setSchema] = useState<FormData | null>(null)

  const resultFromGetOne = useGetOne<Form>('forms', { id })
  const data = resultFromGetOne.data
  const error = resultFromGetOne.error as { message?: string }
  const formData = data?.form as unknown as FormData

  useEffect(() => {
    if (formData) setSchema({
      display: 'wizard',
      title: formData?.title,
      name: formData?.name,
      type: ['form'],
      components: formData?.components
    })
  }, [formData])

  return (
    data?.form && !error ?
      <>
        <nav>
          <PreviewFormButton formData={data} slug={slug} active={active} />
          <Button
            component={Link}
            label="Instellingen"
            to={{ pathname: `/forms/${id}` }}>
            <SettingsIcon />
          </Button>
          <SaveButton top formData={data} schema={schema} />
        </nav>
        <Title title={`Formulier ${data?.name} aanpassen`} />
        { schema && <FormulierenBuilder form={schema} onFormBuilderChange={setSchema} /> }
        <menu>
          <SaveButton formData={data} schema={schema} />
        </menu>
      </> :
      <p>{error?.message || 'Het formulier kon niet geladen worden'}</p>
  )
}

export default FormEditor
