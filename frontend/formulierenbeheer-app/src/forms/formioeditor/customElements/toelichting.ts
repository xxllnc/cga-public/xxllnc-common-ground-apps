export const toelichting = {
  title: 'Toelichting',
  key: 'toelichting',
  icon: 'commenting-o',
  schema: {
    key: 'fieldSet',
    type: 'fieldset',
    label: 'Field Set',
    input: false,
    tableView: false,
    components: [
      {
        label: 'Wilt u een (extra) toelichting geven?',
        optionsLabelPosition: 'right',
        inline: false,
        tableView: false,
        values: [
          {
            label: 'Ja',
            value: 'ja',
            shortcut: 'J',
          },
          {
            label: 'Nee',
            value: 'nee',
            shortcut: 'N',
          },
        ],
        key: 'wiltUEenExtraToelichtingGeven',
        type: 'radio',
        input: true,
      },
      {
        label: 'Toelichting',
        autoExpand: false,
        tableView: true,
        validate: {
          maxLength: 1000,
        },
        key: 'toelichting',
        conditional: {
          show: true,
          when: 'wiltUEenExtraToelichtingGeven',
          eq: 'ja',
        },
        type: 'textarea',
        input: true,
      },
    ],
  },
}
