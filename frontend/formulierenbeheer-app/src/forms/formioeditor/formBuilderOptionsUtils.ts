/* eslint-disable */
import { i18nDutch } from '../../i18n'
import { toelichting } from './customElements/toelichting'

const componentTypes = [
  'address', 'container', 'currency', 'datamap',
  'datetime', 'day', 'email', 'phoneNumber',
  'resource', 'select', 'survey', 'tags',
  'textarea', 'textfield', 'time', 'tree', 'url', 'file'
]

const defaultOptions = {
  builder: {
    premium: false,
    basic: {
      components: {
        datetime: {
          title: 'Date / Time',
          group: 'basic',
          icon: 'calendar',
          documentation: '/userguide/#datetime',
          weight: 120,
          schema: {
            type: 'datetime',
            format: 'dd-MM-yyyy hh:mm a'
          }
        },
        password: false
      }
    },
    advanced: {
      components: {
        datetime: false
      }
    },
    xxllnc: {
      title: 'Xxllnc presets',
      weight: 99,
      components: {
        toelichting,
        customJson: {
          title: 'Custom JSON',
          key: 'customJson',
          icon: 'terminal',
          schema: {
            type: 'custom',
            isNew: true,
            key: 'custom',
            protected: false,
            persistent: true
          }
        }
      }
    }
  },
  language: 'nl',
  i18n: {
    nl: i18nDutch
  },
  editForm: {}
}

interface FormIOEditFormComponentOptions {
  key: string
  components: [
    {
      key: string
      ignore: boolean
    }
  ]
}

interface FormIOFormBuilderOptions {
  editForm: {
    address?: [FormIOEditFormComponentOptions]
    container?: [FormIOEditFormComponentOptions]
    currency?: [FormIOEditFormComponentOptions]
    datamap?: [FormIOEditFormComponentOptions]
    datetime?: [FormIOEditFormComponentOptions]
    day?: [FormIOEditFormComponentOptions]
    email?: [FormIOEditFormComponentOptions]
    phoneNumber?: [FormIOEditFormComponentOptions]
    resource?: [FormIOEditFormComponentOptions]
    select?: [FormIOEditFormComponentOptions]
    survey?: [FormIOEditFormComponentOptions]
    tags?: [FormIOEditFormComponentOptions]
    textarea?: [FormIOEditFormComponentOptions]
    textfield?: [FormIOEditFormComponentOptions]
    time?: [FormIOEditFormComponentOptions]
    tree?: [FormIOEditFormComponentOptions]
    url?: [FormIOEditFormComponentOptions]
  }
}

const getFormBuilderOptions = (): FormIOFormBuilderOptions => {
  componentTypes.forEach(element => {
    defaultOptions.editForm[String(element)] = [
      {
        key: 'validation',
        components: [
          {
            key: 'unique',
            ignore: true
          }
        ]
      }
    ]
  })
  return defaultOptions
}

export default getFormBuilderOptions
