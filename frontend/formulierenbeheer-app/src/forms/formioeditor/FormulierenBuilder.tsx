import { FC } from 'react'
import { FormBuilder as FormBuilderIO, Components } from '@formio/react'
import { CustomFormioComponents } from 'xxllnc-formio-components'
import getFormBuilderOptions from './formBuilderOptionsUtils'
import { FormData } from '../../types/forms'
import './styles.scss'

// eslint-disable-next-line
Components.setComponents(CustomFormioComponents)

interface FormBuilderProps {
  form: FormData
  onFormBuilderChange: (data: FormData) => void
}

const FormulierenBuilder: FC<FormBuilderProps> = ({ form, onFormBuilderChange }) => {

  const handleChange = (schema: FormData): void => {
    onFormBuilderChange(schema)
  }

  return (
    <FormBuilderIO
      form={form}
      onChange={handleChange}
      options={getFormBuilderOptions()}
    />
  )
}

export default FormulierenBuilder


