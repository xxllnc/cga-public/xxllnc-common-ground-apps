import { AdminContext, ResourceContextProvider } from 'react-admin'
import { act, render, screen, waitFor } from '@testing-library/react'
import providers from '../../providers'
import FormEditor from './FormEditor'
import queryString from 'query-string'
jest.mock('query-string')

describe('Formseditor test', () => {

  it('should render the FormEditor', async () => {
    const mockeQueryString = queryString as jest.Mocked<typeof queryString>
    mockeQueryString.parse.mockReturnValue({
      id: 'c16ae581-8d6f-424e-bff3-3a70a1374f0a',
      slug: 'formulier-a',
      active: 'true'
    })

    const dataProviderMock = providers.data

    act(() => {
      render(
        <AdminContext dataProvider={dataProviderMock} >
          <ResourceContextProvider value="forms">
            <FormEditor />
          </ResourceContextProvider>
        </AdminContext>
      )
    })

    await act(() => waitFor(() => expect(screen.getByText('Het formulier kon niet geladen worden')).toBeDefined()))
    expect(screen.getByText('Preview')).toBeDefined()
  })
})

