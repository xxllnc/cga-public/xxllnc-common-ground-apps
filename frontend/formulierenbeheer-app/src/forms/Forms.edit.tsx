import { FormControlLabel, FormGroup, Switch } from '@mui/material'
import { FC } from 'react'
import {
  ArrayField, Datagrid, Show, ShowProps, SimpleShowLayout, TextField, useRecordContext
} from 'react-admin'
import { authToken, HeaderTitle } from 'xxllnc-react-components'
import { ShareFormButton } from '../components'
import { FormGetOneResponseDetails, Rights } from '../types/generatedFormTypes'
import { FormsEditActions } from './Forms.edit.actions'

const AddFormShareButton: FC = () => {
  const organizationId = authToken.getOrganizationId()
  const record = useRecordContext<FormGetOneResponseDetails>()
  const isOwner = record?.shares?.find(share => share.rights === Rights.OWNER && share.organizationUuid === organizationId)
  return <ShareFormButton
    id={record?.id}
    disabled={!isOwner}
    shares={record?.shares}
  />
}

const Privat: FC = () => {
  const record = useRecordContext<FormGetOneResponseDetails>()
  return (
    <FormGroup>
      <FormControlLabel disabled control={<Switch defaultChecked={record?.private ?? false} />} label="Privé" />
    </FormGroup>
  )
}

const FormEdit: FC<Partial<ShowProps>> = (props) => (
  <Show
    {...props}
    title={<HeaderTitle title="Formulier %name% aanpassen" />}
    actions={<FormsEditActions />}
  >
    <SimpleShowLayout>
      <TextField label="Formulier" source="name" />
      <TextField label="Auteur" source='author' />
      <Privat />
      <AddFormShareButton />
      <ArrayField fullWidth source="shares" label="">
        <Datagrid bulkActionButtons={false}>
          <TextField label="Organisatie" source="organizationName" />
          <TextField label="Rechten" source="rights" />
        </Datagrid>
      </ArrayField>
    </SimpleShowLayout>
  </Show>
)


export default FormEdit
