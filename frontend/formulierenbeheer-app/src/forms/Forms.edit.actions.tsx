import { FC } from 'react'
import { ActionsToolbar } from '../components'
import { Button, useRecordContext } from 'react-admin'
import { Link } from 'react-router-dom'
import EditIcon from '@mui/icons-material/Edit'
import { FormGetOneResponseDetails } from '../types/generatedFormTypes'
import { getFormSearchString } from '../utils'
import PublicIcon from '@mui/icons-material/Public'
import SettingsIcon from '@mui/icons-material/Settings'
import { authToken } from 'xxllnc-react-components'

const AddShareButton: FC = () => {
  const record = useRecordContext<FormGetOneResponseDetails>()
  const organizationId = authToken.getOrganizationId()
  const sharedInstance = record?.shares?.find(share => share.organizationUuid === organizationId)

  return <Button
    component={Link}
    label="In gebruik nemen"
    style={{ float: 'right' }}
    disabled={record?.private || !!sharedInstance}
    to={{
      pathname: '/share/create',
      search: `?formUuid=${record?.id}&name=${record?.name}&slug=${record?.slug}`
    }}
  >
    <PublicIcon />
  </Button>
}

const SettingsButton: FC = () => {
  const record = useRecordContext<FormGetOneResponseDetails>()
  const organizationId = authToken.getOrganizationId()
  const sharedInstance = record?.shares?.find(share => share.organizationUuid === organizationId)

  return (
    <Button
      component={Link}
      label="Instellingen"
      disabled={!sharedInstance}
      to={{
        pathname: `/share/${sharedInstance?.id}`
      }}>
      <SettingsIcon />
    </Button>
  )
}


export const FormsEditActions: FC = () => {
  const record = useRecordContext<FormGetOneResponseDetails>()

  return (
    <ActionsToolbar source='forms' to='/forms'>
      <AddShareButton />
      <SettingsButton />
      <Button
        component={Link}
        label="Ontwerpen"
        to={{
          pathname: '/formioeditor',
          search: getFormSearchString(record)
        }}>
        <EditIcon />
      </Button>
    </ActionsToolbar>
  )
}
