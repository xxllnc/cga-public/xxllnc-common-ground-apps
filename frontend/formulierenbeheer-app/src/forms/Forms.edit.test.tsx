import { AdminContext, ResourceContextProvider } from 'react-admin'
import { render, screen, waitFor } from '@testing-library/react'
import providers from '../providers'
import FormEdit from './Forms.edit'

jest.setTimeout(100000)

describe('Forms edit test', () => {

  it('should render the edit', async () => {

    const dataProviderMock = providers.data

    render(
      <AdminContext dataProvider={dataProviderMock} >
        <ResourceContextProvider value="forms">
          <FormEdit id={'c16ae581-8d6f-424e-bff3-3a70a1374f0a'} />
        </ResourceContextProvider>
      </AdminContext>
    )


    await waitFor(() => expect(screen.getByText('Formulier')).toBeDefined())

    expect(screen.getByText('Formulier')).toBeDefined()
    expect(screen.getByText('formulier a')).toBeDefined()
    expect(screen.getByText('Auteur')).toBeDefined()
    expect(screen.getByText('Privé')).toBeDefined()

  })
})

