import { cloneElement, useContext, createContext, ReactNode } from 'react'
import {
  FilterButton,
  useListContext,
  useResourceContext,
  ListActionsProps,
  CreateButton,
  // SelectColumnsButton
} from 'react-admin'
import { ListActionsToolbar } from 'xxllnc-react-components'
import { ImportFormsButton } from '../components'

type FilterContextType = ReactNode[]
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
const FilterContext = createContext<FilterContextType>(undefined)

export const FormsActions = (props: ListActionsProps): JSX.Element => {
  const { className, exporter, filters: filtersProp, ...rest } = props
  const {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    displayedFilters,
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    filterValues,
    showFilter
  } = useListContext(props)
  const resource = useResourceContext(rest)
  const filters = useContext(FilterContext) || filtersProp
  return (
    <ListActionsToolbar i18n="">
      {/* <SelectColumnsButton /> */}
      {filtersProp
        ? cloneElement(filtersProp, {
          resource,
          showFilter,
          // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
          displayedFilters,
          // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
          filterValues,
          context: 'button'
        })
        : filters && <FilterButton />}
      <ImportFormsButton />
      <CreateButton variant="contained" />
    </ListActionsToolbar>
  )
}
