import { act, fireEvent, render, screen, waitFor } from '@testing-library/react'
import user from '@testing-library/user-event'
import { AdminContext, ResourceContextProvider } from 'react-admin'
import providers from '../providers'
import FormList from './Forms.list'

jest.setTimeout(60000)

describe('Forms list test', () => {

  it('should render the list with form rows', async () => {

    const dataProviderMock = providers.data

    render(
      <AdminContext dataProvider={dataProviderMock} >
        <ResourceContextProvider value="forms">
          <FormList resource='forms' ><></></FormList>
        </ResourceContextProvider>
      </AdminContext>
    )

    await waitFor(() => expect(screen.getByText('Naam')).toBeDefined())
    expect(screen.getByText('Auteur')).toBeDefined()

    // Test Status filter by counting the occurences of text Actief and Inactief
    act(() => { fireEvent.click(screen.getByRole('button', { name: 'Actief' })) })
    await waitFor(() => expect(screen.getAllByText('Actief')).toHaveLength(4))

    act(() => { fireEvent.click(screen.getByRole('button', { name: 'Inactief' })) })
    await waitFor(() => expect(screen.getAllByText('Inactief')).toHaveLength(1))
    await waitFor(() => expect(screen.getAllByText('Actief')).toHaveLength(1))

  })

  it('should be able to import forms with the importbutton', async () => {

    const dataProviderMock = providers.data

    // eslint-disable-next-line max-len
    const str = '[{"name":"formulier a","status":"INACTIVE","form":"{}","config":null,"publishDate":"2001-01-01","id":"c16ae581-8d6f-424e-bff3-3a70a1374f0a","slug":"formulier-a","url":"https://exxellence.vcap.me/formulierenbeheer/api/v1/forms/c16ae581-8d6f-424e-bff3-3a70a1374f0a"},{"name":"formulier b","status":"ACTIVE","form":"{}","config":null,"publishDate":"2001-01-01","id":"69c08406-3984-4344-9b6d-6f4adec2a4b7","slug":"formulier-b","url":"https://exxellence.vcap.me/formulierenbeheer/api/v1/forms/69c08406-3984-4344-9b6d-6f4adec2a4b7"}]'
    const blob = new Blob([str])
    const file = new File([blob], 'formExport.json', {
      type: 'application/JSON',
    })

    render(
      <AdminContext dataProvider={dataProviderMock} >
        <ResourceContextProvider value="forms">
          <FormList resource='forms' ><></></FormList>
        </ResourceContextProvider>
      </AdminContext>
    )

    // First wait on the inital list to load and klik button and wait for all use effects to finish
    act(() => { fireEvent.click(screen.getByRole('button', { name: 'Actief' })) })
    await waitFor(() => expect(screen.getByText('Naam')).toBeDefined())
    await waitFor(() => expect(screen.getAllByText('Actief')).toHaveLength(4))

    // Test Import button
    act(() => { fireEvent.click(screen.getByRole('button', { name: 'Importeren' })) })
    await waitFor(() => expect(screen.getByText('Selecteer een bestand om te importeren')).toBeDefined())

    act(() => { fireEvent.click(screen.getByRole('button', { name: 'Selecteer bestand' })) })

    const input = screen.getByTestId('uploadInput')
    await user.upload(input, file)
    await waitFor(() => expect(screen.getByText('Importeer formulier 1/2')).toBeDefined())

    const nameFormA: HTMLInputElement = screen.getByLabelText('Naam')
    await waitFor(() => expect(nameFormA.value).toBe('formulier a'))

    act(() => { fireEvent.click(screen.getByRole('button', { name: 'Overslaan' })) })
    await waitFor(() => expect(screen.getByText('Importeer formulier 2/2')).toBeDefined())

    const nameFormB: HTMLInputElement = screen.getByLabelText('Naam')
    await waitFor(() => expect(nameFormB.value).toBe('formulier b'))

    act(() => { fireEvent.click(screen.getByRole('button', { name: 'Overslaan' })) })
    await waitFor(() => expect(screen.getByText('Alle formulieren zijn geïmporteerd')).toBeDefined())
    act(() => { fireEvent.click(screen.getByRole('button', { name: 'Klaar' })) })
  })
})
