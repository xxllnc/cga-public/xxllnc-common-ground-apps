export const getUrl = (): string => window.location.origin + '/formulierenbeheer/api/v1'
export const getSettingsUrl = (): string => window.location.origin + '/instellingen/api/v1'
export const getRequestsUrl = (): string => window.location.origin + '/verzoeken/api/v1'
export const getPublicFormUrl = (): string => window.location.origin + '/formulieren'