import { FC } from 'react'
import Admin from './Admin'
import { useNavigate, BrowserRouter } from 'react-router-dom'
import { getEnvVariable, Auth0ProviderWithHistory } from 'xxllnc-react-components'

export const redirectUri = `${window.location.origin}${process.env.PUBLIC_URL}/login`
const scope = 'cga.user cga.admin openid profile'
const getDomain = () => getEnvVariable('REACT_APP_AUTH0_DOMAIN')
const getClientID = () => getEnvVariable('REACT_APP_AUTH0_CLIENT_ID')
const getAudience = () => getEnvVariable('REACT_APP_AUTH0_AUDIENCE')

// functional component because useNavigate hook can only be used inside a Router
const Auth: FC = () => {
  const navigate = useNavigate()

  return (
    <Auth0ProviderWithHistory
      audience={getAudience()}
      scope={scope}
      redirectUri={redirectUri}
      domain={getDomain()}
      clientId={getClientID()}
    >
      <Admin />
    </Auth0ProviderWithHistory>
  )
}
const App: FC = () => (
  <BrowserRouter basename={process.env.PUBLIC_URL}>
    <Auth />
  </BrowserRouter>
)

export default App
