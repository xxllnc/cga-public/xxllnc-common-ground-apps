import { downloadFile } from '.'

declare global {
  interface Navigator {
    msSaveOrOpenBlob: (blobOrBase64: Blob | string, filename: string) => void
  }
}

describe('downloadFile test', () => {

  global.URL.createObjectURL = jest.fn()

  it('should download a file', () => {
    global.URL.createObjectURL = jest.fn(() => 'details')
    window.navigator.msSaveOrOpenBlob = jest.fn(() => 'details')

    const exampleForms = [{
      formName: 'testForm'
    }]

    downloadFile({ content: JSON.stringify(exampleForms), fileName: 'formExport.json', contentType: 'application/json' })

    expect(window.navigator.msSaveOrOpenBlob).toHaveBeenCalledTimes(1)
  })

  it('should download a file when msSaveOrOpenBlob is not available', () => {
    global.URL.createObjectURL = jest.fn(() => 'details')
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    document.body.appendChild = jest.fn(() => 'details')

    const exampleForms = [{
      formName: 'testForm'
    }]

    downloadFile({ content: JSON.stringify(exampleForms), fileName: 'formExport.json', contentType: 'application/json' })

    // eslint-disable-next-line @typescript-eslint/unbound-method
    expect(document.body.appendChild).toHaveBeenCalledTimes(1)
  })
})
