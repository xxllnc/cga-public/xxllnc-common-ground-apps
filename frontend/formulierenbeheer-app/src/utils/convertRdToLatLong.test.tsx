import { convertRdToLatLong } from './convertRdToLatLong'

describe('convertRdToLatLong test', () => {

  it('should convert rijksdriehoek to lat long', () => {
    const expected = {latitude: 52.29922959694955, longitude: 6.523456791764043}
    const converted = convertRdToLatLong(232505, 479636)

    expect(converted).toStrictEqual(expected)
  })
})
