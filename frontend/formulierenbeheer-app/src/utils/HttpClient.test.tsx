import { getCall, optimisticFetch, postCall } from './HttpClient'

describe('HttpClient test', () => {
  it('should fetch with optimisticFetch', async () => {
    global.fetch = jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve({ data: 'test' }),
      })
    ) as jest.Mock

    const headers = new Headers()
    const response = await optimisticFetch<{ data: string }>({
      url: 'someurl',
      headers
    })

    expect(response).toStrictEqual({data: 'test'})
  })

  it('should fetch with getCall', async () => {
    global.fetch = jest.fn(() =>
      Promise.resolve({
        ok: true,
        json: () => Promise.resolve({ data: 'test' }),
      })
    ) as jest.Mock

    const response = await getCall<{ data: string }>('someurl', 'someOperation')

    expect(response).toStrictEqual({data: 'test'})
  })

  it('should fetch with postCall', async () => {
    global.fetch = jest.fn(() =>
      Promise.resolve({
        ok: true,
        json: () => Promise.resolve({ data: 'test' }),
      })
    ) as jest.Mock

    const response = await postCall<{ data: string }>('someurl', 'something', 'token', 'someOperation')

    expect(response).toStrictEqual({data: 'test'})
  })

  it.skip('should throw with ok is false in postCall', () => {
    global.fetch = jest.fn(() =>
      Promise.resolve({
        ok: false,
        json: () => Promise.resolve({ data: 'test' }),
      })
    ) as jest.Mock

    expect(async () => {
      await postCall<{ data: string }>('someurl', 'something', 'token', 'someOperation')
    }).toThrow(new Error('Er heeft zich een onbekende fout voorgedaan. Neem contact op als het probleem blijft bestaan.'))
  })
})
