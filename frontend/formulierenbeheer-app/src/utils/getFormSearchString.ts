import { RaRecord } from 'react-admin'
import { Form } from '../types/generatedFormTypes'
import { isActiveForm } from './isActiveForm'

export const getFormSearchString = (record: RaRecord | undefined): string => {
  if (!record) return ''
  const { id, slug } = record as Form
  const active = isActiveForm(record)
  return `id=${id}&slug=${slug}&active=${active}`
}