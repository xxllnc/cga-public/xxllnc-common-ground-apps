const NOT_FOUND_MESSAGE = 'Er is een fout opgetreden, het formulier is niet gevonden. ' +
'Neem contact op met de organisatie waarbij u het formulier probeert in te dienen of probeer het later opnieuw.'

const UNKNOWN_ERROR_MESSAGE = 'Er heeft zich een onbekende fout voorgedaan. ' +
'Neem contact op als het probleem blijft bestaan.'

export const parseResponse = async <T>(response: Response): Promise<T> => {
  try {
    return await response.json() as T
  } catch (_) {
    return {} as unknown as T
  }
}

export const checkResponse = async <T>(response: Response): Promise<T> => {
  if (response.status === 404)
    throw new Error(NOT_FOUND_MESSAGE)

  if (!response.ok)
    throw new Error(UNKNOWN_ERROR_MESSAGE)

  try {
    return await response.json() as T

  } catch (_) {
    throw new Error(UNKNOWN_ERROR_MESSAGE)
  }
}
