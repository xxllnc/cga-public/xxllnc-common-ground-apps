
export interface ErrorType {
  body?: {
    detail?: {
      message?: string
      msg?: string
    }[]
    message?: string
  }
}

export const getErrorMessage = (error: ErrorType): string => error?.body?.message || error?.body?.detail?.[0]?.message
    || error?.body?.detail?.[0]?.msg || 'onbekende fout'