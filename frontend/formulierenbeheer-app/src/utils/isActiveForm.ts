import { isBefore } from 'date-fns'
import { RaRecord } from 'react-admin'

export interface IsActiveFormProps extends RaRecord {
  publishDate?: string | number | Date | null
}
export const isActiveForm = (record?: IsActiveFormProps): boolean => {

  if (!record?.publishDate)
    return false
  try {

    const date = (typeof record?.publishDate === 'string') ? new Date(record?.publishDate) : record?.publishDate
    return record?.status === 'ACTIVE' &&
      !!record?.publishDate &&
      isBefore(date, new Date())
  } catch (_) {
    return false
  }
}