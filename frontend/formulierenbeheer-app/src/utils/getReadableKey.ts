export const getReadableKey = (key: string): string => {
  const string = key.replace(/[A-Z][a-z]*/g, str => ` ${str.toLowerCase()}`).trim()
  return string.charAt(0).toUpperCase() + string.slice(1)
}