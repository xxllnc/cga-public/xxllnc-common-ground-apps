interface Props {
  content: any
  fileName: string
  contentType: string
}

export const downloadFile = ({content, fileName, contentType}: Props): void => {
  const fakeLink = document.createElement('a')
  fakeLink.style.display = 'none'
  document.body.appendChild(fakeLink)
  const blob = new Blob([content], { type: contentType })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    window.navigator.msSaveOrOpenBlob(blob, `${fileName}.json`)
  } else {
    fakeLink.setAttribute('href', URL.createObjectURL(blob))
    fakeLink.setAttribute('download', fileName)
    fakeLink.click()
  }
}