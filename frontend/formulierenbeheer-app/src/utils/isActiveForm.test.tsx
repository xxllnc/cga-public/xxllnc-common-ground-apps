import { isActiveForm } from '.'

describe('isActiveForm test', () => {

  it('should return true if the form is active', () => {
    const activeForm = {
      config: null,
      form: {},
      id: '09dcc6bb-fc8f-4892-b3c6-d5fbe2a03d9f',
      name: 'formulier b',
      organizationName: 'Exxellence',
      organizationUuid: '0aa2beca-3819-4f49-aba2-2631e0bcbfb8',
      private: true,
      publishDate: '2022-02-10',
      rights: 'OWNER',
      slug: 'formulier-b',
      status: 'ACTIVE'
    }

    // Given
    const status = isActiveForm(activeForm)

    // We expect the status to be true
    expect(status).toBe(true)
  })

  it('should return false if the form is inactive', () => {
    const activeForm = {
      config: null,
      form: {},
      id: '09dcc6bb-fc8f-4892-b3c6-d5fbe2a03d9f',
      name: 'formulier b',
      organizationName: 'Exxellence',
      organizationUuid: '0aa2beca-3819-4f49-aba2-2631e0bcbfb8',
      private: true,
      publishDate: '2022-02-10',
      rights: 'OWNER',
      slug: 'formulier-b',
      status: 'INACTIVE'
    }

    // Given
    const status = isActiveForm(activeForm)

    // We expect the status to be false
    expect(status).toBe(false)
  })

  it('should return false if the form is active, but the publishDate is in the future', () => {
    const activeForm = {
      config: null,
      form: {},
      id: '09dcc6bb-fc8f-4892-b3c6-d5fbe2a03d9f',
      name: 'formulier b',
      organizationName: 'Exxellence',
      organizationUuid: '0aa2beca-3819-4f49-aba2-2631e0bcbfb8',
      private: true,
      publishDate: '2122-03-22',
      rights: 'OWNER',
      slug: 'formulier-b',
      status: 'ACTIVE'
    }

    // Given
    const status = isActiveForm(activeForm)

    // We expect the status to be false
    expect(status).toBe(false)
  })

  it('should return false if the form is active, but the publishDate is not set', () => {
    const activeForm = {
      config: null,
      form: {},
      id: '09dcc6bb-fc8f-4892-b3c6-d5fbe2a03d9f',
      name: 'formulier b',
      organizationName: 'Exxellence',
      organizationUuid: '0aa2beca-3819-4f49-aba2-2631e0bcbfb8',
      private: true,
      publishDate: null,
      rights: 'OWNER',
      slug: 'formulier-b',
      status: 'ACTIVE'
    }

    // Given
    const status = isActiveForm(activeForm)

    // We expect the status to be false
    expect(status).toBe(false)
  })

  it('should return throw if a invalid date was provided', () => {
    const activeForm = {
      config: null,
      form: {},
      id: '09dcc6bb-fc8f-4892-b3c6-d5fbe2a03d9f',
      name: 'formulier b',
      organizationName: 'Exxellence',
      organizationUuid: '0aa2beca-3819-4f49-aba2-2631e0bcbfb8',
      private: true,
      publishDate: 'some_text',
      rights: 'OWNER',
      slug: 'formulier-b',
      status: 'ACTIVE'
    }

    // Given
    const status = isActiveForm(activeForm)

    // We expect the status to be false
    expect(status).toBe(false)
  })
})
