import { getObjectProperties } from './getObjectProperties'

describe('getObjectProperties test', () => {

  it('should return a array with properties', () => {
    const object = {
      string: 'stringvalue',
      boolean: true,
      array: [
        {
          stringInArray: 'stringvalue'
        }
      ],
      childObject: {
        stringInObject: 'stringvalue'
      }
    }

    // Given
    const result = getObjectProperties(object)

    // We expect the object to be transformed in a array with key value pairs
    expect(result).toHaveLength(4)

    expect(result[0].key).toBe('string')
    expect(result[0].value).toBe('stringvalue')
  })
})
