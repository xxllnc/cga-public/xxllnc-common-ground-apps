/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable security/detect-object-injection, @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-member-access */

interface RequestInputValue {
  key: string
  value: string
}

export const getObjectProperties = (object: any): RequestInputValue[] => {
  let values: RequestInputValue[] = []
  for (const property in object) {
    if (!object.hasOwnProperty(property)) continue
    if (['string', 'number', 'boolean'].includes(typeof object[property])) {
      values.push({ key: property, value: String(object[property])})
    } else if (Array.isArray(object[property])) {
      object[property].forEach(item => {
        values = [...values, ...getObjectProperties(item)]
      })
    } else if (typeof object[property] === 'object') {
      values = [...values, ...getObjectProperties(object[property])]
    }
  }
  return values
}