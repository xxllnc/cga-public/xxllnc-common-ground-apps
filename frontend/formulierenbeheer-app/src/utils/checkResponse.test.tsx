/* eslint-disable @typescript-eslint/no-unsafe-argument */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { checkResponse } from './checkResponse'
const { Response, Headers } = jest.requireActual('node-fetch')

describe('checkResponse test', () => {

  it('should get data from response', async () => {
    const response = new Response(JSON.stringify({ data: { } }), {
      status: 200,
      statusText: 'ok',
      headers: new Headers({
        'Content-Type': 'application/json',
        'Accept': '*/*'
      })
    })
    const converted = await checkResponse<{ data: any }>(response)

    expect(converted.data).toStrictEqual({})
  })

  it('should get error from response', () => {
    const response = new Response(JSON.stringify({ data: { } }), {
      status: 200,
      statusText: 'ok',
      headers: new Headers({
        'Content-Type': 'application/json',
        'Accept': '*/*'
      })
    })

    expect(async () => checkResponse<{ data: any }>(response)).not.toThrow()
  })
})