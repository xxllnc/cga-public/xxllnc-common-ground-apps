import { getReadableKey } from './getReadableKey'

describe('getReadableKey test', () => {

  it('should convert camelcase keys to sentence case', () => {
    const key = 'confirmationCheckBox'

    // Given
    const converted = getReadableKey(key)

    // We expect the key to be converted to sentence case
    expect(converted).toBe('Confirmation check box')
  })
})
