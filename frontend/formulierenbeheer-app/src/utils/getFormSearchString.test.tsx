import { getFormSearchString } from './getFormSearchString'

describe('getFormSearchString test', () => {

  it('should convert record to a search string', () => {
    const form = {
      config: null,
      form: { },
      id: '09dcc6bb-fc8f-4892-b3c6-d5fbe2a03d9f',
      name: 'formulier b',
      organizationName: 'Exxellence',
      organizationUuid: '0aa2beca-3819-4f49-aba2-2631e0bcbfb8',
      private: true,
      publishDate: '2022-02-10',
      rights: 'OWNER',
      slug: 'formulier-b',
      status: 'ACTIVE'
    }
    const expected = `id=${form.id}&slug=${form.slug}&active=true`

    // Given
    const result = getFormSearchString(form)

    // We expect the result to match the expected string
    expect(result).toBe(expected)
  })

  it('should display active false when the form is inactive', () => {
    const form = {
      config: null,
      form: { },
      id: '09dcc6bb-fc8f-4892-b3c6-d5fbe2a03d9f',
      name: 'formulier b',
      organizationName: 'Exxellence',
      organizationUuid: '0aa2beca-3819-4f49-aba2-2631e0bcbfb8',
      private: true,
      publishDate: '2022-02-10',
      rights: 'OWNER',
      slug: 'formulier-b',
      status: 'INACTIVE'
    }
    const expected = `id=${form.id}&slug=${form.slug}&active=false`

    // Given
    const result = getFormSearchString(form)

    // We expect the result to match the expected string
    expect(result).toBe(expected)
  })

  it('should display empty string if form is undefined', () => {
    const form = undefined

    // Given
    const result = getFormSearchString(form)

    // We expect the result to match the expected string
    expect(result).toBe('')
  })
})
