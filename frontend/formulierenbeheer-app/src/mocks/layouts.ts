import { Layout } from '../types/forms'

const layouts: Layout[] = [
  {
    id: 'Layout1',
    uuid: 'a750f105412cee337cf9c9f458d3c1e1',
    name: 'Zaakstad',
    primaryColour: '#29aac7',
    secondaryColour: '#32b145',
    letterType: 'Arial',
    colourLink: '#29aac7',
    logo: 'MijnLogo.png'
  },
  {
    id: 'Layout2',
    uuid: 'a750f105412cee337cf9c9f458d3c1e2',
    name: 'Zaakstad 1',
    primaryColour: '#7aa89f',
    secondaryColour: '#b19532',
    letterType: 'Calibri',
    colourLink: '#29aac7',
    logo: 'MijnLogo.png'
  }
]

export default layouts
