import { Form, Rights, Status } from '../types/generatedFormTypes'

const forms: Form[] = [
  {
    config: [],
    organizationUuid: 'c16ae581-8d6f-424e-bff3-3a70a1374f0a',
    organizationName: 'Exxellence',
    slug: 'formulier-a',
    name: 'formulier a',
    status: Status.ACTIVE,
    publishDate: '2022-02-17',
    id: 'c16ae581-8d6f-424e-bff3-3a70a1374f0a',
    rights: Rights.OWNER
  },
  {
    config: [],
    organizationUuid: 'a750f105412cee337cf9c9f458d3c1e1',
    organizationName: 'Exxellence',
    slug: 'Evenementenvergunning',
    name: 'Evenementenvergunning',
    status: Status.ACTIVE,
    publishDate: '2022-02-17',
    id: 'a750f105412cee337cf9c9f458d3c1e1',
    rights: Rights.OWNER
  },
  {
    config: [],
    organizationUuid: 'a750f105412cee337cf9c9f458d3c1e3',
    organizationName: 'Exxellence',
    slug: 'melding-openbaar-ruimte',
    name: 'Melding Openbaar Ruimte',
    status: Status.ACTIVE,
    publishDate: '2022-02-17',
    id: 'a750f105412cee337cf9c9f458d3c1e3',
    rights: Rights.OWNER
  }
]

export default forms
