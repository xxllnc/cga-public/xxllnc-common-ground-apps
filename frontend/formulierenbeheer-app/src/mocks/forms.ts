import { Form } from '../types/forms'
import { Rights, Status } from '../types/generatedFormTypes'

const form = {
  title: 'mock form',
  name: 'mock',
  components: [{
    key: 'mock_form',
    ignore: false
  }]
}

const forms: Form[] = [
  {
    form,
    organizationUuid: 'c16ae581-8d6f-424e-bff3-3a70a1374f0a',
    organizationName: 'Exxellence',
    slug: 'formulier-a',
    id: 'c16ae581-8d6f-424e-bff3-3a70a1374f0a',
    name: 'formulier a',
    status: Status.ACTIVE,
    publishDate: '2022-02-17',
    shares: [
      {
        config: [],
        organizationUuid: 'c16ae581-8d6f-424e-bff3-3a70a1374f0a',
        organizationName: 'Exxellence',
        slug: 'formulier-a',
        name: 'formulier a',
        status: Status.ACTIVE,
        publishDate: '2022-02-17',
        id: 'c16ae581-8d6f-424e-bff3-3a70a1374f0a',
        rights: Rights.OWNER
      }
    ]
  },
  {
    form,
    organizationUuid: 'a750f105412cee337cf9c9f458d3c1e1',
    organizationName: 'Exxellence',
    slug: 'Evenementenvergunning',
    id: 'a750f105412cee337cf9c9f458d3c1e1',
    name: 'Evenementenvergunning',
    status: Status.ACTIVE,
    publishDate: '2022-02-17',
    shares: [
      {
        config: [],
        organizationUuid: 'a750f105412cee337cf9c9f458d3c1e1',
        organizationName: 'Exxellence',
        slug: 'Evenementenvergunning',
        name: 'Evenementenvergunning',
        status: Status.ACTIVE,
        publishDate: '2022-02-17',
        id: 'a750f105412cee337cf9c9f458d3c1e1',
        rights: Rights.OWNER
      }
    ]
  },
  {
    form,
    organizationUuid: 'a750f105412cee337cf9c9f458d3c1e3',
    organizationName: 'Exxellence',
    slug: 'melding-openbaar-ruimte',
    id: 'a750f105412cee337cf9c9f458d3c1e3',
    name: 'Melding Openbaar Ruimte',
    status: Status.ACTIVE,
    publishDate: '2022-02-17',
    private: false,
    shares: [
      {
        config: [],
        organizationUuid: 'a750f105412cee337cf9c9f458d3c1e3',
        organizationName: 'Exxellence',
        slug: 'melding-openbaar-ruimte',
        name: 'Melding Openbaar Ruimte',
        status: Status.ACTIVE,
        publishDate: '2022-02-17',
        id: 'a750f105412cee337cf9c9f458d3c1e3',
        rights: Rights.OWNER
      },
      {
        config: [],
        organizationUuid: 'a750f105412cee337cf9c9f458d3c1e4',
        organizationName: 'Den Haag',
        slug: 'melding-openbaar-ruimte',
        name: 'Melding Openbaar Ruimte',
        status: Status.ACTIVE,
        publishDate: '2022-02-17',
        id: 'a750f105412cee337cf9c9f458d3c1e4',
        rights: Rights.VIEWER
      },
      {
        config: [],
        organizationUuid: 'a750f105412cee337cf9c9f458d3c1e5',
        organizationName: 'Utrecht',
        slug: 'melding-openbaar-ruimte',
        name: 'Melding Openbaar Ruimte',
        status: Status.ACTIVE,
        publishDate: '2022-02-17',
        id: 'a750f105412cee337cf9c9f458d3c1e5',
        rights: Rights.EDITOR
      }
    ]
  }
]

export default forms
