import { SettingType } from '../types/forms'

const generalSettings: SettingType[] = [
  {
    name: 'Algemene',
    description: 'Algemene Instellingen',
    id: 'generalSettings',
    uuid: '3fa85f64-5717-4562-b3fc-2c963f66afa6'
  },
  {
    name: 'Formulier',
    description: 'Formulier Instellingen',
    id: 'formSettings',
    uuid: '3fa85f64-5717-4562-b3fc-2c963f66afa4'
  },
  {
    name: 'Css',
    description: 'Css Instellingen',
    id: 'cssSettings',
    uuid: '3fa85f64-5717-4562-b3fc-2c963f66afa5'
  }
]

export default generalSettings