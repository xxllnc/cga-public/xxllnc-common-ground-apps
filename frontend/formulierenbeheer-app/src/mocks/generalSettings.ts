import { GeneralSetting } from '../types/forms'

const generalSettings: GeneralSetting[] = [
  {
    name: 'Test Setting 1',
    value: 'Test Setting 1',
    description: 'Test Setting 1',
    regex: '',
    settingTypeId: 1,
    id: 'SET-0001',
    uuid: '3fa85f64-5717-4562-b3fc-2c963f66afa7',
    lastModified: '2021-11-03T14:27:28.949Z'
  },
  {
    name: 'Test Setting 2',
    value: 'Test Setting 2',
    description: 'Test Setting 2',
    regex: '',
    settingTypeId: 2,
    id: 'SET-0002',
    uuid: '3fa85f64-5717-4562-b3fc-2c963f66afa8',
    lastModified: '2021-11-02T14:27:28.949Z'
  },
  {
    name: 'cssOverride',
    value: '// add your code',
    description: 'Specifieke css voor de organisatie',
    regex: '',
    settingTypeId: 4,
    id: 'SET-0003',
    uuid: '3fa85f64-5717-4562-b3fc-2c963f66afa7',
    lastModified: '2021-11-02T14:27:28.949Z'
  }
]

export default generalSettings