/* eslint-disable camelcase */

const requests = [
  {
    formUuid: '56b7cfe0-c9c5-4f47-aec5-4ce5e7151482',
    status: 'ACTIVE',
    formInput: {
      wiltUAnoniemBlijven: 'nee',
      mor_onderwerp_melding: 'bestrating',
      ztc_omschrijving: 'beschrijving niet aanwezig',
      locatie: {
        type: 'Point',
        coordinates: [
          5.285797119140625,
          51.90843157768965
        ]
      },
      confirmationCheckBox: true,
      mor_voornaam: 'voortest',
      mor_achternaam: 'achtertest',
      mor_telefoonnummer: '123456789',
      mor_email: 'test@test2.nl'
    },
    id: '009b58ae-9a31-4726-ae7c-20c3463fa6ae',
    registrationDate: '2022-06-16'
  },
  {
    formUuid: 'a8b0eb6d-93e7-43f7-ac58-4fa6a7b2508f',
    status: 'ACTIVE',
    formInput: {
      deVraag: 'Dit is mijn vraag 8shnP',
      confirmationCheckBox: true
    },
    id: '00b6496b-9daa-4eca-8763-201db48e8230',
    registrationDate: '2022-08-31'
  }
]

export default requests