import { render, screen, waitFor } from '@testing-library/react'
import Login from './Login'

// TODO: Fix test CGA-3643
describe.skip('Login page test', () => {

  it('should render the login page', async () => {

    //Given
    render(
      <Login />
    )

    await waitFor(() => expect(screen.getByText('login.hint')).toBeDefined())
  })
})
