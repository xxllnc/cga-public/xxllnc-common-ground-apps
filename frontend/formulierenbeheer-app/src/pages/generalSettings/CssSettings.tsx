import { FC, ChangeEvent, useState } from 'react'
import { SimpleForm, useNotify, RaRecord, useUpdate, useTranslate, useRecordContext, Toolbar, SaveButton } from 'react-admin'
import { Grid, FormControlLabel, Switch, Typography } from '@mui/material'
import { FieldValues, useWatch, useFormContext } from 'react-hook-form'
import Editor, { DiffEditor } from '@monaco-editor/react'
import { ExportScssButton } from '../../components/button/ExportScssButton'
import { ImportScssButton } from '../../components/button/ImportScssButton'

const FormContent: FC = () => {
  const translate = useTranslate()
  const { value: originalValue } = useRecordContext<{ value: string }>()
  const [darkMode, setDarkMode] = useState(true)
  const [diff, setDiff] = useState(false)

  const { setValue } = useFormContext()
  const currentValue = useWatch<{ value: string }>({ name: 'value' })

  const handleEditorValidation = (markers: string[]) => setValue('errors', markers.length > 0)
  const handleOnChangeDarkMode = (event: ChangeEvent<HTMLInputElement>) => setDarkMode(event.target.checked)
  const handleOnChangeDiff = (event: ChangeEvent<HTMLInputElement>) => setDiff(event.target.checked)

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Typography variant="h6">{translate('cssSettings.title')}</Typography>
        <Typography variant="body1">{translate('cssSettings.description')}</Typography>
      </Grid>
      <Grid container justifyContent="flex-end">
        <FormControlLabel
          control={<Switch checked={darkMode} onChange={handleOnChangeDarkMode} />}
          label={translate('cssSettings.darkMode')}
        />
        <FormControlLabel
          control={<Switch checked={diff} onChange={handleOnChangeDiff} />}
          label={translate('cssSettings.diff')}
        />
      </Grid>
      <Grid item xs={12}>
        {!diff ?
          <Editor
            height="60vh"
            defaultLanguage="scss"
            value={currentValue ?? ''}
            theme={darkMode ? 'vs-dark' : 'light'}
            onChange={newValue => setValue('value', newValue || '')}
            onValidate={handleEditorValidation}
          /> :
          <DiffEditor
            height="60vh"
            language="scss"
            original={originalValue ?? ''}
            modified={currentValue ?? ''}
            theme={darkMode ? 'vs-dark' : 'light'}
            options={{
              readOnly: true
            }}
          />}
      </Grid>
      <Grid
        container
        style={{ marginBottom: 10 }}
        justifyContent="flex-end"
      >
        <ImportScssButton />
        <ExportScssButton />
      </Grid>
    </Grid>
  )
}

const SettingsToolbar: FC = () => (
  <Toolbar>
    <SaveButton alwaysEnable />
  </Toolbar>
)
interface CssSettingsrecord extends RaRecord {
  hasErrors: boolean
  id: string
}

const CssSettings: FC = () => {
  const notify = useNotify()
  const [update] = useUpdate()
  const record = useRecordContext()

  const save = async (data: FieldValues & Partial<CssSettingsrecord>) => {
    try {
      if (data.hasErrors) return notify('cssSettings.hasErrors', { type: 'warning' })
      await update('settings', {
        id: record.id,
        data,
        previousData: record
      })
      return notify('cssSettings.saved', { type: 'info' })
    } catch (_error) {
      return notify('generalError', { type: 'warning' })
    }
  }

  return (
    <SimpleForm onSubmit={save} toolbar={<SettingsToolbar />}>
      <FormContent />
    </SimpleForm>
  )
}

export default CssSettings