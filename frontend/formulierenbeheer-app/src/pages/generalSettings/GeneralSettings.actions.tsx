import { FC } from 'react'
import { ActionsToolbar } from '../../components'

export const GeneralSettingsEditActions: FC = () => (
  <ActionsToolbar source="settings" to="/settings"/>
)
