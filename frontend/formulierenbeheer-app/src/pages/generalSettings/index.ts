import SettingsIcon from '@mui/icons-material/Settings'
import { CustomResourceProps } from '../../Resources'
import GeneralSettingsEdit from './GeneralSettings.edit'
import GeneralSettingsList from './GeneralSettings.list'

const GeneralSettings: CustomResourceProps = {
  name: 'settings',
  list: GeneralSettingsList,
  edit: GeneralSettingsEdit,
  icon: SettingsIcon,
  options: {
    label: 'general',
    scopes: ['cga.admin', 'cga.user'],
    isSubMenu: true
  }
}


export default GeneralSettings
