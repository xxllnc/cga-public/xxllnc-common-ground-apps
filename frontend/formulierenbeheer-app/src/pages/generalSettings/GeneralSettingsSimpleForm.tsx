import { FC } from 'react'
import {
  maxLength,
  minLength,
  ReferenceInput,
  required,
  SelectInput,
  SimpleForm,
  TextInput,
  useRecordContext,
  Toolbar,
  SaveButton
} from 'react-admin'
import { GeneralSetting } from '../../types/forms'
import CssSettings from './CssSettings'

const SettingsToolbar: FC = () => (
  <Toolbar>
    <SaveButton />
  </Toolbar>
)

const GeneralSettingsSimpleForm: FC = () => (
  <SimpleForm toolbar={<SettingsToolbar />}>
    <TextInput
      label={'name'}
      source="name"
      validate={[required('Het veld Naam is verplicht, vul de Naam in'), minLength(1), maxLength(255)]}
    />
    <TextInput
      label={'value'}
      source="value"
      validate={required('Het veld Waarde is verplicht, vul de Waarde in')}
    />
    <TextInput
      label={'description'}
      source="description"
    />
    <TextInput
      label={'regex'}
      source="regex"
      validate={required('Het veld Reguliere expressie is verplicht, vul de Reguliere expressie in')}
      defaultValue=".*"
    />
    <ReferenceInput
      label="type"
      source="settingTypeId"
      reference="settingTypes"
      sort={{ field: 'name', order: 'ASC' }}
    >
      <SelectInput
        label={'Type'}
        optionText="name"
        fullWidth
        validate={[required()]}
      />
    </ReferenceInput>
  </SimpleForm>
)

const GeneralSettingsForm: FC = () => {
  const record = useRecordContext<GeneralSetting>()
  return record?.name === 'cssOverride' ? <CssSettings /> : <GeneralSettingsSimpleForm />
}

export default GeneralSettingsForm