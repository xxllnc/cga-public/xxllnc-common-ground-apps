import { render, screen, waitFor } from '@testing-library/react'
import { AdminContext, ResourceContextProvider } from 'react-admin'
import providers from '../../providers'
import GeneralSettingsList from './GeneralSettings.list'

describe('generalSettings list test', () => {

  it('should render the list with generalSettings', async () => {

    const dataProviderMock = providers.data

    // Given
    render(
      <AdminContext dataProvider={dataProviderMock} >
        <ResourceContextProvider value="settings">
          <GeneralSettingsList resource='settings'><></></GeneralSettingsList>
        </ResourceContextProvider>
      </AdminContext>
    )

    await waitFor(() => expect(screen.getAllByText('Test Setting 2')).toHaveLength(2))

  })
})
