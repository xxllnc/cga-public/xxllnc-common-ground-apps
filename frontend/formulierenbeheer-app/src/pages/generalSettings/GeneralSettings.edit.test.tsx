import { render, screen, waitFor } from '@testing-library/react'
import { AdminContext, ResourceContextProvider } from 'react-admin'
import providers from '../../providers'
import GeneralSettingsEdit from './GeneralSettings.edit'


describe('GeneralSettings edit test', () => {

  it('should render the edit', async () => {

    const dataProviderMock = providers.data

    //Given
    render(
      <AdminContext dataProvider={dataProviderMock} >
        <ResourceContextProvider value="settings">
          <GeneralSettingsEdit id={'SET-0001'} />
        </ResourceContextProvider>
      </AdminContext>
    )

    // We expect 'name *' as required label for one input field
    await waitFor(() => expect(screen.getByLabelText('name *')).toBeDefined())


  })

  it('should render the edit for css setting', async () => {

    const dataProviderMock = providers.data

    //Given
    render(
      <AdminContext dataProvider={dataProviderMock} >
        <ResourceContextProvider value="settings">
          <GeneralSettingsEdit id={'SET-0003'} />
        </ResourceContextProvider>
      </AdminContext>
    )

    // We expect 'cssSettings.title' at the top of the page
    await waitFor(() => expect(screen.getByText('cssSettings.title')).toBeDefined())
  })
})
