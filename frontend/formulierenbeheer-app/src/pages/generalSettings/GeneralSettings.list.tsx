import { FC } from 'react'
import {
  Datagrid,
  DateField,
  List,
  ListProps,
  ReferenceField,
  TextField
} from 'react-admin'

const GeneralSettingsList: FC<ListProps> = (props) => (
  <List {...props} exporter={false} >
    <Datagrid rowClick="edit" bulkActionButtons={false}>
      <TextField source="name" label={'name'} />
      <TextField source="description" label={'description'} />
      <ReferenceField
        source="settingTypeId"
        reference="settingTypes"
        label={'type'}
        link={false}
      >
        <TextField source="name" />
      </ReferenceField>
      <DateField source="lastModified" label={'lastModified'} />
    </Datagrid>
  </List>
)

export default GeneralSettingsList