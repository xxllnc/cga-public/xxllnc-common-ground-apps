
import { FC } from 'react'
import { Edit, EditProps } from 'react-admin'
import { HeaderTitle } from 'xxllnc-react-components'
import { GeneralSettingsEditActions } from './GeneralSettings.actions'
import GeneralSettingsForm from './GeneralSettingsSimpleForm'

const GeneralSettingsEdit: FC<Partial<EditProps>> = (props) => (
  <Edit
    actions={<GeneralSettingsEditActions />}
    title={<HeaderTitle title="Algemene instelling %name% aanpassen" />}
    {...props}
  >
    <GeneralSettingsForm />
  </Edit>
)

export default GeneralSettingsEdit
