import { Card, styled } from '@mui/material'
import { xxllncColor } from 'xxllnc-react-components'

export const FilterSidebarCard = styled(Card)(
  ({ theme }) => ({
    'paddingRight': theme.spacing(6),
    'boxSizing': 'border-box',
    'backgroundColor': 'transparent',
    'borderRadius': 0,
    'order': -1,
    'maxWidth': '33.7%',
    'marginLeft': 'auto',
    'marginRight': 'auto',
    'width': '100%',
    [theme.breakpoints.down('lg')]: {
      display: 'none',
    },
    '& > div': {
      padding: 0,
    },
    '& .MuiTypography-overline': {
      color: xxllncColor.dark60,
    },
    '& .MuiInputBase-root': {
      'borderRadius': '50px',
      '& #q': {
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(1),
        height: '40px',
        boxSizing: 'border-box',
      },
    },
    '& .MuiFilledInput-root': {
      '&.Mui-focused': {
        background: xxllncColor.light100,
      },
      '&:hover': {
        background: xxllncColor.light100,
      },
    },
  })
)