import { CircularProgress } from '@mui/material'
import { styled } from '@mui/styles'
import { FC } from 'react'

const StyledDiv = styled('div')({
  position: 'absolute',
  left: '50%',
  top: '50%'
})

export const Spinner: FC<{ display: boolean }> = ({ display }) => (
  display ? (
    <StyledDiv>
      <CircularProgress />
    </StyledDiv>
  ) : null
)