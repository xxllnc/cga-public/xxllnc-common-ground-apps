import { MenuItem, TextField } from '@mui/material'
import { Form } from '../../types/forms'
import { Status } from '../../types/generatedFormTypes'

interface Props {
  form?: Form
  onChange: (n, v) => void
  disabled: boolean
}

export const ImportForm = ({ form, onChange, disabled }: Props): JSX.Element | null => {

  if (!form) return null

  return (
    <form>
      <TextField
        sx={{ marginBottom: '10px' }}
        id="form-name"
        label="Naam"
        disabled={disabled}
        value={form.name || ''}
        onChange={event => onChange('name', event.target.value)}
      />
      <TextField
        select
        id="form-status"
        label="Status"
        value={form.status}
        onChange={event => onChange('status', event.target.value)}
        helperText={'Let op: Alle geïmporteerde formulieren zijn standaard inactief, ' +
          'maar kunnen geactiveerd worden door de status op actief te zetten.'}
      >
        <MenuItem value={Status.ACTIVE}>Actief</MenuItem>
        <MenuItem value={Status.INACTIVE}>Inactief</MenuItem>
      </TextField>
      {form.status === Status.ACTIVE &&
        <TextField
          sx={{ marginTop: '10px' }}
          id="form-date"
          label="Publicatiedatum"
          type="date"
          InputLabelProps={{ shrink: true }}
          disabled={disabled}
          value={form.publishDate || new Date().toISOString().split('T')[0]}
          onChange={event => onChange('publishDate', event.target.value)}
        />
      }
    </form>
  )
}
