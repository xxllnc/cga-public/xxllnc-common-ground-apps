import { Paper, Typography } from '@mui/material'

export interface RequestError {
  body?: {
    detail?: {
      message?: string
      msg?: string
    }[]
    message?: string
  }
}

interface Props {
  error?: RequestError
}

const getErrorMessage = (error: RequestError): string => error?.body?.message
  || error?.body?.detail?.[0]?.message || error?.body?.detail?.[0]?.msg
  || 'Er ging iets mis. Neem contact op als het probleem blijft bestaan.'

export const ImportFormError = ({ error }: Props): JSX.Element | null => (
  (!error) ? null :
    <Paper sx={{
      backgroundColor: 'rgb(253, 236, 234)',
      padding: '6px 16px',
      borderRadius: '4px',
      marginBottom: '10px'
    }}>
      <Typography sx={{
        color: 'rgb(97, 26, 21)',
        fontSize: '0.875rem'
      }}
      >
        {getErrorMessage(error)}</Typography>
    </Paper>
)