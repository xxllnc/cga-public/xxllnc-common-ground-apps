import { TopToolbar, RaRecord } from 'react-admin'
import { BackButton } from '../../components'
import { ToolbarTitle } from '..'
import { Theme } from '@mui/material'
import makeStyles from '@mui/styles/makeStyles'
import createStyles from '@mui/styles/createStyles'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    toolbar: {
      display: 'flex',
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    spacer: {
      flex: 1,
    },
    title: {
      fontSize: 20,
      fontWeight: 600,
      lineHeight: '28px',
      marginRight: 'auto',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
    },
    iconContainer: {
      'borderRadius': 8,
      'height': 32,
      'lineHeight': 0,
      'marginRight': theme.spacing(1),
      'maxWidth': 32,
      'overflow': 'hidden',
      '& img': {
        height: '100%',
        width: '100%',
      }
    }
  }),
)

interface ActionsToolbarProps {
  record?: RaRecord,
  source?: string,
  i18n?: string,
  tag?: React.ReactNode | null,
  to?: string,
  children?: React.ReactNode,
}

export const ActionsToolbar = ({ record, source, i18n, tag, to, children }: ActionsToolbarProps): JSX.Element => {
  const classes = useStyles()

  return (
    <TopToolbar className={classes.toolbar}>
      <BackButton to={to} />
      <ToolbarTitle record={record} source={source} i18n={i18n} className={classes.title} />
      {tag}
      <span className={classes.spacer} />
      {children}
    </TopToolbar>
  )
}
