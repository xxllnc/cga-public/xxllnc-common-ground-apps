import { FC, useState, useRef } from 'react'
import {
  Grow,
  Paper,
  Popper,
  Button,
  Divider,
  useTheme,
  MenuItem,
  MenuList,
  ButtonGroup,
  useMediaQuery,
  ClickAwayListener
} from '@mui/material'
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown'

interface Option {
  label: string
  action: () => void
}

interface Props {
  label: string
  options: Option[]
  bottomOption?: Option
  selected: string
  disabled?: boolean
}

export const SplitButton: FC<Props> = ({ label, options, bottomOption, selected, disabled }) => {
  const theme = useTheme()
  const [open, setOpen] = useState(false)
  const anchorRef = useRef<HTMLDivElement>(null)
  const matches = useMediaQuery(theme.breakpoints.up('sm'))

  const handleMenuItemClick = (_event: any, index: number) => {
    // eslint-disable-next-line security/detect-object-injection
    options[index].action()
    setOpen(false)
  }

  const handleBottomOptionClick = () => {
    bottomOption?.action()
    setOpen(false)
  }

  const handleToggle = () => setOpen((prevOpen) => !prevOpen)

  const handleClose = (event: Event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target as HTMLElement)) return
    setOpen(false)
  }

  return (
    <>
      <ButtonGroup variant="outlined" ref={anchorRef} aria-label={label}>
        { matches && <Button disabled={disabled} >{ selected }</Button> }
        <Button
          size="small"
          aria-controls={open ? 'split-button-menu' : undefined}
          aria-expanded={open ? 'true' : undefined}
          aria-label={`selecteer ${label}`}
          aria-haspopup="menu"
          disabled={disabled}
          onClick={handleToggle}
        >
          <ArrowDropDownIcon />
        </Button>
      </ButtonGroup>
      <Popper
        open={open}
        anchorEl={anchorRef.current}
        role={undefined}
        style={{ zIndex: 99 }}
        transition
        disablePortal
      >
        { ({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            style={{
              transformOrigin:
                placement === 'bottom' ? 'center top' : 'center bottom',
            }}
          >
            <Paper>
              <ClickAwayListener onClickAway={handleClose}>
                <MenuList id="split-button-menu">
                  { options.map((option, index) => (
                    <MenuItem
                      key={option.label}
                      selected={option.label === selected}
                      onClick={(event) => handleMenuItemClick(event, index)}
                    >
                      { option.label.toLowerCase() }
                    </MenuItem>
                  )) }
                  { bottomOption && <Divider /> }
                  { bottomOption &&
                      <MenuItem
                        key={bottomOption.label}
                        onClick={handleBottomOptionClick}
                      >
                        { bottomOption.label }
                      </MenuItem> }
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        ) }
      </Popper>
    </>
  )
}