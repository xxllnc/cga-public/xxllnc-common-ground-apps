import CheckIcon from '@mui/icons-material/Check'
import LocationCityIcon from '@mui/icons-material/LocationCity'
import {
  Autocomplete, Avatar, Dialog, DialogActions, DialogContent, DialogTitle, List,
  ListItem, ListItemAvatar, ListItemText, TextField
} from '@mui/material'
import { FC, useState } from 'react'
import {
  Button, Identifier, useCreate, useDelete, useGetList, useNotify, useRefresh, useTranslate, useUpdate
} from 'react-admin'
import { authToken } from 'xxllnc-react-components'
import { Form, OrganizationReturn, Rights } from '../../types/generatedFormTypes'
import { SplitButton } from './SplitButton'

const availableOrganizations: { label: string, organizationUuid: string }[] = []

interface Props {
  id: Identifier
  disabled: boolean
  shares?: Form[]
  style?: React.CSSProperties
}

export const ShareFormButton: FC<Props> = ({ id, disabled = false, shares }) => {

  const translate = useTranslate()

  const [open, setOpen] = useState(false)

  const notify = useNotify()
  const refresh = useRefresh()
  const [create] = useCreate()
  const [deleteOne] = useDelete()
  const [update] = useUpdate()

  const onSuccess = () => {
    refresh()
    updateAvalibleOrganizations()
  }

  const organizationId = authToken.getOrganizationId()
  const { data: organizations, error } = useGetList<OrganizationReturn>(
    'organizations',
    { sort: { field: 'name', order: 'DESC' } }
  )

  if (error) notify(`Er is een fout opgetreden ${error}`, { type: 'error' })

  const updateAvalibleOrganizations = () => {
    availableOrganizations.splice(0, availableOrganizations.length)
    const sharedIdSet = new Set()
    shares?.forEach(share => sharedIdSet.add(share.organizationUuid))

    if (organizations)
      Object.values(organizations).forEach(organization => {
        if (!sharedIdSet.has(organization.id)) {
          availableOrganizations.push({
            label: organization.name,
            organizationUuid: organization.id
          })
        }
      })
  }

  if (organizations && availableOrganizations.length === 0 && shares) updateAvalibleOrganizations()

  const handleCloseClick = () => {
    updateAvalibleOrganizations()
    setOpen(false)
  }

  const handleShare = (_event: any, value: { label: string } | null, _reason: any) => {

    const selectedOrganization = availableOrganizations.find(o => o.label === value?.label)

    if (!selectedOrganization) return

    const newShare = {
      organizationUuid: selectedOrganization.organizationUuid,
      organizationName: selectedOrganization.label,
      rights: 'VIEWER',
      id
    }

    void create('share', { data: newShare },
      {
        onSuccess,
        onError: (err) => {
          notify(`Er is een fout opgetreden bij het delen van het formulier: ${(err as Error).message}`, { type: 'error' })
        },
      })
  }

  const handleDelete = (share: Form) => {

    void deleteOne(
      'share',
      { id: share.id, previousData: { ...share } },
      {
        onSuccess,
        onError: (err) => {
          notify(`Er is een fout opgetreden bij het verwijderen van een organizatie: ${(err as Error).message}`, { type: 'error' })
        },
      }
    )

  }

  const handleUpdate = (share: Form, rights: Rights) => {

    void update(
      'share',
      { id: share.id, data: { ...share, rights }, previousData: { ...share } },
      {
        onSuccess,
        onError: (err) => {
          notify(`Er is een fout opgetreden bij het bijwerken van de rechten: ${(err as Error).message}`, { type: 'error' })
        },
      })
  }

  return (
    <>
      <Button
        label={translate('share.shareButton')}
        disabled={disabled}
        onClick={() => setOpen(true)}
        style={{ float: 'right' }}
      >
        <LocationCityIcon />
      </Button>
      <Dialog
        fullWidth
        open={open}
        onClose={handleCloseClick}
        aria-label="share-dialog"
      >
        <DialogTitle id="share-dialog-title">{translate('share.dialogTitle')}</DialogTitle>
        <DialogContent style={{ height: '500px' }}>
          <Autocomplete
            disablePortal
            options={availableOrganizations}
            renderInput={(params) => <TextField {...params} label={translate('share.organization')} />}
            onChange={handleShare}
          />
          <List>
            {shares?.map(share => (
              <ListItem key={share.id}>
                <ListItemAvatar>
                  <Avatar sx={{ bgcolor: '#bbdefb', color: '#1e88e5' }}>
                    <LocationCityIcon />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText primary={share.organizationName} />
                <SplitButton
                  label={translate('share.shareOptions')}
                  options={[
                    { label: translate('share.VIEWER'), action: () => void handleUpdate(share, Rights.VIEWER) },
                    { label: translate('share.EDITOR'), action: () => void handleUpdate(share, Rights.EDITOR) },
                    { label: translate('share.OWNER'), action: () => void handleUpdate(share, Rights.OWNER) },
                  ]}
                  bottomOption={{ label: translate('share.remove'), action: () => void handleDelete(share) }}
                  selected={translate(`share.${share.rights}`)}
                  disabled={share.organizationUuid === organizationId}
                />
              </ListItem>
            ))}
          </List>
        </DialogContent>
        <DialogActions>
          <Button
            label={translate('share.done')}
            onClick={handleCloseClick}
            variant={'contained'}
          >
            <CheckIcon />
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}
