import { FC } from 'react'
import { Button, FormControl } from '@mui/material'

interface Props {
  display: boolean
  accept?: string
  onChange: (event) => void
}

export const UploadFileForImportButton: FC<Props> = ({display, onChange, accept}) => display ? (
  <FormControl>
    <input
      data-testid="uploadInput"
      style={{ display: 'none' }}
      id="upload-file"
      onChange={onChange}
      accept={accept ?? ''}
      type="file"
    />
    <label htmlFor="upload-file">
      <Button
        variant="contained"
        color="primary"
        component="span"
      >
        Selecteer bestand
      </Button>
    </label>
  </FormControl>
) : null