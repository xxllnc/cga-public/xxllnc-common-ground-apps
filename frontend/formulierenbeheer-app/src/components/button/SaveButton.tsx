import { FC, useEffect } from 'react'
import { Button, useNotify, useUpdate } from 'react-admin'
import { parseData, authToken } from 'xxllnc-react-components'
import SaveIcon from '@mui/icons-material/Save'
import { FormGetOneResponseDetails, Rights } from '../../types/generatedFormTypes'

interface SaveButtonProps {
  top?: boolean
  formData: FormGetOneResponseDetails
  schema: object | null
}

export const SaveButton: FC<SaveButtonProps> = ({ top = false, formData, schema }) => {
  const notify = useNotify()
  const organizationId = authToken.getOrganizationId()

  useEffect(() => {
    const unSavedData = localStorage.getItem(`form_${formData?.id}`)
    if (unSavedData && !top) {
      const parsed = parseData<object>(unSavedData)
      void saveFormSchema('forms', {
        id: formData.id,
        data: {
          ...formData,
          form: parsed ?? undefined
        },
        previousData: formData,
      },
      {
        onSuccess: () => {
          localStorage.removeItem(`form_${formData?.id}`)
          window.location.reload()
        }
      }
      )
    }
  }, [])

  const [saveFormSchema, { isLoading }] = useUpdate('forms', {
    id: formData.id,
    data: {
      ...formData,
      form: schema ?? undefined
    },
    previousData: formData,
  },
  {
    onSuccess: () => {
      localStorage.removeItem(`form_${formData?.id}`)
      notify('Formuliergegevens zijn opgeslagen', { type: 'info' })
    },
    onError: (err: Error) => {
      const errMsg = err?.message ?? 'Geen authenticatie token gevonden.'
      notify(`Er is een fout opgetreden: ${errMsg}`, { type: 'error' })
    }
  })

  const onSave = () => {
    localStorage.setItem(`form_${formData?.id}`, JSON.stringify(schema))
    void saveFormSchema()
  }

  const isEditorOrOwner = formData?.shares?.some(
    share => (
      (share?.rights === Rights.OWNER || share?.rights === Rights.EDITOR)
      && share.organizationUuid === organizationId
    )
  )

  const disableIfNotEditorOrOwner = !!!isEditorOrOwner

  return <Button
    variant={top ? 'text' : 'contained'}
    style={{ float: top ? 'unset' : 'right' }}
    onClick={onSave}
    disabled={(disableIfNotEditorOrOwner || isLoading)}
    label='Opslaan'
  >
    <SaveIcon />
  </Button>
}