import { AdminContext, ResourceContextProvider } from 'react-admin'
import { act, render, screen, waitFor, fireEvent } from '@testing-library/react'
import providers from '../../providers'
import { BackButton, ExportFormsButton, ShareFormButton, SplitButton } from '.'
import forms from '../../mocks/forms'
import { Form } from '../../types/forms'

describe('Buttons test', () => {

  it('should render the BackButton', () => {

    render(
      <AdminContext >
        <BackButton label="Test" />
      </AdminContext>
    )

    expect(screen.getByText('Test')).toBeDefined()
  })

  it('should render the ExportFormsButton', async () => {
    const dataProviderMock = providers.data

    act(() => {
      render(
        <AdminContext dataProvider={dataProviderMock} >
          <ResourceContextProvider value="forms">
            <ExportFormsButton />
          </ResourceContextProvider>
        </AdminContext>
      )
    })

    await act(() => waitFor(() => expect(screen.getByRole('progressbar')).toBeDefined()))
    expect(screen.getByText('ra.action.export')).toBeDefined()
  })

  it('should render the SplitButton', async () => {

    act(() => {
      render(
        <AdminContext >
          <SplitButton
            label={'Split button test'}
            options={[
              { label: 'A', action: () => console.log('A pressed') },
              { label: 'B', action: () => console.log('B pressed') },
              { label: 'C', action: () => console.log('C pressed') }
            ]}
            bottomOption={{ label: 'D', action: () => console.log('D pressed') }}
            selected={'A'}
          />
        </AdminContext>
      )
    })

    expect(screen.getByLabelText('Split button test')).toBeDefined()

    const inputFieldActive: HTMLInputElement = screen.getByLabelText('selecteer Split button test')
    act(() => { fireEvent.click(inputFieldActive) })
    await waitFor(() =>  expect(screen.getAllByRole('menuitem')).toHaveLength(4))

    const allMenuItems = screen.getAllByRole('menuitem')
    act(() => { fireEvent.click(allMenuItems[0]) })
    act(() => { fireEvent.click(allMenuItems[allMenuItems.length -1]) })
  })

  it('should render the ShareFormButton', async () => {
    const dataProviderMock = providers.data
    const mockForm = forms[0]

    act(() => {
      render(
        <AdminContext dataProvider={dataProviderMock} >
          <ResourceContextProvider value="forms">
            <ShareFormButton
              id={mockForm.id}
              shares={mockForm.shares as Form[]}
              disabled={false}
            />
          </ResourceContextProvider>
        </AdminContext>
      )
    })

    expect(screen.getByText('share.shareButton')).toBeDefined()

    const shareButton: HTMLInputElement = screen.getByLabelText('share.shareButton')
    act(() => { fireEvent.click(shareButton) })
    await waitFor(() =>  expect(screen.getByText('share.done')).toBeDefined())

    const searchElement: HTMLInputElement = screen.getByRole('combobox')
    await waitFor(() => fireEvent.input(searchElement, { target: { value: 'Exxellence' } }))

    const doneButton: HTMLInputElement = screen.getByLabelText('share.done')
    act(() => { fireEvent.click(doneButton) })
  })
})

