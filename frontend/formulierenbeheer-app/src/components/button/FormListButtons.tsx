import ContentCopyIcon from '@mui/icons-material/ContentCopy'
import EditIcon from '@mui/icons-material/Edit'
import ImageEye from '@mui/icons-material/RemoveRedEye'
import SettingsIcon from '@mui/icons-material/Settings'
import { FC, memo, MouseEvent } from 'react'
import { Button, ButtonProps, CloneButton, EditButton, useRecordContext } from 'react-admin'
import { Link } from 'react-router-dom'
import { getPublicFormUrl } from '../../AppUrls'
import { getFormSearchString, isActiveForm } from '../../utils'
import { authToken, xxllncColor } from 'xxllnc-react-components'
import { Rights } from '../../types/generatedFormTypes'
import { createTheme } from '@mui/material'

const material = createTheme()
const buttonSx = {
  'minWidth': 40,
  'color': xxllncColor.dark100,
  'span > svg': {
    fontSize: `${material.spacing(3)} !important`
  }
}

export const FormListButtons: FC<ButtonProps> = (props) => {
  const { ...rest } = props

  const organizationId = authToken.getOrganizationId()

  const record = useRecordContext()
  const isOwner = record?.organizationUuid === organizationId

  const onShowHandler = (_event: MouseEvent) => {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    const newWindow = window.open(`${getPublicFormUrl()}?formId=${record?.slug}`, '_blank', 'noopener,noreferrer')
    if (newWindow) newWindow.opener = null
  }


  return (
    <nav style={{ display: 'flex', flexDirection: 'row-reverse', minWidth: 160 }}>
      <Button
        disabled={!isActiveForm(record) || !isOwner}
        onClick={onShowHandler}
        sx={buttonSx}
        {...rest}
      >
        <ImageEye />
      </Button>
      <CloneButton
        label=""
        sx={buttonSx}
        icon={<ContentCopyIcon />}
        {...props}
      />
      <EditButton
        label=""
        sx={buttonSx}
        icon={<SettingsIcon />}
        {...props}
      />
      <Button
        component={Link}
        label=""
        sx={buttonSx}
        to={{
          pathname: '/formioeditor',
          search: getFormSearchString(record)
        }}>
        <EditIcon />
      </Button>
    </nav>
  )
}

export default memo(FormListButtons)
