import { FC } from 'react'
import { Button, useNotify, useTranslate } from 'react-admin'
import { useWatch } from 'react-hook-form'
import DownloadIcon from '@mui/icons-material/GetApp'
import { downloadFile } from '../../utils'

export const ExportScssButton: FC = () => {
  const translate = useTranslate()
  const notify = useNotify()
  const value = useWatch<{ value: string }>({ name: 'value' })

  const exportScss = () => {
    if (!value) {
      notify(translate('cssSettings.exportError'), { type: 'info' })
      return
    }
    downloadFile({ content: value, fileName: 'scssExport.scss', contentType: 'text/x-scss' })
    notify(translate('cssSettings.exportedMessage'), { type: 'info' })
  }

  return (
    <Button
      label="ra.action.export"
      onClick={exportScss}
    >
      <DownloadIcon />
    </Button>
  )
}