import PublishIcon from '@mui/icons-material/Publish'
import CloseIcon from '@mui/icons-material/Close'
import CheckIcon from '@mui/icons-material/Check'
import SkipNextIcon from '@mui/icons-material/SkipNext'
import { Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material'
import { useEffect, useState } from 'react'
import {
  Button, useCreate, useNotify, useRefresh
} from 'react-admin'
import { parseData } from 'xxllnc-react-components'
import { Form } from '../../types/forms'
import { Status } from '../../types/generatedFormTypes'
import { ImportForm, ImportFormError, RequestError } from '../form'
import { Spinner } from '../progress'
import { UploadFileForImportButton } from './UploadFileForImportButton'


export const ImportFormsButton = (): JSX.Element => {
  const notify = useNotify()
  const refresh = useRefresh()
  const [open, setOpen] = useState(false)
  const [uploaded, setUploaded] = useState(false)
  const [forms, setForms] = useState<Form[]>()
  const [totalForms, setTotalForms] = useState<number>(0)
  const [error, setError] = useState<undefined | RequestError>()
  const [create, { isLoading, error: createError }] = useCreate()

  const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms))

  const onNextForm = () => {
    setError(undefined)
    setForms(forms?.filter((_, i) => i !== 0))
  }

  useEffect(() => {
    let isActive = true
    if (!open) {
      // Delay reset of variables to make sure the popup is closed before they are reset
      void sleep(500).then(() => {
        if (isActive) {
          setForms(undefined)
          setUploaded(false)
          setTotalForms(0)
          setError(undefined)
        }
      })
    }
    return () => { isActive = false }
  }, [open])

  // We need to use local state for the error so we can reset it when the popup is closed
  useEffect(() => setError((createError as RequestError)), [createError])

  useEffect(() => {
    // After uploading a form we remove the form if it has no errors
    if (forms && !isLoading && !createError) {
      onNextForm()
      refresh()
      if (forms.length <= 1) setUploaded(true)
    }
  }, [createError, isLoading])

  const handleCloseClick = (_event?: any, reason?: 'backdropClick' | 'escapeKeyDown') => {
    if (reason && isLoading) return
    setOpen(false)
  }

  const handleContinueClick = () => {
    if (uploadIsCompleted() || !forms)
      return handleCloseClick()
    void create('forms', { data: forms[`${0}`] })
  }

  const onFileChange = (event: any) => {
    const fileReader = new FileReader()
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument, @typescript-eslint/no-unsafe-member-access
    fileReader.readAsText(event?.target?.files[0], 'UTF-8')
    fileReader.onload = e => {
      const result = parseData<Form[]>(e?.target?.result as unknown as string)
      if (result) {
        if (!Array.isArray(result) || !result[0].id) {
          notify('Het geselecteerde bestand bevat geen (geldige) formulieren', { type: 'warning' })
          return
        }
        setTotalForms(result.length)
        setForms(result.map(f => ({ ...f, status: Status.INACTIVE })))
      } else {
        notify('Het bestand kon niet geupload worden', { type: 'warning' })
      }
    }
  }

  const onTextFieldChange = (param: string, value: string) => {
    setForms(forms?.map((form, i) => {
      if (i === 0) {
        form[`${param}`] = value
        // If the name is changed, we also want to change the title in the form object
        if (param === 'name')
          form.title = value
      }
      return form
    }))
  }

  const getTitle = () => {
    if (!forms || totalForms === 0) return 'Selecteer een bestand om te importeren'
    if (forms.length === 0) return 'Alle formulieren zijn geïmporteerd'
    if (totalForms === 1) return 'Importeer formulier'
    return `Importeer formulier ${(totalForms - forms.length) + 1}/${totalForms}`
  }

  const uploadIsCompleted = () => {
    if (uploaded) return true
    return forms && forms.length === 0
  }

  return (
    <>
      <Button
        label="Importeren"
        onClick={() => setOpen(true)}
      >
        <PublishIcon />
      </Button>
      <Dialog
        fullWidth
        open={open}
        onClose={handleCloseClick}
        aria-label="import-dialog"
      >
        <DialogTitle id="import-dialog-title">{getTitle()}</DialogTitle>
        <DialogContent style={{ display: 'flex', flexDirection: 'column', alignItems: 'left' }}>
          <UploadFileForImportButton
            display={!forms && !uploaded}
            onChange={onFileChange}
            accept="application/json"
          />
          <ImportFormError error={error} />
          <ImportForm
            form={forms?.find(x => x !== undefined)}
            onChange={onTextFieldChange}
            disabled={isLoading}
          />
          <Spinner display={isLoading} />
        </DialogContent>
        <DialogActions>
          {!uploadIsCompleted() &&
            <Button
              disabled={isLoading}
              label="Annuleren"
              onClick={handleCloseClick}
              variant={'contained'}
              color="secondary"
            >
              <CloseIcon />
            </Button>}
          {forms && (forms.length > 1 || (forms.length > 0 && totalForms > 1)) && <Button
            disabled={isLoading}
            label="Overslaan"
            onClick={onNextForm}
            variant={'contained'}
            color="secondary"
          >
            <SkipNextIcon />
          </Button>}
          <Button
            disabled={!forms || isLoading}
            label={uploadIsCompleted() ? 'Klaar' : 'Importeer'}
            onClick={handleContinueClick}
            variant={'contained'}
          >
            {uploadIsCompleted() ? <CheckIcon /> : <PublishIcon />}
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}