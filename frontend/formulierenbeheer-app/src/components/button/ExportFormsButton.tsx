import { useState } from 'react'
import DownloadIcon from '@mui/icons-material/GetApp'
import { BulkActionProps, Button, useNotify, useUnselectAll, useGetList, Confirm, useTranslate } from 'react-admin'
import { format } from 'date-fns'
import { Form } from '../../types/forms'
import { downloadFile } from '../../utils'
import { Spinner } from '../progress'
import { authToken } from 'xxllnc-react-components'

export const ExportFormsButton = ({ selectedIds, filterValues }: BulkActionProps): JSX.Element => {
  const notify = useNotify()
  const translate = useTranslate()
  const [showConfirm, setShowConfirm] = useState(false)

  const unselectAll = useUnselectAll('forms')
  const { data: allForms, isLoading } = useGetList<Form>('forms')

  const clickHandler = () => {
    if (!selectedIds || !allForms) {
      notify(translate('formExport.failedMessage'), { type: 'info' })
      return
    }
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    if (filterValues && Object.keys(filterValues).length > 0) {
      setShowConfirm(true)
      return
    }
    exportForms()
  }

  const confirmHandler = (confirmed: boolean) => {
    if (confirmed) exportForms()
    setShowConfirm(false)
  }

  const exportForms = () => {
    const selectedForms = allForms?.filter(form => selectedIds?.includes(form.id))
    const amount = selectedForms?.length || 0
    const date = format(new Date(), 'yyyy-MM-dd')
    const organizationName = authToken.getOrganizationName()
    const fileName = `${organizationName}_export_${amount}-forms__${date}.json`

    downloadFile({ content: JSON.stringify(selectedForms), fileName, contentType: 'application/json' })
    notify(translate('formExport.successMessage', { amount }), { type: 'info' })
    unselectAll()
  }

  if (isLoading)
    return <Spinner display />

  return (
    <>
      <Button
        label="ra.action.export"
        onClick={clickHandler}
      >
        <DownloadIcon />
      </Button>
      <Confirm
        isOpen={showConfirm}
        title={translate('formExport.confirmTitle')}
        content={translate('formExport.confirmMessage')}
        onConfirm={() => confirmHandler(true)}
        onClose={() => confirmHandler(false)}
        confirm={translate('formExport.confirm')}
        cancel={translate('formExport.cancel')}
      />
    </>
  )
}