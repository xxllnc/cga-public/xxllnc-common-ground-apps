import { useState, FC } from 'react'
import { Button } from 'react-admin'
import { Dialog, DialogContent, DialogTitle} from '@mui/material'
import PreviewIcon from '@mui/icons-material/Preview'
import CloseIcon from '@mui/icons-material/Close'
import { authToken } from 'xxllnc-react-components'
import { FormGetOneResponseDetails, Rights } from '../../types/generatedFormTypes'


import { getPublicFormUrl } from '../../AppUrls'

interface Props {
  formData: FormGetOneResponseDetails
  slug: string
  active: string
}

export const PreviewFormButton: FC<Props> = ({ formData, slug, active }) => {
  const [open, setOpen] = useState(false)
  const organizationId = authToken.getOrganizationId()

  const isEditorOrOwner = formData?.shares?.some(
    share => (
      (share?.rights === Rights.OWNER || share?.rights === Rights.EDITOR)
      && share.organizationUuid === organizationId
    )
  )

  return (
    <>
      <Button
        label="Preview"
        onClick={() => setOpen(true)}
        disabled={!isEditorOrOwner || active === 'false'}
      >
        <PreviewIcon />
      </Button>
      <Dialog
        fullWidth
        maxWidth="lg"
        open={open}
        onClose={() => setOpen(false)}
        aria-label="import-dialog"
      >
        <DialogTitle style={{ display: 'flex', justifyContent: 'end', flexDirection: 'row' }}>
          <Button aria-label='Close' onClick={() => {setOpen(false)}}>
            <CloseIcon/>
          </Button>
        </DialogTitle>
        <DialogContent>

          <div style={{ width: '100%', height: 750, padding: 0, overflow: 'hidden' }}>
            <iframe
              id="preview"
              style={{
                width: '100%',
                height: '100%'
              }}
              src={`${getPublicFormUrl()}?formId=${slug}`}
            />
          </div>
        </DialogContent>
      </Dialog>
    </>
  )
}