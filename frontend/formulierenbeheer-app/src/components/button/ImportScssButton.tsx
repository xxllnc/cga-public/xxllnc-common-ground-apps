import { useState, FC } from 'react'
import { Button, useNotify, useTranslate } from 'react-admin'
import { Dialog, DialogTitle, DialogContent, DialogActions } from '@mui/material'
import { useFormContext } from 'react-hook-form'
import PublishIcon from '@mui/icons-material/Publish'
import CloseIcon from '@mui/icons-material/Close'
import { UploadFileForImportButton } from './UploadFileForImportButton'

export const ImportScssButton: FC = () => {
  const translate = useTranslate()
  const notify = useNotify()
  const [open, setOpen] = useState(false)
  const { setValue } = useFormContext()

  const handleCloseClick = (_event?: any) => setOpen(false)

  const onFileChange = (event: { target: HTMLInputElement }) => {
    const fileReader = new FileReader()

    if (event?.target?.files?.[0]) {
      fileReader.readAsText(event?.target?.files[0], 'UTF-8')
      fileReader.onload = e => {
        const result = e?.target?.result
        if (result) {
          setValue('value', result)
          setOpen(false)
        } else {
          notify('cssSettings.importError', { type: 'warning' })
        }
      }
    }
  }

  return (
    <>
      <Button
        label="cssSettings.import"
        onClick={() => setOpen(true)}
      >
        <PublishIcon />
      </Button>
      <Dialog
        fullWidth
        open={open}
        onClose={handleCloseClick}
        aria-label="import-dialog"
      >
        <DialogTitle id="import-dialog-title">{translate('cssSettings.importDialogTitle')}</DialogTitle>
        <DialogContent style={{ display: 'flex', flexDirection: 'column', alignItems: 'left' }}>
          <UploadFileForImportButton
            display={true}
            onChange={onFileChange}
            accept='text/x-scss'
          />
        </DialogContent>
        <DialogActions>
          <Button
            label={translate('cancel')}
            onClick={handleCloseClick}
            variant={'contained'}
            color="secondary"
          >
            <CloseIcon />
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}