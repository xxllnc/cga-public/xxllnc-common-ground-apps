import { AdminUI, Resource, ResourceProps } from 'react-admin'
import FormRequests from './forms'
import Requests from './requests'
import SharesRequests from './shares'
import About from './pages/about'
import GeneralSettings from './pages/generalSettings'

interface Permissions {
  scopes?: string[]
}

export interface CustomResourceProps extends ResourceProps {
  options: {
    label: string
    settings?: boolean
    scopes: string[]
    isSubMenu?: boolean
  }
}

const availableResources = (_permissions: Permissions): JSX.Element[] => [
  <Resource {...FormRequests} />,
  <Resource {...Requests} />,
  <Resource {...SharesRequests} />,
  <Resource {...GeneralSettings} />,
  <Resource {...About} />,
  <Resource
    name="settingTypes"
    options={{
      label: 'Instellingstype',
      scopes: ['cga.admin', 'cga.user'],
      settings: false
    }}
  />,
  <Resource
    name="share"
    options={{
      label: 'Delen',
      scopes: ['cga.admin'],
      settings: false
    }}
  />,
  <Resource
    name="organizations"
    options={{
      label: 'organizations',
      scopes: ['cga.admin'],
      settings: false
    }}
  />
]

const fallback = (): JSX.Element[] => [
  <Resource
    name={FormRequests.name}
    list={FormRequests.list}
    icon={FormRequests.icon}
    options={{
      label: 'Formulieren'
    }}
  />
]

interface AResource {
  props: CustomResourceProps
}

export const resourcesForScopes = ({ scopes }: Permissions): JSX.Element[] => {

  if (!scopes || scopes.length === 0)
    return fallback()

  const resources = availableResources({ scopes })
    .filter((resource: AResource) => {
      const resourceWithScope = resource.props.options.scopes.find(scope => scopes.includes(scope))
      return resourceWithScope !== undefined
    })
  if (!resources || resources.length === 0)
    return fallback()

  return resources
}