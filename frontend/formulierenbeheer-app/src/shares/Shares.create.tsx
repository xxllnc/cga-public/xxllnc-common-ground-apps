import { FC } from 'react'
import {
  Create,
  CreateProps,
  DateInput,
  BooleanInput,
  RadioButtonGroupInput,
  SimpleForm,
  TextInput,
  maxLength,
  minLength,
  required,
  RaRecord,
  FormDataConsumer,
} from 'react-admin'
import { useLocation } from 'react-router'
import { authToken, HeaderTitle } from 'xxllnc-react-components'
import { Rights, Status } from '../types/generatedFormTypes'

export interface CreateForm extends RaRecord {
  name?: string
  publishDate?: string
  status?: string
}

const redirect = () => '/forms'

const SharesCreate: FC<CreateProps> = (props) => {

  const location = useLocation()
  const params = new URLSearchParams(location?.search)
  const formUuid = params.get('formUuid')
  const name = params.get('name')
  const slug = params.get('slug')
  const dateTime = new Date()
  const currentDateString = `${dateTime.getFullYear()}-${dateTime.getMonth() + 1}-${dateTime.getDate()}`
  const organizationId = authToken.getOrganizationId()
  const organizationName = localStorage.getItem('organization_name') ?? ''

  return (
    <Create
      {...props}
      title={<HeaderTitle title="Share toevoegen" />}
      redirect={redirect}
    >
      <SimpleForm defaultValues={{ status: Status.INACTIVE }} >
        <TextInput
          label="Naam"
          source="name"
          defaultValue={name ?? ''}
          validate={[required(), minLength(1), maxLength(255)]}
        />
        <TextInput source="id" hidden defaultValue={formUuid} />
        <TextInput source="rights" hidden defaultValue={Rights.VIEWER} />
        <TextInput source="organizationUuid" hidden defaultValue={organizationId} />
        <TextInput source="organizationName" hidden defaultValue={organizationName} />
        <TextInput source="slug" hidden defaultValue={slug} />
        <BooleanInput label='privé' source="private" defaultValue={false} />
        <RadioButtonGroupInput
          label="Status"
          source="status"
          choices={[
            { id: Status.ACTIVE, name: 'Actief' },
            { id: Status.INACTIVE, name: 'Inactief' },
          ]}
        />
        <FormDataConsumer>
          {({ formData: localFormDate, ...rest }: { formData: { status: string } }) =>
            localFormDate.status === Status.ACTIVE &&
            <DateInput
              label="Publicatiedatum"
              source="publishDate"
              defaultValue={currentDateString}
              {...rest}
            />
          }
        </FormDataConsumer>
      </SimpleForm>
    </Create>
  )
}

export default SharesCreate
