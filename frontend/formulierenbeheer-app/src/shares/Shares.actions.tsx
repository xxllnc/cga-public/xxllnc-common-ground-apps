import { FC } from 'react'
import { EditActionsProps } from 'react-admin'
import { ActionsToolbar } from '../components'

export const ShareEditActions: FC<EditActionsProps> = () => (
  <ActionsToolbar source="share" />
)
