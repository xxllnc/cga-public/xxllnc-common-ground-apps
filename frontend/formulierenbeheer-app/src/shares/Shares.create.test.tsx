import { render, screen, waitFor } from '@testing-library/react'
import { AdminContext, ResourceContextProvider } from 'react-admin'
import providers from '../providers'
import SharesCreate from './Shares.create'


describe('Shares create test', () => {

  it('should render the create', async () => {

    const dataProviderMock = providers.data

    //Given
    render(
      <AdminContext dataProvider={dataProviderMock} >
        <ResourceContextProvider value="shares">
          <SharesCreate />
        </ResourceContextProvider>
      </AdminContext>
    )

    // We expect 'Naam *' as required label for one input field
    await waitFor(() => expect(screen.getByLabelText('Naam *')).toBeDefined())


  })
})
