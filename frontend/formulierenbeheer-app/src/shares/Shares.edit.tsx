import { FC, useState } from 'react'
import {
  DateInput,
  Edit,
  EditProps,
  RadioButtonGroupInput,
  SimpleForm,
  TextInput,
  maxLength,
  minLength,
  required,
  useNotify,
  useRefresh,
  useRedirect,
  Confirm,
  SimpleFormIterator,
  ArrayInput,
  FormDataConsumer
} from 'react-admin'
import { HeaderTitle } from 'xxllnc-react-components'
import { FormControlLabel, FormGroup, Switch } from '@mui/material'
import { format } from 'date-fns'
import { ErrorType, getErrorMessage } from '../utils/getErrorMessage'
import { Status } from '../types/generatedFormTypes'
import { ShareEditActions } from './Shares.actions'

const SharesEdit: FC<EditProps> = (props) => {
  const [showPopup, setPopup] = useState(false)
  const [changeSlug, setChangeSlug] = useState(false)
  const notify = useNotify()
  const refresh = useRefresh()
  const redirect = useRedirect()
  const confirmText = 'De verkorte naam wordt gebruikt in de URL van het\
  formulier. Bij het wijzigen van de verkorte naam veranderd dus ook de URL \
  van het formulier. Mocht er ergens verwezen worden naar dit formulier\
  middels een URL dan zal dit aangepast moeten worden.'

  const onSuccess = () => {
    notify('Wijzigingen zijn opgeslagen', { type: 'info', messageArgs: null, undoable: false })
    redirect('list')
    refresh()
  }

  const onFailure = (error: unknown) => {
    notify(`Er is een fout opgetreden: ${getErrorMessage(error as ErrorType)}`, { type: 'error' })
  }

  const handleToggle = (_, checked: boolean) => {
    if (checked) {
      setPopup(true)
    } else {
      setChangeSlug(false)
    }
  }

  const handleConfirmAndClose = (confirm: boolean) => {
    setChangeSlug(confirm)
    setPopup(false)
  }

  return (
    <Edit
      {...props}
      title={<HeaderTitle title="Share %name% aanpassen" />}
      actions={<ShareEditActions />}
      mutationOptions={{
        onSuccess,
        onError: onFailure
      }}
      mutationMode="pessimistic"
    >
      <SimpleForm>
        <TextInput
          label={'Naam'}
          source="name"
          validate={[required(), minLength(1), maxLength(255)]}
        />
        <RadioButtonGroupInput
          label={'Status'}
          source="status"
          choices={[
            { id: Status.ACTIVE, name: 'Actief' },
            { id: Status.INACTIVE, name: 'Inactief' },
          ]}
        />
        <FormDataConsumer>
          {({ formData, ...rest }: { formData: { status: string } }) =>
            formData.status === Status.ACTIVE &&
            <DateInput
              label="Publicatiedatum"
              source="publishDate"
              defaultValue={format(new Date(), 'yyyy-MM-dd')}
              {...rest}
            />
          }
        </FormDataConsumer>
        <FormGroup>
          <FormControlLabel
            label="Verkorte naam aanpassen"
            control={<Switch
              checked={changeSlug}
              color={'primary'}
              onChange={handleToggle} />
            } />
        </FormGroup>
        <TextInput
          label={'Verkorte naam'}
          source="slug"
          disabled={!changeSlug}
          fullWidth
          validate={[required(), minLength(1), maxLength(255)]}
        />
        <Confirm
          isOpen={showPopup}
          title="Weet je zeker dat je de verkorte naam wilt aanpassen?"
          content={confirmText}
          onConfirm={() => handleConfirmAndClose(true)}
          onClose={() => handleConfirmAndClose(false)}
          confirm="Ja"
          cancel="Nee"
        />
        <FormDataConsumer>
          {({ formData }: { formData: { config: string } }) =>
            formData.config &&
            <ArrayInput label={'Config'} source="config">
              <SimpleFormIterator>
                <TextInput label={'Instelling'} source="key" validate={[required()]} />
                <TextInput label={'Waarde'} source="value" validate={[required()]} />
              </SimpleFormIterator>
            </ArrayInput>
          }
        </FormDataConsumer>
      </SimpleForm>
    </Edit>
  )
}

export default SharesEdit
