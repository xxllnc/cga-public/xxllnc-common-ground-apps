import { fireEvent, act, render, screen, waitFor } from '@testing-library/react'
import { AdminContext, ResourceContextProvider } from 'react-admin'
import providers from '../providers'
import SharesEdit from './Shares.edit'


describe('Shares edit test', () => {
  it('should render the edit', async () => {
    const dataProviderMock = providers.data

    render(
      <AdminContext dataProvider={dataProviderMock} >
        <ResourceContextProvider value="shares">
          <SharesEdit id={'c16ae581-8d6f-424e-bff3-3a70a1374f0a'} />
        </ResourceContextProvider>
      </AdminContext>
    )

    await waitFor(() => expect(screen.getByLabelText('Naam *')).toBeDefined())
  })

  it('should display confirm popup', async () => {
    const dataProviderMock = providers.data

    render(
      <AdminContext dataProvider={dataProviderMock} >
        <ResourceContextProvider value="shares">
          <SharesEdit id={'c16ae581-8d6f-424e-bff3-3a70a1374f0a'} />
        </ResourceContextProvider>
      </AdminContext>
    )

    await waitFor(() => expect(screen.getByLabelText('Naam *')).toBeDefined())

    const editShortName: HTMLInputElement = screen.getByLabelText('Verkorte naam aanpassen')
    act(() => { fireEvent.click(editShortName) })

    expect(editShortName.value).toBe('on')

    act(() => { fireEvent.click(screen.getByRole('button', { name: 'Ja' })) })
  })


  it('should update the form', async () => {
    const dataProviderMock = providers.data

    render(
      <AdminContext dataProvider={dataProviderMock} >
        <ResourceContextProvider value="shares">
          <SharesEdit id={'c16ae581-8d6f-424e-bff3-3a70a1374f0a'} />
        </ResourceContextProvider>
      </AdminContext>
    )

    await waitFor(() => expect(screen.getByLabelText('Naam *')).toBeDefined())

    const nameInput = screen.getByLabelText('Naam *')
    fireEvent.change(nameInput, { target: { value: 'test name' } })

    const saveButton: HTMLInputElement = screen.getByRole('button', { name: 'ra.action.save' })
    act(() => { fireEvent.click(saveButton) })
  })
})
