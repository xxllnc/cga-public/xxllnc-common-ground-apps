import { CustomResourceProps } from '../Resources'
import SharesCreate from './Shares.create'
import SharesEdit from './Shares.edit'

const FormRequests: CustomResourceProps = {
  name: 'share',
  create: SharesCreate,
  edit: SharesEdit,
  options: {
    label: 'share',
    scopes: ['cga.admin', 'cga.user']
  }
}

export default FormRequests
