import simpleRestProvider from 'ra-data-simple-rest'
import polyglotI18nProvider from 'ra-i18n-polyglot'
import { DataProvider, fetchUtils } from 'react-admin'
import { authProvider, authToken } from 'xxllnc-react-components'
import { getRequestsUrl, getSettingsUrl, getUrl } from '../AppUrls'
import { i18nDutch } from '../i18n'
import { mock } from '../utils/mock'
import mockDataProvider from './data'

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore
const i18nProvider = polyglotI18nProvider(() => i18nDutch, 'nl', { allowMissing: true })

interface Response { headers: Headers, json: any, body: string, status: number }
const httpClient = (url: string, options: RequestInit = {}): Promise<Response> => {

  const token = authToken.getToken() ?? null
  if (!token)
    return Promise.reject(process.env?.NODE_ENV === 'development' ? 'No user Token' : '')

  const user = { authenticated: true, token }
  const headers = new Headers({ 'organization-url': window.origin, ...options?.headers })

  return fetchUtils.fetchJson(url, { ...options, user, headers })
}

const getDataProvider = () => {

  const test = process.env?.NODE_ENV === 'test'
  if (mock())
    console.log('mock is set to true!')

  return !mock() && !test ? myDataProvider : mockDataProvider
}

const getProviderForResource = (resource: string) => {
  switch (resource) {
    case 'settings':
    case 'settingTypes': return settingsProvider
    case 'requests/admin': return requestsProvider
  }
  return normalProvider
}

const normalProvider = simpleRestProvider(getUrl(), httpClient)
const settingsProvider = simpleRestProvider(getSettingsUrl(), httpClient)
const requestsProvider = simpleRestProvider(getRequestsUrl(), httpClient)

const myDataProvider: DataProvider = {
  ...normalProvider,
  getList: (resource, params) => getProviderForResource(resource).getList(resource, params),
  getMany: (resource, params) => getProviderForResource(resource).getMany(resource, params),
  getOne: (resource, params) => getProviderForResource(resource).getOne(resource, params),
  create: (resource, params) => getProviderForResource(resource).create(resource, params),
  update: (resource, params) => getProviderForResource(resource).update(resource, params),
  delete: (resource, params) => getProviderForResource(resource).delete(resource, params),
}

const providers = {
  auth: authProvider,
  data: getDataProvider(),
  i18n: i18nProvider,
  url: getUrl()
}

export default providers
