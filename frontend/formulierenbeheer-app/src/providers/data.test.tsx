import providers from './'

describe('dataProvider test', () => {

  it('should get i18n from provider', () => {
    const i18n = providers.i18n

    expect(i18n.getLocale()).toStrictEqual('nl')
  })

  it('should create a form with create from provider', async () => {
    const data = providers.data

    const result = await data.create('forms', {
      data: {
        id: '1'
      }
    })

    expect(result.data).toStrictEqual({ id: '1'})
  })
  it('should delete a form with delete from provider', async () => {
    const data = providers.data

    const result = await data.delete('forms', { id: 'c16ae581-8d6f-424e-bff3-3a70a1374f0a' })

    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    expect(result.data.id).toStrictEqual('c16ae581-8d6f-424e-bff3-3a70a1374f0a')
  })
  it('should getMany from provider', async () => {
    const data = providers.data

    const result = await data.getMany('forms', { ids: ['c16ae581-8d6f-424e-bff3-3a70a1374f0a'] })

    expect(result.data).toStrictEqual([])
  })
  it.skip('should get getManyReference from provider', () => {
    const data = providers.data

    global.fetch = jest.fn(() =>
      Promise.resolve({
        ok: true,
        json: () => Promise.resolve({ data: 'test' }),
      })
    ) as jest.Mock

    void data.getManyReference('forms', {
      target: 'requests',
      id: 'c16ae581-8d6f-424e-bff3-3a70a1374f0a',
      pagination: { page: 1, perPage: 5 },
      sort: { field: 'id', order: 'DESC' },
      filter: { }
    })

    expect(fetch).toHaveBeenCalled()
  })
  it.skip('should get update from provider', () => {
    const data = providers.data

    global.fetch = jest.fn(() =>
      Promise.resolve({
        ok: true,
        json: () => Promise.resolve({ data: 'test' }),
      })
    ) as jest.Mock

    void data.update('forms', {
      id: 'c16ae581-8d6f-424e-bff3-3a70a1374f0a',
      data: {
        id: 'c16ae581-8d6f-424e-bff3-3a70a1374f0a',
        name: 'New name'
      },
      previousData: {
        id: 'c16ae581-8d6f-424e-bff3-3a70a1374f0a',
        name: 'formulier a'
      }
    })

    expect(fetch).toHaveBeenCalled()
  })
  it.skip('should get updateMany from provider', () => {
    const data = providers.data

    global.fetch = jest.fn(() =>
      Promise.resolve({
        ok: true,
        json: () => Promise.resolve({ data: 'test' }),
      })
    ) as jest.Mock

    void data.updateMany('forms', {
      ids: ['c16ae581-8d6f-424e-bff3-3a70a1374f0a'],
      data: {
        id: 'c16ae581-8d6f-424e-bff3-3a70a1374f0a',
        name: 'New name'
      },
    })

    expect(fetch).toHaveBeenCalled()
  })
  it.skip('should get deleteMany from provider', () => {
    const data = providers.data

    global.fetch = jest.fn(() =>
      Promise.resolve({
        ok: true,
        json: () => Promise.resolve({ data: 'test' }),
      })
    ) as jest.Mock

    void data.deleteMany('forms', {
      ids: ['c16ae581-8d6f-424e-bff3-3a70a1374f0a'],
    })

    expect(fetch).toHaveBeenCalled()
  })
  it('should use auth provider', async () => {
    const auth = providers.auth({
      getAccessTokenSilently: (options?: any, config?: any): Promise<any> => { throw new Error('Function not implemented.') },
      getAccessTokenWithPopup: (options?: any, config?: any): Promise<string> => { throw new Error('Function not implemented.') },
      getIdTokenClaims: (options?: any): Promise<any> => { throw new Error('Function not implemented.') },
      loginWithRedirect: (options?: any): Promise<void> => { throw new Error('Function not implemented.') },
      loginWithPopup: (options?: any, config?: any): Promise<void> => { throw new Error('Function not implemented.') },
      logout: (options?: any) => { throw new Error('Function not implemented.') },
      buildAuthorizeUrl: (options?: any): Promise<string> =>  { throw new Error('Function not implemented.') },
      buildLogoutUrl: (options?: any): string => { throw new Error('Function not implemented.') },
      handleRedirectCallback: (url?: string): Promise<any> => { throw new Error('Function not implemented.') },
      isAuthenticated: false,
      isLoading: false
    }, 'returnTo', true)

    const isLoggedIn = await auth.checkAuth({})
    expect(isLoggedIn).toBe(undefined)
  })
})
