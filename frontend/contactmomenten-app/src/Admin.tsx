import { FC } from 'react'
import { Admin as ReactAdmin, CustomRoutes } from 'react-admin'
import { CustomLayout, theme, useAuth0User } from 'xxllnc-react-components'
import { Route } from 'react-router-dom'
import AboutPage from './pages/about/About'
import LoginPage from './pages/login/Login'
import providers from './providers'
import { resourcesForScopes } from './Resources'
import { mock } from './utils'

const returnTo = `${window.location.origin}${process.env.PUBLIC_URL}/login`

const Admin: FC = () => {

  const auth0 = useAuth0User()

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  return (<ReactAdmin
    i18nProvider={providers.i18n}
    authProvider={providers.auth(auth0, returnTo, mock())}
    dataProvider={providers.data}
    loginPage={LoginPage}
    layout={CustomLayout}
    disableTelemetry
    theme={theme}
  >
    {resourcesForScopes}
    <CustomRoutes>
      <Route path="/about" element={<AboutPage />} />
    </CustomRoutes>
  </ReactAdmin>
  )
}

export default Admin
