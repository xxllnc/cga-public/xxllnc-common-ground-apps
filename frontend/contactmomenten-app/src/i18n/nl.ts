import { TranslationMessages } from 'ra-core'
import dutchMessages from 'ra-language-dutch'
import { nl } from 'xxllnc-react-components'

const translations: TranslationMessages = dutchMessages as unknown as TranslationMessages

export const i18nDutch = {
  ...translations,
  ...nl,
  contact: 'Contact',
  id: 'Identificatie',
  createdByName: 'Aangemaakt door',
  createdAt: 'Aanmaakdatum',
  contactPersoon: 'Contactpersoon',
  noResults: 'De zoekopdracht gaf geen resultaten',
  searchDescription: 'Zoek een persoon in haalcentraal. U kunt zoeken op een combinatie van achternaam en geboortedatum of bsn.',
  name: 'Naam',
  birthDate: 'Geboortedatum',
  postCode: 'Postcode',
  houseNumber: 'Huisnummer',
  bsn: 'Burgerservicenummer',
  search: 'Zoeken',
  result: 'Resultaat',
  results: 'Resultaten'
}
