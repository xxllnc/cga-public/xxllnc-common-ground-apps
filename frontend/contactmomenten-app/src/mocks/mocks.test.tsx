import { GetOneParams } from 'react-admin'
import mockDataProvider from '../providers/data'
import customFields from './customFields'


describe('Mocks test', () => {

  it('mockDataProvider should return the first mockRecource of customFields', async () => {

    const params: GetOneParams = {
      id: '1'
    }

    const customField = () => Promise.resolve({
      data: customFields[0]
    })

    expect(await mockDataProvider.getOne('customFields', params)).toEqual(await customField())
  })

})