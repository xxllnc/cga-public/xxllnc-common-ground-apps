/* eslint-disable */
import {
  CreateParams,
  CreateResult, DeleteParams,
  DeleteResult, GetListParams,
  GetListResult, GetManyParams,
  GetManyResult, GetOneParams, GetOneResult, UpdateParams
} from 'ra-core'
import { RaRecord } from 'react-admin'
import contactMoments from './contactMoments'
import customFields from './customFields'

const getResource = (resource: string) => {
  switch (resource) {
    case 'contacts': return contactMoments
    case 'customFields': return customFields
    default: return [{ id: 1, error: 'unknown resource' }]
  }
}

const sortObject = (key: string, order = 'ASC') => {
  return function innerSort(a: any, b: any) {
    if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) return 0

    const varA = (typeof a[key] === 'string') ? a[key].toUpperCase() : a[key]
    const varB = (typeof b[key] === 'string') ? b[key].toUpperCase() : b[key]

    let comparison = 0
    if (varA > varB) {
      comparison = 1
    } else if (varA < varB) {
      comparison = -1
    }
    return (order === 'DESC') ? (comparison * -1) : comparison
  }
}

const filterList = (list: any[], params: GetListParams) => {
  if (!list || list.length === 0) return list
  const { sort, filter } = params
  let sorted = list.sort(sortObject(sort.field, sort.order))

  if (filter) {
    for (const property in filter) {
      if (!filter.hasOwnProperty(property)) continue
      if (typeof filter[property] === 'string') {
        sorted = sorted.filter(item =>
          (item.hasOwnProperty(property) && item[property].includes(filter[property])) ||
          (property === 'q' && objectContainsSearchString(item, filter[property]))
        )
      }
      if (typeof filter[property] === 'number') {
        sorted = sorted.filter(item => item.hasOwnProperty(property) && filter[property] === item[property]
        )
      }
    }
  }
  return sorted
}

const objectContainsSearchString = (object, searchString) => {
  const array = getRecordProperties(object)
  const found = array.filter(item =>
    item.toLowerCase().includes(searchString.toLowerCase()))
  return found.length > 0
}

const getRecordProperties = (object) => {
  let values: string[] = []
  for (const property in object) {
    if (!object.hasOwnProperty(property)) continue
    if (typeof object[property] === 'string') {
      values.push(object[property])
    } else if (Array.isArray(object[property])) {
      object[property].forEach(item => {
        if (typeof item === 'string') values.push(item)
        if (Array.isArray(item)) item.forEach(child => {
          if (typeof child === 'string') values.push(child)
          if (typeof child === 'object') values = [...values, ...getRecordProperties(child)]
        })
        if (typeof item === 'object') values = [...values, ...getRecordProperties(item)]
      })
    } else if (typeof object[property] === 'object') {
      values = [...values, ...getRecordProperties(object[property])]
    }
  }
  return values
}

const getPageResult = (list: any[], params: GetListParams) => {
  if (!list || list.length === 0) return list
  const { pagination } = params
  const start = (pagination.page - 1) * pagination.perPage
  const end = start + pagination.perPage
  return list.slice(start, end)
}

const getList = <RecordType extends RaRecord>(resource: string, params: GetListParams): Promise<GetListResult<RecordType>> => {
  const list = getResource(resource) as unknown as RaRecord[]
  const data = filterList(list, params)
  const page = getPageResult(data, params)
  if (process.env?.NODE_ENV !== 'test') console.log({ type: 'getList', resource, list, data, page, params })
  return Promise.resolve({
    data: page,
    total: data.length
  })
}

const getOne = <RecordType extends RaRecord>(resource: string, params: GetOneParams): Promise<GetOneResult<RecordType>> => {
  const list = getResource(resource) as unknown as RaRecord[]
  if (!list || list.length === 0) return Promise.resolve({ data: { id: '1' } as any })
  const record = list.filter(item => String(item.id) === String(params.id))[0] as any
  if (process.env?.NODE_ENV !== 'test') console.log({ type: 'getOne', resource, list, record, params })
  return Promise.resolve({
    data: record
  })
}

const uuid = (): string =>
  'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8)
    return v.toString(16)
  })

const create = <RecordType extends RaRecord>(resource: string, params: CreateParams): Promise<CreateResult<RecordType>> => {
  let id = uuid()
  if (process.env?.NODE_ENV !== 'test') console.log({ type: 'create', resource, params, id })
  switch (resource) {
    case 'contactMoments':
      contactMoments.push({
        id,
        ...params.data,
        created_at: '2021-04-26 14:21',
        updated_at: '2021-04-26 14:21'
      })
      break
    case 'customFields':
      customFields.push({
        id,
        ...params.data
      })
      break
  }

  return Promise.resolve({
    data: {
      id,
      ...params.data
    }
  })
}

const deleteOne = <RecordType extends RaRecord>(resource: string, params: DeleteParams): Promise<DeleteResult<RecordType>> => {
  if (process.env?.NODE_ENV !== 'test') console.log({ type: 'delete', resource, params })
  let data: any
  switch (resource) {
    case 'contactMoments':
      const cbIndex = contactMoments.findIndex(record => String(record.id) === params.id)
      data = contactMoments.splice(cbIndex, 1)[0]
      break
    case 'customFields':
      const cfIndex = customFields.findIndex(record => String(record.id) === params.id)
      data = customFields.splice(cfIndex, 1)[0]
      break
  }

  return Promise.resolve({
    data: {
      id: params.id,
      ...data
    }
  })
}

const update = <RecordType extends RaRecord>(resource: string, params: UpdateParams): Promise<GetOneResult<RecordType>> => {
  const list = getResource(resource) as unknown as RaRecord[]
  if (!list || list.length === 0) return Promise.resolve({ data: { id: '1' } as any })
  const record = list.filter(item => String(item.id) === String(params.id))[0] as any
  if (process.env?.NODE_ENV !== 'test') console.log({ type: 'update', resource, list, record, params })
  return Promise.resolve({
    data: {
      ...record,
      ...params.data
    }
  })
}

const getMany = <RecordType extends RaRecord>(resource: string, params: GetManyParams): Promise<GetManyResult<RecordType>> => {
  const list = getResource(resource) as unknown as RaRecord[]
  const data = list.filter(record => params.ids.includes(record.id)) as any[]
  if (process.env?.NODE_ENV !== 'test') console.log({ type: 'getMany', resource, list, data, params })
  return Promise.resolve({
    data: data,
    total: data.length
  })
}

export default {
  getList,
  getOne,
  create,
  delete: deleteOne,
  update,
  getMany
}
