import { FC } from 'react'
import { Navigate } from 'react-router-dom'
import { getEnvVariable, LoginPage } from 'xxllnc-react-components'
import { redirectUri } from '../../App'

const debug = () => getEnvVariable('REACT_APP_DEBUG', 'FALSE').toUpperCase() === 'TRUE'
const audience = () => getEnvVariable('REACT_APP_AUTH0_AUDIENCE')
const loginInfoUrl = () => getEnvVariable('REACT_APP_LOGIN_INFO_URL')

const Login: FC = () => (
  <LoginPage
    scopes={'cga.user cga.admin'}
    redirect={<Navigate to="/" />}
    returnTo={redirectUri}
    debug={debug()}
    loginInfoUrl={loginInfoUrl()}
    audience={audience()}
  />
)

export default Login