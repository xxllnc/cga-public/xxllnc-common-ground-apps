import { Resource, ResourceProps } from 'react-admin'
import { CustomFieldCreate, CustomFieldEdit, CustomFieldList } from 'xxllnc-react-components'
import PostAddIcon from '@mui/icons-material/PostAdd'
import ContactMoments from './contactMoments'
import About from './pages/about'

interface Permissions {
  scopes: string[]
}

export interface CustomResourceProps extends ResourceProps {
  options: {
    label: string
    settings?: boolean
    scopes: string[]
  }
}

const availableResources = ({ scopes }: Permissions): JSX.Element[] => [
  <Resource
    name="contacts"
    icon={ContactMoments.icon}
    list={ContactMoments.list}
    create={ContactMoments.create}
    edit={ContactMoments.edit}
    options={{
      label: 'Contactmomenten',
      settings: false,
      scopes: ['cga.admin', 'cga.user']
    }}
  />,
  <Resource
    name="about"
    icon={About.icon}
    options={{
      label: 'Over',
      settings: true,
      scopes: ['cga.admin', 'cga.user']
    }}
  />,
  <Resource
    name="members"
    options={{
      label: 'Members',
      settings: false,
      scopes: ['cga.admin', 'cga.user']
    }}
  />,
  <Resource
    name="customFields"
    icon={scopes.some(scope => scope === 'cga.admin') ? PostAddIcon : undefined}
    list={scopes.some(scope => scope === 'cga.admin') ? CustomFieldList : undefined}
    edit={scopes.some(scope => scope === 'cga.admin') ? CustomFieldEdit : undefined}
    create={scopes.some(scope => scope === 'cga.admin') ? CustomFieldCreate : undefined}
    options={{
      label: 'Eigen velden',
      settings: scopes.some(scope => scope === 'cga.admin') ? true : false,
      scopes: ['cga.admin', 'cga.user']
    }}
  />
]

const fallback = (): JSX.Element[] => [
  <Resource
    name={ContactMoments.name}
    list={ContactMoments.list}
    icon={ContactMoments.icon}
    options={{
      label: 'Contactmomenten'
    }}
  />
]

interface AResource {
  props: CustomResourceProps
}

export const resourcesForScopes = ({ scopes }: Permissions): JSX.Element[] => {
  if (!scopes || scopes.length === 0) return fallback()

  const resources = availableResources({ scopes }).filter((resource: AResource) => resource.props.options.scopes
    .find(scope => scopes.includes(scope)) !== undefined)
  if (!resources || resources.length === 0) return fallback()
  return resources
}