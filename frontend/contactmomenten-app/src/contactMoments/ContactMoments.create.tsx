import {
  Create,
  CreateProps,
  SimpleForm,
  useGetList
} from 'react-admin'
import { CustomFieldInput, HeaderTitle } from 'xxllnc-react-components'
import BrpPersonInput from '../components/BrpPersonInput'
import { CustomField } from '../types/contactmoments'

const ContactMomentsCreate = (props: CreateProps): JSX.Element => {

  const { data: customFields } = useGetList<CustomField>(
    'customFields',
    { sort: { field: 'sortOrder', order: 'ASC' }, filter: { archived: false } }
  )

  return (
    <Create title={<HeaderTitle title="Contactmoment toevoegen" />} {...props}>
      <SimpleForm redirect="list">
        <BrpPersonInput />
        {
          customFields && customFields.map((customField, index) =>
            <CustomFieldInput
              key={customField.id}
              index={index}
              customField={customField}
            />
          )
        }
      </SimpleForm>
    </Create>
  )
}

export default ContactMomentsCreate
