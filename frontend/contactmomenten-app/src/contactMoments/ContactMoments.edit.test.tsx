import { render, screen, waitFor } from '@testing-library/react'
import { AdminContext, ResourceContextProvider } from 'react-admin'
import providers from '../providers'
import ContactMomentsEdit from './ContactMoments.edit'

describe.skip('ContactMoments edit test', () => {

  it('should render the edit', async () => {
    const dataProviderMock = providers.data

    render(
      <AdminContext dataProvider={dataProviderMock} >
        <ResourceContextProvider value="contactMoments">
          <ContactMomentsEdit id={'TBV001'} />
        </ResourceContextProvider>
      </AdminContext>
    )

    // We expect 'Contactpersoon *' as required label for one input field
    await waitFor(() => expect(screen.getByText('contactPersoon *')).toBeDefined())
    // We expect 'Peter Pietersen' to be the value of field 'Contactpersoon *' in the mock data that is provided
    expect(screen.getByDisplayValue('Peter Pietersen')).toBeDefined()
  })

})
