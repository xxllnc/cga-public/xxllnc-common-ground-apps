import ContactPhoneIcon from '@mui/icons-material/ContactPhone'
import { CustomResourceProps } from '../Resources'
import ContactMomentsCreate from './ContactMoments.create'
import ContactMomentsEdit from './ContactMoments.edit'
import ContactMomentsList from './ContactMoments.list'

const ContactMomentRequests: CustomResourceProps = {
  name: 'contacts',
  list: ContactMomentsList,
  create: ContactMomentsCreate,
  edit: ContactMomentsEdit,
  icon: ContactPhoneIcon,
  options: {
    label: 'Contactmomenten',
    scopes: ['cga.admin', 'cga.user']
  }
}

export default ContactMomentRequests
