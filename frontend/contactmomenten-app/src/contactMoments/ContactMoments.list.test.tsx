import { render, screen, waitFor } from '@testing-library/react'
import { AdminContext, ResourceContextProvider } from 'react-admin'
import providers from '../providers'
import ContactMomentsList from './ContactMoments.list'

describe.skip('contactMoments list test', () => {
  jest.setTimeout(30000)

  it('should render the list with contactMoments rows', async () => {

    const dataProviderMock = providers.data

    // Given
    render(
      <AdminContext dataProvider={dataProviderMock} >
        <ResourceContextProvider value="contacts">
          <ContactMomentsList resource="contacts"><></></ContactMomentsList>
        </ResourceContextProvider>
      </AdminContext>
    )

    screen.debug(undefined, 2000000)
    // We expect 'contact' to be the label of a textfield in the list
    await waitFor(() => expect(screen.getByText('contact')).toBeDefined())

    // We expect 'Peter Pietersen' to be the 'Contact' value of an item in the list
    expect(screen.getByText('Peter Pietersen')).toBeDefined()

  })
})
