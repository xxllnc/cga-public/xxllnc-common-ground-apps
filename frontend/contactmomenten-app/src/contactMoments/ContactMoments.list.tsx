import { Theme, useMediaQuery } from '@mui/material'
import { List, ListProps, RaRecord, SimpleList } from 'react-admin'
import { CustomFieldListGuesser, useAuthorized } from 'xxllnc-react-components'
import { ContactMomentActions } from './ContactMoment.actions'
import { FilterSidebar, FilterToolbar } from './filters'

const ContactMomentsList = (props: ListProps): JSX.Element => {

  useAuthorized()
  const isXSmall = useMediaQuery<Theme>(theme => theme.breakpoints.down('md'))

  return isXSmall ? (
    <List
      {...props}
      aside={<FilterSidebar />}
      filters={<FilterToolbar />}
      exporter={false}
    >
      <SimpleList
        // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
        primaryText={(record: RaRecord) => `${record.contact} `}
        secondaryText={(record) => `Aanmaakdatum: ${new Date((record as unknown as { createdAt: Date }).createdAt).toLocaleDateString()}`}
        tertiaryText={record => `${record.id}`}
      />
    </List>
  ) : (
    <CustomFieldListGuesser
      {...props}
      aside={<FilterSidebar />}
      filters={<FilterToolbar />}
      actions={<ContactMomentActions />}
      ignoreList={['uuid', 'updatedAt', 'customFields']}
    />
  )
}

export default ContactMomentsList
