import { FC } from 'react'
import { ActionsToolbar } from '../components/toolbars/ActionsToolbar'

export const ContactMomentensEditActions: FC = () => (
  <ActionsToolbar source='contacts' to='/contacts' />
)