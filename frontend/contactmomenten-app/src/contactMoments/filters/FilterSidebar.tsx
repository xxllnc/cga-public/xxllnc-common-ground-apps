import { FC } from 'react'
import { SavedQueriesList , FilterLiveSearch, useTranslate } from 'react-admin'
import { CreatedAtFilter, CreatedByAssignedToMeFilter, SidebarCard } from 'xxllnc-react-components'
import { CardContent, Typography } from '@mui/material'

export const FilterSidebar: FC = () => {
  const translate = useTranslate()

  return (
    <SidebarCard sx={{ maxWidth: 250 }}>
      <CardContent>
        <Typography variant="h6" component="div">
          { translate('filters') }
        </Typography>
        <FilterLiveSearch source="q" />
        <SavedQueriesList />
        <CreatedByAssignedToMeFilter showAssignedToMe={false} />
        <CreatedAtFilter />
      </CardContent>
    </SidebarCard>
  )
}