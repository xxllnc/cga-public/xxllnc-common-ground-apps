import { FC } from 'react'
import { DateTimeInput, Filter, ListProps } from 'react-admin'

export const FilterToolbar: FC<Partial<ListProps>> = (props) => (
  <Filter {...props}>
    <DateTimeInput source="createdAt_gte" label="Datum van" />
    <DateTimeInput source="createdAt_lte" label="Datum tot" />
  </Filter>
)