import { FC } from 'react'
import {
  Edit,
  EditProps,
  SimpleForm,
  useRecordContext,
} from 'react-admin'
import { HeaderTitle, CustomFieldInput } from 'xxllnc-react-components'
import BrpPersonInput from '../components/BrpPersonInput'
import { PydanticMainContactMomentDetails } from '../types/contactmoments'
import { ContactMomentensEditActions } from './ContactMomenten.edit.actions'

const CustomFields: FC = () => {
  const record = useRecordContext<PydanticMainContactMomentDetails>()
  const customFields = record?.customFields ? [...record.customFields] : []

  return (
    <>
      {
        customFields?.map((customField, index) => (
          <CustomFieldInput
            key={customField.id}
            index={index}
            customField={customField}
          />))
      }
    </>
  )
}

const ContactMomentsEdit: FC<EditProps> = (props) => (
  <Edit
    {...props}
    title={<HeaderTitle title="Contactmoment van %contact% aanpassen" />}
    actions={<ContactMomentensEditActions />}
  >
    <SimpleForm redirect="list" sx={{ marginTop: '10px' }}>
      <BrpPersonInput />
      <CustomFields />
    </SimpleForm>
  </Edit>
)


export default ContactMomentsEdit
