import DeleteIcon from '@mui/icons-material/Delete'
import {
  Button, CircularProgress, Grid, IconButton, Paper, TextField, Typography
} from '@mui/material'
import { format, startOfToday } from 'date-fns'
import { FC, KeyboardEvent, useState } from 'react'
import { required, TextInput, useTranslate } from 'react-admin'
import { useFormContext, useWatch } from 'react-hook-form'
import { authToken, parseResponse } from 'xxllnc-react-components'
import { getPersonsUrl } from '../../AppUrls'
import { PersonsResults } from './PersonsResult'


export interface Person {
  burgerservicenummer: string
  naam: {
    aanschrijfwijze: string
  }
  verblijfplaats: {
    adresregel1: string
    adresregel2: string
  }
}

interface PersonResponse {
  _embedded?: { ingeschrevenpersonen?: Person[] }
  detail: string
  title: string
}

const BrpPersonInput: FC = () => {
  const translate = useTranslate()
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState<string>()
  const [lastName, setLastName] = useState<string>()
  const [birthDate, setBirthDate] = useState<string>()
  const [postCode, setPostCode] = useState<string>()
  const [houseNumber, setHouseNumber] = useState<string>()
  const [bsn, setBsn] = useState<string>()
  const [persons, setPersons] = useState<Person[]>([])

  const { setValue } = useFormContext()
  const contactName = useWatch<{ contact: string }>({ name: 'contact' })
  const contactBsn = useWatch<{ contactBsn: string }>({ name: 'contactBsn' })

  // const { input: { onChange: setContactBsn } } = useField('contactBsn')
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
  // const { input: { onChange: setContactName, value: contactName } } = useField('contact')

  const onSearch = () => {
    setLoading(true)
    setError(undefined)
    const params: string[] = []
    if (bsn) params.push(`burgerservicenummer=${bsn}`)
    if (lastName) params.push(`naam__geslachtsnaam=${lastName}`)
    if (birthDate) params.push(`geboorte__datum=${birthDate}`)
    if (postCode) params.push(`verblijfplaats__postcode=${postCode.replace(' ', '')}`)
    if (houseNumber) params.push(`verblijfplaats__huisnummer=${houseNumber}`)

    fetch(`${getPersonsUrl()}/persons?${params.join('&')}`,
      { method: 'GET', headers: { Authorization: authToken.getToken() || '' } })
      .then(response => parseResponse<PersonResponse>(response))
      .then(personsResponse => {
        if (personsResponse?.detail) {
          setError(`${personsResponse.title} ${personsResponse.detail}`)
          // eslint-disable-next-line no-underscore-dangle
        } else if (personsResponse?._embedded?.ingeschrevenpersonen) {
          // eslint-disable-next-line no-underscore-dangle
          setPersons(personsResponse._embedded.ingeschrevenpersonen)
        } else {
          setPersons([])
          setError(translate('noResults'))
        }
      })
      .catch((err: { message: string }) => setError(err.message))
      .finally(() => setLoading(false))
  }

  const handleKeyboardEvent = (e: KeyboardEvent<HTMLImageElement>) => e.key === 'Enter' && onSearch()

  const onSelectPerson = (newValue: Person) => {
    setValue('contact', newValue.naam.aanschrijfwijze)
    setValue('contactBSN', newValue.burgerservicenummer)
  }

  const onDeselectPerson = () => {
    setValue('contact', undefined)
    setValue('contactBSN', undefined)
  }

  return (
    <>
      {
        contactName ?
          <Grid container spacing={2}>
            <Grid item xs={9}>
              <TextInput
                label="contactPersoon"
                source="contact"
                validate={[required()]}
                disabled
                fullWidth
              />
            </Grid>
            <Grid item xs={3}>
              <IconButton
                aria-label="delete"
                onClick={onDeselectPerson}
                style={{ marginTop: 8 }}
              >
                <DeleteIcon />
              </IconButton>
            </Grid>
          </Grid> :
          <Paper style={{ padding: '1rem' }}>
            <Grid container spacing={2} style={{ maxWidth: 640 }}>
              <Grid item xs={12}>
                <Typography variant="h6">{translate('contactPersoon')} <span style={{ color: 'red' }}>*</span></Typography>
                <Typography variant="body2">
                  {translate('searchDescription')}
                </Typography>
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  name="name"
                  label={translate('name')}
                  value={lastName || ''}
                  onChange={(event) => setLastName(event.target.value)}
                  InputLabelProps={{ shrink: true }}
                  onKeyPress={handleKeyboardEvent}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  name="birthDate"
                  label={translate('birthDate')}
                  type="date"
                  value={birthDate || ''}
                  onChange={(event) => setBirthDate(event.target.value)}
                  InputLabelProps={{ shrink: true }}
                  inputProps={{ max: `${format(startOfToday(), 'yyyy-MM-dd')}` }}
                  onKeyPress={handleKeyboardEvent}
                  fullWidth
                />
              </Grid>
              <Grid item xs={9}>
                <TextField
                  name="postCode"
                  label={translate('postCode')}
                  value={postCode || ''}
                  onChange={(event) => setPostCode(event.target.value)}
                  InputLabelProps={{ shrink: true }}
                  onKeyPress={handleKeyboardEvent}
                  fullWidth
                />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  name="houseNumber"
                  label={translate('houseNumber')}
                  type="number"
                  value={houseNumber || ''}
                  onChange={(event) => setHouseNumber(event.target.value)}
                  InputLabelProps={{ shrink: true }}
                  onKeyPress={handleKeyboardEvent}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  name="bsn"
                  label={translate('bsn')}
                  value={bsn || ''}
                  onChange={(event) => setBsn(event.target.value)}
                  InputLabelProps={{ shrink: true }}
                  onKeyPress={handleKeyboardEvent}
                />
              </Grid>
              <Grid item xs={12}>
                <Button
                  variant="contained"
                  onClick={onSearch}
                  disabled={loading}
                >
                  {translate('search')}
                </Button>
              </Grid>
              <Grid item xs={12}>
                {loading && <CircularProgress size={25} thickness={2} />}
                {error && <Typography variant="body2">{error}</Typography>}
              </Grid>
              <PersonsResults persons={persons} onSelectPerson={onSelectPerson} />
            </Grid>
          </Paper>
      }
    </>
  )
}

export default BrpPersonInput