import {
  Divider, List, ListItem,
  ListItemText, ListSubheader, Theme
} from '@mui/material'
import { FC, Fragment } from 'react'
import { useTranslate } from 'react-admin'
import { Person } from '.'

const listStyle = (theme: Theme) => ({
  width: '100%',
  backgroundColor: theme.palette.background.paper,
  position: 'relative',
  overflow: 'auto',
  maxHeight: 257
})

interface Props {
  persons: Person[]
  onSelectPerson: (person: Person) => void
}

export const PersonsResults: FC<Props> = ({ persons, onSelectPerson }) => {

  const translate = useTranslate()

  if (persons.length === 0) return null
  return (
    <List
      component="nav"
      sx={listStyle}
      aria-labelledby="result-list-subheader"
      subheader={
        <ListSubheader component="div" id="result-list-subheader">
          {persons.length > 1 ? translate('results') : translate('result')}
        </ListSubheader>
      }
    >
      {persons.map((person, i) => (
        <Fragment key={person.burgerservicenummer}>
          <ListItem
            button
            onClick={() => onSelectPerson(person)}
          >
            <ListItemText
              primary={`${person.naam.aanschrijfwijze} (${person.burgerservicenummer})`}
              secondary={`${person.verblijfplaats.adresregel1} ${person.verblijfplaats.adresregel2}`}
            />
          </ListItem>
          {i < persons.length - 1 && <Divider />}
        </Fragment>
      ))}
    </List>
  )
}
