import { AdminContext } from 'react-admin'
import { render, screen } from '@testing-library/react'
import { BackButton } from '.'

describe('Buttons test', () => {

  it('should render the BackButton', () => {

    render(
      <AdminContext >
        <BackButton label="Test" />
      </AdminContext>
    )

    expect(screen.getByText('Test')).toBeDefined()
  })
})