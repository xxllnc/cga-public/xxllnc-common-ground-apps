/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface ContactMoment {
  createdByName?: string;
  contact: string;
  id: string;

  /** @format uuid */
  uuid: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt?: string;
}

export interface ContactMomentCreateRequest {
  customFields?: CustomFieldsWithValue[];
  contact: string;
}

export interface ContactMomentUpdateRequest {
  customFields?: CustomFieldsWithValue[];
  contact?: string;
  resultId?: number;
}

export interface CustomField {
  /** Integer representing the sort order of the field. */
  sortOrder: number;
  name: string;
  description?: string;
  required?: boolean;
  type: CustomFieldTypes;
  options?: Options[];
  id: number;

  /** @format uuid */
  uuid: string;
}

export interface CustomFieldCreate {
  name: string;
  description?: string;
  required?: boolean;
  type: CustomFieldTypes;
  options?: Options[];
}

export interface CustomFieldCreateUpdate {
  /** Integer representing the sort order of the field. */
  sortOrder?: number;
  name?: string;
  description?: string;
  required?: boolean;
  type?: CustomFieldTypes;
  options?: Options[];
}

/**
 * An enumeration.
 */
export enum CustomFieldTypes {
  String = "string",
  Text = "text",
  Select = "select",
}

export interface CustomFieldsWithValue {
  /** Integer representing the sort order of the field. */
  sortOrder: number;
  name: string;
  description?: string;
  required?: boolean;
  type: CustomFieldTypes;
  options?: Options[];
  id: number;
  value?: string;
}

export interface HTTPValidationError {
  detail?: ValidationError[];
}

export interface Options {
  id: string;
  name: string;
}

export interface ValidationError {
  loc: string[];
  msg: string;
  type: string;
}

export interface AppSchemasContactMomentSchemasContactMomentDetails {
  customFields?: CustomFieldsWithValue[];
  createdByName?: string;
  contact: string;
  id: string;

  /** @format uuid */
  uuid: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt?: string;
}

export interface PydanticMainContactMomentDetails {
  customFields?: CustomFieldsWithValue[];
  createdByName?: string;
  contact: string;
  id: string;

  /** @format uuid */
  uuid: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt?: string;
}
