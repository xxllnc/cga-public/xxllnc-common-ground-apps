/* eslint-disable */
import {
  CreateParams,
  DeleteManyParams,
  DeleteParams,
  GetListParams,
  GetManyParams,
  GetManyReferenceParams,
  GetOneParams,
  UpdateManyParams,
  UpdateParams,
  DataProvider,
  fetchUtils
} from 'react-admin'
import provider from './'
import mocks from '../mocks'

interface Options extends RequestInit {
  user?: {
    authenticated?: boolean;
    token?: string;
  };
}

const HttpClient = (url: string, options: Options = {}) => fetchUtils.fetchJson(url, options)

const mockDataProvider: DataProvider = {
  getList: async (resource: string, params: GetListParams) => mocks.mockProvider.getList(resource, params),
  getOne: (resource: string, params: GetOneParams) => mocks.mockProvider.getOne(resource, params),
  create: (resource: string, params: CreateParams) => mocks.mockProvider.create(resource, params),
  delete: (resource: string, params: DeleteParams) => mocks.mockProvider.delete(resource, params),
  getMany: (resource: string, params: GetManyParams) => mocks.mockProvider.getMany(resource, params),
  getManyReference: (resource: string, params: GetManyReferenceParams) => HttpClient(`${provider.url}/${resource}/${params.id}`).then(({ json }) => ({ data: json, total: json.length })),
  update: (resource: string, params: UpdateParams) => mocks.mockProvider.update(resource, params),
  updateMany: (resource: string, params: UpdateManyParams) => HttpClient(`${provider.url}/${resource}/${JSON.stringify(params.ids)}`).then(response => ({ data: response.json })),
  deleteMany: (resource: string, params: DeleteManyParams) => HttpClient(`${provider.url}/${resource}/${JSON.stringify(params.ids)}`).then(response => ({ data: response.json }))
}

export default mockDataProvider
