# Check if chosen app node_modules exists
CGA_APP=$1
if [ ! -d "../$CGA_APP/" ] 
then
    echo -e "\033[0;31mApp \"$CGA_APP\" does not exists.\033[0m"
    exit 20
fi

if [ ! -d "../$CGA_APP/src/node_modules/xxllnc-formio-components/dist" ] 
then
    echo -e "\033[0;31mDirectory ../$CGA_APP/src/node_modules/xxllnc-formio-components/dist does not exists.\033[0m"
    cd ../$CGA_APP/
    echo -e "Install node_modules in ../$CGA_APP/src/node_modules"
    yarn --modules-folder src/node_modules/ 
    cd ../formio-components
fi