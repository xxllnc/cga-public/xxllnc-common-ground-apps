import { ExtendedComponentSchema } from 'formiojs'
import baseEditForm from 'formiojs/components/_classes/component/Component.form'
import FileEditData from './editForm/file.edit.data'
import FileEditDisplay from './editForm/file.edit.display'
import FileEditFile from './editForm/file.edit.file'
import FileEditValidation from './editForm/file.edit.validations'

export default (): { components: ExtendedComponentSchema<any>[] } =>
  // eslint-disable-next-line @typescript-eslint/no-unsafe-return, @typescript-eslint/no-unsafe-call
  baseEditForm([
    {
      key: 'display',
      components: FileEditDisplay
    },
    {
      key: 'data',
      components: FileEditData
    },
    {
      label: 'File',
      key: 'file',
      weight: 5,
      components: FileEditFile
    },
    {
      key: 'validation',
      components: FileEditValidation
    },
    {
      key: 'addons',
      ignore: true
    }
  ])
