import { BuilderInfo, Components, ExtendedComponentSchema } from 'formiojs'
import settingsForm from './fileComponent.form'

export default class FileComponent extends Components.components.file {

  static schema(): ExtendedComponentSchema {
    return Components.components.file.schema({
      type: 'fileComponent',
      label: 'Upload',
      key: 'file',
      storage: 'url',
      image: false,
      imageSize: '200',
      filePattern: '*',
      fileMinSize: '0KB',
      fileMaxSize: '500MB',
      uploadOnly: false,
      url: '/verzoeken/api/v1/requests/upload'
    })
  }

  /*
   * Defines the settingsForm when editing a component in the builder.
   */
  public static editForm = settingsForm

  static get builderInfo(): BuilderInfo {
    return {
      title: 'File',
      group: 'basic',
      icon: 'file',
      documentation: '/userguide/#file',
      weight: 130,
      schema: FileComponent.schema()
    }
  }
}