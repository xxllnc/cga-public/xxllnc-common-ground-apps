/* eslint-disable quotes */
/* eslint-disable @typescript-eslint/quotes */
/* eslint-disable max-len */

export default [
  {
    type: 'select',
    input: true,
    key: 'storage',
    label: 'Opslag type',
    placeholder: 'Selecteer het type opslag dat gebruikt moet worden',
    weight: 0,
    valueProperty: 'value',
    data: {
      values: [
        {
          label: 'Url',
          value: 'url'
        }
      ]
    }
  },
  {
    type: 'textfield',
    input: true,
    key: 'url',
    label: 'Url',
    weight: 10,
    placeholder: 'Geef de url op waar de bestanden naar verzonden moeten worden',
    conditional: {
      json: { '===': [{ var: 'data.storage' }, 'url'] }
    }
  },
  {
    type: 'textarea',
    key: 'options',
    label: 'Custom request opties',
    tooltip: 'Geef uw eigen xhr opties (optioneel)',
    rows: 5,
    editor: 'ace',
    input: true,
    weight: 15,
    placeholder: `{
  "withCredentials": true
}`,
    conditional: {
      json: {
        '===': [{
          var: 'data.storage'
        }, 'url']
      }
    }
  },
  {
    type: 'textfield',
    input: true,
    key: 'fileNameTemplate',
    label: 'Bestands naam template',
    placeholder: '(optioneel) { {naam} }-{ {guid} }"',
    tooltip: 'Specificeer een template voor de bestanden. Reguliere template variabelen zijn beschikbaar (`data`, `component`, `user`, `value`, `moment` etc.), en `fileName`, `guid` variabelen zijn beschikbaar. `guid` is verplicht, als dit niet in de template zit, zal het aan het einde worden toegevoegd.',
    weight: 25
  },
  {
    type: 'checkbox',
    input: true,
    key: 'image',
    label: 'Toon als afbeelding(en)',
    weight: 30
  },
  {
    type: 'checkbox',
    input: true,
    key: 'uploadOnly',
    label: 'Alleen uploaden',
    tooltip: 'Als dit is aangevinkt wordt het bestand niet gedownload als een gebruiker er op klikt.',
    weight: 33,
  },
  {
    type: 'textfield',
    input: true,
    key: 'imageSize',
    label: 'Afbeelding preview formaat',
    placeholder: '100',
    weight: 40,
    conditional: {
      json: { '==': [{ var: 'data.image' }, true] }
    }
  },
  {
    type: 'checkbox',
    input: true,
    key: 'webcam',
    label: 'Sta camera toe',
    weight: 32
  },
  {
    type: 'textfield',
    input: true,
    key: 'webcamSize',
    label: 'Camera afbeelding breedte',
    placeholder: '320',
    weight: 38,
    conditional: {
      json: { '==': [{ var: 'data.webcam' }, true] }
    }
  },
  {
    type: 'datagrid',
    input: true,
    label: 'Bestands Types',
    key: 'fileTypes',
    tooltip: 'Specificeer bestandstypes. Dit is handig als u meerdere bestandstypes toestaat, maar de gebruiker in staat wilt stellen te specificeren welk bestandstype elk bestand is.',
    weight: 11,
    components: [
      {
        label: 'Label',
        key: 'label',
        input: true,
        type: 'textfield'
      },
      {
        label: 'Waarde',
        key: 'value',
        input: true,
        type: 'textfield'
      }
    ]
  },
  {
    type: 'textfield',
    input: true,
    key: 'filePattern',
    label: 'File Pattern',
    placeholder: '.jpg,video/*,application/pdf',
    tooltip: 'Zie <a href=\'https://github.com/danialfarid/ng-file-upload#full-reference\' target=\'_blank\'>https://github.com/danialfarid/ng-file-upload#full-reference</a> hoe u patronen kunt specificeren.',
    weight: 50
  },
  {
    type: 'textfield',
    input: true,
    key: 'fileMinSize',
    label: 'Minimale bestandsgrootte',
    placeholder: '0KB',
    tooltip: 'U kunt KB, MB, GB gebruiken.',
    weight: 60
  },
  {
    type: 'textfield',
    input: true,
    key: 'fileMaxSize',
    label: 'Maximale bestands grootte',
    placeholder: '500MB',
    tooltip: 'U kunt KB, MB, GB gebruiken.',
    weight: 70
  },
]
