/* eslint-disable */
import { Story, Meta } from '@storybook/react'
import { Formio } from 'formiojs'
import { Form, Components, FormSchema } from '@tsed/react-formio'
import fileComponent from  './fileComponent'

Formio.use({ fileComponent })

const mockForm: FormSchema = {
	"display": "wizard",
	"title": "Form with file component",
	"name": "formname",
	"type": [
		"form"
	],
	"components": [
		{
			"title": "Test",
			"theme": "default",
			"breadcrumb": "none",
			"buttonSettings": {
				"previous": false,
				"cancel": false,
				"next": false
			},
			"key": "page1",
			"type": "panel",
			"label": "Page 1",
			"components": [
				{
					"label": "File",
					"type": "file",
					"input": true,
					"key": "file",
					"id": "e9i3ae6"
				}
			],
			"id": "eydiokc"
		}
	]
}

export default {
  title: 'FileComponent'
} as Meta

export const FileComponent: Story = () => {
  Components.setComponents({ fileComponent })

  return (
    <Form form={mockForm} />
  )
}