import fileComponent from './fileComponent/fileComponent'
import locationLeafletComponent from './locationLeafletComponent/locationLeafletComponent'

export const CustomFormioComponents = ({
  locationLeafletComponent,
  fileComponent
})