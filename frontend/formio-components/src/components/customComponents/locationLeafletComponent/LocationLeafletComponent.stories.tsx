/* eslint-disable */
import { Story, Meta } from '@storybook/react'
import { Formio } from 'formiojs'
import { Form, Components, FormSchema } from '@tsed/react-formio'
import locationLeafletComponent from  './locationLeafletComponent'

Formio.use({ locationLeafletComponent })

const mockForm: FormSchema = {
	"display": "wizard",
	"title": "Form with leaflet location",
	"name": "formname",
	"type": [
		"form"
	],
	"components": [
		{
			"title": "Test",
			"theme": "default",
			"breadcrumb": "none",
			"buttonSettings": {
				"previous": false,
				"cancel": false,
				"next": false
			},
			"key": "page1",
			"type": "panel",
			"label": "Page 1",
			"components": [
				{
					"label": "Locatie",
					"width": "100%",
					"height": "180px",
					"latitude": 52.265923341416307,
					"longitude": 6.794495616885397,
					"zoomLevel": 10,
					"leafletCssSource": "https://unpkg.com/leaflet@1.7.1/dist/leaflet.css",
					"leafletJsSource": "https://unpkg.com/leaflet@1.7.1/dist/leaflet.js",
					"defaultBackgroundLayer": "https://service.pdok.nl/brt/achtergrondkaart/wmts/v2_0/standaard/EPSG:3857/{z}/{x}/{y}.png",
					"defaultBackgroundLayerAttribution": "Kaartgegevens: © <a href=\"http://www.kadaster.nl\">Kadaster</a>",
					"bagApiUrl": "https://api.bag.acceptatie.kadaster.nl/lvbag/individuelebevragingen/v2",
					"hideSearch": false,
					"bagApiKey": "todo_make_setable",
					"geoDataApiUrl": "https://geodata.nationaalgeoregister.nl/locatieserver/revgeo?type=adres&X={x}&Y={y}&rows=1",
					"type": "locationLeafletComponent",
					"input": true,
					"key": "locatie",
					"widget": {
						"type": "input"
					},
					"inputType": "text",
					"inputFormat": "plain",
					"id": "e9i3ae6"
				}
			],
			"id": "eydiokc"
		}
	]
}

export default {
  title: 'LocationLeafterComponent'
} as Meta

export const LeafletLocation: Story = () => {
  Components.setComponents({ locationLeafletComponent })

  return (
    <Form form={mockForm} />
  )
}