
export interface ResultItem {
  omschrijving: string
  identificatie: string
  _links: {
    adres: {
      href: string
    }
  }
}

export interface SearchResult {
  _embedded: {
    zoekresultaten: ResultItem[]
  }
}

export interface DetailsResult {
  adresseerbaarObjectGeometrie: {
    punt: {
      coordinates: number[]
    }
  }
}

export interface DetailsWithIdResult {
  _embedded: {
    adressen: {
      nummeraanduidingIdentificatie: string
    }[]
  }
}

export interface SuggestResponse {
  response: {
    numFound: number
    docs: {
        weergavenaam: string
      }[]
  }
}