/* eslint-disable no-underscore-dangle, @typescript-eslint/explicit-module-boundary-types, @typescript-eslint/restrict-plus-operands */
/* eslint-disable @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-return, @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access, @typescript-eslint/ban-ts-comment */
// @ts-nocheck
import { Templates } from '@tsed/react-formio'
import { BuilderInfo, Components, ExtendedComponentSchema, Formio } from 'formiojs'
import { LatLngExpression, Map, Marker } from 'leaflet'
import { convertRdToLatLong, optimisticFetch, parseData } from '../../../utils'
import { wgs84ToRd } from '../../../utils/wgs84ToRd'
import { Constants, constants } from './editForm/LocationLeafletComponent.edit.display'
import settingsForm from './LocationLeafletComponent.form'
import { DetailsResult, DetailsWithIdResult, ResultItem, SearchResult, SuggestResponse } from './types'

export default class LocationLeafletComponent extends Components.components.textfield {
  marker: Marker | undefined
  leafletMap: Map | undefined
  component: Constants | undefined
  divMapId = ''

  static schema(): ExtendedComponentSchema {
    return Components.components.textfield.schema({
      type: 'locationLeafletComponent',
      label: 'Locatie',
      key: 'locatie',
      width: constants.defaultWidth,
      height: constants.defaultHeight
    })
  }

  /*
   * Defines the settingsForm when editing a component in the builder.
   */
  public static editForm = settingsForm

  static get builderInfo(): BuilderInfo {
    return {
      title: 'Location map',
      group: 'basic',
      icon: 'map',
      weight: 140,
      schema: LocationLeafletComponent.schema()
    }
  }

  init(): void {
    super.init()
    Formio.requireLibrary('leaflet-css', 'leaflet-pane',
      [{ type: 'styles', src: this.component?.leafletCssSource ?? constants.leafletCssSource }], true)
    Formio.requireLibrary('leaflet', 'L', this.component?.leafletJsSource ?? constants.leafletJsSource, true)
  }

  get defaultSchema(): Components.components.textfield {
    return Components.components.textfield.schema()
  }

  get emptyValue(): string {
    return ''
  }

  get inputInfo(): void {
    const info = super.inputInfo
    info.attr.class += ' leaflet-search'
    return info
  }

  disableMap(): void {
    this.leafletMap?.dragging.disable()
    this.leafletMap.zoomControl.disable()
    this.leafletMap?.doubleClickZoom.disable()
    this.leafletMap?.scrollWheelZoom.disable()
    this.leafletMap?.boxZoom.disable()
    this.leafletMap?.touchZoom.disable()
    this.leafletMap?.keyboard.disable()
  }

  isMapAlreadyInitializedOrDivMapDontExists = (): boolean => {
    const divOfMap = L.DomUtil.get(this.divMapId)
    const divOfMapInnerHtml = L.DomUtil.get(this.divMapId)?.innerHTML
    return !divOfMap || divOfMapInnerHtml === undefined
  }

  addMarker = (lat: number, lng: number): void => {
    this.marker = new L.marker([lat, lng]).addTo(this.leafletMap)
    this.leafletMap?.setView([lat, lng])
  }

  setValue = (lat: number, lng: number): boolean => {
    if (!lng || !lat) return super.setValue('')
    return super.setValue({ type: 'Point', coordinates: [lng, lat] })
  }

  renderElement(value: string, index: number): void {
    // Generate new div id for the map so you can add multiple map in one form.
    this.divMapId = `myMap ${Math.floor(Math.random() * 1000000)}`
    const height = this.component?.height ?? constants.defaultHeight
    const width = this.component?.width ?? constants.defaultWidth
    Templates.current = {
      leaflet: {
        form: (ctx: { mapId: string }) => `
        <div class="autoCompleteLocation">
          <label for="search-address" class="hidden">Zoek een adres</label>
          <input
            id="search-address"
            class="form-control${this.component?.disableMap || this.component?.hideSearch ? ' hidden' : ''}"
            ref="searchAddressRef"
            type="search"
            autocomplete="off"
            placeholder="Zoek een adres"
          >
          <ul ref="searchResultRef"></ul>
          <div style="width:${width};height:${height};" id="${ctx.mapId}"></div>
        </div>`
      }
    }
    return super.renderElement(value, index) + this.renderTemplate('leaflet', { mapId: this.divMapId })
  }

  attach(element: any): void {
    super.attach(element)
    this.loadRefs(element, { mapCoordinates: 'multiple', searchAddressRef: 'single', searchResultRef: 'single' })

    const formElement = document.getElementById('xxllncForm')
    if (!formElement || this.component?.hideSearch) return

    formElement.onclick = null

    if (!this.component?.disableMap) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
      this.addEventListener(this.refs.searchAddressRef, 'input', this.getSuggestions.bind(this))
      // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
      this.addEventListener(this.refs.searchAddressRef, 'search', this.onResetSeach.bind(this))
      formElement.onclick = () => {
        if (!this.refs.searchResultRef) return formElement.onclick = null
        this.refs.searchResultRef.innerHTML = ''
      }
    }
    return
  }

  attachElement(element: any, index: number): void {
    element.setAttribute('type', 'hidden')
    super.attachElement(element, index)
    Formio.libraryReady('leaflet').then(() => {
      if (this.isMapAlreadyInitializedOrDivMapDontExists()) return

      const coordinatesCenter = (this.component?.latitude && this.component?.longitude) ?
        [this.component.latitude, this.component.longitude] as LatLngExpression : constants.defaultCoordinatesCenter
      const zoomLevel = this.component?.zoomLevel ?? constants.defaultZoomLevel
      const disableMap = this.component?.disableMap ?? false
      const backgroundLayer = this.component?.defaultBackgroundLayer ?? constants.defaultBackgroundLayer
      const backgroundLayerAttribution = this.component?.defaultBackgroundLayerAttribution ?? constants.defaultBackgroundLayerAttribution
      // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
      const jsonValue = parseData<{ coordinates: number[] }>(super.getValue())

      this.leafletMap = L.map(this.divMapId).setView(coordinatesCenter, zoomLevel)
      L.tileLayer(backgroundLayer, { attribution: backgroundLayerAttribution }).addTo(this.leafletMap)

      if (disableMap) this.disableMap()

      if (jsonValue && Array.isArray(jsonValue.coordinates) && jsonValue.coordinates.length === 2)
        this.addMarker(jsonValue.coordinates[1], jsonValue.coordinates[0])

      this.leafletMap.on('click', this.onMapClicked.bind(this))
    })
  }

  onMapClicked(event: { latlng: { lat: number, lng: number } }): void {
    void (async (): Promise<void> => {
      if (this.component?.disableMap ?? false) return
      if (this.marker) this.leafletMap?.removeLayer(this.marker)

      const { lat, lng } = event.latlng
      this.addMarker(lat, lng)
      this.setValue(lat, lng)

      if (this.component?.hideSearch) return

      this.refs.searchAddressRef.value = ''

      const rijksdriehoek = wgs84ToRd(lat, lng)
      const result = await optimisticFetch<SuggestResponse>({
        url: this.component?.geoDataApiUrl?.replace('{x}', String(rijksdriehoek.x)).replace('{y}', String(rijksdriehoek.y)) || ''
      })
      if (result?.response?.numFound > 0) this.refs.searchAddressRef.value = result.response.docs[0].weergavenaam
    })()
  }

  onResetSeach(): void {
    if (this.marker) this.leafletMap?.removeLayer(this.marker)
    this.setValue()
  }

  async getSuggestions(event: HTMLInputElement): Promise<void> {
    const value = event.target.value.replace(' ', '+') as string
    if (value === '') return this.refs.searchResultRef.innerHTML = ''
    const headers = new Headers()
    headers.set('X-API-KEY', String(this.component?.bagApiKey))
    const response = await optimisticFetch<SearchResult>({
      url: `${String(this.component?.bagApiUrl)}/adressen/zoek?zoek=${value}`,
      headers
    })
    this.updateSuggestions(response)
  }

  updateSuggestions(result: SearchResult): void {
    this.refs.searchResultRef.innerHTML = ''
    if (!result?._embedded?.zoekresultaten) return
    result._embedded.zoekresultaten.forEach(item => {
      const li = document.createElement('LI')
      const description = document.createTextNode(item.omschrijving)
      li.appendChild(description)
      li.addEventListener('click', () => this.onSuggestionClicked(item), false)
      this.refs.searchResultRef.appendChild(li)
    })
  }

  onSuggestionClicked(item: ResultItem): void {
    void (async (): Promise<void> => {
      const headers = new Headers()
      headers.set('X-API-KEY', String(this.component?.bagApiKey))
      const addressResponse = await optimisticFetch<DetailsWithIdResult>({
        url: item._links?.adres?.href,
        headers
      })
      const adressen = addressResponse?._embedded?.adressen
      if (!adressen || adressen.length === 0) return window.alert('Dit adres lijkt geen locatie te hebben')

      const nummeraanduidingIdentificatie = adressen[0].nummeraanduidingIdentificatie
      headers.set('Accept-Crs', 'epsg:28992')
      const details = await optimisticFetch<DetailsResult>({
        url: `${String(this.component?.bagApiUrl)}/adressenuitgebreid/${nummeraanduidingIdentificatie}`,
        headers
      })
      const coordinates = details?.adresseerbaarObjectGeometrie?.punt?.coordinates
      if (!coordinates) return window.alert('Dit adres lijkt geen locatie te hebben')

      const converted = convertRdToLatLong(coordinates[0], coordinates[1])
      if (this.marker) this.leafletMap?.removeLayer(this.marker)
      this.addMarker(converted.latitude, converted.longitude)
      this.setValue(converted.latitude, converted.longitude)
      this.refs.searchResultRef.innerHTML = ''
      this.refs.searchAddressRef.value = item.omschrijving
    })()
  }
}