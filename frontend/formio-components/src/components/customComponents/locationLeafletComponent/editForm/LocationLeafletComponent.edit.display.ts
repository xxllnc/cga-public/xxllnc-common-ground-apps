import { LatLngExpression } from 'leaflet'

const defaultLatitude = 52.265923341416304
const defaultLongitude = 6.794495616885397

export interface Constants {
  height?: string
  width?: string
  defaultHeight: string
  defaultWidth: string
  defaultLatitude: number
  defaultLongitude: number
  defaultCoordinatesCenter: LatLngExpression
  defaultZoomLevel: number
  leafletCssSource: string
  leafletJsSource: string
  defaultBackgroundLayer: string
  defaultBackgroundLayerAttribution: string
  defaultBagApiKey: string
  defaultBagApiBaseUrl: string
  defaultGeoDataApiUrl: string
  disableMap?: boolean
  hideSearch?: boolean
  latitude?: number
  longitude?: number
  zoomLevel?: number
  geoDataApiUrl?: string
  bagApiUrl?: string
  bagApiKey?: string
}

export const constants: Constants = {
  defaultHeight: '180px',
  defaultWidth: '100%',
  defaultLatitude,
  defaultLongitude,
  defaultCoordinatesCenter: [defaultLatitude, defaultLongitude],
  defaultZoomLevel: 10,
  leafletCssSource: 'https://unpkg.com/leaflet@1.7.1/dist/leaflet.css',
  leafletJsSource: 'https://unpkg.com/leaflet@1.7.1/dist/leaflet.js',
  defaultBackgroundLayer: 'https://service.pdok.nl/brt/achtergrondkaart/wmts/v2_0/standaard/EPSG:3857/{z}/{x}/{y}.png',
  defaultBackgroundLayerAttribution: 'Kaartgegevens: © <a href="http://www.kadaster.nl">Kadaster</a>',
  defaultBagApiKey: 'your_bag_api_key',
  defaultBagApiBaseUrl: 'https://api.bag.acceptatie.kadaster.nl/lvbag/individuelebevragingen/v2',
  defaultGeoDataApiUrl: 'https://geodata.nationaalgeoregister.nl/locatieserver/revgeo?type=adres&X={x}&Y={y}&rows=1',
}

export default [
  {
    weight: 120,
    type: 'textfield',
    input: true,
    key: 'width',
    label: 'Breedte',
    tooltip: 'Geef hier de breedte van de kaart aan.',
    defaultValue: constants.defaultWidth
  },
  {
    weight: 130,
    type: 'textfield',
    input: true,
    key: 'height',
    label: 'Hoogte',
    tooltip: 'Geef hier de hoogte van de kaart aan.',
    defaultValue: constants.defaultHeight
  },
  {
    weight: 140,
    type: 'textfield',
    input: true,
    key: 'latitude',
    label: 'Latitude',
    tooltip: 'Op welke latitude (noorder- of zuiderbreedte) moet de kaart gecentreerd worden?',
    defaultValue: constants.defaultLatitude
  },
  {
    weight: 150,
    type: 'textfield',
    input: true,
    key: 'longitude',
    label: 'Longitude',
    tooltip: 'Op welke longitude (ooster- of westerlengte) moet de kaart gecentreerd worden?',
    defaultValue: constants.defaultLongitude
  },
  {
    weight: 160,
    type: 'textfield',
    input: true,
    key: 'zoomLevel',
    label: 'Zoom niveau',
    tooltip: 'In hoeverre moet de kaart ingezoomd zijn?',
    defaultValue: constants.defaultZoomLevel
  },
  {
    weight: 170,
    type: 'textfield',
    input: true,
    key: 'leafletCssSource',
    label: 'Url van het Leaflet css bestand',
    tooltip: 'Geef hier de url op van het Leaflet css bestand.',
    defaultValue: constants.leafletCssSource
  },
  {
    weight: 180,
    type: 'textfield',
    input: true,
    key: 'leafletJsSource',
    label: 'Url van het Leaflet javascript bestand',
    tooltip: 'Geef hier de url op van het Leaflet javascript bestand.',
    defaultValue: constants.leafletJsSource
  },
  {
    weight: 181,
    type: 'textfield',
    input: true,
    key: 'defaultBackgroundLayer',
    label: 'Url van de achtergrondkaart',
    tooltip: 'Geef hier de url op van de achtergrondkaart.',
    defaultValue: constants.defaultBackgroundLayer
  },
  {
    weight: 182,
    type: 'textfield',
    input: true,
    key: 'defaultBackgroundLayerAttribution',
    label: 'Bronverwijzing',
    tooltip: 'Geef hier de bronverwijzing van de achtergrondkaart, dat getoond wordt op de kaart.',
    defaultValue: constants.defaultBackgroundLayerAttribution
  },
  {
    weight: 183,
    type: 'textfield',
    input: true,
    key: 'bagApiUrl',
    label: 'BAG api url',
    tooltip: 'Geef hier de BAG individuelebevragingen api url op.',
    defaultValue: constants.defaultBagApiBaseUrl
  },
  {
    weight: 184,
    type: 'checkbox',
    input: true,
    key: 'hideSearch',
    label: 'Hide search',
    tooltip: 'Hiermee kan de zoekbalk boven de map verborgen worden.',
    defaultValue: false
  },
  {
    weight: 185,
    type: 'textfield',
    input: true,
    key: 'bagApiKey',
    label: 'BAG api key',
    tooltip: 'Geef hier de BAG individuelebevragingen api key op.',
    defaultValue: constants.defaultBagApiKey
  },
  {
    weight: 186,
    type: 'textfield',
    input: true,
    key: 'geoDataApiUrl',
    label: 'Geodata api url',
    tooltip: 'Geef hier de url op waar de adres omschrijving opgehaald kan worden met de geselecteerde lat long.',
    defaultValue: constants.defaultGeoDataApiUrl
  },
  {
    key: 'labelPosition',
    ignore: true
  },
  {
    key: 'placeholder',
    ignore: true
  },
  {
    key: 'hidden',
    ignore: true
  },
  {
    key: 'tooltip',
    ignore: true
  },
  {
    key: 'autofocus',
    ignore: true
  },
  {
    key: 'disabled',
    ignore: true
  },
  {
    key: 'tableView',
    ignore: true
  },
  {
    key: 'modalEdit',
    ignore: true
  }
]
