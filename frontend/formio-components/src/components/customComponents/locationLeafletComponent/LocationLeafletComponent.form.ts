import { ExtendedComponentSchema } from 'formiojs'
import baseEditForm from 'formiojs/components/_classes/component/Component.form'
import LocationLeafletComponentEditDisplay from './editForm/LocationLeafletComponent.edit.display'

export default (): { components: ExtendedComponentSchema<any>[] } =>
  // eslint-disable-next-line @typescript-eslint/no-unsafe-return, @typescript-eslint/no-unsafe-call
  baseEditForm([
    {
      key: 'display',
      components: LocationLeafletComponentEditDisplay
    },
    {
      key: 'api',
      ignore: true,
    },
    {
      key: 'data',
      ignore: true,
    }
  ])
