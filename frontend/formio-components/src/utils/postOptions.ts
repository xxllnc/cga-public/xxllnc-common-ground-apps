const createHeader = (token: string): Headers => {
  const headers = new Headers()
  headers.append('Content-Type', 'application/json')
  if (token) headers.append('Authorization', 'Bearer ' + token)
  return headers
}

export const postOptions = (data: string, token: string): RequestInit => ({
  method: 'POST',
  headers: createHeader(token),
  body: data
})

