/* eslint-disable security/detect-object-injection */
const x0 = 155000
const y0 = 463000
const f0 = 52.15517440
const l0 = 5.38720621

const Rp = [0, 1, 2, 0, 1, 3, 1, 0, 2]
const Rq = [1, 1, 1, 3, 0, 1, 3, 2, 3]
const Rpq = [190094.945, -11832.228, -114.221, -32.391, -0.705, -2.34, -0.608, -0.008, 0.148]

const Sp = [1, 0, 2, 1, 3, 0, 2, 1, 0, 1]
const Sq = [0, 2, 0, 2, 0, 1, 2, 1, 4, 4]
const Spq = [309056.544, 3638.893, 73.077, -157.984, 59.788, 0.433, -6.439, -0.032, 0.092, -0.054]

export const wgs84ToRd = (lat: number, lon: number) => {
  const df = 0.36 * (lat - f0)
  const dl = 0.36 * (lon - l0)
  let x = x0
  let y = y0

  for (let i = 0; i < 8; i++) {
    x = x + Rpq[i] * df ** Rp[i] * dl ** Rq[i]
  }

  for (let i = 0; i < 9; i++) {
    y = y + Spq[i] * df ** Sp[i] * dl ** Sq[i]
  }
  return { x, y }
}