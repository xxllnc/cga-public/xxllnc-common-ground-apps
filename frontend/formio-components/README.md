<img src="https://brand.exxellence.nl/wp-content/uploads/2021/01/beloftes-02.svg" alt="drawing" style="display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;"/>

Formio components by xxllnc for faster and simpler web development.

---

# Installation

```
// with npm
npm install xxllnc-formio-components

// with yarn
yarn add xxllnc-formio-components
```

---

# Usage

```typescript
// TODO: Add example
```

**Linux**

Give execution rights to the following scripts.

```bash
$ chmod +x replace-package.sh
$ chmod +x check.sh
```

Change the AppToTest variable in the package.json config to the directory of the app you want to test.

```json
"config" : { "AppToTest" : "formuliereninwoner-app" },
```

Run watch script to start testing, use the clean:app script to cleanup.

```json
"scripts": {
    "watch": "./check.sh $npm_package_config_AppToTest && chokidar src -c \"./replace-package.sh $npm_package_config_AppToTest\"",
    "clean:app": "rimraf ../$npm_package_config_AppToTest/frontend/src/node_modules",
  },
```

**App**

Make sure the chosen app uses the node_modules in the src folder.

In docker-compose.yml change the following settings in your app configuration:

```yml
dockerfile: Dockerfile.local
volumes:
  - "./frontend/{YOUR-APP}:/app:cached"
  - "./frontend/{YOUR-APP}/src/node_modules:/app/node_modules:cached"
```
