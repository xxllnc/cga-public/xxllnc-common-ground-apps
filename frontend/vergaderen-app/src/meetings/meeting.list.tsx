import {
  List,
  Datagrid,
  TextField,
  ListProps,
  DateField,
  SimpleList,
  useTranslate
} from 'react-admin'
import { FilterSidebar, FilterToolbar } from './filters'
import { useTheme, useMediaQuery } from '@material-ui/core'

export const HybridMeetingList = (props: ListProps): JSX.Element => {
  const theme = useTheme()
  const isSmall = useMediaQuery(theme.breakpoints.down('sm'))
  const translate = useTranslate()

  return (
    <List {...props}
      bulkActionButtons={false}
      exporter={false}
      aside={<FilterSidebar />}
      filters={<FilterToolbar />}
      sort={{ field: 'datetime', order: 'DESC' }}
    >
      {isSmall ? (
        <SimpleList
          primaryText={<TextField source='subject' />}
          secondaryText={<DateField source='datetime' showTime />}
          tertiaryText={<TextField source='location' />}
        />
      ) : (
        <Datagrid rowClick='show'>
          <TextField label={translate('Meeting.subject')} source='subject' />
          <TextField label={translate('Meeting.description')} source='description' />
          <DateField label={translate('Meeting.time')} source='datetime' showTime locales="nl" options={{
            timeStyle: 'short',
            dateStyle: 'short',
            hour12: false
          }} />
          <TextField label={translate('Meeting.type')} source='type' />
          <TextField label={translate('Meeting.state')} source='state' />
          <TextField label={translate('Meeting.chairman')} source='chairman' />
          <TextField label={translate('Meeting.location')} source='location' />
        </Datagrid>
      )}
    </List >
  )

}