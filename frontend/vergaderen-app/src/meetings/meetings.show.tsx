import { makeStyles } from '@material-ui/core'
import {
  TextField,
  ListProps,
  DateField,
  Show,
  TabbedShowLayout,
  Tab,
  ReferenceArrayField,
  Datagrid,
  FunctionField,
  Record,
  useTranslate,
} from 'react-admin'

import { MeetingStateButton, SortOrderField } from '../components'

const CustomTitle = ({ record }: { record?: Record }) => (
  <span>
    {//eslint-disable-next-line
      record ? `${record.type} ${new Date(record.datetime).toLocaleString('nl', {
        timeStyle: 'short',
        dateStyle: 'short',
        hour12: false
      })}` : 'Vergadering'
    }
  </span>
)

const useStyles = makeStyles({
  actionButtons: {
    margin: 8,
  },
})

export const MeetingShow = (props: ListProps): JSX.Element => {
  const classes = useStyles()
  const translate = useTranslate()

  return (
    <Show title={<CustomTitle />}  {...props}>
      <TabbedShowLayout>
        <Tab label={translate('Meeting.agenda')}>
          <ReferenceArrayField addLabel={false} reference="meetingitems" source="relations"
            fullWidth sort={{ field: 'sortOrder', order: 'ASC' }}>
            <Datagrid width="100%" rowClick='show'>
              <TextField label={translate('Meeting.caseNumber')} source='caseNumber' />
              <TextField label={translate('Meeting.appliant')} source='applicant' />
              <TextField label={translate('Meeting.caseType')} source='type' />
              {// eslint-disable-next-line @typescript-eslint/no-unsafe-return, @typescript-eslint/no-unsafe-member-access
                <FunctionField label={translate('Decision.decision')} render={record => record?.itemResult ?? '-'} />
              }
              <TextField label={translate('Meeting.subject')} source='subject' />
              {
                // eslint-disable-next-line @typescript-eslint/no-unsafe-return, @typescript-eslint/no-unsafe-member-access
                <FunctionField label={translate('Meeting.attatchments')} render={record => `${record?.relations.length as string}`} />
              }
              <SortOrderField label={translate('sortorder')} source="sortOrder" sortable={true} />

            </Datagrid>
          </ReferenceArrayField>
        </Tab>
        <Tab label={translate('Meeting.details')}>
          <TextField label={translate('Meeting.subject')} source='subject' />
          <TextField label={translate('Meeting.description')} source='description' />
          <TextField label={translate('Meeting.type')} source='type' />
          <TextField label={translate('Meeting.chairman')} source='chairman' />
          <TextField label={translate('Meeting.state')} source='state' />
          <DateField label={translate('Meeting.time')} source='datetime' addLabel showTime locales="nl" options={{
            timeStyle: 'short',
            dateStyle: 'short',
            hour12: false
          }} />
          <TextField label={translate('Meeting.location')} source='location' />
          <div className={classes.actionButtons}>
            <MeetingStateButton source='state' />
          </div>
        </Tab>

      </TabbedShowLayout>
    </Show>
  )
}
