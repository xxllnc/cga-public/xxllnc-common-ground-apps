import { CustomResourceProps } from '../Resources'
import MeetingRoomIcon from '@material-ui/icons/MeetingRoom'
import { HybridMeetingList } from './meeting.list'
import { MeetingShow } from './meetings.show'

const Meetings: CustomResourceProps = {
  name: 'meetings',
  list: HybridMeetingList,
  // create: ContactMomentsCreate,
  // edit: MeetingEdit,
  show: MeetingShow,
  icon: MeetingRoomIcon,
  options: {
    label: 'Vergaderingen',
    permissions: ['cga.admin:read', 'cga.user:read']
  }
}

export default Meetings