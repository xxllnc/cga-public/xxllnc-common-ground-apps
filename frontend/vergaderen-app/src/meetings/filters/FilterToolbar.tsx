import { DateTimeInput, Filter, ListProps, TextInput, useTranslate } from 'react-admin'

export const FilterToolbar = (props: ListProps): JSX.Element => {
  const translate = useTranslate()
  return (
    <Filter {...props}>
      <TextInput label={translate('Filters.search')} source="q" />
      <DateTimeInput source="datetime_gte" label="Datum van" />
      <DateTimeInput source="datetime_lte" label="Datum tot" />
    </Filter>
  )
}