/* eslint-disable camelcase */
import {
  FilterList,
  FilterListItem,
  FilterLiveSearch,
  useTranslate,
} from 'react-admin'
import TodayIcon from '@material-ui/icons/Today'
import LockOpenIcon from '@material-ui/icons/LockOpen'
import FilterListIcon from '@material-ui/icons/FilterList'
import {
  startOfToday,
  startOfWeek,
  subWeeks,
  startOfMonth,
  subMonths,
  endOfToday,
  endOfWeek,
  endOfMonth,
  startOfTomorrow,
  endOfTomorrow,
  addWeeks,
  addMonths,
} from 'date-fns'

export const DatetimeFilter = (): JSX.Element => {
  const translation = useTranslate()
  return (
    <FilterList label={translation('Filters.Time.piriod')} icon={<TodayIcon />}>
      <FilterListItem
        label={translation('Filters.Time.today')}
        value={{
          datetime_gte: startOfToday().toISOString(),
          datetime_lte: endOfToday().toISOString(),
        }}
      />
      <FilterListItem
        label={translation('Filters.Time.tomorrow')}
        value={{
          datetime_gte: startOfTomorrow().toISOString(),
          datetime_lte: endOfTomorrow().toISOString(),
        }}
      />
      <FilterListItem
        label={translation('Filters.Time.thisWeek')}
        value={{
          datetime_gte: startOfWeek(new Date()).toISOString(),
          datetime_lte: endOfWeek(new Date()).toISOString(),
        }}
      />
      <FilterListItem
        label={translation('Filters.Time.nextWeek')}
        value={{
          datetime_gte: subWeeks(startOfWeek(new Date()), 1).toISOString(),
          datetime_lte: startOfWeek(new Date()).toISOString(),
        }}
      />
      <FilterListItem
        label={translation('Filters.Time.lastWeek')}
        value={{
          datetime_gte: addWeeks(startOfWeek(new Date()), 1).toISOString(),
          datetime_lte: addWeeks(endOfWeek(new Date()), 1).toISOString(),
        }}
      />
      <FilterListItem
        label={translation('Filters.Time.thisMonth')}
        value={{
          datetime_gte: startOfMonth(new Date()).toISOString(),
          datetime_lte: endOfMonth(new Date()).toISOString(),
        }}
      />
      <FilterListItem
        label={translation('Filters.Time.nextMonth')}
        value={{
          datetime_gte: subMonths(startOfMonth(new Date()), 1).toISOString(),
          datetime_lte: subMonths(endOfMonth(new Date()), 1).toISOString(),
        }}
      />
      <FilterListItem
        label={translation('Filters.Time.lastMonth')}
        value={{
          datetime_gte: addMonths(startOfMonth(new Date()), 1).toISOString(),
          datetime_lte: addMonths(endOfMonth(new Date()), 1).toISOString(),
        }}
      />
      <FilterListItem
        label={translation('Filters.Time.earlier')}
        value={{
          datetime_gte: undefined,
          datetime_lte: subMonths(endOfMonth(new Date()), 2).toISOString(),
        }}
      />
    </FilterList>)
}

export const MeetingTypeFilter = (): JSX.Element => (
  <FilterList label="Type" icon={<FilterListIcon />}>
    <FilterListItem
      label="College B&W"
      value={{
        type: 'College B&W'
      }}
    />
    <FilterListItem
      label="Raadsvergadering"
      value={{
        type: 'Raadsvergadering'
      }}
    />
    <FilterListItem
      label="Teamvergadering"
      value={{
        type: 'Teamvergadering'
      }}
    />
  </FilterList>)

export const MeetingStateFilter = (): JSX.Element => {
  const translation = useTranslate()
  return (
    <FilterList label="Status" icon={<LockOpenIcon />}>
      <FilterListItem
        label={translation('Filters.State.nieuw')}
        value={{
          state: 'Nieuw'
        }}
      />
      <FilterListItem
        label={translation('Filters.State.started')}
        value={{
          state: 'Started'
        }}
      />
      <FilterListItem
        label={translation('Filters.State.completed')}
        value={{
          state: 'Completed'
        }}
      />
      <FilterListItem
        label={translation('Filters.State.archived')}
        value={{
          type: 'Archived'
        }}
      />
    </FilterList>)
}

export const MeetingOrganizerFilter = (): JSX.Element => (
  // <FilterList label="Organisator" icon={<AssignmentIndIcon />}>
  <FilterLiveSearch source="q" />
  // </FilterList>

)

