import { Card as MuiCard, CardContent, withStyles } from '@material-ui/core'
import { DatetimeFilter, MeetingTypeFilter, MeetingStateFilter, MeetingOrganizerFilter } from './Filters'

const Card = withStyles(theme => ({
  root: {
    [theme.breakpoints.up('sm')]: {
      order: -1,
      width: '15em',
      minWidth: '10em',
      marginRight: '1em',
    },
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
}))(MuiCard)

export const FilterSidebar = (): JSX.Element => (
  <Card>
    <CardContent>
      <MeetingOrganizerFilter />
      <MeetingStateFilter />
      <MeetingTypeFilter />
      <DatetimeFilter />
    </CardContent>
  </Card>
)
