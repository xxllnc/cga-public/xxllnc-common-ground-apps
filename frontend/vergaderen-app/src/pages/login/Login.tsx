import { FC } from 'react'
import { createTheme, ThemeProvider } from '@material-ui/core'
import { Redirect } from 'react-router'
import { getEnvVariable, LoginPage, theme } from 'xxllnc-react-components'
import { getUrl } from '../../AppUrls'

const debug = getEnvVariable('REACT_APP_DEBUG').toUpperCase() === 'TRUE'
const redirectUri = `${window.location.origin}/vergaderen#/login`

const LoginWithTheme: FC = () => (
  <ThemeProvider theme={createTheme(theme)}>
    <LoginPage
      scopes={'cga.user cga.admin'}
      redirect={<Redirect to="/" />}
      returnTo={redirectUri}
      debug={debug}
      apiurl={getUrl()}
    />
  </ThemeProvider>
)

export default LoginWithTheme
