import InfoIcon from '@material-ui/icons/Info'
import { CustomResourceProps } from '../../Resources'

const About: CustomResourceProps = {
  name: 'about',
  icon: InfoIcon,
  options: {
    label: 'Over',
    settings: true,
    permissions: ['cga.admin:read', 'cga.user:read']
  }
}

export default About
