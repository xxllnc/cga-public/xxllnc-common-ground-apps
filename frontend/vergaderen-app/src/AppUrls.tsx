import { getEnvVariable } from 'xxllnc-react-components'

export const getUrl = (): string => window.location.origin + '/vergaderen/mock'
export const getMembersUrl = (): string => getEnvVariable('REACT_APP_GEBRUIKERSBEHEER_SERVER_URL')