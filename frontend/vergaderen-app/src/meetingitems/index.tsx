import { CustomResourceProps } from '../Resources'
import MeetingRoomIcon from '@material-ui/icons/MeetingRoom'
import { MeetingItemsShow } from './meetingitems.show'


const MeetingItems: CustomResourceProps = {
  name: 'meetingitems',
  //   list: HybridMeetingList,
  // create: ContactMomentsCreate,
  // edit: MeetingEdit,
  show: MeetingItemsShow,
  icon: MeetingRoomIcon,
  options: {
    label: 'Vergader items',
    permissions: ['cga.admin:read', 'cga.user:read']
  }
}

export default MeetingItems