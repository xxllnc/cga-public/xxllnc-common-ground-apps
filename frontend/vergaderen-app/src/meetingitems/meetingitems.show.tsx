import {
  Datagrid,
  FunctionField,
  ListProps,
  ReferenceArrayField,
  Record,
  Show,
  SimpleShowLayout,
  TextField,
  useTranslate,
} from 'react-admin'
import { FileDownloadButton, FileFieldWithIcon } from '../components'
import NoteField from '../components/NoteField/NoteField'
import DecisionField from '../components/DecisionField/DecisionField'

const CustomTitle = ({ record }: { record?: Record }) => {
  const translate = useTranslate()
  return (
    <span>
      {
        record ? `${record.caseNumber as string} ${record.subject as string}` : translate('Meeting.meetingSubject')
      }
    </span>
  )
}

export const MeetingItemsShow = (props: ListProps): JSX.Element => {
  const translate = useTranslate()
  return (
    <Show title={<CustomTitle />}{...props}>
      <SimpleShowLayout>
        <TextField label={translate('Meeting.subject')} source='subject' />
        <TextField label={translate('Meeting.description')} source='description' />
        {
          <FunctionField label={translate('Decision.decision')} render={(record: Record | undefined) =>
            record?.itemResult ? `${record.itemResult as string}` : '-'} />
        }
        <TextField label={translate('Meeting.caseNumber')} source='caseNumber' />
        <ReferenceArrayField source="result" label={translate('Decision.decisions')} reference="decisions" addLabel>
          <Datagrid>
            <TextField label={translate('employeeId')} source="employee.display_name" />
            {
              <FunctionField label={translate('Decision.decision')}
                render={(record: Record | undefined) => record?.result ? `${record.result as string}` : '-'} />
            }
            {
              <FunctionField label={translate('Decision.remark')}
                render={(record: Record | undefined) => record?.remark ? `${record.remark as string}` : '-'} />
            }
            <DecisionField label={translate('Decision.decide')} source={'id'} />
          </Datagrid>
        </ReferenceArrayField>
        <ReferenceArrayField label={translate('Document.documents')}
          reference="documents" source="relations" emptyText="Er zijn geen documetnen gevonden">
          {/* todo add state when no documents are found */}
          <Datagrid>
            <FileFieldWithIcon label={translate('Document.fileName')} source='name' />
            <FileDownloadButton source='url' label={translate('download')} />
          </Datagrid>
        </ReferenceArrayField >
        <NoteField />
      </SimpleShowLayout>
    </Show>
  )
}