import { fetchUtils } from 'ra-core'
import jsonServerProvider from 'ra-data-json-server'
import { authProvider, authToken } from 'xxllnc-react-components'
import { getUrl } from '../AppUrls'

interface Response { headers: Headers, json: any, body: string, status: number }
const httpClient = (url: string, options: RequestInit = {}): Promise<Response> => {

  const token = authToken.getToken() ?? null
  if (!token)
    return Promise.reject(process.env?.NODE_ENV === 'development' ? 'No user Token' : '')

  const user = { authenticated: true, token }
  const headers = new Headers({ 'organization-url': window.origin, ...options?.headers })

  return fetchUtils.fetchJson(url, { ...options, user, headers })
}

const providers = {
  auth: authProvider,
  url: getUrl(),
  data: jsonServerProvider(getUrl(), httpClient)
}

export default providers
