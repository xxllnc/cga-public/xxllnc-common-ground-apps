import { CustomFieldTypes, PydanticMainContactMomentDetails } from '../types/contactmoments'

const contactMoments: PydanticMainContactMomentDetails[] = [
  {
    uuid: '29f07ca8-6c81-4e2f-9a7f-b21305f68290',
    id: 'TBV001',
    contact: 'Peter Pietersen',
    createdAt: '2021-04-03 19:45:24',
    updatedAt: '2021-04-03 19:45:24',
    employeeId: 1,
    customFields: [
      {
        id: 1,
        name: 'Kanaal',
        description: 'Kanaal',
        required: false,
        type: CustomFieldTypes.Text,
        value: 'Balie',
        sortOrder: 1,
      },
      {
        id: 2,
        name: 'Telefoonnummer',
        description: 'Telefoonnummer',
        required: false,
        type: CustomFieldTypes.Text,
        value: '0612345678',
        sortOrder: 2,
      }
    ],
    Kanaal: 'Balie',
    Telefoonnummer: '0612345678'
  },
  {
    uuid: '9bacf0fc-802e-4a40-912e-f65b36891f88',
    id: 'TBV002',
    contact: 'Tessa Tester',
    createdAt: '2021-04-03 19:45:23',
    updatedAt: '2021-04-03 19:45:23',
    customFields: [
      {
        id: 1,
        name: 'Kanaal',
        description: 'Kanaal',
        required: false,
        type: CustomFieldTypes.Text,
        value: '',
        sortOrder: 1,
      },
      {
        id: 2,
        name: 'Telefoonnummer',
        description: 'Telefoonnummer',
        required: false,
        type: CustomFieldTypes.Text,
        value: '',
        sortOrder: 2,
      }
    ],
    employeeId: 2,
    Kanaal: 'Email',
    Telefoonnummer: '0612345678'
  },
  {
    uuid: 'f737b561-e23c-4cd1-a36c-ddf7807b9449',
    id: 'TBV003',
    contact: 'Willem Wever',
    createdAt: '2021-05-04 15:45:00',
    updatedAt: '2021-05-04 15:46:23',
    customFields: [
      {
        id: 1,
        name: 'Kanaal',
        description: 'Kanaal',
        required: false,
        type: CustomFieldTypes.Text,
        value: '',
        sortOrder: 1,
      },
      {
        id: 2,
        name: 'Telefoonnummer',
        description: 'Telefoonnummer',
        required: false,
        type: CustomFieldTypes.Text,
        value: '',
        sortOrder: 2,
      }
    ],
    employeeId: 3,
    Kanaal: 'Balie',
    Telefoonnummer: '0612345678'
  }
]

export default contactMoments
