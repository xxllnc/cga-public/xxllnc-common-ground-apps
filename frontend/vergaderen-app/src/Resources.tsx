import { Resource, ResourceProps } from 'react-admin'
import About from './pages/about'
import Meetings from './meetings'


const fallback = (): JSX.Element[] => [
  <Resource
    name={Meetings.name}
    list={Meetings.list}
    icon={Meetings.icon}
  />
]

export interface CustomResourceProps extends ResourceProps {
  options: {
    label: string
    settings?: boolean
    permissions: string[]
  }
}

const createResourceElement = (resources: CustomResourceProps[]): JSX.Element[] =>
  resources.map(resource => (<Resource {...resource} />))

const availableResources: CustomResourceProps[] = [About, Meetings]

interface Permissions {
  permissions: string[]
}

export const resourcesForPermissions = ({ permissions }: Permissions): JSX.Element[] => {
  if (process.env?.NODE_ENV === 'test') return createResourceElement(availableResources)
  if (process.env?.REACT_APP_MOCK_REQUESTS === 'true') return createResourceElement(availableResources)
  if (!permissions || permissions.length === 0) return fallback()

  const resources = availableResources.filter(resource => resource?.options?.permissions
    .find(permission => permissions.includes(permission)) !== undefined)

  if (!resources || resources.length === 0) return fallback()
  return createResourceElement(resources)
}