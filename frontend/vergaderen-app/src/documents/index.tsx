import { CustomResourceProps } from '../Resources'

const Documents: CustomResourceProps = {
  name: 'documents',
  options: {
    label: 'Documenten',
    permissions: ['cga.admin:read', 'cga.user:read']
  }
}

export default Documents