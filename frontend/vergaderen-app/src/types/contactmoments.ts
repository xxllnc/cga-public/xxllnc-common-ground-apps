/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface ContactMoment {
  contact: string;
  employeeId?: number;
  id: string;

  /** @format uuid */
  uuid: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt?: string;
  Kanaal?: string;
  Telefoonnummer?: string;
}

export interface ContactMomentCreateRequest {
  customFields?: CustomFieldsWithValue[];
  contact: string;
  employeeId?: number;
}

export interface ContactMomentUpdateRequest {
  customFields?: CustomFieldsWithValue[];
  contact?: string;
  employeeId?: number;
  resultId?: number;
}

export interface CustomField {
  /** Integer representing the sort order of the field. */
  sortOrder: number;
  name: string;
  description?: string;
  required?: boolean;
  type: CustomFieldTypes;
  options?: Options[];
  id: number;

  /** @format uuid */
  uuid: string;
}

export interface CustomFieldCreate {
  name: string;
  description?: string;
  required?: boolean;
  type: CustomFieldTypes;
  options?: Options[];
}

export interface CustomFieldCreateUpdate {
  /** Integer representing the sort order of the field. */
  sortOrder?: number;
  name?: string;
  description?: string;
  required?: boolean;
  type?: CustomFieldTypes;
  options?: Options[];
}

/**
 * An enumeration.
 */
export enum CustomFieldTypes {
  String = "string",
  Text = "text",
  Select = "select",
}

export interface CustomFieldsWithValue {
  /** Integer representing the sort order of the field. */
  sortOrder: number;
  name: string;
  description?: string;
  required?: boolean;
  type: CustomFieldTypes;
  options?: Options[];
  id: number;
  value?: string;
}

export interface Employee {
  name: string;
  email: string;
  id: number;

  /** @format uuid */
  uuid: string;
}

export interface EmployeeBase {
  name: string;
  email: string;
}

export interface EmployeeUpdate {
  name?: string;
  email?: string;
}

export interface HTTPValidationError {
  detail?: ValidationError[];
}

export interface Options {
  id: string;
  name: string;
}

export interface ValidationError {
  loc: string[];
  msg: string;
  type: string;
}

export interface AppSchemasContactMomentSchemasContactMomentDetails {
  customFields?: CustomFieldsWithValue[];
  contact: string;
  employeeId?: number;
  id: string;

  /** @format uuid */
  uuid: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt?: string;
}

export interface PydanticMainContactMomentDetails {
  customFields?: CustomFieldsWithValue[];
  contact: string;
  employeeId?: number;
  id: string;

  /** @format uuid */
  uuid: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt?: string;
  Kanaal?: string;
  Telefoonnummer?: string;
}
