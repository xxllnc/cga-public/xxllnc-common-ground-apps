import { CustomResourceProps } from '../Resources'
import { Comment } from '@material-ui/icons/'


const MeetingNotes: CustomResourceProps = {
  name: 'meetingnotes',
  options: {
    label: 'vergader notities',
    permissions: ['cga.admin:read', 'cga.user:read']
  }
}

export default MeetingNotes