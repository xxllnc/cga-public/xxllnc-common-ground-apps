import { FC } from 'react'
import { Admin as ReactAdmin, DataProvider, Resource } from 'react-admin'
import { Route } from 'react-router-dom'
import { useAuth0User, getEnvVariable } from 'xxllnc-react-components'
import polyglotI18nProvider from 'ra-i18n-polyglot'
import { Layout } from './layout/Layout'
import providers from './providers'
import AboutPage from './pages/about/About'
import LoginPage from './pages/login/Login'
import { i18nDutch } from './i18n'
import Meetings from './meetings'
import MeetingItems from './meetingitems'
import Documents from './documents'
import Notes from './notes'
import MeetingNotes from './meetingnotes'
import Decisions from './decisions'

const returnTo = `${window.location.origin}/vergaderen`
const mock = getEnvVariable('REACT_APP_MOCK_REQUESTS').toUpperCase() === 'TRUE'

const i18nProvider = polyglotI18nProvider(
  () => i18nDutch,
  'nl',
  { allowMissing: true }
)

const Admin: FC = () => {
  const auth0 = useAuth0User()

  return (
    <ReactAdmin
      customRoutes={[
        <Route
          path="/about"
          component={AboutPage}
        />,
      ]}
      i18nProvider={i18nProvider}
      dataProvider={providers.data}
      authProvider={providers.auth(auth0, returnTo, mock)}
      layout={Layout}
      loginPage={LoginPage}
      disableTelemetry
    >
      <Resource
        name={Meetings.name}
        list={Meetings.list}
        icon={Meetings.icon}
        show={Meetings.show}
        // edit={Meetings.edit}
        options={Meetings.options}
      />
      <Resource
        name={MeetingItems.name}
        // list={MeetingItems.list}
        // icon={MeetingItems.icon}
        show={MeetingItems.show}
        // edit={Meetings.edit}
        options={MeetingItems.options} />

      <Resource
        name={Documents.name}
        options={Documents.options} />

      <Resource
        name={Notes.name}
        options={Notes.options} />
      <Resource
        name={MeetingNotes.name}
        options={MeetingNotes.options} />

      <Resource
        name={Decisions.name}
        edit={Decisions.edit}
        options={Decisions.options} />

    </ReactAdmin>
  )
}


export default Admin
