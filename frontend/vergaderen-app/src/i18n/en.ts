import englishMessages from 'ra-language-english'

export const i18nEnglish = {
  ...englishMessages,
  employeeId: 'Employee',
  contact: 'Contact',
  id: 'Identification',
  createdAt: 'Creation date',
  cancel: 'Cancel',
  submit: 'Submit',
  Decision: {
    takeDecision: 'Take decision',
    decide: 'Decide',
    decision: 'Decision',
    decisions: 'Decisions',
    remark: 'Remark'
  },
  download: 'Download',
  Meeting: {
    subject: 'Subject',
    description: 'Description',
    caseNumber: 'Casenumber',
    meetingSubject: 'Meeting subject',
    state: 'State',
    chairman: 'Chairman',
    applicant: 'Applicant',
    location: 'Location',
    type: 'Type',
    time: 'Time',
    caseType: 'Casetype',
    attatchments: 'Attatchments',
    agenda: 'Agenda',
    details: 'Details',
  },
  Document: {
    documents: 'Documents',
    fileName: 'Filename',
  },
  Note: {
    addNote: 'Add note',
    updateNote: 'Update note',
    editNote: 'Edit note',
    youAddedANoteOn: 'You added a note on'
  },
  Filters: {
    search: 'Search',
    Time: {
      piriod: 'Piriod',
      today: 'Today',
      tomorrow: 'Tomorrow',
      thisWeek: 'This week',
      nextWeek: 'Next week',
      lastWeek: 'Last week',
      thisMonth: 'This maand',
      nextMonth: 'Next maand',
      lastMonth: 'Last maand',
      earlier: 'Earlier',
    },
    MeetingType: {

    },
    State: {
      nieuw: 'Nieuw',
      started: 'Started',
      completed: 'Completed',
      archived: 'Archived',
    },
    stortorder: 'Stortorder'
  }
}
