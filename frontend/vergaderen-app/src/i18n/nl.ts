/* eslint-disable camelcase */
import dutchMessages from 'ra-language-dutch'
import { nl } from 'xxllnc-react-components'

export const i18nDutch = {
  ...dutchMessages,
  ...nl,
  employeeId: 'Medewerker',
  contact: 'Contact',
  id: 'Identificatie',
  createdAt: 'Aanmaakdatum',
  cancel: 'Anulleren',
  submit: 'Verzenden',
  Decision: {
    takeDecision: 'Besluit nemen',
    decide: 'Beslissen',
    decision: 'Beslissing',
    decisions: 'Beslissingen',
    remark: 'Opmerking'
  },
  download: 'Download',
  Meeting: {
    subject: 'Onderwerp',
    description: 'Beschrijving',
    caseNumber: 'Zaaknummer',
    meetingSubject: 'Vergader onderwerp',
    state: 'Status',
    chairman: 'Voorzitter',
    applicant: 'Aanvrager',
    location: 'Locatie',
    type: 'Type',
    time: 'Tijd',
    caseType: 'Zaaktype',
    attatchments: 'Bijlagen',
    agenda: 'Agenda',
    details: 'Details',
  },
  Document: {
    documents: 'Documenten',
    fileName: 'Bestandsnaam',
  },
  Note: {
    addNote: 'Notitie toevoegen',
    updateNote: 'Update notitie',
    editNote: 'Notitie aanpassen',
    youAddedANoteOn: 'Je hebt een notitie toegevoegd op'
  },
  Filters: {
    search: 'Zoeken',
    Time: {
      piriod: 'Periode',
      today: 'Vandaag',
      tomorrow: 'Morgen',
      thisWeek: 'Deze week',
      nextWeek: 'Volgende week',
      lastWeek: 'Vorige week',
      thisMonth: 'Deze maand',
      nextMonth: 'Volgende maand',
      lastMonth: 'Vorige maand',
      earlier: 'Eerder',
    },
    MeetingType: {

    },
    State: {
      nieuw: 'Nieuw',
      started: 'Begonnen',
      completed: 'Afgerond',
      archived: 'Gearchiveerd',
    },
    stortorder: 'Sortering'
  }

}
