import { TextFieldProps, SelectInput, TextInput, Button, useRecordContext, Record, useMutation, useTranslate } from 'react-admin'
import { FC, useState } from 'react'
import { DialogForm, DialogFormProps } from '../DialogForm'
export interface DesissionFieldProps
  extends TextFieldProps {
  source: string
}

const DecisionField: FC<DesissionFieldProps> = () => {
  const record = useRecordContext<Record>()
  const translate = useTranslate()

  const [showDialog, setshowDialog] = useState(false)
  const handleOpen = () => setshowDialog(true)
  const handleClose = () => setshowDialog(false)
  const [mutate, { loading }] = useMutation()


  const handleSubmit: DialogFormProps['onSubmit'] = (values) => {
    void mutate(
      {
        type: 'update',
        resource: 'decisions',
        payload: { id: record.id, data: { ...record, ...values } }
      }
    )
    void handleClose()
  }

  return (
    <>
      <Button onClick={handleOpen} label='Besluit nemen'></Button>
      <DialogForm
        open={showDialog}
        loading={loading}
        onSubmit={handleSubmit}
        onCancel={handleClose}
        title={translate('Decision.takeDecision')}
        submitLabel={translate('Decision.decide')}
        cancelLabel={translate('cancel')}
      >
        <SelectInput fullWidth source="result" label={translate('Decision.decision')} choices={[
          { id: 'Accoord', name: 'Accoord' },
          { id: 'Accoord mits', name: 'Accoord mits' },
          { id: 'Afgewezen', name: 'Afgewezen' },
        ]} />
        {/* todo decide what the maxLength will be for this method */}
        <TextInput inputProps={{ maxLength: 50 }} multiline fullWidth label={translate('Decision.remark')} source="remark" />
      </DialogForm>
    </>
  )

}

export default DecisionField