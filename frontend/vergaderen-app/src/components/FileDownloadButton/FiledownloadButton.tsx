import { CloudDownload } from '@material-ui/icons'
import { Button, ButtonProps, useRecordContext, useTranslate } from 'react-admin'
import PropTypes from 'prop-types'

export const FileDownloadButton = (props: FileDownloadButtonProps): JSX.Element => {
  const record = useRecordContext(props)
  const translate = useTranslate()
  return (
    <Button label={translate('Download')} onClick={() => {
      // -- Safe as no value holds user input
      // eslint-disable-next-line @typescript-eslint/no-unsafe-argument, security/detect-non-literal-fs-filename
      window.open(record[props.source], '_blank')
    }}><CloudDownload /></Button>
  )
}
FileDownloadButton.propTypes = {
  label: PropTypes.string,
  record: PropTypes.object,
  source: PropTypes.string.isRequired,
}

export interface FileDownloadButtonProps
  extends ButtonProps {
  source: string
}
