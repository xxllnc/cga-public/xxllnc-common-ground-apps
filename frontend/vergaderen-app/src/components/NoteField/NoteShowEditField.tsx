import { Button, FilledInput, IconButton, makeStyles, Tooltip, Typography } from '@material-ui/core'
import { useRecordContext, useUpdate } from 'ra-core'
import EditIcon from '@material-ui/icons/Edit'
import { useState, ChangeEvent, FormEvent } from 'react'
import { DateField } from 'ra-ui-materialui'
import { useTranslate } from 'react-admin'


const useStyles = makeStyles(theme => ({
  root: {
    marginBottom: theme.spacing(2),
  },
  metadata: {
    marginBottom: theme.spacing(1),
    color: theme.palette.text.secondary,
  },
  metadataDate: {
    fontSize: '1rem'
  },
  textarea: {
    paddingTop: 16,
    paddingLeft: 14,
    paddingRight: 60,
    paddingBottom: 14,
    lineHeight: 1.3,
  },
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: theme.spacing(1),
  },
  cancel: {
    marginRight: theme.spacing(1),
  },
  content: {
    backgroundColor: '#efefef',
    padding: '0 1em',
    borderRadius: 10,
    display: 'flex',
    alignItems: 'stretch',
    width: '50vw',
    minHeight: '10vh',
    marginBottom: theme.spacing(1),
  },
  forminput: {
    padding: '0 1em',
    borderRadius: 10,
    display: 'flex',
    alignItems: 'stretch',
    minWidth: '50vw',
    minHeight: '10vh',
    marginBottom: theme.spacing(1),
  },
  text: {
    flex: 1,
  },
  paragraph: {
    fontFamily: theme.typography.fontFamily,
    fontSize: theme.typography.body1.fontSize,
    lineHeight: 1.3,
    marginBottom: theme.spacing(2.4),
  },
  toolbar: {
    marginLeft: theme.spacing(2),
    visibility: 'hidden',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  form: {
    minWidth: 'inherit',
    minHeight: 'inherit'
  },
  label: {
    color: theme.palette.text.secondary,
  }
}))

const NoteShowEditField = (): JSX.Element => {
  const [isHover, setHover] = useState(false)
  const [isEditing, setEditing] = useState(false)
  const record: { content: string, id: string } = useRecordContext()
  const [noteText, setNoteText] = useState(record.content)
  const translate = useTranslate()
  const classes = useStyles()

  const handleEnterEditMode = () => {
    setEditing(true)
  }

  const handleCancelEdit = () => {
    setEditing(false)
    setNoteText(record?.content)
    setHover(false)
  }

  const handleTextChange = (event: ChangeEvent<HTMLInputElement>) => {
    setNoteText(event.target.value)
  }

  const [updateNote] = useUpdate(
    'notes',
    record?.id,
    {
      ...record,
      content: noteText,
      createdAt: new Date().toISOString()
    },
    record
  )

  const handleNoteUpdate = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    void updateNote()
    setEditing(false)
    setHover(false)
  }

  return (
    <div
      className={classes.root}
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      {isEditing ? (
        <div className={classes.forminput}>
          <form className={classes.form} onSubmit={handleNoteUpdate}>
            <FilledInput
              value={noteText}
              onChange={handleTextChange}
              fullWidth
              multiline
              className={classes.textarea}
            />
            <div className={classes.buttons}>
              <Button
                className={classes.cancel}
                onClick={handleCancelEdit}
                color="primary"
              >
                {translate('cancel')}
              </Button>
              <Button
                type="submit"
                color="primary"
                variant="contained"
              >
                {translate('Note.updateNote')}
              </Button>
            </div>
          </form>
        </div>
      ) : (
        <div>
          <div className={classes.content}>
            <div className={classes.text}>
              {record.content
                .split('\n')
                .map((paragraph: string, index: number) => (
                  <p className={classes.paragraph} key={index}>
                    {paragraph}
                  </p>
                ))}
            </div>
            <div
              className={classes.toolbar}
              style={{ visibility: isHover ? 'visible' : 'hidden' }}
            >
              <Tooltip title={translate('Note.editNote')}>
                <IconButton size="small"
                  onClick={handleEnterEditMode}>
                  <EditIcon />
                </IconButton>
              </Tooltip>
            </div>
          </div>
          <div className={classes.metadata}>
            <Typography component="span">
              {translate('Note.youAddedANoteOn')}{' '}
            </Typography>
            <DateField className={classes.metadataDate}
              source="createdAt"
              record={record}
              showTime
              locales="nl"
              options={{
                dateStyle: 'full',
                timeStyle: 'short',
              }}
            />
          </div>
        </div>
      )}
    </div>
  )
}

export default NoteShowEditField
