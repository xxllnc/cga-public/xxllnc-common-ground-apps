import { Button, FilledInput, makeStyles } from '@material-ui/core'
import { useRecordContext, Record } from 'ra-core'
import { useState, ChangeEvent, FormEvent } from 'react'
import { Labeled, ReferenceField, useCreate, useGetOne, useTranslate } from 'react-admin'
import { getUrl } from '../../AppUrls'
import NoteShowEditField from './NoteShowEditField'


const useStyles = makeStyles(theme => ({
  root: {
    marginBottom: theme.spacing(2),
  },
  metadata: {
    marginBottom: theme.spacing(1),
    color: theme.palette.text.secondary,
  },
  textarea: {
    paddingTop: 16,
    paddingLeft: 14,
    paddingRight: 60,
    paddingBottom: 14,
    lineHeight: 1.3,
  },
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: theme.spacing(1),
  },
  cancel: {
    marginRight: theme.spacing(1),
  },
  content: {
    backgroundColor: '#efefef',
    padding: '0 1em',
    borderRadius: 10,
    display: 'flex',
    alignItems: 'stretch',
    width: '50vw',
    minHeight: '10vh',
    marginBottom: theme.spacing(1),
  },
  forminput: {
    padding: '0 1em',
    borderRadius: 10,
    display: 'flex',
    alignItems: 'stretch',
    minWidth: '50vw',
    minHeight: '10vh',
    marginBottom: theme.spacing(1),
  },
  text: {
    flex: 1,
  },
  paragraph: {
    fontFamily: theme.typography.fontFamily,
    fontSize: theme.typography.body1.fontSize,
    lineHeight: 1.3,
    marginBottom: theme.spacing(2.4),
  },
  toolbar: {
    marginLeft: theme.spacing(2),
    visibility: 'hidden',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  form: {
    minWidth: 'inherit',
    minHeight: 'inherit'
  },
  label: {
    color: theme.palette.text.secondary,
  }
}))

const Notefield = (): JSX.Element => {
  const record = useRecordContext<Record & { content: string }>()
  const [noteText, setNoteText] = useState<string>(record.content)
  const noteuuid = self.crypto.randomUUID()
  const { data } = useGetOne('meetingnotes', record.id)
  const hasNote = data !== undefined
  const translate = useTranslate()
  const classes = useStyles()

  const handleTextChange = (event: ChangeEvent<HTMLInputElement>) => {
    setNoteText(event.target.value)
  }

  const [createmeetingnote] = useCreate(
    'meetingnotes',
    {
      id: record.id,
      note: noteuuid
    }
  )

  const [createnote] = useCreate(
    'notes',
    {
      id: noteuuid,
      url: `${getUrl() || 'http://backup.zaaken.nl'}/notes/${noteuuid}`,
      content: noteText,
      createdAt: new Date().toISOString()
    }
  )


  const handleNoteCreate = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    void createnote()
    void createmeetingnote()

  }

  return (
    <Labeled label="Persoonlijke Notitie" className={classes.label}>
      <div>
        {!hasNote ? (
          <div className={classes.root}>
            <div className={classes.forminput}>
              <form className={classes.form} onSubmit={handleNoteCreate}>
                <FilledInput
                  value={noteText}
                  onChange={handleTextChange}
                  fullWidth
                  multiline
                  className={classes.textarea}
                />
                <div className={classes.buttons}>
                  <Button
                    type='submit'
                    color='primary'
                    variant='contained'
                  >
                    {translate('Note.addNote')}
                  </Button>
                </div>
              </form>
            </div>
          </div>
        ) : (
          <ReferenceField source="id" reference="meetingnotes" link={false}>
            <ReferenceField source='note' reference='notes' link={false}>
              <NoteShowEditField />
            </ReferenceField>
          </ReferenceField>
        )}
      </div>
    </Labeled>
  )
}
export default Notefield