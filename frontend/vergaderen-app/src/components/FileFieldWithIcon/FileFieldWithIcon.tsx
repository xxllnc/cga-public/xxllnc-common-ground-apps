import { makeStyles } from '@material-ui/core'
import PropTypes from 'prop-types'
import { FC } from 'react'
import { FileFieldProps, useRecordContext, Record } from 'react-admin'
import { FileIcon, defaultStyles } from 'react-file-icon'

const useStyles = makeStyles({
  icon: {
    width: '16px',
    display: 'inline-block',
    padding: '0px 8px 0px 0px'
  },
})

export interface FileFieldWithIconProps
  extends FileFieldProps {
  name?: string
}

export interface FileRecord extends Record {
  name?: string
}
export const FileFieldWithIcon: FC<FileFieldWithIconProps> = (props) => {
  const record = useRecordContext<FileRecord>(props)
  const name = record?.name
  const classes = useStyles()

  return name ? (
    <span>
      <span className={classes.icon}>
        {getIcon(name.split('.')?.[1])}
      </span>
      {name.split('.')?.[0]}
    </span>
  ) : null
}

const getIcon = (extention: string) => {
  const styledIcons = Object.keys(defaultStyles)

  if (styledIcons.includes(extention)) {
    return <FileIcon extension={extention} {...defaultStyles[String(extention)]} />
  } else {
    return <FileIcon extension='document' type='document' />
  }
}


