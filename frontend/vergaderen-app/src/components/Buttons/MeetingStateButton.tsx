import { Button, makeStyles } from '@material-ui/core'
import PropTypes from 'prop-types'
import { ButtonProps, useRecordContext, useUpdate } from 'react-admin'

const useStyles = makeStyles({
  buttons: {
    display: 'flex',
    justifyContent: 'space-between'
  },
})

export const MeetingStateButton = (props: MeetingStateButtonProps): JSX.Element => {
  const record = useRecordContext(props)
  const classes = useStyles()
  const [updateState] = useUpdate(
    'meetings',
    record?.id,
    {
      ...record,
      // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
      state: getNextState(record[props.source])
    },
    record
  )

  const [updateStatePrevius] = useUpdate(
    'meetings',
    record?.id,
    {
      ...record,
      // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
      state: getPreviusState(record[props.source])
    },
    record
  )

  const currentState = record[props.source] as unknown as string
  return (
    <div className={classes.buttons}>
      <Button
        color='secondary'
        onClick={void updateStatePrevius}
        disabled={currentState === 'Nieuw'}
        variant='contained'>Vorige status</Button>
      <Button
        color='secondary'
        onClick={void updateState}
        disabled={currentState === 'Archived'}
        variant='contained'>
        {// eslint-disable-next-line security/detect-object-injection
          `Vergadering ${stateChangesButtons[currentState] as string}`
        }
      </Button>
    </div>
  )

}

MeetingStateButton.propTypes = {
  label: PropTypes.string,
  record: PropTypes.object,
  source: PropTypes.string.isRequired,
}

type State = 'Nieuw' | 'Started' | 'Completed' | 'Archived'
const stateChanges: { [key in State]: State | null } = {
  Nieuw: 'Started',
  Started: 'Completed',
  Completed: 'Archived',
  Archived: null
}
const stateChangesButtons: { [key in State]: string } = {
  Nieuw: 'Starten',
  Started: 'Afronden',
  Completed: 'Archiveren',
  Archived: 'Gearchiveerd'
}
// eslint-disable-next-line security/detect-object-injection
const getNextState = (currentState: State): State | null => stateChanges[currentState]

const getPreviusState = (currentState: State): State | null => (
  // eslint-disable-next-line security/detect-object-injection
  Object.keys(stateChanges).find(key => stateChanges[key] === currentState) as State ?? null
)


export interface MeetingStateButtonProps
  extends ButtonProps {
  source: string
}
