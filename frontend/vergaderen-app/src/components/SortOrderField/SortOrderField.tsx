import { TextFieldProps, useRecordContext, useRefresh, useUpdate } from 'react-admin'
import { IconButton } from '@material-ui/core'
import { ArrowUpward, ArrowDownward } from '@material-ui/icons'
import { FC } from 'react'

export interface SortOrderFieldProps
  extends TextFieldProps {
  source: string
}

export const SortOrderField: FC<SortOrderFieldProps> = (props) => {

  const { source } = props
  const record = useRecordContext(props)
  const refresh = useRefresh()

  const [change] = useUpdate()

  const onChange = async (diff: number) => {
    // -- Safe as no value holds user input
    // eslint-disable-next-line security/detect-object-injection
    await change('meetingitems', record.id, { id: record.id, data: { ...record, sortOrder: record[source] as number + diff } }, record)
    refresh()
  }

  return <>
    <IconButton
      aria-label="down"
      // disabled={downStatus.loading}
      onClick={() => void onChange(-1)}
    >
      <ArrowUpward fontSize="small" />
    </IconButton>
    <IconButton
      aria-label="up"
      // disabled={upStatus.loading}
      onClick={() => void onChange(1)}
    >
      <ArrowDownward fontSize="small" />
    </IconButton>
  </>
}
