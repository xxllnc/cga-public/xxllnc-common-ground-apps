import { CustomResourceProps } from '../Resources'

const Decisions: CustomResourceProps = {
  name: 'decisions',
  options: {
    label: 'Beslissingen',
    permissions: ['cga.admin:read', 'cga.user:read']
  }
}

export default Decisions