import { CustomResourceProps } from '../Resources'
import { Comment } from '@material-ui/icons/'


const Notes: CustomResourceProps = {
  name: 'notes',
  //   list: HybridMeetingList,
  // create: ContactMomentsCreate,
  // edit: MeetingEdit,
  //   show: MeetingItemsShow,
  icon: Comment,
  options: {
    label: 'Notities',
    permissions: ['cga.admin:read', 'cga.user:read']
  }
}

export default Notes