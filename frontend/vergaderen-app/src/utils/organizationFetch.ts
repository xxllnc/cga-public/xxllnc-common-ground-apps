/* eslint-disable camelcase */
import { fetchUtils } from 'ra-core'
// TODO get type from gebruikersbeheer?
export interface OrganizationBase {
  /** @example org_AEglyXUkuFrbC2Rs */
  id: string
  name: string
  display_name: string
}

const mockFetch = (): OrganizationBase | null => {
  if (process.env?.REACT_APP_MOCK_ORGANIZATION_FETCH?.toLowerCase === 'true'.toLowerCase)
    return {
      id: process.env?.REACT_APP_MOCK_ORGANIZATION_FETCH_ID ?? '',
      name: 'exxellence',
      display_name: 'Exxellence Google'
    }

  return null
}

const isOrg = (object: unknown): object is OrganizationBase => Object.prototype.hasOwnProperty.call(object, 'id')
  && Object.prototype.hasOwnProperty.call(object, 'name')

const organizationFetch = async (url: string, name: string): Promise<OrganizationBase> => {

  const mock = mockFetch()
  if (mock) return Promise.resolve(mock)

  const data = await fetchUtils.fetchJson(`${url}/organizations/${name}`)
  return (isOrg(data.json)) ? Promise.resolve(data.json) : Promise.reject()
}

export default organizationFetch
