import { authToken } from 'xxllnc-react-components'

export const getPermissions = (): string[] => {
  try {
    const token = authToken.getToken() || ''
    const parsed = JSON.parse(atob(token.split('.')[1])) as {permissions: string[]}
    return parsed.permissions
  } catch (e) {
    return []
  }
}
