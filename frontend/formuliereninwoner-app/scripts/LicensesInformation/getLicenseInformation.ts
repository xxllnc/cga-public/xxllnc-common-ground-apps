// yarn ts-node --project ./node.tsconfig.json  scripts/LicensesInformation/getLicenseInformation.ts
import { existsSync, readFileSync, writeFileSync } from 'fs'
import checker from 'license-checker-rseidelsohn'

export interface DetailsOfLicense {
  licenses: string
  repository?: string
  licenseUrl?: string
  publisher?: string
}

interface Licenses { [dependency: string]: Partial<DetailsOfLicense> }

const scanPackages = () => {

  const packageJsonDependencies: string[] = []
  const packageJsonPaths = ['./']

  packageJsonPaths.forEach((path) => {
    const packageJsonFile = JSON.parse(readFileSync(path + 'package.json', 'utf8')) as Licenses
    const dependencies = packageJsonFile.dependencies
    if (dependencies !== undefined) {
      Object.entries(dependencies).forEach(([key, value]) => {
        if (value) {
          const dependency = `${key}@${(value.includes('^') ? value.substring(1) : value)}`
          if (!packageJsonDependencies.includes(dependency)) {
            packageJsonDependencies.push(dependency)
          }
        }
      })
    }
  })

  if (packageJsonDependencies.length > 0)
    checker.init({
      start: './',
      direct: true
    }, (err: Error, packages: Licenses) => {
      if (err) {
        console.log(err)
      } else {
        const filteredPackages = Object.keys(packages).filter(i => packageJsonDependencies.includes(i))
        const licenses: Licenses = {}
        filteredPackages.forEach((filteredPackage) => {
          const licenseContentObject = packages[String(filteredPackage)] as Licenses
          Object.keys(licenseContentObject).forEach((key) => {
            if (key === 'path' || key === 'licenseFile') {
              delete licenseContentObject[String(key)]
            }
          })
          licenses[String(filteredPackage)] = licenseContentObject
        })

        packageJsonPaths.forEach((path) => {
          if (existsSync(`${path}src/`))
            writeFileSync(path + 'src/licenses.json', JSON.stringify(licenses, null, '\t'))
        })
      }
    })
}

console.log('Start scanning packages')
scanPackages()
console.log('Scanning packages complete')
