import { fireEvent, render, screen, waitFor } from '@testing-library/react'
import { StoreProvider } from 'easy-peasy'
import { BrowserRouter } from 'react-router-dom'
import { store } from 'store'
import { Status } from 'types/generatedFormTypes'
import { FormPage } from './FormPage'

jest.setTimeout(60000)

describe('FormPage test', () => {
  const mockJsonResponse = Promise.resolve({ status: Status.ACTIVE }) // Ignore rest of response since we dont use it
  const mockSuccess = Promise.resolve({ json: () => mockJsonResponse, ok: true })
  const mockFailed = Promise.resolve({ ok: false })


  const globalRef = global

  const renderForm = () => (
    render(
      <BrowserRouter>
        <StoreProvider store={store}>
          <FormPage />
        </StoreProvider>
      </BrowserRouter>
    )
  )

  it('should display the thank you page when the form is submitted', async () => {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    globalRef.fetch = jest.fn().mockImplementation(() => mockSuccess)


    //Given
    renderForm()

    // We expect to be able to submit the form

    // Enter the required fields
    await waitFor(() => expect(screen.getByLabelText('Voornaam')).toBeDefined())
    const nameElement: HTMLInputElement = screen.getByLabelText('Voornaam')
    await waitFor(() => fireEvent.input(nameElement, { target: { value: 'Henk' } }))

    fireEvent.click(screen.getByRole('button', { name: 'Volgende button. Click to go to the next tab' }))
    await waitFor(() => expect(screen.getByLabelText('Voornaam')).toBeDefined())

    const checkbox = screen.getAllByRole('checkbox', { checked: false })[0] as HTMLInputElement
    fireEvent.click(checkbox)

    await waitFor(() => expect(checkbox.checked).toEqual(true))
    fireEvent.click(screen.getByRole('button', { name: 'Versturen button. Click to submit the form' }))
    await waitFor(() => expect(screen.getByRole('button', { name: 'Afsluiten' })).toBeDefined())
    expect(screen.getByText('Bedankt!')).toBeDefined()
  })

  it('should return an error message if the submit failed', async () => {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    globalRef.fetch = jest.fn().mockImplementation(() => mockFailed)

    //Given
    renderForm()

    // We expect to be able to submit the form

    // Enter the required fields
    await waitFor(() => expect(screen.getByLabelText('Voornaam')).toBeDefined())
    const nameElement: HTMLInputElement = screen.getByLabelText('Voornaam')
    await waitFor(() => fireEvent.input(nameElement, { target: { value: 'Henk' } }))

    await waitFor(() => fireEvent.click(screen.getByRole('button', { name: 'Volgende button. Click to go to the next tab' })))

    const checkbox = screen.getAllByRole('checkbox', { checked: false })[0] as HTMLInputElement
    fireEvent.click(checkbox)

    await waitFor(() => fireEvent.click(screen.getByRole('button', { name: 'Versturen button. Click to submit the form' })))

    await waitFor(() => expect(screen.queryAllByText('Bedankt!')).toHaveLength(0))

  })

  it('should not be able to navigate with the buttons in the sidebar', async () => {

    //Given
    renderForm()

    // Enter the required fields
    await waitFor(() => expect(screen.getByLabelText('Voornaam')).toBeDefined())
    const nameElement: HTMLInputElement = screen.getByLabelText('Voornaam')
    await waitFor(() => fireEvent.input(nameElement, { target: { value: 'Henk' } }))

    // We expect to go to the selected pages
    await waitFor(() => fireEvent.click(screen.getByRole('button', { name: 'Afronden knop. Ga naar pagina Afronden' })))
    expect(screen.queryByText('Stap 2 van 2')).toBeNull()
  })

  it('should be able to find the location search field', async () => {

    //Given
    renderForm()

    // We expect the search bar for an address to be present
    await waitFor(() => expect(screen.findByPlaceholderText('Zoek een adres')).toBeDefined())
  })

  it('should redirect after click on cancel', async () => {

    const { location } = window

    Reflect.deleteProperty(window, 'location')
    Object.defineProperty(window, 'location', {
      value: new URL('http://localhost')
    })

    //Given
    renderForm()

    // Press the cancel button
    await waitFor(() => expect(screen.getByRole('button', { name: 'Annuleren button. Click to reset the form' })).toBeDefined())
    fireEvent.click(screen.getByRole('button', { name: 'Annuleren button. Click to reset the form' }))
    await waitFor(() => expect(screen.getByText('Weet u het zeker dat u wilt stoppen?')).toBeDefined())

    fireEvent.click(screen.getByRole('button', { name: 'Ja' }))
    expect(window.location.href).toEqual('https://xxllnc.nl/')

    Object.defineProperty(window, 'location', location)

  })
})
