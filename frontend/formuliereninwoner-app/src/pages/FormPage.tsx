import { FC, useCallback, useEffect, useRef, useState } from 'react'
import { useParams } from 'react-router-dom'
import { Form as FormIo, Formio } from 'formiojs'
import { CircularProgress, Typography } from '@material-ui/core'
import { Components, Form, FormSchema, Submission } from '@tsed/react-formio'
import { CustomFormioComponents } from 'xxllnc-formio-components'
import { Isolate, Page, Sidebar, CancelFormModal } from 'components'
import { useTemplateOverrides } from 'guards'
import { useCallbackData, useForceUpdate, useFormData } from 'hooks'
import { ThankYouPage } from 'pages'
import { useStoreActions, useStoreState } from 'store'
import { nl } from 'translations'
import { IForm } from 'types/form'
import { checkIfComponentHasRequiredFields } from 'utils/checkIfComponentHasRequiredFields'
import { getEnvVariable, addUserMetaDataPage, addUserMetaData } from 'utils'
import useSession from 'guards/useSession'
import '../styles/css/main.css'
import '../styles/css/normalize.css'

Formio.use(CustomFormioComponents)

export const FormPage: FC = () => {
  const params = useParams<{ formId: string }>()
  const useQueryParameters = (): URLSearchParams => new URLSearchParams(window.location.search)
  const query = useQueryParameters()

  const getFormId = () => query?.get('formId') ? query.get('formId') : params?.formId ?? ''

  const formRef = useRef<IForm | null>(null)
  const defaultDocumentTitle = getEnvVariable('REACT_APP_TITLE')
  const { submitForm, error, isSubmitting, isLoading, isSubmitDone } = useFormData(getFormId())
  const forceUpdate = useForceUpdate()
  const [activePageIndex, setActivePageIndex] = useState(0)
  const [highestVisitedPageIndex, setHighestVisitedPageIndex] = useState(0)
  const [requiredFieldsOnPages, setRequiredFieldsOnPages] = useState<boolean[]>([])
  const [cancelForm, setCancelForm] = useState(false)

  const form = useStoreState((state) => state.formData.form)
  const setForm = useStoreActions(actions => actions.formData.setForm)
  const metaData = useStoreState((state) => state.authentication.metaData)
  const generalSettings = useStoreState(state => state.settings.generalSettings)
  const setShouldLogin = useStoreActions(actions => actions.authentication.setShouldLogin)
  const urlAfsluitenKnop = () => form?.config?.urlAfsluitenKnop || generalSettings?.urlAfsluitenKnop?.value || 'https://xxllnc.nl'
  const submitCompleteMessage = () => form?.config?.submitCompletedMessage || generalSettings?.submitCompletedMessage?.value
    || 'Het indienen van het formulier is succesvol verlopen.'

  const { isReady, UserSession } = useSession({ config: form?.config, generalSettings })

  useCallbackData(formRef)
  useTemplateOverrides()

  Components.setComponents(CustomFormioComponents)

  useEffect(() => {
    if (metaData) {
      addUserMetaDataPage(form, setForm)
      // Delay adding data to make sure formRef is set
      setTimeout(() => addUserMetaData(formRef, metaData), 200)
    }
  }, [metaData])

  useEffect(() => {
    document.title = form?.name ?? defaultDocumentTitle
  }, [form?.name])

  useEffect(() => {
    if (error) formRef.current?.setAlert('danger', error)
  }, [error])

  const init = useRef(true)
  useEffect(() => {
    if (init.current) {
      init.current = false
    } else {
      formRef.current?.setPage(activePageIndex)
      if (activePageIndex > highestVisitedPageIndex)
        setHighestVisitedPageIndex(activePageIndex)
    }
  }, [activePageIndex])

  useEffect(() => {
    if (form?.formContent)
      setRequiredFieldsOnPages(
        form.formContent.components.map(page =>
          // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
          checkIfComponentHasRequiredFields(page.components)
        )
      )
  }, [form])

  const onFormReady = useCallback((formioRef: IForm | FormIo) => {
    const shouldUpdate = formRef.current === null
    if (formRef) {
      formRef.current = formioRef as IForm

      // FormIo Form Events: https://help.form.io/developers/form-renderer#form-events
      // formioRef.on('pagesChanged', () => { })
    }
    if (shouldUpdate) forceUpdate()
  }, [])

  const showOrHideButtons = (disabled: boolean) => {
    const elements = document.getElementsByClassName('btn') as HTMLCollectionOf<HTMLButtonElement>
    Array.from(elements).forEach(element => element.disabled = disabled)
  }

  const onNextOrPrev = (event: { page: number }) => setActivePageIndex(event.page)

  const getStepTitle = (): string => {
    if (formRef.current?.allPages) {
      const numberOfPages = formRef.current.allPages.length
      return `Stap ${activePageIndex + 1} van ${numberOfPages}`
    }
    return ''
  }

  // Replace with a customEvent hook instead of fucntion
  const handleCustomEvents = (event: { data: any, type: any }): void => {
    let url = window.location.href
    const index = url.indexOf('?')
    url = url.substring(0, index !== -1 ? index : url.length)
    /*
    * We use this handleCustomEvent switch for demo pupose only, this will be replaced by actual calls.
    */
    switch (event.type) {
      case 'payWithIdeal': window.location.href = url + '?callback=payment_success&uuid=111'; break
      case 'payWithBank': window.location.href = url + '?callback=payment_failed&uuid=111'; break
      case 'login': setShouldLogin(); break
      default: break
    }
  }

  const onCancel = () => {
    setActivePageIndex(0)
    window.location.href = urlAfsluitenKnop()
  }

  const options = {
    language: 'nl',
    i18n: { nl },
    // FormIo Hooks: https://help.form.io/developers/form-renderer#hooks
    hooks: {
      beforeCancel: () => {
        setCancelForm(true)
        return false
      },
    }
  }

  if (isSubmitDone && form && !error) {
    return <ThankYouPage urlAfsluitenKnop={urlAfsluitenKnop()} submitCompleteMessage={submitCompleteMessage()} />
  }

  return (
    <>
      <UserSession />
      <Page
        title={form?.name || ''}
        currentPage={activePageIndex}
        formRef={formRef.current}
      >
        <div className="row">
          <Sidebar
            allPages={formRef.current?.allPages ?? []}
            updatePage={(newPageIndex: number) => setActivePageIndex(newPageIndex)}
            activePageIndex={activePageIndex}
            highestVisitedPageIndex={highestVisitedPageIndex}
            isSubmitting={isSubmitting}
          />
          <div id="xxllncForm" className="form lg-8 xl-6">
            {(isLoading || !isReady) &&
              <div className="loading">
                <p >Bezig met laden...</p>
                <CircularProgress size={25} thickness={2} color={'inherit'} />
              </div>
            }
            {!form && error && <p>{error}</p>}
            <CancelFormModal cancelForm={cancelForm} setCancelForm={setCancelForm} onCancel={onCancel} />
            {form && isReady && <>
              <span className="steps-title-text sub">{getStepTitle()}</span>
              <Isolate>
                <Form
                  form={form.formContent}
                  onFormReady={onFormReady}
                  onNextPage={onNextOrPrev}
                  onPrevPage={onNextOrPrev}
                  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                  // @ts-ignore ignore is nessessary because of error in options type definition
                  options={options}
                  onCustomEvent={handleCustomEvents}
                  onAsyncSubmit={async (submission: Submission<FormSchema>) => {
                    // We need to overwrite the submit because formIo does not disable the navigation buttons during submit
                    showOrHideButtons(true)
                    await submitForm(submission)
                    showOrHideButtons(false)
                    return Promise.reject()
                  }}
                />
              </Isolate>
              {requiredFieldsOnPages?.[Number(activePageIndex)] &&
                <Typography>Velden met een <span style={{ color: '#EB0000' }}>*</span> zijn verplicht.</Typography>
              }
            </>}
          </div>
        </div>
      </Page>
    </>
  )
}

