import { FC } from 'react'
import { Page } from 'components/layout/defaultPage'

interface Props {
  urlAfsluitenKnop: string
  submitCompleteMessage: string
}

export const ThankYouPage: FC<Props> = ({ urlAfsluitenKnop, submitCompleteMessage }) => (
  <Page className="thankyou-page">
    <h1>{'Bedankt!'}</h1>
    <p>{submitCompleteMessage}</p>
    <button
      className="btn btn-primary"
      onClick={() => window.location.href = urlAfsluitenKnop}
    >
      Afsluiten
    </button>
  </Page>
)