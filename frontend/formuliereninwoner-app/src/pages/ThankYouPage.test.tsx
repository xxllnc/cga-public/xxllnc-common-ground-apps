/* eslint-disable @typescript-eslint/ban-ts-comment */
import { fireEvent, render, screen, waitFor } from '@testing-library/react'
import { ThankYouPage } from 'pages'
import { store } from 'store'
import { StoreProvider } from 'easy-peasy'

describe('ThankYouPage test', () => {

  // Add mock to prevent console.error in testlog
  // @ts-ignore
  delete window.location
  // @ts-ignore
  window.location = { assign: jest.fn() }

  it('should render a thank you message when no error is provided', async () => {

    //Given
    render(
      // https://github.com/ctrlplusb/easy-peasy/issues/741
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      <StoreProvider store={store}>
        <ThankYouPage
          submitCompleteMessage="Het indienen van het formulier is succesvol verlopen."
          urlAfsluitenKnop='https://xxllnc.nl'
        />
      </StoreProvider>
    )

    // We expect to see a thank you message
    await waitFor(() => expect(screen.getByText('Bedankt!')).toBeDefined())
    expect(screen.getByText('Het indienen van het formulier is succesvol verlopen.')).toBeDefined()
    fireEvent.click(screen.getByRole('button', { name: 'Afsluiten' }))
  })
})
