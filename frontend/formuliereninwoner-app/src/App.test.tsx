/* eslint-disable @typescript-eslint/ban-ts-comment */
import { render, screen, waitFor } from '@testing-library/react'
import App from './App'
import { store } from 'store'
import { StoreProvider } from 'easy-peasy'


Object.defineProperty(global.self, 'crypto', {
  value: {
    // @ts-ignore
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-return
    getRandomValues: (arr: string | unknown[]) => crypto.randomBytes(arr.length)
  }
})
// @ts-ignore
global.crypto.subtle = {}


describe('App test', () => {

  it('should render page not found when no path is provided', async () => {

    //Given
    render(
      // https://github.com/ctrlplusb/easy-peasy/issues/741
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      <StoreProvider store={store}>
        <App />
      </StoreProvider>
    )


    // We expect to see the page not found page
    await waitFor(() => expect(screen.getByText('Pagina niet gevonden')).toBeDefined())
  })

  it('should render the form page when the correct path is used', async () => {
    window.history.pushState({}, 'Test page', '/formulieren/mockedForm')

    render(
      // https://github.com/ctrlplusb/easy-peasy/issues/741
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      <StoreProvider store={store}>
        <App />
      </StoreProvider>
    )

    // We expect to see the formpage
    await waitFor(() => expect(screen.getByText('mockedForm')).toBeDefined())
  })
})
