import ReactDOM from 'react-dom/client'
import { StoreProvider } from 'easy-peasy'
import App from './App'
import { store } from 'store'
import './styles/page.scss'

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
)

root.render(
  // https://github.com/ctrlplusb/easy-peasy/issues/741
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  <StoreProvider store={store}>
    <App />
  </StoreProvider>
)
