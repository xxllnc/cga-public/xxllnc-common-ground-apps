import { authToken } from 'utils/authToken'

interface Props {
  method?: 'POST'
  headers?: Headers
}
export const createHeader = ({ method, headers = new Headers() }: Props): Headers => {

  headers.append('organization-url', window.origin)

  if (method === 'POST') headers.append('Content-Type', 'application/json')

  const token = authToken.getToken()
  if (token) headers.append('Authorization', token)

  return headers
}

export const postOptions = (data: string): RequestInit => ({
  method: 'POST',
  headers: createHeader({ method: 'POST' }),
  mode: 'cors',
  body: data
})

