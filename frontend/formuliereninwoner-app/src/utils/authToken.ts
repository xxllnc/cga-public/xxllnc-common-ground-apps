import jwtDecode from 'jwt-decode'

export interface OrganizationBase {
  organization: string
}

interface Token {
  'https://xxllnc.nl/organization_name': string
}

interface AuthToken {
  getToken: () => string | null
  setToken: (token: string) => void
  deleteToken: () => void,
  getOrganization: () => OrganizationBase | null,
  setOrganization: ({ organization }: OrganizationBase) => void,
  deleteOrganization: () => void,
  logout: () => void
}

const tokenName = 'token'

export const authToken: AuthToken = {
  getToken: () => localStorage.getItem(tokenName),
  setToken: (token) => {
    localStorage.setItem(tokenName, `Bearer ${token}`)
    try {
      const decoded = jwtDecode<Token>(token)
      const organizationName = decoded['https://xxllnc.nl/organization_name']
      if (organizationName !== undefined) localStorage.setItem('organization_name', organizationName)
    } catch (_e) {
      // invalid token, do nothing
    }
  },
  deleteToken: () => localStorage.removeItem(tokenName),
  getOrganization: () => {
    const id = localStorage.getItem('organization_id') ?? null
    return id ? { organization: id } : null
  },
  setOrganization: ({ organization }) => {
    localStorage.setItem('organization_id', organization ?? '')
  },
  deleteOrganization: () => { localStorage.removeItem('organization_id') },
  logout: () => {
    authToken.deleteToken()
  }
}
