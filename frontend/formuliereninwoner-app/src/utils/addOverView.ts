import { FormSchema } from '@tsed/react-formio'
import { ExtendedComponentSchema } from 'formiojs'
import confirmationCheckBoxComponent from '../jsonFiles/confirmationCheckBoxComponent.json'

/*
 * A single panel is created with all pages.
 * Each component is disabled as it is only to show the user his input.
 * Specific components can be skipped by giving it the notInOverview parameter.
 * Pages with no components are skipped.
 */


interface Component extends ExtendedComponentSchema {
  components?: {
    notInOverview: boolean
  }[]
}

const createOverviewPage = (formComponents: ExtendedComponentSchema[]): FormSchema => ({
  type: 'panel',
  title: 'Afronden',
  key: 'xxllnc_overviewPage',
  components:
    formComponents?.filter(c => !c.notInOverview)
      .map((formComponent: Component) => ({
        ...formComponent,
        components: formComponent.components?.filter(c => !c.notInOverview)
          .map((component: Component) => ({
            ...component,
            disabled: true,
            validate: undefined,
            disableMap: true,
            components: component.components?.filter(c => !c.notInOverview)
          })) as ExtendedComponentSchema[]
      }))
      .filter(c => c.components.length > 0)
})

export const addOverView = (form: FormSchema | null): FormSchema => {

  if (!form || Object.keys(form).length === 0)
    throw Error('Er heeft zich een fout voorgedaan tijdens het verwerken van het gevraagde formulier. ' +
      'Neem contact op als het probleem blijft bestaan.')

  const overViewPage = createOverviewPage(form.components || [])
  overViewPage.components.push(confirmationCheckBoxComponent)

  return {
    ...form,
    components: form?.components.concat(overViewPage) || []
  }
}