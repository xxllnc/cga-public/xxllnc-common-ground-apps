import { Component } from 'types/form'
import { checkIfComponentHasRequiredFields } from './checkIfComponentHasRequiredFields'

const page = {
  title: 'Page 1',
  components: [
    {
      label: 'field1',
      validate: {
        required: false
      }
    },
    {
      label: 'field2',
      validate: {
        required: false
      }
    }
  ],
  validate: {
    required: false
  }
}

const page2 = {
  title: 'Page 2',
  components: [
    {
      label: 'field1',
      validate: {
        required: false
      }
    },
    {
      label: 'field2',
      validate: {
        required: true
      }
    }
  ],
  validate: {
    required: false
  }
}

const pageWithColumn = {
  title: 'Page 4',
  components: [
    {
      columns: [
        {
          components: [
            {
              label: 'field',
              validate: { required: true }
            }
          ]
        }
      ],
      validate: {
        required: false
      }
    }
  ]
}

const pageWithTable = {
  title: 'Page 5',
  components: [
    {
      label: 'Table',
      validate: {
        required: false
      },
      rows: [
        [
          {
            components: [
              {
                label: 'Text Field',
                validate: {
                  required: true
                }
              }
            ]
          },
          { components: [] },
          { components: [] }
        ],
        [{ components: [] }, { components: [] }, { components: [] }],
        [{ components: [] }, { components: [] }, { components: [] }]
      ]
    }
  ],
  validate: {
    required: false
  }
}

const afronden = {
  title: 'Afronden',
  components: [
    {
      title: 'Page 1',
      components: [
        {
          label: 'Select Boxes'
        },
        {
          label: 'Veld A'
        }
      ],
      validate: {
        required: false
      }
    },
    {
      title: 'Akkoord',
      components: [
        {
          label: 'checkbox',
          validate: {
            required: true
          },
        }
      ]
    }
  ]
}


describe('checkIfComponentHasRequiredFields test', () => {

  it('should have no error when components is empty', () => {

    //Given
    const result = checkIfComponentHasRequiredFields({})

    expect(result).toBeFalsy()
  })

  test.each`
    pages                             | expectedResult
    ${[page, page, page]}             | ${[false, false, false]}
    ${[page, afronden]}               | ${[false, true]}
    ${[page, page2]}                  | ${[false, true]}
    ${[page, page2, pageWithColumn]}  | ${[false, true, true]}
    ${[page, page2, pageWithTable]}   | ${[false, true, true]}

  `('Should get the expected result for eacht input',
    ({ pages, expectedResult }: { pages: Component[], expectedResult: boolean[] }) => {

      //Given
      const formContent: Component = {
        title: 'the title',
        components: pages
      }
      expect(formContent.components).toBeDefined()
      if (formContent.components) {
        const result = formContent.components.map(component => checkIfComponentHasRequiredFields(component))
        expect(result).toStrictEqual(expectedResult)
      }

    })

})
