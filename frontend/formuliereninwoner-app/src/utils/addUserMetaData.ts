import { FormSchema } from '@tsed/react-formio'
import { ActionCreator } from 'easy-peasy'
import { MetaData } from 'store/authenticationStore'
import { Form, IForm } from 'types/form'
import { addOverView } from './addOverView'

/*
 * A single panel is created with the user meta data from auth source.
 * Each component is disabled as it is only to show the user's personal data.
 */

const createMetaDataPage = (): FormSchema => ({
  type: 'panel',
  title: 'Mijn gegevens',
  key: 'xxllnc_metaDataPage',
  components: [
    {
      disabled: true,
      validate: undefined,
      disableMap: true,
      label: 'Bsn',
      type: 'textfield',
      key: 'digid_bsn',
    },
    {
      disabled: true,
      validate: undefined,
      disableMap: true,
      label: 'Geboortedatum',
      type: 'textfield',
      key: 'digid_geboortedatum',
    },
    {
      disabled: true,
      validate: undefined,
      disableMap: true,
      label: 'Voornamen',
      type: 'textfield',
      key: 'digid_voornamen',
    },
    {
      disabled: true,
      validate: undefined,
      disableMap: true,
      label: 'Achternaam',
      type: 'textfield',
      key: 'digid_geslachtsnaam',
    }
  ]
})

export const addUserMetaDataPage = (form: Form | undefined, setForm: ActionCreator<Form | undefined>) => {
  if (!form?.formContent) return
  const metaDataPage = createMetaDataPage()
  form.formContent.components.unshift(metaDataPage)

  // Redo the overview page since we added a new page
  form.formContent.components.pop()
  setForm({
    ...form,
    formContent: addOverView(form.formContent)
  })
}

export const addUserMetaData = (formRef: { current: IForm | null }, metaData: MetaData) => {
  if (!formRef?.current) return
  if (!metaData.bsn) {
    formRef.current?.setAlert('danger', 'Ophalen persoonsgegevens mislukt. Neem contact op als het probleem blijft bestaan.')
    return
  }

  formRef.current?.getComponent('digid_bsn').setValue(metaData.bsn)
  formRef.current?.getComponent('digid_geboortedatum').setValue(metaData.geboortedatum)
  formRef.current?.getComponent('digid_voornamen').setValue(metaData.naam.voornamen)
  formRef.current?.getComponent('digid_geslachtsnaam').setValue(metaData.naam.geslachtsnaam)

  // TODO: Because of the 'overview page redo' in addUserMetaDataPage values are not displayed
  // in the 'mijn gegevens' page. Changing page forces the data to be displayed. Fix this in the issue below.
  // https://xxllnc.atlassian.net/browse/CGA-4226
  formRef.current?.setPage(1)
  formRef.current?.setPage(0)
}