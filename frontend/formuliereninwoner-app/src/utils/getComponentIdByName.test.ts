import { getComponentIdByName } from './getComponentIdByName'

const page = {
  title: 'Page 1',
  components: [
    {
      id: '1',
      key: 'component1',
      label: 'field1',
      validate: {
        required: false
      }
    },
    {
      id: '2',
      key: 'component2',
      label: 'field2',
      validate: {
        required: false
      }
    }
  ],
  validate: {
    required: false
  }
}

const pageWithTable = {
  title: 'Page 5',
  components: [
    {
      id: '1',
      key: 'component1',
      label: 'Table',
      validate: {
        required: false
      },
      rows: [
        [
          {
            components: [
              {
                id: '2',
                key: 'component2',
                label: 'Text Field',
                validate: {
                  required: true
                }
              }
            ]
          },
          {
            components: [
              {
                id: '3',
                key: 'component3',
                label: 'Table',
                validate: {
                  required: false
                },
                rows: [
                  [
                    {
                      components: [
                        {
                          id: '4',
                          key: 'component4',
                          label: 'Text Field',
                          validate: {
                            required: true
                          }
                        }
                      ]
                    },
                    { components: [] },
                    { components: [] }
                  ],
                  [{ components: [] }, { components: [] }, { components: [] }],
                  [{ components: [] }, { components: [] }, { components: [] }]
                ]
              }
            ]
          },
          { components: [] }
        ],
        [{ components: [] }, { components: [] }, { components: [] }],
        [{ components: [] }, { components: [] }, { components: [] }]
      ]
    }
  ],
  validate: {
    required: false
  }
}

describe('getComponentIdByName test', () => {

  it('should have no error when components is empty', () => {

    //Given
    const result = getComponentIdByName({}, 'component1')

    expect(result).toBeUndefined()
  })

  it('should return the id when the key exists', () => {

    //Given
    const result = getComponentIdByName(page, 'component1')

    expect(result).toBe('1')
  })

  it('should return the id from inside a table', () => {

    //Given
    const result = getComponentIdByName(pageWithTable, 'component2')

    expect(result).toBe('20') // We expect 20, because form.io adds a 0 to ids inside a table
  })

  it('should return the id from inside a child table', () => {

    //Given
    const result = getComponentIdByName(pageWithTable, 'component4')

    expect(result).toBe('40') // We expect 40, because form.io adds a 0 to ids inside a table
  })
})
