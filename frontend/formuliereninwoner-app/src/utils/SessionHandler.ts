let warningAction: () => void
let sessionTimeOut: ReturnType<typeof setTimeout>

const timeOutIn = 15 * 60 * 60 * 1000

const events = [
  'load',
  'mousemove',
  'mousedown',
  'click',
  'scroll',
  'keypress'
]

export const SessionHandler = {
  setSession: (authenticated: boolean, showWarning: () => void): void => {
    if (authenticated) {
      warningAction = showWarning
      events.forEach(item => window.addEventListener(item, SessionHandler.resetTimeout))
      SessionHandler.resetTimeout()
    }
  },
  resetTimeout: (): void => {
    clearTimeout(sessionTimeOut)
    sessionTimeOut = setTimeout(() => SessionHandler.endSession(), timeOutIn)
  },
  endSession: (showWarning = true): void => {
    clearTimeout(sessionTimeOut)
    events.forEach(item => window.removeEventListener(item, SessionHandler.resetTimeout))
    if (showWarning) warningAction()
  }
}
