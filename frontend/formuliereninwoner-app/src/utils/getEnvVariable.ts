declare global {
  interface Window { env: { [key: string]: string } }
}

export const getEnvVariable = (name: string): string =>
  window?.env?.[String(name)] ?? process.env?.[String(name)] ?? ''