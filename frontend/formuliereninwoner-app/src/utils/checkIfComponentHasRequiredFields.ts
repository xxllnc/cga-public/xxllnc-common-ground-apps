import { Component } from 'types/form'

export const checkIfComponentHasRequiredFields = (component: Component): boolean => {

  if (component?.validate?.required)
    return true

  if (Array.isArray(component))
    return !!component.find((subComponent: Component) => checkIfComponentHasRequiredFields(subComponent))

  if (Array.isArray(component?.components))
    return !!component.components.find(subComponent => checkIfComponentHasRequiredFields(subComponent))

  if (Array.isArray(component?.columns))
    return !!component.columns.find(subComponent => checkIfComponentHasRequiredFields(subComponent))

  if (Array.isArray(component?.rows))
    return !!component.rows.find(subComponent => subComponent.find(subSubComponent => checkIfComponentHasRequiredFields(subSubComponent)))

  return false
}