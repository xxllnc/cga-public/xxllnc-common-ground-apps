import { checkResponse, parseResponse } from './checkResponse'
import { createHeader, postOptions } from './postOptions'

interface Props {
  method?: 'GET' | 'POST'
  url: string
  headers?: Headers
}

export const normalFetch = <T>({ method = 'GET', url, headers = new Headers() }: Props): Promise<T> =>
  fetch(url, { method, headers: createHeader({ headers }) })
    .then(response => parseResponse<T>(response))

export const optimisticFetch = <T>({ method = 'GET', url, headers }: Props): Promise<T> =>
  normalFetch<T>({ method, url, headers })
    .catch(_ => ({}) as unknown as T)

export const getCall = <T>(url: string, operation: string): Promise<T> =>
  fetch(`${url}/${operation}`, { headers: createHeader({}) })
    .then(response => checkResponse<T>(response))

export const postCall = async <T>(url: string, data: string, operation: string): Promise<T> =>
  fetch(`${url}/${operation}`, postOptions(data))
    .then(response => checkResponse<T>(response))