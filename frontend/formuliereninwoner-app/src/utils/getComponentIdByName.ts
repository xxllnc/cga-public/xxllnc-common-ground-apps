import { Component } from 'types/form'

const findInArray = (components: Component[], name: string, depth = 0): string | undefined => {
  let id: string | undefined
  components.forEach((subComponent: Component) => {
    const result = getComponentIdByName(subComponent, name, depth)
    if (result) id = result
  })
  return id
}

export const getComponentIdByName = (component: Component, name: string, depth = 0): string | undefined => {
  if (component?.key === name) return component.id

  if (Array.isArray(component)) return findInArray(component, name, depth)
  if (Array.isArray(component?.components)) return findInArray(component.components, name, depth)
  if (Array.isArray(component?.columns)) return findInArray(component.columns, name, depth)

  if (Array.isArray(component?.rows)) {
    let id: string | undefined
    component.rows.forEach(row => {
      const result = findInArray(row, name, depth +1)
      if (result) id = result
    })
    // We need to add a zero if we are in a table because Form.io adds a zero after a id in components inside a table.
    // We add the depth check to avoid adding multiple zero's if there is a table inside a table.
    const idPostfix = depth === 0 ? '0' : ''
    return id ? `${id}${idPostfix}` : undefined
  }

  return undefined
}