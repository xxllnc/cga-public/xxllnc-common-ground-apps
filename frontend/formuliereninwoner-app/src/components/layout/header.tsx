import { FC } from 'react'
import logo from '../../assets/img/zaakstad-logo.svg'
import { FormRefProps } from '../../types/form'

export interface HeaderProps extends FormRefProps {
  title: string
  currentPage: number
  progressBar: boolean
}

export const getProgress = ({ formRef, currentPage }: Partial<HeaderProps>): number => {
  if (!formRef?.allPages?.length || currentPage === undefined)
    return 0
  const total = formRef.allPages.length
  return total < currentPage + 1 ? 100 : (100 * (currentPage + 1)) / total
}

export const Header: FC<Partial<HeaderProps>> = ({
  title = '',
  currentPage,
  formRef,
  progressBar
}) => (
  <header>
    <div className="header-content">
      <div className="row">
        <div className="header-logo xs-12 md-3 lg-3 xl-3">
          <a className="logo" href="http://www.zaakstad.nl">
            <img src={logo} alt='Link naar homepage van zaakstad' />
          </a>
        </div>
        <div className="header-title xs-12 md-7 lg-7 xl-7">
          <h1 className="title">{title}</h1>
        </div>
      </div>
    </div>
    {progressBar &&
        <div className="header-progress-bar">
          <div className="header-progress-bar-progress"
            style={{ width: `${getProgress({ formRef, currentPage })}%` }}
          />
        </div>
    }
  </header>
)