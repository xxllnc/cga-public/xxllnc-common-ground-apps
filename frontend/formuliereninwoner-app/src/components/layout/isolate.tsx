import { Component, PropsWithChildren } from 'react'

export class Isolate extends Component<PropsWithChildren<unknown>> {
  shouldComponentUpdate(): boolean {
    return false
  }

  render(): JSX.Element {
    return <>
      {
        this.props.children
      }
    </>
  }
}
