import { Footer, Header } from 'components'
import { IForm } from 'types/form'

interface PageProps {
  children?: React.ReactNode
  className?: string
  title?: string
  currentPage?: number
  formRef?: IForm | null
  progressBar?: boolean
}

export const Page = ({
  children,
  className,
  title,
  currentPage,
  formRef,
  progressBar = true
}: PageProps): JSX.Element => (
  <>
    <Header
      title={title}
      currentPage={currentPage}
      formRef={formRef}
      progressBar={progressBar}
    />
    <main className={className}>
      {children}
    </main>
    <Footer />
  </>
)

