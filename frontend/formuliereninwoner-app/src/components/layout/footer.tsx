import { useState } from 'react'
import { LicenseModal } from 'components'
import logo from 'assets/img/zaakstad-logo.svg'
import phoneIcon from 'assets/img/phone.svg'
import envelopeIcon from 'assets/img/envelope.svg'
import licensesIcon from 'assets/img/licenses.svg'
import licenses from '../../licenses.json'

export const Footer = (): JSX.Element => {
  const [showLicenses, setShowLicenses] = useState<boolean>(false)
  return (
    <footer>
      <div className="footer-inner">
        <div className="footer-logo">
          <a href="#">
            <img src={logo} alt="Logo van zaakstad" />
          </a>
        </div>
        <a className="footer-contact-icon" href="tel:+31-074-259-40-08">
          <img src={phoneIcon} alt="telefoonnummer: +31-074-259-40-08" />
        </a>
        <hr className="spacer" />
        <a className="footer-contact-icon" href="mailto: sales@exxellence.nl">
          <img src={envelopeIcon} alt="e-mail: sales@exxellence.nl" />
        </a>
        <hr className="spacer" />
        <a data-testid="display_licenses" className="footer-contact-icon" onClick={() => setShowLicenses(true)}>
          <img src={licensesIcon} alt="licenses" />
        </a>
        <LicenseModal show={showLicenses} onClose={() => setShowLicenses(false)} licenses={licenses} />
      </div>
    </footer>
  )
}

