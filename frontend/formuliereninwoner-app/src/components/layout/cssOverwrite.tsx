import { getPublicSettingsUrl } from 'AppUrls'
import { FC } from 'react'
import { Helmet } from 'react-helmet'

// The CssOverwrite pulls the css file from the settings api
// The css can be created from the 'formulierenbeheer' app's general / css overwrite page

export const CssOverwrite: FC = () => (
  <Helmet>
    <link rel="stylesheet" href={`${getPublicSettingsUrl()}/settings/css?orgUrl=${window.location.origin}`} />
  </Helmet>
)
