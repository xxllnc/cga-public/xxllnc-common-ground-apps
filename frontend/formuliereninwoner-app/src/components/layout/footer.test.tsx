import { render, screen, waitFor, fireEvent } from '@testing-library/react'
import { Footer } from 'components'

describe('Footer test', () => {

  it('should render the footer', () => {

    // Given
    render(<Footer/>)

    // expect the footer to display the phone number and email
    expect(screen.getByAltText('telefoonnummer: +31-074-259-40-08')).toBeDefined()
    expect(screen.getByAltText('e-mail: sales@exxellence.nl')).toBeDefined()
  })

  it('should display the licenses', async () => {

    // Given
    render(<Footer/>)

    // expect the licenses to be displayed when the icon is clicked
    await waitFor(() => fireEvent.click(screen.getByTestId('display_licenses')))
    expect(screen.getByText('Licentie informatie')).toBeDefined()
  })

  it('should hide the licenses info when hide button is pressed', async () => {

    // Given
    render(<Footer/>)

    // expect the licenses to be hidden when the hide licenses button is pressed
    await waitFor(() => fireEvent.click(screen.getByTestId('display_licenses')))
    expect(screen.getByText('Licentie informatie')).toBeDefined()

    await waitFor(() => fireEvent.click(screen.getByTestId('hide_licenses')))
    const licenseInfoTitle = screen.queryByText('Licentie informatie')
    expect(licenseInfoTitle).toBeNull()
  })

  it('should show details when a license is pressed', async () => {

    // Given
    render(<Footer/>)

    // expect the license details to be displayed when a specific license is clicked
    await waitFor(() => fireEvent.click(screen.getByTestId('display_licenses')))
    expect(screen.getByText('Licentie informatie')).toBeDefined()

    await waitFor(() => fireEvent.click(screen.getAllByTestId('show_license_details')[0]))
    expect(screen.getByText('License')).toBeDefined()
  })
})