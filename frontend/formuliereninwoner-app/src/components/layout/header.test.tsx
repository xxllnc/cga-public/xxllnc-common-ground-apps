import { render, screen } from '@testing-library/react'
import { IForm } from 'types/form'
import { Header, getProgress } from './header'
import { store } from 'store'
import { StoreProvider } from 'easy-peasy'

const title = 'Form title'
const page = {
  checkValidity: (_data) => true,
  component: { title: 'component 1' },
  data: 'unknown'
}
const formRef: IForm = {
  setPage: (_val) => null,
  allPages: [page],
  setAlert: (_val) => null,
  checkValidity: () => true,
  getComponent: (_val) => ({ setValue: (_val2) => null }),
  triggerRedraw: () => null,
  submission: { data: {}}
}

describe('Header test', () => {

  it('should render the empty Header', () => {

    //Given
    render(
      // https://github.com/ctrlplusb/easy-peasy/issues/741
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      <StoreProvider store={store}>
        <Header />
      </StoreProvider>
    )

    // We expect an empty header
    expect(screen.findAllByText('Form title')).toMatchObject({})
  })

  it('should render the Header with only the Title', () => {

    render(
      // https://github.com/ctrlplusb/easy-peasy/issues/741
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      <StoreProvider store={store}>
        <Header title={title} />
      </StoreProvider>
    )

    // We expect an empty header
    expect(screen.getByText(title)).toBeDefined()
  })

  it('should render the Header with Title and currentPage', () => {

    //Given
    render(
      // https://github.com/ctrlplusb/easy-peasy/issues/741
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      <StoreProvider store={store}>
        <Header title={title} currentPage={1} />
      </StoreProvider>
    )

    // We expect an empty header
    expect(screen.getByText(title)).toBeDefined()
  })

  it('should render the Header with Title, currentPage and formRef', () => {

    //Given
    render(
      // https://github.com/ctrlplusb/easy-peasy/issues/741
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      <StoreProvider store={store}>
        <Header title={title} currentPage={0} formRef={formRef} progressBar={true} />
      </StoreProvider>
    )

    // We expect an empty header
    expect(screen.getByText(title)).toBeDefined()
  })
})

describe('test getProgress', () => {

  it('should calculate the correct progress', () => {


    expect(getProgress({})).toEqual(0)
    expect(getProgress({ formRef })).toEqual(0)
    expect(getProgress({ formRef: { ...formRef, allPages: [] }, currentPage: 0 })).toEqual(0)

    expect(getProgress({ formRef, currentPage: 0 })).toEqual(100)

    const withTwoPages: IForm = { ...formRef, allPages: [page, page] }
    expect(getProgress({ formRef: withTwoPages, currentPage: 0 })).toEqual(50)
    expect(getProgress({ formRef: withTwoPages, currentPage: 1 })).toEqual(100)
    expect(getProgress({ formRef: withTwoPages, currentPage: 2 })).toEqual(100)

    const withThreePages: IForm = { ...formRef, allPages: [page, page, page] }
    expect(getProgress({ formRef: withThreePages, currentPage: 0 })).toEqual(100 / 3)
    expect(getProgress({ formRef: withThreePages, currentPage: 1 })).toEqual((100 / 3) * 2)
    expect(getProgress({ formRef: withThreePages, currentPage: 2 })).toEqual(100)
    expect(getProgress({ formRef: withThreePages, currentPage: 3 })).toEqual(100)
  })

})
