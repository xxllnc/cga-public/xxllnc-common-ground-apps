/* eslint-disable security/detect-object-injection */
import { FC } from 'react'
import { Page } from '../../types/form'

interface Props {
  updatePage: (newValue: number) => void
  activePageIndex: number
  highestVisitedPageIndex: number
  allPages: Page[]
  isSubmitting: boolean
}

export interface GetStateOfButton extends Pick<Props, 'activePageIndex' | 'highestVisitedPageIndex'> {
  currentPage: Page
  index: number
  oneInvalidPage: boolean
}

export type StateOfButton = 'isActive' | 'isInactive' | 'isDone' | 'isInvalid'

export const getStateOfButton = ({
  activePageIndex,
  highestVisitedPageIndex,
  currentPage,
  index,
}: GetStateOfButton): StateOfButton => {

  if (activePageIndex === index)
    return 'isActive'

  if (index <= highestVisitedPageIndex) {
    const inputIsValid = currentPage?.checkValidity(currentPage.data)
    return inputIsValid ? 'isDone' : 'isInvalid'
  }

  return 'isInactive'
}

const buttonClassAndIcon: { [state in StateOfButton]: { className: string, icon: string } } = {
  isActive: { className: 'step-active', icon: 'arrow_forward' },
  isDone: { className: 'step-done', icon: 'check' },
  isInvalid: { className: 'step-invalid', icon: 'error' },
  isInactive: { className: 'step-inactive', icon: 'radio_button_unchecked' },
}

export const Sidebar: FC<Props> = ({
  updatePage,
  activePageIndex,
  highestVisitedPageIndex,
  allPages,
  isSubmitting
}) => {
  let oneInvalidPage = false

  const pageClickHandler = (index: number, state: string) => {
    if (state === 'isInactive' || isSubmitting) return
    updatePage(index)
  }

  return (
    <nav
      id="navigation"
      className="lg-4 xl-3"
      aria-label="Navigatie"
    >
      <ol className="steps">
        {
          allPages.map((currentPage, index) => {
            const state = getStateOfButton({ activePageIndex, highestVisitedPageIndex, currentPage, index, oneInvalidPage })
            const title = currentPage.component?.title ?? ''
            const extraLiOptions = state === 'isActive' ? { 'aria-current': 'step' as const} : {}
            if (state === 'isInvalid')
              oneInvalidPage = true
            return (
              <li
                key={`page_${index}`}
                className={`step ${buttonClassAndIcon[state].className}`}
                { ...extraLiOptions }
              >
                <span className="material-icons step-icon" aria-hidden="true">{buttonClassAndIcon[state].icon}</span>
                <span className='sr-only'>{ state === 'isDone' ? 'Afgerond' : 'Nog niet afgerond'}</span>
                <button
                  aria-label={`${title} knop. Ga naar pagina ${title}`}
                  onClick={() => pageClickHandler(index, state)}
                  aria-disabled={state === 'isInactive' || isSubmitting}
                >
                  {title}
                </button>
              </li>
            )
          })
        }
      </ol>
    </nav>
  )
}