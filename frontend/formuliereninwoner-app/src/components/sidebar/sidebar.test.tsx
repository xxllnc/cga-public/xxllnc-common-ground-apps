import { render, screen } from '@testing-library/react'
import { IForm, Page } from 'types/form'
import { GetStateOfButton, getStateOfButton, Sidebar, StateOfButton } from 'components'


const page0: Page = {
  checkValidity: (_data) => true,
  component: { title: 'page 0' },
  data: 'unknown'
}
const page1: Page = {
  checkValidity: (_data) => true,
  component: { title: 'page 1' },
  data: 'unknown'
}
const page2: Page = {
  checkValidity: (_data) => false,
  component: { title: 'page 2' },
  data: 'unknown'
}
const page3: Page = {
  checkValidity: (_data) => true,
  component: { title: 'page 3' },
  data: 'unknown'
}
const formRef: IForm = {
  setPage: (_val) => { console.log('') },
  allPages: [page0, page1, page2, page3],
  setAlert: (_val) => { console.log('') },
  checkValidity: (_val) => false,
  getComponent: (_val) => ({ setValue: (_val2) => { console.log('') } }),
  triggerRedraw: () => { console.log('') },
  submission: { data: {}}
}

describe('Sidebar test', () => {

  it('should render the SideBar for the first page', () => {

    const updatePage = jest.fn()
    //Given
    render(<Sidebar
      updatePage={updatePage}
      activePageIndex={0}
      highestVisitedPageIndex={0}
      allPages={formRef.allPages}
      isSubmitting={false}
    />)

    // expect the pages in the sidebar
    expect(screen.getByText('page 0')).toBeDefined()
    expect(screen.getByText('page 1')).toBeDefined()
    expect(screen.getByText('page 2')).toBeDefined()
    expect(screen.getByText('page 3')).toBeDefined()

    // expect one arrow icon and three disabled icons
    expect(screen.getAllByText('arrow_forward').length).toBe(1)
    expect(screen.getAllByText('radio_button_unchecked').length).toBe(3)


  })

  it('should render the SideBar when the second page is active', () => {

    const updatePage = jest.fn()
    //Given
    render(<Sidebar
      updatePage={updatePage}
      activePageIndex={1}
      highestVisitedPageIndex={0}
      allPages={formRef.allPages}
      isSubmitting={false}
    />)

    // expect the pages in the sidebar
    expect(screen.getByText('page 0')).toBeDefined()
    expect(screen.getByText('page 1')).toBeDefined()
    expect(screen.getByText('page 2')).toBeDefined()
    expect(screen.getByText('page 3')).toBeDefined()

    // expect a check ico, an arrow and three disabled icons
    expect(screen.getAllByText('arrow_forward').length).toBe(1)
    expect(screen.getAllByText('check').length).toBe(1)
    expect(screen.getAllByText('radio_button_unchecked').length).toBe(2)

  })
})

describe('test getStateOfButton', () => {

  test.each`
    activePageIndex | highestVisitedPageIndex | currentPage | index | expectedResult
    ${0}            | ${0}                    | ${page0}    | ${0}  | ${'isActive'}
    ${1}            | ${0}                    | ${page0}    | ${0}  | ${'isDone'}
    ${0}            | ${0}                    | ${page1}    | ${1}  | ${'isInactive'}
    ${2}            | ${1}                    | ${page2}    | ${2}  | ${'isActive'}
    ${3}            | ${2}                    | ${page2}    | ${2}  | ${'isInvalid'}
    ${3}            | ${2}                    | ${page3}    | ${3}  | ${'isActive'}
  `('Should get the expected result for activePageIndex: $activePageIndex with highestVisitedPageIndex: $highestVisitedPageIndex',
    ({ activePageIndex, highestVisitedPageIndex, currentPage, index, expectedResult }:
      GetStateOfButton & { expectedResult: StateOfButton }) => {

      const result = getStateOfButton({ activePageIndex, highestVisitedPageIndex, currentPage, index, oneInvalidPage: false })
      expect(result).toBe(expectedResult)

    })

})
