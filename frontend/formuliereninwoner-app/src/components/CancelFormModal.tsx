import { Modal } from 'bootstrap'
import { FC, useEffect, useRef, useState } from 'react'

export interface Props {
  cancelForm: boolean
  setCancelForm: (value: React.SetStateAction<boolean>) => void
  onCancel: () => void
}

export const CancelFormModal: FC<Props> = ({ cancelForm, setCancelForm, onCancel }) => {

  const modalRef = useRef<HTMLDivElement>(null)
  const [modal, setmodal] = useState<Modal | null>(null)

  useEffect(() => {
    if (modalRef.current)
      setmodal(new Modal(modalRef.current, { backdrop: 'static', keyboard: false }))
  }, [modalRef])

  useEffect(() => {
    if (!modal)
      return
    cancelForm ? modal.show() : modal.hide()
  }, [cancelForm])

  const clickOnClose = () => { setCancelForm(false) }

  return (
    <div className="modal fade" ref={modalRef} tabIndex={-1} >
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="staticBackdropLabel">Annuleren?</h5>
            <button type="button" className="btn-close" onClick={clickOnClose} aria-label="Sluiten"></button>
          </div>
          <div className="modal-body">
            <p>Alle ingevulde gegevens worden gewist.</p>
            <p>Weet u het zeker dat u wilt stoppen?</p>
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-secondary" onClick={clickOnClose}>Nee</button>
            <button type="button" className="btn btn-primary" onClick={onCancel}>Ja</button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CancelFormModal
