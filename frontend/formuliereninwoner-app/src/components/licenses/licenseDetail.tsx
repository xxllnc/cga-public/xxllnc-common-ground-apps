import '../../styles/license.scss'
import { useState } from 'react'
import { GetLicenseForTypeWithUsername } from './GetLicenseForTypeWithUsername'

export interface DetailsOfLicense {
    licenses: string
    repository?: string
    licenseUrl?: string
    publisher?: string
  }

  interface DetailsProps {
    packageName: string
    packageDetails: DetailsOfLicense
  }

const ExtractNameFromGithubUrl = (url?: string): string | null => {
  if (!url) return null
  // eslint-disable-next-line security/detect-unsafe-regex
  const reg = /((https?:\/\/)?(www\.)?github\.com\/)?(@|#!\/)?([A-Za-z0-9_]{1,15})(\/([-a-z]{1,20}))?/i
  const components = reg.exec(url)
  return (components && components.length > 5) ? components[5] : null
}

const Detail = ({packageName, packageDetails}: DetailsProps): JSX.Element => {
  const [showDetails, setShowDetails] = useState(false)

  const rawName = packageName.startsWith('@') ? packageName.substr(1) : packageName
  const [name, version] = rawName.split('@')
  const username = ExtractNameFromGithubUrl(packageDetails.repository) ||
    ExtractNameFromGithubUrl(packageDetails.licenseUrl) || name

  return (
    <div className='wrapper'>
      <button data-testid='show_license_details' onClick={() => setShowDetails(!showDetails)}>
        <div className="grid">
          <div className="gridItem">
            <img alt="" role="presentation"
              src={`https://github.com/${username}.png`} />
          </div>
          <div className="gridItem">
            <h5>{name}</h5>
            <p>Version: {version}, License: {packageDetails.licenses}</p>
          </div>
        </div>
      </button>
      {showDetails &&
        <div>
          <article
            role={'presentation'}
            className={'noteDetail'}
            key={'article'}
          >
            <section>
              <header>
                <h4>License</h4>
              </header>
              {GetLicenseForTypeWithUsername(
                { username, licenses: packageDetails.licenses }).split('\n')
                .map((item, i) => <p key={i}>{item}</p>)}
            </section>
          </article>
        </div>
      }
    </div>
  )
}

export default Detail