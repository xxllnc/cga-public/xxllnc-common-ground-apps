import { FC } from 'react'
import '../../styles/modal.scss'
import License, { DetailsOfLicense } from './licenseDetail'

interface LicenseModalProps {
    show: boolean
    onClose(): void
    licenses: { [key: string]: DetailsOfLicense }
}

export const LicenseModal: FC<LicenseModalProps> = ({show, onClose, licenses}) => show ? (
  <div className="modal" style={{ display: 'block' }}>
    <div className="ccard">
      <header className="header">
        <h2>Licentie informatie</h2>
        <span
          data-testid="hide_licenses"
          onClick={() => onClose()}
          className="cbutton">
          &times;
        </span>
      </header>
      <div className="ccontent">
        {Object.keys(licenses).map((key) =>
          <License key={key} packageName={key} packageDetails={licenses[String(key)]}/>
        )}
      </div>
    </div>
  </div>
) : null
