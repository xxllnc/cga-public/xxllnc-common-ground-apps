import { FC } from 'react'
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core'

interface Props {
  shouldDisplayWarning: boolean
  extendSession: () => void
}

export const SessionExpiredWarning: FC<Props> = ({ shouldDisplayWarning, extendSession }) => <Dialog
  open={shouldDisplayWarning}
  onClose={extendSession}
  aria-labelledby="alert-session-title"
  aria-describedby="alert-session-description"
>
  <DialogTitle id="alert-session-title">
        Uw session verloopt bijna
  </DialogTitle>
  <DialogContent>
    <DialogContentText id="alert-session-description">
          Druk op de knop om de sessie te verlengen.
    </DialogContentText>
  </DialogContent>
  <DialogActions>
    <Button onClick={extendSession}>
          Sessie verlengen
    </Button>
  </DialogActions>
</Dialog>
