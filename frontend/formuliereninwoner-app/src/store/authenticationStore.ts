import { User } from '@auth0/auth0-spa-js'
import { Action, action } from 'easy-peasy'

export interface MetaData {
  bsn: string
  geboortedatum: string
  naam: {
    voornamen: string
    geslachtsnaam: string
    aanschrijfwijze: string
  }
}

export interface Authentication {
  shouldLogin: boolean
  user: User | undefined
  metaData: MetaData | undefined
  setUser: Action<Authentication, User | undefined>
  setShouldLogin: Action<Authentication>
}

export const AuthenticationStore: Authentication = {
  shouldLogin: false,
  user: undefined,
  metaData: undefined,
  setUser: action((state, user) => {
    state.user = user
    state.metaData = user?.['https://xxllnc.nl/metadata'] as MetaData
  }),
  setShouldLogin: action((state) => {
    state.shouldLogin = true
  }),
}