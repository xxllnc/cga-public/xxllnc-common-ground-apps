import { createStore, createTypedHooks } from 'easy-peasy'
import { Authentication, AuthenticationStore } from './authenticationStore'
import { FormData, FormStore } from './formStore'
import { Settings, SettingsStore } from './settingsStore'

const isDev = process.env.NODE_ENV === 'development'

export interface StoreModel {
  authentication: Authentication
  settings: Settings
  formData: FormData
}

export const store = createStore<StoreModel>(
  {
    authentication: AuthenticationStore,
    settings: SettingsStore,
    formData: FormStore
  },
  {
    devTools: isDev,
    name: 'Formuliereninwoner'
  }
)

const typedHooks = createTypedHooks<StoreModel>()

export const useStoreActions = typedHooks.useStoreActions
export const useStoreDispatch = typedHooks.useStoreDispatch
export const useStoreState = typedHooks.useStoreState