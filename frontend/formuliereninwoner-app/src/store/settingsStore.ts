import { getPublicSettingsUrl } from 'AppUrls'
import { Action, action, Thunk, thunk } from 'easy-peasy'
import { PublicSetting } from 'types/generatedSettingTypes'
import { getCall } from 'utils/HttpClient'
import { parseData } from 'xxllnc-formio-components'

export interface GeneralSettings {
  [name: string]: PublicSetting
}

export interface Settings {
  init: Thunk<Settings>
  generalSettings: GeneralSettings | null
  loadSettings: Action<Settings, GeneralSettings>
  clearSettings: Action<Settings>
}

const storageName = 'generalSettings'
const fetchGeneralSettings = async (): Promise<GeneralSettings> => {

  if (process.env?.NODE_ENV === 'test')
    return Promise.resolve({})

  const settingsFromStorage = sessionStorage.getItem(storageName)
  if (settingsFromStorage)
    return Promise.resolve(parseData<GeneralSettings>(settingsFromStorage) ?? {})

  const fetchedSettings = await getCall<PublicSetting[]>(getPublicSettingsUrl(), 'settings')
    .catch(() => []) as PublicSetting[]

  return Promise.resolve(
    fetchedSettings.reduce((prev, cur): GeneralSettings => {
      prev[cur.name] = cur
      return prev
    }, {}))
}

export const SettingsStore: Settings = {
  init: thunk(async (actions) => {
    const response = await fetchGeneralSettings()
    actions.loadSettings(response)
  }),
  generalSettings: null,
  loadSettings: action((state, payload) => {
    state.generalSettings = payload
    sessionStorage.setItem(storageName, JSON.stringify(payload))
  }),
  clearSettings: action((state) => {
    state.generalSettings = null
    sessionStorage.removeItem(storageName)
  })
}
