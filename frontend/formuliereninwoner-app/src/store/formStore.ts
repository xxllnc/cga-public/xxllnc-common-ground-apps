import { Action, action } from 'easy-peasy'
import { Form } from 'types/form'

export interface FormData {
  form: Form | undefined
  setForm: Action<FormData, Form | undefined>
}

export const FormStore: FormData = {
  form: undefined,
  setForm: action((state, form) => {
    state.form = form
  })
}