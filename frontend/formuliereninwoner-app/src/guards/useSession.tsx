import { Button, Grid, makeStyles, createStyles, Typography } from '@material-ui/core'
import ExitToAppIcon from '@material-ui/icons/ExitToApp'
import { SessionExpiredWarning } from 'components'
import { FC, useEffect, useState } from 'react'
import { getEnvVariable, SessionHandler } from 'utils'
import { Auth0Client, AuthenticationError } from '@auth0/auth0-spa-js'
import { useStoreActions, useStoreState } from 'store'
import { GeneralSettings } from 'store/settingsStore'

const digIdLoginUrl = () => getEnvVariable('REACT_APP_DIGID_URL')
const domain = () => getEnvVariable('REACT_APP_AUTH0_DOMAIN')
const clientId = () => getEnvVariable('REACT_APP_AUTH0_CLIENT_ID')

let warningTimeOut: ReturnType<typeof setTimeout> | null
let auth0SpaJs: Auth0Client

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      position: 'absolute',
      top: 10,
      right: 10,
      zIndex: 99
    },
  }),
)

interface SessionProps {
  config: null | { [key: string]: string } | undefined
  generalSettings: GeneralSettings | null
}

interface SessionResponseProps {
  isAuthenticated: boolean
  UserSession: FC
  isReady: boolean
}

const useSession = ({ config, generalSettings }: SessionProps): SessionResponseProps => {
  const [isReady, setIsReady] = useState(false)
  const [shouldDisplayWarning, setShouldDisplayWarning] = useState(false)

  const setUser = useStoreActions(actions => actions.authentication.setUser)
  const { user, metaData } = useStoreState((state) => ({
    user: state.authentication.user,
    metaData: state.authentication.metaData
  }))

  const organization = config?.organization || generalSettings?.organization?.value

  const Header: FC = () => {
    const classes = useStyles()

    return <div className={classes.root}>
      { isReady && !!user && (
        <Grid container justify="flex-end" spacing={2}>
          <Grid item>
            <Typography style={{ marginTop: '7px'}}>{ metaData?.naam?.aanschrijfwijze || '' }</Typography>
          </Grid>
          <Grid item>
            <Button
              onClick={() => void logout()}
              aria-label="logout current user"
              variant="outlined"
              startIcon={<ExitToAppIcon />}
            >
              uitloggen
            </Button>
          </Grid>
        </Grid>
      ) }
      { isReady && !user && organization && (
        <Grid container justify="flex-end" spacing={2}>
          <Grid item>
            <Button
              onClick={login}
              aria-label="login with DigId"
              variant="outlined"
              startIcon={<ExitToAppIcon />}
            >
              inloggen
            </Button>
          </Grid>
        </Grid>
      )}
      <SessionExpiredWarning
        shouldDisplayWarning={shouldDisplayWarning}
        extendSession={extendSession}
      />
    </div>
  }

  const extendSession = () => {
    if (warningTimeOut) clearTimeout(warningTimeOut)
    warningTimeOut = null
    SessionHandler.setSession(!!user, enableWarning)
    setShouldDisplayWarning(false)
  }

  const enableWarning = () => {
    warningTimeOut = setTimeout(() => void logout(), 10 * 1000)
    setShouldDisplayWarning(true)
  }

  const logout = async () => {
    setShouldDisplayWarning(false)
    setIsReady(false)
    SessionHandler.endSession(false)

    await auth0SpaJs.logout().catch((error: Error) => console.log({ error }))
  }

  const login = () => {
    if (typeof window !== 'undefined') {
      // eslint-disable-next-line max-len
      window.location.href = `${digIdLoginUrl()}?client_id=${clientId()}&redirect_uri=${window.location.href}&organization=${organization || ''}`
    }
  }

  useEffect(() => {
    SessionHandler.setSession(!!user, enableWarning)
  }, [])

  useEffect(() => {
    if (organization && !auth0SpaJs) {
      auth0SpaJs = new Auth0Client({
        domain: domain(),
        clientId: clientId(),
        authorizationParams: {
          // eslint-disable-next-line camelcase
          redirect_uri: window.location.href || '',
          organization
        }
      })

      const getToken = async () => {
        try {
          await auth0SpaJs.getTokenSilently()
          const userResponse = await auth0SpaJs.getUser()
          setUser(userResponse)
        } catch (error) {
          if ((error as AuthenticationError).error === 'login_required' && config?.shouldLogin === 'true') login()
        } finally {
          setIsReady(true)
        }
      }

      getToken().catch((error: Error) => console.log({ error }))
    } else if (config !== undefined) {
      setIsReady(true)
    }
  }, [config])

  return {
    isAuthenticated: !!user,
    UserSession: Header,
    isReady
  }
}

export default useSession