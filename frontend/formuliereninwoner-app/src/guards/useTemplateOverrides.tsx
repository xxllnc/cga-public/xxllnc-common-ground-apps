/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/restrict-template-expressions */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { useEffect } from 'react'
import { Templates } from '@tsed/react-formio'
import { errorList } from 'templateOverrides'
import { Component } from 'types/form'

export interface ContextProps {
  id: string
  component: Component
  errors: {
    message: string
    keyOrPath: string
  }[]
  t: (text: string) => string
}

export const useTemplateOverrides = () => {

  useEffect(() => {
    // Get the original template(s)
    const label = Templates.current.label.form
    const input = Templates.current.input.form

    // Override templates
    Templates.current = {
      // Use the original template if you only want to wrap additional content arround the original template.
      // In this case we wrap a anchor arround the label to be able to jump to the input label from the error list.
      'label': {
        form: (ctx: ContextProps) =>  `<a id="a-${ctx.id}">${label(ctx)}</a>`
      },
      // Or create your own template to customize it completely.
      // In this case we want to change the warning title to a h5 and wrap each list item in a hyperlink to the anchor.
      'errorsList': {
        form: (ctx: ContextProps) => errorList(ctx)
      },
      // Some components share templates. You can specify the type to avoid overriding the template for all components.
      // The number component shares the generic 'input' component for instance. Use "<templateName>-<componentType>" to override it.
      'input-number': {
        form: (ctx: ContextProps) =>  `${input(ctx)}`
      }
    }
  }, [])

  // More information about templating in form.io can be found here
  // https://help.form.io/developers/form-templates
}
