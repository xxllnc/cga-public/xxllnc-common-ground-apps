import { ContextProps } from 'guards'
import { getComponentIdByName } from 'utils'

export const errorList = ({ errors, t, component }: ContextProps): string => {
  const list = errors.map((err) => {
    const id = getComponentIdByName(component, err.keyOrPath)
    return `<li><a href="#a-${id || ''}" tabIndex="0" role="link">${err.message}</a></li>`
  }).join('')

  return `<h5>${t('error')}</h5><ul>${list}</ul>`
}