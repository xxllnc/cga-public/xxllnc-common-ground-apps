export const getActiveFormsUrl = (): string => window.location.origin + '/formulierenbeheer/api/v1/active/forms'
export const getPublicSettingsUrl = (): string => window.location.origin + '/instellingen/api/v1/public'
export const getRequestsUrl = (): string => window.location.origin + '/verzoeken/api/v1'
export const getPersonsUrl = (): string => window.location.origin + '/persons/api/v1'
