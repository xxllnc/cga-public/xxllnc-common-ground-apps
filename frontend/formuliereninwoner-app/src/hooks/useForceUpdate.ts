import { useState } from 'react'

export const useForceUpdate = (): () => void => {
  const [_, setValue] = useState(0)

  // By increasing the state of this component by one we force the component were we use this hook to rerender
  const update = () => setValue(value => value + 1)

  return update
}
