import { FormSchema, Submission } from '@tsed/react-formio'
import { getActiveFormsUrl, getRequestsUrl } from 'AppUrls'
import { mockedForm } from 'mocks'
import { useEffect, useState } from 'react'
import { useStoreActions, useStoreState } from 'store'
import { addOverView } from 'utils'
import { RawForm } from '../types/form'
import { getCall, postCall } from '../utils/HttpClient'

interface ReturnProps {
  error?: string
  isLoading: boolean
  isSubmitting: boolean
  isSubmitDone: boolean
  submitForm: (submission: Submission<FormSchema>) => Promise<any>
}

export const useFormData = (formId: string | null): ReturnProps => {
  const formUrl = getActiveFormsUrl()
  const requestUrl = getRequestsUrl()

  const setForm = useStoreActions(actions => actions.formData.setForm)
  const form = useStoreState((state) => state.formData.form)
  const [error, setError] = useState<string>()
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [isSubmitting, setIsSubmitting] = useState<boolean>(false)
  const [isSubmitDone, setIsSubmitDone] = useState<boolean>(false)

  useEffect(() => {
    if (process.env?.NODE_ENV === 'test') {
      setForm({
        ...mockedForm,
        formContent: addOverView(mockedForm.formContent)
      })
    } else if (formId) {
      setIsLoading(true)
      getCall<RawForm>(formUrl, formId)
        .then(response => {
          const config: { [key: string]: string } = {}
          response?.config?.forEach(setting => {
            config[setting.key] = setting.value
          })

          setForm({
            ...response,
            config,
            formContent: addOverView(response.form)
          })
        })
        .catch((e: Error) => setError(e.message))
        .finally(() => { setIsLoading(false) })
    }
  }, [formId])


  const submitForm = (submission: Submission<FormSchema>) => {

    setIsSubmitting(true)
    setError(undefined)
    const stringifiedForm = JSON.stringify({
      formUuid: form?.id,
      status: form?.status,
      formInput: (submission.data as { [key: string]: unknown })
    })

    return postCall(requestUrl, stringifiedForm, 'requests')
      .then(response => {
        setIsSubmitDone(true)
        return response
      })
      .catch((e: Error) => setError(e.message))
      .finally(() => setIsSubmitting(false))
  }

  return {
    error,
    isLoading,
    isSubmitting,
    isSubmitDone,
    submitForm,
  }
}
