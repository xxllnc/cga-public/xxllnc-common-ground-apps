import { useEffect } from 'react'
import { IForm } from 'types/form'

export const useCallbackData = (formRef: { current: IForm | null }): void => {
  const queryParams = new URLSearchParams(window.location.search)

  useEffect(() => {
    if (!formRef?.current) return
    const callback = queryParams.get('callback')

    /*
    * These callbacks are for demo purpase and should be replaced.
    */
    if (callback === 'payment_success') formRef.current.setAlert('info', 'Redirected after completed payment')
    if (callback === 'payment_failed') formRef.current.setAlert('warning', 'Redirected after payment failed')
  }, [queryParams])

}