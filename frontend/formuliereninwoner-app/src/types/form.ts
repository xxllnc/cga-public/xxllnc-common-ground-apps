import { FormSchema } from '@tsed/react-formio'
import { FormWithDetails as GeneratedForm } from './generatedFormTypes'

export interface FormRefProps {
  formRef: IForm | null
}

export interface Component {
  id?: string
  key?: string
  title?: string
  label?: string
  validate?: { required: boolean }
  components?: Component[]
  columns?: Component[]
  rows?: { components: Component[] }[][]
}

export interface Page {
  checkValidity: (data: unknown) => boolean
  component: Component
  data: unknown
}

export interface IForm {
  [x: string]: any
  setPage: (newValue: number) => void
  setAlert: (type: string, message: string) => void
  allPages: Page[]
  checkValidity: (data: unknown, dirty: boolean, row: unknown, silent: boolean) => boolean
  getComponent: (name: string) => { setValue: (value: string) => void }
  triggerRedraw: () => void
  submission: { data: { [key: string]: string } }
}

export interface RawForm extends Omit<GeneratedForm, 'form' | 'config'> {
  form: FormSchema
  config: null | { key: string, value: string }[]
}

export interface Form extends Omit<GeneratedForm, 'form' | 'config'> {
  formContent: FormSchema
  config: null | { [key: string]: string }
}
