/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface HTTPValidationError {
  /** Detail */
  detail?: ValidationError[];
}

export interface PublicSetting {
  /** Name of the setting */
  name: string;

  /** Value of the setting */
  value: string;

  /** Description of the setting */
  description: string;

  /** Regex of the setting */
  regex: string;

  /** id of the type of the setting */
  settingTypeId?: number;

  /** The unique id of the setting */
  id: string;

  /**
   * The unique uuid of the setting
   * @format uuid
   */
  uuid: string;
}

export interface Setting {
  /** Name of the setting */
  name: string;

  /** Value of the setting */
  value: string;

  /** Description of the setting */
  description: string;

  /** Regex of the setting */
  regex: string;

  /** id of the type of the setting */
  settingTypeId?: number;

  /** The unique id of the setting */
  id: string;

  /**
   * The unique uuid of the setting
   * @format uuid
   */
  uuid: string;

  /**
   * Date of last modification
   * @format date-time
   */
  lastModified?: string;

  /** type of the setting */
  settingType?: SettingType;
}

export interface SettingCreate {
  /** Name of the setting */
  name: string;

  /** Value of the setting */
  value: string;

  /** Description of the setting */
  description: string;

  /** Regex of the setting */
  regex: string;

  /** id of the type of the setting */
  settingTypeId?: number;
}

export interface SettingType {
  /** Name of the setting */
  name: string;

  /** Description of the setting */
  description: string;

  /** The unique id of the setting */
  id: number;

  /**
   * The unique uuid of the setting
   * @format uuid
   */
  uuid: string;
}

export interface SettingTypeCreate {
  /** Name of the setting */
  name: string;

  /** Description of the setting */
  description: string;
}

export interface SettingTypeUpdate {
  /** Name of the setting */
  name?: string;

  /** Description of the setting */
  description?: string;
}

export interface SettingUpdate {
  /** Name of the setting */
  name?: string;

  /** Value of the setting */
  value?: string;

  /** Description of the setting */
  description?: string;

  /** Regex of the setting */
  regex?: string;

  /** id of the type of the setting */
  settingTypeId?: number;
}

export interface ValidationError {
  /** Location */
  loc: string[];

  /** Message */
  msg: string;

  /** Error Type */
  type: string;
}
