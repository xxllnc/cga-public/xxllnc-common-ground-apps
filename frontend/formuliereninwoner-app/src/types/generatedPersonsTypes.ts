/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

/**
 *     De aanduiding die wordt gebruikt voor adressen die geen straatnaam en huisnummeraanduidingen hebben
 * `tegenover` - to
 * `bij` - by
 */
export enum AanduidingBijHuisnummerEnum {
  Tegenover = "tegenover",
  Bij = "bij",
}

/**
 *     Geeft aan dat de persoon behandeld wordt als Nederlander, of dat door de rechter is vastgesteld dat de persoon niet de Nederlandse nationaliteit bezit
 * `behandeld_als_nederlander` - B
 * `vastgesteld_niet_nederlander` - V
 */
export enum AanduidingBijzonderNederlanderschapEnum {
  BehandeldAlsNederlander = "behandeld_als_nederlander",
  VastgesteldNietNederlander = "vastgesteld_niet_nederlander",
}

/**
 *     Gegevens over de voltrekking van het huwelijk of het aangaan van het geregistreerd partnerschap.
 * **datum** : De datum waarop het huwelijk is voltrokken of het partnerschap is aangegaan.
 * **land** : Het land waar het huwelijk is voltrokken of het partnerschap is aangegaan.
 * **plaats** : De gemeente waar het huwelijk is voltrokken of het partnerschap is aangegaan. Voor een plaats buiten Nederland bevat het antwoord een buitenlandse plaatsnaam of aanduiding.
 */
export interface AangaanHuwelijkPartnerschap {
  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datum?: DatumOnvolledig;
  land?: Waardetabel;
  plaats?: Waardetabel;

  /** Geeft aan welke gegevens over het voltrekken van het huwelijk of aangaan van het partnerschap in onderzoek zijn. Zie de functionele specificaties. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature) */
  inOnderzoek?: AangaanHuwelijkPartnerschapInOnderzoek;
}

/**
 * Geeft aan welke gegevens over het voltrekken van het huwelijk of aangaan van het partnerschap in onderzoek zijn. Zie de functionele specificaties. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature)
 */
export interface AangaanHuwelijkPartnerschapInOnderzoek {
  /** Datum */
  datum?: boolean;

  /** Land */
  land?: boolean;

  /** Plaats */
  plaats?: boolean;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datumIngangOnderzoek?: DatumOnvolledig;
}

/**
 * Gegevens over de datums die mogelijk niet volledig zijn.
 */
export interface DatumOnvolledig {
  /**
   * Dag
   * Als de dag van de datum bekend is wordt dit element gevuld, ook als de volledige datum bekend is.
   * @min 1
   * @max 31
   * @example 3
   */
  dag?: number;

  /**
   * Datum
   * Als de volledige datum bekend is wordt de datum gevuld die in de date definitie past.
   * @format date
   * @example 1989-05-03
   */
  datum?: string;

  /**
   * Jaar
   * Als het jaar van de datum bekend is wordt dit element gevuld, ook als de volledige datum bekend is.
   * @max 9999
   * @example 1989
   */
  jaar?: number;

  /**
   * Maand
   * Als de maand van een datum bekend is wordt dit element gevuld, ook als de volledige datum bekend is.
   * @min 1
   * @max 12
   * @example 5
   */
  maand?: number;
}

/**
 *     Gegevens over de geboorte van respectievelijk de persoon, de ouder, de echtgenoot/geregistreerd partner, de eerdere echtgenoot/geregistreerd partner of het kind.
 * **datum** : Datum waarop de persoon is geboren
 * **land** : Land waar de persoon is geboren
 * **plaats** : De plaats waar de persoon is geboren. Voor een plaats buiten Nederland is gemeentecode=1999 (RNI) en gemeentenaam de buitenlandse plaatsnaam of aanduiding.
 */
export interface Geboorte {
  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datum?: DatumOnvolledig;
  land?: Waardetabel;
  plaats?: Waardetabel;

  /** Geeft aan welke gegevens over de geboorte van de persoon in onderzoek zijn. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature) */
  inOnderzoek?: GeboorteInOnderzoek;
}

/**
 * Geeft aan welke gegevens over de geboorte van de persoon in onderzoek zijn. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature)
 */
export interface GeboorteInOnderzoek {
  /** Datum */
  datum?: boolean;

  /** Land */
  land?: boolean;

  /** Plaats */
  plaats?: boolean;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datumIngangOnderzoek?: DatumOnvolledig;
}

/**
 *     Geeft aan dat de persoon een man of vrouw is, of dat het geslacht (nog) onbekend is
 * `man` - M
 * `vrouw` - V
 * `onbekend` - O
 */
export enum GeslachtEnum {
  Man = "man",
  Vrouw = "vrouw",
  Onbekend = "onbekend",
}

/**
 * Gegevens over het gezag over de persoon.
 */
export interface Gezagsverhouding {
  /**
   * Indicatiecurateleregister
   * Geeft aan dat de persoon onder curatele is gesteld.
   *
   * @example true
   */
  indicatieCurateleRegister?: boolean;

  /**
   *     Geeft aan wie het gezag heeft over de minderjarige persoon.
   * * `ouder1` - 1
   * * `ouder2` - 2
   * * `derden` - D
   * * `ouder1_en_derde` - 1D
   * * `ouder2_en_derde` - 2D
   * * `ouder1_en_ouder2` - 12
   */
  indicatieGezagMinderjarige?: IndicatieGezagMinderjarigeEnum;

  /** Geeft aan welke gegevens van de gezagsverhouding in onderzoek zijn. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature) */
  inOnderzoek?: GezagsverhoudingInOnderzoek;
}

/**
 * Geeft aan welke gegevens van de gezagsverhouding in onderzoek zijn. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature)
 */
export interface GezagsverhoudingInOnderzoek {
  /** Indicatiecurateleregister */
  indicatieCurateleRegister?: boolean;

  /** Indicatiegezagminderjarige */
  indicatieGezagMinderjarige?: boolean;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datumIngangOnderzoek?: DatumOnvolledig;
}

export interface HTTPValidationError {
  /** Detail */
  detail?: ValidationError[];
}

/**
 *     Geeft aan wie het gezag heeft over de minderjarige persoon.
 * `ouder1` - 1
 * `ouder2` - 2
 * `derden` - D
 * `ouder1_en_derde` - 1D
 * `ouder2_en_derde` - 2D
 * `ouder1_en_ouder2` - 12
 */
export enum IndicatieGezagMinderjarigeEnum {
  Ouder1 = "ouder1",
  Ouder2 = "ouder2",
  Derden = "derden",
  Ouder1EnDerde = "ouder1_en_derde",
  Ouder2EnDerde = "ouder2_en_derde",
  Ouder1EnOuder2 = "ouder1_en_ouder2",
}

export interface IngeschrevenPersoonHal {
  /**
   * Burgerservicenummer
   * @example 555555021
   */
  burgerservicenummer?: string;

  /**
   * Anummer
   * Het A-nummer van de persoon
   *
   */
  aNummer?: string;

  /**
   * Indicatie geheim
   * Gegevens mogen niet worden verstrekt aan derden / maatschappelijke instellingen.
   *
   */
  geheimhoudingPersoonsgegevens?: boolean;

  /**
   *     Geeft aan dat de persoon een man of vrouw is, of dat het geslacht (nog) onbekend is
   * * `man` - M
   * * `vrouw` - V
   * * `onbekend` - O
   */
  geslachtsaanduiding?: GeslachtEnum;

  /**
   * Leeftijd
   * Leeftijd in jaren op het moment van bevragen.
   *
   * @example 34
   */
  leeftijd?: number;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datumEersteInschrijvingGBA?: DatumOnvolledig;
  kiesrecht?: Kiesrecht;
  naam?: NaamPersoon;

  /** Geeft aan welke gegevens van de persoon in onderzoek zijn. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature). */
  inOnderzoek?: PersoonInOnderzoek;

  /** Nationaliteiten */
  nationaliteiten?: Nationaliteit[];

  /**
   *     Gegevens over de geboorte van respectievelijk de persoon, de ouder, de echtgenoot/geregistreerd partner, de eerdere echtgenoot/geregistreerd partner of het kind.
   * * **datum** : Datum waarop de persoon is geboren
   * * **land** : Land waar de persoon is geboren
   * * **plaats** : De plaats waar de persoon is geboren. Voor een plaats buiten Nederland is gemeentecode=1999 (RNI) en gemeentenaam de buitenlandse plaatsnaam of aanduiding.
   */
  geboorte?: Geboorte;

  /** * **datum**: de datum waarop de bijhouding van de persoonsgegevens is gestaakt. */
  opschortingBijhouding?: OpschortingBijhouding;

  /**
   *     Gegevens over het overlijden van de persoon.
   * * **datum** : datum waarop de persoon is overleden.
   * * **land** : land waar de persoon is overleden.
   * * **plaats** : gemeente waar de persoon is overleden. Is de persoon overleden buiten Nederland, dan bevat het antwoord alleen een buitenlandse plaatsnaam of aanduiding.
   */
  overlijden?: Overlijden;

  /** Eigenschappen van het adres die kunnen worden hergebruikt in andere API's waarin adresgegevens worden opgenomen. */
  verblijfplaats?: Verblijfplaats;

  /** Gegevens over het gezag over de persoon. */
  gezagsverhouding?: Gezagsverhouding;

  /**
   *     Gegevens over de verblijfsrechtelijke status van de persoon.
   * * **datumEinde**: Datum waarop de geldigheid van de gegevens over de verblijfstitel is beëindigd.
   * * **datumIngang**: Datum waarop de gegevens over de verblijfstitel geldig zijn geworden.
   * * **aanduiding** : Verblijfstiteltabel die aangeeft over welke verblijfsrechtelijke status de persoon beschikt.
   */
  verblijfstitel?: Verblijfstitel;

  /** Reisdocumentnummers */
  reisdocumentnummers?: Reisdocumentnummer[];

  /** Kinderen */
  kinderen?: Kind[];

  /** Ouders */
  ouders?: Ouder[];

  /** Partners */
  partners?: Partner[];
}

export interface Kiesrecht {
  /**
   * Europeeskiesrecht
   * Geeft aan of persoon een oproep moet ontvangen voor verkiezingen voor het Europees parlement.
   *
   * @example true
   */
  europeesKiesrecht?: boolean;

  /**
   * Uitgeslotenvankiesrecht
   * @example true
   */
  uitgeslotenVanKiesrecht?: boolean;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  einddatumUitsluitingEuropeesKiesrecht?: DatumOnvolledig;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  einddatumUitsluitingKiesrecht?: DatumOnvolledig;
}

/**
 * Gegevens over een kind van de persoon.
 */
export interface Kind {
  /**
   * Burgerservicenummer
   * @example 555555021
   */
  burgerservicenummer?: string;

  /**
   * Leeftijd
   * Leeftijd op het moment van bevragen
   *
   * @example 12
   */
  leeftijd?: number;

  /** Geeft aan of de gegevens over het kind van de persoon in onderzoek zijn. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature) */
  inOnderzoek?: KindInOnderzoek;
  naam?: Naam;

  /**
   *     Gegevens over de geboorte van respectievelijk de persoon, de ouder, de echtgenoot/geregistreerd partner, de eerdere echtgenoot/geregistreerd partner of het kind.
   * * **datum** : Datum waarop de persoon is geboren
   * * **land** : Land waar de persoon is geboren
   * * **plaats** : De plaats waar de persoon is geboren. Voor een plaats buiten Nederland is gemeentecode=1999 (RNI) en gemeentenaam de buitenlandse plaatsnaam of aanduiding.
   */
  geboorte?: Geboorte;
}

/**
 * Geeft aan of de gegevens over het kind van de persoon in onderzoek zijn. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature)
 */
export interface KindInOnderzoek {
  /** Burgerservicenummer */
  burgerservicenummer?: boolean;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datumIngangOnderzoek?: DatumOnvolledig;
}

export interface Naam {
  /**
   * Geslachtsnaam
   * De achternaam van een persoon.
   *
   * @example Vries
   */
  geslachtsnaam?: string;

  /**
   * Voorletters
   * De voorletters van de persoon, afgeleid van de voornamen.
   *
   * @example P.J.
   */
  voorletters?: string;

  /**
   * Voornamen
   * De verzameling namen voor de geslachtsnaam, gescheiden door spaties.
   *
   * @example Pieter Jan
   */
  voornamen?: string;

  /**
   * Voorvoegsel
   * @example de
   */
  voorvoegsel?: string;
  adellijkeTitelPredikaat?: Waardetabel;

  /** Geeft aan welke gegevens over de naam in onderzoek zijn. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature) */
  inOnderzoek?: NaamInOnderzoek;
}

/**
 * Geeft aan welke gegevens over de naam in onderzoek zijn. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature)
 */
export interface NaamInOnderzoek {
  /** Geslachtsnaam */
  geslachtsnaam?: boolean;

  /** Voornamen */
  voornamen?: boolean;

  /** Voorvoegsel */
  voorvoegsel?: boolean;

  /** Adellijketitelpredikaat */
  adellijkeTitelPredikaat?: boolean;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datumIngangOnderzoek?: DatumOnvolledig;
}

export interface NaamPersoon {
  /**
   * Geslachtsnaam
   * De achternaam van een persoon.
   *
   * @example Vries
   */
  geslachtsnaam?: string;

  /**
   * Voorletters
   * De voorletters van de persoon, afgeleid van de voornamen.
   *
   * @example P.J.
   */
  voorletters?: string;

  /**
   * Voornamen
   * De verzameling namen voor de geslachtsnaam, gescheiden door spaties.
   *
   * @example Pieter Jan
   */
  voornamen?: string;

  /**
   * Voorvoegsel
   * @example de
   */
  voorvoegsel?: string;
  adellijkeTitelPredikaat?: Waardetabel;

  /** Geeft aan welke gegevens over de naam in onderzoek zijn. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature) */
  inOnderzoek?: NaamPersoonInOnderzoek;

  /**
   * Aanhef
   * Kun je gebruiken als aanhef in een brief gericht aan persoon.
   *
   * @example Hoogwelgeboren heer
   */
  aanhef?: string;

  /**
   * Aanschrijfwijze
   * Samengestelde naam die je kunt gebruiken in de communicatie met de persoon.
   *
   * @example H.W. baron van den Aedel
   */
  aanschrijfwijze?: string;

  /**
   * Regelvoorafgaandaanaanschrijfwijze
   * Deze regel moet als aparte regel boven de aanschrijfwijze worden geplaatst. Komt alleen voor bij personen met een adellijke titel of predicaat.
   *
   * @example De hoogwelgeboren heer
   */
  regelVoorafgaandAanAanschrijfwijze?: string;

  /**
   * Gebruikinlopendetekst
   * Naam van persoon die je kunt gebruiken als je in lopende tekst (bijvoorbeeld in een brief) aan persoon refereert.
   *
   * @example baron Van den Aedel
   */
  gebruikInLopendeTekst?: string;

  /**
   *     De manier waarop de geslachtsnaam van persoon en partner van persoon moet worden verwerkt in de manier waarop persoon wil worden aangesproken
   * * `eigen` - E - gebruik alleen de eigen naam
   * * `eigen_partner` - N - gebruik de eigen naam voor de partnernaam
   * * `partner` - P gebruik alleen de partnernaam
   * * `partner_eigen` - V - gebruik de partnernaam voor de eigen naam.
   *
   * De aanduiding naamgebruik is verwerkt in Aanhef, Aanschrijfwijze en gebruikInLopendeTekst."
   */
  aanduidingNaamgebruik?: NaamgebruikEnum;
}

/**
 * Geeft aan welke gegevens over de naam in onderzoek zijn. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature)
 */
export interface NaamPersoonInOnderzoek {
  /** Geslachtsnaam */
  geslachtsnaam?: boolean;

  /** Voornamen */
  voornamen?: boolean;

  /** Voorvoegsel */
  voorvoegsel?: boolean;

  /** Adellijketitelpredikaat */
  adellijkeTitelPredikaat?: boolean;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datumIngangOnderzoek?: DatumOnvolledig;

  /** Aanduidingnaamgebruik */
  aanduidingNaamgebruik?: boolean;
}

/**
*     De manier waarop de geslachtsnaam van persoon en partner van persoon moet worden verwerkt in de manier waarop persoon wil worden aangesproken
* `eigen` - E - gebruik alleen de eigen naam
* `eigen_partner` - N - gebruik de eigen naam voor de partnernaam
* `partner` - P gebruik alleen de partnernaam
* `partner_eigen` - V - gebruik de partnernaam voor de eigen naam.


De aanduiding naamgebruik is verwerkt in Aanhef, Aanschrijfwijze en gebruikInLopendeTekst."
*/
export enum NaamgebruikEnum {
  Eigen = "eigen",
  EigenPartner = "eigen_partner",
  Partner = "partner",
  PartnerEigen = "partner_eigen",
}

/**
 * * **redenOpname** : De reden op grond waarvan de persoon de nationaliteit gekregen heeft.
 */
export interface Nationaliteit {
  /**
   *     Geeft aan dat de persoon behandeld wordt als Nederlander, of dat door de rechter is vastgesteld dat de persoon niet de Nederlandse nationaliteit bezit
   * * `behandeld_als_nederlander` - B
   * * `vastgesteld_niet_nederlander` - V
   */
  aanduidingBijzonderNederlanderschap?: AanduidingBijzonderNederlanderschapEnum;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datumIngangGeldigheid?: DatumOnvolledig;
  nationaliteit?: Waardetabel;
  redenOpname?: Waardetabel;

  /** Geeft aan welke gegevens over de nationaliteit in onderzoek zijn. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature) */
  inOnderzoek?: NationaliteitInOnderzoek;
}

/**
 * Geeft aan welke gegevens over de nationaliteit in onderzoek zijn. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature)
 */
export interface NationaliteitInOnderzoek {
  /** Aanduidingbijzondernederlanderschap */
  aanduidingBijzonderNederlanderschap?: boolean;

  /** Nationaliteit */
  nationaliteit?: boolean;

  /** Redenopname */
  redenOpname?: boolean;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datumIngangOnderzoek?: DatumOnvolledig;
}

/**
 * * **datum**: de datum waarop de bijhouding van de persoonsgegevens is gestaakt.
 */
export interface OpschortingBijhouding {
  /**
   *     Redenen voor opschorting van de bijhouding
   * * `overlijden` - O
   * * `emigratie` - E
   * * `ministerieel_besluit` - M
   * * `pl_aangelegd_in_de_rni` - R - opgeschort omdat persoon is ingeschreven in het Register Niet Ingezeten (RNI).
   * * `fout` - F
   */
  reden?: RedenOpschortingBijhoudingEnum;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datum?: DatumOnvolledig;
}

/**
 *     Gegevens over de ouder van de persoon.
 * **datumIngangFamilierechtelijkeBetrekking** - De datum waarop de familierechtelijke betrekking is ontstaan.
 */
export interface Ouder {
  /**
   * Burgerservicenummer
   * @example 555555021
   */
  burgerservicenummer?: string;

  /**
   *     Geeft aan dat de persoon een man of vrouw is, of dat het geslacht (nog) onbekend is
   * * `man` - M
   * * `vrouw` - V
   * * `onbekend` - O
   */
  geslachtsaanduiding?: GeslachtEnum;

  /**
   *     Geeft aan om welke ouder het gaat volgens de BRP
   * * `ouder1` - 1
   * * `ouder2` - 2
   */
  ouderAanduiding?: OuderAanduidingEnum;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datumIngangFamilierechtelijkeBetrekking?: DatumOnvolledig;
  naam?: Naam;

  /** Geeft aan welke gegevens van de de ouder in onderzoek zijn. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature) */
  inOnderzoek?: OuderInOnderzoek;

  /**
   *     Gegevens over de geboorte van respectievelijk de persoon, de ouder, de echtgenoot/geregistreerd partner, de eerdere echtgenoot/geregistreerd partner of het kind.
   * * **datum** : Datum waarop de persoon is geboren
   * * **land** : Land waar de persoon is geboren
   * * **plaats** : De plaats waar de persoon is geboren. Voor een plaats buiten Nederland is gemeentecode=1999 (RNI) en gemeentenaam de buitenlandse plaatsnaam of aanduiding.
   */
  geboorte?: Geboorte;
}

/**
 *     Geeft aan om welke ouder het gaat volgens de BRP
 * `ouder1` - 1
 * `ouder2` - 2
 */
export enum OuderAanduidingEnum {
  Ouder1 = "ouder1",
  Ouder2 = "ouder2",
}

/**
 * Geeft aan welke gegevens van de de ouder in onderzoek zijn. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature)
 */
export interface OuderInOnderzoek {
  /** Burgerservicenummer */
  burgerservicenummer?: boolean;

  /** Datumingangfamilierechtelijkebetrekking */
  datumIngangFamilierechtelijkeBetrekking?: boolean;

  /** Geslachtsaanduiding */
  geslachtsaanduiding?: boolean;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datumIngangOnderzoek?: DatumOnvolledig;
}

/**
 *     Gegevens over het overlijden van de persoon.
 * **datum** : datum waarop de persoon is overleden.
 * **land** : land waar de persoon is overleden.
 * **plaats** : gemeente waar de persoon is overleden. Is de persoon overleden buiten Nederland, dan bevat het antwoord alleen een buitenlandse plaatsnaam of aanduiding.
 */
export interface Overlijden {
  /**
   * Indicatieoverleden
   * Geeft aan dat iemand is overleden (waarde true), ongeacht of de overlijdensdatum bekend is.
   *
   */
  indicatieOverleden?: boolean;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datum?: DatumOnvolledig;
  land?: Waardetabel;
  plaats?: Waardetabel;

  /** Geeft aan welke gegevens over het overlijden van de persoon in onderzoek zijn. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature) */
  inOnderzoek?: OverlijdenInOnderzoek;
}

/**
 * Geeft aan welke gegevens over het overlijden van de persoon in onderzoek zijn. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature)
 */
export interface OverlijdenInOnderzoek {
  /** Datum */
  datum?: boolean;

  /** Land */
  land?: boolean;

  /** Plaats */
  plaats?: boolean;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datumIngangOnderzoek?: DatumOnvolledig;
}

/**
 * Gegevens over een gesloten huwelijk/geregistreerd partnerschap van de persoon.
 */
export interface Partner {
  /**
   * Burgerservicenummer
   * @example 555555021
   */
  burgerservicenummer?: string;

  /**
   *     Geeft aan dat de persoon een man of vrouw is, of dat het geslacht (nog) onbekend is
   * * `man` - M
   * * `vrouw` - V
   * * `onbekend` - O
   */
  geslachtsaanduiding?: GeslachtEnum;

  /**
   *     Soort verbintenis die bij de burgerlijke stand is ingeschreven
   * * `huwelijk` - H
   * * `geregistreerd_partnerschap` - P
   */
  soortVerbintenis?: SoortVerbintenisEnum;
  naam?: Naam;

  /**
   *     Gegevens over de geboorte van respectievelijk de persoon, de ouder, de echtgenoot/geregistreerd partner, de eerdere echtgenoot/geregistreerd partner of het kind.
   * * **datum** : Datum waarop de persoon is geboren
   * * **land** : Land waar de persoon is geboren
   * * **plaats** : De plaats waar de persoon is geboren. Voor een plaats buiten Nederland is gemeentecode=1999 (RNI) en gemeentenaam de buitenlandse plaatsnaam of aanduiding.
   */
  geboorte?: Geboorte;

  /** Geeft aan welke gegevens over het huwelijk of het partnerschap in onderzoek zijn. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature) */
  inOnderzoek?: PartnerInOnderzoek;

  /**
   *     Gegevens over de voltrekking van het huwelijk of het aangaan van het geregistreerd partnerschap.
   * * **datum** : De datum waarop het huwelijk is voltrokken of het partnerschap is aangegaan.
   * * **land** : Het land waar het huwelijk is voltrokken of het partnerschap is aangegaan.
   * * **plaats** : De gemeente waar het huwelijk is voltrokken of het partnerschap is aangegaan. Voor een plaats buiten Nederland bevat het antwoord een buitenlandse plaatsnaam of aanduiding.
   */
  aangaanHuwelijkPartnerschap?: AangaanHuwelijkPartnerschap;
}

/**
 * Geeft aan welke gegevens over het huwelijk of het partnerschap in onderzoek zijn. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature)
 */
export interface PartnerInOnderzoek {
  /** Burgerservicenummer */
  burgerservicenummer?: boolean;

  /** Geslachtsaanduiding */
  geslachtsaanduiding?: boolean;

  /** Soortverbintenis */
  soortVerbintenis?: boolean;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datumIngangOnderzoek?: DatumOnvolledig;
}

/**
 * Geeft aan welke gegevens van de persoon in onderzoek zijn. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature).
 */
export interface PersoonInOnderzoek {
  /** Burgerservicenummer */
  burgerservicenummer?: boolean;

  /** Geslachtsaanduiding */
  geslachtsaanduiding?: boolean;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datumIngangOnderzoek?: DatumOnvolledig;
}

/**
 *     Redenen voor opschorting van de bijhouding
 * `overlijden` - O
 * `emigratie` - E
 * `ministerieel_besluit` - M
 * `pl_aangelegd_in_de_rni` - R - opgeschort omdat persoon is ingeschreven in het Register Niet Ingezeten (RNI).
 * `fout` - F
 */
export enum RedenOpschortingBijhoudingEnum {
  Overlijden = "overlijden",
  Emigratie = "emigratie",
  MinisterieelBesluit = "ministerieel_besluit",
  PlAangelegdInDeRni = "pl_aangelegd_in_de_rni",
  Fout = "fout",
}

/**
 * Het nummer van het verstrekte Nederlandse reisdocument.
 * @example 546376728
 */
export type Reisdocumentnummer = string;

/**
 *     Aanduiding van het soort adres
 * `woonadres` - W - adres waar de persoon woont
 * `briefadres` - B - het adres van een andere persoon of van een instelling (de zogenoemde briefadresgever). Met dit adres van de briefadresgever is de persoon zonder woonadres toch bereikbaar voor de overheid.
 */
export enum SoortAdresEnum {
  Woonadres = "woonadres",
  Briefadres = "briefadres",
}

/**
 *     Soort verbintenis die bij de burgerlijke stand is ingeschreven
 * `huwelijk` - H
 * `geregistreerd_partnerschap` - P
 */
export enum SoortVerbintenisEnum {
  Huwelijk = "huwelijk",
  GeregistreerdPartnerschap = "geregistreerd_partnerschap",
}

export interface ValidationError {
  /** Location */
  loc: string[];

  /** Message */
  msg: string;

  /** Error Type */
  type: string;
}

/**
 * Eigenschappen van het adres die kunnen worden hergebruikt in andere API's waarin adresgegevens worden opgenomen.
 */
export interface Verblijfplaats {
  /**
   * openbareruimte naam
   * Een naam die door de gemeente aan een openbare ruimte is gegeven.
   * @example Laan van de landinrichtingscommissie Duiven-Westervoort
   */
  straat?: string;

  /**
   * Huisnummer
   * Een nummer dat door de gemeente aan een adresseerbaar object is gegeven.
   * @example 1
   */
  huisnummer?: number;

  /**
   * Huisletter
   * Een toevoeging aan een huisnummer in de vorm van een letter die door de gemeente aan een adresseerbaar object is gegeven.
   * @example A
   */
  huisletter?: string;

  /**
   * Huisnummertoevoeging
   * Een toevoeging aan een huisnummer of een combinatie van huisnummer en huisletter die door de gemeente aan een adresseerbaar object is gegeven.
   * @example bis
   */
  huisnummertoevoeging?: string;

  /**
   * Postcode
   * De door PostNL vastgestelde code die bij een bepaalde combinatie van een straatnaam en een huisnummer hoort.
   * @example 6922KZ
   */
  postcode?: string;

  /**
   * woonplaats naam
   * Een woonplaats is een gedeelte van het grondgebied van de gemeente met een naam.
   * @example Duiven
   */
  woonplaats?: string;

  /**
   * Adresseerbaarobjectidentificatie
   * De verblijfplaats van de persoon kan een ligplaats, een standplaats of een verblijfsobject zijn.
   *
   * @example 0226010000038820
   */
  adresseerbaarObjectIdentificatie?: string;

  /**
   *     De aanduiding die wordt gebruikt voor adressen die geen straatnaam en huisnummeraanduidingen hebben
   * * `tegenover` - to
   * * `bij` - by
   */
  aanduidingBijHuisnummer?: AanduidingBijHuisnummerEnum;

  /**
   * Nummeraanduidingidentificatie
   * Unieke identificatie van een nummeraanduiding (en het bijbehorende adres) in de BAG.
   *
   * @example 0518200000366054
   */
  nummeraanduidingIdentificatie?: string;

  /**
   *     Aanduiding van het soort adres
   * * `woonadres` - W - adres waar de persoon woont
   * * `briefadres` - B - het adres van een andere persoon of van een instelling (de zogenoemde briefadresgever). Met dit adres van de briefadresgever is de persoon zonder woonadres toch bereikbaar voor de overheid.
   */
  functieAdres?: SoortAdresEnum;

  /**
   * Indicatievestigingvanuitbuitenland
   * Geeft aan dat de ingeschreven persoon zich vanuit het buitenland heeft ingeschreven.
   *
   */
  indicatieVestigingVanuitBuitenland?: boolean;

  /**
   * Locatiebeschrijving
   * Omschrijving van de ligging van een verblijfsobject, standplaats of ligplaats.
   *
   * @example Naast de derde brug
   */
  locatiebeschrijving?: string;

  /**
   * Kortenaam
   * De officiële openbareruimtenaam uit de Basisregistratie Gebouwen en Adressen (BAG) of een verkorte versie.
   *
   */
  korteNaam?: string;

  /**
   * Vanuitvertrokkenonbekendwaarheen
   * Geeft aan dat de persoon is teruggekeerd uit een situatie van 'vertrokken onbekend waarheen.'
   *
   * @example true
   */
  vanuitVertrokkenOnbekendWaarheen?: boolean;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datumAanvangAdreshouding?: DatumOnvolledig;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datumIngangGeldigheid?: DatumOnvolledig;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datumInschrijvingInGemeente?: DatumOnvolledig;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datumVestigingInNederland?: DatumOnvolledig;
  gemeenteVanInschrijving?: Waardetabel;
  landVanwaarIngeschreven?: Waardetabel;

  /**
   * Adresregel1
   * Het eerste deel van een adres is een combinatie van de straat en huisnummer.
   *
   * @example Laan van de landinrichtingscommissie Duiven-Westervoort 26A-3
   */
  adresregel1?: string;

  /**
   * Adresregel2
   * Het tweede deel van een adres is een combinatie van woonplaats eventueel in combinatie met de postcode.
   *
   * @example 1234AA Nootdorp
   */
  adresregel2?: string;

  /**
   * Adresregel3
   * Het derde deel van een adres is optioneel. Het gaat om een of meer geografische gebieden van het adres in het buitenland.
   *
   * @example Selangor
   */
  adresregel3?: string;

  /**
   * Vertrokkenonbekendwaarheen
   * Indicatie dat de ingeschreven persoon is vertrokken naar het buitenland, maar dat niet bekend is waar naar toe.
   *
   */
  vertrokkenOnbekendWaarheen?: boolean;
  land?: Waardetabel;

  /** Geeft aan welke gegevens over het verblijf en adres van de persoon in onderzoek zijn. Elementen van het GBA-adres zelf (Dat zou eigenlijk een BAG-adres moeten zijn) kunnen niet in onderzoek zijn. Wel de relatie naar de nummeraanduiding. Dat wordt gedaan door de identificatiecodeNummeraanduiding in onderzoek te zetten. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature) */
  inOnderzoek?: VerblijfplaatsInOnderzoek;
}

/**
 * Geeft aan welke gegevens over het verblijf en adres van de persoon in onderzoek zijn. Elementen van het GBA-adres zelf (Dat zou eigenlijk een BAG-adres moeten zijn) kunnen niet in onderzoek zijn. Wel de relatie naar de nummeraanduiding. Dat wordt gedaan door de identificatiecodeNummeraanduiding in onderzoek te zetten. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature)
 */
export interface VerblijfplaatsInOnderzoek {
  /** Aanduidingbijhuisnummer */
  aanduidingBijHuisnummer?: boolean;

  /** Datumaanvangadreshouding */
  datumAanvangAdreshouding?: boolean;

  /** Datuminganggeldigheid */
  datumIngangGeldigheid?: boolean;

  /** Datuminschrijvingingemeente */
  datumInschrijvingInGemeente?: boolean;

  /** Datumvestiginginnederland */
  datumVestigingInNederland?: boolean;

  /** Functieadres */
  functieAdres?: boolean;

  /** Gemeentevaninschrijving */
  gemeenteVanInschrijving?: boolean;

  /** Huisletter */
  huisletter?: boolean;

  /** Huisnummer */
  huisnummer?: boolean;

  /** Huisnummertoevoeging */
  huisnummertoevoeging?: boolean;

  /** Nummeraanduidingidentificatie */
  nummeraanduidingIdentificatie?: boolean;

  /** Adresseerbaarobjectidentificatie */
  adresseerbaarObjectIdentificatie?: boolean;

  /** Landvanwaaringeschreven */
  landVanwaarIngeschreven?: boolean;

  /** Locatiebeschrijving */
  locatiebeschrijving?: boolean;

  /** Straat */
  straat?: boolean;

  /** Postcode */
  postcode?: boolean;

  /** Kortenaam */
  korteNaam?: boolean;

  /** Verblijfbuitenland */
  verblijfBuitenland?: boolean;

  /** Woonplaats */
  woonplaats?: boolean;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datumIngangOnderzoek?: DatumOnvolledig;
}

/**
 *     Gegevens over de verblijfsrechtelijke status van de persoon.
 * **datumEinde**: Datum waarop de geldigheid van de gegevens over de verblijfstitel is beëindigd.
 * **datumIngang**: Datum waarop de gegevens over de verblijfstitel geldig zijn geworden.
 * **aanduiding** : Verblijfstiteltabel die aangeeft over welke verblijfsrechtelijke status de persoon beschikt.
 */
export interface Verblijfstitel {
  aanduiding?: Waardetabel;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datumEinde?: DatumOnvolledig;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datumIngang?: DatumOnvolledig;

  /** Geeft aan welke gegevens over de verblijfstitel in onderzoek zijn. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature) */
  inOnderzoek?: VerblijfstitelInOnderzoek;
}

/**
 * Geeft aan welke gegevens over de verblijfstitel in onderzoek zijn. Zie de [functionele specificaties](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/blob/v1.1.0/features/in_onderzoek.feature)
 */
export interface VerblijfstitelInOnderzoek {
  /** Aanduiding */
  aanduiding?: boolean;

  /** Datumeinde */
  datumEinde?: boolean;

  /** Datumingang */
  datumIngang?: boolean;

  /** Gegevens over de datums die mogelijk niet volledig zijn. */
  datumIngangOnderzoek?: DatumOnvolledig;
}

export interface Waardetabel {
  /**
   * Code
   * @example 6030
   */
  code?: string;

  /**
   * Omschrijving
   * @example Nederland
   */
  omschrijving?: string;
}
