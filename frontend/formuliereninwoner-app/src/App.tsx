import { FC } from 'react'
import { BrowserRouter, useRoutes } from 'react-router-dom'
import { CssOverwrite } from 'components/layout/cssOverwrite'
import { useStoreActions } from 'store'
import { FormPage } from 'pages'
import { Page } from 'components/layout'
import './styles/styles.scss'


export const getRedirectUrl = (query: URLSearchParams): string => {
  const href = window.location.href
  const formId = query.get('formId') ? query.get('formId') : href.split('/').pop()
  return `${window.location.origin}/formulieren?formId=${formId ?? ''}`
}

const App: FC = () => {
  const initSettings = useStoreActions(actions => actions.settings.init)


  const Pages = () => useRoutes([
    { path: '/formulieren/:formId', element: <FormPage /> },
    { path: '/formulieren', element: <FormPage /> },
    { path: '*', element: <Page className={'centerText'}> <p>Pagina niet gevonden</p></Page> }
  ])

  initSettings()

  return (
    <>
      <CssOverwrite />
      <BrowserRouter>
        <Pages />
      </BrowserRouter>
    </>
  )
}

export default App