#!/bin/bash

function is_template_database() {
  local var_database_name=$1
  local var_application_database_name=$2

  if [ -z "$var_database_name" ] || [ -z "$var_application_database_name" ]; then
    return 1
  fi

  if [[ $var_database_name == "template_$var_application_database_name"* ]]; then
    return 0
  fi

  return 1
}

function is_database() {
  local var_database_name=$1
  local var_application_database_name=$2

  if [ -z "$var_database_name" ] || [ -z "$var_application_database_name" ]; then
    return 1
  fi

  if [[ $var_database_name == "$var_application_database_name"* ]]; then
    return 0
  fi

  is_template_database "$var_database_name" "$var_application_database_name"; var_result=$?
  if [ "$var_result" == "0" ]; then
    return 0
  fi

  return 1
}

function alembic_upgrade_head() {
  local var_alembic_upgrade=$1
  local var_application_database_name=$2
  local var_application_container=$3
  local var_database_url=$4
  local var_database_name=$5

  if [ -z "$var_application_container" ]; then
    return 0
  fi

  if [ -z "$var_alembic_upgrade" ] || [ -z "$var_application_database_name" ] || [ -z "$var_database_url" ] || [ -z "$var_database_name" ]; then
    return 1
  fi

  if [ "$var_alembic_upgrade" != "yes" ]; then
    return 0
  fi

  # check if application database is valid
  is_database "$var_database_name" "$var_application_database_name"; var_result=$?
  if [ "$var_result" != "0" ]; then
    return 1
  fi

  echo "docker-compose run -e DATABASE_URL=$var_database_url --rm ${var_application_container} alembic upgrade head"
  docker-compose run -e DATABASE_URL=$var_database_url --rm ${var_application_container} alembic upgrade head
}

function should_add_initial_data() {
  local var_add_initial_data=$1
  local var_application_database_name=$2
  local var_application_container=$3
  local var_database_url=$4
  local var_database_name=$5

  if [ -z "$var_application_container" ]; then
    return 0
  fi

  if [ -z "$var_add_initial_data" ] || [ -z "$var_application_database_name" ] || [ -z "$var_database_url" ] || [ -z "$var_database_name" ]; then
    return 1
  fi

  if [ "$var_add_initial_data" != "yes" ]; then
    return 0
  fi

  # check if application database is valid
  is_database "$var_database_name" "$var_application_database_name"; var_result=$?
  if [ "$var_result" != "0" ]; then
    return 1
  fi

  # ignore the template database
  is_template_database "$var_database_name" "$var_application_database_name"; var_result=$?
  if [ "$var_result" == "0" ]; then
    return 0
  fi

  echo "docker-compose run -e DATABASE_URL=$var_database_url --rm ${var_application_container} python3 app/initial_data.py"
  docker-compose run -e DATABASE_URL=$var_database_url --rm ${var_application_container} python3 app/initial_data.py
}


# Key: docker service name. Value: db name
declare -A applications
applications["All"]="Placeholder for all"
applications["contactmomenten-api"]="contactmomenten"
applications["terugbelverzoeken-api"]="callbackrequests"
applications["formulierenbeheer-api"]="formscatalogue"
applications["verzoeken-api"]="requestregistry"
applications["instellingen-api"]="settings"
applications["payment-api"]="payments"
applications["persons-api"]="persons"

basic_con_url="postgresql://postgres:password@postgres"

echo "This script is meant to run by the developer to create the local cga app databases for development"

read -p "Is the postgress docker container running? [yes]: " postgres_is_running
postgres_is_running=${postgres_is_running:-yes}
if test "$postgres_is_running" != "yes"; then
  echo "To continue, first start the postgres docker container and then start this script."
  exit
fi

PS3="Select an application: "
select app in $( echo ${!applications[@]} | tr ' ' $'\n' | sort )
do
  break
done

read -p "Alembic upgrade head? [yes]: " alembic_upgrade
alembic_upgrade=${alembic_upgrade:-yes}

read -p "Add initial data? [yes]: " add_initial_data
add_initial_data=${add_initial_data:-yes}

###########################################################################################
# read all databases and determin for each database if it needs to be updated
# and/or if the initial data needs to be added.
###########################################################################################
for line in $(docker-compose run --rm postgres psql -P pager=off -d "$basic_con_url/postgres" -t -c "SELECT datname FROM pg_database;")
do
  db_name="${line//[$'\t\r\n ']}"
  if test "$db_name" != ""; then
    application_container=""
    application_database_name=""
    database_url="$basic_con_url/$db_name"

    if test "$app" != "All"; then
      application_container=$app
      application_database_name="${applications[$app]}"
    else
      for key in "${!applications[@]}"
      do
        application_database_name="${applications[$key]}"
        if [[ ($db_name == "$application_database_name"*) || ($db_name == "template_$application_database_name"*) ]]; then
          application_container="$key"
          break
        fi
        application_database_name=""
      done

    fi

    alembic_upgrade_head "$alembic_upgrade" "$application_database_name" "$application_container" "$database_url" "$db_name"
    should_add_initial_data "$add_initial_data" "$application_database_name" "$application_container" "$database_url" "$db_name"

  fi
done
