#!/bin/bash
set -e

exxellence=(
  "contactmomenten_1581f563_3090_41a0_8d32_8d6adc4e8018"
  "callbackrequests_f21f8176_78fe_4692_b071_12fa22bb29a3"
  "payments_c39d59fc_5c68_4091_b340_9a78602e86fe"
  "requestregistry_c39d59fc_5c68_4091_b340_9a78602e86fe"
  "settings_c39d59fc_5c68_4091_b340_9a78602e86fe"
)
zaakstad=(
  "contactmomenten_184b1f09_4c0a_4693_b055_4a86a3092d3a"
  "callbackrequests_dd4ca9a8_36ac_4f61_9e9a_d17b29aec1db"
  "payments_3f0c79a7_d875_4d30_8032_a5985341a018"
  "requestregistry_3f0c79a7_d875_4d30_8032_a5985341a018"
  "settings_3f0c79a7_d875_4d30_8032_a5985341a018"
)
zuiddrecht=(
  "contactmomenten_9082f61b_b259_4c9c_bc46_3f1933c57036"
  "callbackrequests_2a2566ff_f0ca_4de3_81f2_0eef939de21b"
  "payments_1b6ee52f_dd98_49ba_93c8_43e8624fe57b"
  "requestregistry_1b6ee52f_dd98_49ba_93c8_43e8624fe57b"
  "settings_1b6ee52f_dd98_49ba_93c8_43e8624fe57b"
)
generic_databases=(
  "xxeco"
  "formscatalogue"
  "persons"
  "contactmomenten"
  "callbackrequests"
  "payments"
  "requestregistry"
  "settings"
) 

all_database=("${generic_databases[@]}" "${exxellence[@]}" "${zaakstad[@]}" "${zuiddrecht[@]}")
for databases_name in "${all_database[@]}"; do

  username=${databases_name%%_*}

  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL

    CREATE DATABASE $databases_name;

    \c $databases_name
    -- Enable UUID field type
    CREATE EXTENSION "uuid-ossp";
EOSQL

  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
  SELECT 'CREATE USER $username SUPERUSER PASSWORD ''${username}123'''
  WHERE NOT EXISTS (SELECT FROM pg_catalog.pg_roles WHERE rolname = '$username')\gexec
EOSQL

  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
  GRANT ALL PRIVILEGES ON DATABASE $databases_name TO $username;
EOSQL
done

template_database=(
  "template_callbackrequests"
  "template_contactmomenten"
  "template_payments"
  "template_requestregistry"
  "template_settings"
)
for template_name in "${template_database[@]}"; do

  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL

    CREATE DATABASE $template_name;

    \c $template_name
    -- Enable UUID field type
    CREATE EXTENSION "uuid-ossp";

EOSQL
done



