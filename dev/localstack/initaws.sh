aws --endpoint-url=http://localhost:4566 sns create-topic --name xxeco-event-topic
aws --endpoint-url=http://localhost:4566 sqs create-queue --queue-name xxeco-dlq
aws --endpoint-url=http://localhost:4566 sqs create-queue --queue-name xxeco-audit-queue --attributes file:///docker-entrypoint-initaws.d/xxeco-audit-queue.json
aws --endpoint-url=http://localhost:4566 sns subscribe --topic-arn arn:aws:sns:eu-central-1:000000000000:xxeco-event-topic --protocol sqs --notification-endpoint http://localhost:4566/000000000000/xxeco-audit-queue --attributes file:///docker-entrypoint-initaws.d/xxeco-topic-subscription.json

aws --endpoint-url=http://localhost:4566 s3api create-bucket --bucket verzoeken-api-temporary-file-upload-store --region eu-central-1 --create-bucket-configuration  file:///docker-entrypoint-initaws.d/s3.json
