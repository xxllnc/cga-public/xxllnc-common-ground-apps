#!/bin/bash

if [ ! -x "$(command -v mkcert)" ]; then
    echo 'Error: mkcert is not installed.' >&2
    exit 1
fi

mkcert -cert-file dev.crt -key-file dev.key "*.vcap.me"
cp "$(mkcert -CAROOT)"/rootCA.pem ca.pem