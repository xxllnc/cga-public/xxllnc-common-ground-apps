Run "create_certs.sh" to create and install the certificates for local
development (requires `mkcert` to be installed; you can find the latest version
at https://github.com/FiloSottile/mkcert/releases).