# Xxllnc Common Ground Apps

This repository contains all the Xxllnc Common Ground Apps

## Project Layout

```
backend (python api's)
└── <api-name>
    └── app
        ├── alembic
        │   └── versions # where migrations are located
        ├── api
        │   └── api_v1
        │       └── endpoints
        ├── core    # config
        ├── db      # db models
        ├── tests   # pytest
        └── main.py # entrypoint to api
dev
└── certs     # Local certificates for TLS
└── db        # Script to initialize PostgreSQL databases on first start
└── nginx     # nginx configuration
e2e           # end to end tests
helm          # Helmchart to deploy to K8s
frontend(typescript react apps)
└── <app-name>
    └── public
    └── src
        ├── components
        │   └── Home.tsx
        ├── config
        │   └── index.tsx   # constants
        ├── __tests__
        │   └── test_home.tsx
        ├── index.tsx   # entrypoint
        └── App.tsx     # handles routing
scripts       # Some general scripts
```

## Development

This project depends on the following dependencies:

- docker and docker-compose
- Python and poetry for local backend development
- nodejs, npm and yarn for frontend development

### Quick Start

Create the certificates that are required to use https on the local development
environment. This requires [mkcert](https://github.com/FiloSottile/mkcert/releases)
to be installed.

```bash
# To install the locally generated root certificate in your browser/environment:
mkcert -install

cd dev/certs
./create-certs.sh
cd ../..
```

The generated pem needs to be copied to "/etc/custom-certs/ca.pem"

First time build the containers

```bash
docker-compose build
```

Starting the project with hot-reloading enabled (the first time it will take a while):

```bash
docker-compose up -d
```

To run the alembic migrations (to get your database schema up to date):

First make sure that the postgres container is running

For contactmoments run to add the tables and optionally add the intial data.

```bash
./dev/create_tables_and_add_data.sh
```

And follow the instructions on the screen to run the alembic migrations and
optionally add test data.

_Note: If you see an Nginx error at first with a `502: Bad Gateway` page, you may have to wait for webpack to build the development server (the nginx container builds much more quickly)._

Relevant URLs:

Frontend:

- https://exxellence.vcap.me/contactmomenten/
- https://exxellence.vcap.me/terugbelverzoeken/
- https://exxellence.vcap.me/formulierenbeheer/
- https://exxellence.vcap.me/formulieren/
- https://exxellence.vcap.me/mijnomgeving
- https://exxellence.vcap.me/vergaderen

API docs:

- https://exxellence.vcap.me/contactmomenten/api/docs
- https://exxellence.vcap.me/terugbelverzoeken/api/docs
- https://exxellence.vcap.me/formulierenbeheer/api/docs
- https://exxellence.vcap.me/verzoeken/api/docs
- https://exxellence.vcap.me/instellingen/api/docs
- https://exxellence.vcap.me/persons/api/docs
- https://exxellence.vcap.me/payments/api/docs

Alternative urls:
Instead of using exxellence.vcap.me the following urls are also supported. Each of the
folowing urls represent an other organization.

- https://zaakstad.vcap.me/...
- https://zuiddrecht.vcap.me/...

pgAdmin4:

- https://pgadmin.vcap.me/ - log in as `admin@example.com` with password `password`.

### Login

You can now login with the user `admin@mijn.xxllnc.com` and password `password`

### Rebuilding containers:

```bash
docker-compose build
```

### Restarting containers:

```bash
docker-compose restart
```

### Bringing containers down:

```bash
docker-compose down
```

### Frontend Development

Alternatively to running inside docker, it can sometimes be easier
to use npm/yarn directly for quicker reloading. To run using yarn:

```bash
cd frontend
yarn
yarn start
```

This should redirect you to http://localhost:\<see .env for portnumber>

## Migrations

To create a new migration, first define the table in `app.db.models` and then run:

```bash
docker-compose run --rm contactmomenten-api alembic revision --autogenerate -m "describe change here"
```

```bash
docker-compose run --rm terugbelverzoeken-api alembic revision --autogenerate -m "describe change here"
```

```bash
docker-compose run --rm formulierenbeheer-api alembic revision --autogenerate -m "describe change here"
```

```bash
docker-compose run --rm verzoeken-api alembic revision --autogenerate -m "describe change here"
```

```bash
docker-compose run --rm instellingen-api alembic revision --autogenerate -m "describe change here"
```

```bash
docker-compose run --rm payment-api alembic revision --autogenerate -m "describe change here"
```

This will generate an update script for Alembic. Double-check if it does what
it should, adjust as needed, and add it to your commit. The upgrade/downgrade
is described in the `upgrade` and `downgrade` methods of a newly created file
in `api/app/alembic/versions`.

For more information see
[Alembic's official documentation](https://alembic.sqlalchemy.org/en/latest/tutorial.html#create-a-migration-script).

### Run new migrations

With the following script one or more databases can be updated and filled with initial data

```bash
./dev/create_tables_and_add_data.sh
```

And follow the instructions on the screen

It is possible to downgrade or upgrade again with the following commands
docker-compose run --rm alembic downgrade -1

docker-compose run --rm alembic upgrade head

<br>
To use an other, than the default, database add the option -e DATABASE\_URL to the command
For example to use the db of Zaakstad use the following command:

```bash
docker-compose run -e DATABASE_URL=postgresql://contactmomenten:contactmomenten123@postgres:5432/contactmomenten_bf585e6d_e641_4ef7_be55_9d3a7054773a --rm contactmomenten-api alembic upgrade head
```

```bash
docker-compose run --rm contactmomenten-api alembic downgrade -1
docker-compose run --rm contactmomenten-api alembic upgrade head
```

```bash
docker-compose run --rm terugbelverzoeken-api alembic downgrade -1
docker-compose run --rm terugbelverzoeken-api alembic upgrade head
```

```bash
docker-compose run --rm formulierenbeheer-api alembic downgrade -1
docker-compose run --rm formulierenbeheer-api alembic upgrade head
```

```bash
docker-compose run --rm verzoeken-api alembic downgrade -1
docker-compose run --rm verzoeken-api alembic upgrade head
```

```bash
docker-compose run --rm instellingen-api alembic downgrade -1
docker-compose run --rm instellingen-api alembic upgrade head
```

```bash
docker-compose run --rm payment-api alembic downgrade -1
docker-compose run --rm payment-api alembic upgrade head
```

```bash
docker-compose run --rm persons-api alembic downgrade -1
docker-compose run --rm persons-api alembic upgrade head
```

## Testing

This projects contains frontend and backend unittests and end 2 end tests. All these tests are run in the gitlab ci/cd pipeline. The unittests rune before the containers are created. The end 2 end test runs after deployment to development.

### Frontend unit tests

To start the frontend unit tests goto the desired directory and run

```bash
yarn test
```

### Backend unit tests

There are three possible ways to start the backend tests.

#### 1 Run test directly in vs code

For some context and background information see [https://code.visualstudio.com/docs/python/testing](https://code.visualstudio.com/docs/python/testing)
<br>

First make sure postgres is running and the docker container has run at least once so all necessary settings are set

```
cd ./backend/<api dir>
poetry install
```

Now click f1 and type `>Python: Select Interpreter ` Select the environment that was just installed

Because each backend has it own python environment we need to set the vs code setting Python > Testing: Cwd to the correct directory where you want to test. For example set it to `backend/formulierenbeheer-api`

Then open a test and then click on the beaker icon in vs code for the **Test Explorer** view. There should be a entry Python Tests. Use this to run or debug the tests

#### 2 Run test directly in console

```bash
cd ./backend/<api dir>
poetry install
poetry run pytest -p no:cacheprovider --verbose --cov-branch --cov=. --junitxml=./junit.xml --cov-report=xml:./coverage.xml
```

#### 3 Run tests in docker container

```bash
docker-compose run --rm contactmomenten-api pytest
```

```bash
docker-compose run --rm terugbelverzoeken-api pytest
```

```bash
docker-compose run --rm formulierenbeheer-api pytest
```

```bash
docker-compose run --rm verzoeken-api pytest
```

```bash
docker-compose run --rm instellingen-api pytest
```

```bash
docker-compose run --rm persons-api pytest
```

```bash
docker-compose run --rm payment-api pytest
```

any arguments to pytest can also be passed after this command. For instance:

```bash
docker-compose run --rm <api name>-api pytest -vvsx
```

This runs the tests in extra verbose mode (-v -v), stops at the first error
(-x) and shows anything that was printed to STDOUT (-s). Usefull arguments are:

Get rid of annoying lib warnings written the end of the tests.

```bash
docker-compose run --rm <api name>-api pytest -vvsx --disable-warnings
```

Run tests in specific file.

```bash
docker-compose run --rm <api name>-api pytest /app/<path_of_test_file>/<test_file_name> -vvsx --disable-warnings

For example:
docker-compose run --rm <api name>-api pytest /app/app/api/api_v1/routers/tests/test_contact_moments.py -vvsx --disable-warnings
```

Run specific test in specific file.

```bash
docker-compose run --rm <api name>-api pytest /app/<path_of_test_file>/<test_file_name> -vvsx --disable-warnings -k <test_name>

For example:
docker-compose run --rm <api name>-api pytest /app/app/api/api_v1/routers/tests/test_contact_moments.py -vvsx --disable-warnings -k test_get_contact_moment_list
```

To run all of the tests that run in the CI pipeline, you can run the following
script:

```bash
docker-compose run --rm <api name>-api ./ci.sh
```

### End 2 End tests

To run the end 2 end test in vs code see ./e2e/README.md

## Logging

```bash
docker-compose logs
```

Or for a specific service:

```bash
docker-compose logs -f name_of_service # frontend|api|db
```

### Query logging

To enable database query logging, set the environment variable `DATABASE_ECHO`
to something Python considers to be `True`:

```bash
echo 'DATABASE_ECHO=1' >> .env
```

Then restart the API container and you'll see every database query as it's
executed.

## novu

This project uses novu for notifications this service will automaticly start with the main stack.

### setup

Copy the `.env.example` file to `.env`
You can do this with the following command `cp .env.example .env`
Don't forget to make a secret string for the jwt token in the `.env` file

### urls

The following urls have been made for the novu service.

|URL| Component |
|---|---|
|https://novu.vcap.me|Novu management|
|https://novu-api.vcap.me|Novu API|
|https://novu-ws.vcap.me|Novu Web socket|
|https://novu-widget.vcap.me|Novu Widget|
|https://novu-embed.vcap.me|Novu Embed|

## License

This project is licensed under the European Union Public Licence v. 1.2 - see the LICENSES/EUPL1-2.txt file for details
