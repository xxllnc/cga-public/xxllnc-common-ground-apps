.. SPDX-FileCopyrightText: xxllnce B.V.
..
.. SPDX-License-Identifier: EUPL-1.2


Contributing
============

Thank you for your support and effort in helping this project grow. We welcome
all contributors to our codebase. And by reading the following guidelines, we
are hoping you get al the information you need to write beautiful and working
code.



Docker
------

The project contains a docker-compose.yml and Dockerfile, which means
you can startup your project by running the known docker-compose commands. In
short:

::

  docker-compose up -d



Code location
-------------

This code is hosted on gitlab, please see the README.md for the exact
location of this specific codebase. We use `git` as our repository software.

Code quality
------------

Along with readable code, we like extensible documentation, unit tests and
a solid understanding of the location of our code(PEP8).

In short:

- Use spaces instead of tabs
- Make sure your maximum line width does not exceed 79 characters
- Write a test for every function
- Write your documentation



Committing code
---------------

After you followed our code quality guidelines above, you are ready for a code
review from someone else. We assume you already forked the project to your
own private repository.

Commit message
^^^^^^^^^^^^^^

Please rebase your changes to a single commit describing your change. Make
sure we know whether it is a feature or a bugfix, or simply, follow the given
commit message template `.git-commit-template`. You can set it up as a default
by running:

::

  git config commit.template .git-commit-template

Merge request
^^^^^^^^^^^^^

Now that you have a solid commmit message describing the change you've made
to the software, it is time to create a merge request, so other people can
review your changes. Use the "Merge request" button from within gitlab to do
just that.

People will think something about your implementation, but they will allways
make sure you know that to do with it. The rules below describe the checks
a developer does when reviewing your code.

Code review rules
^^^^^^^^^^^^^^^^^

**Code layout**:

- Code coverage is at the least higher than before the commit, or reason is
  given. Run ci.sh to see the current code coverage
- README.md is updated, when usage changes
- Code contains proper Python type hinting

**Functionality**:

Code review template
^^^^^^^^^^^^^^^^^^^^

When giving feedback to fellow developers, using more words is always better
than none.
**Examples:**

- I like your solution, but instead of ` a = a + 1 ` you could also
  increment the variable without extra code by doing: ` a += 1 ` .
- Nice work! I really like what you did by refactoring this piece of code
- Missing documentation: at function `addProject` you forgot to mention
  the required parameter `project`

Please make sure you approve the merge request at the top when you did not
find any showstoppers (thumbs down), this way the other party can decide what
to do next: leave it this way, or improve and process the given suggestions
and showstoppers.


reporting bugs
^^^^^^^^^^^^^^^^^^^^
if a bug is related to security send a mail to security@xxllnc.nl
Otherwise send a mail to productmanagement@xxllnc.nl
That incudes the following if available:
- What application the bug is in
- Why do you think it is a bug
- Steps to reproduce
- What the impact of the bug is
