{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "mijnomgeving-app.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "mijnomgeving-app.labels" -}}
helm.sh/chart: {{ include "mijnomgeving-app.chart" . }}
{{ include "mijnomgeving-app.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "mijnomgeving-app.selectorLabels" -}}
app.kubernetes.io/name: {{  .Values.apps.mijnomgeving.name }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "mijnomgeving-app.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "fullname" (list $ .Values.apps.mijnomgeving.name)) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}
