{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "formuliereninwoner-app.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "formuliereninwoner-app.labels" -}}
helm.sh/chart: {{ include "formuliereninwoner-app.chart" . }}
{{ include "formuliereninwoner-app.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "formuliereninwoner-app.selectorLabels" -}}
app.kubernetes.io/name: {{  .Values.apps.formuliereninwoner.name }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "formuliereninwoner-app.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "fullname" (list $ .Values.apps.formuliereninwoner.name)) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}
