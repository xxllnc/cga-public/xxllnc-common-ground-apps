{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "formulierenbeheer-app.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "formulierenbeheer-app.labels" -}}
helm.sh/chart: {{ include "formulierenbeheer-app.chart" . }}
{{ include "formulierenbeheer-app.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "formulierenbeheer-app.selectorLabels" -}}
app.kubernetes.io/name: {{  .Values.apps.formulierenbeheer.name }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "formulierenbeheer-app.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "fullname" (list $ .Values.apps.formulierenbeheer.name)) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}
