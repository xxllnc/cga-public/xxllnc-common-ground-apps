{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "root-overview-page.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "root-overview-page.labels" -}}
helm.sh/chart: {{ include "root-overview-page.chart" . }}
{{ include "root-overview-page.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "root-overview-page.selectorLabels" -}}
app.kubernetes.io/name: {{  .Values.apps.rootOverviewPage.name }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "root-overview-page.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "fullname" (list $ .Values.apps.rootOverviewPage.name)) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}
