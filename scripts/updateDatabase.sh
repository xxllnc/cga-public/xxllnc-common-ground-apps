#!/bin/bash

db_user=$1
db_host=$2
application=$3

if [[ $# -ne 3 ]]; then
    echo "Illegal number of parameters" >&2
    exit 2
fi

#####################################################
#  Main
#
#  Main script start here
#####################################################
echo "This script is meant to be run by the helm to update all database instances"

  for line in $(psql -P pager=off -U ${db_user} -h ${db_host} -d postgres -t -c "SELECT datname FROM pg_database;")
  do
    echo "Updating database for ${application}"
    db_name="${line//[$'\t\r\n ']}"
    if [[ ($db_name == "$application"*) || ($db_name == "template_$application"*) ]]; then
        # alembic uses the DATABASE_URL env varible to determine what database is updated
        DATABASE_URL="postgresql://${db_user}:${PGPASSWORD}@${db_host}/${db_name}"
        echo "Updating database ${db_name}"
        alembic upgrade head
        if test $? != "0"; then
            echo "database update failed"
        else
            echo "database update successful"
        fi
    fi
done
