#!/bin/sh

# Recreate config file
rm -rf ./env-config.js
touch ./env-config.js

# Add assignment 
echo "window.env = {" >> ./env-config.js

# REACT_APP enviroment variabels are written to the env-config.js file
# Each line represents key=value pairs
printenv | grep REACT_APP | \
while read line
do
  # Split env variables by character `=`
  if printf '%s\n' "$line" | grep -q -e '='; then
    varname=$(printf '%s\n' "$line" | sed -e 's/=.*//')
    value=$(printf '%s\n' "$line" | sed -e 's/^[^=]*=//')
  fi
  
  # Append configuration property to JS file
  echo "  \"$varname\": \"$value\"," >> ./env-config.js
done

echo "}" >> ./env-config.js