export ALLOWED_LICENSES_BACKEND="
Apache Software License;
BSD License;
European Union Public Licence 1.2 (EUPL 1.2);
GNU Library or Lesser General Public License (LGPL);
MIT;
MIT License;
Mozilla Public License 2.0 (MPL 2.0);
Python Software Foundation License;
GNU General Public License v2 or later (GPLv2+);
BSD;
GNU General Public License v3 (GPLv3);
Apache License 2.0;
Apache License, Version 2.0;
CC0 1.0 Universal (CC0 1.0) Public Domain Dedication;
ISC;
LGPL
"

export ALLOWED_LICENSES_FRONTEND="
AGPL-3.0;
Apache-2.0;
BSD*;
BSD-2-Clause;
BSD-3-Clause;
CC-BY-3.0;
CC-BY-4.0;
CC0-1.0;
EUPL-1.2;
ISC;
LGPL-3.0;
MIT;
MPL-2.0;
ODC-By-1.0;
Public Domain;
Python-2.0;
Unlicense;
WTFPL;
0BSD
"
