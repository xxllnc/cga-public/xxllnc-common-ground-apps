# CI/CD

This project uses gitlab CI/CD for its automation. Its devided up in a number of steps these steps install build, test audit and deploy the apps.

## Install

In the install step the dependencies are installed of the application. For the backend a seprate contaier is used that has poetry pre installed. This image is maintained by the CGA team. For the front-end the image node:16 is used. This is a public image from the dockerhub.

## QA

In the QA stage the applications are checked for any licences that are not allowed. This works by way of a whitelist thats defined in `scripts/load-allowed-licenses.sh`. A list of the used licenses can be found as a Artifact in the pipeline. If a license is found with a licence thats not on the whitelist the job fails and the app will not deploy.

## Test

In this stage all the unittest and linting is run on the apps. This generates a artifact with the test report. This artifact can be found in the pipeline. If any test fails the pipeline will stop and the app will not deploy. This stage is also responsible for linting the helm charts

## Audit

This stage audits the app for any security issues found in the CVE database if any issue is found the pipeline fails there is a whitelist for issues that have no fix yet.

## Build

This stage builds the application using the individual build tools per type off application.

## Package

This stage packages the application in its dokcer container and tags it with its version number. Depending on what branch the pipeline is run the development or main tag is added to the container. This container is pushed to the GitLab container registry for later use.

## Tag latest container

This job is run when there are no changes to the app but a new tag needs to be added to the docker container. This stage pulles the latest and gives it a new tag with the new version number.

## Package helm

This stage is responsible for packaging and uploading the helm charts to the package registry of gitlab

## Deploy

this stage triggers the deploy pipeline in a other project

## E2E

The end to end has two stages one to install the tests and one to run it. The tests make a test report if anything goes wrong.
