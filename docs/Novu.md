# Configure Novu server

This documentation describes how to setup the novu server for locale development.

# Register account in Novu Dashboard

Go to https://novu.vcap.me and click on `Sign Up` below the sign in button. During registertration you will be asked to provide your full name, email and password. After a successful registration you will be asked to give the application a name. You can use any name during development setup.

If you get an error during the submit of the `Sign up` then make sure that the certificat on https://novu-api.vcap.me/v1/auth/register is accepted

# Setup email provider

After the registration is completed you will be directed to the quickstart page on https://novu.vcap.me/quickstart.

First setup a email provider so we can send emails.
Click on `Configure now` below the section `Connect your delivery providers`.
Then click on the `Connect` button in the `SendGrid` tab to configure sendgrid.

You can use the development sendgrid api key that can be found in the `BitWarden` vault under the name `SendGrid Demo API key`.

For development you can use the following email address:  
`wouter.bouwmeester@xxllnc.nl`

> **_NOTE:_** The sender email must be a registered email address within the sendgrid account you use. The email address mentioned above is registered within the development account. You can also create your own sendgrid account and provide your own api key and sender email address.

Give the sender a name of choice and click connect to activate the provider.

# Setup a template

Return to https://novu.vcap.me/quickstart and click on `Create Now` below the section `Create your first notification template`

> **_NOTE:_** It is important to realize that regardless of the type of step you would like to setup (email, sms, chat, push), you should always add a `In-App` step in the flow to guarandee that the recipient will see the notification in the `Mijn Berichten` section inside the `Mijn omgevingen` application. (if you dont want this you can leave the `In-App` section out)

To create a template, click on `New`.
Enter a name in `Notification Name` and click on `Create`. Note that this name will be used to `trigger` the template.

In the following promt you will see a note.js code snippet and curl example specific for the just created template. You can copy the curl for example to import the trigger request in postman.

Close the popup and click on `Workflow Editor` to configure the template.

# Add steps in a template

The following steps will result in a template that will send an email and notification in mijn omgevingen.

Drag and drop the `In-App` step in the editor and drop it in the section with the text `Place your next step here`. Then click on the step and click on `Edit Template` in the upper right corner of the editor.

From top to bottom you can now:

- Add a redirect url _i.e/tasks/{{taskId}}_
- Add the notification content _Write your notification content here..._  
  ![alt text](images/notification_content.jpg "Write your notification content here")
- Add a action _+ Add Action_
- Add a new feed _Name your feed_

> **_NOTE:_** The redirect url and new feed are currently not implemented in `Mijn omgevingen`. For the `Mijn berichten` page, the notification content will be used.

Add the following as notification content:  
{{name}}! This is our {{customObject.world}}

After you entered the notification content, click on `Update` to save the template.

The value for `name` and `customObject.world` will be retrieved from the payload of the trigger that Novu receives.

Return to the workflow editor with the `Go Back` button and repeat the previous steps, but now we add the `Email` step. After clicking on `Edit Template` you will see the email template has a `Subject line` and body section.

Add {{name}}! This is our {{customObject.world}} as `Subject line` and save the template with `Update`

# Register a subscriber

To be able to send messages to a person we need to register this person as a subscriber. For testing purposes you should register the following user manual:

```
curl -k --location --request POST 'https://novu-api.vcap.me/v1/subscribers' \
--header 'Authorization: ApiKey <YOUR_API_KEY>' \
--header 'Content-Type: application/json' \
--data-raw '{
    "subscriberId": "2352984859",
    "email": "<YOUR_EMAIL_ADDRESS>",
    "firstName": "Merel",
    "lastName": "Kooyman"
}'
```

> **_NOTE:_** <YOUR_API_KEY> kan be found under settings (https://novu.vcap.me/settings)

> **_NOTE:_** This subscriberId is the `aNumber` of a bsn test account we use for bsn 999993847. It is registered with the test person Merel Kooyman. You should enter your own email to be able to receive test emails.

# Send a trigger

Go back to the `Edit Template` page with `Go Back` and click on the `Trigger Snippet` button. Select Curl and copy the content.

Go to `Postman`, import the Curl and update the payload to look like this:

```
{
   payload: {
     sender: 'Xxllnc',
     message: 'This is the notification message',
     name: 'Hello',
     customObject: {
       world: 'World'
     }
   }
}
```

Replace the dummy value behind `subscriberId` with `2352984859` to send the notification to the just registered subscriber.

> **_NOTE:_** Note that the `name` and `customObject` values are specific for the template we just created. The `sender` and `message` values are required for the `Mijn Berichten` page in `Mijn Omgevingen`.

Update the url to https://novu-api.vcap.me/v1/events/trigger before sending the trigger.

## Curl

Instead of postman curl can also be used to send a trigger. Make sure the correct api-key, from settings, is used

```
curl -k --location --request POST 'https://novu-api.vcap.me/v1/events/trigger' \
     --header 'Authorization: ApiKey <YOUR_API_KEY>' \
     --header 'Content-Type: application/json' \
     --data-raw '{
        "name": "notify1",
        "to" : {
            "subscriberId": "2352984859"
        },
        "payload": {
          "sender": "Xxllnc",
          "message": "This is the notification message",
          "name": "Hello",
          "customObject": {
            "world": "World"
          }
        }
    }'
```

# View notifications in Mijn Omgevingen

Go to `Settings` https://novu.vcap.me/settings and click on `Api Keys`.
Copy the value of `Application Identifier` and past this in the `REACT_APP_NOVU_APP_ID` line in the docker-compose.override.yml in the `mijnomgeving-app` service section.

Set REACT_APP_NOVU_APP_ID in docker compose override

Start or restart the `Mijn Omgevingen` application with  
`$ docker-compose --profile mijnomgeving up -d`

Open the notification page on https://exxellence.vcap.me/mijnomgeving/notifications.
