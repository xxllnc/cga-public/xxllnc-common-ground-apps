# Formulieren

This document describes how multi-tenancy should work in formulierenbeheer and which things have not yet been implemented.

## Functional description

An administrator or a consultant can create a new formulieren(beheer) app for an organization in the eco-system control-panel. To do this the following steps are taken in the control-panel:

1. The administrator or consultant logs in to the control-panel. For an existing organization: Search the organization and click on the organization to open it.
   - This assumes that there already is a organization. If not, first create the organization.
2. Choose the tab apps.
3. Click on the 'Formulieren app'
   - When formulieren is not shown in the list of apps, choose Aanmaken. The App toevoegen window appears add the app and choose OPSLAAN.
   - Then choose the new created Formulieren app in the list.
4. Choose tab Omgevingen. A list of all active instances appears.
5. To create a new instance for the organization press the button AANMAKEN. The Omgeving aanmaken window appears. Enter values and press OPSLAAN. The Formulieren window appears.
6. Open the new instance. The Formulieren window appears.
7. The next step is to add an unique primary hostname to the instance. Click on button AANMAKEN in the Hosts section. The Alias aanmaken window appears.
8. Use an unique sub-domain like `example.cga.xxllnc.nl` or use an own domain when an own domain is available for the organization. Press OPSLAAN.

These are all the functional steps to create a new instance of formulieren and formulierenbeheer. When this is fully implemented the app should be available in a few minutes and the organization can create new forms and submit forms.

### Provisioner

When a new instance is added in the control-panel this is picked up by the provisioner. The provisioner takes the following steps to actually create the new instance:

1. The organization is added to the organization table in the formulierenbeheer database.
2. The instance url is added to the url table in the formulierenbeheer database.
3. For verzoeken-api, instellingen-api and payment-api a new database is created. The app-instance-id is added as a suffix to the database name where the - sign is replaced by an underscore. For example the name of the settings database for instance 1b6ee52f-dd98-49ba-93c8-43e8624fe57b, becomes settings_1b6ee52f_dd98_49ba_93c8_43e8624fe57b
4. A new ingres instance is added for the instance url

### Add databases manual

At this moment (2022-12-22) the provisioner is not ready yet, the following theps need to be executed to manually add the databses:

1. Open the vpn connection
2. Open DBeaver
3. Create a new sql script
   - Do not expand the prod tree to prevent conflicts when using the templates
   - Use the instance id in the script below:

```
CREATE DATABASE instellingen_<id> WITH TEMPLATE template_instellingen
CREATE DATABASE payments_<id> WITH TEMPLATE template_payments
CREATE DATABASE verzoeken_<id> WITH TEMPLATE template_verzoeken
```

- Press 'Run script' to create the databases

### Add organization in the database

This step is only needed if you created a new organization in Functional description step 1

1. Open the formulierenbeheer api docs for the instance you created
   - For instance `https://example.cga.xxllnc.nl/formulierenbeheer/api/docs`
2. Also open the formulieren beheer app and login to get a jwt token
3. Use this jwt token to Authenticate in the api docs so we can make calls
4. Get the `https://xxllnc.nl/organization_uuid` from the jwt token
5. Go to `Add organization` and add a organization with this id and the name of the organization

## Adding users

The following steps need to be executed to give a user access to the new app instance. I the example below we used the auth0 uaername password login with gmail.com as hostname.

1. Go back to mijn.xxllnc.nl and open the organization you used to create the instance
2. Go to `Authentication` and click on `auth0` and then on `Gebruikers`
3. Click on `Nieuwe gebruiker` select the correct connection type and provide the name and email address
   - The user should now go to gmail and setup a password for the account
4. Now go to Auth0, select the correct tenant and click on `Organization`
5. Search the organization, click on the organization
6. Click on `Members` and click on the user you just created
7. Remove the defaul eco role and add `Xxllnc CGA Users` and `Xxllnc CGA Administrators`

### using the new instance

When all the above steps are done the new instance is available. For this example the url `example.cga.xxllnc.nl` is used as the new instance URL.

First the formulierenbeheer app is opend by going to the URL `https://example.cga.xxllnc.nl/formulierenbeheer`
The frontend is opened. To login with auth0 the auth0Identifier for the organization must be determined. This is done by a call to the control-panel `https://mijn.xxllnc.nl/api/v1/login_info?filter={"emailAddress":"first.last@xxllnc.nl"}`.

After login there is a call to the formulierenbeheer-api to get all forms for the organisation.
The main difference with formulierenbeheer-api and the other api's
(verzoeken-api, instellingen-apy and payments-api) is that formulierenbeheer-api uses one database
where all the forms are stored for all organizations. This is done so it is easy to share and use
forms from other organizations.

To get all the forms of an organization the organization_uuid from the JWT token is used.

From the formulierenbeheer app a form can be opened in the formulierenbeheer app.
When a form is opened the formulieren app makes a call to the active forms end-point.
Main difference with the other end points is that there is no JWT token required.
In the header of that call the organization-url: `https://example.cga.xxllnc.nl` is added.
This should be the exact same URL as the one that is added to the primary host of the app instance
in the control-panel. Based on the organization-url all the forms for that organization are returned

On the same time the form is opened there is a call to the instellingen-api to get the CSS
for the organization. Here the organization-url is added as a URL parameter instead of a header
In the instellingen-api the right db connection is determined based on the organization-url.
The database is uniquely identified by the instance_uuid to get the correct instance-uuid there
is a call to the control-panel to get the instance-uuid based on the primary host that is set in
the control-panel. With the instance-uuid from the control-panel the right db session is created or
selected if it already exists.

After all the fields are filled in, the form is submitted to the verzoeken-api.
Here also the organization-url: `https://example.cga.xxllnc.nl` is added to the header.
On the same way as described for the css the correct instance db is selected.
The verzoeken-api db temporary stores the submitted forms until the form is successfully transferred to the case system back-end. Documents are temporary stored in an S3 container for now the documents
are destroyed after 24 hours

## TODO

Here is a list of the things that need te be done to fully support the above description

1. Provisioner need to be deployed using helm. For the db part there was already a working version
   for contactmomenten. For forms the same principle could be used.
   [See helm chart](https://gitlab.com/xxllnc/zaakgericht/platform/helm-charts/-/blob/50671790abcfcc590731415fdf7ca27ee55a5cf2/charts/xxllnc-cga/templates/_provisioner.tpl)
   The database is created based on a template database. This template database and all other app
   databases are updated by Alembic. The update is done by the [updateDatabse.sh](https://gitlab.com/xxllnc/cga-public/xxllnc-common-ground-apps/-/blob/development/scripts/updateDatabase.sh) script.
2. A new provisioner needs to be added that creates a new ingresss instance so the primary url
   can be used. In auth0 and the control-panel the allowed origins need to be set. In auth0 there are
   wildcards used for _.dev.cga.xxllnc.nl and _.cga.xxllnc.nl. For the control-panel there was [CSMIJ-916](https://xxllnc.atlassian.net/browse/CSMIJ-916). Check the status on the last one.
3. To determine the correct db session now the get app instances endpoint of the control-panel is used. This endpoint gives a list of all app instances and this list is filtered. In [CSMIJ-793](https://xxllnc.atlassian.net/browse/CSMIJ-793) there is a description to change this.
