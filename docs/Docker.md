# Docker compose parameters and override's xxllnc CGA

This document describes the different profiles and commands to start the Common Ground Apps in different modes

## Profiles

These are the different profiles the stack can be started with.

|Profile name|What containers start|
|---|---|
|cm| Starts Contactmomenten and its supporting apps|
|tbv|Starts Terug bel verzoeken and its supporting apps|
|forms|Starts Formulieren and its supporting apps|
|vergader|Starts Vergaderen and its supporting apps|
|mijnomgeving|Starts Mijn Omgeving and its supporting apps|
|all|Starts all app's|
|allapi|Starts all backends|
|allapp|Starts all frontends|

## Overrides

For this project there are 4 overrides to chose from.

|Override|purpose|
|---|---|
|docker-compose-override.yml|Default override to start the apps with all its parameters|
|docker/docker-compose-override-no-build.yml|Only use development images to start the containers|
|docker/docker-compose-override-no-build-frontend.yml|Use development images to start the frontend and still build the API's|
|docker/docker-compose-override-no-build-backend.yml|Build the frontends and start the API's from development images|

## Commands

To start you can use the following commands, you can edit the profile to your desired app or apps

**Be sure to pull first by doing instead of doing `up -d` do `pull`**

* Start all apps and build them
    `docker-compose --profile all up -d`
* Start all apps and build nothing
    `docker-compose -f docker-compose.yml -f docker/docker-compose.override-no-build.yml --profile all up -d`
* Start all the apps and build the frontend
    `docker-compose -f docker-compose.yml -f docker/docker-compose.override-no-build-backend.yml --profile all up -d`
* Start all the apps and build the backend
    `docker-compose -f docker-compose.yml -f docker/docker-compose.override-no-build-frontend.yml --profile all up -d`
