# Configure development environment on Windows using WSL2

This document describes how to set up a `xxllnc-common-ground-apps` development system using
WSL2 in Windows 10.

## Prerequisites

- [Windows Subsystem for Linux, version 2](https://docs.microsoft.com/en-us/windows/wsl/install-win10) (WSL2) is installed
- [Docker](https://docs.docker.com/docker-for-windows/install/) is installed and configured to be enabled in your WSL2 environment
- [Visual Studio Code](https://code.visualstudio.com/) is installed
- NodeJS version 14 (LTS) with npm (and optionally yarn) is installed

## Clone repository

### Linux

It is recommended to work from inside a WSL2 environment.
Perform the following steps to clone the git repository.

From Windows, open a WSL2 prompt. You can do this from powershell, for example
if you have Ubuntu installed:

```
C:\work> ubuntu
```

You can also use the [Windows Terminal](https://www.microsoft.com/nl-nl/p/windows-terminal/9n0dx20hk701) app, which is able to launch WSL2 shells directly.

The easiest way to access Gitlab is by using a separate ssh key inside the WSL2
environment. To generate one run `ssh-keygen` and add it to your GitLab account
(it's under "Settings"):

```bash
$ ssh-keygen
$ cat .ssh/id_rsa.pub
```

You can now clone the `contactmomenten` repository:

```bash
$ git clone git@gitlab.com:xxllnc/cga/contactmomenten.git
```

## Configure IDE

### Linux

Open VSCode from inside WSL2.

```bash
$ code xxllnc-common-ground-apps
```

Inside vscode, open a terminal and create a Python virtual environment using
poetry

If poetry is not installed then install it,
see https://python-poetry.org/docs/master/#installation for details.
make sure both python and pip are available in PATH

```bash
$ curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python -
```

or replace python with python3 if the python alias is not available

```bash
$ curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python3 -
```

Then install alle the dependencies for each project

```bash
$ cd backend/<api>
$ poetry install
```

Or if the dependencies are already installed, then update with:

```bash
$ cd backend/<api>
$ poetry update
```

Next to select the python interperter in vs code open menu: File -> Preferences -> settings
Add the folder "~/.cache/pypoetry/virtualenvs" to the (remote [WSL:Ubuntu]) setting "Python: Venv path"

Then press Ctrl+shift+p en type Python: Select Interpreter to select the corresponding virtual enviroment.
This can also be used to switch to an other .venv in vs code

For more information about poetry see documentation on https://python-poetry.org/docs
Information about installing new dependencies: https://python-poetry.org/docs/basic-usage/#installing-dependencies
Updating dependencies: https://python-poetry.org/docs/basic-usage/#updating-dependencies-to-their-latest-versions
Removing dependencies: https://python-poetry.org/docs/cli/#remove

## Certificates

### Linux

The development contains a script that uses `mkcert` to create a private CA and
certificates to enable https to the development environment. To make it run
in WSL2:

```bash
$ sudo wget -O /usr/local/bin/mkcert https://github.com/FiloSottile/mkcert/releases/download/v1.4.3/mkcert-v1.4.3-linux-amd64
$ sudo chmod 755 /usr/local/bin/mkcert
$ cd dev/certs
$ chmod 777 create-certs.sh
$ ./create-certs.sh
$ cd ../..
```

The CA certificate is in `dev/certs/ca.pem` -- load this into your OS (or browser)
CA list to get rid of the "This is insecure!" warnings.

For FireFox a description of how to trust the generated certificate can be found here: https://javorszky.co.uk/2019/11/06/get-firefox-to-trust-your-self-signed-certificates/

### Windows

Download mkcert from https://github.com/FiloSottile/mkcert/releases
And run from within the certs dir

```powershell
PS C:\Work\dev\certs> mkcert -cert-file dev.crt -key-file dev.key "*.vcap.me"
```

And copy the rootCA.pm as ca.pem to the dev\certs folder

For FireFox a description of how to trust the generated certificate can be found here: https://javorszky.co.uk/2019/11/06/get-firefox-to-trust-your-self-signed-certificates/

# Frontend

## NodeJS

Standard node 10 is installed in ubuntu 20 under wsl. Node needs to be update to version 14. Follow these steps:

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
```

This will install the nvm script to your user account. To use it, you must first source your .bashrc file:

```bash
source ~/.bashrc
```

Now, you can ask NVM which versions of Node are available:

```bash
nvm list-remote
```

And remember the latest v14 version, and install it with

```bash
nvm install v14.7.3
```

check if the latest version is installed correctly

```bash
node --version
```

### start frontend outside of docker-container

To start the frontend make add a new .env file and copy the contents of the .env.example in it
then use yarn or npm to install and start the app

```bash
$ yarn install
```

```bash
$ yarn start
```

## Start

You can now start the containers:

```bash
$ docker-compose build
$ docker-compose up -d
```

Make sure to update the db, the first time the app is started

```bash
docker-compose run --rm contactmomenten-api alembic upgrade head
```

The contactmomenten should now be up and running!

Open some of the available url's to confirm the services are running -- see
[README.md](../README.md) in the root of this project for more:

- https://exxellence.vcap.me/contactmomenten
- https://redis.vcap.me

## Best practices

Some best practices that can make life a little easier.

### SSH agent

To interact with GitLab an SSH key is required and to set up a connection the
password for the SSH key is required.

Entering the password every time when interacting with GitLab can become
annoying fast. To overcome this, you can set up an SSH agent which will store
your key and provides it to `ssh` as needded.

It will ask for your password once (when opening your first shell prompt).

To enable the SSH agent automatically, run:

```bash
$ sudo apt-get install keychain
$ echo 'eval $(keychain --eval --agents ssh id_rsa)' >> ~/.bashrc
```

(note the double `>>`! Don't use a single `>` or you'll overwrite the contents
of your `.bashrc` instead of appending to it)

Now when you start a shell in your WSL2 environment an SSH agent is started
and it will ask for your password only once. It will use the stored key each
time when interacting with GitLab, until your WSL2 VM is restarted (including
a full reboot of the Windows host).
