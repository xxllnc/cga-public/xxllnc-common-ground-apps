import { BrowserContext, Page } from '@playwright/test'
import { getSubDomainUrl, test, xxllncUser, zaakstadUser } from '../e2e_tests'
import { openAppAndLogin } from '../partial-tests'
import { testConfig } from '../playwright.config'

const apps = [
  { url: `${getSubDomainUrl('')}formulierenbeheer/`, user: xxllncUser, pageTitle: 'xxllnc formulieren' },
  { url: `${getSubDomainUrl('zaakstad')}formulierenbeheer/`, user: zaakstadUser, pageTitle: 'xxllnc formulieren' }
]
apps.forEach(app => {

  test.describe.parallel(`Log in to app ${app.url}`, () => {

    let context: BrowserContext
    let page: Page

    test.beforeAll(async ({ browser }) => {
      context = await browser.newContext(testConfig.options)
      page = await context.newPage()
    })

    test.afterAll(async () => {
      await context.close()
    })

    test('See if app can be opend and login works @PRODUCTION', async ({ }) => {

      await openAppAndLogin({ page, appUrl: app.url, pageTitle: app.pageTitle, user: app.user })
    })

  })
})