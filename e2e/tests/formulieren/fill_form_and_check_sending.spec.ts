import { BrowserContext, expect, Page, test } from '@playwright/test'
import { baseUrl, xxllncUser } from '../../e2e_tests'
import {
  logOff, openAppAndLogin, setFilterAndGetCountFound
} from '../../partial-tests'
import { testConfig } from '../../playwright.config'
import { locators, randomString } from '../../utils'

const { options } = testConfig
const app = 'formulierenbeheer'
const appUrl = `${baseUrl}${app}/`
const myQuestion = `Dit is mijn vraag ${randomString()}`
const formName = 'Stel uw vraag'

test.describe.serial('Fill form, send and check sending', () => {
  let page: Page
  let pageInwoner: Page
  let context: BrowserContext

  test.beforeAll(async ({ browser }) => {
    context = await browser.newContext(options)
    page = await context.newPage()
    await openAppAndLogin({
      page,
      appUrl,
      pageTitle: 'xxllnc formulieren',
      user: xxllncUser
    })
  })

  test.afterAll(async () => {
    await logOff({ page, appUrl })
    await context.close()
  })

  test('Search form', async ({ }) => {
    await page.locator('div[role="button"]:has-text("Privé")').click()
    await Promise.all([
      page.waitForLoadState('domcontentloaded'),
      page.waitForLoadState('networkidle')
    ])
    await setFilterAndGetCountFound({ page, filterText: formName })
  })

  test('Open form', async ({ }) => {
    [pageInwoner] = await Promise.all([
      page.waitForEvent('popup'),
      page.getByRole('row', { name: new RegExp('Selecteer rij Stel uw vraag xxllnc Actief.*') }).getByRole('button').click()
    ])
  })

  test('Fill the question', async ({ }) => {
    await pageInwoner.locator('input[name="data\\[deVraag\\]"]').click()

    await pageInwoner.locator('input[name="data\\[deVraag\\]"]').fill(myQuestion)
  })

  test('Go to the overview page', async ({ }) => {
    await pageInwoner.locator('text=Volgende').click()
  })

  test('Check the confirmation box', async ({ }) => {
    await pageInwoner.locator('input[name="data\\[confirmationCheckBox\\]"]').check()
  })

  test('Send the form', async ({ }) => {
    await pageInwoner.locator('button:has-text("Versturen")').click()
  })

  // TODO: Turn on again when a delay to show the next screen has been implemented
  // test('Check the sending message 1 form', async ({ }) => {
  //   // Check text=Versturen geslaagd
  //   await Promise.all([
  //     await expect(pageInwoner.locator('text=Versturen geslaagd')).toBeVisible()
  //   ])
  // })

  test('Check the sending message 2 form', async ({ }) => {
    await expect.poll(async () => {
      const h1Loc = pageInwoner.locator('text=Bedankt!')
      return h1Loc.isVisible()
    }, {
      // Probe, wait 1s, probe, wait 2s, probe, wait 10s, probe, wait 10s, probe, .... Defaults to [100, 250, 500, 1000].
      intervals: [1_000],
      timeout: 80_000
    }).toBe(true)

    await expect(pageInwoner.locator(' text=Bedankt!')).toBeVisible()
  })

  test('Check the sending message 3 form', async ({ }) => {
    await expect(pageInwoner.locator('text=Het indienen van het formulier is succesvol verlopen.')).toBeVisible()
  })

  test('Close the form', async ({ }) => {
    await pageInwoner.locator('text=Afsluiten').click()
    await expect(pageInwoner).toHaveURL('https://xxllnc.nl')
  })

  test('Goto the registration of requests', async ({ }) => {
    await Promise.all([
      await page.waitForLoadState(),
      await page.locator('[data-testid="PostAddIcon"] path >> nth=0').click(),
    ])
  })

  test('Sort on registration date descending', async ({ }) => {
    await page.locator('text=Registratie datum').click()
    await page.locator('text=Registratie datum').click()
  })

  test('Check the sending registrations for 1 minute or until found', async ({ }) => {
    await expect.poll(async () => {
      await page.locator('text=Tonen >> nth=0').click()
      const found = await page.locator('text=' + myQuestion).isVisible()
      await page.locator(locators.iconByDataTestid('ArrowBackIcon', '1')).click()
      return found
    }, {
      // Probe, wait 1s, probe, wait 2s, probe, wait 10s, probe, wait 10s, probe, .... Defaults to [100, 250, 500, 1000].
      intervals: [1_000, 2_000, 10_000],
      timeout: 60_000
    }).toBe(true)
  })

})

