import { BrowserContext, expect, Page, test } from '@playwright/test'
import { format, subDays } from 'date-fns'
import { baseUrl, getSubDomainUrl, xxllncUser, zaakstadUser } from '../../e2e_tests'
import {
  checkIfFormExists, deleteFormFromOverviewScreen,
  logOff, openAppAndLogin, setFilterAndExpectOneItemFound, setFilterAndGetCountFound
} from '../../partial-tests'
import { testConfig } from '../../playwright.config'
import { locators, openSettingsFormPage } from '../../utils'

const { shortTimeout, options, clickDelay, longTimeout } = testConfig
const formNameOriginal = 'Aanvraag parkeerplaats inwoner'
const date = format(subDays(new Date(), 1), 'yyyy-MM-dd HH:mm:ss:SSS')
const formNameClone = `${formNameOriginal} ${date}`
const appUrl = `${baseUrl}formulierenbeheer/`
const appUrlZaakstad = `${getSubDomainUrl('zaakstad')}formulierenbeheer/`

test.describe.serial('Share form with other organization', () => {
  let pageUser: Page
  let pageOwner: Page
  let context: BrowserContext

  test.beforeAll(async ({ browser }) => {
    context = await browser.newContext(options)
    pageOwner = await context.newPage()
    pageUser = await context.newPage()

    await openAppAndLogin({
      page: pageOwner,
      appUrl,
      pageTitle: 'xxllnc formulieren',
      user: xxllncUser
    })

    await openAppAndLogin({
      page: pageUser,
      appUrl: appUrlZaakstad,
      pageTitle: 'xxllnc formulieren',
      user: zaakstadUser
    })

    await pageOwner.locator('div[role="button"]:has-text("Privé")').click()
    await expect(pageOwner).toHaveURL(new RegExp('.*filter=%7B%7D&order=ASC&page=1&perPage=10&sort=name'))
  })

  test.afterAll(async ({ }) => {
    // Clean up: remove the form
    await deleteFormFromOverviewScreen({ page: pageOwner, formName: formNameClone })

    await logOff({ page: pageUser, appUrl: appUrlZaakstad })
    await logOff({ page: pageOwner, appUrl })
    await context.close()
  })

  test('Reset Openbaar filter in tab EDITOR organization', async ({ }) => {
    await pageUser.locator('div[role="button"]:has-text("Privé")').click()
    await expect(pageUser).toHaveURL(new RegExp('.*filter=%7B%7D&order=ASC&page=1&perPage=10&sort=name'))
  })

  test('Search original form in tab OWNER', async ({ }) => {
    await pageOwner.locator('div[role="button"]:has-text("Privé")').click()
    await Promise.all([
      pageOwner.waitForLoadState('domcontentloaded'),
      pageOwner.waitForLoadState('networkidle')
    ])
    await setFilterAndGetCountFound({ page: pageOwner, filterText: formNameOriginal })
  })

  test('Create clone in tab OWNER', async ({ }) => {
    await pageOwner.locator('[data-testid="ContentCopyIcon"]').first().click()
    await pageOwner.locator('input[name="name"]').fill(formNameClone)
    await pageOwner.locator('text="Actief" >> nth=0').click({ delay: clickDelay })
    await expect(pageOwner.locator('#status_ACTIVE')).toBeChecked()
    await pageOwner.locator('input[name="publishDate"]').fill(format(subDays(new Date(), 1), 'yyyy-MM-dd'))
    await pageOwner.locator('input[name="private"]').click({ delay: clickDelay })
    await expect(pageOwner.locator('input[name="private"]')).not.toBeChecked()
    await pageOwner.locator('text=Opslaan').click()
    await expect(pageOwner.locator('text=Formulier is toegevoegd')).toBeVisible({ timeout: longTimeout })

    await expect(pageOwner.locator('text=Formulier is toegevoegd')).toBeHidden({ timeout: longTimeout })

    await checkIfFormExists({ page: pageOwner, formName: formNameClone })
  })

  test('Open settings nr 1 in tab OWNER', async ({ }) => {
    await openSettingsFormPage({ page: pageOwner, formName: formNameClone })
  })

  test('Check visibility nr 1 xxllnc in tab OWNER', async ({ }) => {
    await expect(pageOwner.locator('td:has-text("xxllnc")')).toBeVisible()
  })

  test('Check visibility nr 1 OWNER in tab OWNER', async ({ }) => {
    await expect(pageOwner.locator('td:has-text("OWNER")')).toBeVisible()
  })

  test('Check not visible nr 1 Zaakstad in tab OWNER', async ({ }) => {
    await expect.soft(pageOwner.locator('td:has-text("Zaakstad")'),
      'Zaakstad should not be here. Has the previous test gone wrong maybe?').toBeHidden({ timeout: shortTimeout })
  })

  test('Check nr 2 disabled In gebruik nemen in tab OWNER', async ({ }) => {
    await expect(pageOwner.locator(locators.fieldByAriaLabel('In gebruik nemen', '1'))).toBeDisabled()
  })

  test('Check nr 2 in tab OWNER enabled Instellingen', async ({ }) => {
    await expect(pageOwner.locator(locators.fieldByAriaLabel('Instellingen', '1'))).toBeEnabled()
  })

  test('Start sharing the form on tab OWNER', async ({ }) => {
    await pageOwner.locator('text=Delen').click()
  })

  test('Select the organization zaakstad', async ({ }) => {
    await pageOwner.locator('input[role="combobox"]').click()
    await pageOwner.locator('input[role="combobox"]').fill('zaak')
    await pageOwner.locator('ul[role="listbox"]:has-text("Zaakstad")').click()
  })

  test('Select the role schrijfrechten', async ({ }) => {
    await expect(pageOwner.locator('button:has-text("Lees rechten")')).toBeVisible()
    await pageOwner.locator('button[aria-label="selecteer Deel opties"] >> nth=1').click({ delay: clickDelay })

    await pageOwner.locator('//li[.//text()="schrijf rechten"]').nth(1).click()
    await pageOwner.locator('text=Klaar').click()
  })

  test('Check visibility nr 2 xxllnc in tab OWNER', async ({ }) => {
    await expect(pageOwner.locator('td:has-text("xxllnc")')).toBeVisible()
  })

  test('Check visibility nr 2 xxllnc OWNER in tab OWNER', async ({ }) => {
    await expect(pageOwner.locator('td:has-text("OWNER")')).toBeVisible()
  })

  test('Check visibility Zaakstad in tab OWNER', async ({ }) => {
    await expect(pageOwner.locator('td:has-text("Zaakstad")')).toBeVisible()
  })

  test('Check visibility Zaakstad EDITOR in tab OWNER', async ({ }) => {
    await expect(pageOwner.locator('td:has-text("EDITOR")')).toBeVisible()
  })

  // Back to EDITOR organization
  test('Search and open settings nr 1 in EDITOR tab', async ({ }) => {
    await setFilterAndExpectOneItemFound({ page: pageUser, filterText: formNameClone })
    await openSettingsFormPage({ page: pageUser, formName: formNameClone })
  })

  test('Check visibility xxllnc in EDITOR tab', async ({ }) => {
    await expect(pageUser.locator('td:has-text("xxllnc")')).toBeVisible()
  })

  test('Check visibility xxllnc OWNER in EDITOR tab', async ({ }) => {
    await expect(pageUser.locator('td:has-text("OWNER")')).toBeVisible()
  })

  test('Check visibility Zaakstad in EDITOR tab', async ({ }) => {
    await expect(pageUser.locator('td:has-text("Zaakstad")')).toBeVisible()
  })

  test('Check visibility Zaakstad EDITOR in EDITOR tab', async ({ }) => {
    await expect(pageUser.locator('td:has-text("EDITOR")')).toBeVisible()
  })

  test('Check in EDITOR tab disabled In gebruik nemen', async ({ }) => {
    await expect(pageUser.locator(locators.fieldByAriaLabel('In gebruik nemen', '1'))).toBeDisabled()
  })

  test('Check in EDITOR tab enabled Instellingen', async ({ }) => {
    await expect(pageUser.locator(locators.fieldByAriaLabel('Instellingen', '1'))).toBeEnabled()
  })

  test('Remove form from organization', async ({ }) => {
    await pageUser.locator('text=Instellingen').click()

    await pageUser.locator('text=Verwijderen').click()

    await pageUser.locator('text=Bevestigen').click()
    await expect(pageUser).toHaveURL(/.*formulierenbeheer\/share/)
  })

  test('Search and open settings nr 2 in EDITOR tab', async ({ }) => {
    await pageUser.locator(locators.iconByDataTestid('DescriptionIcon', '1')).click()
    await expect(pageUser).toHaveURL(/.*\/formulierenbeheer\/forms.*/)

    await setFilterAndExpectOneItemFound({ page: pageUser, filterText: formNameClone })
    await openSettingsFormPage({ page: pageUser, formName: formNameClone })
  })

  test('Check visibility nr 2 xxllnc in EDITOR tab', async ({ }) => {
    await expect(pageUser.locator('td:has-text("xxllnc")')).toBeVisible()
  })

  test('Check visibility nr 2 xxllnc OWNER in EDITOR tab', async ({ }) => {
    await expect(pageUser.locator('td:has-text("OWNER")')).toBeVisible()
  })

  test('Check not visible nr 2 Zaakstad in EDITOR tab', async ({ }) => {
    await expect(pageUser.locator('td:has-text("Zaakstad")')).toBeHidden({ timeout: shortTimeout })
  })

  test('Search and open settings nr 2 in tab OWNER', async ({ }) => {
    await pageOwner.locator(locators.iconByDataTestid('DescriptionIcon', '1')).click()
    await expect(pageOwner).toHaveURL(/.*\/formulierenbeheer\/forms/)

    await setFilterAndExpectOneItemFound({ page: pageOwner, filterText: formNameClone })
    await openSettingsFormPage({ page: pageOwner, formName: formNameClone })
  })

  test('Check not visible Zaakstad in tab OWNER', async ({ }) => {
    await expect(pageOwner.locator('td:has-text("Zaakstad")')).toBeHidden({ timeout: shortTimeout })
  })
})