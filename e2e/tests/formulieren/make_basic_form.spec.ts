import { BrowserContext, expect, Page, test } from '@playwright/test'
import { format, subDays } from 'date-fns'
import { authorOrganization, baseUrl, xxllncUser } from '../../e2e_tests'
import { checkIfFormExists, createNewForm, deleteFormFromOverviewScreen, logOff, openAppAndLogin } from '../../partial-tests'
import { testConfig } from '../../playwright.config'
import { fillValueAndCheckIfFieldIsSet, fixMoreThanOneDialog, locators, openEditFormPage } from '../../utils'

const app = 'formulierenbeheer'
const appUrl = `${baseUrl}${app}/`
const date = format(subDays(new Date(), 1), 'yyyy-MM-dd HH:mm:ss:SSS')
const { options, clickDelay, longTimeout } = testConfig

test.describe.serial('Make basic form', () => {

  const formName = `Basic form ${date}`
  let context: BrowserContext
  let page: Page

  test.beforeAll(async ({ browser }) => {
    context = await browser.newContext(options)
    page = await context.newPage()
    await openAppAndLogin({
      page,
      appUrl,
      pageTitle: 'xxllnc formulieren',
      user: xxllncUser
    })
  })

  test.afterAll(async () => {
    // Clean up form
    await deleteFormFromOverviewScreen({ page, formName })

    await logOff({ page, appUrl })
    await context.close()
  })

  test('Create basic form', async ({ }) => {
    await expect(page.locator(locators.text(authorOrganization)).first()).toBeVisible()

    await createNewForm({ page, formName })

    await checkIfFormExists({ page, formName })

    await openEditFormPage({ page, formName })
  })

  test('Fix more then one dialog', async () => {
    const firstPage = page.locator(locators.text('Pagina 1')).first()
    await expect(firstPage).toBeVisible()

    firstPage.hover()

    const modal = page.locator('[aria-label="Edit button\\. Click to open component settings modal window"]').first()
    await expect(modal).toBeVisible()
    await modal.click()

    await fixMoreThanOneDialog(page, modal)

    await page.locator('div[role="dialog"] button:has-text("Opslaan")').click()

  })

  test('Add text field', async () => {
    await page.dragAndDrop(
      'text=Tekstveld',
      '.builder-components.drag-container.formio-builder-components',
      {
        targetPosition: {
          x: 0,
          y: 0
        }
      }
    )

    const label = page.locator('input[name="data\\[label\\]"]').first()
    await expect(label).toBeVisible()
    await fillValueAndCheckIfFieldIsSet({ fieldLocator: label, value: 'Invoer' })

    await page.locator('div[role="dialog"] >> text=Opslaan').click()
  })

  test('Save form', async () => {
    await page.locator('menu [aria-label="Opslaan"]').click()

  })

  test('Check message Formuliergegevens zijn opgeslagen to be visible', async () => {
    await expect(page.locator('text=Formuliergegevens zijn opgeslagen')).toBeVisible({ timeout: longTimeout })
    await expect(page.locator('text=Formuliergegevens zijn opgeslagen')).toBeHidden({ timeout: longTimeout })
  })

  test('Open form preview', async () => {
    await page.locator('[aria-label="Voorbeeld"]').click()
  })

  test('Check Stap 1 van 2 to be visible', async () => {
    await page.waitForLoadState()
    await expect(page.frameLocator('#preview').locator('text=Stap 1 van 2 >> nth=0')).toBeVisible()
  })

  test('Close preview', async () => {
    await page.locator('[aria-label="Close"]').click({ delay: clickDelay })
  })
})
