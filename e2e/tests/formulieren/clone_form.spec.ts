import { BrowserContext, expect, Page, test } from '@playwright/test'
import { format, subDays } from 'date-fns'
import { baseUrl, xxllncUser } from '../../e2e_tests'
import {
  deleteFormFromOverviewScreen,
  logOff, openAppAndLogin,
  setFilterAndReturnTrueIfFound
} from '../../partial-tests'
import { testConfig } from '../../playwright.config'
import { locators } from '../../utils'

const { options, longTimeout, superLongTimeout } = testConfig
const app = 'formulierenbeheer'
const appUrl = `${baseUrl}${app}/`

const formNameOrigin = 'Ziek en/of beter melden werknemer'
const urlStrSearch = `${'.*forms\\?filter=%7B%22q%22%3A%22' + encodeURIComponent(formNameOrigin) +
  '%22%7D&order=ASC&page=1&perPage=10&sort=name'}`

const date = `${format(subDays(new Date(), 1), 'yyyy-MM-dd HH:mm:ss:SSS')}`
const formNameClone = `${formNameOrigin + ' ' + date}`
const formNameCloneSlug = `${formNameClone.replace(/[:|+|' '|\/]/g, '-')}`
const urlStrConEditMode = `${'(.*)formioeditor\\?id=(.*)&slug=' + formNameCloneSlug + '&active=true'}`

const dateSick = `${format(subDays(new Date(), 10), 'yyyy-MM-dd') + '12:00 PM'}`
const dateSickToBeOnOverviewFirstTime = `${format(new Date(), 'yyyy-MM-dd') + 'T01:00:00'}`
const dateSickToBeOnOverviewAfterChange = `${format(subDays(new Date(), 10), 'yyyy-MM-dd') + 'T12:00:00'}`
const dateBetter = `${format(new Date(), 'yyyy-MM-dd')} + 12:00 PM`
const dateBetterToBeOnOverview = `${format(new Date(), 'yyyy-MM-dd') + 'T12:00:00'}`

test.describe.serial('Clone form', () => {
  let page: Page
  let context: BrowserContext

  test.beforeAll(async ({ browser }) => {
    context = await browser.newContext(options)
    page = await context.newPage()
    await openAppAndLogin({
      page,
      appUrl,
      pageTitle: 'xxllnc formulieren',
      user: xxllncUser
    })
  })

  test.afterAll(async () => {
    // Clean up: remove the form
    await deleteFormFromOverviewScreen({ page, formName: formNameClone })

    await logOff({ page, appUrl })
    await context.close()
  })

  test('Search origin without finding', async ({ }) => {
    expect(await setFilterAndReturnTrueIfFound({ page, filterText: formNameOrigin })).toBeFalsy()
  })

  test('Uncheck private forms', async ({ }) => {
    await page.locator('div[role="button"]:has-text("Privé")').click()
  })

  test('Check change in url', async ({ }) => {
    await expect(page).toHaveURL(new RegExp(urlStrSearch, 'g'))
  })

  test('Check in row 1 form name origin to be visible', async ({ }) => {
    await expect(page.locator('//tr[1]/td[3]')).toHaveText(formNameOrigin)
  })

  test('Click clone action', async ({ }) => {
    await page.locator('[data-testid="ContentCopyIcon"]').first().click()
  })

  test('Fill form name clone', async ({ }) => {
    await page.locator('input[name="name"]').click()
    await page.locator('input[name="name"]').fill(formNameClone)
  })

  test('Save clone', async ({ }) => {
    await page.locator('text=Opslaan').click()
    await expect(page.locator('text=Formulier is toegevoegd')).toBeVisible({ timeout: longTimeout })

    await expect(page.locator('text=Formulier is toegevoegd')).toBeHidden({ timeout: longTimeout })
  })

  test('Search clone', async ({ }) => {
    expect(await setFilterAndReturnTrueIfFound({ page, filterText: formNameClone })).toBeTruthy()
  })

  test('Open clone in edit mode', async ({ }) => {
    await page.locator(locators.iconByDataTestid('EditIcon', '1')).click()

    await expect(page).toHaveURL(new RegExp(urlStrConEditMode, 'g'))
  })

  test('Check text Ziek en/of beter melden to be visible', async ({ }) => {
    await expect(page.locator('text=Wil je je ziek of beter melden')).toBeVisible()
  })

  test('Open preview', async ({ }) => {
    await page.locator('text=Voorbeeld').click()
  })

  test('Fill email address partially', async ({ }) => {
    await page.frameLocator('#preview').locator('input[name="data\\[eMailadres\\]"]').click()

    await page.frameLocator('#preview').locator('input[name="data\\[eMailadres\\]"]').fill('g.thijssen')
  })

  test('Check error message email to be visible', async ({ }) => {
    await expect(page.frameLocator('#preview').locator('text=E-mailadres moet een geldig e-mail adres bevatten.')).toBeVisible()
  })

  test('Fill email address completely', async ({ }) => {
    await page.frameLocator('#preview').locator('input[name="data\\[eMailadres\\]"]').click()

    await page.frameLocator('#preview').locator('input[name="data\\[eMailadres\\]"]').fill('g.thijssen@xxllnc.nl')
  })

  test('Select Beter melden', async ({ }) => {
    await page.frameLocator('#preview').locator('input[value="beterMelden"]').click()
  })

  test('Fill date Beter melding', async ({ }) => {
    await page.frameLocator('#preview').locator('input[type="text"] >> nth=1').click()
    await page.frameLocator('#preview').locator('input[type="text"] >> nth=1').fill(dateBetter)
  })

  test('Check date Ziek melding is mandatory', async ({ }) => {
    await page.frameLocator('#preview').locator('text=Volgende').click()

    await expect(page.frameLocator('#preview').locator(
      'text=Wat is de eerste ziektedag? Wat is de eerste ziektedag? is verplicht >> i')).toBeVisible()
  })

  test('Select date Sick in calendar on today 1 AM', async ({ }) => {
    await page.frameLocator('#preview').locator('i').first().click()
    await page.frameLocator('#preview').locator('.dayContainer > span[aria-current="date"]').first().click()

    await page.frameLocator('#preview').locator('.flatpickr-time > div > .arrowDown').first().click({
      clickCount: 11
    })

    await page.frameLocator('#preview').locator('input[type="text"]').first().click()
  })

  test('Fill date Better melding', async ({ }) => {
    await page.frameLocator('#preview').locator('input[type="text"] >> nth=1').click()
    await page.frameLocator('#preview').locator('input[type="text"] >> nth=1').fill(dateBetter)
  })

  test('Goto next page', async ({ }) => {
    await page.frameLocator('#preview').locator('text=Volgende >> nth=0').click()
  })

  test('Test canceling the form and then cancel the canceling', async ({ }) => {
    await page.frameLocator('#preview').getByRole('button', { name: 'Vorige button. Click to go back to the previous tab' }).click()

    await page.frameLocator('#preview').getByRole('button', { name: 'Annuleren button. Click to reset the form' }).click()

    await page.frameLocator('#preview').getByRole('button', { name: 'Nee' }).click()
  })

  test('Goto again to the next page', async ({ }) => {
    await page.frameLocator('#preview').locator('text=Volgende >> nth=0').click()
  })

  test('Check date Sick first time', async ({ }) => {
    await expect(page.frameLocator('#preview').locator(
      'input[name="data[watIsDeEersteZiektedag]"]')).toHaveValue(dateSickToBeOnOverviewFirstTime, { timeout: 2000 })
  })

  test('Go back to previous page', async ({ }) => {
    await page.frameLocator('#preview').locator('text=Vorige').click()
  })

  test('Select other Sick melding date', async ({ }) => {
    await page.frameLocator('#preview').locator('input[type="text"] >> nth=0').click()
    await page.frameLocator('#preview').locator('input[type="text"] >> nth=0').fill(dateSick)
  })

  test('Fill Opmerking', async ({ }) => {
    await page.frameLocator('#preview').locator('input[type="text"] >> nth=0').press('Shift+Tab')
    await page.frameLocator('#preview').locator('textarea[name="data\\[ziekMeldenWerknemer\\]"]').click()
    await page.frameLocator('#preview').locator('textarea[name="data\\[ziekMeldenWerknemer\\]"]').fill('Dit is mijn opmerking')
  })

  test('Goto the overview again', async ({ }) => {
    await page.frameLocator('#preview').locator('text=Volgende >> nth=0').click()
  })

  test('Check the changed date Sick', async ({ }) => {
    await expect(page.frameLocator('#preview').locator(
      'input[name="data[watIsDeEersteZiektedag]"]')).toHaveValue(dateSickToBeOnOverviewAfterChange)
  })

  test('Check date Better', async ({ }) => {
    await expect(page.frameLocator('#preview').locator(
      'input[name="data[watIsDeLaatsteZiektedag]"]')).toHaveValue(dateBetterToBeOnOverview)
  })

  test('Check the remark', async ({ }) => {
    await expect(page.frameLocator('#preview').locator('text=Dit is mijn opmerking')).toHaveText('Dit is mijn opmerking')
  })

  test('Check confirmation box', async ({ }) => {
    await page.frameLocator('#preview').locator('input[name="data\\[confirmationCheckBox\\]"]').check()
  })

  test('Send the form', async ({ }) => {
    await page.frameLocator('#preview').locator('button:has-text("Versturen")').click()
  })

  test('Check the sending message 2 form', async ({ }) => {
    await expect.poll(async () => {
      const h1Loc = page.frameLocator('#preview').locator('text=Bedankt!')
      return h1Loc.isVisible()
    }, {
      // Probe, wait 1s, probe, wait 2s, probe, wait 10s, probe, wait 10s, probe, .... Defaults to [100, 250, 500, 1000].
      intervals: [1_000],
      timeout: 80_000
    }).toBe(true)

    await expect(page.frameLocator('#preview').locator(' text=Bedankt!')).toBeVisible()
  })

  test('Check the sending message 3 form', async ({ }) => {
    await expect(page.frameLocator('#preview').locator(
      'text=Het indienen van het formulier is succesvol verlopen.')).toBeVisible()
  })

  test('Close the form Bedankt page', async ({ }) => {
    await page.frameLocator('#preview').locator('text=Afsluiten').click()
  })

  test('Check send shown url', async ({ }) => {
    await expect.soft(page.frameLocator('#preview').locator('text=xxllnc Zaakgericht')).toBeVisible({ timeout: superLongTimeout })
  })

  test('Close preview', async ({ }) => {
    await page.locator('[aria-label="Close"]').click()
  })

})
