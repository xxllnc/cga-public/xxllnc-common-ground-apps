import { BrowserContext, expect, Page, test } from '@playwright/test'
import { addDays, format, subDays } from 'date-fns'
import { baseUrl, xxllncUser } from '../../e2e_tests'
import { deleteFormFromOverviewScreen, logOff, openAppAndLogin, setFilterAndGetCountFound } from '../../partial-tests'
import { testConfig } from '../../playwright.config'

const app = 'formulierenbeheer'
const appUrl = `${baseUrl}${app}/`
const importName = 'initial_filter_data.json'
const formNameStart = 'Filter'
const publishDateEarlier = format(subDays(new Date(), 63), 'yyyy-MM-dd')
const publishDateLastWeek = format(subDays(new Date(), 7), 'yyyy-MM-dd')
const publishDateLastMonth = format(subDays(new Date(), 31), 'yyyy-MM-dd')
const publishDateFromTomorrowOn = format(addDays(new Date(), 1), 'yyyy-MM-dd')
const publishDateToday = format(new Date(), 'yyyy-MM-dd')
const { options } = testConfig

test.describe.serial('Initial filter forms in db', () => {

  let context: BrowserContext
  let page: Page

  test.beforeAll(async ({ browser }) => {
    context = await browser.newContext(options)
    page = await context.newPage()
    await openAppAndLogin({
      page,
      appUrl,
      pageTitle: 'xxllnc formulieren',
      user: xxllncUser
    })
  })

  test.afterAll(async () => {
    await logOff({ page, appUrl })
    await context.close()
  })

  test('Reset Openbaar filter', async ({ }) => {
    await page.locator('div[role="button"]:has-text("Privé")').click()
  })

  test('Search and bulk delete forms if present', async ({ }) => {
    await deleteFormFromOverviewScreen({ page, formName: formNameStart, deleteAllFound: true })
  })

  test('Click import of', async ({ }) => {
    await page.locator('[aria-label="Importeren"]').click()
  })

  test('Upload file', async ({ }) => {
    await page.locator('input[data-testid="uploadInput"]').setInputFiles('testdata/' + importName)
  })

  test('Set file 1 active', async ({ }) => {
    await page.locator('[aria-label="import-dialog"] div[role="button"]:has-text("Inactief")').click()

    await page.locator('li[role="option"]:has-text("Actief")').first().click()
  })

  test('Set file 1 publish date', async ({ }) => {
    await page.locator('input[id=form-date]').fill(publishDateLastWeek)
  })

  test('Importeer file 1', async ({ }) => {
    await page.locator('[aria-label="Importeer"]').click()
  })

  test('Set file 2 active', async ({ }) => {
    await page.locator('[aria-label="import-dialog"] div[role="button"]:has-text("Inactief")').click()

    await page.locator('li[role="option"]:has-text("Actief")').first().click()
  })

  test('Set file 2 publish date', async ({ }) => {
    await page.locator('input[id=form-date]').fill(publishDateLastMonth)
  })

  test('Importeer file 2', async ({ }) => {
    await page.locator('[aria-label="Importeer"]').click()
  })

  test('Set file 3 active', async ({ }) => {
    await page.locator('[aria-label="import-dialog"] div[role="button"]:has-text("Inactief")').click()

    await page.locator('li[role="option"]:has-text("Actief")').first().click()
  })

  test('Set file 3 publish date', async ({ }) => {
    await page.locator('input[id=form-date]').fill(publishDateEarlier)
  })

  test('Importeer file 3', async ({ }) => {
    await page.locator('[aria-label="Importeer"]').click()
  })

  test('Set file 4 active', async ({ }) => {
    await page.locator('[aria-label="import-dialog"] div[role="button"]:has-text("Inactief")').click()

    await page.locator('li[role="option"]:has-text("Actief")').first().click()
  })

  test('Set file 4 publish date', async ({ }) => {
    await page.locator('input[id=form-date]').fill(publishDateEarlier)
  })

  test('Importeer file 4', async ({ }) => {
    ([
      await page.locator('[aria-label="Importeer"]').click()
    ])
  })

  test('Set file 5 active', async ({ }) => {
    await page.locator('[aria-label="import-dialog"] div[role="button"]:has-text("Inactief")').click()

    await page.locator('li[role="option"]:has-text("Actief")').first().click()
  })

  test('Set file 5 publish date', async ({ }) => {
    await page.locator('input[id=form-date]').fill(publishDateFromTomorrowOn)
  })

  test('Importeer file 5', async ({ }) => {
    await page.locator('[aria-label="Importeer"]').click()
  })

  test('Skip file 6', async ({ }) => {
    await page.locator('[aria-label="Overslaan"]').click()
  })

  test('Set file 7 active', async ({ }) => {
    await page.locator('[aria-label="import-dialog"] div[role="button"]:has-text("Inactief")').click()

    await page.locator('li[role="option"]:has-text("Actief")').first().click()
  })

  test('Set file 7 publish date', async ({ }) => {
    await page.locator('input[id=form-date]').fill(publishDateFromTomorrowOn)
  })

  test('Importeer file 7', async ({ }) => {
    await page.locator('[aria-label="Importeer"]').click()
  })

  test('Set file 8 active', async ({ }) => {
    await page.locator('[aria-label="import-dialog"] div[role="button"]:has-text("Inactief")').click()

    await page.locator('li[role="option"]:has-text("Actief")').first().click()
  })

  test('Set file 8 publish date', async ({ }) => {
    await page.locator('input[id=form-date]').fill(publishDateLastWeek)
  })

  test('Importeer file 8', async ({ }) => {
    await page.locator('[aria-label="Importeer"]').click()
  })

  test('Set file 9 active', async ({ }) => {
    await page.locator('[aria-label="import-dialog"] div[role="button"]:has-text("Inactief")').click()


    await page.locator('li[role="option"]:has-text("Actief")').first().click()
  })

  test('Set file 9 publish date', async ({ }) => {
    await page.locator('input[id=form-date]').fill(publishDateToday)
  })

  test('Importeer file 9', async ({ }) => {
    await page.locator('[aria-label="Importeer"]').click()
  })

  test('Set file 10 active', async ({ }) => {
    await page.locator('[aria-label="import-dialog"] div[role="button"]:has-text("Inactief")').click()

    await page.locator('li[role="option"]:has-text("Actief")').first().click()
  })

  test('Set file 10 publish date', async ({ }) => {
    await page.locator('input[id=form-date]').fill(publishDateToday)
  })

  test('Importeer file 10', async ({ }) => {
    await page.locator('[aria-label="Importeer"]').click()
  })

  test('Skip file 11', async ({ }) => {
    await page.locator('[aria-label="Overslaan"]').click()
  })

  test('Click Klaar', async ({ }) => {
    await page.locator('[aria-label="Klaar"]').click()
  })

  test('Check Number of files found', async ({ }) => {
    const countFound = await setFilterAndGetCountFound({ page, filterText: formNameStart })
    expect(countFound === 9).toBeTruthy()
  })

})