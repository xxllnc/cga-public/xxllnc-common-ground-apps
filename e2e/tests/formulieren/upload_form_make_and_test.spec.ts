import { BrowserContext, expect, Page, test } from '@playwright/test'
import { format, subDays } from 'date-fns'
import { readFileSync } from 'fs'
import { authorOrganization, baseUrl, xxllncUser } from '../../e2e_tests'
import { checkIfFormExists, createNewForm, deleteFormFromOverviewScreen, logOff, openAppAndLogin } from '../../partial-tests'
import { testConfig } from '../../playwright.config'

import { fillValueAndCheckIfFieldIsSet, fixMoreThanOneDialog, locators, openEditFormPage } from '../../utils'

const { options, clickDelaySlow, longTimeout } = testConfig
const app = 'formulierenbeheer'
const appUrl = `${baseUrl}${app}/`
const date = format(subDays(new Date(), 1), 'yyyy-MM-dd HH:mm:ss:SSS')
const formName = `Upload uw bestand ${date}`

test.describe.serial('Make and test upload form', () => {

  let context: BrowserContext
  let page: Page
  let pageInwoner: Page

  test.beforeAll(async ({ browser }) => {
    context = await browser.newContext(options)
    page = await context.newPage()
    await openAppAndLogin({
      page,
      appUrl,
      pageTitle: 'xxllnc formulieren',
      user: xxllncUser
    })
  })

  test.afterAll(async () => {
    await deleteFormFromOverviewScreen({ page, formName })

    await logOff({ page, appUrl })
    await context.close()
  })

  test('Create new form', async ({ }) => {
    // Check if we are in the main page
    await expect(page.locator(locators.text(authorOrganization)).first()).toBeVisible()

    await createNewForm({ page, formName })

    // Check the new form is there
    await checkIfFormExists({ page, formName })

    // open the new form
    await openEditFormPage({ page, formName })
  })

  test('Check Pagina 1 settings mode open', async () => {
    // make sure we are on Pagina 1
    await page.locator('text=Pagina 1').hover()
    const modal = page.locator('[aria-label="Edit button\\. Click to open component settings modal window"]').first()
    await expect(modal).toBeVisible()
    await modal.click()
  })

  test('Fix more than one dialog', async () => {
    // Close all the popups and open one if there are more then one popups on the page
    const modal = page.locator('[aria-label="Edit button\\. Click to open component settings modal window"]').first()
    await fixMoreThanOneDialog(page, modal)
  })

  test('Rename page 1', async () => {
    await page.locator('[placeholder="Paneeltitel"]').fill('Pagina Upload een bestand')
    await expect(page.locator(locators.text('Pagina Upload een bestand')).first()).toBeVisible()

    await page.locator('div[role="dialog"] button:has-text("Opslaan")').click()
    await page.waitForSelector(locators.partialTextLocator('Xxllnc presets'))
  })

  test('Add the upload field', async () => {
    await page.dragAndDrop('text=Bestand >> nth=1', 'text=Sleep een component en zet hem neer in het formulier')
  })

  test('Rename upload field Upload een bestand', async () => {
    await fillValueAndCheckIfFieldIsSet({ fieldLocator: page.locator('[placeholder="Veldlabel"]'), value: 'Upload een bestand' })
  })

  test('Add file type nr 1', async () => {
    await page.getByPlaceholder('Veldlabel').fill('Upload uw bestand')

    await page.getByRole('link', { name: 'Bestand' }).click()

    const label = page.locator('input[name="data\\[fileTypes\\]\\[0\\]\\[label\\]"]')
    await label.click()
    await label.fill('JSON bestand')

    const value = page.locator('input[name="data\\[fileTypes\\]\\[0\\]\\[value\\]"]')
    await value.click()
    await value.fill('JSON')

  })

  test('Add file type nr 2', async () => {
    await page.getByRole('button', { name: ' Nog een toevoegen' }).click()

    const label = page.locator('input[name="data\\[fileTypes\\]\\[0\\]\\[label\\]"]')
    await label.fill('Pdf')
    await label.press('Tab')

    await page.locator('input[name="data\\[fileTypes\\]\\[1\\]\\[value\\]"]').fill('PDF')
  })

  test('Change the minimum file size', async () => {
    await page.getByPlaceholder('0KB').click()

    await page.getByPlaceholder('0KB').fill('10KB')
  })

  test('Change the maximum file size', async () => {
    await page.getByPlaceholder('500MB').click()

    await page.getByPlaceholder('500MB').fill('20KB')
  })

  test('Save the upload field details', async () => {
    await page.locator('div[role="dialog"]:has-text'
      + '("Bestand Component Help Scherm Bestand Data Validatie API Voorwaardelijk Logica O")'
    ).getByRole('button', { name: 'Opslaan' }).click()
    await page.waitForSelector(locators.partialTextLocator('Xxllnc presets'))
  })

  test('Save the new form', async () => {
    await page.locator('nav:has-text("VoorbeeldInstellingenOpslaan")').getByRole('button',
      { name: 'Opslaan' }).click({ timeout: clickDelaySlow })
  })

  test('Check message Formuliergegevens zijn opgeslagen to be visible', async () => {
    await expect(page.locator('text=Formuliergegevens zijn opgeslagen')).toBeVisible({ timeout: longTimeout })
    await expect(page.locator('text=Formuliergegevens zijn opgeslagen')).toBeHidden({ timeout: longTimeout })
  })

  test('Open the form in preview mode for the first time', async () => {
    await page.getByRole('button', { name: 'Voorbeeld' }).click()
  })

  test('Test file upload is not mandatory', async () => {
    await page.frameLocator('#preview').getByRole('button',
      { name: 'Volgende button. Click to go to the next tab' }).click()
    await expect(page.frameLocator('#preview').getByRole('button',
      { name: 'Versturen button. Click to submit the form' })).toBeVisible()
  })

  test('Return to edit form and open settings of upload field', async () => {
    await page.getByRole('button', { name: 'Close' }).click()

    await page.locator('text=Upload uw bestand >> nth=1').click()

    await page.getByRole('button', { name: 'Edit button. Click to open component settings modal window' }).nth(1).click()
  })

  test('Make the upload field mandatory', async () => {
    await page.getByRole('link', { name: 'Validatie' }).click()

    await page.getByLabel('Verplicht').check()

    await page.locator('div[role="dialog"]:has-text("Bestand Component Help Scherm Bestand Data Validatie API'
      + ' Voorwaardelijk Logica O")').getByRole('button', { name: 'Opslaan' }).click()

    await page.locator('nav:has-text("VoorbeeldInstellingenOpslaan")').getByRole('button', { name: 'Opslaan' }).click()
    await expect(page.locator('text=Formuliergegevens zijn opgeslagen')).toBeVisible({ timeout: longTimeout })
    await expect(page.locator('text=Formuliergegevens zijn opgeslagen')).toBeHidden({ timeout: longTimeout })
  })

  test('Return to main screen and open form in other tab', async () => {
    await page.getByRole('menuitem', { name: 'Formulieren' }).click()
    await expect(page).toHaveURL(new RegExp(appUrl + 'forms'))

    const [pageInwonerTemp] = await Promise.all([
      page.waitForEvent('popup'),
      page.locator('[data-testid="RemoveRedEyeIcon"]').click()
    ])
    pageInwoner = pageInwonerTemp
    await pageInwoner.waitForLoadState()

    await pageInwoner.getByText('Sleep bestanden om ze toe te voegen, of bladeren Browse to attach file for Uploa').click()
  })

  test('Test file upload is mandatory', async () => {
    await pageInwoner.getByRole('button',
      { name: 'Volgende button. Click to go to the next tab' }).click()
    await expect(pageInwoner.getByRole('link', { name: 'Upload uw bestand is verplicht' })).toBeVisible()
  })

  test('Add file to test min size', async () => {
    // Read your file into a buffer.
    const buffer = readFileSync('testdata/Stel-uw-vraag.json')

    // Create the DataTransfer and File
    const dataTransfer = await pageInwoner.evaluateHandle((buffer) => {
      const dt = new DataTransfer()
      // Convert the buffer to a hex array
      const file = new File([buffer.toString()], 'Stel-uw-vraag.json', { type: 'application/json' })
      dt.items.add(file)
      return dt
    }, buffer.toJSON().data)

    // Now dispatch
    await pageInwoner.dispatchEvent('//div[@class="fileSelector"]', 'drop', { dataTransfer })
  })

  test('Check the error message for a too small file size', async () => {
    await expect(pageInwoner.getByText('File is too small; it must be at least 10KB')).toBeVisible()
  })

  test('Remove the file', async () => {
    await pageInwoner.getByText('Remove button. Press to remove Stel-uw-vraag.json File is too small. it m').click()
  })

  test('Add file to test max size', async () => {
    // Read your file into a buffer.
    const buffer = readFileSync('testdata/Ziek-enof-beter-melden.json')

    // Create the DataTransfer and File
    const dataTransfer = await pageInwoner.evaluateHandle((buffer) => {
      const dt = new DataTransfer()
      // Convert the buffer to a hex array
      const file = new File([buffer.toString()], 'Ziek-enof-beter-melden.json', { type: 'application/json' })
      dt.items.add(file)
      return dt
    }, buffer.toJSON().data)

    // Now dispatch
    await pageInwoner.dispatchEvent('//div[@class="fileSelector"]', 'drop', { dataTransfer })
  })

  test('Check the error message for a too large file size', async () => {
    await expect(pageInwoner.getByText('File is too big; it must be at most 20KB')).toBeVisible()
  })

  test('Remove also this file', async () => {
    await pageInwoner.getByText('Remove button. Press to remove Ziek-enof-beter-melden.json File is too big. it m').click()
  })

  test('Add another file', async () => {
    // Read your file into a buffer.
    const buffer = readFileSync('testdata/Upload testbestand.pdf')

    // Create the DataTransfer and File
    const dataTransfer = await pageInwoner.evaluateHandle((buffer) => {
      const dt = new DataTransfer()
      // Convert the buffer to a hex array
      const file = new File([buffer.toString()], 'Upload testbestand.pdf', { type: 'application/pdfn' })
      dt.items.add(file)
      return dt
    }, buffer.toString('utf8'))

    // Now dispatch
    await pageInwoner.dispatchEvent('//div[@class="fileSelector"]', 'drop', { dataTransfer })
  })

  test('Select the file type Pdf', async () => {
    await pageInwoner.locator('select').selectOption('PDF')
  })

  test('Go to the overview page', async () => {
    await pageInwoner.getByRole('button',
      { name: 'Volgende button. Click to go to the next tab' }).click()
  })

  test('Check presence of the name of the file', async () => {
    await expect(pageInwoner.locator('text=Upload testbestand.pdf')).toBeVisible()
  })

  test('Sign, send and end the form', async () => {
    await pageInwoner.getByLabel(
      'Ik heb dit formulier naar waarheid ingevuld en ga akkoord met het gebruik van mijn '
      + 'persoonsgegevens voor het verwerken van dit formulier.').check()

    await pageInwoner.getByRole('button', { name: 'Versturen button. Click to submit the form' }).click()

    await pageInwoner.getByRole('button', { name: 'Afsluiten' }).click()
  })

  test('Close tab pageInwoner', async () => {
    await pageInwoner.close()
  })
})
