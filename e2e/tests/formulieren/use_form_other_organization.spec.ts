import { BrowserContext, expect, Page, test } from '@playwright/test'
import { format, subDays } from 'date-fns'
import { baseUrl, getSubDomainUrl, xxllncUser, zaakstadUser } from '../../e2e_tests'
import {
  checkIfFormExists, deleteFormFromOverviewScreen,
  logOff, openAppAndLogin, setFilterAndExpectOneItemFound, setFilterAndGetCountFound
} from '../../partial-tests'
import { testConfig } from '../../playwright.config'
import { locators, openSettingsFormPage } from '../../utils'

const { shortTimeout, options, softErrorMessage, longTimeout, clickDelay } = testConfig
const formNameOriginal = 'Stel uw vraag'
const date = format(subDays(new Date(), 1), 'yyyy-MM-dd HH:mm:ss:SSS')
const formNameClone = `${formNameOriginal} ${date}`
const appUrl = `${baseUrl}formulierenbeheer/`
const appUrlZaakstad = `${getSubDomainUrl('zaakstad')}formulierenbeheer/`

test.describe.serial('Use form other organization', () => {
  let page: Page
  let pageOwner: Page
  let context: BrowserContext

  test.beforeAll(async ({ browser }) => {
    context = await browser.newContext(options)
    pageOwner = await context.newPage()
    page = await context.newPage()

    await openAppAndLogin({
      page: pageOwner,
      appUrl,
      pageTitle: 'xxllnc formulieren',
      user: xxllncUser
    })

    await openAppAndLogin({
      page,
      appUrl: appUrlZaakstad,
      pageTitle: 'xxllnc formulieren',
      user: zaakstadUser
    })

    await pageOwner.locator('div[role="button"]:has-text("Privé")').click()
    await expect(pageOwner).toHaveURL(new RegExp('.*filter=%7B%7D&order=ASC&page=1&perPage=10&sort=name'))
  })

  test.afterAll(async ({ }) => {
    // Clean up: remove the form
    await deleteFormFromOverviewScreen({ page: pageOwner, formName: formNameClone })

    await logOff({ page, appUrl: appUrlZaakstad })
    await logOff({ page: pageOwner, appUrl })
    await context.close()
  })

  test('Reset Openbaar filter in tab VIEWER', async ({ }) => {
    await page.locator('div[role="button"]:has-text("Privé")').click()
    await expect(page).toHaveURL(new RegExp('.*filter=%7B%7D&order=ASC&page=1&perPage=10&sort=name'))
  })

  test('Search original form in tab OWNER', async ({ }) => {
    await setFilterAndGetCountFound({ page: pageOwner, filterText: formNameOriginal })
  })

  test('Create clone in tab OWNER', async ({ }) => {
    await pageOwner.locator('[data-testid="ContentCopyIcon"]').first().click()
    await pageOwner.locator('input[name="name"]').fill(formNameClone)
    await pageOwner.locator('text="Actief" >> nth=0').click({ delay: clickDelay })
    await expect(pageOwner.locator('#status_ACTIVE')).toBeChecked()
    await pageOwner.locator('input[name="publishDate"]').fill(format(subDays(new Date(), 1), 'yyyy-MM-dd'))
    await pageOwner.locator('input[name="private"]').uncheck()
    await expect(pageOwner.locator('input[name="private"]')).not.toBeChecked()
    await pageOwner.locator('text=Opslaan').click()
    await expect(pageOwner.locator('text=Formulier is toegevoegd')).toBeVisible({ timeout: longTimeout })

    await expect(pageOwner.locator('text=Formulier is toegevoegd')).toBeHidden({ timeout: longTimeout })

    await checkIfFormExists({ page: pageOwner, formName: formNameClone })
  })

  test('Search and open settings in tab VIEWER', async ({ }) => {
    await setFilterAndGetCountFound({ page, filterText: formNameClone })
    await openSettingsFormPage({ page, formName: formNameClone })
  })

  test('Check visibility nr 1 xxllnc in tab VIEWER', async ({ }) => {
    await expect(page.locator('td:has-text("xxllnc")')).toBeVisible()
  })

  test('Check visibility nr 1 xxllnc OWNER in tab VIEWER', async ({ }) => {
    await expect(page.locator('td:has-text("OWNER")')).toBeVisible()
  })

  test('Check not visible nr 1 Zaakstad in tab VIEWER', async ({ }) => {
    await expect(page.locator('td:has-text("Zaakstad")')).toBeHidden({ timeout: shortTimeout })
  })

  test('Check enabled In gebruik nemen in tab VIEWER', async ({ }) => {
    const ariaDisabled = await page.locator(locators.fieldByAriaLabel('In gebruik nemen', '1')).getAttribute('aria-disabled')
    expect(ariaDisabled).toBeNull()
  })

  test('Check disabled Instellingen in tab VIEWER', async ({ }) => {
    const ariaDisabled = await page.locator(locators.fieldByAriaLabel('Instellingen', '1')).getAttribute('aria-disabled')
    expect(ariaDisabled).not.toBeNull()
  })

  // Start using the form
  test('Start using form by VIEWER in tab VIEWER', async ({ }) => {
    await page.locator('text=In gebruik nemen').click()
    await expect(page).toHaveURL(/.*\/formulierenbeheer\/share\/create\?formUuid=.*/)
  })

  test('Set form active for VIEWER in tab VIEWER', async ({ }) => {
    await page.locator('#status_ACTIVE').check()
  })

  test('Save using of form by VIEWER in tab VIEWER', async ({ }) => {
    await page.locator('text=Opslaan').click()
    await expect(page).toHaveURL(/.*\/formulierenbeheer\/share/)
  })

  test('Check showing of message Element toegevoegd in tab VIEWER', async ({ }) => {
    await expect.soft(page.locator('text=Element toegevoegd'), softErrorMessage).toBeVisible()
  })

  test('Search and open settings nr 2 in tab VIEWER', async ({ }) => {
    await page.locator(locators.iconByDataTestid('DescriptionIcon', '1')).click()
    await expect(page).toHaveURL(/.*\/formulierenbeheer\/forms.*/)

    await setFilterAndExpectOneItemFound({ page, filterText: formNameClone })
    await openSettingsFormPage({ page, formName: formNameClone })
  })

  test('Check visibility nr 2 xxllnc in tab VIEWER', async ({ }) => {
    await expect(page.locator('td:has-text("xxllnc")')).toBeVisible()
  })

  test('Check visibility nr 2 xxllnc OWNER in tab VIEWER', async ({ }) => {
    await expect(page.locator('td:has-text("OWNER")')).toBeVisible()
  })

  test('Check visibility Zaakstad in tab VIEWER', async ({ }) => {
    await expect(page.locator('td:has-text("Zaakstad")')).toBeVisible()
  })

  test('Check visibility Zaakstad VIEWER in tab VIEWER', async ({ }) => {
    await expect(page.locator('td:has-text("VIEWER")')).toBeVisible()
  })

  test('Check disabled In gebruik nemen in tab VIEWER', async ({ }) => {
    const ariaDisabled = await page.locator(locators.fieldByAriaLabel('In gebruik nemen', '1')).getAttribute('aria-disabled')
    expect(ariaDisabled).not.toBeNull()
  })

  test('Check enabled Instellingen in tab VIEWER', async ({ }) => {
    const ariaDisabled = await page.locator(locators.fieldByAriaLabel('Instellingen', '1')).getAttribute('aria-disabled')
    expect(ariaDisabled).toBeNull()
  })

  // Check sharing in tab OWNER
  test('Search and open settings in tab OWNER', async ({ }) => {
    await setFilterAndExpectOneItemFound({ page: pageOwner, filterText: formNameClone })
    await openSettingsFormPage({ page: pageOwner, formName: formNameClone })
  })

  test('Check disabled In gebruik nemen in tab OWNER', async ({ }) => {
    const ariaDisabled = await page.locator(locators.fieldByAriaLabel('In gebruik nemen', '1')).getAttribute('aria-disabled')
    expect(ariaDisabled).not.toBeNull()
  })

  test('Check enabled Instellingen in tab OWNER', async ({ }) => {
    const ariaDisabled = await page.locator(locators.fieldByAriaLabel('Instellingen', '1')).getAttribute('aria-disabled')
    expect(ariaDisabled).toBeNull()
  })

  test('Check visibility xxllnc in tab OWNER', async ({ }) => {
    await expect(pageOwner.locator('td:has-text("xxllnc")')).toBeVisible()
  })

  test('Check visibility xxllnc OWNER in tab OWNER', async ({ }) => {
    await expect(pageOwner.locator('td:has-text("OWNER")')).toBeVisible()
  })

  test('Check visibility Zaakstad in tab OWNER', async ({ }) => {
    await expect(pageOwner.locator('td:has-text("Zaakstad")')).toBeVisible()
  })

  test('Check visibility Zaakstad VIEWER in tab OWNER', async ({ }) => {
    await expect(pageOwner.locator('td:has-text("VIEWER")')).toBeVisible()
  })

  // Back to viewer
  test('Remove form from organization in tab VIEWER', async ({ }) => {
    await page.locator('text=Instellingen').click()

    await page.locator('text=Verwijderen').click()

    await page.locator('text=Bevestigen').click()
    await expect(page).toHaveURL(/.*formulierenbeheer\/share/)
  })

  test('Check showing of message Element verwijderd in tab VIEWER', async ({ }) => {
    await expect.soft(page.locator('text=Element verwijderd'), softErrorMessage).toBeVisible()
  })

  test('Search and open settings nr 3 in tab VIEWER', async ({ }) => {
    await page.locator(locators.iconByDataTestid('DescriptionIcon', '1')).click()
    await expect(page).toHaveURL(/.*\/formulierenbeheer\/forms/)

    await setFilterAndExpectOneItemFound({ page, filterText: formNameClone })
    await openSettingsFormPage({ page, formName: formNameClone })
  })

  test('Check visibility nr 3 xxllnc in tab VIEWER', async ({ }) => {
    await expect(page.locator('td:has-text("xxllnc")')).toBeVisible()
  })

  test('Check visibility nr 3 xxllnc OWNER in tab VIEWER', async ({ }) => {
    await expect(page.locator('td:has-text("OWNER")')).toBeVisible()
  })

  test('Check not visible nr 2 Zaakstad in tab VIEWER', async ({ }) => {
    await expect(page.locator('td:has-text("Zaakstad")')).toBeHidden({ timeout: shortTimeout })
  })
})