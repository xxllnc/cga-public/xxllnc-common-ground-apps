/* eslint-disable playwright/no-conditional-in-test */
import { BrowserContext, expect, Page, test } from '@playwright/test'
import { format, subDays } from 'date-fns'
import { authorOrganization, baseUrl, xxllncUser } from '../../e2e_tests'
import { logOff, openAppAndLogin, setFilterAndReturnTrueIfFound } from '../../partial-tests'
import { testConfig } from '../../playwright.config'
import { locators } from '../../utils'
const formNameArray = [
  { name: 'Stel uw vraag', fileName: 'Stel-uw-vraag.json', publicPrivateIcon: 'MuiSvgIcon-colorPrimary', textToCheck: 'De vraag' },
  {
    name: 'Ziek en/of beter melden', fileName: 'Ziek-enof-beter-melden.json', publicPrivateIcon: 'MuiSvgIcon-colorPrimary',
    textToCheck: 'E-mailadres'
  },
  {
    name: 'Aanvraag parkeerplaats inwoner', fileName: 'Aanvraag-parkeerplaats-inwoner.json', publicPrivateIcon: 'MuiSvgIcon-colorDisabled',
    textToCheck: 'Wilt u anoniem blijven'
  }
]
let found = false
const publishDate = format(subDays(new Date(), 2), 'yyyy-MM-dd')
const publishDateNumeric = format(subDays(new Date(), 2), 'd-M-yyyy')
const { options } = testConfig
const app = 'formulierenbeheer'
const appUrl = `${baseUrl}${app}/`

formNameArray.forEach(form => {

  const [formName, fileName, publicPrivateIcon, textToCheck] = [form.name, form.fileName, form.publicPrivateIcon, form.textToCheck]
  test.describe.serial(`Import form "${formName}" when not present`, () => {

    let context: BrowserContext
    let page: Page

    test.beforeAll(async ({ browser }) => {
      context = await browser.newContext(options)
      page = await context.newPage()
      await openAppAndLogin({
        page,
        appUrl,
        pageTitle: 'xxllnc formulieren',
        user: xxllncUser
      })
    })

    test.afterAll(async () => {
      await logOff({ page, appUrl })
      await context.close()
    })

    test(`Reset Openbaar filter "${formName}"`, async ({ }) => {
      await page.locator('div[role="button"]:has-text("Privé")').click()
    })

    test(`Import all the files one by one, when not present "${formName}"`, async ({ }) => {

      await Promise.all([
        page.waitForNavigation(),
        console.log('Click icon Formulieren'),
        page.locator('[data-testid="DescriptionIcon"] path').click()
      ])

      await page.waitForLoadState()
    })

    // test(`Search "${formName}"`, async () => {
    //   const countFound = await setFilterAndReturnTrueIfFound({ page, filterText: formName })

    // eslint-disable-next-line playwright/no-conditional-in-test
    // if (!countFound) {
    test(`Search form "${formName}"`, async ({ }) => {
      found = await setFilterAndReturnTrueIfFound({ page, filterText: formName })
    })

    test(`Click import of "${formName}"`, async ({ }) => {
      if (!found) {
        await page.locator('[aria-label="Importeren"]').click()
      }
    })

    test(`Upload file "${formName}"`, async ({ }) => {
      if (!found) {
        await page.locator('input[data-testid="uploadInput"]').setInputFiles('testdata/' + fileName)
      }
    })

    test(`Set active "${formName}"`, async ({ }) => {
      if (!found) {
        await page.locator('[aria-label="import-dialog"] div[role="button"]:has-text("Inactief")').click()

        await page.locator('li[role="option"]:has-text("Actief")').first().click()
      }
    })

    test(`Set publish date "${formName}"`, async ({ }) => {
      if (!found) {
        await page.locator('input[id=form-date]').fill(publishDate)
      }
    })

    test(`Click Importeer "${formName}"`, async ({ }) => {
      if (!found) {
        await page.locator('[aria-label="Importeer"]').click()
      }
    })

    test(`Click Klaar "${formName}"`, async ({ }) => {
      if (!found) {
        await page.locator('[aria-label="Klaar"]').click()
      }
    })

    test(`Check setting public of "${formName}"`, async ({ }) => {
      if (!found) {
        await expect(page.locator('//*[contains(@class,"' + publicPrivateIcon + '")] >> nth=0')).toBeVisible()
      }
    })

    test(`Check owner of "${formName}"`, async ({ }) => {
      if (!found) {
        await expect(page.locator(locators.partialTextLocator(authorOrganization))).toBeVisible()
      }
    })

    test(`Check publication date of "${formName}"`, async ({ }) => {
      if (!found) {
        await expect(page.locator(locators.partialTextLocator(publishDateNumeric))).toBeVisible()
      }
    })
    // }
    // })
    test(`Check status active of "${formName}"`, async ({ }) => {
      await expect(page.locator(locators.partialTextLocator('Actief'))).toBeVisible()
    })

    test(`Open the new form  "${formName}"`, async ({ }) => {
      await page.locator(locators.iconByDataTestid('EditIcon', '1')).click()
    })

    test(`Check that the first field is there in "${formName}"`, async ({ }) => {
      await expect(page.locator(locators.partialTextLocator(textToCheck))).toBeVisible()
    })

  })
})