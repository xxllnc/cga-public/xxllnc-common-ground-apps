import { BrowserContext, expect, Page, test } from '@playwright/test'
import { format, subDays } from 'date-fns'
import { authorOrganization, baseUrl, xxllncUser } from '../../e2e_tests'
import { checkIfFormExists, createNewForm, deleteFormFromOverviewScreen, logOff, openAppAndLogin } from '../../partial-tests'
import { testConfig } from '../../playwright.config'
import abonnement from '../../testdata/Aanvraag-parkeerplaats-abonnement.json'

import { fillValueAndCheckIfFieldIsSet, fixMoreThanOneDialog, locators, openEditFormPage } from '../../utils'

const { options, clickDelay, typeDelay, typeDelaySlow, longTimeout } = testConfig
const app = 'formulierenbeheer'
const appUrl = `${baseUrl}${app}/`
const date = format(subDays(new Date(), 1), 'yyyy-MM-dd HH:mm:ss:SSS')
const formName = `Aanvraag parkeerplaats ${date}`

test.describe.serial('Make Aanvraag parkeerplaats', () => {

  let context: BrowserContext
  let page: Page

  test.beforeAll(async ({ browser }) => {
    context = await browser.newContext(options)
    page = await context.newPage()
    await openAppAndLogin({
      page,
      appUrl,
      pageTitle: 'xxllnc formulieren',
      user: xxllncUser
    })
  })

  test.afterAll(async () => {
    // Clean up: remove the form
    await deleteFormFromOverviewScreen({ page, formName })

    await logOff({ page, appUrl })
    await context.close()
  })

  test('Create new form', async ({ }) => {
    // Check if we are in the main page
    await expect(page.locator(locators.text(authorOrganization)).first()).toBeVisible()

    await createNewForm({ page, formName })

    // Check the new form is there
    await checkIfFormExists({ page, formName })

    // open the new form
    await openEditFormPage({ page, formName })
  })

  test('Check Pagina 1 settings mode open', async () => {
    // make sure we are on Pagina 1
    const modal = page.locator('[aria-label="Edit button\\. Click to open component settings modal window"]').first()
    await expect(modal).toBeVisible()
    await modal.click()
  })

  test('Fix more than one dialog', async () => {
    // Close all the popups and open one if there are more then one popups on the page
    const modal = page.locator('[aria-label="Edit button\\. Click to open component settings modal window"]').first()
    await fixMoreThanOneDialog(page, modal)
  })

  test('Rename page 1 to Pagina uw gegevens', async () => {
    await page.locator('[placeholder="Paneeltitel"]').fill('Pagina Uw gegevens')
    await expect(page.locator(locators.text('Pagina Uw gegevens')).first()).toBeVisible()

    await page.locator('div[role="dialog"] button:has-text("Opslaan")').click()
    await page.waitForSelector(locators.partialTextLocator('Xxllnc presets'))
  })

  test('Add a radiobutton', async () => {
    await page.dragAndDrop('text=Radio', 'text=Sleep een component en zet hem neer in het formulier')
  })

  test('Rename radiobutton Wilt U anoniem blijven', async () => {
    await fillValueAndCheckIfFieldIsSet({ fieldLocator: page.locator('[placeholder="Veldlabel"]'), value: 'Wilt u anoniem blijven?' })
  })

  test('Fill option1', async () => {
    await page.locator('a:has-text("Data")').click()
    await fillValueAndCheckIfFieldIsSet({
      fieldLocator: page.locator('input[name="data\\[values\\]\\[0\\]\\[label\\]"]'),
      value: 'Ja, hoor',
      expectedValue: 'jaHoor',
      fieldLocatorValue: page.locator('input[name="data\\[values\\]\\[0\\]\\[value\\]"]')
    })
  })

  test('Add another', async () => {
    await page.locator('text=Nog een toevoegen').first().click()
  })

  test('Fill option2', async () => {
    await fillValueAndCheckIfFieldIsSet({
      fieldLocator: page.locator('input[name="data\\[values\\]\\[1\\]\\[label\\]"]'),
      value: 'Nee',
      expectedValue: 'nee',
      fieldLocatorValue: page.locator('input[name="data\\[values\\]\\[1\\]\\[value\\]"]')
    })
  })

  test('Go to tab Validatie and set required and only available items', async () => {
    await page.locator('a:has-text("Validatie")').click()
  })

  test('Set required', async () => {
    await expect(page.locator('input[name="data\\[validate\\.required\\]"]')).toBeVisible()
    await page.locator('input[name="data\\[validate\\.required\\]"]').check()
  })

  test('set only available items', async () => {
    await page.locator('input[name="data\\[validate\\.onlyAvailableItems\\]"]').check()
  })

  test('Save the radiobutton settings', async () => {
    await page.locator('div[role="dialog"] >> text=Opslaan').click()
    await page.waitForSelector(locators.partialTextLocator('Xxllnc presets'))
  })

  test('Add Tekstveld component for name', async () => {
    await page.dragAndDrop(
      'text=Tekstveld', '.builder-components.drag-container.formio-builder-components', { targetPosition: { x: 0, y: 100 } }
    )
  })

  test('Name Tekstveld Uw volledige naam component', async () => {
    await fillValueAndCheckIfFieldIsSet({ fieldLocator: page.locator('[placeholder="Veldlabel"]'), value: 'Uw volledige naam' })
  })

  test('Go to tab Voorwaardelijk for Tekstveld Naam', async () => {
    await page.locator('a:has-text("Voorwaardelijk")').click()
  })

  test('Set visibility1 for Tekstveld Naam', async () => {
    await expect(page.locator(locators.dropDownSelection('Dit component moet zichtbaar zijn'))).toBeVisible()
    await page.locator(locators.dropDownSelection('Dit component moet zichtbaar zijn')).click()
    await page.locator(locators.inputLabelAboveLocator('Dit component moet zichtbaar zijn')).type(
      'Juist', { delay: typeDelay })
    await page.locator(locators.partialTextLocatorByNumber('Juist', '2')).click()
  })

  test('Set visibility2 for Tekstveld Naam', async () => {
    await page.locator(locators.dropDownSelection('Als het component:')).click()
    await page.locator(locators.inputLabelAboveLocator('Als het component:')).type('Wilt u', { delay: typeDelay })
    await expect(page.locator(locators.partialTextLocator('Wilt u anoniem blijven? ('))).toBeVisible()
    await page.locator(locators.partialTextLocator('Wilt u anoniem blijven? (')).click()
  })

  test('Set visibility3 for Tekstveld Naam', async () => {
    await page.locator(locators.inputLabelAboveLocator('Deze waarde heeft:')).fill('Nee')
  })

  test('Save Tekstveld Naam', async () => {
    await page.locator('div[role="dialog"] >> text=Opslaan').click()
    await page.waitForSelector(locators.partialTextLocator('Xxllnc presets'))
  })

  // Page 2
  test('Add page 2 Pagina Uw parkeerplaats', async () => {
    await page.locator('li:has-text(" Pagina ") span >> nth=1').click()
  })

  test('Click page 2 settings', async () => {
    await expect(page.locator(locators.text('Page 2'))).toBeVisible()
    await page.locator(locators.text('Page 2')).click()
    await page.locator(locators.text('Page 2 >> nth=1')).click()
  })

  test('Open page 2 settings', async () => {
    await page.locator('[aria-label="Edit button\\. Click to open component settings modal window"]').first().click()
  })

  test('Rename page 2 to Pagina Uw parkeerplaats', async () => {
    await fillValueAndCheckIfFieldIsSet({ fieldLocator: page.locator('[placeholder="Paneeltitel"]'), value: 'Pagina Uw parkeerplaats' })
  })

  test('Save Page 2 settings', async () => {
    await page.locator('div[role="dialog"] button:has-text("Opslaan")').click()
    await page.waitForSelector(locators.partialTextLocator('Xxllnc presets'))
  })

  test('Add Kaart component', async () => {
    await page.dragAndDrop('text=Kaart', 'text=Sleep een component en zet hem neer in het formulier', { targetPosition: { x: 0, y: 0 } })
  })

  test('Name Kaart component', async () => {
    await fillValueAndCheckIfFieldIsSet({
      fieldLocator: page.locator('[placeholder="Veldlabel"]'),
      value: 'Geef de locatie van uw parkeerplaats'
    })
  })

  test('Fill BAG API key', async () => {
    await fillValueAndCheckIfFieldIsSet({
      fieldLocator: page.locator('input[name="data\\[bagApiKey\\]"]'),
      value: 'l75b78079574534027a7df1dc10f3657a5'
    })
  })

  test('Save Kaart component settings', async () => {
    await page.locator('div[role="dialog"] >> text=Opslaan').click()
    await page.waitForSelector(locators.partialTextLocator('Xxllnc presets'))
  })

  // Page 3
  test('Add a page 3 Pagina Abonnementen to the form', async () => {
    await page.locator('text=Pagina >> nth=2').click()
  })

  test('Click settings page 3', async () => {
    await expect(page.locator(locators.text('Page 3'))).toBeVisible()
    await page.locator(locators.text('Page 3')).click()
    await page.locator(locators.text('Page 3 >> nth=1')).click()
  })

  test('Open code page 3', async () => {
    await page.locator('[aria-label="Edit json button\\. Click to edit json of the current component"]').click()
  })

  test('Open component and fill with code', async () => {
    await page.locator('(//*[@class="ace_paren ace_lparen"])[2]').press('Space')
    const jsonObject = page.locator('textarea')
    await jsonObject.press('Control+a')

    await jsonObject.fill(JSON.stringify(abonnement))
  })

  test('Save settings page 3 to the form', async () => {
    await page.locator('div[role="dialog"] >> text=Opslaan').click()
    await page.waitForSelector(locators.partialTextLocator('Xxllnc presets'))
  })

  test('Scroll to top of page', async () => {
    await page.locator('text=voorbeeld').scrollIntoViewIfNeeded()
  })

  test('Add Tekstveld component', async () => {
    await page.dragAndDrop('text=Tekstveld', '.builder-components.drag-container.formio-builder-components', {
      sourcePosition: { x: 45, y: 13 },
      targetPosition: { x: 50, y: 15 }
    })
  })

  test('Name Tekstveld Door u te betalen: component', async () => {
    await fillValueAndCheckIfFieldIsSet({ fieldLocator: page.locator('[placeholder="Veldlabel"]'), value: 'Door u te betalen: ' })
  })

  test('Unset spellcheck', async () => {
    await page.locator(locators.checkbox('Spellingcontrole toestaan')).uncheck()
  })

  test('Set uitgeschakeld', async () => {
    await page.locator(locators.checkbox('Uitgeschakeld')).check()
  })
  //The next 2 tests have been added because the Eigenschapsnaam is sometimes
  //not automatically set. The checks later go wrong in that case
  test('Goto tab Api', async () => {
    await page.locator('a:has-text("API")').click()
  })

  test('Set Eigenschapsnaam Tekstveld doorUTeBetalen', async () => {
    await fillValueAndCheckIfFieldIsSet({ fieldLocator: page.locator('input[name="data\\[key\\]"]'), value: 'doorUTeBetalen' })
  })

  test('Goto tab Voorwaardelijk', async () => {
    await page.locator('a:has-text("Voorwaardelijk")').click()
  })

  test('Set visible1 when Totaal5 > 0', async () => {
    await page.locator(locators.dropDownSelection('Dit component moet zichtbaar zijn')).click()
    await page.locator(locators.inputLabelAboveLocator('Dit component moet zichtbaar zijn')).type(
      'Onjuist', { delay: typeDelay })
    await page.locator(locators.partialTextLocatorByNumber('Onjuist', '2')).click()
  })

  test('Set visible2 when Totaal5 > 0', async () => {
    await page.locator(locators.dropDownSelection('Als het component:')).click()
    await page.locator(locators.inputLabelAboveLocator('Als het component:')).type('Totaal (', { delay: typeDelay })
    await page.locator(locators.partialTextLocatorByNumber('Totaal (totaal', '2')).click()
  })

  test('Set visible3 when Totaal5 > 0', async () => {
    await fillValueAndCheckIfFieldIsSet({ fieldLocator: page.locator(locators.inputLabelAboveLocator('Deze waarde heeft:')), value: 'NaN' })
  })

  test('Set when to redraw the text field', async () => {
    await page.locator('a:has-text("Data")').click()

    await page.locator(locators.dropDownSelection('Opnieuw tekenen na')).click()
    await page.locator(locators.inputLabelAboveLocator('Opnieuw tekenen na')).type('Elke ver', { delay: typeDelay })
    await page.locator('text=Elke verandering').click()

  })

  test('Start set Berekende waarde', async () => {
    await page.locator('div[role="button"]:has-text("Berekende waarde")').click()

    await page.locator(locators.text('Enter custom javascript code.')).click({ delay: clickDelay })
    await page.locator('(//div/div[text()="1"])[3]').click({ delay: clickDelay })
  })

  test('Fill Berekende waarde', async () => {
    await page.locator('.ace_editor.ace-xcode.ace_hasPlaceholder.ace_focus .ace_scroller .ace_content').type(
      'var formatter = new Intl.NumberFormat(\"nl-Nl\",')
    await page.locator('textarea >> nth=2').press('Enter')
    await page.locator('textarea >> nth=2').type(' {')
    await page.locator('textarea >> nth=2').press('Enter')
    await page.locator('textarea >> nth=2').type('  style: \"currency\",')
    await page.locator('textarea >> nth=2').press('Enter')
    await page.locator('textarea >> nth=2').type('  currency: \"EUR\",')
    await page.locator('textarea >> nth=2').press('Enter')
    await page.locator('textarea >> nth=2').type(
      '// These options are needed to round to whole numbers if that\"s what you want.')
    await page.locator('textarea >> nth=2').press('Enter')
    await page.locator('textarea >> nth=2').type(
      '//minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)')
    await page.locator('textarea >> nth=2').press('Enter')
    await page.locator('textarea >> nth=2').type(
      '//maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)')
    await page.locator('textarea >> nth=2').press('ArrowDown')
    await page.locator('textarea >> nth=2').press('ArrowRight')
    await page.locator('textarea >> nth=2').press('ArrowRight')
    await page.locator('textarea >> nth=2').type(';')
    await page.locator('textarea >> nth=2').press('Enter')
    await page.locator('textarea >> nth=2').type(
      'value =  \"Door u te betalen: \" + formatter.format(row.totaal5);')
  })

  test('Save Tekstveld Door u te betalen: settings', async () => {
    await page.locator('div[role="dialog"] >> text=Opslaan').click()
    await page.waitForSelector(locators.partialTextLocator('Xxllnc presets'))
  })

  test('Scroll nr 2 to top of page', async () => {
    await page.locator('text=voorbeeld').scrollIntoViewIfNeeded()
  })

  test('Move nr 1 textfield down', async () => {
    await page.dragAndDrop(
      'text=Door u te betalen: ', '.builder-components.drag-container.formio-builder-components',
      { targetPosition: { x: 0, y: 300 } }
    )
  })

  test('Move nr 2 textfield down', async () => {
    await page.dragAndDrop(
      'text=Door u te betalen: ', '.builder-components.drag-container.formio-builder-components',
      { targetPosition: { x: 0, y: 800 } }
    )
  })
  // Save the form
  test('Save the form Aanvraag parkeerplaats', async () => {
    await page.locator('menu [aria-label="Opslaan"]').click()
  })

  test('Check message Formuliergegevens zijn opgeslagen to be visible', async () => {
    await expect(page.locator('text=Formuliergegevens zijn opgeslagen')).toBeVisible({ timeout: longTimeout })
    await expect(page.locator('text=Formuliergegevens zijn opgeslagen')).toBeHidden({ timeout: longTimeout })
  })

  // Test1 the form
  test('Test the form Aanvraag parkeerplaats: Maand abonnement is visible', async () => {
    await expect(page.locator(locators.partialTextLocator('Maand abonnement'))).toBeVisible()
  })

  // Test2 the form
  test('Test the form Aanvraag parkeerplaats: Werkweek abonnement is visible', async () => {
    await expect(page.locator(locators.partialTextLocator('Werkweek abonnement'))).toBeVisible()
  })

  // Test3 the form
  test('Test the form Aanvraag parkeerplaats: Iedere dag is visible ', async () => {
    await expect(page.locator(locators.partialTextLocator('Iedere dag'))).toBeVisible()
  })

  // Test4 the form
  test('Test the form Aanvraag parkeerplaats: Kies uw abonnement is visible', async () => {
    await expect(page.locator(locators.partialTextLocator('Kies uw abonnement'))).toBeVisible()
  })

  // Test5 the form
  test('Test the form Aanvraag parkeerplaats: Door u te betalen', async () => {
    await expect(page.locator(locators.partialTextLocator('Door u te betalen'))).toBeVisible()
  })

  test('Open form preview', async () => {
    await page.locator('[aria-label="Voorbeeld"]').click()
  })

  test('Check preview is open', async () => {
    const modal = page.frameLocator('#preview').locator('text=Stap 1 van 4')
    await expect.soft(modal).toBeVisible()
  })

  test('Set Wilt u anoniem blijven?', async () => {
    await page.frameLocator('#preview').locator('text=Ja, hoor').click()
  })

  test('Click Volgende', async () => {
    await page.frameLocator('#preview').locator('text=Volgende >> nth=0').click()
  })

  test('Click input Zoek een adres', async () => {
    await page.frameLocator('#preview').locator('[placeholder="Zoek een adres"]').click()
  })

  test('Type part of address', async () => {
    await page.frameLocator('#preview').locator('[placeholder="Zoek een adres"]').type(
      'Welbergweg 80', { delay: typeDelaySlow })
  })

  test('Select right address', async () => {
    await page.frameLocator('#preview').locator('text=Welbergweg 80, 7556PE Hengelo').click()
  })

  test('Click Volgende on page Uw locatie', async () => {
    await page.frameLocator('#preview').locator('text=Volgende >> nth=0').click()
  })

  test('Check Maand abonnement', async () => {
    await page.frameLocator('#preview').locator('input[name="data\\[maand\\]"]').check()
  })

  test('Fill number of maand subscriptions', async () => {
    await page.frameLocator('#preview').locator('input[name="data\\[number1\\]"]').click()
    await page.frameLocator('#preview').locator('input[name="data\\[number1\\]"]').fill('3')
  })

  test('Fill number of werkweek subscriptions without Werkweek abonnement checked', async () => {
    await page.frameLocator('#preview').locator('input[name="data\\[number2\\]"]').click()
    await page.frameLocator('#preview').locator('input[name="data\\[number2\\]"]').fill('2')
  })

  test('Check Iedere dag abonnement', async () => {
    await page.frameLocator('#preview').locator('input[name="data\\[iedereDag\\]"]').check()
  })

  test('Fill number of Iedere dag subscriptions', async () => {
    await page.frameLocator('#preview').locator('input[name="data\\[number3\\]"]').click()
    await page.frameLocator('#preview').locator('input[name="data\\[number3\\]"]').fill('1')
  })

  test('Click on Volgende on page Kies uw abonnement', async () => {
    await page.frameLocator('#preview').locator('text=Volgende >> nth=0').click()
  })

  test('Click on Vorige on page Kies uw abonnement', async () => {
    await page.frameLocator('#preview').locator('text=Vorige >> nth=0').click()
  })

  test('Check number of werkweek subscriptions without Werkweek abonnement checked', async () => {
    //By now it should have been emptied
    const numberValue = await page.frameLocator('#preview').locator('input[name="data\\[number2\\]"]').inputValue()
    expect(numberValue).toBe('')
  })

  test('Click again on Volgende on page Kies uw abonnement', async () => {
    await page.frameLocator('#preview').locator('text=Volgende >> nth=0').click()
  })

  test('Check radiobutton Ja hoor checked', async () => {
    const isChecked = await page.frameLocator('#preview').locator('//input[@role="radio" and @value="jaHoor"]').getAttribute('checked')
    expect(isChecked).toBe('true')
  })

  //TODO enable when running stable
  // test('Compare screenshot to check map selection' +
  //  '@formbeheer-bulk - advanced @formbeheer-advanced @aanvraag_parkeerplaats', async () => {
  //   await expect(page).toHaveScreenshot();
  // })

  test('Check Maand to be checked on overview', async () => {
    await page.frameLocator('#preview').locator('input[name="data\\[maand\\]"]').scrollIntoViewIfNeeded()
    const isChecked = await page.frameLocator('#preview').locator('input[name="data\\[maand\\]"]').getAttribute('checked')
    expect(isChecked).toBe('true')
  })

  test('Check Number of subscriptions of Maand', async () => {
    const numberValue = await page.frameLocator('#preview').locator('input[name="data\\[number1\\]"]').inputValue()
    expect(numberValue).toBe('3')
  })

  test('Check totaal1 value on overview', async () => {
    const totalValue = await page.frameLocator('#preview').locator('input[name="data\\[totaal1\\]"]').inputValue()
    expect(totalValue.replace(/\u00a0/g, ' ')).toBe('€ 150,00')
  })

  test('Check Werkweek to be unchecked', async () => {
    const isChecked = await page.frameLocator('#preview').locator('input[name="data\\[werkweek\\]"]').getAttribute('checked')
    expect(isChecked).toBeNull()
  })

  test('Check Number of subscriptions of Werkweek', async () => {
    const numberValue = await page.frameLocator('#preview').locator('input[name="data\\[number2\\]"]').inputValue()
    expect(numberValue).toBe('')
  })

  test('Check totaal2 value on overview', async () => {
    const totalValue = await page.frameLocator('#preview').locator('input[name="data\\[totaal2\\]"]').inputValue()
    expect(totalValue).toBe('')
  })

  test('Check Iedere dag to be checked', async () => {
    const isChecked = await page.frameLocator('#preview').locator('input[name="data\\[iedereDag\\]"]').getAttribute('checked')
    expect(isChecked).toBe('true')
  })

  test('Check Number of subscriptions of Iedere dag', async () => {
    const numberValue = await page.frameLocator('#preview').locator('input[name="data\\[number3\\]"]').inputValue()
    expect(numberValue).toBe('1')
  })

  test('Check totaal3 value on overview', async () => {
    const totalValue = await page.frameLocator('#preview').locator('input[name="data\\[totaal3\\]"]').inputValue()
    expect(totalValue.replace(/\u00a0/g, ' ')).toBe('€ 375,00')
  })

  test('Check totaal5 value on overview', async () => {
    const totalValue = await page.frameLocator('#preview').locator('input[name="data\\[totaal5\\]"]').inputValue()
    expect(totalValue.replace(/\u00a0/g, ' ')).toBe('€ 525,00')
  })

  test('Check Text on overview Door u te betalen', async () => {
    const doorUTeBetalen = await page.frameLocator('#preview').locator('input[name="data\\[doorUTeBetalen\\]"]').inputValue()
    expect(doorUTeBetalen.replace(/\u00a0/g, ' ')).toBe('Door u te betalen: € 525,00')
  })

  test('Click on confirmation box', async () => {
    await page.frameLocator('#preview').locator('input[name="data\\[confirmationCheckBox\\]"]').check()
  })

  test('Click Versturen Aanvraag parkeerplaats', async () => {
    await page.frameLocator('#preview').locator('text=Versturen').click()
  })

  test('Close preview Aanvraag parkeerplaats', async () => {
    await page.locator('[aria-label="Close"]').click()
  })
})
