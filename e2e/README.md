# E2e: Getting started

To run the e2e tests local the packages need to be installed and a .env file needs to be created.
Follow thes steps to start testing

goto the e2e directory

```
cd e2e/
```

Install the packages

```
yarn install --frozen-lockfile
```

Create a .env file

```
cp .env.example .env
```

Then edit the .env file and fill in the correct values

## Initial test run

First make sure to run this step

Some test rely on some forms. To create these forms first run 1_initial_data_other and 2_initial_filter_data
This can be done by the folowwing command

```
npx playwright test 1_initial_data_other 2_initial_filter_data
```

## start test from shell

Run all tests:

```
npx playwright test
```

Run all tests without the init test scripts:

```
npx playwright test --grep-invert 1_initial_data_other --grep-invert 2_initial_filter_data
```

## start test from vs-code

Make sure the playwright extention is installed. it is added as a recomanded extention in this project

Open the test explorer in vs-code and start a test by clicking on the play button

When no tests are shown somtimes vs-code need to be restarted.
