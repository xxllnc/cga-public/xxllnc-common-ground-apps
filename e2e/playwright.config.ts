import type { PlaywrightTestConfig } from '@playwright/test'
import { devices } from '@playwright/test'
import { TestOptions } from './e2e_tests'

/**
 * See https://playwright.dev/docs/test-configuration.
 */
const config: PlaywrightTestConfig<TestOptions> = {
  testDir: './tests',
  timeout: 30 * 1000,
  globalTimeout: process.env.CI ? 1200 * 1000 : 600 * 1000,
  expect: {
    timeout: 5000
  },
  fullyParallel: true,
  forbidOnly: !!process.env.CI,
  retries: 0,
  workers: process.env.CI ? 1 : '25%',
  reporter: 'html',
  use: {
    video: process.env.CI ? 'retain-on-failure' : 'on',
    screenshot: 'only-on-failure',
    actionTimeout: 0,
    trace: 'retain-on-failure',
    ignoreHTTPSErrors: true,
  },
  outputDir: './tests/test-screenshots/',
  projects: [
    {
      name: 'chromium',
      use: {
        ...devices['Desktop Chrome'],
      },
    },
    // {
    //   name: 'firefox',
    //   use: {
    //     ...devices['Desktop Firefox'],
    //   },
    // },

    // {
    //   name: 'webkit',
    //   use: {
    //     ...devices['Desktop Safari'],
    //   },
    // },

    /* Test against mobile viewports. */
    // {
    //   name: 'Mobile Chrome',
    //   use: {
    //     ...devices['Pixel 5'],
    //   },
    // },
    // {
    //   name: 'Mobile Safari',
    //   use: {
    //     ...devices['iPhone 12'],
    //   },
    // },

    /* Test against branded browsers. */
    // {
    //   name: 'Microsoft Edge',
    //   use: {
    //     channel: 'msedge',
    //   },
    // },
    // {
    //   name: 'Google Chrome',
    //   use: {
    //     channel: 'chrome',
    //   },
    // },
  ],
}

export const testConfig = {
  clickDelay: 200,
  clickDelaySlow: 1500,
  shortTimeout: process.env.CI ? 3000 : 1500,
  superShortTimeout: 800,
  longTimeout: 10000,
  superLongTimeout: 15000,
  softErrorMessage: 'This could be a timing error!!!',
  typeDelaySlow: 300,
  typeDelay: 50,
  options: {
    viewport: {
      width: 1200,
      height: 600
    },
    deviceScaleFactor: 0,
    recordVideo: {
      dir: './tests/test-recordings/'
    }
  }
}

export default config
