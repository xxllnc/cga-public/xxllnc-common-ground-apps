import { Page } from '@playwright/test'

export const openPage = (page: Page, url: string) =>
  page.goto(url.toString())