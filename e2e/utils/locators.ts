
export const locators = {
  text: (text: string) => `text=${text}`,
  componentJson: (text: string) => `//div[@class="ace_gutter-cell" and .//text()="${text}"]`,
  inputLabelLeftLocator: (textToReplace: string) => `(//*[.//text()="${textToReplace}"]//parent::vorm-field/descendant::input)[1]`,
  partialTextLocator: (textToReplace: string) => `(//*[contains(.//text(),"${textToReplace}")])[1]`,
  partialTextLocatorByNumber: (textToReplace: string, numberOfOccurence: string) =>
    `(//*[contains(.//text(),"${textToReplace}")])[${numberOfOccurence}]`,
  inputLabelAboveLocator: (textToReplace: string) => `(//*[contains(.//text(),"${textToReplace}")]/following::input)[1]`,
  linkLocator: (textToReplace: string) => `//a[.//text()="${textToReplace}"]`,
  inputVormFieldLocator: (textToReplace: string) => `//vorm-field[.//text()="${textToReplace}"]//input`,
  iconByDataTestid: (textToReplace: string, numberOfOccurence: string) =>
    `(//*[@data-testid="${textToReplace}"]/ancestor::a)[${numberOfOccurence}]`,
  fieldByAriaLabel: (textToReplace: string, numberOfOccurence: string) => `(//*[@aria-label="${textToReplace}"])[${numberOfOccurence}]`,
  dropDownSelection: (textToReplace: string) =>
    `//label[contains(.//text(),"${textToReplace}")]//parent::div//div[@class="form-control ui fluid selection dropdown"]`,
  checkbox: (textToReplace: string) => `//*[contains(.//text(),"${textToReplace}")]//parent::label/input`
}