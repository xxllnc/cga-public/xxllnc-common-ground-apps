import { Page, Locator } from '@playwright/test'

// sometimes more than one dialog is opened.
// This function detected if there is more than one open Dialog and closes them
export const fixMoreThanOneDialog = async (page: Page, modal: Locator): Promise<boolean> => {
  const dialogsCount = await page.locator('//div[@class="formio-dialog formio-dialog-theme-default component-settings"]').count()
  if (dialogsCount > 1) {
    for (let i = 0; i < dialogsCount; i++) {
      await page.locator(`button:has-text("Opslaan") >> nth=${dialogsCount + 1 - i}`).click()
    }
    await page.locator('text=Pagina 1').click()
    await modal.click()
  }

  return Promise.resolve(dialogsCount > 1)
}
