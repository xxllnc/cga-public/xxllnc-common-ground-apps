import { Page } from '@playwright/test'
import { locators } from '.'

interface Props {
  page: Page
  formName: string,
}

export const openEditFormPage = async ({ page, formName }: Props) => {
  await page.waitForSelector('//tr[1]/td[3][contains(.//text(),"' + formName + '")]')
  await page.locator(locators.iconByDataTestid('EditIcon', '1')).click()
}

export const openSettingsFormPage = async ({ page, formName }: Props) => {
  await page.waitForSelector('//tr[1]/td[3][contains(.//text(),"' + formName + '")]')
  await page.locator(locators.iconByDataTestid('SettingsIcon', '2')).click()
}