import { expect, Locator } from '@playwright/test'
import { testConfig } from '../playwright.config'

interface Props {
  fieldLocator: Locator,
  value: string
  expectedValue?: string,
  fieldLocatorValue?: Locator
}

export const fillValueAndCheckIfFieldIsSet = async ({
  fieldLocator, value, expectedValue = value, fieldLocatorValue = fieldLocator }: Props): Promise<void> => {

  await fieldLocator.click({ delay: testConfig.clickDelay })
  await fieldLocator.press('Control+a')
  await fieldLocator.fill(value)
  await expect(fieldLocatorValue).toHaveValue(expectedValue)
}