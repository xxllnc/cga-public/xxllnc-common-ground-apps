import { test as base } from '@playwright/test'
import dotenv from 'dotenv'
import { LoginUser } from './types'

// Read from default ".env" file.
dotenv.config()

const baseDomain = process.env?.BASE_DOMAIN

export const authorOrganization = process.env?.AUTHOR_ORGANIZATION ?? ''

export const xxllncUser: LoginUser = {
  name: process.env?.EXXELLENCE_USER_NAME ?? '',
  password: process.env?.EXXELLENCE_USER_PASSWORD ?? ''
}

export const zaakstadUser: LoginUser = {
  name: process.env?.ZAAKSTAD_USER_NAME ?? '',
  password: process.env?.ZAAKSTAD_USER_PASSWORD ?? ''
}

export const baseUrl = `https://${(baseDomain === 'vcap.me') ? 'exxellence.' : ''}${baseDomain}/`

export const getSubDomainUrl = (subDomain: string) => {
  if (subDomain === '')
    return (baseDomain === 'vcap.me') ? `https://exxellence.${baseDomain}/` : `https://${baseDomain}/`

  return `https://${subDomain}.${baseDomain}/`
}

export type TestOptions = {
  baseUrl: string
}

export const test = base.extend<TestOptions>({
  // Define an option and provide a default value.
  // We can later override it in the config.
  baseUrl: ['https://dev.cga.xxllnc.nl', { option: true }],
})

console.log({ baseDomain, authorOrganization, baseUrl })