import { expect, Page } from '@playwright/test'
import { differenceInMilliseconds, format, subDays } from 'date-fns'
import { testConfig } from '../playwright.config'

interface Props {
  page: Page
  formName: string
  makePublic?: boolean
  makeActive?: boolean
  startDate?: string
}
const { longTimeout, softErrorMessage, clickDelay } = testConfig
export const createNewForm = async ({
  page,
  formName,
  makePublic = false,
  makeActive = true,
  startDate = format(subDays(new Date(), 1), 'yyyy-MM-dd')
}: Props): Promise<void> => {

  const dateTime1 = new Date()
  console.log('Click 1 at: ' + format(dateTime1, 'd-M-yyyy HH:mm:ss.SSS'))
  await page.locator('[aria-label="Toevoegen"]').click({ delay: clickDelay })
  try {
    console.log('Before visible: ' + differenceInMilliseconds(new Date(), dateTime1))
    await expect(page.locator('text=Bron formulier')).toBeVisible()
    console.log('After visible: ' + differenceInMilliseconds(new Date(), dateTime1))
  }
  catch (err) {
    console.log('Before click 2: ' + differenceInMilliseconds(new Date(), dateTime1))
    await page.locator('[aria-label="Toevoegen"]').click()
  }
  console.log('Before fill name: ' + differenceInMilliseconds(new Date(), dateTime1))
  await page.locator('input[name="name"]').fill(formName)

  if (makeActive) {
    await page.locator('text="Actief" >> nth=0').click({ delay: clickDelay })
    await expect(page.locator('#status_ACTIVE')).toBeChecked()
    await page.locator('input[name="publishDate"]').fill(startDate)
  } else {
    await page.locator('text="Inactief" >> nth=0').click({ delay: clickDelay })
    await expect(page.locator('#status_INACTIVE')).toBeChecked()
  }

  if (makePublic) {
    await page.locator('input[name="private"]').click({ delay: clickDelay })
    await expect(page.locator('input[name="private"]')).not.toBeChecked()
  }

  await page.locator('[aria-label="Opslaan"]').click({ delay: clickDelay })

  await expect.soft(page.locator('text=Formulier is toegevoegd'), softErrorMessage).toBeVisible()
  await expect(page.locator('text=Formulier is toegevoegd')).toBeHidden({ timeout: longTimeout })
}