import { expect, Page } from '@playwright/test'
import { testConfig } from '../playwright.config'
import { LoginUser } from '../types'
import { isUrlOpened, locators, openPage } from '../utils'

interface OpenAppAndloginProps {
  page: Page
  appUrl: string
  pageTitle: string
  user: LoginUser
}
const { longTimeout } = testConfig

export const openAppAndLogin = async ({ page, appUrl, pageTitle, user }: OpenAppAndloginProps) => {
  await openPage(page, appUrl)
  await isUrlOpened(page, pageTitle)

  await expect(page.locator(locators.text('Log in om verder te gaan')).first()).toBeVisible({ timeout: longTimeout })

  await page.locator('input[type="text"]').fill(user.name)

  const buttonInloggen = page.locator('button:has-text("Inloggen")')
  await buttonInloggen.click()
  const inputPassword = page.locator('input[name="password"]')
  //Function isVisible does not wait for a timeout anymore, but returns immediately
  for (let i = 0; i < 4; i++) {
    await page.waitForLoadState('domcontentloaded')
    await page.waitForLoadState('networkidle')
    try {
      await expect(inputPassword).toBeVisible({ timeout: longTimeout })
      break
    }
    catch {
      if (await buttonInloggen.isVisible())
        await buttonInloggen.click()
      else
        break
    }
  }

  await page.locator('input[name="password"]').fill(user.password)
  await page.locator('button[name="action"]').click()

  await page.waitForLoadState('domcontentloaded')
  await page.waitForLoadState('networkidle')
  await page.waitForNavigation()
  await page.waitForLoadState('load')
}