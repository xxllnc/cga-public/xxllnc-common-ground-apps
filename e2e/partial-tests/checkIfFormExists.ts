import { Page } from '@playwright/test'
import { setFilterAndExpectOneItemFound } from '.'

interface Props {
  page: Page
  formName: string
}

export const checkIfFormExists = async ({ page, formName }: Props) => {
  await page.waitForSelector('//span[.//text()="Privé"]//parent::div')
  // eslint-disable-next-line playwright/no-eval
  const isPrivate = await page.$eval(
    '//span[.//text()="Privé"]//parent::div',
    el => el.getAttribute('data-selected')
  )

  // eslint-disable-next-line playwright/no-eval
  const isPublic = await page.$eval(
    '//span[.//text()="Openbaar"]//parent::div',
    el => el.getAttribute('data-selected')
  )

  if (isPrivate === 'true') {
    await page.locator('div[role="button"]:has-text("Privé")').click()
    await Promise.all([
      page.waitForLoadState('domcontentloaded'),
      page.waitForLoadState('networkidle')
    ])
  }

  if (isPublic === 'true') {
    await page.locator('div[role="button"]:has-text("Openbaar")').click()
    await Promise.all([
      page.waitForLoadState('domcontentloaded'),
      page.waitForLoadState('networkidle')
    ])

  }

  await setFilterAndExpectOneItemFound({ page, filterText: formName })
}