import { expect, Page } from '@playwright/test'
import { testConfig } from '../playwright.config'

const { clickDelay } = testConfig
const urlLogoffEnd = 'login'

interface LogOff {
  page: Page
  appUrl: string
}
export const logOff = async ({ page, appUrl }: LogOff): Promise<void> => {

  await page.locator('[aria-label="Profiel"]').click()

  try {
    await expect(await page.locator('li[role="menuitem"]:has-text("Uitloggen")')).toBeVisible()
  }
  catch (err) {
    await page.locator('[aria-label="Profiel"]').click()
  }

  await page.locator('li[role="menuitem"]:has-text("Uitloggen")').click({ delay: clickDelay })

  await expect(page).toHaveURL(appUrl + urlLogoffEnd)

}