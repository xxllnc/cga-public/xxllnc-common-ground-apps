import { expect, Page } from '@playwright/test'
import { testConfig } from '../playwright.config'
import { locators } from '../utils'
import { setFilterAndGetCountFound, setFilterAndReturnTrueIfFound } from './ItemSearch'
interface Delete {
  page: Page
  formName: string
  deleteAllFound?: boolean
}
export const deleteFormFromOverviewScreen = async ({ page, formName, deleteAllFound = false }: Delete): Promise<void> => {
  const { clickDelay, longTimeout, softErrorMessage } = testConfig

  await page.locator('[data-testid="DescriptionIcon"] path').click()

  page.waitForLoadState()

  const formExist = await setFilterAndReturnTrueIfFound({ page, filterText: formName })
  if (!formExist) {
    console.log('No "' + formName + '" is found. No delete performed')
    return
  }

  const countFound = await setFilterAndGetCountFound({ page, filterText: formName })

  if (!deleteAllFound && countFound > 1) {
    console.log('More than one formname is found and just 1 needs to be deleted. No delete performed')
    return
  }

  const textVerwijderd = countFound > 1 ? `text=${countFound} elementen verwijderd` : 'text=Element verwijderd'
  const locator = countFound > 1 ? '[aria-label="Alles selecteren"]' : locators.fieldByAriaLabel('Selecteer rij', '1')

  await page.locator(locator).click({ delay: clickDelay })
  await Promise.all([
    page.waitForSelector(locator),
    page.waitForLoadState('domcontentloaded')
  ])

  await page.locator('[aria-label="Verwijderen"]').click({ delay: clickDelay })
  await page.locator('button:has-text("Bevestigen")').click({ delay: clickDelay })

  await expect.soft(page.locator(textVerwijderd), softErrorMessage).toBeVisible({ timeout: longTimeout })
  await expect(page.locator(textVerwijderd)).toBeHidden({ timeout: longTimeout })
  await page.waitForSelector('text=Geen resultaten gevonden')
}


