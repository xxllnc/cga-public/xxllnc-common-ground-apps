import { expect, Page } from '@playwright/test'
import { testConfig } from '../playwright.config'

const { longTimeout } = testConfig
interface Props {
  page: Page
  filterText: string
}

export const waitForResponseAndCheckUrl = async (page: Page, regExpTxt: string): Promise<void> => {

  await page.waitForResponse(res => res.status() === 200)
  await Promise.all([
    page.waitForLoadState('domcontentloaded'),
    page.waitForLoadState('networkidle')
  ])

  await expect(page).toHaveURL(new RegExp(regExpTxt))
}

export const setFilterAndGetCountFound = async ({ page, filterText }: Props): Promise<number> => {

  await page.locator('input[name="q"]').click()
  // reload page if Filter is already set
  const searchText = await page.locator('input[name="q"]').inputValue()
  if (searchText.toString() === filterText) {
    await page.reload()
    await page.locator('[aria-label="Veld wissen"]').click()

    await waitForResponseAndCheckUrl(page, '.*filter=%7B%7D&order=ASC&page=1&perPage=10&sort=name')
  }

  await page.locator('input[name="q"]').fill(filterText)
  await waitForResponseAndCheckUrl(page, `.*${encodeURIComponent(filterText)}.*`)

  const noResult = page.locator('text=Geen resultaten gevonden')
  const result = page.locator('//tr[1]/td[3][contains(.//text(),"' + filterText + '")]')
  //Function isVisible does not wait for a timeout anymore, but returns immediately
  for (let i = 0; i < 8; i++) {
    if (await noResult.isVisible())
      return 0
    else {
      if (await result.isVisible()) {
        break
      }
    }
  }

  const displayedRows = page.locator('//*[contains(@class,"MuiTablePagination-displayedRows")]')
  await expect(displayedRows).toBeVisible()

  return parseInt((await displayedRows.innerText()).split(' ')[2], 10)
}

export const setFilterAndExpectOneItemFound = async ({ page, filterText }: Props): Promise<void> => {

  expect(await setFilterAndGetCountFound({ page, filterText })).toEqual(1)
}

export const setFilterAndReturnTrueIfFound = async ({ page, filterText }: Props): Promise<boolean> =>
  await setFilterAndGetCountFound({ page, filterText }) > 0

