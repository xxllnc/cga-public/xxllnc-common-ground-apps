#!/usr/bin/env python3

"""Module for creating initial data for the database"""

from app.db import models
from app.db.session import SessionLocal
from datetime import date
from sqlalchemy import text
from sqlalchemy.orm import Session


def truncate_tables() -> None:
    """Truncate tables"""

    database: Session = SessionLocal()

    database.execute(text('truncate table "request" cascade'))

    database.commit()


def create_initial_data() -> None:
    """Creates initial data for database"""
    database: Session = SessionLocal()

    request1 = models.Request(
        id="c16ae581-8d6f-424e-bff3-3a70a1374f0a",
        registration_date=date(2021, 6, 18),
        form_uuid="11111111-3333-4444-a555-666666666666",
        status="INACTIVE",
        form_input={},
    )

    request2 = models.Request(
        id="69c08406-3984-4344-9b6d-6f4adec2a4b7",
        registration_date=date(2020, 10, 7),
        form_uuid="11111111-3333-4444-a555-666666666666",
        status="INACTIVE",
        form_input={},
    )

    database.add(request1)
    database.add(request2)

    database.flush()
    database.commit()


if __name__ == "__main__":
    print("First remove any existing data")
    truncate_tables()
    print("Creating initial data")
    create_initial_data()
    print("Initial data created")
