"""Module for all request api routes"""
import json
from app import domain, schemas
from app.core import config
from app.core.auth0 import validate_optional_user_token
from app.core.constant import (
    CONFIG_ITEM_CASE_SYSTEM,
    CONFIG_ITEM_NOTIFICATION_URL,
    CONFIG_ITEM_SEND_NOTIFICATION,
)
from app.db.session import get_db
from app.services import zaaksysteem_service
from app.services.commonground_service import get_resource
from app.services.helper_functions import _get_config_item_value, get_config_item
from app.services.notification_service import NotifyActions, notify
from app.services.validation.validation_service import ValidationService
from exxellence_shared_cga.core.rest_helpers import SearchParameters
from exxellence_shared_cga.core.types import SemanticError, SemanticErrorType
from fastapi import APIRouter, Depends, Header
from fastapi import status as http_status
from loguru import logger
from requests import Response as RequestResponse
from sqlalchemy.orm import Session
from typing import Union

validation_service = ValidationService()

router = r = APIRouter()


# validate the filter and sort parameters
def validate_search_parameters():
    return SearchParameters(
        filter_options_schema=schemas.RequestFilterableFields,
        sort_options_schema=schemas.RequestSortableFields,
    )


def _send_notification(config_items: list) -> bool:
    notify = _get_config_item_value(config_items, CONFIG_ITEM_SEND_NOTIFICATION)
    return notify["value"].lower() == "true" if notify else False


def _notification_url(config_items: list) -> Union[str, None]:
    notify_url = _get_config_item_value(config_items, CONFIG_ITEM_NOTIFICATION_URL)
    return notify_url["value"] if notify_url else None


def get_notification_body(form_resource: Union[bool, RequestResponse], body: dict) -> dict:
    if not isinstance(form_resource, RequestResponse):
        return body

    form_object = form_resource.json()
    if form_object and "config" in form_object and isinstance(form_object["config"], list):
        config_items: list = form_object["config"]

        api_test_case_type_url = _get_config_item_value(config_items, "apiTestCaseTypeUrl")
        if not api_test_case_type_url:
            return body

        api_test_body = _get_config_item_value(config_items, "apiTestBody")
        if not api_test_body:
            return body

        api_test_body_value = json.loads(api_test_body["value"])
        api_test_body_value["zaaktype"] = str(api_test_case_type_url["value"])
        api_test_body_value["omschrijving"] = str(body)
        return api_test_body_value

    return body


def get_notification_url(
    form_resource: Union[bool, RequestResponse], url: str
) -> Union[str, None]:
    """
    Return the notification url where the notification should be send to.
    Returns None if no notification should be send.

    :param form_resource: The form_resource contains the form
    :type form_resource: bool or RequestResponse
    :param url: the default notification URL
    :type url: str
    :return: notification url if a notification should be send, None otherwise
    :rtype: str or None
    """
    if not isinstance(form_resource, RequestResponse):
        return None

    form_object = form_resource.json()
    if form_object and "config" in form_object and isinstance(form_object["config"], list):
        config_items: list = form_object["config"]

        # Check if a notification should be send.
        if _send_notification(config_items):
            logger.info("_send_notification")
            # Use default url or a specific url.
            notify_url = _notification_url(config_items)
            return notify_url if notify_url else url

    return None


@r.post(
    "/requests",
    response_model=schemas.Request,
    status_code=http_status.HTTP_201_CREATED,
    responses={
        http_status.HTTP_409_CONFLICT: {"description": "Conflict. Request not created."},
    },
    dependencies=[Depends(validate_optional_user_token)],
)
def create_request(
    request_create_request: schemas.RequestBase,
    organization_url: str = Header(...),
    database: Session = Depends(get_db),
):

    # validate the request
    validation_result = validation_service.validate_request(
        request_create_request, organization_url, database
    )
    if not validation_result["valid"]:
        raise SemanticError(
            loc={
                "source": "body",
                "field": "formInput",
                "detail": validation_result["error"],
            },
            msg=("Er zijn fouten gevonden tijdens de validatie van het formulier"),
            type=SemanticErrorType.invalid_input,
        )

    request = domain.request.create_request(
        database=database,
        request_create_request=schemas.RequestBase(
            form_uuid=request_create_request.form_uuid,
            status=request_create_request.status,
            form_input=request_create_request.form_input,
        ),
    )

    form_resource: Union[bool, RequestResponse] = get_resource(
        f"{config.get_active_forms_api_url(organization_url)}/{request.form_uuid}"
    )

    notification_url: Union[None, str] = get_notification_url(
        form_resource,
        config.NOTIFICATION_URL,
    )

    if notification_url:
        response = notify(
            NotifyActions.CREATE,
            str(request.id),
            get_notification_body(form_resource, request.form_input),
            notification_url,
        )
        logger.trace(f"Result notification send: {response}")
    else:
        logger.info("No notification send")

    case_system: Union[str, None] = get_config_item(form_resource, CONFIG_ITEM_CASE_SYSTEM)

    if case_system is not None and case_system == "zaaksysteem.nl":
        zaaksysteem_service.create(request, form_resource)

    return request
