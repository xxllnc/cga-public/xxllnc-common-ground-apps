import uuid
from app import domain, schemas
from app.core.auth0 import get_admin_scopes
from app.db.session import get_db
from app.resources import error_messages, errors
from exxellence_shared_cga.core.rest_helpers import SearchParameters
from exxellence_shared_cga.core.types import GenericId, IdType, SemanticError
from fastapi import APIRouter, Depends, Response
from fastapi import status as http_status
from sqlalchemy.orm import Session
from typing import List

router = r = APIRouter()


# validate the filter and sort parameters
def validate_search_parameters():
    return SearchParameters(
        filter_options_schema=schemas.StatusFilterableFields,
        sort_options_schema=schemas.StatusSortableFields,
    )


@r.get(
    "/status/{id}",
    response_model=schemas.StatusDetails,
    responses={http_status.HTTP_404_NOT_FOUND: {"description": "Status not found"}},
    dependencies=[Depends(get_admin_scopes)],
)
def get_status(id: uuid.UUID, database: Session = Depends(get_db)):
    """Returns a single status, given its unique id"""

    try:
        status = domain.status.get_status(database=database, id=id)
    except SemanticError as semantic_error:
        errors.not_found_error(semantic_error, error_messages.STATUS_NOT_FOUND_WITH.format(id))

    return status


@r.get(
    "/status",
    response_model=List[schemas.StatusDetails],
    dependencies=[Depends(get_admin_scopes)],
)
def get_statusses(
    response: Response,
    database: Session = Depends(get_db),
    search_parameters: dict = Depends(validate_search_parameters()),
):
    """Returns a list of statusses"""

    search_result = domain.status.get_status_list(
        database=database,
        **search_parameters,
    )

    response.headers["Content-Range"] = search_result.as_content_range("results")

    return search_result.result


@r.post(
    "/status",
    response_model=schemas.StatusDetails,
    status_code=http_status.HTTP_201_CREATED,
    responses={
        http_status.HTTP_409_CONFLICT: {"description": "Conflict. Status not created."},
    },
    dependencies=[Depends(get_admin_scopes)],
)
def create_status(
    status_create_request: schemas.StatusCreateRequest,
    database: Session = Depends(get_db),
):

    status = domain.status.add_new_status(
        db=database,
        obj_in=status_create_request,
    )

    return status


@r.put(
    "/status/{id}",
    response_model=schemas.StatusDetails,
    responses={
        http_status.HTTP_400_BAD_REQUEST: {"description": "Status not updated"},
        http_status.HTTP_404_NOT_FOUND: {"description": "Status not found"},
        http_status.HTTP_409_CONFLICT: {"description": "Conflict. Status not updated."},
    },
    dependencies=[Depends(get_admin_scopes)],
)
def update_status(
    id: uuid.UUID,
    status_update_request: schemas.Status,
    database: Session = Depends(get_db),
):

    try:
        domain.status.get_status(database=database, id=id)
    except SemanticError as semantic_error:
        errors.not_found_error(
            semantic_error,
            error_messages.STATUS_NOT_FOUND_WITH.format(id),
        )
    # update the status
    try:
        updated_status = domain.status.update_status(
            database=database,
            id=id,
            status_update_request=status_update_request,
        )
    except SemanticError as semantic_error:
        errors.not_found_error(
            semantic_error,
            error_messages.STATUS_NOT_FOUND_WITH.format(id),
        )

    return updated_status


@r.delete(
    "/status/{id}",
    responses={
        http_status.HTTP_404_NOT_FOUND: {"description": "Status not found"},
        http_status.HTTP_409_CONFLICT: {"description": "Conflict. Status not deleted."},
    },
    dependencies=[Depends(get_admin_scopes)],
)
def delete_status(id: uuid.UUID, database: Session = Depends(get_db)):

    try:
        id_type = IdType.uuid if isinstance(id, uuid.UUID) else IdType.sid
        domain.status.delete(db=database, id=GenericId(id=id, type=id_type))
    except SemanticError as semantic_error:
        errors.not_found_error(
            semantic_error,
            error_messages.STATUS_NOT_FOUND_WITH.format(id),
        )

    return {"message": error_messages.STATUS_IS_DELETED.format(id)}
