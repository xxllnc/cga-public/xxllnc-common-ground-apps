from app import schemas
from app.core.auth0 import validate_optional_user_token
from app.services.s3 import delete_object, get_object, upload_stream_to_s3
from fastapi import APIRouter, Depends, File, Form, Request, UploadFile
from fastapi import status as http_status

router = r = APIRouter()


@r.post(
    "/requests/upload",
    response_model=schemas.FileUploadResponse,
    status_code=http_status.HTTP_200_OK,
    dependencies=[Depends(validate_optional_user_token)],
)
def upload_file(request: Request, file: UploadFile = File(...), name: str = Form()):

    result = upload_stream_to_s3(
        organization_url=request.headers["origin"], unique_filename=name, stream=file.file
    )
    return result


@r.delete(
    "/requests/file/{instance_id}/{unique_filename}",
    status_code=202,  # Always return 202
    dependencies=[Depends(validate_optional_user_token)],
)
def delete_file(request: Request, instance_id: str, unique_filename: str):

    delete_object(request.headers["origin"], instance_id, unique_filename)


@r.get(
    "/requests/file/{instance_id}/{unique_filename}",
    status_code=202,  # Always return 202
    dependencies=[Depends(validate_optional_user_token)],
)
def get_file(request: Request, instance_id: str, unique_filename: str):

    get_object(request.headers["origin"], instance_id, unique_filename)
