import pytest
from app.api.api_v1.routers.tests import HEADERS, REQUESTS, REQUESTS_ADMIN
from app.api.api_v1.routers.tests.asserts import assert_method_not_allowed
from app.api.api_v1.routers.tests.helper import empty_body
from app.core.config import BASE_PATH_V1
from fastapi import status as http_status
from fastapi.testclient import TestClient


# ---------------------------------------------
# Testing an API for a given HTTP method without path parameter id.
# Reject request with HTTP response code 405 Method not allowed.
# ---------------------------------------------
@pytest.mark.parametrize("request_body", [None, empty_body])
@pytest.mark.parametrize(
    "url, http_method, status_code",
    [
        (REQUESTS_ADMIN, "DELETE", None),
        (REQUESTS_ADMIN, "PATCH", None),
        (REQUESTS_ADMIN, "POST", None),
        (REQUESTS_ADMIN, "PUT", None),
        (REQUESTS, "DELETE", None),
        (REQUESTS, "PATCH", None),
        (REQUESTS, "POST", http_status.HTTP_422_UNPROCESSABLE_ENTITY),
        (REQUESTS, "PUT", None),
    ],
)
def test_http_method_without_required_path_parameter(
    client: TestClient,
    url: str,
    http_method: str,
    request_body: None | dict,
    status_code: None | int,
):
    response = client.request(
        method=http_method, url=f"{BASE_PATH_V1}{url}", json=request_body, headers=HEADERS
    )
    if status_code:
        assert response.status_code == status_code
    else:
        assert_method_not_allowed(response, http_method)
