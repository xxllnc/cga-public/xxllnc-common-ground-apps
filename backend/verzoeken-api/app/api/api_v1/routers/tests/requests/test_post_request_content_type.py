import pytest
from app.api.api_v1.routers.tests import CONTENT_TYPE_VALUE_APPLICATION_JSON, HEADERS, REQUESTS
from app.api.api_v1.routers.tests.asserts import (
    assert_415_unsupported_media_type,
    assert_form_created,
)
from app.api.api_v1.routers.tests.helper import form1
from app.core.config import BASE_PATH_V1
from fastapi.testclient import TestClient


# ---------------------------------------------
# Test post request without a request content type.
# The body contains JSON data.
# Content type is not mandatory.
# A sender that generates a message containing a payload body SHOULD generate
# a Content-Type header field in that message unless the intended media type
# of the enclosed representation is unknown to the sender.
# The missing content type is ignored.The response Content-Type is always 'application/json'.
# ---------------------------------------------
def test_post_request_without_request_header_content_type(client: TestClient):
    response = client.post(f"{BASE_PATH_V1}{REQUESTS}", json=form1.dict(), headers=HEADERS)
    assert_form_created(response, form1)


# ---------------------------------------------
# Test post request with a request content type application/json.
# The body contains JSON data.
# The Accept header is ignored. the response Content-Type is always 'application/json'.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_accept",
    [
        "application/javascript",
        CONTENT_TYPE_VALUE_APPLICATION_JSON,  # allowed
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "not valid",
        "application/*",
        "*/*",
        "",
        None,
    ],
)
def test_post_request_with_request_header_content_type_application_json(
    client: TestClient, http_request_header_accept: str
):
    headers = {
        **HEADERS,
        "Accept": f"{http_request_header_accept}",
        "Content-Type": CONTENT_TYPE_VALUE_APPLICATION_JSON,
    }

    response = client.post(f"{BASE_PATH_V1}{REQUESTS}", headers=headers, json=form1.dict())
    assert_form_created(response, form1)


# ---------------------------------------------
# Test post request with an invalid request content type.
# The body contains JSON data.
# The Content-Type header attribute specifies the format of the data in the
# request body so that receiver can parse it into appropriate format.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_content_type",
    [
        "aplication/javascript",
        "aplication/json",
        "application/javascript",
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "not valid",
        None,
    ],
)
def test_post_request_with_request_header_content_type_with_invalid_value(
    client: TestClient, http_request_header_content_type: str
):
    headers = {**HEADERS, "Content-Type": f"{http_request_header_content_type}"}

    response = client.post(f"{BASE_PATH_V1}{REQUESTS}", headers=headers, json=form1.dict())

    assert_415_unsupported_media_type(response)
