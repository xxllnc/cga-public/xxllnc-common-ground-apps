import json
import pytest
from app.api.api_v1.routers.requests import (
    _notification_url,
    _send_notification,
    get_notification_url,
)
from app.api.api_v1.routers.tests import NOTIFICATION_URL
from app.core.constant import CONFIG_ITEM_NOTIFICATION_URL, CONFIG_ITEM_SEND_NOTIFICATION
from requests import Response as RequestResponse
from typing import Union
from unittest.mock import Mock


# ---------------------------------------------
# Test _get_config_item_value function
# ---------------------------------------------
@pytest.mark.parametrize(
    "config_items, item_name, expect_result",
    [
        ([], CONFIG_ITEM_SEND_NOTIFICATION, False),
        (
            [{"key": CONFIG_ITEM_SEND_NOTIFICATION, "value": "false"}],
            CONFIG_ITEM_SEND_NOTIFICATION,
            True,
        ),
        (
            [{"key": CONFIG_ITEM_SEND_NOTIFICATION, "value": "true"}],
            CONFIG_ITEM_SEND_NOTIFICATION,
            True,
        ),
        (
            [{"key": "send_notification", "value": "false"}],
            CONFIG_ITEM_SEND_NOTIFICATION,
            False,
        ),
        (
            [{"key": CONFIG_ITEM_SEND_NOTIFICATION.lower(), "value": "true"}],
            CONFIG_ITEM_SEND_NOTIFICATION,
            False,
        ),
    ],
)
def test_get_config_item_value(config_items: list, item_name: str, expect_result: bool):
    config_item = next(
        (item for item in config_items if item["key"] == item_name),
        None,
    )
    if expect_result:
        assert isinstance(config_item, dict)
    else:
        assert config_item is None


# ---------------------------------------------
# Test _notification_url function
# ---------------------------------------------
@pytest.mark.parametrize(
    "config_items, expected_result",
    [
        ([], None),
        ([{"key": "notification_url", "value": "false"}], None),
        (
            [
                {
                    "key": CONFIG_ITEM_NOTIFICATION_URL,
                    "value": "https://example.com",
                }
            ],
            "https://example.com",
        ),
    ],
)
def test_notification_url(config_items: list, expected_result: Union[str, None]):
    notify_url: Union[str, None] = _notification_url(config_items)
    if notify_url:
        assert notify_url == expected_result
    else:
        assert notify_url is expected_result


# ---------------------------------------------
# Test _send_notification function
# ---------------------------------------------
@pytest.mark.parametrize(
    "config_items, expected_result",
    [
        ([], False),
        ([{"key": "send_notification", "value": "false"}], False),
        ([{"key": CONFIG_ITEM_SEND_NOTIFICATION, "value": "false"}], False),
        ([{"key": CONFIG_ITEM_SEND_NOTIFICATION, "value": "true"}], True),
        ([{"key": CONFIG_ITEM_SEND_NOTIFICATION, "value": "TrUe"}], True),
    ],
)
def test_send_notification(config_items: list, expected_result: bool):
    assert _send_notification(config_items) == expected_result


# ---------------------------------------------
# Test get_notification_url function
# Test with form_resource type bool
# ---------------------------------------------
@pytest.mark.parametrize("form_resource_type", [False, True])
def test_get_notification_url(form_resource_type: bool):
    assert get_notification_url(form_resource_type, NOTIFICATION_URL) is None


# ---------------------------------------------
# Test get_notification_url function
# ---------------------------------------------
@pytest.mark.parametrize(
    "response_return_value, expect_result, expect_url",
    [
        ("{}", False, None),
        ("[]", False, None),
        (f'{{"{CONFIG_ITEM_SEND_NOTIFICATION}": "false"}}', False, None),
        (f'{{"{CONFIG_ITEM_SEND_NOTIFICATION}": "true"}}', False, None),
        (
            f'{{"key": "{CONFIG_ITEM_SEND_NOTIFICATION}", "value": "false"}}',
            False,
            None,
        ),
        (
            f'{{"key": "{CONFIG_ITEM_SEND_NOTIFICATION}", "value": "true"}}',
            False,
            None,
        ),
        (
            f'{{"config": [{{"key": "{CONFIG_ITEM_SEND_NOTIFICATION}", "value": "false"}}]}}',  # noqa: E501
            False,
            None,
        ),
        (
            f'{{"config": [{{"key": "{CONFIG_ITEM_SEND_NOTIFICATION}", "value": "true"}}]}}',  # noqa: E501
            True,
            NOTIFICATION_URL,
        ),
        (
            f'{{"config": [{{"key": "key1", "value": "value1"}}, {{"key": "{CONFIG_ITEM_SEND_NOTIFICATION}", "value": "false"}}, {{"key": "key2", "value": "value2"}}]}}',  # noqa: E501
            False,
            None,
        ),
        (
            f'{{"config": [{{"key": "key1", "value": "value1"}}, {{"key": "{CONFIG_ITEM_SEND_NOTIFICATION}", "value": "true"}}, {{"key": "key2", "value": "value2"}}]}}',  # noqa: E501
            True,
            NOTIFICATION_URL,
        ),
        (
            f'{{"config": [{{"key": "key1", "value": "value1"}}, {{"key": "{CONFIG_ITEM_SEND_NOTIFICATION}", "value": "false"}}, {{"key": "{CONFIG_ITEM_NOTIFICATION_URL}", "value": "https://example.com"}}]}}',  # noqa: E501
            False,
            None,
        ),
        (
            f'{{"config": [{{"key": "key1", "value": "value1"}}, {{"key": "{CONFIG_ITEM_SEND_NOTIFICATION}", "value": "true"}}, {{"key": "{CONFIG_ITEM_NOTIFICATION_URL}", "value": "https://example.com"}}]}}',  # noqa: E501
            True,
            "https://example.com",
        ),
    ],
)
def test_send_notification_response(
    response_return_value: str, expect_result: bool, expect_url: str
):
    response = Mock(spec=RequestResponse)
    response.json.return_value = json.loads(response_return_value)
    result: Union[str, None] = get_notification_url(response, NOTIFICATION_URL)
    if expect_result:
        assert result == expect_url
    else:
        assert result is None
