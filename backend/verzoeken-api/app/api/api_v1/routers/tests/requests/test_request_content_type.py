import app.services.validation.validation_service_constants as vsc
import pytest
from app.api.api_v1.routers.tests import (
    CONTENT_TYPE_VALUE_APPLICATION_JSON,
    FORMULIERENBEHEER_ACTIVE_FORMS_API,
    HEADERS,
    REQUESTS,
)
from app.api.api_v1.routers.tests.asserts import (
    assert_415_unsupported_media_type,
    assert_invalid_input,
)
from app.api.api_v1.routers.tests.conftest import default_body
from app.api.api_v1.routers.tests.test_mock_data import add_request_mock_data_to_db
from app.core.config import BASE_PATH_V1
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session


# ---------------------------------------------
# Test post request without a request content type.
# The body contains JSON data.
# Content type is not mandatory.
# A sender that generates a message containing a payload body SHOULD generate
# a Content-Type header field in that message unless the intended media type
# of the enclosed representation is unknown to the sender.
# The missing content type is ignored.The response Content-Type is always 'application/json'.
# ---------------------------------------------
def test_post_request_without_request_header_content_type(client: TestClient, test_db: Session):
    mock = add_request_mock_data_to_db(test_db)

    # formUuid should contain an uuid instead of form data.
    body = default_body(mock)
    response = client.post(f"{BASE_PATH_V1}{REQUESTS}", json=body, headers=HEADERS)

    error_msg = vsc.ERROR_MESSAGE_FORM_URL_NOT_VALID.format(
        f'{FORMULIERENBEHEER_ACTIVE_FORMS_API}/{body["formUuid"]}'
    )
    assert_invalid_input(response=response, error_msg=error_msg)

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test post request with a request content type application/json.
# The body contains JSON data.
# The Accept header is ignored. the response Content-Type is always 'application/json'.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_accept",
    [
        "application/javascript",
        CONTENT_TYPE_VALUE_APPLICATION_JSON,  # allowed
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "not valid",
        "application/*",
        "*/*",
        "",
        None,
    ],
)
def test_post_request_with_request_header_content_type_application_json(
    client: TestClient, http_request_header_accept: str, test_db: Session
):
    mock = add_request_mock_data_to_db(test_db)

    headers = {
        **HEADERS,
        "Accept": f"{http_request_header_accept}",
        "Content-Type": CONTENT_TYPE_VALUE_APPLICATION_JSON,
    }

    # formUuid should contain an uuid instead of form data.
    body = default_body(mock)

    response = client.post(f"{BASE_PATH_V1}{REQUESTS}", headers=headers, json=body)

    error_msg = vsc.ERROR_MESSAGE_FORM_URL_NOT_VALID.format(
        f'{FORMULIERENBEHEER_ACTIVE_FORMS_API}/{body["formUuid"]}'
    )
    assert_invalid_input(response=response, error_msg=error_msg)

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test post request with an invalid request content type.
# The body contains JSON data.
# The Content-Type header attribute specifies the format of the data in the
# request body so that receiver can parse it into appropriate format.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_content_type",
    [
        "application/javascript",
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "not valid",
        None,
    ],
)
def test_post_request_with_request_header_content_type_with_invalid_value(
    client: TestClient, http_request_header_content_type: str, test_db: Session
):
    mock = add_request_mock_data_to_db(test_db)

    headers = {**HEADERS, "Content-Type": f"{http_request_header_content_type}"}

    # formUuid should contain an uuid instead of a form io object
    body = default_body(mock)

    response = client.post(f"{BASE_PATH_V1}{REQUESTS}", headers=headers, json=body)

    assert_415_unsupported_media_type(response)
