import app.services.validation.tests.test_validate as tv
import app.services.validation.validation_service_constants as vsc
import pytest
from app import schemas
from app.api.api_v1.routers.tests import FORMULIERENBEHEER_ACTIVE_FORMS_API, HEADERS, REQUESTS
from app.api.api_v1.routers.tests.asserts import (
    assert_form_created,
    assert_invalid_input,
    get_msg_from_detail,
)
from app.api.api_v1.routers.tests.conftest import default_body
from app.api.api_v1.routers.tests.helper import form1, form2, form3
from app.api.api_v1.routers.tests.test_mock_data import add_request_mock_data_to_db
from app.core.config import BASE_PATH_V1
from app.resources.error_messages import FIELD_REQUIRED, VALUE_ERROR_MISSING
from fastapi import status as http_status
from fastapi.testclient import TestClient
from requests.models import Response
from sqlalchemy.orm import Session
from unittest.mock import Mock


# ---------------------------------------------
# Test post request with an empty body.
# ---------------------------------------------
def test_post_request_with_empty_body(client: TestClient):
    response = client.post(f"{BASE_PATH_V1}{REQUESTS}", json={})
    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()

    assert len(res) == 1
    assert len(res["detail"]) == 4
    assert len(res["detail"][0]) == 3
    for index in [1, 2, 3]:
        assert len(res["detail"][index]["loc"]) == 2
        assert res["detail"][index]["loc"]["source"] == "body"
        assert res["detail"][index]["msg"] == FIELD_REQUIRED
        assert res["detail"][index]["type"] == VALUE_ERROR_MISSING
    assert res["detail"][0]["loc"]["field"] == "organization-url"
    assert res["detail"][1]["loc"]["field"] == "formUuid"
    assert res["detail"][2]["loc"]["field"] == "status"
    assert res["detail"][3]["loc"]["field"] == "formInput"


# ---------------------------------------------
# Test post request with an invalid formUuid value.
# ---------------------------------------------
@pytest.mark.parametrize(
    "form_uuid",
    [
        ("{11111111-3333-4444-a555-666666666666-11"),
        ("https://google.nl"),
        (""),
        ("__"),
        ('{"name": "formulier a","comp":""}'),
        ("{}"),
        ("[]"),
        (
            "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890"  # noqa: E501
        ),
        (
            "12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901"  # noqa: E501
        ),
    ],
)
def test_post_request_invalid_form_uuid_value(client: TestClient, form_uuid: str):
    body = form1.dict(by_alias=True)
    body["formUuid"] = form_uuid
    response = client.post(f"{BASE_PATH_V1}{REQUESTS}", json=body, headers=HEADERS)

    error_msg = vsc.ERROR_MESSAGE_FORM_URL_NOT_VALID.format(
        f"{FORMULIERENBEHEER_ACTIVE_FORMS_API}/{form_uuid}"
    )
    assert_invalid_input(response=response, error_msg=error_msg)


# ---------------------------------------------
# Test post request with an invalid mimetype.
# ---------------------------------------------
def test_post_request_invalid_mime_type(client: TestClient):
    body = form2.dict(by_alias=True)
    response = client.post(f"{BASE_PATH_V1}{REQUESTS}", json=body, headers=HEADERS)
    assert_invalid_input(response=response)
    res = response.json()
    tv._assert_field_attachment_unsupported_mime_type(get_msg_from_detail(res)[0], "blaat")


# ---------------------------------------------
# Test post request with a valid mimetype.
# ---------------------------------------------
def test_post_request_valid_mime_type(client: TestClient, mocker: Mock):

    mocker.patch(
        "app.services.s3._upload_stream_to_s3",
        return_value=schemas.FileUploadResponse(
            url=f"https://test.nl/app_instance_id/{form3.form_input['file'][0]['name']}", size=1234
        ),
    )

    body = form3.dict(by_alias=True)
    response = client.post(f"{BASE_PATH_V1}{REQUESTS}", json=body, headers=HEADERS)

    assert response.status_code == http_status.HTTP_400_BAD_REQUEST
    res = response.json()

    assert len(res["detail"]) == 1
    print(res["detail"])
    assert res["detail"] == [
        {
            "loc": {"source": "body", "field": "formInput.file.storage"},
            "msg": "Unknwon storage. Only 'url' is supported",
            "type": "semantic_error.invalid_input",
        }
    ]


# ---------------------------------------------
# Test post request with an unknown/invalid form.
# ---------------------------------------------
@pytest.mark.parametrize("api_return_value", ["", '{"name": "formulier a"}'])
def test_post_request_unknown_form_uuid_resource(
    client: TestClient, mocker: Mock, api_return_value: str
):
    response = Mock(spec=Response)
    response.json.return_value = api_return_value
    response.status_code = http_status.HTTP_200_OK

    mocker.patch(
        "app.services.validation.validation_service.get_resource",
        return_value=response,
    )

    response = client.post(  # type: ignore
        f"{BASE_PATH_V1}{REQUESTS}", json=form1.dict(by_alias=True), headers=HEADERS
    )

    error_msg = vsc.ERROR_MESSAGE_FORM_NOT_FOUND.format(
        f"{FORMULIERENBEHEER_ACTIVE_FORMS_API}/{form1.form_uuid}"
    )
    assert_invalid_input(response=response, error_msg=error_msg)


# ---------------------------------------------
# Test post request with optional fields containing empty string
# ---------------------------------------------
def test_post_request_with_optional_fields_with_empty_strings(client: TestClient):
    body = form1.dict(by_alias=True)
    body["formInput"]["lastName"] = ""
    body["formInput"]["watIsUwVraag"] = ""
    body["formInput"]["email"] = ""
    body["formInput"]["datetime"] = ""

    response = client.post(f"{BASE_PATH_V1}{REQUESTS}", json=body, headers=HEADERS)
    assert_form_created(response=response, form=form1, check_form_input_value=False)


# ---------------------------------------------
# Test post request with additional fields
# ---------------------------------------------
def test_post_request_with_additional_fields(client: TestClient):
    body = form3.dict(by_alias=True)
    body["formInput"]["unknownField"] = "Field shoud not be in response"

    response = client.post(f"{BASE_PATH_V1}{REQUESTS}", json=body, headers=HEADERS)
    error_msg = "Onbekende velden in verzonden formulier"
    assert_invalid_input(response=response, error_msg=error_msg)


# ---------------------------------------------
# Testing an API with POST requests
# ---------------------------------------------
def test_post_method_requests(client: TestClient, test_db: Session):
    mock = add_request_mock_data_to_db(test_db)

    # formUuid should contain an uuid instead of form data.
    body = default_body(mock)
    response = client.post(f"{BASE_PATH_V1}{REQUESTS}", json=body, headers=HEADERS)
    error_msg = vsc.ERROR_MESSAGE_FORM_URL_NOT_VALID.format(
        f'{FORMULIERENBEHEER_ACTIVE_FORMS_API}/{body["formUuid"]}'
    )
    assert_invalid_input(response=response, error_msg=error_msg)
