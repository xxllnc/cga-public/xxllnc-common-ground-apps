import pytest
from app.api.api_v1.routers.tests import (
    HEADERS,
    HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER,
    HTTP_METHOD_OVERRIDE_URI_PARAMETER,
    REQUESTS,
)
from app.api.api_v1.routers.tests.asserts import assert_form_created, assert_method_not_allowed
from app.api.api_v1.routers.tests.helper import empty_body, form1
from app.api.api_v1.routers.tests.parameters import http_method_override
from app.core.config import BASE_PATH_V1
from fastapi.testclient import TestClient
from typing import Final

SUPPORTED_METHODS: Final = ["post"]
UNSUPPORTED_METHODS: Final = [ele for ele in http_method_override if ele not in SUPPORTED_METHODS]


# ---------------------------------------------
# Testing an API with HTTP methods that are not supported.
# Reject request with HTTP response code 405 (Method not allowed).
# Supported HTTP methods: POST
# ---------------------------------------------
@pytest.mark.parametrize("http_method", UNSUPPORTED_METHODS)
@pytest.mark.parametrize("request_body", [None, empty_body])
def test_http_methods_not_supported(client: TestClient, http_method: str, request_body: str):
    response = client.request(
        http_method, f"{BASE_PATH_V1}{REQUESTS}", json=request_body, headers=HEADERS
    )
    assert_method_not_allowed(response, http_method)


# ---------------------------------------------
# Test HTTP Method Overriding for the http method POST
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("header", HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER)
def test_post_method_http_override(client: TestClient, method: str, header: str):
    body = form1.dict(by_alias=True)
    headers = {header: method}
    headers.update(HEADERS)
    response = client.post(f"{BASE_PATH_V1}{REQUESTS}", json=body, headers=headers)
    assert_form_created(response, form1)


# ---------------------------------------------
# Test HTTP Method Overriding for the http method POST
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("parameter", HTTP_METHOD_OVERRIDE_URI_PARAMETER)
def test_post_method_http_override_uri_parameter(client: TestClient, method: str, parameter: str):
    body = form1.dict(by_alias=True)
    response = client.post(
        f"{BASE_PATH_V1}{REQUESTS}?{parameter}={method.upper()}", json=body, headers=HEADERS
    )
    assert_form_created(response, form1)


# ---------------------------------------------
# Test the unsupported http methods for HTTP Method Overriding.
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("unsupported_method", UNSUPPORTED_METHODS)
@pytest.mark.parametrize("header", HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER)
def test_http_method_overriding_for_unsupported_http_methods(
    client: TestClient, method: str, unsupported_method: str, header: str
):
    headers = {header: method}
    headers.update(HEADERS)
    response = client.request(unsupported_method, f"{BASE_PATH_V1}{REQUESTS}", headers=headers)
    assert_method_not_allowed(response, unsupported_method)


# ---------------------------------------------
# Test the unsupported http methods for HTTP Method Overriding.
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("unsupported_method", UNSUPPORTED_METHODS)
@pytest.mark.parametrize("parameter", HTTP_METHOD_OVERRIDE_URI_PARAMETER)
def test_http_method_overriding_for_unsupported_http_methods_uri_parameter(
    client: TestClient, method: str, unsupported_method: str, parameter: str
):
    response = client.request(
        unsupported_method,
        f"{BASE_PATH_V1}{REQUESTS}?{parameter}={method.upper()}",
        headers=HEADERS,
    )
    assert_method_not_allowed(response, unsupported_method)
