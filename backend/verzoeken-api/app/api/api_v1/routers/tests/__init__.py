from app.core import config
from typing import Final

REQUESTS: Final[str] = "/requests"
REQUESTS_ADMIN: Final[str] = "/requests/admin"
STATUS: Final[str] = "/status"


FORMULIERENBEHEER_ACTIVE_FORMS_API: Final[
    str
] = f"{config.get_active_forms_api_url('https://exxellence.vcap.me')}"

HEADERS: Final = {"organization-url": "https://exxellence.vcap.me"}

NOTIFICATION_URL: Final[str] = f"{config.NOTIFICATION_URL}"

HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER: Final[list[str]] = [
    "X-HTTP-Method",
    "X-Method-Override",
    "X-HTTP-Method-Override",
]

HTTP_METHOD_OVERRIDE_URI_PARAMETER: Final[list[str]] = [
    x.lower() for x in HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER
]

CONTENT_TYPE_VALUE_APPLICATION_JSON: Final[str] = "application/json"
