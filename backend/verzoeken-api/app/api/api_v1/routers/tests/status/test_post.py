from app.api.api_v1.routers.tests import STATUS
from app.api.api_v1.routers.tests.test_mock_data import (
    add_request_mock_data_to_db,
    add_status_mock_data_to_db,
)
from app.core.config import BASE_PATH_V1
from app.db import models
from datetime import date
from fastapi import status as http_status
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session


# ---------------------------------------------
# Test post request with an empty body.
# ---------------------------------------------
def test_post_status_with_empty_body(client: TestClient):
    response = client.post(f"{BASE_PATH_V1}{STATUS}", json={})

    res = response.json()
    assert response.status_code == http_status.HTTP_404_NOT_FOUND
    assert len(res) == 2
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]) == 3

    assert len(res["detail"][0]["loc"]) == 2
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["msg"] == "Record with that uuid does not exist"

    assert res["detail"][0]["type"] == "semantic_error.not_found"
    assert res["detail"][0]["loc"]["field"] == "id"


# ---------------------------------------------
# Test post status.
# ---------------------------------------------
def test_post_status(client: TestClient, test_db: Session):
    request = add_request_mock_data_to_db(test_db)
    status1 = {
        "requestId": str(request.request1.id),
        "paymentStatus": models.PaymentStatusEnum.TE_BETALEN,
        "paymentStatusDate": str(date(2021, 6, 19)),
        "requestStatus": "Niet verzonden",
        "requestStatusDate": str(date(2021, 6, 19)),
    }

    response = client.post(f"{BASE_PATH_V1}{STATUS}", json=status1)

    res = response.json()
    assert response.status_code == http_status.HTTP_201_CREATED
    assert len(res) == 6
    assert res["requestId"] == str(request.request1.id)
    assert res["paymentStatus"] == models.PaymentStatusEnum.TE_BETALEN
    assert res["paymentStatusDate"] == str(date(2021, 6, 19))
    assert res["requestStatus"] == "Niet verzonden"
    assert res["requestStatusDate"] == str(date(2021, 6, 19))


# ---------------------------------------------
# Test post status for already existing request.
# ---------------------------------------------
def test_post_status_for_existing_request(client: TestClient, test_db: Session):
    request = add_request_mock_data_to_db(test_db)
    add_status_mock_data_to_db(test_db, request.request1.sid, request.request2.sid)
    status1 = {
        "requestId": str(request.request1.id),
        "paymentStatus": models.PaymentStatusEnum.TE_BETALEN,
        "paymentStatusDate": str(date(2021, 6, 19)),
    }
    response = client.post(f"{BASE_PATH_V1}{STATUS}", json=status1)
    res = response.json()
    assert response.status_code == http_status.HTTP_409_CONFLICT
    assert len(res) == 2
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]) == 3
    assert len(res["detail"][0]["loc"]) == 2
    assert res["detail"][0]["loc"]["source"] == "body"
    assert (
        res["detail"][0]["msg"] == "Record with that request_id already exists in given time frame"
    )
    assert res["detail"][0]["type"] == "semantic_error.not_unique"
    assert res["detail"][0]["loc"]["field"] == "request_id"
