import pytest
from app.api.api_v1.routers.tests import CONTENT_TYPE_VALUE_APPLICATION_JSON, STATUS
from app.api.api_v1.routers.tests.test_mock_data import (
    add_request_mock_data_to_db,
    add_status_mock_data_to_db,
)
from app.core.config import BASE_PATH_V1
from app.resources import error_messages
from fastapi import status as http_status
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session


# ---------------------------------------------
# Delete "/status/{id}"
# ---------------------------------------------
def test_delete_status_without_content_type(client: TestClient, test_db: Session):
    request = add_request_mock_data_to_db(test_db)
    mock = add_status_mock_data_to_db(test_db, request.request1.sid, request.request2.sid)

    response = client.delete(f"{BASE_PATH_V1}{STATUS}/{mock.status1.uuid}")

    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 1
    assert res["message"] == error_messages.STATUS_IS_DELETED.format(mock.status1.uuid)


# ---------------------------------------------
# Delete "/status/{id}"
# ---------------------------------------------
def test_delete_requests_with_content_type_application_json(client: TestClient, test_db: Session):
    request = add_request_mock_data_to_db(test_db)
    mock = add_status_mock_data_to_db(test_db, request.request1.sid, request.request2.sid)

    headers = {"Content-Type": CONTENT_TYPE_VALUE_APPLICATION_JSON}
    response = client.delete(f"{BASE_PATH_V1}{STATUS}/{mock.status1.uuid}", headers=headers)
    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 1
    assert res["message"] == error_messages.STATUS_IS_DELETED.format(mock.status1.uuid)


# ---------------------------------------------
# Delete "/status/{id}"
# Test delete status twice.
# ---------------------------------------------
def test_delete_requests_twice(client: TestClient, test_db: Session):
    request = add_request_mock_data_to_db(test_db)
    mock = add_status_mock_data_to_db(test_db, request.request1.sid, request.request2.sid)

    response1 = client.delete(f"{BASE_PATH_V1}{STATUS}/{mock.status1.uuid}")
    response2 = client.delete(f"{BASE_PATH_V1}{STATUS}/{mock.status1.uuid}")

    assert response1.status_code == http_status.HTTP_200_OK
    res1 = response1.json()
    assert len(res1) == 1
    assert res1["message"] == error_messages.STATUS_IS_DELETED.format(mock.status1.uuid)

    assert response2.status_code == http_status.HTTP_404_NOT_FOUND
    res2 = response2.json()
    assert len(res2["detail"]) == 9
    assert res2["detail"] == error_messages.NOT_FOUND


# ---------------------------------------------
# Delete "/status/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_path_parameter_id_value",
    [
        "99",
        "-1",
        "99999999999999999999999999999999999999999999999999999999999999",
        "_",
        "aa",
        "unknown",
        "0",
        "__",
        "<>",
        "111111111111111111111111111111111111111111111111111111111",
        " AND 1::int=1",
        " UNION ALL SELECT NULL,version(),NULL LIMIT 1 OFFSET 1--",
        " or 1=1",
        " or = 1' or '1' = '1",
        "not valid",
    ],
)
def test_delete_requests_with_invalid_path_parameter_id_value(
    client: TestClient, invalid_path_parameter_id_value: str
):

    response = client.delete(f"{BASE_PATH_V1}{STATUS}/{invalid_path_parameter_id_value}")
    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res) == 1
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]) == 3
    assert len(res["detail"][0]["loc"]) == 2
    assert res["detail"][0]["loc"]["source"] == "path"
    assert res["detail"][0]["loc"]["field"] == "id"
    assert res["detail"][0]["msg"] == "value is not a valid uuid"
    assert res["detail"][0]["type"] == "type_error.uuid"


# ---------------------------------------------
# Delete "/status/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_path_parameter_id_value",
    [
        ">'\"()%26%25<acx><ScRiPt%20>jLeb(9968)</ScRiPt>",
        "text/plain",
        "application/*",
        "*/*",
    ],
)
def test_delete_requests_with_invalid_path_parameter_id_value_2(
    client: TestClient, invalid_path_parameter_id_value: str
):
    response = client.delete(f"{BASE_PATH_V1}{STATUS}/{invalid_path_parameter_id_value}")
    assert response.status_code == http_status.HTTP_404_NOT_FOUND
    res = response.json()
    assert len(res) == 1
    assert res["detail"] == error_messages.NOT_FOUND


# ---------------------------------------------
# Delete "/status/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_path_parameter_id_value",
    [
        "..;/",
        "",
        "?id=1 AND 1::int=1",
        "?id=1 UNION ALL SELECT NULL,version(),NULL LIMIT 1 OFFSET 1--",
    ],
)
def test_delete_requests_with_invalid_path_parameter_id_value_3(
    client: TestClient, invalid_path_parameter_id_value: str
):

    response = client.delete(f"{BASE_PATH_V1}{STATUS}/{invalid_path_parameter_id_value}")
    assert response.status_code == http_status.HTTP_307_TEMPORARY_REDIRECT
    headers = response.headers
    assert len(headers) == 2
    assert headers["content-length"] == str(0)
    assert headers["Location"] is not None
