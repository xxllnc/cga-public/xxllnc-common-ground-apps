from app import schemas
from app.db.models import StatusEnum  # pragma: no cover
from typing import Any, Final

empty_body: dict = {}

form_uuid: Final = "3d23f094-efbf-413b-bdf3-fa69b3f9f433"

form1: Final = schemas.RequestBase(
    form_uuid=form_uuid,
    status=StatusEnum.ACTIVE,
    form_input={
        "tEst": "",
        "name": "bestaat",
        "lastName": "",
        "watIsUwVraag": "",
    },
)

form2: Final = schemas.RequestBase(
    form_uuid=form_uuid,
    status=StatusEnum.ACTIVE,
    form_input={
        "textField": "re",
        "name": "Test",
        "file": [
            {
                "storage": "base64",
                "name": "test-a4e6be3f-bfc3-435b-b7e3-5be891504a2a.txt",
                "url": "data:text/plain;base64,dGVzdA==",
                "size": 4,
                "type": "blaat",
                "originalName": "test.txt",
            }
        ],
        "confirmationCheckBox": "true",
    },
)

form3: Final = schemas.RequestBase(
    form_uuid=form_uuid,
    status=StatusEnum.ACTIVE,
    form_input={
        "lastName": "re",
        "name": "Test",
        "file": [
            {
                "storage": "base64",
                "name": "test-a4e6be3f-bfc3-435b-b7e3-5be891504a2a.txt",
                "url": "data:text/plain;base64,dGVzdA==",
                "size": 4,
                "type": "text/plain",
                "originalName": "test.txt",
            }
        ],
        "confirmationCheckBox": "true",
    },
)


form4: Final = schemas.RequestBase(
    form_uuid=form_uuid,
    status=StatusEnum.ACTIVE,
    form_input={"textField": "test"},
)


form4_request_data: dict[str, Any] = {
    "id": form_uuid,
    "name": "test_1",
    "slug": "test_1",
    "status": "ACTIVE",
    "form": {
        "components": [
            {
                "type": "panel",
                "key": "page1",
                "components": [
                    {
                        "validate": {"required": False},
                        "key": "textField",
                        "type": "textfield",
                    }
                ],
            }
        ]
    },
    "config": [],
    "organizationUuid": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
    "organizationName": "Exxellence",
    "publishDate": "2022-01-01",
    "private": True,
    "rights": "OWNER",
}
