import datetime
import json
import pytest
from app.api.api_v1.routers.tests.test_mock_data import RequestMockData
from pathlib import Path
from requests.models import Response
from typing import Any, Final, Optional, Text
from unittest.mock import Mock

with open(Path(__file__).absolute().parent / "formio_form_data.json") as file:
    form = json.load(file)
    file.close()

# This is the api return value.
# It contains an valid (formio) form.
API_RETURN_VALUE: Final = {
    "name": "formulier a",
    "slug": "formulier-a",
    "status": "ACTIVE",
    "form": form,
    "config": "",
    "publishDate": "2021-09-06",
    "id": "918deb7d-364c-4124-a67a-d23590d34bc3",
}

# This is the file api return value.
# It contains dummy file data.
FILE_API_RETURN_VALUE: Final = {
    "url": "https://testurl/file_uuid/file_filename",
    "name": "filename",
    "size": 1000,
}


def default_body(mock: RequestMockData) -> dict:
    return {
        "registrationDate": str(datetime.date.today()),
        "formUuid": '{"name": "formulier a","components":""}',
        "status": mock.STATUS_ACTIVE,
        "formInput": {
            "tEst": "",
            "name": "bestaat niet",
            "lastName": "",
            "watIsUwVraag": "",
        },
    }


@pytest.fixture(scope="module", name="get_form_request_function")
def fixture_mock_get_form_request_function():
    def _mock_func(param1, param2: bool, param3: dict):
        response = Mock(spec=Response)
        response.json.return_value = API_RETURN_VALUE
        response.status_code = 200
        return response

    return _mock_func


@pytest.fixture(autouse=True)
def fixture_mock_commonground_service_post(monkeypatch):
    """Mock app.services.commonground_service.apiRequests.post
    for all tests."""

    # always return True
    def fake_request(
        url: Text,
        verify: bool,
        json: Optional[dict] = None,
        headers: Optional[Any] = None,
        data: Optional[Any] = None,
        files: Optional[Any] = None,
    ):

        if files:
            # Process file upload api call
            response = Mock(spec=Response)
            response.json.return_value = FILE_API_RETURN_VALUE
            response.status_code = 200
            return response

        if json:
            # Process file upload api call
            response = Mock(spec=Response)
            response.status_code = 204
            return response

        return True

    # Application of the monkeypatch to replace
    # app.services.commonground_service.apiRequests.post
    # with the behavior of fake_request defined above.
    monkeypatch.setattr(
        "app.services.commonground_service.apiRequests.post",
        fake_request,
    )


@pytest.fixture(autouse=True)
def fixture_mock_commonground_service_request_get(monkeypatch):
    """Mock app.services.commonground_service.apiRequests.get
    for all tests."""

    def fake_request(
        url: Text,
        headers: Any,
        verify: Any,
        data: Optional[Any] = None,
    ):

        response = Mock(spec=Response)
        response.json.return_value = API_RETURN_VALUE
        response.status_code = 200
        return response

    # Application of the monkeypatch to replace
    # app.services.commonground_service.apiRequests.get
    # with the behavior of fake_request defined above.
    monkeypatch.setattr(
        "app.services.commonground_service.apiRequests.get",
        fake_request,
    )
