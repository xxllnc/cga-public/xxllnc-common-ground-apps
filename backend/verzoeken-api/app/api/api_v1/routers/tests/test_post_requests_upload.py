from app import schemas
from base64 import b64decode
from fastapi import status as http_status
from fastapi.testclient import TestClient
from io import BytesIO
from typing import Final
from unittest.mock import Mock
from urllib.parse import ParseResult, urlparse
from uuid import uuid4

VERZOEKEN_REQUESTS_UPLOAD_API: Final = (
    "https://exxellence.vcap.me/verzoeken/api/v1/requests/upload"
)


# ---------------------------------------------
# Test post requests /requests/upload
# ---------------------------------------------
def test_post_requests_upload(client: TestClient, mocker: Mock):

    data_url = "data:text/plain;base64,ZGZkZmRmZGZk"
    data_url_parts: list[str] = data_url.split(",")
    data_base64_decoded: bytes = b64decode(data_url_parts[1])

    parsed_url: ParseResult = urlparse(VERZOEKEN_REQUESTS_UPLOAD_API)
    request_header: Final = {
        "origin": f"{parsed_url.scheme}://{parsed_url.netloc}",
    }

    files_data = {"file": ("filename", BytesIO(data_base64_decoded), "text/plain")}
    form_data = {"name": f"filename{uuid4()}.txt"}

    mocker.patch(
        "app.services.s3._upload_stream_to_s3",
        return_value=schemas.FileUploadResponse(
            url=f"https://test.nl/app_instance_id/{form_data['name']}", size=1234
        ),
    )

    response = client.post(
        f"{VERZOEKEN_REQUESTS_UPLOAD_API}",
        files=files_data,
        data=form_data,
        headers=request_header,
        verify=True,
    )

    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 2
    assert "size" in res
    assert "url" in res
