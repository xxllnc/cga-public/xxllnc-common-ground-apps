import datetime
from app import schemas
from app.api.api_v1.routers.tests import CONTENT_TYPE_VALUE_APPLICATION_JSON
from app.resources import error_messages
from fastapi import status as http_status
from requests.models import Response
from typing import Union


def assert_form_created(
    response: Response, form: schemas.RequestBase, check_form_input_value: bool = True
):
    registration_date = str(datetime.date.today())

    assert response.status_code == http_status.HTTP_201_CREATED
    res = response.json()
    assert len(res) == 5
    assert res["id"] is not None
    assert res["registrationDate"] == registration_date
    assert res["formUuid"] == form.form_uuid
    assert res["status"] == form.status
    assert res["formInput"] is not None
    if check_form_input_value:
        assert res["formInput"] == form.form_input

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert response_headers["content-length"] is not None

    assert len(res) == 5
    assert res["id"] is not None
    assert res["registrationDate"] == registration_date
    assert res["formUuid"] == form.form_uuid
    assert res["status"] == form.status


def get_msg_from_detail(res: dict) -> str:
    return res["detail"][0]["loc"]["detail"][0]["msg"]


def assert_invalid_input(response: Response, error_msg: Union[str, None] = None):
    assert response.status_code == http_status.HTTP_400_BAD_REQUEST

    res = response.json()
    assert len(res) == 2
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]) == 3
    assert res["detail"][0]["type"] == "semantic_error.invalid_input"

    if error_msg:
        assert get_msg_from_detail(res) == error_msg


def assert_method_not_allowed(response: Response, http_method: str):
    assert response.status_code == http_status.HTTP_405_METHOD_NOT_ALLOWED
    if http_method != "head":
        res = response.json()
        assert len(res) == 1
        assert res["detail"] == error_messages.METHOD_NOT_ALLOWED


def assert_415_unsupported_media_type(response: Response):
    assert response.status_code == http_status.HTTP_415_UNSUPPORTED_MEDIA_TYPE

    expected_body = {
        "detail": [
            {
                "loc": {"source": "header", "field": "Content-Type"},
                "msg": "Content-Type header must be application/json",
                "type": "value_error.missing",
            }
        ]
    }
    assert response.json() == expected_body

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert response_headers["content-length"] is not None
