#!/usr/bin/env python3

import datetime
from app.db import models
from app.db.models import StatusEnum
from collections import namedtuple
from datetime import date
from sqlalchemy.orm import Session
from typing import Final

SUBMITTER: Final = "test@exxellence.nl"

# Declaring namedtuple() as return value
RequestMockData = namedtuple(
    "RequestMockData",
    [
        "request1",
        "request2",
        "STATUS_INACTIVE",
        "STATUS_ACTIVE",
        "SUBMITTER",
    ],
)

StatusMockData = namedtuple(
    "StatusMockData",
    [
        "status1",
        "status2",
    ],
)


def add_request_mock_data_to_db(db: Session) -> RequestMockData:
    """Add mock data"""

    request1 = models.Request(
        id=None,
        registration_date=str(datetime.date.today()),
        form_uuid="11111111-3333-4444-a555-666666666666",
        status=StatusEnum.ACTIVE,
        form_input={
            "tEst": "",
            "name": "bestaat niet",
            "lastName": "",
            "watIsUwVraag": "",
        },
    )
    request2 = models.Request(
        id=None,
        registration_date=date(2011, 10, 4),
        form_uuid="11111111-3333-4444-a555-666666666666",
        status=StatusEnum.ACTIVE,
        form_input={
            "tEst": "",
            "name": "bestaat niet",
            "lastName": "",
            "watIsUwVraag": "",
        },
    )

    db.add(request1)
    db.add(request2)
    db.flush()

    return RequestMockData(
        request1,
        request2,
        StatusEnum.INACTIVE,
        StatusEnum.ACTIVE,
        SUBMITTER,
    )


def add_status_mock_data_to_db(db: Session, request_sid, request_sid2) -> StatusMockData:

    status1 = models.RequestStatus(
        uuid=None,
        request_id=request_sid,
        payment_status=models.PaymentStatusEnum.BETALING_AFGEROND,
        payment_status_date=date(2021, 6, 19),
    )
    status2 = models.RequestStatus(
        uuid=None,
        request_id=request_sid2,
        payment_status=models.PaymentStatusEnum.TE_BETALEN,
        payment_status_date=date(2021, 6, 1),
    )
    db.add(status1)
    db.add(status2)
    db.flush()

    return StatusMockData(
        status1,
        status2,
    )
