import pytest
from app.core.config import BASE_PATH_V1
from app.resources import error_messages
from fastapi import status as http_status
from fastapi.testclient import TestClient


# ---------------------------------------------
# Test get request with an unknown endpoint.
# ---------------------------------------------
@pytest.mark.parametrize(
    "get_unknown_endpoint",
    [
        "blablabla",
        "not valid",
        "*/*",
        "",
        None,
        "/unknown",
        "/..;/",
        "/forms%20%20dfdfdfdfererrdfdf",
        "/requestsAdmin%20%20dfdfdfdfererrdfdf",
    ],
)
def test_get_method_for_unknown_path(client: TestClient, get_unknown_endpoint: str):
    response = client.get(f"{BASE_PATH_V1}{get_unknown_endpoint}")
    assert response.status_code == http_status.HTTP_404_NOT_FOUND
    res = response.json()
    assert len(res) == 1
    assert res["detail"] == error_messages.NOT_FOUND
