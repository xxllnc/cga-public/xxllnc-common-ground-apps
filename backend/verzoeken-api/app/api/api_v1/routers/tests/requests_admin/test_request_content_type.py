import pytest
import requests
from app.api.api_v1.routers.tests import (
    CONTENT_TYPE_VALUE_APPLICATION_JSON,
    HEADERS,
    REQUESTS_ADMIN,
)
from app.api.api_v1.routers.tests.asserts import assert_415_unsupported_media_type
from app.api.api_v1.routers.tests.test_mock_data import add_request_mock_data_to_db
from app.core.config import BASE_PATH_V1
from fastapi import status as http_status
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session


# ---------------------------------------------
# Test get request without a request content type.
# A sender that generates a message containing a payload body SHOULD generate
# a Content-Type header field in that message unless the intended media type
# of the enclosed representation is unknown to the sender.
# ---------------------------------------------
def test_get_requests_list_request_without_request_header_content_type(client: TestClient):
    response = client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}")

    assert response.status_code == http_status.HTTP_200_OK

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 3
    assert response_headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert response_headers["content-length"] is not None
    assert response_headers["content-range"] is not None


# ---------------------------------------------
# Test get request with a request content type application/json.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_accept",
    [
        "application/javascript",
        CONTENT_TYPE_VALUE_APPLICATION_JSON,  # allowed
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "not valid",
        "application/*",
        "*/*",
        "",
        None,
    ],
)
def test_get_requests_list_request_with_request_header_content_type_application_json(
    client: TestClient, http_request_header_accept: str
):

    headers = {
        "Accept": f"{http_request_header_accept}",
        "Content-Type": CONTENT_TYPE_VALUE_APPLICATION_JSON,
    }

    response = client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}", headers=headers)

    assert response.status_code == http_status.HTTP_200_OK

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 3
    assert response_headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert response_headers["content-length"] is not None
    assert response_headers["content-range"] is not None


# ---------------------------------------------
# Test get request with an invalid request content type.
# The Content-Type header attribute specifies the format of the data in the
# request body so that receiver can parse it into appropriate format.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_content_type",
    [
        "application/javascript",
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "",
        "not valid",
        None,
    ],
)
def test_get_requests_list_request_with_request_header_content_type_with_invalid_value(
    client: TestClient, http_request_header_content_type: str
):
    headers = {"Content-Type": f"{http_request_header_content_type}"}

    response = client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}", headers=headers)

    assert response.status_code == http_status.HTTP_200_OK

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 3
    assert response_headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert response_headers["content-length"] is not None
    assert response_headers["content-range"] is not None


# ---------------------------------------------
# Test get request without a request content type.
# A sender that generates a message containing a payload body SHOULD generate
# a Content-Type header field in that message unless the intended media type
# of the enclosed representation is unknown to the sender.
# ---------------------------------------------
def test_get_one_requests_request_without_request_header_content_type(
    client: TestClient, test_db: Session
):
    mock = add_request_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}/{mock.request1.id}")

    assert response.status_code == http_status.HTTP_200_OK

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test get request with a request content type application/json.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_accept",
    [
        "application/javascript",
        CONTENT_TYPE_VALUE_APPLICATION_JSON,  # allowed
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "not valid",
        "application/*",
        "*/*",
        "",
        None,
    ],
)
def test_get_one_requests_request_with_request_header_content_type_application_json(
    client: TestClient, http_request_header_accept: str, test_db: Session
):
    mock = add_request_mock_data_to_db(test_db)

    headers = {
        "Accept": f"{http_request_header_accept}",
        "Content-Type": CONTENT_TYPE_VALUE_APPLICATION_JSON,
    }

    response = client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}/{mock.request1.id}", headers=headers)
    assert response.status_code == http_status.HTTP_200_OK

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test get request with an invalid request content type.
# The Content-Type header attribute specifies the format of the data in the
# request body so that receiver can parse it into appropriate format.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_content_type",
    [
        "application/javascript",
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "",
        "not valid",
        None,
    ],
)
def test_get_one_requests_request_with_request_header_content_type_with_invalid_value(
    client: TestClient, http_request_header_content_type: str, test_db: Session
):
    mock = add_request_mock_data_to_db(test_db)

    headers = {"Content-Type": f"{http_request_header_content_type}"}

    response = client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}/{mock.request1.id}", headers=headers)

    assert response.status_code == http_status.HTTP_200_OK

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test get request with an invalid request content type.
# The Content-Type header attribute specifies the format of the data in the
# request body so that receiver can parse it into appropriate format.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_content_type",
    [
        " application/javascript",
        " application/octet-stream",
        " multipart/form-data",
        " text/html",
        " text/plain",
        " ",
        " not valid",
    ],
)
def test_get_one_request_request_with_request_header_content_type_with_invalid_value_2(
    client: TestClient, http_request_header_content_type: str, test_db: Session
):
    mock = add_request_mock_data_to_db(test_db)

    headers = {"Content-Type": f"{http_request_header_content_type}"}

    # requests.exceptions.InvalidHeader: Invalid return character or
    # leading space in header: Content-Type
    with pytest.raises(requests.exceptions.InvalidHeader):
        client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}/{mock.request1.id}", headers=headers)


# ---------------------------------------------
# Test delete request without a request content type.
# The missing content type is ignored.The response Content-Type is always 'application/json'.
# ---------------------------------------------
def test_delete_request_without_request_header_content_type(client: TestClient, test_db: Session):
    mock = add_request_mock_data_to_db(test_db)

    response = client.delete(f"{BASE_PATH_V1}{REQUESTS_ADMIN}/{mock.request1.id}", headers=HEADERS)

    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 1

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test delete request with a request content type application/json.
# The Accept header is ignored. the response Content-Type is always 'application/json'.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_accept",
    [
        "application/javascript",
        CONTENT_TYPE_VALUE_APPLICATION_JSON,  # allowed
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "not valid",
        "application/*",
        "*/*",
        "",
        None,
    ],
)
def test_delete_request_with_request_header_content_type_application_json(
    client: TestClient, http_request_header_accept: str, test_db: Session
):
    mock = add_request_mock_data_to_db(test_db)

    headers = {
        **HEADERS,
        "Accept": f"{http_request_header_accept}",
        "Content-Type": CONTENT_TYPE_VALUE_APPLICATION_JSON,
    }

    response = client.delete(f"{BASE_PATH_V1}{REQUESTS_ADMIN}/{mock.request1.id}", headers=headers)

    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 1

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test delete request with an invalid request content type.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_content_type",
    [
        "application/javascript",
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "",
        "not valid",
        None,
    ],
)
def test_delete_request_with_request_header_content_type_with_invalid_value(
    client: TestClient, http_request_header_content_type: str, test_db: Session
):
    mock = add_request_mock_data_to_db(test_db)

    headers = {"Content-Type": f"{http_request_header_content_type}"}

    response = client.delete(f"{BASE_PATH_V1}{REQUESTS_ADMIN}/{mock.request1.id}", headers=headers)

    assert_415_unsupported_media_type(response)
