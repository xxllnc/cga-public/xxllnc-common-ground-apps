import pytest
from app.api.api_v1.routers.tests import CONTENT_TYPE_VALUE_APPLICATION_JSON, REQUESTS_ADMIN
from app.api.api_v1.routers.tests.test_mock_data import add_request_mock_data_to_db
from app.core.config import BASE_PATH_V1
from app.resources import error_messages
from fastapi import status as http_status
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session


# ---------------------------------------------
# Get "/requests/{id}"
# ---------------------------------------------
def test_get_requests_by_id_without_content_type(client: TestClient, test_db: Session):
    mock = add_request_mock_data_to_db(test_db)

    for id in [mock.request1.id, mock.request2.id]:
        response = client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}/{id}")

        assert response.status_code == http_status.HTTP_200_OK
        res = response.json()
        assert len(res) == 5
        assert res["id"] == str(id)


# ---------------------------------------------
# Get "/requests/{id}"
# ---------------------------------------------
def test_get_requests_by_id_with_content_type_application_json(
    client: TestClient, test_db: Session
):
    mock = add_request_mock_data_to_db(test_db)

    headers = {"Content-Type": CONTENT_TYPE_VALUE_APPLICATION_JSON}

    for id in [mock.request1.id, mock.request2.id]:
        response = client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}/{id}", headers=headers)

        assert response.status_code == http_status.HTTP_200_OK
        res = response.json()
        assert len(res) == 5
        assert res["id"] == str(id)


# ---------------------------------------------
# Get "/requests/{id}"
# ---------------------------------------------
def test_get_requests_by_id_with_unknown_id(client: TestClient, test_db: Session):
    add_request_mock_data_to_db(test_db)

    headers = {"Content-Type": CONTENT_TYPE_VALUE_APPLICATION_JSON}

    find_id = "c16ae581-8d6f-424e-bff3-3a70a1374f01"
    response = client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}/{find_id}", headers=headers)

    assert response.status_code == http_status.HTTP_404_NOT_FOUND
    res = response.json()
    assert len(res["detail"]) == 9
    assert res["detail"] == error_messages.NOT_FOUND


# ---------------------------------------------
# Get "/requests/{id}"
# Test ignore the query parameters.
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter",
    [
        "",
        "unknown",
        "0",
        "__",
        "<>",
        "id=1 AND 1::int=1",
        "?id=1 AND 1::int=1",
        "&id=1 AND 1::int=1",
        "id=1 UNION ALL SELECT NULL,version(),NULL LIMIT 1 OFFSET 1--",
        None,
    ],
)
def test_get_requests_with_addition_query_params(
    client: TestClient, test_db: Session, query_parameter: str
):
    mock = add_request_mock_data_to_db(test_db)

    for id in [mock.request1.id]:
        response = client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}/{id}?{query_parameter}")
        assert response.status_code == http_status.HTTP_200_OK
        res = response.json()
        assert len(res) == 5
        assert res["id"] == str(mock.request1.id)
        assert res["registrationDate"] == mock.request1.registration_date
        assert res["formUuid"] == mock.request1.form_uuid
        assert res["status"] == mock.request1.status
        assert res["formInput"] == mock.request1.form_input


# ---------------------------------------------
# Get "/requests/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_id_value",
    [
        "unknown",
        "0",
        "__",
        "<>",
        "111111111111111111111111111111111111111111111111111111111",
    ],
)
def test_get_requests_with_invalid_id_value(
    client: TestClient, test_db: Session, invalid_id_value: str
):
    add_request_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}/{invalid_id_value}")

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res) == 1
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]) == 3
    assert len(res["detail"][0]["loc"]) == 2
    assert res["detail"][0]["loc"]["source"] == "path"
    assert res["detail"][0]["loc"]["field"] == "id"
    assert res["detail"][0]["msg"] == "value is not a valid uuid"
    assert res["detail"][0]["type"] == "type_error.uuid"


# ---------------------------------------------
# Get "/requests/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_id_value",
    [
        " AND 1::int=1",
        " UNION ALL SELECT NULL,version(),NULL LIMIT 1 OFFSET 1--",
        " or 1=1",
        " or = 1' or '1' = '1",
        "not valid",
    ],
)
def test_get_requests_with_invalid_id_value_2(
    client: TestClient, test_db: Session, invalid_id_value: str
):
    mock = add_request_mock_data_to_db(test_db)

    id_value: str = f"{mock.request1.id}{invalid_id_value}"

    response = client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}/{id_value}")

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res) == 1
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]) == 3
    assert len(res["detail"][0]["loc"]) == 2
    assert res["detail"][0]["loc"]["source"] == "path"
    assert res["detail"][0]["loc"]["field"] == "id"
    assert res["detail"][0]["msg"] == "value is not a valid uuid"
    assert res["detail"][0]["type"] == "type_error.uuid"


# ---------------------------------------------
# Get "/requests/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_id_value",
    [
        ">'\"()%26%25<acx><ScRiPt%20>jLeb(9968)</ScRiPt>",
        "text/plain",
        "application/*",
        "*/*",
    ],
)
def test_get_requests_with_invalid_id_value_3(
    client: TestClient, test_db: Session, invalid_id_value: str
):
    add_request_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}/{invalid_id_value}")

    assert response.status_code == http_status.HTTP_404_NOT_FOUND
    res = response.json()
    assert len(res["detail"]) == 9
    assert res["detail"] == error_messages.NOT_FOUND


# ---------------------------------------------
# Get "/requests/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
def test_get_requests_with_invalid_id_value_4(client: TestClient, test_db: Session):
    mock = add_request_mock_data_to_db(test_db)

    invalid_id = "..;/"

    response = client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}/{mock.request1.id}{invalid_id}")

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res) == 1
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]) == 3
    assert len(res["detail"][0]["loc"]) == 2
    assert res["detail"][0]["loc"]["source"] == "path"
    assert res["detail"][0]["loc"]["field"] == "id"
    assert res["detail"][0]["msg"] == "value is not a valid uuid"
    assert res["detail"][0]["type"] == "type_error.uuid"
