import datetime
import pytest
from app.api.api_v1.routers.tests import CONTENT_TYPE_VALUE_APPLICATION_JSON, REQUESTS_ADMIN
from app.api.api_v1.routers.tests.test_mock_data import add_request_mock_data_to_db
from app.core.config import BASE_PATH_V1
from app.resources import error_messages
from fastapi import status as http_status
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session


# ---------------------------------------------
# Delete "/requests/{id}"
# ---------------------------------------------
def test_delete_requests_without_content_type(client: TestClient, test_db: Session):
    mock = add_request_mock_data_to_db(test_db)

    for id in [mock.request1.id, mock.request2.id]:
        response = client.delete(f"{BASE_PATH_V1}{REQUESTS_ADMIN}/{id}")

        assert response.status_code == http_status.HTTP_200_OK
        res = response.json()
        assert len(res) == 1
        assert res["message"] == error_messages.REQUEST_IS_DELETED.format(id)


# ---------------------------------------------
# Delete "/requests/{id}"
# ---------------------------------------------
def test_delete_requests_with_content_type_application_json(client: TestClient, test_db: Session):
    mock = add_request_mock_data_to_db(test_db)

    headers = {"Content-Type": CONTENT_TYPE_VALUE_APPLICATION_JSON}

    for id in [mock.request1.id, mock.request2.id]:
        response = client.delete(f"{BASE_PATH_V1}{REQUESTS_ADMIN}/{id}", headers=headers)

        assert response.status_code == http_status.HTTP_200_OK
        res = response.json()
        assert len(res) == 1
        assert res["message"] == error_messages.REQUEST_IS_DELETED.format(id)


# ---------------------------------------------
# Delete "/requests/{id}"
# Test delete request twice.
# ---------------------------------------------
def test_delete_requests_twice(client: TestClient, test_db: Session):
    mock = add_request_mock_data_to_db(test_db)

    for id in [mock.request1.id, mock.request2.id]:
        response1 = client.delete(f"{BASE_PATH_V1}{REQUESTS_ADMIN}/{id}")
        response2 = client.delete(f"{BASE_PATH_V1}{REQUESTS_ADMIN}/{id}")

        assert response1.status_code == http_status.HTTP_200_OK
        res1 = response1.json()
        assert len(res1) == 1
        assert res1["message"] == error_messages.REQUEST_IS_DELETED.format(id)

        assert response2.status_code == http_status.HTTP_404_NOT_FOUND
        res2 = response2.json()
        assert len(res2["detail"]) == 9
        assert res2["detail"] == error_messages.NOT_FOUND


# ---------------------------------------------
# Delete "/requests/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_path_parameter_id_value",
    [
        "99",
        "-1",
        "99999999999999999999999999999999999999999999999999999999999999",
        "_",
        "aa",
        "unknown",
        "0",
        "__",
        "<>",
        "111111111111111111111111111111111111111111111111111111111",
        " AND 1::int=1",
        " UNION ALL SELECT NULL,version(),NULL LIMIT 1 OFFSET 1--",
        " or 1=1",
        " or = 1' or '1' = '1",
        "not valid",
    ],
)
def test_delete_requests_with_invalid_path_parameter_id_value(
    client: TestClient, test_db: Session, invalid_path_parameter_id_value: str
):
    add_request_mock_data_to_db(test_db)

    response = client.delete(f"{BASE_PATH_V1}{REQUESTS_ADMIN}/{invalid_path_parameter_id_value}")
    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res) == 1
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]) == 3
    assert len(res["detail"][0]["loc"]) == 2
    assert res["detail"][0]["loc"]["source"] == "path"
    assert res["detail"][0]["loc"]["field"] == "id"
    assert res["detail"][0]["msg"] == "value is not a valid uuid"
    assert res["detail"][0]["type"] == "type_error.uuid"


# ---------------------------------------------
# Delete "/requests/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_path_parameter_id_value",
    [
        ">'\"()%26%25<acx><ScRiPt%20>jLeb(9968)</ScRiPt>",
        "text/plain",
        "application/*",
        "*/*",
    ],
)
def test_delete_requests_with_invalid_path_parameter_id_value_2(
    client: TestClient, test_db: Session, invalid_path_parameter_id_value: str
):
    add_request_mock_data_to_db(test_db)

    response = client.delete(f"{BASE_PATH_V1}{REQUESTS_ADMIN}/{invalid_path_parameter_id_value}")
    assert response.status_code == http_status.HTTP_404_NOT_FOUND
    res = response.json()
    assert len(res) == 1
    assert res["detail"] == error_messages.NOT_FOUND


# ---------------------------------------------
# Delete "/requests/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_path_parameter_id_value",
    [
        "..;/",
        "",
        "?id=1 AND 1::int=1",
        "?id=1 UNION ALL SELECT NULL,version(),NULL LIMIT 1 OFFSET 1--",
    ],
)
def test_delete_requests_with_invalid_path_parameter_id_value_3(
    client: TestClient, test_db: Session, invalid_path_parameter_id_value: str
):
    add_request_mock_data_to_db(test_db)

    response = client.delete(f"{BASE_PATH_V1}{REQUESTS_ADMIN}/{invalid_path_parameter_id_value}")

    assert response.status_code == http_status.HTTP_307_TEMPORARY_REDIRECT
    headers = response.headers
    assert len(headers) == 2
    assert headers["content-length"] == str(0)
    assert headers["Location"] is not None


# ---------------------------------------------
# Testing an API with DELETE /{id} requests
# ---------------------------------------------
def test_delete_method_requests_with_path_parameter(client: TestClient, test_db: Session):
    mock = add_request_mock_data_to_db(test_db)

    response = client.delete(f"{BASE_PATH_V1}{REQUESTS_ADMIN}/{mock.request1.id}")

    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 1
    assert res["message"] == error_messages.REQUEST_IS_DELETED.format(mock.request1.id)


# ---------------------------------------------
# Testing an API with a DELETE request
# without path parameter id.
# Reject request with HTTP response code 405 Method not allowed,
# because we can't delete the whole collection.
# ---------------------------------------------
def test_delete_method_requests_without_path_parameter(client: TestClient):
    body = {"registrationDate": str(datetime.date.today())}

    response = client.delete(f"{BASE_PATH_V1}{REQUESTS_ADMIN}", json=body)

    assert response.status_code == http_status.HTTP_405_METHOD_NOT_ALLOWED
    res = response.json()
    assert len(res) == 1
    assert res["detail"] == "Method Not Allowed"
