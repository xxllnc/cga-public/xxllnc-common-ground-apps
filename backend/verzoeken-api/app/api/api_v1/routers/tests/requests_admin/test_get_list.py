import pytest
from app.api.api_v1.routers.tests import CONTENT_TYPE_VALUE_APPLICATION_JSON, REQUESTS_ADMIN
from app.api.api_v1.routers.tests.test_mock_data import add_request_mock_data_to_db
from app.core.config import BASE_PATH_V1
from fastapi import status as http_status
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session


# ---------------------------------------------
# Testing an API with GET requests
# ---------------------------------------------
def test_get_requests_list(client: TestClient, test_db: Session):
    add_request_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}")

    assert response.status_code == http_status.HTTP_200_OK
    headers = response.headers
    assert len(headers) == 3

    assert headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert headers["content-range"] == "results 0-1/2"

    res = response.json()
    assert len(res) == 2

    assert len(res[0]) == 5
    assert res[0]["id"] is not None
    assert res[0]["registrationDate"] is not None
    assert res[0]["formUuid"] is not None
    assert res[0]["status"] is not None
    assert res[0]["formInput"] is not None

    assert len(res[1]) == 5
    assert res[1]["id"] is not None
    assert res[1]["registrationDate"] is not None
    assert res[1]["formUuid"] is not None
    assert res[1]["status"] is not None
    assert res[1]["formInput"] is not None


# ---------------------------------------------
# Testing an API with GET requests.
# Sort by valid fields. Sort order is ascending
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter_sort_value",
    ["id", "status", "registrationDate"],
)
def test_get_requests_list_sort_by_valid_field_asc(
    client: TestClient, test_db: Session, query_parameter_sort_value: str
):
    add_request_mock_data_to_db(test_db)

    response = client.get(
        f'{BASE_PATH_V1}{REQUESTS_ADMIN}?sort=["{query_parameter_sort_value}","ASC"]'
    )

    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 2


# ---------------------------------------------
# Testing an API with GET requests.
# Sort by valid fields. Sort order is descending
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter_sort_value",
    ["id", "status", "registrationDate"],
)
def test_get_requests_list_sort_by_valid_field_desc(
    client: TestClient, test_db: Session, query_parameter_sort_value: str
):
    add_request_mock_data_to_db(test_db)

    response = client.get(
        f'{BASE_PATH_V1}{REQUESTS_ADMIN}?sort=["{query_parameter_sort_value}","DESC"]'
    )

    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 2


# ---------------------------------------------
# Testing an API with GET requests.
# Sort by invalid field. Sort order is ascending
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter_sort_value",
    [
        "",
        "unkownfield",
        "_",
        "url",
        "form",
        "formUuid",
        "organization",
        "submitter",
        "formInput",
        None,
    ],
)
def test_get_requests_list_sort_by_invalid_field_asc(
    client: TestClient, query_parameter_sort_value: str
):
    response = client.get(
        f'{BASE_PATH_V1}{REQUESTS_ADMIN}?sort=["{query_parameter_sort_value}","ASC"]'
    )
    assert response.status_code == http_status.HTTP_400_BAD_REQUEST


# ---------------------------------------------
# Testing an API with GET requests.
# Sort by invalid field. Sort order is descending
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter_sort_value",
    [
        "unkownfield",
        "_",
        "url",
        "form",
        "formUuid",
        "organization",
        "submitter",
        "formInput",
        None,
    ],
)
def test_get_requests_list_sort_by_invalid_field_desc(
    client: TestClient, query_parameter_sort_value: str
):
    response = client.get(
        f'{BASE_PATH_V1}{REQUESTS_ADMIN}?sort=["{query_parameter_sort_value}","DESC"]'
    )
    assert response.status_code == http_status.HTTP_400_BAD_REQUEST


# ---------------------------------------------
# Testing an API with GET requests.
# Sort by invalid order.
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter_order_value",
    [
        "unkown",
        "order",
        "_",
        "LEFT",
        "asc",
        "AsC",
        "desc",
        "DeSc",
        "",
        " ",
        None,
    ],
)
def test_get_requests_list_sort_by_invalid_order(
    client: TestClient, query_parameter_order_value: str
):
    response = client.get(
        f'{BASE_PATH_V1}{REQUESTS_ADMIN}?sort=["name","{query_parameter_order_value}"]'
    )
    assert response.status_code == http_status.HTTP_400_BAD_REQUEST


# ---------------------------------------------
# Testing an API with GET requests.
# Sort by registrationDate. Sort order is ascending
# ---------------------------------------------
def test_get_requests_list_sort_by_registration_date_asc(client: TestClient, test_db: Session):
    mock = add_request_mock_data_to_db(test_db)

    response = client.get(f'{BASE_PATH_V1}{REQUESTS_ADMIN}?sort=["registrationDate","ASC"]')

    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 2
    assert res[0]["registrationDate"] == str(mock.request2.registration_date)
    assert res[1]["registrationDate"] == str(mock.request1.registration_date)


# ---------------------------------------------
# Testing an API with GET requests.
# Sort by registrationDate. Sort order is descending
# ---------------------------------------------
def test_get_requests_list_sort_by_registration_date_desc(client: TestClient, test_db: Session):
    mock = add_request_mock_data_to_db(test_db)

    response = client.get(f'{BASE_PATH_V1}{REQUESTS_ADMIN}?sort=["registrationDate","DESC"]')

    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 2
    assert res[0]["registrationDate"] == str(mock.request1.registration_date)
    assert res[1]["registrationDate"] == str(mock.request2.registration_date)


# ---------------------------------------------
# Testing an API with GET requests.
# Test invalid sort and order combinations.
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter_value",
    [
        "[]",
        '[""]',
        '["name"]',
        '["name",]',
        '["unknownfield"]',
        "[,]",
        '[,"ASC"]',
        '[,"DESC"]',
        '[,"TEST"]',
        "[,,,]",
    ],
)
def test_get_requests_list_invalid_sort_order_combinations(
    client: TestClient, test_db: Session, query_parameter_value: str
):
    add_request_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}?sort={query_parameter_value}")

    assert response.status_code == http_status.HTTP_400_BAD_REQUEST
    res = response.json()
    assert (
        res["detail"]
        == f"Invalid sort order: {query_parameter_value}. sort; needs to be a JSON list with two elements: The field to sort by, and the order (ASC or DESC). Example: `[field, sortOrder]` where sortOrder is ASC or DESC"  # noqa: E501
    )


# ---------------------------------------------
# Testing an API with GET requests.
# Test start range without end range.
# Ignore the start range because the end range is not set.
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter",
    [
        "range=[0]",
        "range=[1]",
        "range=[4]",
        "range=[100]",
        "range=[]",
        "range=[aa]",
        "range=[-1]",
    ],
)
def test_get_requests_list_range_with_start_without_end(
    client: TestClient, test_db: Session, query_parameter: str
):
    add_request_mock_data_to_db(test_db)

    with pytest.raises(ValueError):
        client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}?{query_parameter}")


# ---------------------------------------------
# Testing an API with GET requests.
# Test start range with an end range.
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter, response_length, header_content_range",
    [
        ("range=[0,3]", 2, "results 0-1/2"),
        ("range=[1,2]", 1, "results 1-1/2"),
        ("range=[3,3]", 0, "results 3-2/2"),
    ],
)
def test_get_requests_list_range_with_start_with_end(
    client: TestClient,
    test_db: Session,
    query_parameter: str,
    response_length: int,
    header_content_range: str,
):
    add_request_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}?{query_parameter}")

    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == int(f"{response_length}")

    headers = response.headers
    assert len(headers) == 3
    assert headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert headers["content-range"] == f"{header_content_range}"


# ---------------------------------------------
# Testing an API with GET requests.
# Test invalid start range an end range combinations.
# ---------------------------------------------
def test_get_requests_list_range_with_invalid_start_end_combination(
    client: TestClient, test_db: Session
):
    add_request_mock_data_to_db(test_db)

    # test start with an empty end range
    with pytest.raises(ValueError):
        client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}?range=[3,0]")

    # test empty start with an empty end range
    with pytest.raises(ValueError):
        client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}?range=[,]")

    # test range end cannot be before start
    with pytest.raises(ValueError):
        client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}?range=[3,2]")

    # test end value is greater than or equal to 0
    with pytest.raises(ValueError):
        client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}?range=[0,-1]")

    # test end value is not a valid integer
    with pytest.raises(ValueError):
        client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}?range=[0,aabbcc]")

    # test start and end value are not a valid integer
    with pytest.raises(ValueError):
        client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}?range=[aa,bb]")

    # test page size cannot exceed 100
    with pytest.raises(ValueError):
        client.get(
            f"{BASE_PATH_V1}{REQUESTS_ADMIN}?range=[1111111111111111111111111111111,1111111111111111111111111111111222222222222222222222222222222222222222222222]",  # noqa: E501
        )


# ---------------------------------------------
# Testing an API with GET requests.
# Test end range without start range.
# Throw ValueError because the start range is not set.
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter",
    [
        "range=[,0]",
        "range=[,1]",
        "range=[,4]",
        "range=[,100]",
        "range=[,]",
        "range=[,aa]",
        "range=[,-1]",
    ],
)
def test_get_requests_list_range_with_end_without_start(
    client: TestClient, test_db: Session, query_parameter: str
):
    add_request_mock_data_to_db(test_db)

    with pytest.raises(ValueError):
        client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}?{query_parameter}")


# ---------------------------------------------
# Testing an API with GET requests.
# Test fulltext search with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter",
    [
        'filter={""}',
        'filter={"aaaaaaaaaaaaa"}',
        'filter={"aaaaaaaaaaaaa":}',
        "filter={:}",
        'filter={:""}',
    ],
)
def test_get_requests_list_invalid_full_text_filter(
    client: TestClient, test_db: Session, query_parameter: str
):
    add_request_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}?{query_parameter}")

    assert response.status_code == http_status.HTTP_400_BAD_REQUEST


# ---------------------------------------------
# Testing an API with GET requests.
# Test fulltext search.
# Fulltext search is not implemented yet.
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter",
    [
        ('filter={"q":""}'),
        ('filter={"q":"aaaaaaaaaaaaa"}'),
        ('filter={"q":"verzoek "}'),
        ('filter={"q":"1 "}'),
        ('filter={"q":"0"}'),
        ('filter={"q":"None"}'),
        ('filter={"q":"t"}'),
        ('filter={"q":"T"}'),
    ],
)
def test_get_requests_list_full_text_filter(
    client: TestClient, test_db: Session, query_parameter: str
):
    add_request_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}?{query_parameter}")
    assert response.status_code == http_status.HTTP_400_BAD_REQUEST


# ---------------------------------------------
# Testing an API with GET requests.
# Test filter option for field Status.
# Filter option is not implemented yet.
# ---------------------------------------------
@pytest.mark.parametrize(
    "filter_query_parameter",
    [
        ('filter={"status":"INACTIVE"}'),
        ('filter={"status":"ACTIVE"}'),
        ('filter={"status":""}'),
        ('filter={"status":"None"}'),
        # ('filter={"registrationDate":"iNaCtIvE"}'), # TODO CGA-1385 fix DataError # noqa: E501
    ],
)
def test_get_requests_list_filter_options(
    client: TestClient, test_db: Session, filter_query_parameter: str
):
    add_request_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}?{filter_query_parameter}")
    assert response.status_code == http_status.HTTP_400_BAD_REQUEST


# ---------------------------------------------
# Testing an API with GET requests.
# Test fulltext search in combination with filter option for field Status.
# Fulltext search is not implemented yet.
# Filter option is not implemented yet.
# ---------------------------------------------
@pytest.mark.parametrize(
    "filter_query_parameter",
    [
        ('filter={"q":"","status":"INACTIVE"}'),
        ('filter={"q":"aaaaaaaaaaaaa","status":"INACTIVE"}'),
        ('filter={"q":"t","status":"INACTIVE"}'),
        ('filter={"q":"T","status":"INACTIVE"}'),
        ('filter={"q":"","status":"ACTIVE"}'),
        ('filter={"q":"t","status":"ACTIVE"}'),
        ('filter={"q":"T","status":"ACTIVE"}'),
    ],
)
def test_get_requests_list_full_text_filter_and_filter_option_status(
    client: TestClient, test_db: Session, filter_query_parameter: str
):
    add_request_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}?{filter_query_parameter}")
    assert response.status_code == http_status.HTTP_400_BAD_REQUEST


# ---------------------------------------------
# Testing the API with a GET requests
# to retreive all the requests.
# Empty list.
# ---------------------------------------------
def test_get_method_requests_list_empty(client: TestClient):
    response = client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}")
    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 0


# ---------------------------------------------
# Testing the API with a GET requests
# to retreive all the requests.
# List is not emtpy.
# ---------------------------------------------
def test_get_method_requests_list(client: TestClient, test_db: Session):
    add_request_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{REQUESTS_ADMIN}")
    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 2
