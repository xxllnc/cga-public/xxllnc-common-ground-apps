import uuid
from app import domain, schemas
from app.core.auth0 import get_admin_scopes
from app.db.session import get_db
from app.resources import error_messages, errors
from exxellence_shared_cga.core.types import GenericId, IdType, SemanticError
from fastapi import APIRouter, Depends
from fastapi import status as http_status
from sqlalchemy.orm import Session
from typing import List

router = r = APIRouter()


@r.get(
    "/documents/{request_id}",
    response_model=List[schemas.DocumentId],
    responses={http_status.HTTP_404_NOT_FOUND: {"description": "Document not found"}},
    dependencies=[Depends(get_admin_scopes)],
)
def get_documents_ids(request_id: uuid.UUID, database: Session = Depends(get_db)):
    """Returns all document ids, given a request id"""

    try:
        status = domain.documents.get_documents_to_delete(database=database, id=request_id)
    except SemanticError as semantic_error:
        errors.not_found_error(
            semantic_error,
            error_messages.DOCUMENT_NOT_FOUND_WITH.format(request_id),
        )

    return status


@r.delete(
    "/documents/{id}",
    responses={
        http_status.HTTP_404_NOT_FOUND: {"description": "Document not found"},
        http_status.HTTP_409_CONFLICT: {"description": "Conflict. Document not deleted."},
    },
    dependencies=[Depends(get_admin_scopes)],
)
def delete_document(id: uuid.UUID, database: Session = Depends(get_db)):

    try:
        id_type = IdType.uuid if isinstance(id, uuid.UUID) else IdType.sid
        domain.documents.delete(db=database, id=GenericId(id=id, type=id_type))
    except SemanticError as semantic_error:
        errors.not_found_error(
            semantic_error,
            error_messages.DOCUMENT_IS_DELETED.format(id),
        )

    return {"message": error_messages.DOCUMENT_IS_DELETED.format(id)}
