"""Module for all request admin api routes"""

import uuid
from app import domain, schemas
from app.core.auth0 import get_admin_scopes
from app.db.session import get_db
from app.resources import error_messages, errors
from app.services.validation.validation_service import ValidationService
from exxellence_shared_cga import GenericId, IdType
from exxellence_shared_cga.core.rest_helpers import SearchParameters
from exxellence_shared_cga.core.types import SemanticError
from fastapi import APIRouter, Depends, Response
from fastapi import status as http_status
from sqlalchemy.orm import Session
from typing import List

validation_service = ValidationService()

router = r = APIRouter()


# validate the filter and sort parameters
def validate_search_parameters():
    return SearchParameters(
        filter_options_schema=schemas.RequestFilterableFields,
        sort_options_schema=schemas.RequestSortableFields,
    )


@r.get(
    "/requests/admin/{id}",
    response_model=schemas.Request,
    responses={http_status.HTTP_404_NOT_FOUND: {"description": "Request not found"}},
    dependencies=[Depends(get_admin_scopes)],
)
def get_request_by_id(id: uuid.UUID, database: Session = Depends(get_db)):

    try:
        request = domain.request.get(db=database, id=GenericId(id=id, type=IdType.id))
    except SemanticError as semantic_error:
        errors.not_found_error(semantic_error, error_messages.REQUEST_NOT_FOUND_WITH.format(id))

    return request


@r.get(
    "/requests/admin",
    response_model=List[schemas.Request],
    dependencies=[Depends(get_admin_scopes)],
)
def get_requests(
    response: Response,
    database: Session = Depends(get_db),
    search_parameters: dict = Depends(validate_search_parameters()),
):

    search_result = domain.request.get_multi(
        db=database,
        query=None,
        **search_parameters,
    )

    response.headers["Content-Range"] = search_result.as_content_range("results")

    return search_result.result


@r.delete(
    "/requests/admin/{id}",
    responses={
        http_status.HTTP_404_NOT_FOUND: {"description": "Request not found"},
        http_status.HTTP_409_CONFLICT: {"description": "Conflict. Request not deleted."},
    },
    dependencies=[Depends(get_admin_scopes)],
)
def delete_request(id: uuid.UUID, database: Session = Depends(get_db)):

    domain.request.delete(db=database, id=GenericId(id=id, type=IdType.id))

    return {"message": error_messages.REQUEST_IS_DELETED.format(id)}
