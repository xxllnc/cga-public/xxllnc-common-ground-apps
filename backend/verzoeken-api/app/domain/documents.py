"""Module for css CRUD calls"""
import sqlalchemy as sa
import uuid
from app import schemas
from app.db import models
from exxellence_shared_cga.core.types import SemanticError, SemanticErrorType
from exxellence_shared_cga.domain.base import CRUDBase
from sqlalchemy.orm import Session
from typing import List


class CRUDStatus(
    CRUDBase[
        models.Documents,
        schemas.StatusBase,
        schemas.StatusBase,
    ]
):
    def get_documents_to_delete(
        self,
        database: Session,
        id: uuid.UUID,
    ) -> schemas.DocumentId:

        request_statement = sa.select(models.Request).where(models.Request.id == id)
        result = database.execute(request_statement).scalars().first()

        if not result:
            raise SemanticError(
                loc={"source": "path", "field": "id"},
                msg="Record not found",
                type=SemanticErrorType.not_found,
            )
        query = database.query(models.Documents).filter(models.Documents.request_id == result.sid)
        documents_result = query.all()
        if not documents_result:
            raise SemanticError(
                loc={"source": "body", "field": "id"},
                msg="Record with that uuid does not exist",
                type=SemanticErrorType.not_found,
            )

        return_val: List[schemas.DocumentId(id)] = []  # type: ignore # noqa: E501

        for result in documents_result:

            return_val.append(schemas.DocumentId(id=result.uuid))  # type: ignore # noqa: E501

        return return_val  # type: ignore


documents = CRUDStatus(models.Documents, models)
