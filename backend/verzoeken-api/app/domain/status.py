"""Module for css CRUD calls"""

import sqlalchemy as sa
import uuid
from app import schemas
from app.db import models
from exxellence_shared_cga.core.types import (
    FilterOptions,
    RangeOptions,
    SearchResult,
    SemanticError,
    SemanticErrorType,
    SortOptions,
)
from exxellence_shared_cga.domain.base import CRUDBase
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from typing import Optional


class CRUDStatus(
    CRUDBase[
        models.RequestStatus,
        schemas.StatusBase,
        schemas.StatusBase,
    ]
):
    def add_new_status(
        self,
        db: Session,
        obj_in: schemas.StatusCreateRequest,
    ) -> schemas.StatusDetails:

        select_statement = sa.select(models.Request).where(models.Request.id == obj_in.request_id)
        request_result = db.execute(select_statement).scalars().first()
        if not request_result:
            raise SemanticError(
                loc={"source": "body", "field": "id"},
                msg="Record with that uuid does not exist",
                type=SemanticErrorType.not_found,
            )
        obj_in.request_id = request_result.sid

        obj_in_data = obj_in.dict(exclude_unset=True)
        db_obj = self.model(**obj_in_data)  # type: ignore
        try:
            db.add(db_obj)
            db.flush()  # generate uuid used in audit log
            db.commit()
        except sa.exc.IntegrityError as e:
            raise SemanticError(
                loc={"source": "body", "field": "request_id"},
                msg="Record with that request_id already exists in given time " + "frame",
                type=SemanticErrorType.not_unique,
            ) from e
        db.refresh(db_obj)
        db_obj.request_id = request_result.id
        return db_obj

    def get_status(
        self,
        database: Session,
        id: uuid.UUID,
    ) -> schemas.StatusDetails:

        select_statement = sa.select(self.model).where(self.model.uuid == id)
        result = database.execute(select_statement).scalars().first()

        if not result:
            raise SemanticError(
                loc={"source": "path", "field": "id"},
                msg="Record not found",
                type=SemanticErrorType.not_found,
            )
        request_statement = sa.select(models.Request).where(
            models.Request.sid == result.request_id
        )
        request_result = database.execute(request_statement).scalars().first()
        if not request_result:
            raise SemanticError(
                loc={"source": "body", "field": "id"},
                msg="Record with that uuid does not exist",
                type=SemanticErrorType.not_found,
            )
        database.refresh(result)
        result.request_id = request_result.id
        return result

    def get_status_list(
        self,
        database: Session,
        sort_options: Optional[SortOptions],
        range_options: Optional[RangeOptions],
        filter_options: Optional[FilterOptions],
    ) -> SearchResult:

        query = sa.select(self.model)  # type: ignore

        result = self.get_multi(
            db=database,
            query=query,
            sort_options=sort_options,
            range_options=range_options,
            filter_options=filter_options,
            return_scalars=True,
        )

        for status in result.result:

            request_statement = sa.select(models.Request).where(
                models.Request.sid == status.request_id
            )

            request_result = database.execute(request_statement).scalars().first()
            if not request_result:
                raise SemanticError(
                    loc={"source": "body", "field": "id"},
                    msg="Record with that uuid does not exist",
                    type=SemanticErrorType.not_found,
                )
            status.request_id = request_result.id
        return result

    def update_status(
        self,
        database: Session,
        id: uuid.UUID,
        status_update_request: schemas.Status,
    ) -> schemas.StatusDetails:
        # Get the status to be updated
        select_statement = sa.select(self.model).where(self.model.uuid == id)
        status_result = database.execute(select_statement).scalars().first()

        # Get the request for the request sid
        request_statement = sa.select(models.Request).where(
            models.Request.id == status_result.request_id
        )
        request_result = database.execute(request_statement).scalars().first()
        if not request_result:
            raise SemanticError(
                loc={"source": "body", "field": "id"},
                msg="Record with that uuid does not exist",
                type=SemanticErrorType.not_found,
            )
        # Set the request_id in Status with the Request sid
        status_result.request_id = request_result.sid
        obj_data = jsonable_encoder(status_result)

        if isinstance(status_result, dict):
            update_data = status_result
        else:
            update_data = status_update_request.dict(exclude_unset=True)
        for field in obj_data:  # type: ignore
            if field in update_data:
                setattr(status_result, field, update_data[field])
        try:
            database.add(status_result)
            database.commit()
        except sa.exc.IntegrityError as e:
            raise SemanticError(
                loc={"source": "body", "field": "name"},
                msg="Unable to update Record",
                type=SemanticErrorType.not_unique,
            ) from e
        database.refresh(status_result)
        # Set the request_id in the Status with the Request id
        status_result.request_id = request_result.id  # type: ignore

        return jsonable_encoder(status_result)


status = CRUDStatus(models.RequestStatus, models)
