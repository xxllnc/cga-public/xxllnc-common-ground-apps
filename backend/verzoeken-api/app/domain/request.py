import sqlalchemy as sa
import uuid
from app import schemas
from app.db import models
from exxellence_shared_cga.core.types import (
    FilterOptions,
    RangeOptions,
    SearchResult,
    SemanticError,
    SemanticErrorType,
    SortOptions,
)
from exxellence_shared_cga.domain.base import CRUDBase
from loguru import logger
from pydantic import parse_obj_as
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session
from typing import Optional, cast


def _process_documents(
    database: Session, request_sid: int, documents: Optional[list[schemas.Document]]
):

    if not (documents) or len(documents) == 0:
        return

    for document in documents:
        if document.storage != "url":
            raise SemanticError(
                loc={"source": "body", "field": "formInput.file.storage"},
                msg="Unknwon storage. Only 'url' is supported",
                type=SemanticErrorType.invalid_input,
            )

        document.request_id = request_sid
        document_request = models.Documents(**document.dict(exclude_unset=True))
        database.add(document_request)

    try:
        database.flush()
        database.commit()

    except IntegrityError as exception:
        logger.error(f"Cannot add document, exception: {exception}")
        raise SemanticError(
            loc={"source": "body", "field": "sid"},
            msg="Unable to create Record, another record with that id already exists",
            type=SemanticErrorType.not_unique,
        ) from exception


class CRUDRequest(
    CRUDBase[
        models.Request,
        schemas.RequestBase,
        schemas.RequestUpdate,
    ]
):
    """Class for request CRUD calls"""

    def get_request(
        self,
        database: Session,
        id: uuid.UUID,
    ) -> schemas.Request:
        """Basic CRUD get item call"""

        select_statement = sa.select(self.model).where(self.model.id == id)
        result = database.execute(select_statement).scalars().first()

        if not result:
            raise SemanticError(
                loc={"source": "path", "field": "id"},
                msg="Record not found",
                type=SemanticErrorType.not_found,
            )

        return result

    def get_request_list(
        self,
        database: Session,
        sort_options: Optional[SortOptions],
        range_options: Optional[RangeOptions],
        filter_options: Optional[FilterOptions],
    ) -> SearchResult:
        """Basic CRUD get collection call"""

        query = sa.select(self.model)  # type: ignore

        return self.get_multi(
            db=database,
            query=query,
            sort_options=sort_options,
            range_options=range_options,
            filter_options=filter_options,
            return_scalars=True,
        )

    def create_request(
        self,
        database: Session,
        request_create_request: schemas.RequestBase,
    ) -> schemas.Request:
        """Basic CRUD create call"""

        documents: Optional[list[schemas.Document]] = None
        if request_create_request.form_input.get("file", False):
            # Get uploade documents form_input and remove file element
            documents = parse_obj_as(
                list[schemas.Document], request_create_request.form_input.get("file", None)
            )
            request_create_request.form_input.__delitem__("file")

        database_request = models.Request(**request_create_request.dict(exclude_unset=True))

        try:
            database.add(database_request)
            database.flush()
            database.commit()
        except IntegrityError as exception:
            logger.error(f"Cannot add request, exception: {exception}")
            raise SemanticError(
                loc={"source": "body", "field": "name"},
                msg="Unable to create Record, another record with that name already exists",  # noqa: E501
                type=SemanticErrorType.not_unique,
            ) from exception

        database.refresh(database_request)

        _process_documents(database, cast(int, database_request.sid), documents)

        return database_request


request = CRUDRequest(models.Request, models)
