"""This module contains the database models"""

import datetime
import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as pg
import uuid
from .session import Base
from enum import Enum
from sqlalchemy.sql.sqltypes import Date


class StatusEnum(str, Enum):

    ACTIVE = "ACTIVE"
    INACTIVE = "INACTIVE"


class PaymentStatusEnum(str, Enum):

    TE_BETALEN = "Te betalen"
    BETALING_INGEDIEND = "Betaling ingediend"
    BETALING_AFGEROND = "Betaling afgerond"


class Request(Base):

    __tablename__ = "request"
    default_sort_field = "registration_date"

    sid = sa.Column(
        sa.Integer,
        sa.Sequence("request_sid_seq", metadata=Base.metadata),
        primary_key=True,
        index=True,
    )
    id = sa.Column(pg.UUID(as_uuid=True), nullable=False, index=True, default=uuid.uuid4)
    registration_date = sa.Column(sa.DateTime, default=datetime.datetime.utcnow)
    form_uuid = sa.Column(sa.String, nullable=False)
    status = sa.Column(sa.Enum(StatusEnum))
    form_input = sa.Column(sa.JSON)


class Documents(Base):

    __tablename__ = "documents"
    default_sort_field = "name"

    sid = sa.Column(
        sa.Integer,
        primary_key=True,
        index=True,
    )
    request_id = sa.Column(sa.Integer, sa.ForeignKey("request.sid"), index=True, nullable=True)
    uuid = sa.Column(pg.UUID(as_uuid=True), nullable=False, index=True, default=uuid.uuid4)
    storage = sa.Column(sa.String, nullable=True)
    name = sa.Column(sa.String, nullable=True)
    url = sa.Column(sa.String, nullable=True)
    size = sa.Column(sa.String, nullable=True)
    type = sa.Column(sa.String, nullable=True)
    original_name = sa.Column(sa.String, nullable=True)


class RequestStatus(Base):

    __tablename__ = "request_status"
    default_sort_field = "uuid"

    sid = sa.Column(
        sa.Integer,
        primary_key=True,
        index=True,
    )
    uuid = sa.Column(
        pg.UUID(as_uuid=True),
        nullable=False,
        index=True,
        default=uuid.uuid4,
        unique=True,
    )
    request_id = sa.Column(sa.Integer, sa.ForeignKey("request.sid"), index=True, unique=True)
    payment_status = sa.Column(sa.Enum(PaymentStatusEnum), nullable=True)
    payment_status_date = sa.Column(Date, default=datetime.date.today, nullable=True)
    request_status = sa.Column(sa.String, nullable=True)
    request_status_date = sa.Column(Date, default=datetime.date.today, nullable=True)
