import os
import sys
from dotenv import load_dotenv
from typing import Final, Union

if "pytest" in sys.modules:
    load_dotenv(".env.test")


def to_int(value: Union[str, None], default_value: int) -> int:
    try:
        return int(value) if value else default_value
    except ValueError:
        return default_value


# The title of the API
PROJECT_NAME: Final[str] = "xxllnc-verzoeken"
BASE_PATH: Final[str] = "/verzoeken/api"
BASE_PATH_V1: Final[str] = f"{BASE_PATH}/v1"
APP_ID: Final[str] = os.environ.get("APP_ID", "")
USE_APP_INSTANCE_ID: Final[bool] = os.environ.get("USE_APP_INSTANCE_ID", "False").upper() == "TRUE"

#############################################
# SQLAlchemy settings
#############################################
SQLALCHEMY_DATABASE_URI: Final[str] = os.environ.get(
    "DATABASE_URL",
    "postgresql://requestregistry:requestregistry123@localhost:5432/requestregistry",
)
# If set to True, all database queries will be logged as they are executed.
SQLALCHEMY_ECHO: Final[bool] = os.environ.get("DATABASE_ECHO", "False").upper() == "TRUE"
SQLALCHEMY_POOL_SIZE: Final[int] = to_int(os.environ.get("SQLALCHEMY_POOL_SIZE", None), 5)
SQLALCHEMY_POOL_MAX_OVERFLOW: Final[int] = to_int(
    os.environ.get("SQLALCHEMY_POOL_MAX_OVERFLOW", None), 10
)

# Maximum number of results from a search. To retrieve more, use paging.
MAX_PAGE_SIZE: Final = int(os.environ.get("MAX_PAGE_SIZE", "100"))

# TODO CGA-1609 remove default value
# The notification URL
NOTIFICATION_URL: Final = os.environ.get(
    "NOTIFICATION_URL",
    "https://5748cff6-31e1-47d3-83d7-c080f24d94fd.bus.koppel.app/webhook",
)

# Flag to enable or disable audit logging
AUDIT_LOGGING: Final = bool(os.environ.get("AUDIT_LOGGING", True))

#############################################
# Auth0 controlpanel token settings
#############################################
AUTH0_ECO_CLIENT_ID: Final[str] = os.environ.get("AUTH0_ECO_CLIENT_ID", "")
AUTH0_ECO_CLIENT_SECRET: Final[str] = os.environ.get("AUTH0_ECO_CLIENT_SECRET", "")
AUTH0_ECO_AUDIENCE: Final[str] = os.environ.get("AUTH0_ECO_AUDIENCE", "")
AUTH0_ECO_DOMAIN: Final[str] = os.environ.get("AUTH0_ECO_DOMAIN", "")

#############################################
# Auth0 redis server settings
#############################################
REDIS_HOST: Final[str] = os.environ.get("REDIS_HOST", "redis")
REDIS_PASSWORD: Final[str | None] = os.environ.get("REDIS_PASSWORD", None)

CONTROLPANEL_URL: Final[str] = os.environ.get("CONTROLPANEL_URL", "")

# Verify
SSL_CERT_FILE: Final[str | bool] = os.environ.get("SSL_CERT_FILE", True)
DAYS_TO_STORE: Final = os.environ.get("DAYS_TO_STORE", "")

# Auth0 Domain
DOMAIN = os.environ["DOMAIN"]

# OIDC client-id, used to verify if we're the audience of access tokens
OIDC_CLIENT_ID = os.environ["OIDC_CLIENT_ID"]

#############################################
# AWS settings
#############################################
BUCKET_NAME = os.environ.get("BUCKET_NAME", "verzoeken-api-temporary-file-upload-store")
AWS_REGION_NAME: Final[str] = os.environ.get("AWS_REGION_NAME", "")
AWS_ACCESS_KEY_ID: Final[str | None] = os.environ.get("AWS_ACCESS_KEY_ID", None)
AWS_SECRET_ACCESS_KEY: Final[str | None] = os.environ.get("AWS_SECRET_ACCESS_KEY", None)
USE_LOCALSTACK_S3_ENDPOINT_URL: Final[bool] = (
    os.environ.get("USE_LOCALSTACK_S3_ENDPOINT_URL", "False").upper() == "TRUE"
)
AWS_S3_ENDPOINT_URL: Final[str | None] = (
    "http://localstack:4566" if USE_LOCALSTACK_S3_ENDPOINT_URL else None
)


# The formulierenbeheer active forms API endpoint
def get_active_forms_api_url(organization_url: str) -> str:
    return f"{organization_url}/formulierenbeheer/api/v1/active/forms"


ZAAKSYSTEEM_URL: Final = os.environ.get(
    "ZAAKSYSTEEM_URL",
    "https://example.com/api/v1/case/create",
)

ZAAKSYSTEEM_API_KEY: Final = os.environ.get(
    "ZAAKSYSTEEM_API_KEY",
    "see bitwarden",
)

ZAAKSYSTEEM_API_INTERFACE_ID: Final = os.environ.get(
    "ZAAKSYSTEEM_API_INTERFACE_ID",
    "see bitwarden",
)
KOPPELAPP_URL: Final[str] = os.environ.get("KOPPELAPP_URL", "")
KOPPELAPP_FORMULIEREN_KEY: Final[str] = os.environ.get("KOPPELAPP_FORMULIEREN_KEY", "")
KOPPELAPP_FORMULIEREN_CERT: Final[str] = os.environ.get("KOPPELAPP_FORMULIEREN_CERT", "")
