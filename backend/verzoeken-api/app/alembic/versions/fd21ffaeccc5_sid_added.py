"""sid added

Revision ID: fd21ffaeccc5
Revises: 443191362ef4
Create Date: 2021-10-21 16:15:14.874332+02:00

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql
from sqlalchemy.schema import CreateSequence, Sequence

# revision identifiers, used by Alembic.
revision = "fd21ffaeccc5"
down_revision = "443191362ef4"
branch_labels = None
depends_on = None


def upgrade():
    """Upgrade"""

    # The next line is mannually added to make the sequence for the id
    op.execute(CreateSequence(Sequence("request_sid_seq")))

    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column("request", sa.Column("sid", sa.Integer(), nullable=False))
    op.alter_column("request", "url", existing_type=sa.VARCHAR(), nullable=True)
    op.alter_column(
        "request",
        "registrationDate",
        existing_type=postgresql.TIMESTAMP(),
        nullable=True,
    )
    op.alter_column("request", "form", existing_type=sa.VARCHAR(), nullable=True)
    op.alter_column(
        "request",
        "status",
        existing_type=postgresql.ENUM("ACTIVE", "INACTIVE", name="status"),
        nullable=True,
    )
    op.alter_column("request", "formInput", existing_type=sa.VARCHAR(), nullable=True)
    op.create_index(op.f("ix_request_id"), "request", ["id"], unique=False)
    op.create_index(op.f("ix_request_sid"), "request", ["sid"], unique=False)
    # ### end Alembic commands ###


def downgrade():
    """Downgrade"""
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f("ix_request_sid"), table_name="request")
    op.drop_index(op.f("ix_request_id"), table_name="request")
    op.alter_column("request", "formInput", existing_type=sa.VARCHAR(), nullable=False)
    op.alter_column(
        "request",
        "status",
        existing_type=postgresql.ENUM("ACTIVE", "INACTIVE", name="status"),
        nullable=False,
    )
    op.alter_column("request", "form", existing_type=sa.VARCHAR(), nullable=False)
    op.alter_column(
        "request",
        "registrationDate",
        existing_type=postgresql.TIMESTAMP(),
        nullable=False,
    )
    op.alter_column("request", "url", existing_type=sa.VARCHAR(), nullable=False)
    op.drop_column("request", "sid")
    # ### end Alembic commands ###
