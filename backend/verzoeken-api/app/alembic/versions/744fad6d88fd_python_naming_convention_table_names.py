""" python naming convention table names

Revision ID: 744fad6d88fd
Revises: a2fc87e5e5b0
Create Date: 2022-01-27 11:47:29.036552+01:00

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = "744fad6d88fd"
down_revision = "a2fc87e5e5b0"
branch_labels = None
depends_on = None


def upgrade():
    """Upgrade"""
    op.rename_table("formStorage", "form_storage")
    op.drop_index("ix_formStorage_id", table_name="form_storage")
    op.drop_index("ix_formStorage_sid", table_name="form_storage")
    op.create_index(op.f("ix_form_storage_id"), "form_storage", ["id"], unique=False)
    op.create_index(op.f("ix_form_storage_sid"), "form_storage", ["sid"], unique=False)
    op.drop_constraint("formStorage_pkey", "form_storage")
    op.create_primary_key("form_storage_pkey", "form_storage", ["sid"])

    op.execute(
        """ALTER TABLE form_storage ALTER COLUMN sid SET DEFAULT nextval('"form_storage_sid_seq"'::regclass);"""  # noqa: E501
    )

    op.execute('DROP SEQUENCE "formStorage_sid_seq"')
    # ### end Alembic commands ###


def downgrade():
    """Downgrade"""
    # ### commands auto generated by Alembic - please adjust! ###
    op.rename_table("form_storage", "formStorage")
    op.drop_index(op.f("ix_form_storage_sid"), table_name="form_storage")
    op.drop_index(op.f("ix_form_storage_id"), table_name="form_storage")
    op.create_index("ix_formStorage_sid", "form_storage", ["sid"], unique=False)
    op.create_index("ix_formStorage_id", "form_storage", ["id"], unique=False)
    # ### end Alembic commands ###
