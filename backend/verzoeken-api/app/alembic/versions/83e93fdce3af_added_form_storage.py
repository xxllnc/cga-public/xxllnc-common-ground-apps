"""added form storage

Revision ID: 83e93fdce3af
Revises: fd21ffaeccc5
Create Date: 2021-10-19 17:27:21.778022+02:00

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql
from sqlalchemy.schema import CreateSequence, Sequence

# revision identifiers, used by Alembic.
revision = "83e93fdce3af"
down_revision = "fd21ffaeccc5"
branch_labels = None
depends_on = None


def upgrade():
    """Upgrade"""

    # The next line is mannually added to make the sequence for the id
    op.execute(CreateSequence(Sequence("form_storage_sid_seq")))

    op.create_table(
        "formStorage",
        sa.Column("sid", sa.Integer(), nullable=False),
        sa.Column("id", postgresql.UUID(as_uuid=True), nullable=False),
        sa.Column("formId", sa.String(), nullable=True),
        sa.Column("form", sa.String(), nullable=True),
        sa.Column("submitter", sa.String(), nullable=True),
        sa.Column("storageDate", sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint("sid"),
    )
    # ### end Alembic commands ###


def downgrade():
    """Downgrade"""
    op.drop_table("formStorage")
    # ### end Alembic commands ###
