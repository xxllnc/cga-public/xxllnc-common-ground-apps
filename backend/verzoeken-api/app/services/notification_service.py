"""This notification_service module is used to notify the
   notification component using the commonground_service"""

from app.services.commonground_service import create_resource
from enum import Enum
from loguru import logger
from requests import Response
from typing import Union


class NotifyActions(Enum):
    """A enum for the possible notification actions"""

    CREATE = "Create"


def notify(action: NotifyActions, uuid: str, body: dict, url: str) -> Union[bool, Response]:
    """Notify the notification component with action and form input
    for a given object (uuid)."""

    if not isinstance(action, NotifyActions):
        logger.error(f"No notification send, because action is not valid. action: {action}")
        return False

    response = create_resource(url, body)

    return response
