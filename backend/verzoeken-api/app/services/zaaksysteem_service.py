import requests as apiRequests
from app import schemas
from app.core import config
from app.core.constant import CONFIG_ITEM_CASE_TYPE
from app.services.helper_functions import get_config_item
from loguru import logger
from requests import Response as RequestResponse
from typing import Final, Union


def create(request: schemas.Request, form_resource: Union[bool, RequestResponse]):
    """creates a case based on the form_resource and config settings"""
    request_headers: Final = {
        "API-Interface-Id": config.ZAAKSYSTEEM_API_INTERFACE_ID,
        "API-Key": config.ZAAKSYSTEEM_API_KEY,
        "Content-Type": "application/json",
        "Accept": "application/json",
    }

    values = {}
    form_values = request.form_input
    for key in form_values:
        if type(form_values[key]) is dict:
            complex_value = form_values[key]
            if complex_value["type"] == "Point" and complex_value["coordinates"] is not None:
                values[key] = [
                    {
                        "geojson": {
                            "type": "FeatureCollection",
                            "features": [
                                {
                                    "type": "Feature",
                                    "properties": {},
                                    "geometry": {
                                        "type": "Point",
                                        "coordinates": complex_value["coordinates"],
                                    },
                                }
                            ],
                        },
                        "address": {"full": "Welbergweg 80, 7556PE Hengelo"},
                    }
                ]
        else:
            values[key] = [form_values[key]]
    case_type = get_config_item(form_resource, CONFIG_ITEM_CASE_TYPE)

    data = {
        "casetype_id": case_type,
        "source": "behandelaar",
        "values": values,
        "requestor": {"type": "employee", "id": {"username": "admin"}},
    }

    response = apiRequests.post(
        url=config.ZAAKSYSTEEM_URL,
        json=data,
        headers=request_headers,
    )

    logger.debug(f"Zaaksysteem content {response.content!r}")
