import requests
from app import schemas
from app.core import config
from app.core.constant import CONFIG_ITEM_CASE_TYPE
from app.services.helper_functions import get_config_item
from exxellence_shared_cga.core.types import ProcessError
from fastapi import Form, UploadFile
from fastapi import status as http_status
from loguru import logger
from pydantic import parse_obj_as
from requests import Response as RequestResponse
from typing import Final, Optional, Union

two_way_cert: Final = (config.KOPPELAPP_FORMULIEREN_CERT, config.KOPPELAPP_FORMULIEREN_KEY)


def post_document(file: UploadFile, case_uuid: str = Form()):

    form_data = {"case_uuid": case_uuid}
    files = {"document_file": (file.filename, file.file, file.content_type)}

    response = requests.post(
        f"{config.KOPPELAPP_URL}/api/v2/document/create_document",
        data=form_data,
        files=files,
        cert=(config.KOPPELAPP_FORMULIEREN_CERT, config.KOPPELAPP_FORMULIEREN_KEY),
    )
    if response.status_code != 200:
        raise ProcessError(
            loc={"source": "backend"},
            msg=response.json(),
            status_code=http_status.HTTP_422_UNPROCESSABLE_ENTITY,
        )

    return None


def create(request: schemas.Request, form_resource: Union[bool, RequestResponse]):
    """creates a case based on the form_resource and config settings"""

    values = {}
    form_values = request.form_input
    for key in form_values:
        if type(form_values[key]) is dict:
            complex_value = form_values[key]
            if complex_value["type"] == "Point" and complex_value["coordinates"] is not None:
                values[key] = [
                    {
                        "geojson": {
                            "type": "FeatureCollection",
                            "features": [
                                {
                                    "type": "Feature",
                                    "properties": {},
                                    "geometry": {
                                        "type": "Point",
                                        "coordinates": complex_value["coordinates"],
                                    },
                                }
                            ],
                        },
                        "address": {"full": "Welbergweg 80, 7556PE Hengelo"},
                    }
                ]
        else:
            values[key] = [form_values[key]]
    case_type = get_config_item(form_resource, CONFIG_ITEM_CASE_TYPE)
    # TODO Find out how to add attributes and connect the geo data to the case and use the
    # generated_cm_schema.
    # Zie https://xxllnc.atlassian.net/wiki/spaces/CGA/pages/966841892868/Koppeling+met+zaaksysteem
    data = {
        "case_uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        "case_type_version_uuid": case_type,
        "contact_channel": "behandelaar",
        "confidentiality": "public",
        "custom_fields": values,
        "options": {"allow_missing_required_fields": True},
    }
    response = requests.post(
        f"{config.KOPPELAPP_URL}/api/v2/cm/case/create_case",
        data=data,
        cert=(config.KOPPELAPP_FORMULIEREN_CERT, config.KOPPELAPP_FORMULIEREN_KEY),
    )
    geo_data = {"uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6", "geojson": values}
    geo_response = requests.post(
        f"{config.KOPPELAPP_URL}/api/v2/geo/create_geo_feature",
        data=geo_data,
        cert=(config.KOPPELAPP_FORMULIEREN_CERT, config.KOPPELAPP_FORMULIEREN_KEY),
    )
    logger.debug(geo_response)

    documents: Optional[list[schemas.Document]] = None
    if request.form_input.get("file", False):
        # Get upload documents form_input
        documents = parse_obj_as(list[schemas.Document], request.form_input.get("file", None))
        logger.debug(documents)

    logger.debug(f"Zaaksysteem content {response.content!r}")
