import requests
from fastapi import HTTPException
from loguru import logger


def call_external_validation_api(organization_url: str, form: dict, form_input: dict):

    verify_if_not_vcap: bool = "vcap.me" not in organization_url

    payload = {"form": form, "formInput": {"data": form_input}}
    response = requests.post(
        url=f"{organization_url}/validation-api/validate", json=payload, verify=verify_if_not_vcap
    )

    # External validation-api is not found, only log error but continue validation
    if response.status_code in {404, 502}:
        logger.error("external validation service not found")
        return

    if response.status_code != 204:
        raise HTTPException(status_code=response.status_code, detail=response.json())
