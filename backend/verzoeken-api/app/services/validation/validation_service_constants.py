"""This module contains all the constants for the validation_service"""

from typing import Final

# Constants
MIME_TYPES: Final = [
    "image/bmp",
    "application/msword",
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    "application/vnd.ms-fontobject",
    "image/gif",
    "text/html",
    "image/jpeg",
    "image/png",
    "application/pdf",
    "image/svg+xml",
    "image/tiff",
    "text/plain",
]


# Functions
def get_type_name(obj) -> str:
    """
    Returns the string with the type name, without the <class> information.

    :param obj: The object
    :type obj: dict
    :return: the string with the type name
    :rtype: str
    """
    return type(obj).__name__


# Messages
ERROR_MESSAGE_FORM_STATUS_INACTIVE: Final = "Er is een fout opgetreden, het formulier is niet gevonden. Neem contact op met de organisatie waarbij u het formulier probeert in te dienen of probeer het later opnieuw."  # noqa: E501
ERROR_MESSAGE_FORM_URL_NOT_VALID: Final = "form: {} is geen geldige url form (json)."
ERROR_MESSAGE_FORM_NOT_FOUND: Final = (
    "form: {} is geen bestaand formulier object in de forms-catalogue."
)
ERROR_MESSAGE_FORM_NOT_PUBLISHED: Final = (
    "form: {} kan niet ingediend worden. Het formulier is nog niet gepubliceerd."  # noqa: E501
)
ERROR_MESSAGE_UNSUPPORTED_MIME_TYPE: Final = "Mimetype: {} is geen geldig mimetype."

ERROR_MESSAGE_TEXT_NOT_VALID: Final = "{} verwacht een input van het type string, niet {}."

ERROR_MESSAGE_NUMBER_NOT_VALID: Final = "{}: {} is geen geldig nummer."
ERROR_MESSAGE_FIELD_IS_MANDATORY: Final = "{} is een verplicht veld."
ERROR_MESSAGE_FIELD_MIN_LENGTH: Final = "{} is te kort, {} heeft een minimum lengte van {}."
ERROR_MESSAGE_FIELD_MAX_LENGTH: Final = "{} is te lang, {} heeft een maximum lengte van {}."

ERROR_MESSAGE_FIELD_PATTERN_NOT_STRING: Final = "{} verwacht een input van het type string (niet {}) om te controleren of deze voldoet aan de volgende regular expression: {}."  # noqa: E501
ERROR_MESSAGE_FIELD_PATTERN: Final = (
    "{} moet overeenkomen met de volgende regular expression: {}."  # noqa: E501
)
ERROR_MESSAGE_FIELD_MIN_VALUE: Final = "{} is te laag, {} heeft een minimum waarde van {}."
ERROR_MESSAGE_FIELD_MAX_VALUE: Final = "{} is te hoog, {} heeft een maximum waarde van {}."
ERROR_MESSAGE_EMAIL_NOT_VALID: Final = "{}: {} is geen geldig e-mailadres."
ERROR_MESSAGE_URL_NOT_VALID: Final = "{}: {} is geen geldige url."
ERROR_MESSAGE_URI_NOT_VALID: Final = "{}: {} is geen geldige uri. See https://www.w3.org/Addressing/URL/4_URI_Recommentations.html for more information."  # noqa: E501
ERROR_MESSAGE_CHECKBOX_NOT_VALID: Final = "{}: {} is geen geldige input voor een checkbox, hier word een value van het type boolean verwacht (of string 'true' / 'false'), niet {}."  # noqa: E501
ERROR_MESSAGE_RADIO_NOT_VALID: Final = "{}: {} is geen geldige input voor een radio, hier word een value van het type str verwacht, niet {}."  # noqa: E501
ERROR_MESSAGE_RADIO_VALUE_NOT_VALID: Final = "{}: {} is geen geldige input, de gegeven optie komt niet overeen met een van de opties van {}."  # noqa: E501
ERROR_MESSAGE_DATE_NOT_VALID: Final = "{}: {} is geen geldige datum, geef een geldige dag/datum op, die een d-m-Y formaat heeft."  # noqa: E501
ERROR_MESSAGE_TIME_NOT_VALID: Final = "{}: {} is geen geldige tijd, geef een geldige tijd op, die een HH:mm formaat heeft."  # noqa: E501
ERROR_MESSAGE_DATETIME_NOT_VALID: Final = "{}: {} is geen geldige datum en tijd, geef een geldige datum en tijd op, dat voldoet aan de ISO 8601 format."  # noqa: E501
ERROR_MESSAGE_SELECTBOX_NOT_VALID: Final = "{}: {} is geen geldige input voor selectboxes, hier word een value van het type dict verwacht, niet {}."  # noqa: E501
ERROR_MESSAGE_SELECTBOX_VALUE_NOT_VALID: Final = "{}: {} is geen geldige input, de gegeven opties komen niet overeen met de opties van {}"  # noqa: E501
ERROR_MESSAGE_SELECT_NOT_VALID: Final = "{}: {} is geen geldige input voor een select, hier word een value van het type list of str verwacht, niet {}."  # noqa: E501
ERROR_MESSAGE_SELECT_VALUE_NOT_VALID: Final = "{}: {} is geen geldige input, de gegeven optie komt niet overeen met een van de opties van {}"  # noqa: E501
ERROR_MESSAGE_SELECT_VALUES_NOT_VALID: Final = "{}: {} is geen geldige input, de gegeven opties komen niet overeen met de opties van {}"  # noqa: E501
ERROR_MESSAGE_SELECT_VALUES_ONE_VALUE_ALLOWED: Final = "{}: {} is geen geldige input voor deze select, hier word maar een geselecteerde optie verwacht."  # noqa: E501
ERROR_MESSAGE_PHONE_NUMBER_NOT_VALID: Final = "{}: {} is geen geldig telefoonnummer. Geef een geldig telefoonnummer dat voldoet aan de E.164 standaard."  # noqa: E501
ERROR_MESSAGE_CURRENCY_NOT_VALID: Final = (
    "{}: {} is geen geldige currency, verwacht 2 decimalen achter de comma."  # noqa: E501
)
ERROR_MESSAGE_DATA_URL_NOT_VALID: Final = "{}: {} is geen geldige Data URL."
ERROR_MESSAGE_DATA_URL_BASE64_DATA_NOT_VALID: Final = "{}: {} is geen geldige base64."
ERROR_MESSAGE_ATTACHMENT_VALUE_NOT_DICT: Final = "{}: {} is geen geldige input voor een file/image, hier word een value van het type dict verwacht, niet {}."  # noqa: E501
ERROR_MESSAGE_ATTACHMENT_VALUE_NOT_VALID: Final = "{}: {} is geen geldige input voor een attachment, hier word een value van het type list verwacht, niet {}."  # noqa: E501
ERROR_MESSAGE_FILE_NOT_VALID: Final = "{}: {} is geen geldige file/image."
ERROR_MESSAGE_FILE_KEY_NOT_EXISTS: Final = (
    "{}: {} is geen geldige file/image, omdat een van de volgende keys ontbreekt: {}"  # noqa: E501
)
ERROR_MESSAGE_FILE_STORAGE_NOT_VALID: Final = "{}: {} is geen geldige storage optie."
ERROR_MESSAGE_FILE_STORAGE_TYPE_NOT_VALID: Final = "{}: {} is geen ondersteunde file type."
ERROR_MESSAGE_PASSWORD_NOT_VALID: Final = "{}: {} is geen geldig wachtwoord."
ERROR_MESSAGE_LOCATION_NOT_VALID: Final = "{}: {} is geen geldige locatie"
