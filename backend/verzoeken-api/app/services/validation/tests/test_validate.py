import app.services.validation.validation_service_constants as vsc
from app.services.validation.validation_service import ValidationService
from typing import Any


def _assert_field_is_mandatory(error_message: str, key: str):
    assert error_message == vsc.ERROR_MESSAGE_FIELD_IS_MANDATORY.format(key)


def _assert_field_currency_not_valid(error_message: str, key: str, value: Any):
    assert error_message == vsc.ERROR_MESSAGE_CURRENCY_NOT_VALID.format(key, value)


def _assert_field_text_not_valid(error_message: str, key: str, value: Any):
    assert error_message == vsc.ERROR_MESSAGE_TEXT_NOT_VALID.format(key, vsc.get_type_name(value))


def _assert_field_checkbox_not_valid(error_message: str, key: str, value: Any):
    assert error_message == vsc.ERROR_MESSAGE_CHECKBOX_NOT_VALID.format(
        key, value, vsc.get_type_name(value)
    )


def _assert_field_min_length_not_valid(error_message: str, key: str, min_length: int):
    assert error_message == vsc.ERROR_MESSAGE_FIELD_MIN_LENGTH.format(key, key, min_length)


def _assert_field_max_length_not_valid(error_message: str, key: str, max_length: int):
    assert error_message == vsc.ERROR_MESSAGE_FIELD_MAX_LENGTH.format(key, key, max_length)


def _assert_field_pattern_not_valid(error_message: str, key: str, pattern: str):
    assert error_message == vsc.ERROR_MESSAGE_FIELD_PATTERN.format(key, pattern)


def _assert_field_pattern_not_string(error_message: str, key: str, value: Any, pattern: str):
    assert error_message == vsc.ERROR_MESSAGE_FIELD_PATTERN_NOT_STRING.format(
        key, vsc.get_type_name(value), pattern
    )


def _assert_field_url_not_valid(error_message: str, key: str, value: Any):
    assert error_message == vsc.ERROR_MESSAGE_URL_NOT_VALID.format(key, vsc.get_type_name(value))


def _assert_field_uri_not_valid(error_message: str, key: str, value: str):
    assert error_message == vsc.ERROR_MESSAGE_URI_NOT_VALID.format(key, value)


def _assert_field_phone_number_not_valid(error_message: str, key: str, value: str):
    assert error_message == vsc.ERROR_MESSAGE_PHONE_NUMBER_NOT_VALID.format(key, value)


def _assert_field_datetime_not_valid(error_message: str, key: str, value: str):
    assert error_message == vsc.ERROR_MESSAGE_DATETIME_NOT_VALID.format(key, value)


def _assert_field_date_not_valid(error_message: str, key: str, value: str):
    assert error_message == vsc.ERROR_MESSAGE_DATE_NOT_VALID.format(key, value)


def _assert_field_number_not_valid(error_message: str, key: str, value: Any):
    assert error_message == vsc.ERROR_MESSAGE_NUMBER_NOT_VALID.format(
        key, vsc.get_type_name(value)
    )


def _assert_field_min_value_not_valid(error_message: str, key: str, value: int):
    assert error_message == vsc.ERROR_MESSAGE_FIELD_MIN_VALUE.format(key, key, value)


def _assert_field_max_value_not_valid(error_message: str, key: str, value: int):
    assert error_message == vsc.ERROR_MESSAGE_FIELD_MAX_VALUE.format(key, key, value)


def _assert_field_radio_not_valid(error_message: str, key: str, value: Any):
    assert error_message == vsc.ERROR_MESSAGE_RADIO_NOT_VALID.format(
        key, str(value), vsc.get_type_name(value)
    )


def _assert_field_radio_value_not_valid(error_message: str, key: str, value: str):
    assert error_message == vsc.ERROR_MESSAGE_RADIO_VALUE_NOT_VALID.format(key, value, key)


def _assert_field_select_not_valid(error_message: str, key: str, value: Any):
    assert error_message == vsc.ERROR_MESSAGE_SELECT_NOT_VALID.format(
        key, value, vsc.get_type_name(value)
    )


def _assert_field_select_value_not_valid(error_message: str, key: str, value: str):
    assert error_message == vsc.ERROR_MESSAGE_SELECT_VALUE_NOT_VALID.format(key, value, key)


def _assert_field_select_values_not_valid(error_message: str, key: str, value: str):
    assert error_message == vsc.ERROR_MESSAGE_SELECT_VALUES_NOT_VALID.format(key, value, key)


def _assert_field_select_values_one_value_allowed(error_message: str, key: str, value: str):
    assert error_message == vsc.ERROR_MESSAGE_SELECT_VALUES_ONE_VALUE_ALLOWED.format(key, value)


def _assert_field_selectboxes_not_valid(error_message: str, key: str, value: Any):
    assert error_message == vsc.ERROR_MESSAGE_SELECTBOX_NOT_VALID.format(
        key, str(value), vsc.get_type_name(value)
    )


def _assert_field_selectboxes_value_not_valid(error_message: str, key: str, value: str):
    assert error_message == vsc.ERROR_MESSAGE_SELECTBOX_VALUE_NOT_VALID.format(key, value, key)


def _assert_field_time_not_valid(error_message: str, key: str, value: str):
    assert error_message == vsc.ERROR_MESSAGE_TIME_NOT_VALID.format(key, value)


def _assert_field_file_not_valid(error_message: str, key: str, value: str):
    assert error_message == vsc.ERROR_MESSAGE_FILE_NOT_VALID.format(key, value)


def _assert_field_file_key_not_exists(error_message: str, key: str, value: str, keys: list):
    assert error_message == vsc.ERROR_MESSAGE_FILE_KEY_NOT_EXISTS.format(key, value, keys)


def _assert_field_file_storage_not_valid(error_message: str, key: str, value: str):
    assert error_message == vsc.ERROR_MESSAGE_FILE_STORAGE_NOT_VALID.format(key, value)


def _assert_field_file_type_not_valid(error_message: str, key: str, storage_type: str):
    assert error_message == vsc.ERROR_MESSAGE_FILE_STORAGE_TYPE_NOT_VALID.format(key, storage_type)


def _assert_field_attachment_type_base64_not_valid(error_message: str, key: str, value: str):
    assert error_message == vsc.ERROR_MESSAGE_DATA_URL_NOT_VALID.format(key, value)


def _assert_field_attachment_type_base64_data_not_valid(error_message: str, key: str, value: str):
    assert error_message == vsc.ERROR_MESSAGE_DATA_URL_BASE64_DATA_NOT_VALID.format(key, value)


def _assert_field_attachment_input_value_not_valid(error_message: str, key: str, value: Any):
    assert error_message == vsc.ERROR_MESSAGE_ATTACHMENT_VALUE_NOT_VALID.format(
        key, value, vsc.get_type_name(value)
    )


def _assert_field_attachment_input_value_not_dict(error_message: str, key: str, value: Any):
    assert error_message == vsc.ERROR_MESSAGE_ATTACHMENT_VALUE_NOT_DICT.format(
        key, value, vsc.get_type_name(value)
    )


def _assert_field_attachment_unsupported_mime_type(error_message: str, value: Any):
    assert error_message == vsc.ERROR_MESSAGE_UNSUPPORTED_MIME_TYPE.format(value)


def _assert_field_email_not_valid(error_message: str, key: str, value: str):
    assert error_message == vsc.ERROR_MESSAGE_EMAIL_NOT_VALID.format(key, value)


def _assert_field_password_not_valid(error_message: str, key: str, value: str):
    assert error_message == vsc.ERROR_MESSAGE_PASSWORD_NOT_VALID.format(key, value)


def _assert_field_location_not_valid(error_message: str, key: str, value: str):
    assert error_message == vsc.ERROR_MESSAGE_LOCATION_NOT_VALID.format(key, value)


# ---------------------------------------------
# Validate the minimum length for inputfield
# ---------------------------------------------
def _test_validate_min_length(
    component_data_input: dict[str, Any],
    input_value: str,
    min_length_value: int,
    expect_error: bool,
    nr_of_errors: int,
):
    component_data: dict[str, Any] = {}
    component_data.update(component_data_input)

    for validate_data in [
        {"minLength": min_length_value},
    ]:
        component_data["validate"] = validate_data

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = input_value
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        if expect_error:
            assert len(validation_service.errors.get_errors()) == nr_of_errors
            _assert_field_min_length_not_valid(
                validation_service.errors.get_errors()[0],
                component_data["key"],
                component_data["validate"]["minLength"],
            )
        else:
            assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate the maximum length for inputfield
# ---------------------------------------------
def _test_validate_max_length(
    component_data_input: dict[str, Any],
    input_value: str,
    max_length_value: int,
    expect_error: bool,
    nr_of_errors: int,
):
    component_data: dict[str, Any] = {}
    component_data.update(component_data_input)

    for validate_data in [
        {"maxLength": max_length_value},
    ]:
        component_data["validate"] = validate_data

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = input_value
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        if expect_error:
            assert len(validation_service.errors.get_errors()) == nr_of_errors
            _assert_field_max_length_not_valid(
                validation_service.errors.get_errors()[0],
                component_data["key"],
                component_data["validate"]["maxLength"],
            )
        else:
            assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate the pattern for an inputfield
# ---------------------------------------------
def _test_validate_pattern(
    component_data_input: dict[str, Any],
    validate_data: Any,
    input_value: str,
    expect_error: bool,
    nr_of_errors: int,
):
    component_data: dict[str, Any] = {}
    component_data.update(component_data_input)

    component_data["validate"] = validate_data

    form_input_dict: dict[str, Any] = {}
    form_input_dict[component_data["key"]] = input_value
    component_data.update(form_input_dict)

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input_dict
    validation_service.validate_value(component_data)
    if expect_error:
        assert len(validation_service.errors.get_errors()) == nr_of_errors
        _assert_field_pattern_not_valid(
            validation_service.errors.get_errors()[0],
            component_data["key"],
            component_data["validate"]["pattern"],
        )
    else:
        assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate the pattern for an inputfield
# ---------------------------------------------
def _test_validate_pattern_not_string(
    component_data: dict[str, Any], form_input_dict: dict[str, Any]
):
    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input_dict
    validation_service.validate_value(component_data)
    assert len(validation_service.errors.get_errors()) == 2
    _assert_field_text_not_valid(
        validation_service.errors.get_errors()[0],
        component_data["key"],
        form_input_dict[component_data["key"]],
    )
    _assert_field_pattern_not_string(
        validation_service.errors.get_errors()[1],
        component_data["key"],
        form_input_dict[component_data["key"]],
        component_data["validate"]["pattern"],
    )
