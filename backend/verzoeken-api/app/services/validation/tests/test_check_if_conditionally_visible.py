import pytest
from app.services.validation.check_if_conditionally_visible import check_if_conditionally_visible
from typing import Any, Union

form_input: dict[str, Union[list, dict, str, int, float]] = {
    "select": "selectedValue",
    "selectBox": {"valueA": True, "valueB": False},
    "multiSelect": ["valueA", "valueB"],
    "radio": "value1",
    "currency": 12.13,
    "time": "16:30:00",
    "textField1": "value1",
    "textField2": ["value1", "value2", "value3"],
    "textArea1": "value1",
    "textArea2": ["value1", "value2", "value3"],
    "number1": 1,
    "number2": [8, 9, 10],
    "number3": 12.13,
    "number4": [10.1, 10.2, 100.3],
    "checkbox1": True,
    "checkbox2": False,
    "selectString": "value1",  # select with dataType string or auto
    "selectBoolean": True,  # select with dataType boolean or auto
    "selectNummer": 1,  # select with dataType number or auto
    "dateTime1": "2022-04-04T12:00:00+02:00",  # format: yyyy-MM-dd hh:mm a
}


@pytest.mark.parametrize(
    "when, eq, visible",
    [
        ("select", "selectedValue", True),
        ("select", "otherValue", False),
        ("selectString", "value1", True),
        ("selectString", "value2", False),
        ("selectBoolean", "true", True),
        ("selectBoolean", "false", False),
        ("selectNummer", "1", True),
        ("selectNummer", "2", False),
        ("selectBox", "valueA", True),
        ("selectBox", "valueB", False),
        ("selectBox", "unknownValue", False),
        ("multiSelect", "valueA", True),
        ("multiSelect", "valueB", True),
        ("multiSelect", "unknownValue", False),
        ("checkbox1", "true", True),
        ("checkbox1", "false", False),
        ("checkbox2", "true", False),
        ("checkbox2", "false", True),
        ("radio", "value1", True),
        ("radio", "value2", False),
        ("currency", "12.13", True),
        ("currency", "11.99", False),
        ("time", "16:30:00", True),
        ("time", "16:30:01", False),
        ("textField1", "value1", True),
        ("textField1", "value2", False),
        ("textField1", "value2", False),
        ("textField2", "value2", True),
        ("textField2", "value3", True),
        ("textField2", "value", False),
        ("textArea1", "value1", True),
        ("textArea1", "value2", False),
        ("textArea2", "value1", True),
        ("textArea2", "value2", True),
        ("textArea2", "value3", True),
        ("textArea2", "value", False),
        ("number1", "1", True),
        ("number1", "2", False),
        ("number2", "8", True),
        ("number2", "9", True),
        ("number2", "10", True),
        ("number2", "1", False),
        ("number3", "12.13", True),
        ("number3", "12.12", False),
        ("number4", "10.1", True),
        ("number4", "10.2", True),
        ("number4", "100.3", True),
        ("number4", "100.30", False),
        ("number4", "9.90", False),
        ("dateTime1", "2022-04-04T12:00:00+02:00", True),
        ("dateTime1", "2022-04-04T11:00:00+02:00", False),
    ],
)
def test_check_if_conditionally_visible(when: str, eq: str, visible: bool):

    component: dict[str, Any] = {
        "key": "test_conditions",
        "conditional": {
            "show": True,
            "when": when,
            "eq": eq,
            "json": "",
        },
        "customConditional": "",
    }

    assert check_if_conditionally_visible(component, form_input) == visible

    # Flip show, so the field is not visible when the expression matches
    component["conditional"]["show"] = False  # type: ignore
    assert check_if_conditionally_visible(component, form_input) == (not visible)
