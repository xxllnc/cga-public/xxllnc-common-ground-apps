import pytest
from app import schemas
from app.db.models import StatusEnum
from app.services.validation.validation_service import ValidationService


def test_create_form_input_dict():

    fields_with_values = {
        "tEst": "test",
        "name": "name",
        "confirmationCheckBox": True,
    }
    fields_with_empty_string = {
        "lastName": "",
        "watIsUwVraag": "",
        "email": "",
        "datetime": "",
    }

    # The user input
    request_create_or_update_request = schemas.RequestBase(
        form_uuid="713e6fed-0718-4fbd-a0d1-eb0fd64bc1c8",
        status=StatusEnum.ACTIVE,
        form_input=fields_with_values | fields_with_empty_string,
    )

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.request_create_or_update_request = request_create_or_update_request

    error = validation_service.create_form_input_dict()
    assert error is None
    assert len(validation_service.errors.get_errors()) == 0
    assert int(validation_service.errors.error_amount) == 0
    assert validation_service.form_input_dict == fields_with_values


@pytest.mark.parametrize("form_input", ["", {}, ([])])
def test_create_form_input_dict_with_invalid_data(form_input):

    # The user input
    request_create_or_update_request = schemas.RequestBase(
        form_uuid="713e6fed-0718-4fbd-a0d1-eb0fd64bc1c8",
        status=StatusEnum.ACTIVE,
        form_input=form_input,
    )

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.request_create_or_update_request = request_create_or_update_request

    error = validation_service.create_form_input_dict()
    assert error is None
    assert len(validation_service.errors.get_errors()) == 0
    assert int(validation_service.errors.error_amount) == 0
