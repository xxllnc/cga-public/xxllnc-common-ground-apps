import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation.validation_service import ValidationService
from typing import Any, Final

COMPONENT_DATA: Final = {
    "key": "age",
    "type": "number",
}


# ---------------------------------------------
# Validate an optional inputfield with type number
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error, nr_of_errors",
    [
        (0, False, 0),
        (50, False, 0),
        (-50, True, 1),
        ("", True, 1),
        ("notvalid", True, 1),
        (None, False, 0),
    ],
)
def test_validate_optional(input_value: Any, expect_error: bool, nr_of_errors: int):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {},
        {"required": False},
    ]:
        component_data["validate"] = validate_data

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = input_value
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        if expect_error:
            assert len(validation_service.errors.get_errors()) == nr_of_errors
            tv._assert_field_number_not_valid(
                validation_service.errors.get_errors()[0],
                component_data["key"],
                form_input_dict[component_data["key"]],
            )
        else:
            assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate a mandatory inputfield with type number
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error, nr_of_errors",
    [
        ("", True, 2),
        ("notvalid", True, 1),
        (0, False, 0),
    ],
)
def test_validate_mandatory(input_value: Any, expect_error: bool, nr_of_errors: int):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    component_data["validate"] = {"required": True}

    form_input_dict: dict[str, Any] = {}
    form_input_dict[component_data["key"]] = input_value
    component_data.update(form_input_dict)

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input_dict
    validation_service.validate_value(component_data)
    if expect_error:
        assert len(validation_service.errors.get_errors()) == nr_of_errors
        tv._assert_field_number_not_valid(
            validation_service.errors.get_errors()[0],
            component_data["key"],
            form_input_dict[component_data["key"]],
        )
        if nr_of_errors > 1:
            tv._assert_field_is_mandatory(
                validation_service.errors.get_errors()[1],
                component_data["key"],
            )
    else:
        assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate the min value for a inputfield with type number
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_data, expect_error, nr_of_errors",
    [
        (2, False, 0),
        (3, False, 0),
        (0, True, 1),
        (1, True, 1),
    ],
)
def test_validate_min_value(input_data: int, expect_error: bool, nr_of_errors: int):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {"min": 2},
    ]:
        component_data["validate"] = validate_data

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = input_data
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        if expect_error:
            assert len(validation_service.errors.get_errors()) == nr_of_errors
            tv._assert_field_min_value_not_valid(
                validation_service.errors.get_errors()[0],
                component_data["key"],
                component_data["validate"]["min"],
            )
        else:
            assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate the max value for a inputfield with type number
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_data, expect_error, nr_of_errors",
    [
        (0, False, 0),
        (10, False, 0),
        (11, True, 1),
    ],
)
def test_validate_max_value(input_data: int, expect_error: bool, nr_of_errors: int):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {"max": 10},
    ]:
        component_data["validate"] = validate_data

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = input_data
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        if expect_error:
            assert len(validation_service.errors.get_errors()) == nr_of_errors
            tv._assert_field_max_value_not_valid(
                validation_service.errors.get_errors()[0],
                component_data["key"],
                component_data["validate"]["max"],
            )
        else:
            assert len(validation_service.errors.get_errors()) == 0
