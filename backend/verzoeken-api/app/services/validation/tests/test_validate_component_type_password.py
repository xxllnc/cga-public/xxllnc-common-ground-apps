import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation.validation_service import ValidationService
from typing import Any, Final

COMPONENT_DATA: Final = {
    "key": "password",
    "type": "password",
}


# ---------------------------------------------
# Validate an optional inputfield with type password
# Test validation when value is an empty string.
# ---------------------------------------------
def test_validate_password_empty():
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [{}, {"required": False}]:
        component_data["validate"] = validate_data

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = ""
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        assert len(validation_service.errors.get_errors()) == 1
        tv._assert_field_password_not_valid(
            validation_service.errors.get_errors()[0],
            component_data["key"],
            form_input_dict[component_data["key"]],
        )


# ---------------------------------------------
# Validate an optional inputfield with type password
# Test validation when value is None and
# field is optional.
# ---------------------------------------------
def test_validate_password_optional():
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    component_data["validate"] = {"required": False}

    form_input_dict: dict[str, Any] = {}
    form_input_dict[component_data["key"]] = None
    component_data.update(form_input_dict)

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input_dict
    validation_service.validate_value(component_data)
    assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate an optional inputfield with type password
# Test validation when value is None and
# field is mandatory.
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value",
    [
        None,
    ],
)
def test_validate_password_mandatory_none(input_value: Any):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    component_data["validate"] = {"required": True}

    form_input_dict: dict[str, Any] = {}
    form_input_dict[component_data["key"]] = None
    component_data.update(form_input_dict)

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input_dict
    validation_service.validate_value(component_data)
    assert len(validation_service.errors.get_errors()) == 1
    tv._assert_field_is_mandatory(
        validation_service.errors.get_errors()[0],
        component_data["key"],
    )


# ---------------------------------------------
# Validate an optional inputfield with type password
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error, nr_of_errors",
    [
        ("pAsWoRd12!!", False, 0),
        (0, True, 1),
        (None, False, 0),
    ],
)
def test_validate_optional(input_value: Any, expect_error: bool, nr_of_errors: int):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {},
        {"required": False},
    ]:
        component_data["validate"] = validate_data

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = input_value
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        if expect_error:
            assert len(validation_service.errors.get_errors()) == nr_of_errors
            tv._assert_field_text_not_valid(
                validation_service.errors.get_errors()[0],
                component_data["key"],
                form_input_dict[component_data["key"]],
            )
        else:
            assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate a mandatory inputfield with type password
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error, nr_of_errors",
    [
        ("pAsWoRd12!!", False, 0),
        ("", True, 2),
    ],
)
def test_validate_mandatory(input_value: Any, expect_error: bool, nr_of_errors: int):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {"required": True},
    ]:
        component_data["validate"] = validate_data

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = input_value
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        if expect_error:
            assert len(validation_service.errors.get_errors()) == nr_of_errors
            tv._assert_field_password_not_valid(
                validation_service.errors.get_errors()[0],
                component_data["key"],
                form_input_dict[component_data["key"]],
            )
            tv._assert_field_is_mandatory(
                validation_service.errors.get_errors()[1],
                component_data["key"],
            )
        else:
            assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate the minimum length for inputfield with type password
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error, nr_of_errors",
    [
        ("pAsWoRd12!__3$#$DFsVdfadf@#!$", False, 0),
        ("pAsWoRd12!!", True, 1),
    ],
)
def test_validate_min_length(input_value: str, expect_error: bool, nr_of_errors: int):
    tv._test_validate_min_length(COMPONENT_DATA, input_value, 28, expect_error, nr_of_errors)


# ---------------------------------------------
# Validate the maximum length for inputfield with type password
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error, nr_of_errors",
    [
        ("pAsWoRd12!!", False, 0),
        ("pAsWoRd12!__3$#$DFsVdfadf@#!$", True, 1),
    ],
)
def test_validate_max_length(input_value: str, expect_error: bool, nr_of_errors: int):
    tv._test_validate_max_length(COMPONENT_DATA, input_value, 11, expect_error, nr_of_errors)


# ---------------------------------------------
# Validate the pattern for a inputfield with type password
# ---------------------------------------------
@pytest.mark.parametrize(
    "validate_data, input_value, expect_error, nr_of_errors",
    [
        ({"pattern": ".*"}, "pAsWoRd12!!", False, 0),
        (
            {"pattern": "^([A-Za-z0-9$%#!]{10,20})$"},
            "pAsWoRd12__",
            True,
            1,
        ),
        (
            {"pattern": "^([A-Za-z0-9$%#!]{10,20})$"},
            "pAsWoRd12!!",
            False,
            0,
        ),
    ],
)
def test_validate_pattern(
    validate_data: Any, input_value: str, expect_error: bool, nr_of_errors: int
):
    tv._test_validate_pattern(
        COMPONENT_DATA, validate_data, input_value, expect_error, nr_of_errors
    )


# ---------------------------------------------
# Validate the pattern for inputfield with type password
# ---------------------------------------------
@pytest.mark.parametrize(
    "validate_data, input_value",
    [
        ({"pattern": ".*"}, 10),
        ({"pattern": ".*"}, {}),
        ({"pattern": ".*"}, True),
    ],
)
def test_validate_pattern_not_string(validate_data: dict[str, str], input_value: Any):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    component_data["validate"] = validate_data

    form_input_dict: dict[str, Any] = {}
    form_input_dict[component_data["key"]] = input_value
    component_data.update(form_input_dict)

    tv._test_validate_pattern_not_string(component_data, form_input_dict)
