import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation.validation_service import ValidationService
from typing import Any, Final

COMPONENT_LAYOUT_COLUMNS_DATA: Final = {
    "key": "columns",
    "type": "columns",
    "columns": [],
}

COMPONENT_TEXTFIELD_DATA: Final = {
    "key": "lastname",
    "type": "textfield",
}


# ---------------------------------------------
# Validate an optional inputfield with type textfield
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error",
    [
        ("", False),
        ("last", False),
        (0, True),
    ],
)
def test_validate_optional(input_value: Any, expect_error: bool):
    for validate_data in [
        {},
        {"required": False},
    ]:
        component_textfield_data: dict[str, Any] = {}
        component_textfield_data.update(COMPONENT_TEXTFIELD_DATA)
        component_textfield_data["validate"] = validate_data

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_textfield_data["key"]] = input_value
        component_textfield_data.update(form_input_dict)

        component_textfield_data_list: list = []
        component_textfield_data_list.append(component_textfield_data)

        columns_data: dict[str, Any] = {}
        columns_data["components"] = component_textfield_data_list

        columns_list: list = []
        columns_list.append(columns_data)

        component_data: dict[str, Any] = {}
        component_data.update(COMPONENT_LAYOUT_COLUMNS_DATA)
        component_data["columns"] = columns_list

        form_properties: list = []
        form_properties.append(component_data)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_form_input_dict(form_properties)
        if expect_error:
            tv._assert_field_text_not_valid(
                validation_service.errors.get_errors()[0],
                component_textfield_data["key"],
                form_input_dict[component_textfield_data["key"]],
            )
        else:
            assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate a mandatory inputfield with type textfield
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error",
    [("", True), ("last", False), (None, True)],
)
def test_validate_mandatory(input_value: Any, expect_error: bool):

    component_textfield_data: dict[str, Any] = {}
    component_textfield_data.update(COMPONENT_TEXTFIELD_DATA)
    component_textfield_data["validate"] = {"required": True}

    form_input_dict: dict[str, Any] = {}
    form_input_dict[component_textfield_data["key"]] = input_value
    component_textfield_data.update(form_input_dict)

    component_textfield_data_list: list = []
    component_textfield_data_list.append(component_textfield_data)

    columns_data: dict[str, Any] = {}
    columns_data["components"] = component_textfield_data_list

    columns_list: list = []
    columns_list.append(columns_data)

    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_LAYOUT_COLUMNS_DATA)
    component_data["columns"] = columns_list

    form_properties: list = []
    form_properties.append(component_data)

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input_dict
    validation_service.validate_form_input_dict(form_properties)
    if expect_error:
        tv._assert_field_is_mandatory(
            validation_service.errors.get_errors()[0],
            component_textfield_data["key"],
        )

    else:
        assert len(validation_service.errors.get_errors()) == 0
