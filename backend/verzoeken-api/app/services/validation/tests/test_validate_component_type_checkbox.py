import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation.validation_service import ValidationService
from typing import Any, Final

COMPONENT_DATA: Final = {
    "key": "valid",
    "type": "checkbox",
}


# ---------------------------------------------
# Validate an optional inputfield with type checkbox
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error",
    [
        ("", True),
        ("notvalid", True),
        (0, True),
        (None, False),
        (False, False),
        (True, False),
        ("True", False),
        ("False", False),
        ("TrUe", False),
        ("FaLsE", False),
    ],
)
def test_validate_optional(input_value: Any, expect_error: bool):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {},
        {"required": False},
    ]:
        component_data["validate"] = validate_data

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = input_value
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        if expect_error:
            tv._assert_field_checkbox_not_valid(
                validation_service.errors.get_errors()[0],
                component_data["key"],
                form_input_dict[component_data["key"]],
            )
        else:
            assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate a mandatory inputfield with type checkbox
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error, nr_of_errors",
    [
        ("", True, 2),
        ("notvalid", True, 1),
        (False, False, 0),
        (True, False, 0),
    ],
)
def test_validate_mandatory(input_value: Any, expect_error: bool, nr_of_errors: int):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    component_data["validate"] = {"required": True}

    form_input_dict: dict[str, Any] = {}
    form_input_dict[component_data["key"]] = input_value
    component_data.update(form_input_dict)

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input_dict
    validation_service.validate_value(component_data)
    if expect_error:
        assert len(validation_service.errors.get_errors()) == nr_of_errors
        tv._assert_field_checkbox_not_valid(
            validation_service.errors.get_errors()[0],
            component_data["key"],
            form_input_dict[component_data["key"]],
        )
        if nr_of_errors > 1:
            tv._assert_field_is_mandatory(
                validation_service.errors.get_errors()[1],
                component_data["key"],
            )
    else:
        assert len(validation_service.errors.get_errors()) == 0
