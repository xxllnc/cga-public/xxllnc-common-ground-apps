import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation.validation_service import ValidationService
from typing import Any, Final

KEYS: Final = ["storage", "name", "url", "size", "type", "originalName"]


COMPONENT_DATA: Final = {
    "key": "file",
    "type": "file",
}

FORM_INPUT_FILE_DATA_BASE64: Final = {
    "storage": "base64",
    "name": "test-38c9ec05-a9cf-41d5-a377-d098cf9813b2.txt",
    "url": "data:text/plain;base64,dGVzdDY=",
    "size": 5,
    "type": "text/plain",
    "data": {},
    "originalName": "test.txt",
}

FORM_INPUT_FILE_DATA_URL: Final = {
    "storage": "url",
    "name": "test - kopie-65184d28-60e9-49ef-985e-a83c91959d76.txt",
    "url": "https://exxellence.vcap.me/verzoeken/api/v1/requests/file/6e0a731a-1f66-4cbf-bf4c-c4b1c64566bd/test-kopie.txt",  # noqa: E501
    "size": 5,
    "type": "text/plain",
    "data": {
        "url": "https://exxellence.vcap.me/verzoeken/api/v1/requests/file/6e0a731a-1f66-4cbf-bf4c-c4b1c64566bd/test2%20-%20kopie.txt",  # noqa: E501
        "name": "test - kopie.txt",
        "size": "1000",
        "baseUrl": "https://api.form.io",
        "project": "",
        "form": "",
    },
    "originalName": "test - kopie.txt",
}


# ---------------------------------------------
# Validate attachment with an invalid file input type.
# Type should be a list.
# ---------------------------------------------
@pytest.mark.parametrize(
    "attachment_input",
    [
        "",
        {},
        False,
        True,
        1234567890,
        12.22,
        (),
    ],
)
def test_attachment_type_input_not_valid(attachment_input: Any):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {},
        {"required": False},
    ]:
        component_data["validate"] = validate_data

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = attachment_input
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        assert len(validation_service.errors.get_errors()) == 1
        tv._assert_field_attachment_input_value_not_valid(
            validation_service.errors.get_errors()[0],
            component_data["key"],
            form_input_dict[component_data["key"]],
        )


# ---------------------------------------------
# Validate attachment with an invalid input value
# input value is not dict
# ---------------------------------------------
@pytest.mark.parametrize(
    "attachment_input",
    [
        "",
        False,
        True,
        1234567890,
        12.22,
        None,
        (),
        [],
    ],
)
def test_attachment_type_input_value_not_dict(attachment_input: Any):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {},
        {"required": False},
    ]:
        component_data["validate"] = validate_data

        file_data: list = []
        file_data.append(attachment_input)

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = file_data
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        assert len(validation_service.errors.get_errors()) == 1
        tv._assert_field_attachment_input_value_not_dict(
            validation_service.errors.get_errors()[0],
            component_data["key"],
            form_input_dict[component_data["key"]][0],
        )


# ---------------------------------------------
# Validate attachment with an empty file list.
# ---------------------------------------------
@pytest.mark.parametrize(
    "attachment_input",
    [
        [],
    ],
)
def test_attachment_type_input_empty_list(attachment_input: Any):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {},
        {"required": False},
    ]:
        component_data["validate"] = validate_data

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = attachment_input
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate attachment with invalid mime types
# ---------------------------------------------
@pytest.mark.parametrize(
    "mime_type",
    [
        "wrong_type",
        "text/text",
        "",
    ],
)
def test_attachment_type_invalid_mime_types(mime_type: Any):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {},
        {"required": False},
    ]:
        component_data["validate"] = validate_data

        file_data: dict[str, Any] = {}
        file_data.update(FORM_INPUT_FILE_DATA_BASE64)
        file_data["type"] = mime_type

        file_list_data: list = []
        file_list_data.append(file_data)

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = file_list_data
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        assert len(validation_service.errors.get_errors()) == 2
        tv._assert_field_attachment_unsupported_mime_type(
            validation_service.errors.get_errors()[0],
            form_input_dict[component_data["key"]][0]["type"],
        )
        tv._assert_field_file_not_valid(
            validation_service.errors.get_errors()[1],
            component_data["key"],
            form_input_dict[component_data["key"]][0],
        )


# ---------------------------------------------
# Validate attachment with invalid mime types
# ---------------------------------------------
@pytest.mark.parametrize(
    "mime_type",
    [
        False,
        True,
        1234567890,
        12.22,
        None,
        (),
        [],
    ],
)
def test_attachment_type_invalid_mime_types_2(mime_type: Any):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {},
        {"required": False},
    ]:
        component_data["validate"] = validate_data

        file_data: dict[str, Any] = {}
        file_data.update(FORM_INPUT_FILE_DATA_BASE64)
        file_data["type"] = mime_type

        file_list_data: list = []
        file_list_data.append(file_data)

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = file_list_data
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        assert len(validation_service.errors.get_errors()) == 2
        tv._assert_field_text_not_valid(
            validation_service.errors.get_errors()[0],
            f'{component_data["key"]}: type',
            form_input_dict[component_data["key"]][0]["type"],
        )
        tv._assert_field_file_not_valid(
            validation_service.errors.get_errors()[1],
            component_data["key"],
            form_input_dict[component_data["key"]][0],
        )


# ---------------------------------------------
# Validate attachment with missing mime type
# ---------------------------------------------
def test_attachment_type_mime_type_not_set():
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {},
        {"required": False},
    ]:
        component_data["validate"] = validate_data

        file_data: dict[str, Any] = {}
        file_data.update(FORM_INPUT_FILE_DATA_BASE64)
        file_data.pop("type")

        file_list_data: list = []
        file_list_data.append(file_data)

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = file_list_data
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        assert len(validation_service.errors.get_errors()) == 1
        tv._assert_field_file_key_not_exists(
            validation_service.errors.get_errors()[0],
            component_data["key"],
            form_input_dict[component_data["key"]][0],
            KEYS,
        )


# ---------------------------------------------
# Validate attachment
# ---------------------------------------------
@pytest.mark.parametrize(
    "key",
    [
        "storage",
        "name",
        "url",
        "size",
        "type",
        "originalName",
    ],
)
def test_attachment_base64_keys(key: str):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {},
        {"required": False},
    ]:
        component_data["validate"] = validate_data

        file_data: dict[str, Any] = {}
        file_data.update(FORM_INPUT_FILE_DATA_BASE64)
        file_data.pop(key)

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = [file_data]
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        assert len(validation_service.errors.get_errors()) == 1
        tv._assert_field_file_key_not_exists(
            validation_service.errors.get_errors()[0],
            component_data["key"],
            form_input_dict[component_data["key"]][0],
            KEYS,
        )


# ---------------------------------------------
# Validate attachment with attachment type base64
# ---------------------------------------------
@pytest.mark.parametrize(
    "url_input_value, expect_error, nr_of_errors",
    [
        ("data:text/plain;base64,dGVzdDY=", False, 0),
        ("data2:text/plain;base64,dGV", True, 2),
    ],
)
def test_attachment_type_base64(url_input_value: str, expect_error: bool, nr_of_errors: int):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {},
        {"required": False},
    ]:
        component_data["validate"] = validate_data

        file_data: dict[str, Any] = {}
        file_data.update(FORM_INPUT_FILE_DATA_BASE64)
        file_data["url"] = url_input_value

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = [file_data]
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        if expect_error:
            assert len(validation_service.errors.get_errors()) == nr_of_errors
            tv._assert_field_attachment_type_base64_not_valid(
                validation_service.errors.get_errors()[0],
                f'{component_data["key"]}: url',
                form_input_dict[component_data["key"]][0]["url"],
            )
            tv._assert_field_file_not_valid(
                validation_service.errors.get_errors()[1],
                component_data["key"],
                form_input_dict[component_data["key"]][0],
            )
        else:
            assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate attachment with attachment type url
# ---------------------------------------------
@pytest.mark.parametrize(
    "url_input_value",
    [
        "https://exxellence.vcap.me/verzoeken/api/v1/requests/file/6e0a731a-1f66-4cbf-bf4c-c4b1c64566bd/test-kopie.txt",  # noqa: E501
    ],
)
def test_attachment_type_url(url_input_value: str):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {},
        {"required": False},
    ]:
        component_data["validate"] = validate_data

        file_data: dict[str, Any] = {}
        file_data.update(FORM_INPUT_FILE_DATA_URL)
        file_data["url"] = url_input_value

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = [file_data]
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate attachment with attachment type url
# file field not valid
# ---------------------------------------------
@pytest.mark.parametrize(
    "url_input_value",
    [
        "https://exxellence.vcap.me/verzoeken/api/v1/requests/file/6e0a731a-1f66-4cbf-bf4c-c4b1c64566bd/test-kopie.txt",  # noqa: E501
    ],
)
def test_attachment_type_url_size_not_valid(url_input_value: str):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {},
        {"required": False},
    ]:
        component_data["validate"] = validate_data

        file_data: dict[str, Any] = {}
        file_data.update(FORM_INPUT_FILE_DATA_URL)
        file_data["url"] = url_input_value
        file_data["size"] = "aa"

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = [file_data]
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        assert len(validation_service.errors.get_errors()) == 2
        tv._assert_field_file_not_valid(
            validation_service.errors.get_errors()[1],
            component_data["key"],
            form_input_dict[component_data["key"]][0],
        )


# ---------------------------------------------
# Validate attachment with an invalid file storage type
# ---------------------------------------------
@pytest.mark.parametrize(
    "storage_type, expect_error",
    [
        ("base64", False),
        ("s3", True),
        ("size", True),
        ("type", True),
        ("originalName", True),
    ],
)
def test_attachment_file_storage_type_invalid(storage_type: str, expect_error: bool):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {},
        {"required": False},
    ]:
        component_data["validate"] = validate_data

        file_data: dict[str, Any] = {}
        file_data.update(FORM_INPUT_FILE_DATA_BASE64)
        file_data["storage"] = storage_type
        file_data["url"] = "data:text/plain;base64,dGVzdDY="

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = [file_data]
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        if expect_error:
            assert len(validation_service.errors.get_errors()) == 2
            tv._assert_field_file_type_not_valid(
                validation_service.errors.get_errors()[0],
                component_data["key"],
                file_data["storage"],
            )
            tv._assert_field_file_not_valid(
                validation_service.errors.get_errors()[1],
                component_data["key"],
                form_input_dict[component_data["key"]][0],
            )
        else:
            assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate attachment with an invalid file storage type
# ---------------------------------------------
@pytest.mark.parametrize(
    "storage_type, expect_error",
    [
        ("url", False),
        ("s3", True),
        ("size", True),
        ("type", True),
        ("originalName", True),
    ],
)
def test_attachment_file_storage_type_invalid2(storage_type: str, expect_error: bool):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {},
        {"required": False},
    ]:
        component_data["validate"] = validate_data

        file_data: dict[str, Any] = {}
        file_data.update(FORM_INPUT_FILE_DATA_URL)
        file_data["storage"] = storage_type
        file_data["url"] = "https://example.com/test.txt"

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = [file_data]
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        if expect_error:
            assert len(validation_service.errors.get_errors()) == 2
            tv._assert_field_file_type_not_valid(
                validation_service.errors.get_errors()[0],
                component_data["key"],
                file_data["storage"],
            )
            tv._assert_field_file_not_valid(
                validation_service.errors.get_errors()[1],
                component_data["key"],
                form_input_dict[component_data["key"]][0],
            )
        else:
            assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate attachment with an invalid input value
# storage type is missing
# ---------------------------------------------
@pytest.mark.parametrize(
    "url_input_value",
    [
        "https://exxellence.vcap.me/verzoeken/api/v1/requests/file/6e0a731a-1f66-4cbf-bf4c-c4b1c64566bd/test-kopie.txt",  # noqa: E501
        "https://exxellence.vcap.me/test.txt",
    ],
)
def test_attachment_type_invalid_input_value(url_input_value: str):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {},
        {"required": False},
    ]:
        component_data["validate"] = validate_data

        file_data: dict[str, Any] = {}
        file_data.update(FORM_INPUT_FILE_DATA_URL)
        file_data.pop("storage")
        file_data["url"] = url_input_value

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = [file_data]
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        assert len(validation_service.errors.get_errors()) == 1
        tv._assert_field_file_key_not_exists(
            validation_service.errors.get_errors()[0],
            component_data["key"],
            form_input_dict[component_data["key"]][0],
            KEYS,
        )


# ---------------------------------------------
# Test validation for mandatory
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error, nr_of_errors",
    [
        (FORM_INPUT_FILE_DATA_BASE64, False, 0),
        (FORM_INPUT_FILE_DATA_URL, False, 0),
        ([], True, 1),
    ],
)
def test_validate_mandatory(input_value: Any, expect_error: bool, nr_of_errors: int):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    component_data["validate"] = {"required": True}

    file_list_data: list = []
    if isinstance(input_value, dict):
        file_list_data.append(input_value)

    form_input_value: dict[str, Any] = {}
    form_input_value[component_data["key"]] = file_list_data

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input_value
    validation_service.validate_value(component_data)
    if expect_error:
        assert len(validation_service.errors.get_errors()) == nr_of_errors
        tv._assert_field_is_mandatory(
            validation_service.errors.get_errors()[0],
            component_data["key"],
        )
    else:
        assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Test validation for optional
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value",
    [
        FORM_INPUT_FILE_DATA_BASE64,
        FORM_INPUT_FILE_DATA_URL,
        [],
    ],
)
def test_validate_optional(input_value: Any):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [{}, {"required": False}]:
        component_data["validate"] = validate_data

        file_list_data: list = []
        if isinstance(input_value, dict):
            file_list_data.append(input_value)

        form_input_value: dict[str, Any] = {}
        form_input_value[component_data["key"]] = file_list_data

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_value
        validation_service.validate_value(component_data)
        assert len(validation_service.errors.get_errors()) == 0
