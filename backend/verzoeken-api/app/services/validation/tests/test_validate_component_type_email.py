import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation.validation_service import ValidationService
from typing import Any, Final

COMPONENT_DATA: Final = {
    "key": "email address",
    "type": "email",
}


# ---------------------------------------------
# Validate an optional inputfield with type email
# Test validation when value is an empty string.
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value",
    [
        "",
    ],
)
def test_validate_email_empty(input_value: str):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {},
        {"required": False},
    ]:
        component_data["validate"] = validate_data

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = input_value
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        assert len(validation_service.errors.get_errors()) == 1
        tv._assert_field_email_not_valid(
            validation_service.errors.get_errors()[0],
            component_data["key"],
            form_input_dict[component_data["key"]],
        )


# ---------------------------------------------
# Validate an optional inputfield with type email
# Test validation when value is None
# ---------------------------------------------
def test_validate_email_optional_none():
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {},
        {"required": False},
    ]:
        component_data["validate"] = validate_data

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = None
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate an optional inputfield with type email
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error, nr_of_errors",
    [
        ("test@example.com", False, 0),
        (0, True, 1),
    ],
)
def test_validate_optional(input_value: Any, expect_error: bool, nr_of_errors: int):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {},
        {"required": False},
    ]:
        component_data["validate"] = validate_data

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = input_value
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        if expect_error:
            assert len(validation_service.errors.get_errors()) == nr_of_errors
            tv._assert_field_text_not_valid(
                validation_service.errors.get_errors()[0],
                component_data["key"],
                form_input_dict[component_data["key"]],
            )
        else:
            assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate a mandatory inputfield with type email
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error, nr_of_errors",
    [
        ("test@example.com", False, 0),
        ("", True, 2),
    ],
)
def test_validate_mandatory(input_value: Any, expect_error: bool, nr_of_errors: int):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    component_data["validate"] = {"required": True}

    form_input_dict: dict[str, Any] = {}
    form_input_dict[component_data["key"]] = input_value
    component_data.update(form_input_dict)

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input_dict
    validation_service.validate_value(component_data)
    if expect_error:
        assert len(validation_service.errors.get_errors()) == nr_of_errors
        tv._assert_field_email_not_valid(
            validation_service.errors.get_errors()[0],
            component_data["key"],
            form_input_dict[component_data["key"]],
        )
        tv._assert_field_is_mandatory(
            validation_service.errors.get_errors()[1],
            component_data["key"],
        )
    else:
        assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate the minimum length for inputfield with type email
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error, nr_of_errors",
    [
        ("tester@example.com", False, 0),
        ("test@example.com", True, 1),
    ],
)
def test_validate_min_length(input_value: str, expect_error: bool, nr_of_errors: int):
    tv._test_validate_min_length(COMPONENT_DATA, input_value, 18, expect_error, nr_of_errors)


# ---------------------------------------------
# Validate the maximum length for inputfield with type email
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error, nr_of_errors",
    [
        ("test@example.com", False, 0),
        ("tester@example.com", True, 1),
    ],
)
def test_validate_max_length(input_value: str, expect_error: bool, nr_of_errors: int):
    tv._test_validate_max_length(COMPONENT_DATA, input_value, 16, expect_error, nr_of_errors)


# ---------------------------------------------
# Validate the pattern for a inputfield with type email
# ---------------------------------------------
@pytest.mark.parametrize(
    "validate_data, input_value, expect_error, nr_of_errors",
    [
        ({"pattern": ".*"}, "test@example.com", False, 0),
        ({"pattern": "^[\\w]+\\@[\\w]+\\.nl$"}, "test@example.nl", False, 0),
        ({"pattern": "^[\\w]+\\@[\\w]+\\.nl$"}, "test@example.com", True, 1),
    ],
)
def test_validate_pattern(
    validate_data: Any, input_value: str, expect_error: bool, nr_of_errors: int
):
    tv._test_validate_pattern(
        COMPONENT_DATA, validate_data, input_value, expect_error, nr_of_errors
    )


# ---------------------------------------------
# Validate the pattern for inputfield with type email
# ---------------------------------------------
@pytest.mark.parametrize(
    "validate_data, input_value",
    [
        ({"pattern": ".*"}, 10),
        ({"pattern": ".*"}, {}),
        ({"pattern": ".*"}, True),
    ],
)
def test_validate_pattern_not_string(validate_data: dict[str, str], input_value: Any):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    component_data["validate"] = validate_data

    form_input_dict: dict[str, Any] = {}
    form_input_dict[component_data["key"]] = input_value
    component_data.update(form_input_dict)

    tv._test_validate_pattern_not_string(component_data, form_input_dict)
