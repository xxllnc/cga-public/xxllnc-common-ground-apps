import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation.validation_service import ValidationService
from typing import Any, Final

COMPONENT_DATA: Final = {
    "key": "dateTime",
    "type": "datetime",
}


# ---------------------------------------------
# Validate an optional datetime
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error, nr_of_errors",
    [
        ("2022-02-02", False, 0),
        ("2022-02-02 12:00:00", False, 0),
        ("2022-02-02 23:59:59", False, 0),
        ("2022-02-02T12:00:00+01:00", False, 0),
        ("2022-02-02  12:00:00", True, 1),
        ("02-02-2022", True, 1),
        ("02022022", True, 1),
        ("20220202", False, 0),
        ("12:00:00", True, 1),
        ("", True, 1),
        ("Test", True, 1),
        (None, False, 0),
    ],
)
def test_validate_optional(input_value: Any, expect_error: bool, nr_of_errors: int):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {},
        {"required": False},
    ]:
        component_data["validate"] = validate_data

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = input_value
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        if expect_error:
            assert len(validation_service.errors.get_errors()) == nr_of_errors
            tv._assert_field_datetime_not_valid(
                validation_service.errors.get_errors()[0],
                component_data["key"],
                form_input_dict[component_data["key"]],
            )
        else:
            assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate a mandatory datetime
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error, nr_of_errors",
    [
        ("2022-02-02", False, 0),
        ("2022-02-02 12:00:00", False, 0),
        ("2022-02-02 23:59:59", False, 0),
        ("2022-02-02T12:00:00+01:00", False, 0),
        ("2022-02-02  12:00:00", True, 1),
        ("02-02-2022", True, 1),
        ("02022022", True, 1),
        ("20220202", False, 0),
        ("12:00:00", True, 1),
        ("", True, 2),
    ],
)
def test_validate_mandatory_and(input_value: Any, expect_error: bool, nr_of_errors: int):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    component_data["validate"] = {"required": True}

    form_input_dict: dict[str, Any] = {}
    form_input_dict[component_data["key"]] = input_value
    component_data.update(form_input_dict)

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input_dict
    validation_service.validate_value(component_data)
    if expect_error:
        assert len(validation_service.errors.get_errors()) == nr_of_errors
        tv._assert_field_datetime_not_valid(
            validation_service.errors.get_errors()[0],
            component_data["key"],
            form_input_dict[component_data["key"]],
        )
        if nr_of_errors > 1:
            tv._assert_field_is_mandatory(
                validation_service.errors.get_errors()[1],
                component_data["key"],
            )
    else:
        assert len(validation_service.errors.get_errors()) == 0
