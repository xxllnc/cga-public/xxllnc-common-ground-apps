import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation.validation_service import ValidationService
from typing import Any, Final

COMPONENT_DATA: Final = {
    "key": "phonenumber1",
    "validate": {},
    "multiple": False,  # not implemented
    "type": "phoneNumber",
}


# ---------------------------------------------
# Validate an optional phoneNumber
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error, nr_of_errors",
    [
        ("1234567890", False, 0),
        ("", True, 1),
        (0, True, 1),
        (None, False, 0),
    ],
)
def test_validate_optional(input_value: Any, expect_error: bool, nr_of_errors: int):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {},
        {"required": False},
    ]:
        component_data["validate"] = validate_data

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = input_value
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        if expect_error:
            assert len(validation_service.errors.get_errors()) == nr_of_errors
            tv._assert_field_phone_number_not_valid(
                validation_service.errors.get_errors()[0],
                component_data["key"],
                form_input_dict[component_data["key"]],
            )
        else:
            assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate a mandatory phoneNumber
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error, nr_of_errors",
    [
        ("1234567890", False, 0),
        ("", True, 2),
        (0, True, 1),
    ],
)
def test_validate_mandatory(input_value: Any, expect_error: bool, nr_of_errors: int):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {"required": True},
    ]:
        component_data["validate"] = validate_data

        form_input_dict: dict[str, Any] = {}
        form_input_dict[component_data["key"]] = input_value
        component_data.update(form_input_dict)

        validation_service = ValidationService()
        validation_service.errors = validation_service.Errors()
        validation_service.form_input_dict = form_input_dict
        validation_service.validate_value(component_data)
        if expect_error:
            assert len(validation_service.errors.get_errors()) == nr_of_errors
            tv._assert_field_phone_number_not_valid(
                validation_service.errors.get_errors()[0],
                component_data["key"],
                form_input_dict[component_data["key"]],
            )
            if nr_of_errors > 1:
                tv._assert_field_is_mandatory(
                    validation_service.errors.get_errors()[1],
                    component_data["key"],
                )
        else:
            assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate the minimum length for a phoneNumber
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error, nr_of_errors",
    [
        ("12345", False, 0),
        ("123456", False, 0),
        ("1234", True, 1),
    ],
)
def test_validate_min_length(input_value: str, expect_error: bool, nr_of_errors: int):
    tv._test_validate_min_length(COMPONENT_DATA, input_value, 5, expect_error, nr_of_errors)


# ---------------------------------------------
# Validate the maximum length for a phoneNumber
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error, nr_of_errors",
    [
        ("12345", False, 0),
        ("123456", True, 1),
        ("1234", False, 0),
    ],
)
def test_validate_max_length(input_value: str, expect_error: bool, nr_of_errors: int):
    tv._test_validate_max_length(COMPONENT_DATA, input_value, 5, expect_error, nr_of_errors)


# ---------------------------------------------
# Validate the pattern for a phoneNumber
# ---------------------------------------------
@pytest.mark.parametrize(
    "validate_data, input_value, expect_error, nr_of_errors",
    [
        ({"pattern": ".*"}, "12345", False, 0),
        ({"pattern": "[abc]"}, "12345", True, 1),
        (
            {"pattern": "(\\+)?[\\-\\s0-9]{10,20}"},
            "12345678910",
            False,
            0,
        ),  # telefoonnummer (minimaal 10 tekens)  # noqa: E501
    ],
)
def test_validate_pattern(
    validate_data: Any, input_value: str, expect_error: bool, nr_of_errors: int
):
    tv._test_validate_pattern(
        COMPONENT_DATA, validate_data, input_value, expect_error, nr_of_errors
    )


# ---------------------------------------------
# Validate the pattern for a phoneNumber
# ---------------------------------------------
@pytest.mark.parametrize(
    "validate_data, input_value",
    [
        ({"pattern": ".*"}, 10),
        ({"pattern": "[abc][012]"}, {}),
        ({"pattern": "[abc]"}, True),
    ],
)
def test_validate_pattern_not_string(validate_data: dict[str, str], input_value: Any):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    component_data["validate"] = validate_data

    form_input_dict: dict[str, Any] = {}
    form_input_dict[component_data["key"]] = input_value
    component_data.update(form_input_dict)

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input_dict
    validation_service.validate_value(component_data)
    assert len(validation_service.errors.get_errors()) == 2
    tv._assert_field_phone_number_not_valid(
        validation_service.errors.get_errors()[0],
        component_data["key"],
        form_input_dict[component_data["key"]],
    )
    tv._assert_field_pattern_not_string(
        validation_service.errors.get_errors()[1],
        component_data["key"],
        form_input_dict[component_data["key"]],
        component_data["validate"]["pattern"],
    )
