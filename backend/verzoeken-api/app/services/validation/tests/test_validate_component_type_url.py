import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation.validation_service import ValidationService
from typing import Any, Final

COMPONENT_DATA: Final = {
    "key": "url",
    "type": "url",
}


# ---------------------------------------------
# Validate an optional inputfield with type url
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error",
    [
        ({}, True),
        ("", True),
        ("http://www.xxllnc.nl", False),
        ("notvalid", True),
        (0, True),
        (None, False),
    ],
)
def test_validate_optional(input_value: Any, expect_error: bool):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    component_data["validate"] = {"required": False}

    form_input_dict: dict[str, Any] = {}
    form_input_dict[component_data["key"]] = input_value
    component_data.update(form_input_dict)

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input_dict
    validation_service.validate_value(component_data)
    if expect_error:
        tv._assert_field_url_not_valid(
            validation_service.errors.get_errors()[0],
            component_data["key"],
            form_input_dict[component_data["key"]],
        )
    else:
        assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate a mandatory inputfield with type url
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error",
    [("", True), ("http://www.xxllnc.nl", False)],
)
def test_validate_mandatory(input_value: Any, expect_error: bool):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    component_data["validate"] = {"required": True}

    form_input_dict: dict[str, Any] = {}
    form_input_dict[component_data["key"]] = input_value
    component_data.update(form_input_dict)

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input_dict
    validation_service.validate_value(component_data)
    if expect_error:
        tv._assert_field_url_not_valid(
            validation_service.errors.get_errors()[0],
            component_data["key"],
            form_input_dict[component_data["key"]],
        )
    else:
        assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate the minimum length for inputfield with type url
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error, nr_of_errors",
    [
        ("http://www.nu.nl", True, 1),
        ("http://www.xxllnc.nl", False, 0),
        ("http://www.exxellence.nl", False, 0),
    ],
)
def test_validate_min_length(input_value: str, expect_error: bool, nr_of_errors: int):
    tv._test_validate_min_length(COMPONENT_DATA, input_value, 20, expect_error, nr_of_errors)


# ---------------------------------------------
# Validate the maximum length for inputfield with type url
# ---------------------------------------------
@pytest.mark.parametrize(
    "input_value, expect_error, nr_of_errors",
    [
        ("http://www.xxllnc.nl", False, 0),
        ("http://www.exxellence.nl", True, 1),
    ],
)
def test_validate_max_length(input_value: str, expect_error: bool, nr_of_errors: int):
    tv._test_validate_max_length(COMPONENT_DATA, input_value, 22, expect_error, nr_of_errors)


# ---------------------------------------------
# Validate the pattern for inputfield with type url
# ---------------------------------------------
@pytest.mark.parametrize(
    "validate_data, input_value, expect_error, nr_of_errors",
    [
        ({"pattern": ".*"}, "http://www.xxllnc.nl", False, 0),
        ({"pattern": "[abc]"}, "http://www.xxllnc.nl", True, 1),
    ],
)
def test_validate_pattern(
    validate_data: Any, input_value: str, expect_error: bool, nr_of_errors: int
):
    tv._test_validate_pattern(
        COMPONENT_DATA, validate_data, input_value, expect_error, nr_of_errors
    )


# ---------------------------------------------
# Validate the pattern for inputfield with type url
# ---------------------------------------------
@pytest.mark.parametrize(
    "validate_data, input_data",
    [
        ({"pattern": ".*"}, 10),
        ({"pattern": "[abc][012]"}, {}),
        ({"pattern": "[abc]"}, True),
    ],
)
def test_validate_pattern_not_string(validate_data: dict[str, str], input_data: Any):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)
    component_data["validate"] = validate_data

    form_input_dict: dict[str, Any] = {}
    form_input_dict[component_data["key"]] = input_data
    component_data.update(form_input_dict)

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input_dict
    validation_service.validate_value(component_data)
    assert len(validation_service.errors.get_errors()) == 2
    tv._assert_field_url_not_valid(
        validation_service.errors.get_errors()[0],
        component_data["key"],
        form_input_dict[component_data["key"]],
    )
    tv._assert_field_pattern_not_string(
        validation_service.errors.get_errors()[1],
        component_data["key"],
        form_input_dict[component_data["key"]],
        component_data["validate"]["pattern"],
    )
