import app.services.validation.tests.test_validate as tv
import json
import pytest
from app.api.api_v1.routers.tests.conftest import API_RETURN_VALUE
from app.services.validation.validation_service import ValidationService
from requests.models import Response
from typing import Any, Union
from unittest.mock import Mock


# ---------------------------------------------
# Test the get form properties.
# ---------------------------------------------
def test_get_form_properties_mock():
    """Test the get form properties."""

    response = Mock(spec=Response)
    response.json.return_value = API_RETURN_VALUE
    response.status_code = 200
    assert response is not None

    response_value = response.json()
    assert response_value is not None
    assert isinstance(response_value, dict)

    form = response_value.get("form")
    assert form is not None
    assert isinstance(form, dict)

    form_components: list = form["components"]
    assert form_components is not None
    assert isinstance(form_components, list)


# ---------------------------------------------
# Test required input fields.
# ---------------------------------------------
@pytest.mark.parametrize(
    "field_value",
    [
        "test",
        "_",
        " __  ",
        None,
    ],
)
def test_validate_required_with_value(
    field_value: Union[None, str],
):
    """Test required input fields."""

    # This contains a valid (formio) form, with three input fields.
    # Inputfield name is mandatory.
    form_data: dict[str, Any] = {
        "form": '{"display":"wizard","title":"formtitel","name":"formname","type":["form"],"path":"formpath","components":[{"title":"Page 1","label":"Page 1","type":"panel","key":"page1","components":[{"label":"Vraag","labelPosition":"top","placeholder":"","description":"","tooltip":"","prefix":"","suffix":"","widget":{"type":"input"},"inputMask":"","allowMultipleMasks":false,"customClass":"","tabindex":"","autocomplete":"","hidden":false,"hideLabel":false,"showWordCount":false,"showCharCount":false,"mask":false,"autofocus":false,"spellcheck":true,"disabled":false,"tableView":true,"modalEdit":false,"multiple":false,"persistent":true,"inputFormat":"plain","protected":false,"dbIndex":false,"case":"","encrypted":false,"redrawOn":"","clearOnHide":true,"customDefaultValue":"","calculateValue":"","calculateServer":false,"allowCalculateOverride":false,"validateOn":"change","validate":{"required":false,"pattern":"","customMessage":"","custom":"","customPrivate":false,"json":"","minLength":"","maxLength":"","strictDateValidation":false,"multiple":false,"unique":false},"unique":false,"errorLabel":"","key":"tEst","tags":[],"properties":{},"conditional":{"show":null,"when":null,"eq":"","json":""},"customConditional":"","logic":[],"attributes":{},"overlay":{"style":"","page":"","left":"","top":"","width":"","height":""},"type":"textfield","input":true,"refreshOn":"","dataGridLabel":false,"inputType":"text","id":"emvmmhk","defaultValue":""},{"label":"Voornaam","labelPosition":"top","placeholder":"","description":"","tooltip":"","prefix":"","suffix":"","widget":{"type":"input"},"inputMask":"","allowMultipleMasks":false,"customClass":"","tabindex":"","autocomplete":"","hidden":false,"hideLabel":false,"showWordCount":false,"showCharCount":false,"mask":false,"autofocus":false,"spellcheck":true,"disabled":false,"tableView":true,"modalEdit":false,"multiple":false,"persistent":true,"inputFormat":"plain","protected":false,"dbIndex":false,"case":"","encrypted":false,"redrawOn":"","clearOnHide":true,"customDefaultValue":"","calculateValue":"","calculateServer":false,"allowCalculateOverride":false,"validateOn":"change","validate":{"required":true,"pattern":"","customMessage":"","custom":"","customPrivate":false,"json":"","minLength":"","maxLength":"","strictDateValidation":false,"multiple":false,"unique":false},"unique":false,"errorLabel":"","key":"name","tags":[],"properties":{},"conditional":{"show":null,"when":null,"eq":"","json":""},"customConditional":"","logic":[],"attributes":{},"overlay":{"style":"","page":"","left":"","top":"","width":"","height":""},"type":"textfield","input":true,"refreshOn":"","dataGridLabel":false,"inputType":"text","id":"evzldlun","defaultValue":""},{"label":"Achternaam","tableView":true,"key":"lastName","type":"textfield","input":true,"placeholder":"","prefix":"","customClass":"","suffix":"","multiple":false,"defaultValue":null,"protected":false,"unique":false,"persistent":true,"hidden":false,"clearOnHide":true,"refreshOn":"","redrawOn":"","modalEdit":false,"labelPosition":"top","description":"","errorLabel":"","tooltip":"","hideLabel":false,"tabindex":"","disabled":false,"autofocus":false,"dbIndex":false,"customDefaultValue":"","calculateValue":"","calculateServer":false,"widget":{"type":"input"},"attributes":{},"validateOn":"change","validate":{"required":false,"custom":"","customPrivate":false,"strictDateValidation":false,"multiple":false,"unique":false,"minLength":"","maxLength":"","pattern":""},"conditional":{"show":null,"when":null,"eq":""},"overlay":{"style":"","left":"","top":"","width":"","height":""},"allowCalculateOverride":false,"encrypted":false,"showCharCount":false,"showWordCount":false,"properties":{},"allowMultipleMasks":false,"mask":false,"inputType":"text","inputFormat":"plain","inputMask":"","spellcheck":true,"id":"een8nx9","dataGridLabel":false}],"input":false,"placeholder":"","prefix":"","customClass":"","suffix":"","multiple":false,"defaultValue":null,"protected":false,"unique":false,"persistent":false,"hidden":false,"clearOnHide":false,"refreshOn":"","redrawOn":"","tableView":false,"modalEdit":false,"labelPosition":"top","description":"","errorLabel":"","tooltip":"","hideLabel":false,"tabindex":"","disabled":false,"autofocus":false,"dbIndex":false,"customDefaultValue":"","calculateValue":"","calculateServer":false,"widget":null,"attributes":{},"validateOn":"change","validate":{"required":false,"custom":"","customPrivate":false,"strictDateValidation":false,"multiple":false,"unique":false},"conditional":{"show":null,"when":null,"eq":""},"overlay":{"style":"","left":"","top":"","width":"","height":""},"allowCalculateOverride":false,"encrypted":false,"showCharCount":false,"showWordCount":false,"properties":{},"allowMultipleMasks":false,"tree":false,"theme":"default","breadcrumb":"default","id":"eypah2g","dataGridLabel":false},{"title":"Page 2","label":"Page 2","type":"panel","key":"page2","components":[{"label":"Wat is uw vraag","labelPosition":"top","placeholder":"","description":"","tooltip":"","prefix":"","suffix":"","widget":{"type":"input"},"editor":"","autoExpand":false,"customClass":"","tabindex":"","autocomplete":"","hidden":false,"hideLabel":false,"showWordCount":false,"showCharCount":false,"autofocus":false,"spellcheck":true,"disabled":false,"tableView":true,"modalEdit":false,"multiple":false,"persistent":true,"inputFormat":"html","protected":false,"dbIndex":false,"case":"","encrypted":false,"redrawOn":"","clearOnHide":true,"customDefaultValue":"","calculateValue":"","calculateServer":false,"allowCalculateOverride":false,"validateOn":"change","validate":{"required":false,"pattern":"","customMessage":"","custom":"","customPrivate":false,"json":"","minLength":"","maxLength":"","minWords":"","maxWords":"","strictDateValidation":false,"multiple":false,"unique":false},"unique":false,"errorLabel":"","key":"watIsUwVraag","tags":[],"properties":{},"conditional":{"show":null,"when":null,"eq":"","json":""},"customConditional":"","logic":[],"attributes":{},"overlay":{"style":"","page":"","left":"","top":"","width":"","height":""},"type":"textarea","rows":3,"wysiwyg":false,"input":true,"refreshOn":"","dataGridLabel":false,"allowMultipleMasks":false,"mask":false,"inputType":"text","inputMask":"","fixedSize":true,"id":"exiwwsc","defaultValue":""}],"input":false,"placeholder":"","prefix":"","customClass":"","suffix":"","multiple":false,"defaultValue":null,"protected":false,"unique":false,"persistent":false,"hidden":false,"clearOnHide":false,"refreshOn":"","redrawOn":"","tableView":false,"modalEdit":false,"dataGridLabel":false,"labelPosition":"top","description":"","errorLabel":"","tooltip":"","hideLabel":false,"tabindex":"","disabled":false,"autofocus":false,"dbIndex":false,"customDefaultValue":"","calculateValue":"","calculateServer":false,"widget":null,"attributes":{},"validateOn":"change","validate":{"required":false,"custom":"","customPrivate":false,"strictDateValidation":false,"multiple":false,"unique":false},"conditional":{"show":null,"when":null,"eq":""},"overlay":{"style":"","left":"","top":"","width":"","height":""},"allowCalculateOverride":false,"encrypted":false,"showCharCount":false,"showWordCount":false,"properties":{},"allowMultipleMasks":false,"tree":false,"theme":"default","breadcrumb":"default","id":"ehcb46d"}]}',  # noqa: E501
    }

    # The user input
    form_input: dict[str, str] = {
        "tEst": "test",
        "name": f"{field_value}",  # required = true
        "lastName": "",  # required = false
        "watIsUwVraag": "",  # required = false
    }

    assert form_data is not None
    assert isinstance(form_data, dict)

    assert form_input is not None
    assert isinstance(form_input, dict)

    form = form_data.get("form", "")
    assert form is not None
    assert isinstance(form, str)

    form_components: list = json.loads(form)["components"]
    assert form_components is not None
    assert isinstance(form_components, list)

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input

    validation_service.validate_form_input_dict(form_components)
    assert len(validation_service.errors.get_errors()) == 0
    assert int(validation_service.errors.error_amount) == 0


# ---------------------------------------------
# Test required input fields.
# ---------------------------------------------
@pytest.mark.parametrize(
    "field_empty_value",
    [
        "",
        "      ",
    ],
)
def test_validate_required_without_value(
    field_empty_value: str,
):
    """Test required input fields."""

    # This contains a valid (formio) form, with three input fields.
    # Inputfield name and lastname are mandatory.
    form_data: dict[str, Any] = {
        "form": '{"display":"wizard","title":"formtitel","name":"formname","type":["form"],"path":"formpath","components":[{"title":"Page 1","label":"Page 1","type":"panel","key":"page1","components":[{"label":"Vraag","labelPosition":"top","placeholder":"","description":"","tooltip":"","prefix":"","suffix":"","widget":{"type":"input"},"inputMask":"","allowMultipleMasks":false,"customClass":"","tabindex":"","autocomplete":"","hidden":false,"hideLabel":false,"showWordCount":false,"showCharCount":false,"mask":false,"autofocus":false,"spellcheck":true,"disabled":false,"tableView":true,"modalEdit":false,"multiple":false,"persistent":true,"inputFormat":"plain","protected":false,"dbIndex":false,"case":"","encrypted":false,"redrawOn":"","clearOnHide":true,"customDefaultValue":"","calculateValue":"","calculateServer":false,"allowCalculateOverride":false,"validateOn":"change","validate":{"required":false,"pattern":"","customMessage":"","custom":"","customPrivate":false,"json":"","minLength":"","maxLength":"","strictDateValidation":false,"multiple":false,"unique":false},"unique":false,"errorLabel":"","key":"tEst","tags":[],"properties":{},"conditional":{"show":null,"when":null,"eq":"","json":""},"customConditional":"","logic":[],"attributes":{},"overlay":{"style":"","page":"","left":"","top":"","width":"","height":""},"type":"textfield","input":true,"refreshOn":"","dataGridLabel":false,"inputType":"text","id":"emvmmhk","defaultValue":""},{"label":"Voornaam","labelPosition":"top","placeholder":"","description":"","tooltip":"","prefix":"","suffix":"","widget":{"type":"input"},"inputMask":"","allowMultipleMasks":false,"customClass":"","tabindex":"","autocomplete":"","hidden":false,"hideLabel":false,"showWordCount":false,"showCharCount":false,"mask":false,"autofocus":false,"spellcheck":true,"disabled":false,"tableView":true,"modalEdit":false,"multiple":false,"persistent":true,"inputFormat":"plain","protected":false,"dbIndex":false,"case":"","encrypted":false,"redrawOn":"","clearOnHide":true,"customDefaultValue":"","calculateValue":"","calculateServer":false,"allowCalculateOverride":false,"validateOn":"change","validate":{"required":true,"pattern":"","customMessage":"","custom":"","customPrivate":false,"json":"","minLength":"","maxLength":"","strictDateValidation":false,"multiple":false,"unique":false},"unique":false,"errorLabel":"","key":"name","tags":[],"properties":{},"conditional":{"show":null,"when":null,"eq":"","json":""},"customConditional":"","logic":[],"attributes":{},"overlay":{"style":"","page":"","left":"","top":"","width":"","height":""},"type":"textfield","input":true,"refreshOn":"","dataGridLabel":false,"inputType":"text","id":"evzldlun","defaultValue":""},{"label":"Achternaam","tableView":true,"key":"lastName","type":"textfield","input":true,"placeholder":"","prefix":"","customClass":"","suffix":"","multiple":false,"defaultValue":null,"protected":false,"unique":false,"persistent":true,"hidden":false,"clearOnHide":true,"refreshOn":"","redrawOn":"","modalEdit":false,"labelPosition":"top","description":"","errorLabel":"","tooltip":"","hideLabel":false,"tabindex":"","disabled":false,"autofocus":false,"dbIndex":false,"customDefaultValue":"","calculateValue":"","calculateServer":false,"widget":{"type":"input"},"attributes":{},"validateOn":"change","validate":{"required":false,"custom":"","customPrivate":false,"strictDateValidation":false,"multiple":false,"unique":false,"minLength":"","maxLength":"","pattern":""},"conditional":{"show":null,"when":null,"eq":""},"overlay":{"style":"","left":"","top":"","width":"","height":""},"allowCalculateOverride":false,"encrypted":false,"showCharCount":false,"showWordCount":false,"properties":{},"allowMultipleMasks":false,"mask":false,"inputType":"text","inputFormat":"plain","inputMask":"","spellcheck":true,"id":"een8nx9","dataGridLabel":false}],"input":false,"placeholder":"","prefix":"","customClass":"","suffix":"","multiple":false,"defaultValue":null,"protected":false,"unique":false,"persistent":false,"hidden":false,"clearOnHide":false,"refreshOn":"","redrawOn":"","tableView":false,"modalEdit":false,"labelPosition":"top","description":"","errorLabel":"","tooltip":"","hideLabel":false,"tabindex":"","disabled":false,"autofocus":false,"dbIndex":false,"customDefaultValue":"","calculateValue":"","calculateServer":false,"widget":null,"attributes":{},"validateOn":"change","validate":{"required":false,"custom":"","customPrivate":false,"strictDateValidation":false,"multiple":false,"unique":false},"conditional":{"show":null,"when":null,"eq":""},"overlay":{"style":"","left":"","top":"","width":"","height":""},"allowCalculateOverride":false,"encrypted":false,"showCharCount":false,"showWordCount":false,"properties":{},"allowMultipleMasks":false,"tree":false,"theme":"default","breadcrumb":"default","id":"eypah2g","dataGridLabel":false},{"title":"Page 2","label":"Page 2","type":"panel","key":"page2","components":[{"label":"Wat is uw vraag","labelPosition":"top","placeholder":"","description":"","tooltip":"","prefix":"","suffix":"","widget":{"type":"input"},"editor":"","autoExpand":false,"customClass":"","tabindex":"","autocomplete":"","hidden":false,"hideLabel":false,"showWordCount":false,"showCharCount":false,"autofocus":false,"spellcheck":true,"disabled":false,"tableView":true,"modalEdit":false,"multiple":false,"persistent":true,"inputFormat":"html","protected":false,"dbIndex":false,"case":"","encrypted":false,"redrawOn":"","clearOnHide":true,"customDefaultValue":"","calculateValue":"","calculateServer":false,"allowCalculateOverride":false,"validateOn":"change","validate":{"required":false,"pattern":"","customMessage":"","custom":"","customPrivate":false,"json":"","minLength":"","maxLength":"","minWords":"","maxWords":"","strictDateValidation":false,"multiple":false,"unique":false},"unique":false,"errorLabel":"","key":"watIsUwVraag","tags":[],"properties":{},"conditional":{"show":null,"when":null,"eq":"","json":""},"customConditional":"","logic":[],"attributes":{},"overlay":{"style":"","page":"","left":"","top":"","width":"","height":""},"type":"textarea","rows":3,"wysiwyg":false,"input":true,"refreshOn":"","dataGridLabel":false,"allowMultipleMasks":false,"mask":false,"inputType":"text","inputMask":"","fixedSize":true,"id":"exiwwsc","defaultValue":""}],"input":false,"placeholder":"","prefix":"","customClass":"","suffix":"","multiple":false,"defaultValue":null,"protected":false,"unique":false,"persistent":false,"hidden":false,"clearOnHide":false,"refreshOn":"","redrawOn":"","tableView":false,"modalEdit":false,"dataGridLabel":false,"labelPosition":"top","description":"","errorLabel":"","tooltip":"","hideLabel":false,"tabindex":"","disabled":false,"autofocus":false,"dbIndex":false,"customDefaultValue":"","calculateValue":"","calculateServer":false,"widget":null,"attributes":{},"validateOn":"change","validate":{"required":false,"custom":"","customPrivate":false,"strictDateValidation":false,"multiple":false,"unique":false},"conditional":{"show":null,"when":null,"eq":""},"overlay":{"style":"","left":"","top":"","width":"","height":""},"allowCalculateOverride":false,"encrypted":false,"showCharCount":false,"showWordCount":false,"properties":{},"allowMultipleMasks":false,"tree":false,"theme":"default","breadcrumb":"default","id":"ehcb46d"}]}',  # noqa: E501
    }

    # The user input
    form_input: dict[str, str] = {
        "tEst": "test",
        "name": f"{field_empty_value}",
        "lastName": "",
        "watIsUwVraag": "",
    }

    assert form_data is not None
    assert form_input is not None
    form = form_data.get("form", "")
    form_components: list = json.loads(form)["components"]

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input

    validation_service.validate_form_input_dict(form_components)
    assert type(validation_service.errors) is ValidationService.Errors
    assert type(validation_service.errors.get_errors()) is list
    assert type(validation_service.errors.error_amount) is int

    assert len(validation_service.errors.get_errors()) == 1
    tv._assert_field_is_mandatory(validation_service.errors.get_errors()[0], "name")
    assert int(validation_service.errors.error_amount) == 1

    # TEst met meerdere unieke keys?


# ---------------------------------------------
# Test validate component type 'day'
# Test with invalid values
# ---------------------------------------------
@pytest.mark.parametrize(
    "day_value,validation_required",
    [
        ("", ""),
        ("   ", ""),
        ("00/00/0001", ""),
        ("00/01/0000", ""),
        ("00/01/0001", ""),
        ("01/00/0000", ""),
        ("01/00/0001", ""),
        ("01/01/0000", ""),
        ("32/01/0001", ""),
        ("01/13/0001", ""),
        ("29/02/2027", ""),  # Geen schrikkeljaar
        (
            "00/00/0000",
            True,
        ),  # TODO CGAT-106 expect 2 errors. missing required msg.
        (
            "00-00-0000",
            True,
        ),  # TODO CGAT-106 expect 2 errors. missing required msg.
    ],
)
def test_validate_day_invalid_values(
    day_value: str,
    validation_required: Union[bool, str],
):

    component_data: dict[str, Any] = {
        "key": "day",
        "validate": {"required": f"{validation_required}"},
        "type": "day",
    }

    form_input: dict[str, str] = {
        "day": day_value,
    }

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input
    validation_service.validate_value(component_data)
    assert len(validation_service.errors.get_errors()) == 1
    tv._assert_field_date_not_valid(
        validation_service.errors.get_errors()[0],
        component_data["key"],
        form_input["day"],
    )


# ---------------------------------------------
# Test validate component type 'day'
# Test with invalid values
# ---------------------------------------------
@pytest.mark.parametrize(
    "day_value,validation_required",
    [
        ("", True),
        ("", False),
    ],
)
def test_validate_day_invalid_values_2(
    day_value: str,
    validation_required: bool,
):

    component_data: dict[str, Any] = {
        "key": "day",
        "validate": {"required": f"{validation_required}"},
        "type": "day",
    }

    form_input: dict[str, str] = {
        "day": day_value,
    }

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input
    validation_service.validate_value(component_data)
    assert len(validation_service.errors.get_errors()) == 2
    tv._assert_field_date_not_valid(
        validation_service.errors.get_errors()[0],
        component_data["key"],
        form_input["day"],
    )
    tv._assert_field_is_mandatory(validation_service.errors.get_errors()[1], component_data["key"])


# ---------------------------------------------
# Test validate component type 'day'
# Test with valid values and with
# required validation
# ---------------------------------------------
@pytest.mark.parametrize(
    "day_value, validation_required",
    [
        ("00/00/0000", ""),
        ("01/01/0001", ""),
        ("28/02/2028", ""),
        ("29/02/2028", ""),  # Schrikkeljaar
        ("31/12/9999", ""),
        ("00-00-0000", ""),
        ("01-01-0001", ""),
        ("28-02-2028", ""),
        ("29-02-2028", ""),  # Schrikkeljaar
        ("31-12-9999", ""),
        ("00/00/0000", False),
        ("01/01/0001", False),
        ("28/02/2028", False),
        ("29/02/2028", False),  # Schrikkeljaar
        ("31/12/9999", False),
        ("00-00-0000", False),
        ("01-01-0001", False),
        ("28-02-2028", False),
        ("29-02-2028", False),  # Schrikkeljaar
        ("31-12-9999", False),
        ("01/01/0001", True),
        ("28/02/2028", True),
        ("29/02/2028", True),  # Schrikkeljaar
        ("31/12/9999", True),
        ("01-01-0001", True),
        ("28-02-2028", True),
        ("29-02-2028", True),  # Schrikkeljaar
        ("31-12-9999", True),
    ],
)
def test_validate_day_valid_values_with_required_validation(
    day_value: str,
    validation_required: Union[bool, str],
):

    component_data: dict[str, Any] = {
        "key": "day",
        "validate": {"required": validation_required},
        "type": "day",
    }

    form_input: dict[str, str] = {
        "day": day_value,
    }

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input
    validation_service.validate_value(component_data)
    assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Test validate component type 'day'
# Test with valid values and without validation
# ---------------------------------------------
@pytest.mark.parametrize(
    "day_value",
    [
        ("01/01/0001"),
        ("28/02/2028"),
        ("29/02/2028"),  # Schrikkeljaar
        ("31/12/9999"),
        ("01-01-0001"),
        ("28-02-2028"),
        ("29-02-2028"),  # Schrikkeljaar
        ("31-12-9999"),
    ],
)
def test_validate_day_without_validate(
    day_value: str,
):

    component_data: dict[str, Any] = {
        "key": "day",
        "validate": {},
        "type": "day",
    }

    form_input: dict[str, str] = {
        "day": day_value,
    }

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input
    validation_service.validate_value(component_data)
    assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Test validate component type 'time'
# Test with invalid values
# ---------------------------------------------
@pytest.mark.parametrize(
    "time_value",
    [
        "",
        "12",
        "12:50",
        "12:50:",
        "12:50:12:",
        "12:50:12:dd",
    ],
)
def test_validate_time_invalid_values(
    time_value: str,
):
    component_data: dict[str, Any] = {
        "key": "time",
        "validate": {},
        "type": "time",
    }

    form_input: dict[str, str] = {
        "time": time_value,
    }

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input
    validation_service.validate_value(component_data)
    assert len(validation_service.errors.get_errors()) == 1
    tv._assert_field_time_not_valid(
        str(validation_service.errors.get_errors()[0]),
        component_data["key"],
        form_input["time"],
    )


# ---------------------------------------------
# Test validate component type 'time'
# Test with valid values
# ---------------------------------------------
@pytest.mark.parametrize(
    "time_value",
    [
        "12:50:30",
    ],
)
def test_validate_time_valid_values(
    time_value: str,
):
    component_data: dict[str, Any] = {
        "key": "time",
        "validate": {},
        "type": "time",
    }

    form_input: dict[str, str] = {
        "time": time_value,
    }

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input
    validation_service.validate_value(component_data)
    assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Test validate component type 'datetime'
# Test with invalid values
# ---------------------------------------------
@pytest.mark.parametrize(
    "datetime_value",
    [
        "",
        "0000-00-00",
        "0000-00-01",
        "0000-01-00",
        "0000-01-01",
        "0001-00-01",
        "0001-00-00",
        "0001-01-00",
        "2021-13-01",
        "2021-02-29",  # Geen Schrikkeljaar
        "0000-01-02T",
        "10:22:00+00:53",
        "0000-01-02T10:22:00+00:53",
    ],
)
def test_validate_datetime_invalid_values(
    datetime_value: str,
):
    component_data: dict[str, Any] = {
        "key": "datetime",
        "validate": {},
        "type": "datetime",
    }

    form_input: dict[str, str] = {
        "datetime": datetime_value,
    }

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input
    validation_service.validate_value(component_data)
    assert len(validation_service.errors.get_errors()) == 1
    tv._assert_field_datetime_not_valid(
        str(validation_service.errors.get_errors()[0]),
        component_data["key"],
        form_input["datetime"],
    )


# ---------------------------------------------
# Test validate component type 'datetime'
# Test with valid values
# ---------------------------------------------
@pytest.mark.parametrize(
    "datetime_value",
    [
        "0001-01-01",
        "2021-10-12",
        "2028-02-29",  # Schrikkeljaar
        "9999-12-31",
        "2021-10-27T12:00:00+02:00",
        "2021-10-27T00:00:00+02:00",
    ],
)
def test_validate_datetime_valid_values(
    datetime_value: str,
):
    component_data: dict[str, Any] = {
        "key": "datetime",
        "validate": {},
        "type": "datetime",
    }

    form_input: dict[str, str] = {
        "datetime": datetime_value,
    }

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input
    validation_service.validate_value(component_data)
    assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate an optional inputfield without an
# form input value.
# ---------------------------------------------
@pytest.mark.parametrize(
    "component_data, form_input_data",
    [
        ({"key": "textfield", "validate": {}}, {}),
        ({"key": "textfield", "validate": {"required": False}}, {}),
    ],
)
def test_validate_optional_inputfield_without_form_input(
    component_data: dict[str, Any], form_input_data: dict[str, Any]
):

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input_data
    validation_service.validate_value(component_data)
    assert len(validation_service.errors.get_errors()) == 0


# ---------------------------------------------
# Validate a mandatory inputfield without an
# form input value.
# ---------------------------------------------
@pytest.mark.parametrize(
    "component_data, form_input_data",
    [
        ({"key": "textfield", "validate": {"required": True}}, {}),
    ],
)
def test_validate_mandatory_inputfield_without_form_input(
    component_data: dict[str, Any], form_input_data: dict[str, Any]
):

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input_data
    validation_service.validate_value(component_data)
    assert len(validation_service.errors.get_errors()) == 1
    tv._assert_field_is_mandatory(validation_service.errors.get_errors()[0], component_data["key"])


# ---------------------------------------------
# Test textfield for minLength and maxLength.
# Test with valid values.
# Ignore the value None as test value.
# ---------------------------------------------
@pytest.mark.parametrize(
    "field_value, error_amount, error_message",
    [
        (
            "",
            1,
            "textField is te kort, textField heeft een minimum lengte van 5.",
        ),
        (
            "1234",
            1,
            "textField is te kort, textField heeft een minimum lengte van 5.",
        ),
        ("12345", 0, ""),
        ("Dit is een", 0, ""),
        ("    Dit   ", 0, ""),
        ("          ", 0, ""),
        ("..'.'.'.'.", 0, ""),
        ('Dit "is" e', 0, ""),
        (
            "Dit is een1",
            1,
            "textField is te lang, textField heeft een maximum lengte van 10.",
        ),
        (
            "Dit is een 22222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222221",  # noqa: E501
            1,
            "textField is te lang, textField heeft een maximum lengte van 10.",
        ),
    ],
)
def test_validate_textfield_with_min_length_and_max_length(
    field_value: str,
    error_amount: int,
    error_message: str,
):

    # This contains a valid (formio) form, with an input fields.
    # Inputfield (key=textField) : minLength:5, maxLength:10
    form_data = {
        "form": '{"display":"wizard","title":"formtitel","name":"formname","type":["form"],"path":"formpath","components":[{"title":"Page 1","label":"Page 1","type":"panel","key":"page1", "components":[{"label":"TextField","labelPosition":"top","placeholder":"","description":"","tooltip":"","prefix":"","suffix":"","widget":{"type":"input"},"inputMask":"","allowMultipleMasks":false,"customClass":"","tabindex":"","autocomplete":"","hidden":false,"hideLabel":false,"showWordCount":false,"showCharCount":false,"mask":false,"autofocus":false,"spellcheck":true,"disabled":false,"tableView":true,"modalEdit":false,"multiple":false,"persistent":true,"inputFormat":"plain","protected":false,"dbIndex":false,"case":"","encrypted":false,"redrawOn":"","clearOnHide":true,"customDefaultValue":"","calculateValue":"","calculateServer":false,"allowCalculateOverride":false,"validateOn":"change","validate":{"required":false,"pattern":"","customMessage":"","custom":"","customPrivate":false,"json":"","minLength":5,"maxLength":10,"strictDateValidation":false,"multiple":false,"unique":false},"unique":false,"errorLabel":"","key":"textField","tags":[],"properties":{},"conditional":{"show":null,"when":null,"eq":"","json":""},"customConditional":"","logic":[],"attributes":{},"overlay":{"style":"","page":"","left":"","top":"","width":"","height":""},"type":"textfield","input":true,"refreshOn":"","inputType":"text","id":"e3ybhmw","defaultValue":"","dataGridLabel":false}],"input":false,"placeholder":"","prefix":"","customClass":"","suffix":"","multiple":false,"defaultValue":null,"protected":false,"unique":false,"persistent":false,"hidden":false,"clearOnHide":false,"refreshOn":"","redrawOn":"","tableView":false,"modalEdit":false,"labelPosition":"top","description":"","errorLabel":"","tooltip":"","hideLabel":false,"tabindex":"","disabled":false,"autofocus":false,"dbIndex":false,"customDefaultValue":"","calculateValue":"","calculateServer":false,"widget":null,"attributes":{},"validateOn":"change","validate":{"required":false,"custom":"","customPrivate":false,"strictDateValidation":false,"multiple":false,"unique":false},"conditional":{"show":null,"when":null,"eq":""},"overlay":{"style":"","left":"","top":"","width":"","height":""},"allowCalculateOverride":false,"encrypted":false,"showCharCount":false,"showWordCount":false,"properties":{},"allowMultipleMasks":false,"tree":false,"theme":"default","breadcrumb":"default","id":"e3gl95r","dataGridLabel":false}]}',  # noqa: E501
    }

    # The user input
    form_input = {"textField": f"{field_value}"}

    assert isinstance(form_data, dict)
    assert isinstance(form_input, dict)

    form = form_data.get("form", "")
    assert form is not None
    assert isinstance(form, str)

    form_components: list = json.loads(form)["components"]
    assert form_components is not None
    assert isinstance(form_components, list)

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input

    validation_service.validate_form_input_dict(form_components)
    assert len(validation_service.errors.get_errors()) == error_amount
    assert int(validation_service.errors.error_amount) == error_amount
    if error_amount > 0:
        assert validation_service.errors.get_errors()[0] == error_message
