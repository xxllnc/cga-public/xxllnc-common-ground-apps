import app.services.validation.validation_service_constants as vsc
from typing import Any, Union


def validate_number(component: dict, value: Any) -> Union[None, str]:
    """
    Validate if the value is a number without a decimal point. Returns None
    if validation was successful, an error message otherwise.

    This is only called to validate component type: number, attachment input
    key: size.

    :param component: The component contains the fieldkey the value belongs to.
    :type component: dict
    :param value: The value to validate.
    :type value: Any
    :return: None if validation was successful, an error message otherwise.
    :rtype: None or str
    """

    # TODO: isnumeric? No decimals!! CGA-1766
    if str(value).isnumeric():
        return None

    return vsc.ERROR_MESSAGE_NUMBER_NOT_VALID.format(
        component["key"],
        vsc.get_type_name(value),
    )
