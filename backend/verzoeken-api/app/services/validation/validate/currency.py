import app.services.validation.validation_service_constants as vsc
from typing import Any, Union


def validate_currency(component: dict[Any, Any], value: Any) -> Union[None, str]:
    """
    Validation for component type currency. Returns None if validation was
    successful, an error message otherwise.

    :param component: The component contains the fieldkey the value belongs to.
    :type component: dict
    :param value: The value to validate.
    :type value: Any
    :return: None if validation was successful, an error message otherwise.
    :rtype: None or str
    """

    string_number = (str(value)).split(".")
    if len(string_number) != 2 or len(string_number[1]) != 2:
        return vsc.ERROR_MESSAGE_CURRENCY_NOT_VALID.format(component["key"], value)

    if string_number[0].isnumeric() and string_number[1].isnumeric():
        return None

    return vsc.ERROR_MESSAGE_CURRENCY_NOT_VALID.format(component["key"], value)
