import app.services.validation.validation_service_constants as vsc
from typing import Any, Union


def validate_select(component: dict, value: Any) -> Union[None, str]:
    """
    Validation for component type select. Returns None
    if validation was successful, an error message otherwise.

    This is only called to validate component type: number, attachment input
    key: size.

    :param component: The component contains the fieldkey the value belongs to.
    :type component: dict
    :param value: The value to validate.
    :type value: Any
    :return: None if validation was successful, an error message otherwise.
    :rtype: None or str
    """

    # A value is a list or a string.
    if not isinstance(value, (str, list)):
        return vsc.ERROR_MESSAGE_SELECT_NOT_VALID.format(
            component["key"], value, vsc.get_type_name(value)
        )

    # Get the options.
    component_options_list: list[Any] = [option["value"] for option in component["data"]["values"]]

    # If value is a string make sure the selected option is a valid option
    if isinstance(value, str):
        if value in component_options_list:
            return None

        return vsc.ERROR_MESSAGE_SELECT_VALUE_NOT_VALID.format(
            component["key"], value, component["key"]
        )

    # Value is a list.

    # Make sure minimal one option is selected.
    if len(value) == 0:
        return vsc.ERROR_MESSAGE_SELECT_VALUES_ONE_VALUE_ALLOWED.format(component["key"], value)

    # If only one value is allowed (multiple=false), make sure
    # one option is selected.
    if (not component["multiple"]) and len(value) > 1:
        return vsc.ERROR_MESSAGE_SELECT_VALUES_ONE_VALUE_ALLOWED.format(component["key"], value)

    # Compare the number or values with the number of options.
    if len(value) > len(component_options_list):
        return vsc.ERROR_MESSAGE_SELECT_VALUES_NOT_VALID.format(
            component["key"], value, component["key"]
        )

    # Make sure all selected values are valid options.
    if not set(value).issubset(set(component_options_list)):
        return vsc.ERROR_MESSAGE_SELECT_VALUES_NOT_VALID.format(
            component["key"], value, component["key"]
        )

    return None
