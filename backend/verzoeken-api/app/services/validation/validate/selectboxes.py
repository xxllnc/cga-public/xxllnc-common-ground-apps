import app.services.validation.validation_service_constants as vsc
from typing import Any, Union


def validate_selectboxes(component: dict, value: Any) -> Union[None, str]:
    """
    Validation for component type select boxes. Returns None
    if validation was successful, an error message otherwise.

    This is only called to validate component type: number, attachment input
    key: size.

    :param component: The component contains the fieldkey the value belongs to.
    :type component: dict
    :param value: The value to validate.
    :type value: Any
    :return: None if validation was successful, an error message otherwise.
    :rtype: None or str
    """

    # Make sure the value is of type dict
    if not isinstance(value, dict):
        return vsc.ERROR_MESSAGE_SELECTBOX_NOT_VALID.format(
            component["key"],
            value,
            vsc.get_type_name(value),
        )

    # TODO CGA-1766
    # When all selectboxes are set to
    # False we expect a value like = {\"value1\":false,\"value2\":false,\"value3\":false} but in this case # noqa: E501
    # value will equal {'': False}, so also check for this if not all options match the component options. # noqa: E501
    # if set(value.keys()) != set(component_options) and value != {
    #    "": False
    # }:

    # Compare the nunber or options in the component values.
    component_options_list: list[Any] = [option["value"] for option in component["values"]]
    if len(component_options_list) != len(value):
        return vsc.ERROR_MESSAGE_SELECTBOX_VALUE_NOT_VALID.format(
            component["key"], value, component["key"]
        )

    # Check if the options in value match the options in the component values.
    if set(value.keys()) != set(component_options_list):
        return vsc.ERROR_MESSAGE_SELECTBOX_VALUE_NOT_VALID.format(
            component["key"], value, component["key"]
        )

    # Check if boolean (true/false) or string boolean ('true'/'false')
    for val in value.values():
        if isinstance(val, bool):
            continue

        if isinstance(val, str) and val.lower() in ["true", "false"]:
            # TODO CGA-1766
            # save this^ bool value in the actual request form_input
            # for form_input->component['key'] = value
            # (instead of string version of the bool) ?
            # val = True if val.lower() == "true" else False
            continue

        return vsc.ERROR_MESSAGE_SELECTBOX_VALUE_NOT_VALID.format(
            component["key"], value, component["key"]
        )

    return None
