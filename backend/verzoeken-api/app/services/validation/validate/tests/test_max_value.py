import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation import validate
from typing import Any, Union


# ---------------------------------------------
# Test the validate_max_value function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "number_input, expect_error",
    [
        (0, False),
        (5, False),
        (6, True),
    ],
)
def test_validate_max_value(number_input: int, expect_error: bool):

    component_data: dict[str, Any] = {"key": "age", "validate": {"max": 5}}
    result: Union[None, str] = validate.max_value(component_data, number_input)
    if expect_error:
        tv._assert_field_max_value_not_valid(
            str(result),
            component_data["key"],
            component_data["validate"]["max"],
        )
    else:
        assert result is None
