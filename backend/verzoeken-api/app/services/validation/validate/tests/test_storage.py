import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation import validate
from typing import Union


# ---------------------------------------------
# Test the validate_storage function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "storage_input, expect_error",
    [
        ("base64", False),
        ("url", False),
        ("", True),
        ("notvalid", True),
    ],
)
def test_validate_data_url_base64_url_not_valid(storage_input: str, expect_error: bool):
    key: str = "storage"
    result: Union[None, str] = validate.storage(key, storage_input)
    if expect_error:
        tv._assert_field_file_storage_not_valid(str(result), key, storage_input)
    else:
        assert result is None
