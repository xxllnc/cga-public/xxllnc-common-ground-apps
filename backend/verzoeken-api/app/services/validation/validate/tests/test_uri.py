import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation import validate
from typing import Union


# ---------------------------------------------
# Test the validate_uri function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "url_input, expect_error",
    [
        ("http://www.xxllnc.nl", False),
        ("http://www.xxllnc.nl/Test2 (1).docx", True),
        ("http://www.xxllnc.nl/Test2%20(1).docx", False),
        ("http://www.xxllnc.nl/Test2%20%281%29.docx", False),
        ("http%3A%2F%2Fwww.xxllnc.nl%2FTest2%20%281%29.docx", True),
        ("", True),
        ("Test", True),
    ],
)
def test_validate_uri_string(url_input: str, expect_error: bool):
    component_data: str = "url"
    result: Union[None, str] = validate.uri(component_data, url_input)
    if expect_error:
        tv._assert_field_uri_not_valid(
            str(result),
            component_data,
            url_input,
        )
    else:
        assert result is None


# ---------------------------------------------
# Test the validate_uri function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "url_input, expect_error",
    [
        ("http://www.xxllnc.nl", False),
        ("http://www.xxllnc.nl/Test2 (1).docx", True),
        ("http://www.xxllnc.nl/Test2%20(1).docx", False),
        ("http://www.xxllnc.nl/Test2%20%281%29.docx", False),
        ("http%3A%2F%2Fwww.xxllnc.nl%2FTest2%20%281%29.docx", True),
        ("", True),
        ("Test", True),
    ],
)
def test_validate_uri_dict_key(url_input: str, expect_error: bool):
    component_data: dict[str, str] = {"key": "url"}
    result: Union[None, str] = validate.uri(component_data, url_input)
    if expect_error:
        tv._assert_field_uri_not_valid(
            str(result),
            component_data["key"],
            url_input,
        )
    else:
        assert result is None


# ---------------------------------------------
# Test the validate_uri function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "url_input, expect_error",
    [
        ("http://www.xxllnc.nl", False),
        ("http://www.xxllnc.nl/Test2 (1).docx", True),
        ("http://www.xxllnc.nl/Test2%20(1).docx", False),
        ("http://www.xxllnc.nl/Test2%20%281%29.docx", False),
        ("http%3A%2F%2Fwww.xxllnc.nl%2FTest2%20%281%29.docx", True),
        ("", True),
        ("Test", True),
    ],
)
def test_validate_uri_dict_file(url_input: str, expect_error: bool):
    component_data: dict[str, str] = {"file": "url"}
    result: Union[None, str] = validate.uri(component_data, url_input)
    if expect_error:
        tv._assert_field_uri_not_valid(
            str(result),
            component_data["file"],
            url_input,
        )
    else:
        assert result is None
