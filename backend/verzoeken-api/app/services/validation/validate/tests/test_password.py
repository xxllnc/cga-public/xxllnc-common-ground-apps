import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation.validate import password as validate_password
from password_validation import PasswordPolicy  # type: ignore
from typing import Any, Final, Union

COMPONENT_DATA: Final = {"key": "password"}

PASSWORD_POLICY: Final = PasswordPolicy(
    lowercase=1, uppercase=1, symbols=1, numbers=1, min_length=8
)


# ---------------------------------------------
# Test the validate_password function.
# Check types. Only string is allowed.
# ---------------------------------------------
@pytest.mark.parametrize(
    "password_input",
    [
        None,
        0,
        1234567890,
        12345678.90,
        {},
        False,
        True,
        [],
    ],
)
def test_validate_password_wrong_type(password_input: Any):
    component_data: dict[str, str] = {}
    component_data.update(COMPONENT_DATA)

    results: Union[None, list[str]] = validate_password(
        component_data, password_input, PASSWORD_POLICY
    )
    assert isinstance(results, list)
    tv._assert_field_text_not_valid(str(results[0]), component_data["key"], password_input)


# ---------------------------------------------
# Test the validate_password function.
# Check password policy.
# ---------------------------------------------
@pytest.mark.parametrize(
    "password_input",
    [
        "",
        "222",
    ],
)
def test_validate_password_policy_1(password_input: str):
    component_data: dict[str, str] = {}
    component_data.update(COMPONENT_DATA)

    results: Union[None, list[str]] = validate_password(
        component_data, password_input, PasswordPolicy()
    )

    assert isinstance(results, list)
    assert len(results) == 1
    tv._assert_field_password_not_valid(str(results[0]), component_data["key"], password_input)


# ---------------------------------------------
# Test the validate_password function.
# Check password policy.
# ---------------------------------------------
@pytest.mark.parametrize(
    "password_input, expect_error",
    [
        ("", True),
        ("222", True),
        ("pAsWoRd12!", False),
        ("pAsWoRd12!__3$#$DFsVdfadf@#!$", False),
        ("pAsWoRd__!__a$#$DFsVdfadf@#!$", True),
    ],
)
def test_validate_password_policy_2(password_input: str, expect_error: bool):
    component_data: dict[str, str] = {}
    component_data.update(COMPONENT_DATA)

    password_policy: PasswordPolicy = PasswordPolicy(
        lowercase=1, uppercase=1, symbols=1, numbers=1, min_length=8
    )
    results: Union[None, list[str]] = validate_password(
        component_data, password_input, password_policy
    )

    if expect_error:
        assert isinstance(results, list)
        assert len(results) == 1
        tv._assert_field_password_not_valid(str(results[0]), component_data["key"], password_input)
    else:
        assert results is None
