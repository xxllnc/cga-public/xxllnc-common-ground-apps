import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation import validate
from typing import Any, Union


# ---------------------------------------------
# Test the validate_pattern function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "pattern_input, expect_error",
    [
        ("abc", False),
        ("", True),
        ("123456", True),
    ],
)
def test_validate_pattern(pattern_input: str, expect_error: bool):
    component_data: dict[str, Any] = {
        "key": "lastname",
        "validate": {"pattern": "[abc]"},
    }
    result: Union[None, str] = validate.pattern(component_data, pattern_input)
    if expect_error:
        tv._assert_field_pattern_not_valid(
            str(result),
            component_data["key"],
            component_data["validate"]["pattern"],
        )
    else:
        assert result is None


# ---------------------------------------------
# Test the validate_pattern function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "text_input, expect_error",
    [
        (12, True),
        ({}, True),
        ([], True),
        (True, True),
        (False, True),
        ("", False),
    ],
)
def test_validate_pattern_no_string(text_input: Any, expect_error: bool):
    component_data: dict[str, Any] = {
        "key": "lastname",
        "validate": {"pattern": ".*"},
    }
    result: Union[None, str] = validate.pattern(component_data, text_input)
    if expect_error:
        tv._assert_field_pattern_not_string(
            str(result),
            component_data["key"],
            text_input,
            component_data["validate"]["pattern"],
        )
    else:
        assert result is None
