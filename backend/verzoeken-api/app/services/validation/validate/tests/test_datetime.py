import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation import validate
from typing import Any, Union


# ---------------------------------------------
# Test the validate_datetime function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "datetime_input, expect_error",
    [
        ("2022-02-02", False),
        ("2022-02-02 12:00:00", False),
        ("2022-02-02 23:59:59", False),
        ("2022-02-02T12:00:00+01:00", False),
        ("2022-02-02  12:00:00", True),
        ("02-02-2022", True),
        ("02022022", True),
        ("20220202", False),
        ("12:00:00", True),
        ("", True),
        ("Test", True),
        (None, True),
        (0, True),
        (1234567890, True),
        (12345678.90, True),
        ("\u00BD", True),
        ({}, True),
        (False, True),
        (True, True),
        ([], True),
    ],
)
def test_validate_datetime(datetime_input: Any, expect_error: bool):

    component_data: dict[str, str] = {"key": "datetime"}
    result: Union[None, str] = validate.datetime(component_data, datetime_input)
    if expect_error:
        tv._assert_field_datetime_not_valid(str(result), component_data["key"], datetime_input)
    else:
        assert result is None
