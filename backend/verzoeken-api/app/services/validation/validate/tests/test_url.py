import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation import validate
from typing import Any, Union


# ---------------------------------------------
# Test the validate_url function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "url_input, expect_error",
    [
        (None, True),
        ("", True),
        ("Test", True),
        ("http://www.xxllnc.nl", False),
    ],
)
def test_validate_url(url_input: Any, expect_error: bool):
    component_data: dict[str, str] = {"key": "url"}
    result: Union[None, str] = validate.url(component_data, url_input)
    if expect_error:
        tv._assert_field_url_not_valid(
            str(result),
            component_data["key"],
            url_input,
        )
    else:
        assert result is None
