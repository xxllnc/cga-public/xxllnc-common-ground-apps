import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation import validate
from typing import Any, Final, Union

COMPONENT_DATA: Final = {
    "key": "select1",
    "validate": {},
    "multiple": False,
    "data": {
        "values": [
            {"label": "label1", "value": "value1"},
            {"label": "label2", "value": "value2"},
        ],
        "resource": "",
        "json": "",
        "url": "",
        "custom": "",
    },
    "dataType": "number",  # not implemented
    "type": "select",
}


# ---------------------------------------------
# Test the validate_select function.
# A value is a list or a string.
# ---------------------------------------------
@pytest.mark.parametrize(
    "select_input, expect_error",
    [
        ("value1", False),
        ({}, True),
        (False, True),
        (True, True),
        (0, True),
        (12.22, True),
        (None, True),
    ],
)
def test_validate_select_types(select_input: Any, expect_error: bool):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    result: Union[None, str] = validate.select(component_data, select_input)
    if expect_error:
        tv._assert_field_select_not_valid(str(result), component_data["key"], select_input)
    else:
        assert result is None


# ---------------------------------------------
# Test the validate_select function.
# If value is a string make sure the selected option is a valid option.
# ---------------------------------------------
@pytest.mark.parametrize(
    "select_input, expect_error",
    [
        ("value1", False),
        ("value2", False),
        ("value3", True),
        ("", True),
        ("    ", True),
        ("true", True),
        ("TRUE", True),
        ("false", True),
        ("FALSE", True),
    ],
)
def test_validate_select_value_not_valid(select_input: Any, expect_error: bool):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    result: Union[None, str] = validate.select(component_data, select_input)
    if expect_error:
        tv._assert_field_select_value_not_valid(str(result), component_data["key"], select_input)
    else:
        assert result is None


# ---------------------------------------------
# Test the validate_select function.
# Make sure minimal one option is selected.
# If only one value is allowed (multiple=false), make sure
# one option is selected.
# ---------------------------------------------
@pytest.mark.parametrize(
    "select_input, expect_error",
    [
        (["value1"], False),
        ([], True),
        (["value1", "value2"], True),
        (["val1", "val2", "val3"], True),
    ],
)
def test_validate_select_values_one_value_allowed(select_input: Any, expect_error: bool):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)
    component_data["multiple"] = False  # ony one option allowed

    result: Union[None, str] = validate.select(component_data, select_input)
    if expect_error:
        tv._assert_field_select_values_one_value_allowed(
            str(result), component_data["key"], select_input
        )
    else:
        assert result is None


# ---------------------------------------------
# Test the validate_select function.
# If only one value is allowed (multiple=false), make sure
# one option is selected. And the value should be valid.
# ---------------------------------------------
@pytest.mark.parametrize(
    "select_input, expect_error",
    [
        (["value1"], False),
        (["value2"], False),
        ([""], True),
        (["value3"], True),
        (["vAlUe1"], True),
        ([None], True),
        ([0], True),
        ([True], True),
        ([False], True),
    ],
)
def test_validate_select_values_not_valid(select_input: Any, expect_error: bool):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)
    component_data["multiple"] = False  # ony one option allowed

    result: Union[None, str] = validate.select(component_data, select_input)
    if expect_error:
        tv._assert_field_select_values_not_valid(str(result), component_data["key"], select_input)
    else:
        assert result is None


# ---------------------------------------------
# Test the validate_select function.
# If multiple values are allowed (multiple=true), make sure
# the values are valid. Check also the number of values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "select_input, expect_error",
    [
        (["value1"], False),
        (["value2"], False),
        (["value1", "value2"], False),
        ([""], True),
        (["value3"], True),
        (["vAlUe1"], True),
        ([None], True),
        ([0], True),
        ([True], True),
        ([False], True),
        ([None, None], True),
        (["", ""], True),
        (["", "value2"], True),
        (["value1", "vAluE2"], True),
        (["value1", "value2", "value3"], True),
    ],
)
def test_validate_select_values_not_valid_multiple(select_input: Any, expect_error: bool):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)
    component_data["multiple"] = True  # multiple options allowed

    result: Union[None, str] = validate.select(component_data, select_input)
    if expect_error:
        tv._assert_field_select_values_not_valid(str(result), component_data["key"], select_input)
    else:
        assert result is None
