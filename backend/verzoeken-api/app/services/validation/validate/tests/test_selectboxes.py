import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation import validate
from typing import Any, Final, Union

COMPONENT_DATA: Final = {
    "key": "selectBoxes1",
    "validate": {},
    "values": [
        {"label": "label1", "value": "value1"},
        {"label": "label2", "value": "value2"},
    ],
    "type": "selectboxes",
}


# ---------------------------------------------
# Test the test_validate_selectboxes function.
# Test the input type.
# ---------------------------------------------
@pytest.mark.parametrize(
    "selectboxes_input, expect_error",
    [
        ({"value1": True, "value2": True}, False),
        (False, True),
        (True, True),
        ("", True),
        ("true", True),
        ("TRUE", True),
        ("false", True),
        ("FALSE", True),
        (0, True),
        (12.22, True),
        (None, True),
        ([], True),
    ],
)
def test_validate_selectboxes_types(selectboxes_input: Any, expect_error: bool):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    result: Union[None, str] = validate.selectboxes(component_data, selectboxes_input)
    if expect_error:
        tv._assert_field_selectboxes_not_valid(
            str(result), component_data["key"], selectboxes_input
        )
    else:
        assert result is None


# ---------------------------------------------
# Test the test_validate_selectboxes function.
# Test the number of values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "selectboxes_input, expect_error",
    [
        ({"value1": True, "value2": True}, False),
        ({}, True),
        ({"value1": True}, True),  # expect 2 values instead of 1
        ({"value1": True, "valuex": False}, True),  # value2 is not set
        (
            {"value1": True, "value2": True, "value3": True},
            True,  # expect 2 values instead of 3
        ),
    ],
)
def test_validate_selectboxes_nr_of_values(selectboxes_input: dict[str, Any], expect_error: bool):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    result: Union[None, str] = validate.selectboxes(component_data, selectboxes_input)
    if expect_error:
        tv._assert_field_selectboxes_value_not_valid(
            str(result), component_data["key"], str(selectboxes_input)
        )
    else:
        assert result is None


# ---------------------------------------------
# Test the test_validate_selectboxes function.
# Test the value types
# ---------------------------------------------
@pytest.mark.parametrize(
    "selectboxes_input, expect_error",
    [
        ({"value1": True, "value2": False}, False),
        ({"value1": "true", "value2": "false"}, False),
        ({"value1": "TRUE", "value2": "FALSE"}, False),
        ({"value1": "TrUe", "value2": "FaLsE"}, False),
        ({"value1": "", "value2": False}, True),
        ({"value1": 0, "value2": False}, True),
        ({"value1": 1, "value2": False}, True),
        ({"value1": None, "value2": False}, True),
        ({"value1": {}, "value2": False}, True),
    ],
)
def test_validate_selectboxes_value_type(selectboxes_input: dict[str, Any], expect_error: bool):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    result: Union[None, str] = validate.selectboxes(component_data, selectboxes_input)
    if expect_error:
        tv._assert_field_selectboxes_value_not_valid(
            str(result), component_data["key"], str(selectboxes_input)
        )
    else:
        assert result is None
