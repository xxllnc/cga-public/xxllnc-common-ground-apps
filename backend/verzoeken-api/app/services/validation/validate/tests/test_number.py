import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation import validate
from typing import Any, Union


# ---------------------------------------------
# Test the validate_number function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "number_input, expect_error",
    [
        (0, False),
        (1234567890, False),
        (-50, True),
        (12.22, True),  # float TODO CGA-1766
        (12345678.90, True),  # float TODO CGA-1766
        (-12345678.90, True),  # float TODO CGA-1766
        (
            "\u00B23455",
            False,
        ),  # ²3455 # TODO  CGA-1766 only decimal characters
        ("\u00BD", False),  # ½  # TODO  CGA-1766 only decimal characters
        ("", True),
        ("Test", True),
        ({}, True),
        (False, True),
        (True, True),
        (None, True),
        ([], True),
    ],
)
def test_validate_number(number_input: Any, expect_error: bool):
    component_data: dict[str, str] = {"key": "number"}
    result: Union[None, str] = validate.number(component_data, number_input)
    if expect_error:
        tv._assert_field_number_not_valid(str(result), component_data["key"], number_input)
    else:
        assert result is None
