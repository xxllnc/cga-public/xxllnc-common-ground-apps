import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation import validate
from typing import Any, Union


# ---------------------------------------------
# Test the validate_min_length function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "text_input, expect_error",
    [
        ("", True),
        ("Test", True),
        ("123456", False),
    ],
)
def test_validate_min_length(text_input: Any, expect_error: bool):

    component_data: dict[str, Any] = {
        "key": "lastname",
        "validate": {"minLength": 5},
    }
    result: Union[None, str] = validate.min_length(component_data, text_input)
    if expect_error:
        tv._assert_field_min_length_not_valid(
            str(result),
            component_data["key"],
            component_data["validate"]["minLength"],
        )
    else:
        assert result is None
