import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation.validate import email as validate_email
from typing import Any, Final, Union

COMPONENT_DATA: Final = {"key": "email"}


# ---------------------------------------------
# Test the validate_email function.
# Check types. Only string is allowed.
# ---------------------------------------------
@pytest.mark.parametrize(
    "email_input",
    [
        None,
        0,
        1234567890,
        12345678.90,
        {},
        False,
        True,
        [],
    ],
)
def test_validate_email_wrong_type(email_input: Any):
    component_data: dict[str, str] = {}
    component_data.update(COMPONENT_DATA)
    result: Union[None, str] = validate_email(component_data, email_input)
    tv._assert_field_text_not_valid(str(result), component_data["key"], email_input)


# ---------------------------------------------
# Test the validate_email function.
# check for invalid values
# ---------------------------------------------
@pytest.mark.parametrize(
    "email_input, expect_error",
    [
        ("foo@example.com", False),
        ("test@example.nl", False),
        ("test@example", False),
        ("", True),
        ("wrong_type", True),
        ("text/text", True),
        ("Test", True),
        ("\u00B23455", True),
        ("\u00BD", True),
        ("test@.com", True),
        ("invalid@example,com", True),
        ("foo@bar@example.com", True),
    ],
)
def test_validate_email_values(email_input: str, expect_error: bool):
    component_data: dict[str, str] = {}
    component_data.update(COMPONENT_DATA)
    result: Union[None, str] = validate_email(component_data, email_input)
    if expect_error:
        tv._assert_field_email_not_valid(str(result), component_data["key"], email_input)
    else:
        assert result is None
