import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation import validate
from typing import Any, Union


# ---------------------------------------------
# Test the validate_phone_number function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "phone_number_input, expect_error",
    [
        ("1", True),
        ("12", False),
        ("1234567890", False),
        ("123456789012345", False),
        ("(123) 456-7890", False),
        ("+31 20 1234567", False),  # internationale notatie
        ("+31 20 12345-78", False),  # internationale not met doorkiesnummer
        ("+31 (0)20 1234567", False),  # komt ten onrechte nog voor
        ("\u00B23455", False),  # ²3455
        ("(123456789012345", False),
        ("(020) 123 45 67", True),  # nationale notatie
        ("1234567890123456", True),
        ("", True),
        ("Test", True),
        (None, True),
        (0, True),
        (1234567890, True),
        (12345678.90, True),
        ("\u00BD", True),  # ½
        ({}, True),
        (False, True),
        (True, True),
        ([], True),
    ],
)
def test_validate_phone_number(phone_number_input: Any, expect_error: bool):
    component_data: dict[str, str] = {"key": "phonenumber1"}
    result: Union[None, str] = validate.phone_number(component_data, phone_number_input)
    if expect_error:
        tv._assert_field_phone_number_not_valid(
            str(result), component_data["key"], phone_number_input
        )
    else:
        assert result is None
