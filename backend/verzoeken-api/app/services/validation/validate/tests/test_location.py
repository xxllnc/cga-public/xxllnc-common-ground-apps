import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation import validate
from app.services.validation.validate.location import LocationType
from typing import Final, Union

COMPONENT_DATA: Final = {"key": "location"}


@pytest.mark.parametrize(
    "location_input, expect_error",
    [
        (
            {
                "type": "Point",
                "coordinates": [39.643459450972394, 5.408723620036154],
            },
            False,
        ),
        (
            {
                "coordinates": [39.643459450972394, 5.408723620036154],
                "type": "Point",
            },
            False,
        ),
        ({}, True),
        ("Wrong string", True),
        (
            {
                "type": "Point",
                "coordinates": [39, 643459450972394, 5.408723620036154],
            },
            True,
        ),
        (
            {"type": "Point", "coordinates": [39, 5]},
            True,
        ),
        (
            {
                "type": "Point",
                "coordinates": [39.643459450972394, 5.408723620036154, 4.6494],
            },
            True,
        ),
        (
            {
                "type": "Point",
                "coordinates": ["39.643459450972394", "5.408723620036154"],
            },
            True,
        ),
    ],
)
def test_validate_location(location_input: LocationType, expect_error: bool):
    component_data: dict[str, str] = {}
    component_data.update(COMPONENT_DATA)
    result: Union[None, str] = validate.location(component_data, location_input)
    if expect_error:
        tv._assert_field_location_not_valid(
            str(result), component_data["key"], str(location_input)
        )
    else:
        assert result is None
