import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation import validate
from typing import Any, Union


# ---------------------------------------------
# Test the validate_radio function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "radio_input, expect_error",
    [
        ("value1", False),
        (True, True),
        (False, True),
        (0, True),
        (1234567890, True),
        (12.22, True),
        ({}, True),
        ([], True),
        (None, True),
    ],
)
def test_validate_radio_with_invalid_types(radio_input: Any, expect_error: bool):
    component_data: dict[str, Any] = {
        "key": "radio",
        "values": [{"label": "label1", "value": "value1"}],
    }
    result: Union[None, str] = validate.radio(component_data, radio_input)
    if expect_error:
        tv._assert_field_radio_not_valid(str(result), component_data["key"], radio_input)
    else:
        assert result is None


# ---------------------------------------------
# Test the validate_radio function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "radio_input, expect_error",
    [
        ("value1", False),
        ("value", True),
        ("wrongvalue", True),
        ("", True),
    ],
)
def test_validate_radio_with_values(radio_input: str, expect_error: bool):
    component_data: dict[str, Any] = {
        "key": "radio",
        "values": [{"label": "label1", "value": "value1"}],
    }

    result: Union[None, str] = validate.radio(component_data, radio_input)
    if expect_error:
        tv._assert_field_radio_value_not_valid(str(result), component_data["key"], radio_input)
    else:
        assert result is None


# ---------------------------------------------
# Test the validate_radio function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "radio_input, expect_error",
    [
        ("value1", True),
        ("", True),
    ],
)
def test_validate_radio_without_values(radio_input: str, expect_error: bool):
    component_data: dict[str, str] = {
        "key": "radio",
    }

    # TODO CGA-1766
    with pytest.raises(KeyError):
        validate.radio(component_data, radio_input)

    """
    result: Union[None, str] = validate.radio(component_data, radio_input)

    if expect_error:
        tv._assert_field_radio_value_not_valid(
            str(result), component_data["key"], radio_input
        )
    else:
        assert result is None
    """
