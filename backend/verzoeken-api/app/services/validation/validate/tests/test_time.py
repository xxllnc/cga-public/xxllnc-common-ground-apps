import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation import validate
from typing import Any, Union


# ---------------------------------------------
# Test the validate_time function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "time_input, expect_error",
    [
        ("23:59:59", False),
        ("00:00:00", False),
        ("12:11:10", False),
        ("24:00:00", True),
        ("00:00:60", True),
        ("12:60:01", True),
        ("2022-02-02 23:59:59", True),
        ("2022-02-02T23:59:59", True),
        ("", True),
        ("Test", True),
        (None, True),
        (0, True),
        (1234567890, True),
        (12345678.90, True),
        ("\u00BD", True),
        ({}, True),
        (False, True),
        (True, True),
        ([], True),
    ],
)
def test_validate_time(time_input: Any, expect_error: bool):
    component_data: dict[str, Any] = {"key": "time"}
    result: Union[None, str] = validate.time(component_data, time_input)
    if expect_error:
        tv._assert_field_time_not_valid(str(result), component_data["key"], time_input)
    else:
        assert result is None
