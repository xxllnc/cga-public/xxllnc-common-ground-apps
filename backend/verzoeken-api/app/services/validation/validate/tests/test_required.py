import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation import validate
from typing import Any, Union


# ---------------------------------------------
# Test the validate_required function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "text_input, expect_error",
    [
        (None, True),
        ("", True),  # str
        ("    ", True),  # str
        ("None", False),  # str
        ("null", False),  # str
        ("Test", False),  # str
        ("TRUE", False),  # str
        ("FALSE", False),  # str
        ("0", False),  # str
        ("\u00B23455", False),  # str ²3455
        ("\u00BD", False),  # str ½
        (0, False),  # int
        (1234567890, False),  # int
        (-10, False),  # int
        (12345678.90, False),  # float
        (-12345678.90, False),  # float
        (True, False),  # bool
        (False, False),  # bool
        ({}, True),
        ([], True),
    ],
)
def test_validate_required(text_input: Any, expect_error: bool):
    component_data: dict[str, Any] = {
        "key": "lastname",
        "validate": {"required": True},
    }
    result: Union[None, str] = validate.required(component_data, text_input)
    if expect_error:
        tv._assert_field_is_mandatory(str(result), component_data["key"])
    else:
        assert result is None


# ---------------------------------------------
# Test the validate_required function.
# No required validation available.
# ---------------------------------------------
def test_validate_required_validation_not_exist():
    component_data: dict[str, Any] = {
        "key": "lastname",
        "validate": {},
    }
    result: Union[None, str] = validate.required(component_data, "value")
    assert result is None
