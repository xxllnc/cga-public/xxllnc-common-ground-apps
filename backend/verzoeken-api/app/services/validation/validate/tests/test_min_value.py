import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation import validate
from typing import Any, Union


# ---------------------------------------------
# Test the validate_min_value function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "number_input, expect_error",
    [
        (0, True),
        (4, True),
        (5, False),
    ],
)
def test_validate_min_value(number_input: int, expect_error: bool):

    component_data: dict[str, Any] = {"key": "age", "validate": {"min": 5}}
    result: Union[None, str] = validate.min_value(component_data, number_input)
    if expect_error:
        tv._assert_field_min_value_not_valid(
            str(result),
            component_data["key"],
            component_data["validate"]["min"],
        )
    else:
        assert result is None
