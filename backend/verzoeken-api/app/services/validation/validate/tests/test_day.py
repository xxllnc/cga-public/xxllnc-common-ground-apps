import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation import validate
from typing import Any, Final, Union

COMPONENT_DATA: Final = {"key": "day"}


# ---------------------------------------------
# Test the validate_day function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "day_input, expect_error",
    [
        ("31-12-2022", False),
        ("00-00-0000", False),
        ("00-00-2000", True),
        ("00-12-0000", True),
        ("00-12-2000", True),
        ("31-00-0000", True),
        ("31-00-2000", True),
        ("2022-02-02", True),
        ("2022-02-02 12:00:00", True),
        ("2022-02-02T12:00:00+01:00", True),
        ("02022022", True),
        ("20220202", True),
        ("12:00:00", True),
        ("", True),
        ("Test", True),
        (None, True),
        (0, True),
        (1234567890, True),
        (12345678.90, True),
        ("\u00BD", True),
        ({}, True),
        (False, True),
        (True, True),
        ([], True),
    ],
)
def test_validate_day(day_input: Any, expect_error: bool):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {},
        {"required": False},
    ]:
        component_data["validate"] = validate_data
        result: Union[None, str] = validate.day(component_data, day_input)
        if expect_error:
            tv._assert_field_date_not_valid(str(result), component_data["key"], day_input)
        else:
            assert result is None


# ---------------------------------------------
# Test the validate_day function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "day_input, expect_error",
    [
        ("31-12-2022", False),
        ("00-00-0000", True),
        ("00-00-2000", True),
        ("00-12-0000", True),
        ("00-12-2000", True),
        ("31-00-0000", True),
        ("31-00-2000", True),
        ("2022-02-02", True),
        ("2022-02-02 12:00:00", True),
        ("2022-02-02T12:00:00+01:00", True),
        ("02022022", True),
        ("20220202", True),
        ("12:00:00", True),
        ("", True),
        ("Test", True),
        (None, True),
        (0, True),
        (1234567890, True),
        (12345678.90, True),
        ("\u00BD", True),
        ({}, True),
        (False, True),
        (True, True),
        ([], True),
    ],
)
def test_validate_day_mandatory(day_input: Any, expect_error: bool):
    component_data: dict[str, Any] = {}
    component_data.update(COMPONENT_DATA)

    for validate_data in [
        {"required": True},
    ]:
        component_data["validate"] = validate_data
        result: Union[None, str] = validate.day(component_data, day_input)
        if expect_error:
            tv._assert_field_date_not_valid(str(result), component_data["key"], day_input)
        else:
            assert result is None
