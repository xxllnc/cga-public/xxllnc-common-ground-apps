import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation import validate
from typing import Any, Union


# ---------------------------------------------
# Test the validate_checkbox function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "checkbox_input, expect_error",
    [
        (False, False),
        (True, False),
        ("true", False),
        ("TRUE", False),
        ("TrUe", False),
        ("false", False),
        ("FALSE", False),
        ("FaLsE", False),
        (0, True),
        (1234567890, True),
        (12.22, True),
        ("", True),
        ({}, True),
        (None, True),
        ([], True),
    ],
)
def test_validate_checkbox(checkbox_input: Any, expect_error: bool):

    component_data: dict[str, str] = {"key": "valid"}
    result: Union[None, str] = validate.checkbox(component_data, checkbox_input)
    if expect_error:
        tv._assert_field_checkbox_not_valid(str(result), component_data["key"], checkbox_input)
    else:
        assert result is None
