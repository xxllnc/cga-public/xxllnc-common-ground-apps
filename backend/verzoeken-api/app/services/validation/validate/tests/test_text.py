import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation import validate
from typing import Any, Union


# ---------------------------------------------
# Test the validate_text function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "text_input, expect_error",
    [
        ("", False),
        ("Test", False),
        (None, True),  # None is not a string
        (0, True),
        (1234567890, True),
        (12345678.90, True),  # float
        ("\u00B23455", False),  # ²3455
        ("\u00BD", False),  # ½
        ({}, True),
        (False, True),
        (True, True),
        ([], True),
    ],
)
def test_validate_text(text_input: Any, expect_error: bool):
    component_data: dict[str, str] = {"key": "textfield"}
    result: Union[None, str] = validate.text(component_data, text_input)
    if expect_error:
        tv._assert_field_text_not_valid(str(result), component_data["key"], text_input)
    else:
        assert result is None
