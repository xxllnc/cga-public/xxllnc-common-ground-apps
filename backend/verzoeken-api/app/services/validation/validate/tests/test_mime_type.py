import app.services.validation.tests.test_validate as tv
import app.services.validation.validation_service_constants as vsc
import pytest
from app.services.validation import validate
from typing import Any, Final, Union

COMPONENT_DATA: Final = {"key": "file: type"}


# ---------------------------------------------
# Test the validate_mime_type function.
# Check type
# ---------------------------------------------
@pytest.mark.parametrize(
    "mime_type_input",
    [
        None,
        0,
        1234567890,
        12345678.90,
        {},
        False,
        True,
        [],
    ],
)
def test_validate_mime_type_wrong_type(mime_type_input: Any):
    component_data: dict[str, str] = {}
    component_data.update(COMPONENT_DATA)
    result: Union[None, str] = validate.mime_type(component_data, mime_type_input)
    tv._assert_field_text_not_valid(str(result), component_data["key"], mime_type_input)


# ---------------------------------------------
# Test the validate_mime_type function.
# check for invalid values
# ---------------------------------------------
@pytest.mark.parametrize(
    "mime_type_input",
    [
        "",
        "wrong_type",
        "text/text",
        "Test",
        "\u00B23455",
        "\u00BD",
    ],
)
def test_validate_mime_type_invalid_values(mime_type_input: str):
    component_data: dict[str, str] = {}
    component_data.update(COMPONENT_DATA)
    result: Union[None, str] = validate.mime_type(component_data, mime_type_input)
    tv._assert_field_attachment_unsupported_mime_type(str(result), mime_type_input)


# ---------------------------------------------
# Test the validate_mime_type function.
# check for valid values
# ---------------------------------------------
def test_validate_mime_type_valid_values():
    component_data: dict[str, str] = {}
    component_data.update(COMPONENT_DATA)

    for mime_type_value in vsc.MIME_TYPES:
        result: Union[None, str] = validate.mime_type(component_data, mime_type_value)
        assert result is None
