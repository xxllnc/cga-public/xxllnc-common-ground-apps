import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation import validate
from typing import Any, Union


# ---------------------------------------------
# Test the validate_currency function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "currency_input, expect_error",
    [
        (1234.55, False),
        ("00.00", False),
        ("1234.55", False),
        (00.00, True),
        (-1234.55, True),
        ("-1234.55", True),
        ("1234,55", True),
        ("EUR 1234.55", True),
        ("€ 1234.55", True),
        ("10", True),
        ("10.1", True),
        ("10.123", True),
        ("", True),
        ("Test", True),
        (None, True),
        (0, True),
        (1234567890, True),
        (12345678.90, True),
        ("\u00BD", True),
        ({}, True),
        (False, True),
        (True, True),
        ([], True),
    ],
)
def test_validate_currency(currency_input: Any, expect_error: bool):
    component_data: dict[str, Any] = {"key": "currency"}
    result: Union[None, str] = validate.currency(component_data, currency_input)
    if expect_error:
        tv._assert_field_currency_not_valid(str(result), component_data["key"], currency_input)
    else:
        assert result is None
