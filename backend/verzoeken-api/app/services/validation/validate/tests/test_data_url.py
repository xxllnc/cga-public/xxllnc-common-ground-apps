import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation import validate
from typing import Union


# ---------------------------------------------
# Test the validate_data_url_base64 function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "data_url_input, expect_error",
    [
        ("data:text/plain;base64,SGVsbG8sIFdvcmxkIQ==", False),
        ("data:;base64,SGVsbG8sIFdvcmxkIQ==", False),
        ("data:text/plain,SGVsbG8sIFdvcmxkIQ==", False),
        ("data:,SGVsbG8sIFdvcmxkIQ==", False),
        ("SGVsbG8sIFdvcmxkIQ==", True),
        ("data:SGVsbG8sIFdvcmxkIQ==", True),
        ("", True),
    ],
)
def test_validate_data_url_base64_url_not_valid(data_url_input: str, expect_error: bool):
    key: str = "url"
    result: Union[None, str] = validate.data_url_base64(key, data_url_input)
    if expect_error:
        tv._assert_field_attachment_type_base64_not_valid(str(result), key, data_url_input)
    else:
        assert result is None


# ---------------------------------------------
# Test the validate_data_url_base64 function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "data_url_input, expect_error",
    [
        ("data:text/plain;base64,SGVsbG8sIFdvcmxkIQ==", False),
        ("data:,Hello%2C%20World%21", False),
        ("data:,~8", True),
    ],
)
def test_validate_data_url_base64_base64_data_not_valid(data_url_input: str, expect_error: bool):
    key: str = "url"
    result: Union[None, str] = validate.data_url_base64(key, data_url_input)
    if expect_error:
        tv._assert_field_attachment_type_base64_data_not_valid(str(result), key, data_url_input)
    else:
        assert result is None
