import app.services.validation.tests.test_validate as tv
import pytest
from app.services.validation import validate
from typing import Any, Union


# ---------------------------------------------
# Test the validate_max_length function.
# ---------------------------------------------
@pytest.mark.parametrize(
    "text_input, expect_error",
    [
        ("", False),
        ("Test", False),
        ("123456", True),
    ],
)
def test_validate_max_length(text_input: Any, expect_error: bool):

    component_data: dict[str, Any] = {
        "key": "lastname",
        "validate": {"maxLength": 5},
    }
    result: Union[None, str] = validate.max_length(component_data, text_input)
    if expect_error:
        tv._assert_field_max_length_not_valid(
            str(result),
            component_data["key"],
            component_data["validate"]["maxLength"],
        )
    else:
        assert result is None
