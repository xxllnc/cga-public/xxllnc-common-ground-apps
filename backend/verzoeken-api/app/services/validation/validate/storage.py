import app.services.validation.validation_service_constants as vsc
from typing import Union


def validate_storage(key: str, value: str) -> Union[None, str]:
    """
    Validation for attachment-storage. Returns None if
    validation was successful, an error message otherwise.

    :param key: The key the value belongs to.
    :type key: str
    :param value: The value to validate.
    :type value: str
    :return: None if validation was successful, an error message otherwise.
    :rtype: None or str
    """

    if value not in ("base64", "url"):
        return vsc.ERROR_MESSAGE_FILE_STORAGE_NOT_VALID.format(key, value)

    return None
