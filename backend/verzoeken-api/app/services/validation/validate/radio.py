import app.services.validation.validation_service_constants as vsc
from typing import Any, Union


def validate_radio(component: dict, value: Any) -> Union[None, str]:
    """
    Validate the radio component. Returns None
    if validation was successful, an error message otherwise.

    This is only called to validate component type: number, attachment input
    key: size.

    :param component: The component contains the fieldkey the value belongs to.
    :type component: dict
    :param value: The value to validate.
    :type value: Any
    :return: None if validation was successful, an error message otherwise.
    :rtype: None or str
    """

    # Make sure the value is of type string
    if not isinstance(value, str):
        return vsc.ERROR_MESSAGE_RADIO_NOT_VALID.format(
            component["key"], str(value), vsc.get_type_name(value)
        )

    # TODO CGA-1766
    # Make sure it is one of the valid options
    component_options = [option["value"] for option in component["values"]]
    if value not in component_options:
        return vsc.ERROR_MESSAGE_RADIO_VALUE_NOT_VALID.format(
            component["key"], value, component["key"]
        )

    return None
