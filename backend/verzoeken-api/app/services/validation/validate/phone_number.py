import app.services.validation.validation_service_constants as vsc
import re
from typing import Any, Union


def validate_phone_number(component: dict[Any, Any], value: Any) -> Union[None, str]:
    """
    Validate if the value is a phone number. Returns None if validation was
    successful, an error message otherwise.

    This is only called to validate component type: textfield, textarea
    and email, attachment input key: name, type and original_name, and to
    validate the email.

    :param component: The component contains the fieldkey the value belongs to.
    :type component: dict
    :param value: The value to validate.
    :type value: Any
    :return: None if validation was successful, an error message otherwise.
    :rtype: None or str
    """

    if not isinstance(value, str):
        return vsc.ERROR_MESSAGE_PHONE_NUMBER_NOT_VALID.format(component["key"], value)

    # TODO CGA-1843
    phone_number = re.sub("[^0-9+]", "", value)
    regex_result = re.match(r"^\+?[1-9]\d{1,14}$", phone_number)

    # another regex option for getting a E.164 phoneNumber (+ the specific land code): # noqa: E501
    # \+?(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]| # noqa: E501
    # 5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$
    if not regex_result:
        return vsc.ERROR_MESSAGE_PHONE_NUMBER_NOT_VALID.format(component["key"], value)

    return None
