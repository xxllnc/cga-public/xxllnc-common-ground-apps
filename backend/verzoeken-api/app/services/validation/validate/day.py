import app.services.validation.validation_service_constants as vsc
from datetime import datetime
from typing import Any, Union


def validate_day(component: dict[Any, Any], value: Any) -> Union[None, str]:
    """
    Validate if the value is a day or date with format %d-%m-%Y"; 31-12-2021
    Returns None if validation was successful, an error message otherwise.

    :param component: The component contains the fieldkey the value belongs to.
    :type component: dict
    :param value: The value to validate.
    :type value: Any
    :return: None if validation was successful, an error message otherwise.
    :rtype: None or str
    """

    if not isinstance(value, str):
        return vsc.ERROR_MESSAGE_DATE_NOT_VALID.format(component["key"], value)

    day_str: str = value.replace("/", "-")

    # If the value contains a default formio value (00/00/0000)
    # and the value is not required,
    # ignore the format check
    if (day_str == "00-00-0000") and (
        (
            ("required" in component["validate"] and not component["validate"]["required"])
            or ("required" not in component["validate"])
        )
    ):
        return None

    try:
        datetime.strptime(day_str, "%d-%m-%Y")
    except ValueError:
        return vsc.ERROR_MESSAGE_DATE_NOT_VALID.format(component["key"], value)

    return None
