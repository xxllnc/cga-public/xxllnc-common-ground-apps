import app.services.validation.validation_service_constants as vsc
from typing import Any, Union
from validators import url  # type: ignore


def validate_url(component: dict, value: Any) -> Union[None, str]:
    """
    Validate if the value is a url. Returns None
    if validation was successful, an error message otherwise.

    This is only called to validate component type: number, attachment input
    key: size.

    :param component: The component contains the fieldkey the value belongs to.
    :type component: dict
    :param value: The value to validate.
    :type value: Any
    :return: None if validation was successful, an error message otherwise.
    :rtype: None or str
    """

    if not url(str(value)):  # type: ignore
        return vsc.ERROR_MESSAGE_URL_NOT_VALID.format(
            component["key"],
            vsc.get_type_name(value),
        )

    return None
