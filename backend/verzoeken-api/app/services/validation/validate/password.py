import app.services.validation.validation_service_constants as vsc
from app.services.validation.validate.text import validate_text
from password_validation import PasswordPolicy  # type: ignore
from typing import Any, Union


def validate_password(
    component: dict[Any, Any], value: Any, password_policy: PasswordPolicy
) -> Union[None, list[str]]:
    """
    Validate if the value is a password. Returns None if validation was
    successful, an error message otherwise.

    :param component: The component contains the fieldkey the value belongs to.
    :type component: dict
    :param value: The value to validate.
    :type value: Any
    :return: None if validation was successful, a list of error messages
             otherwise.
    :rtype: None or list
    """

    results: list[str] = []
    result: Union[None, str] = validate_text(component, value)
    if result:
        results.append(result)
        return results

    if password_policy.validate(value):
        return None

    results.append(vsc.ERROR_MESSAGE_PASSWORD_NOT_VALID.format(component["key"], value))

    # TODO CGA-1970
    # unfulfilled_requirements = password_policy.test_password(value)
    # for unfulfilled_requirement in unfulfilled_requirements:
    #    results.append(
    #        str(component["key"]) + f": {unfulfilled_requirement.name}"
    #    )

    return results
