import app.services.validation.validation_service_constants as vsc
import re
from typing import Any, Union


def validate_pattern(component: dict, value: Any) -> Union[None, str]:
    """
    Validates for pattern. Returns None if validation was
    successful, an error message otherwise.

    :param component: The component contains the fieldkey and validation
                      configuration the value belongs to.
    :type component: dict
    :param value: The value to validate.
    :type value: Any
    :return: None if validation was successful, an error message otherwise.
    :rtype: None or str
    """

    if (
        "validate" in component
        and "pattern" in component["validate"]
        and component["validate"]["pattern"] is not None
        and component["validate"]["pattern"] != ""
    ):
        # Make sure value is a of type string, cause
        # if not = internal server error
        if not isinstance(value, str):
            return vsc.ERROR_MESSAGE_FIELD_PATTERN_NOT_STRING.format(
                component["key"],
                vsc.get_type_name(value),
                component["validate"]["pattern"],
            )
        else:
            regex_result = re.match(component["validate"]["pattern"], value)
            if not regex_result:
                return vsc.ERROR_MESSAGE_FIELD_PATTERN.format(
                    component["key"], component["validate"]["pattern"]
                )

    return None
