import app.services.validation.validation_service_constants as vsc
from typing import Union
from validators import url  # type: ignore


def validate_uri(component: Union[dict, str], value: str) -> Union[None, str]:
    """
    Validate if the value is a uri. Returns None
    if validation was successful, an error message otherwise.

    :param component: The component contains the fieldkey the value belongs to.
    :type component: dict or str
    :param value: The value to validate.
    :type value: str
    :return: None if validation was successful, an error message otherwise.
    :rtype: None or str
    """

    key_str: str = ""
    if isinstance(component, str):
        key_str = component

    if isinstance(component, dict):
        if "key" in component:
            key_str = str(component["key"])
        elif "file" in component:
            key_str = str(component["file"])

    # Only validates urls for now, not reversed dns uri's
    if not url(value):  # type: ignore
        return vsc.ERROR_MESSAGE_URI_NOT_VALID.format(key_str, value)

    return None
