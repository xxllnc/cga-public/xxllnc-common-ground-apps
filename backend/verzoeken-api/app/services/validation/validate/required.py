import app.services.validation.validation_service_constants as vsc
from typing import Any, Union


def validate_required(component: dict[Any, Any], value: Any) -> Union[None, str]:
    """
    Validate if a required value is set. Returns None if validation was
    successful, an error message otherwise.

    :param component: The component contains the fieldkey and validation
                      configuration the value belongs to.
    :type component: dict
    :param value: The value to validate.
    :type value: Any
    :return: None if validation was successful, an error message otherwise.
    :rtype: None or str
    """

    # A value is required when the required attribute is set (True).
    if "required" not in component["validate"]:
        return None

    if not component["validate"]["required"]:
        return None

    # TODO CGA-1766
    # Check if boolean (true/false)
    if isinstance(value, bool):
        return None

    # Check if number
    if isinstance(value, int):
        return None

    if value and (not isinstance(value, str) or value.strip()):
        return None

    return vsc.ERROR_MESSAGE_FIELD_IS_MANDATORY.format(component["key"])
