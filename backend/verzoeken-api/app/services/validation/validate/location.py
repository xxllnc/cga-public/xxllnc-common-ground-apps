import app.services.validation.validation_service_constants as vsc
from typing import Tuple, TypedDict, Union

LocationType = TypedDict(
    "LocationType",
    {"type": str, "coordinates": Tuple[float, float]},
)


def validate_location(component: dict, value: LocationType) -> Union[None, str]:
    """
    Validate if the value is a location. Returns None if validation was
    successful, an error message otherwise.

    :param component: The component contains the fieldkey the value belongs to.
    :type component: dict
    :param value: The value to validate.
    :type value: LocationType
    :return: None if validation was successful, an error message otherwise.
    :rtype: None or str
    """

    error_message: str = vsc.ERROR_MESSAGE_LOCATION_NOT_VALID.format(component["key"], str(value))

    try:
        long, lat = value["coordinates"]
        if not isinstance(long, float) or not isinstance(lat, float):
            return error_message
    except (ValueError, KeyError, TypeError):
        return error_message

    return None
