import app.services.validation.validation_service_constants as vsc
from base64 import b64decode, b64encode
from binascii import Error as BinasciiError
from typing import Union


def validate_data_url_base64(key: str, value: str) -> Union[None, str]:
    """
    Validate if the Data URL contains a valid Base64 string. Returns None if
    validation was successful, an error message otherwise.

    Data URLs are composed of four parts: a prefix (data:), a MIME type
    indicating the type of data, an optional base64 token if non-textual,
    and the data itself:

    data:[<mediatype>][;base64],<data>

    :param key: The key the value belongs to.
    :type key: str
    :param value: The value to validate.
    :type value: str
    :return: None if validation was successful, an error message otherwise.
    :rtype: None or str
    """

    if not value.startswith("data:"):
        return vsc.ERROR_MESSAGE_DATA_URL_NOT_VALID.format(key, value)

    data_url_parts: list[str] = value.split(",")
    if len(data_url_parts) < 2:
        return vsc.ERROR_MESSAGE_DATA_URL_NOT_VALID.format(key, value)

    try:
        b64encode(b64decode(data_url_parts[1])) == data_url_parts[1]
    except BinasciiError:
        return vsc.ERROR_MESSAGE_DATA_URL_BASE64_DATA_NOT_VALID.format(key, value)

    return None
