import app.services.validation.validation_service_constants as vsc
from datetime import datetime
from typing import Any, Union


def validate_time(component: dict[Any, Any], value: Any) -> Union[None, str]:
    """
    Validate if the value is a time with format %H:%M:%S"; 11-50-00
    Returns None if validation was successful, an error message otherwise.

    :param component: The component contains the fieldkey the value belongs to.
    :type component: dict
    :param value: The value to validate.
    :type value: Any
    :return: None if validation was successful, an error message otherwise.
    :rtype: None or str
    """

    if not isinstance(value, str):
        return vsc.ERROR_MESSAGE_TIME_NOT_VALID.format(component["key"], value)

    try:
        datetime.strptime(value, "%H:%M:%S")
    except ValueError:
        return vsc.ERROR_MESSAGE_TIME_NOT_VALID.format(component["key"], value)

    return None
