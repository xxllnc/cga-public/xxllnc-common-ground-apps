import app.services.validation.validation_service_constants as vsc
from app.services.validation import validate
from typing import Any, Union


def validate_mime_type(component: dict[Any, Any], value: Any) -> Union[None, str]:
    """
    Validate if the value is a mimetype. Returns None if validation was
    successful, an error message otherwise.

    :param component: The component contains the fieldkey the value belongs to.
    :type component: dict
    :param value: The value to validate.
    :type value: Any
    :return: None if validation was successful, an error message otherwise.
    :rtype: None or str
    """

    result: Union[None, str] = validate.text(component, value)
    if result:
        return result

    if value not in vsc.MIME_TYPES:
        return vsc.ERROR_MESSAGE_UNSUPPORTED_MIME_TYPE.format(value)

    return None
