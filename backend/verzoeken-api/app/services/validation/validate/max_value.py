import app.services.validation.validation_service_constants as vsc
from typing import Any, Union


def validate_max_value(component: dict, value: Any) -> Union[None, str]:
    """
    Validates for maximum value. Returns None if validation was
    successful, an error message otherwise.

    :param component: The component contains the fieldkey and validation
                      configuration the value belongs to.
    :type component: dict
    :param value: The value to validate.
    :type value: Any
    :return: None if validation was successful, an error message otherwise.
    :rtype: None or str
    """

    if component["validate"]["max"] and int(value) > int(component["validate"]["max"]):
        return vsc.ERROR_MESSAGE_FIELD_MAX_VALUE.format(
            component["key"], component["key"], component["validate"]["max"]
        )

    return None
