import app.services.validation.validation_service_constants as vsc
from typing import Any, Union


def validate_checkbox(component: dict, value: Any) -> Union[None, str]:
    """
    Validate the checkbox value. Returns None
    if validation was successful, an error message otherwise.

    This is only called to validate component type: number, attachment input
    key: size.

    :param component: The component contains the fieldkey the value belongs to.
    :type component: dict
    :param value: The value to validate.
    :type value: Any
    :return: None if validation was successful, an error message otherwise.
    :rtype: None or str
    """

    # Check if boolean (true/false) or string boolean ('true'/'false')
    if isinstance(value, bool):
        return None

    if isinstance(value, str) and value.lower() in ["true", "false"]:
        # TODO CGA-1766
        # save this^ bool value in the actual request form_input
        # for form_input->component['key'] = value
        # (instead of string version of the bool) ?
        value = True if value.lower() == "true" else False
        return None

    return vsc.ERROR_MESSAGE_CHECKBOX_NOT_VALID.format(
        component["key"], value, vsc.get_type_name(value)
    )
