import app.services.validation.validation_service_constants as vsc
from typing import Any, Union


def validate_text(component: dict[Any, Any], value: Any) -> Union[None, str]:
    """
    Validate if the value is a string. Returns None if validation was
    successful, an error message otherwise.

    This is only called to validate component type: textfield, textarea
    and email, attachment input key: name, type and original_name, and to
    validate the email.

    :param component: The component contains the fieldkey the value belongs to.
    :type component: dict
    :param value: The value to validate.
    :type value: Any
    :return: None if validation was successful, an error message otherwise.
    :rtype: None or str
    """

    if not isinstance(value, str):
        return vsc.ERROR_MESSAGE_TEXT_NOT_VALID.format(
            component["key"],
            vsc.get_type_name(value),
        )

    return None
