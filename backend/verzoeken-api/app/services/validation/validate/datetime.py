import app.services.validation.validation_service_constants as vsc
from datetime import datetime
from typing import Any, Union


def validate_datetime(component: dict[Any, Any], value: Any) -> Union[None, str]:
    """
    Validate if the value is a datetime in ISO 8601
    format; 2021-08-03T07:40:51+00:00. Returns None if validation was
    successful, an error message otherwise.

    :param component: The component contains the fieldkey the value belongs to.
    :type component: dict
    :param value: The value to validate.
    :type value: Any
    :return: None if validation was successful, an error message otherwise.
    :rtype: None or str
    """

    if not isinstance(value, str):
        return vsc.ERROR_MESSAGE_DATETIME_NOT_VALID.format(component["key"], value)

    try:
        datetime.fromisoformat(value)
    except ValueError:
        return vsc.ERROR_MESSAGE_DATETIME_NOT_VALID.format(component["key"], value)

    return None
