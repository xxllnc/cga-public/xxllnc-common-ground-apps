import app.services.validation.validation_service_constants as vsc
from app.services.validation.validate.text import validate_text
from typing import Any, Union
from validate_email import validate_email as ve  # type: ignore


def validate_email(component: dict[Any, Any], value: Any) -> Union[None, str]:
    """
    Validate if the value is an email. Returns None if validation was
    successful, an error message otherwise.

    Verify if email exists is not implemented (yet).

    :param component: The component contains the fieldkey the value belongs to.
    :type component: dict
    :param value: The value to validate.
    :type value: Any
    :return: None if validation was successful, an error message otherwise.
    :rtype: None or str
    """

    result: Union[None, str] = validate_text(component, value)
    if result:
        return result

    # Do not check if email really exists.
    if not ve(value, verify=False):
        return vsc.ERROR_MESSAGE_EMAIL_NOT_VALID.format(component["key"], value)

    return None
