from typing import Any, Optional


def _check_instance_and_convert_to_string(value: Any) -> Optional[str]:
    if isinstance(value, str):
        return value

    if isinstance(value, bool):
        # True -> true and False -> false
        return "true" if value else "false"

    if isinstance(value, (int, float)):
        return str(value)

    return None


def check_if_conditionally_visible(component: dict, form_input: dict) -> bool:

    custom_conditional = component.get("customConditional", None)
    conditional = component.get("conditional", None)

    # TODO implement this check CGA-2112
    if custom_conditional:
        # For now default to False so user is able to submit form
        return False

    if conditional:
        if conditional.get("when", False):

            trigger_key: str = conditional.get("when")
            value = form_input.get(trigger_key)
            eq: str = conditional.get("eq")

            # show, to include or exclude field when condition is true
            show: bool = conditional.get("show")

            if isinstance(value, list):
                for x in value:
                    value_str = _check_instance_and_convert_to_string(x)
                    if value_str and eq == value_str:
                        return show
            elif isinstance(value, dict) and eq in value:
                return value.get(eq) == show
            else:
                value_str = _check_instance_and_convert_to_string(value)
                if value_str:
                    value = value_str

            return (value == eq) == show

        # TODO implement this check CGA-2113
        if conditional.get("json", False):
            # For now default to False so user is able to submit form
            return False

    return True
