"""
This validation_service module is used to validate the input for a request by
comparing its form input to the settings of all properties (/components)
in the form that is filled in by the user
"""
import app.services.validation.validation_service_constants as vsc
import babel.numbers  # type: ignore
import ipaddress
import json
import re
import validators  # type: ignore
from .call_external_validation_api import call_external_validation_api
from .check_if_conditionally_visible import check_if_conditionally_visible
from app import schemas
from app.core import config
from app.db import models
from app.resources import errors
from app.services.commonground_service import get_resource
from app.services.helper_functions import get_storage_type
from app.services.validation import validate
from decimal import Decimal, DecimalException
from fastapi import status as http_status
from fastapi.exceptions import HTTPException
from loguru import logger
from password_validation import PasswordPolicy  # type: ignore
from sqlalchemy.orm import Session
from typing import Any, Final, List, Optional, Union
from uuid import UUID


class ValidationService:
    """The validationService class, this class is used to validate
    the form input of a request"""

    def __init__(self):
        """
        Inits ValidationService.
        On init the following attributes are set to the default values
            - request_create_or_update_request: the request input, set when
            doing a validate_request()
            - database: database Session
            - errors: Errors class
            - form_components:  a list with the properties (/components) from
            the form given in the request
            - form_input_dict: a dictionary with the values from the form_input
            given in the request
            - password_policy: for validating a password from the
            password_validation library.
        """
        self.request_create_or_update_request: Optional[schemas.RequestBase] = None
        self.database: Optional[Session] = None
        self.errors: ValidationService.Errors = self.Errors()
        self.form: dict = {}
        self.form_components: list = []
        self.form_input_dict: dict = {}
        self.form_expected_keys: list[str] = []
        self.password_policy: PasswordPolicy = PasswordPolicy(
            lowercase=1, uppercase=1, symbols=1, numbers=1, min_length=8
        )

    class Errors:
        """An errors class to store errors in while validating the form_input,
        so we can return them all at the end of validate_request()"""

        def __init__(self):
            self.local_errors: list = []
            self.error_amount: int = 0

        def add_error(self, message: str):
            self.local_errors.append(message)
            self.error_amount = self.error_amount + 1

        def get_errors(self):
            return self.local_errors

    def _fetch_form_components_from_api_and_set_self_form_components(
        self, id: str, organization_url: str
    ) -> Union[List[dict], None]:
        api_url = config.get_active_forms_api_url(organization_url)
        form_url: Final[str] = f"{api_url}/{id}"
        form_not_found_error = errors.generate_bad_request(
            vsc.ERROR_MESSAGE_FORM_NOT_FOUND.format(form_url),
            "invalidForm",
        )
        try:
            UUID(id)
        except ValueError:
            return errors.generate_bad_request(
                vsc.ERROR_MESSAGE_FORM_URL_NOT_VALID.format(form_url),
                "invalidForm",
            )

        response = get_resource(form_url, organization_url)

        if isinstance(response, bool) or response.status_code != http_status.HTTP_200_OK:
            return form_not_found_error

        json_response = response.json()

        if not isinstance(json_response, dict) or "form" not in json_response:
            return form_not_found_error

        self.form = json_response["form"]
        form_components = json_response["form"].get("components")

        if (
            form_components
            and isinstance(form_components, list)
            and len(form_components) > 0
            and "type" in form_components[0]
        ):

            self.form_components = form_components
            return None

        return errors.generate_bad_request(
            f"{form_url} form.io form should at least contain one valid "
            'component in a list of "components"',
            "invalidForm",
        )

    def validate_request(
        self,
        request_create_or_update_request: schemas.RequestBase,
        organization_url: str,
        database: Session,
    ) -> dict:
        """Validates the form_input of a request and returns true or an error"""  # noqa: E501

        self.request_create_or_update_request = request_create_or_update_request  # (re)set request
        self.database = database

        error = self._fetch_form_components_from_api_and_set_self_form_components(
            request_create_or_update_request.form_uuid, organization_url
        )
        if error:
            return {"valid": False, "error": error}

        error = self.create_form_input_dict()
        if isinstance(error, list):
            return {"valid": False, "error": error}

        call_external_validation_api(
            organization_url=organization_url, form=self.form, form_input=self.form_input_dict
        )

        self.errors = self.Errors()  # (re)set errors
        self.form_expected_keys = [
            "confirmationCheckBox",
            "digid_bsn",
            "digid_geboortedatum",
            "digid_geslachtsnaam",
            "digid_voornamen",
        ]
        self.validate_form_input_dict()

        if self.errors.error_amount > 0:
            return {
                "valid": False,
                "error": [
                    {
                        "msg": self.errors.get_errors(),
                        "type": "formInputValidation",
                        "error_amount": self.errors.error_amount,
                    }
                ],
            }

        for key in request_create_or_update_request.form_input.keys():
            if key not in self.form_expected_keys:
                return {
                    "valid": False,
                    "error": [
                        {
                            "msg": "Onbekende velden in verzonden formulier",
                            "type": "formInputValidation",
                            "error_amount": 1,
                        }
                    ],
                }

        return {"valid": True, "error": None}

    def create_form_input_dict(self):
        """Creates a normal dictionary with each property & value pair from
        the form_input json blob"""
        form_input: dict = {}
        try:
            if self.request_create_or_update_request:
                # For validation, remove all fieds that only contain a empty string as value
                form_input = {
                    k: v
                    for k, v in self.request_create_or_update_request.form_input.items()
                    if v != ""
                }
                self.form_input_dict = form_input
                return None
        except ValueError:
            if len(form_input) > 100:
                form_input = {}
            return errors.generate_bad_request(
                f"form_input {form_input} is geen geldige json.",
                "invalidFormInput",
            )

    def validate_value(self, component: dict):

        key: Final[str] = component["key"]
        validate: Final[dict] = component.get("validate", {})
        value: Final = self.form_input_dict.get(key, None)

        required = validate.get("required", False)
        conditionally_visible = check_if_conditionally_visible(component, self.form_input_dict)

        if not conditionally_visible:
            return  # field was not visible, abort validation

        # Field was visible so add it to the expected key
        self.form_expected_keys.append(key)

        if value is None:
            if not required:
                return

            if required:
                err = vsc.ERROR_MESSAGE_FIELD_IS_MANDATORY.format(key)
                logger.error(f"No value: {err}")
                self.errors.add_error(err)

            return

        self.__component_type_switch(component, value)

        for validation in validate:
            self.__validate_type_switch(validation, component, value)

    def validate_form_input_dict(self, form_components: Optional[list] = None):
        """Validates the values in the (form_input) dictionary with the
        properties from a form"""
        if form_components is None:
            form_components = self.form_components

        # loop through form properties and validate the values
        for component in form_components:
            comp_type = component["type"]
            is_layout = comp_type in ["fieldset", "well"]
            is_special = comp_type in ["container", "datagrid", "editGrid"]

            if is_layout or is_special:
                self.validate_form_input_dict(component["components"])

            elif comp_type == "panel":
                conditionally_visible = check_if_conditionally_visible(
                    component, self.form_input_dict
                )
                if not conditionally_visible:
                    return  # panel was not visible, abort validation
                self.validate_form_input_dict(component["components"])

            elif comp_type == "columns":
                for column in component["columns"]:
                    self.validate_form_input_dict(column["components"])

            elif comp_type == "table":
                for row in component["rows"]:
                    for column in row:
                        self.validate_form_input_dict(column["components"])
            else:
                self.validate_value(component)

    def __component_type_switch(self, component: dict, value):
        """A switch for different component types"""
        # Get the function from switcher dictionary
        # Execute the function
        switcher = {
            "textfield": validate.text,
            "textarea": validate.text,
            "number": validate.number,
            "uuid": self.__validate_uuid,
            "email": validate.email,
            "phoneNumber": validate.phone_number,
            "date": validate.day,
            "day": validate.day,
            "time": validate.time,
            "datetime": validate.datetime,
            "month": self.__validate_month,
            "week": self.__validate_week,
            "checkbox": validate.checkbox,
            "selectboxes": validate.selectboxes,
            "select": validate.select,
            "radio": validate.radio,
            "currency": validate.currency,
            "money": self.__validate_money,
            "file": self.__validate_attachment,
            "image": self.__validate_attachment,
            "color": self.__validate_color,
            "url": validate.url,
            "float": self.__validate_float,
            "double": self.__validate_double,
            "integer": self.__validate_integer,
            # "int32": self.__validate_int32, # TODO CGA-1844
            # "int64": self.__validate_int64, # TODO CGA-1844
            "uri": validate.uri,
            "hostname": self.__validate_hostname,
            "ipv4": self.__validate_ipv4,
            "ipv6": self.__validate_ipv6,
            "byte": self.__validate_byte,
            "binary": self.__validate_binary,
            "kenteken": self.__validate_kenteken,
            "bsn": self.__validate_bsn,
            "kvk": self.__validate_kvk,
            "rsin": self.__validate_rsin,
            "gemeente code": self.__validate_gemeente_code,
            "locationLeafletComponent": validate.location,
        }
        func = switcher.get(component["type"], None)
        if func is not None:
            result = func(component, value)
            if result:
                self.errors.add_error(result)
            return

        # Get the function from switcher dictionary
        # Execute the function
        switcher2 = {
            "password": validate.password,
        }
        func2 = switcher2.get(component["type"], None)
        if func2 is not None:
            # TODO CGA-1970
            password_policy: PasswordPolicy = PasswordPolicy(
                lowercase=1, uppercase=1, symbols=1, numbers=1, min_length=8
            )
            result = func2(component, value, password_policy)
            if result:
                for x in result:
                    self.errors.add_error(x)

    def __validate_type_switch(self, validation: str, component: dict, value):
        """A switch for different validations"""
        switcher = {
            "unique": self.__validate_unique,
            "required": validate.required,
            "minLength": validate.min_length,
            "maxLength": validate.max_length,
            "min": validate.min_value,
            "max": validate.max_value,
            "exclusiveMinimum": self.__validate_exclusive_minimum,
            "exclusiveMaximum": self.__validate_exclusive_maximum,
            "multipleOf": self.__validate_multiple_of,
            "minItems": self.__validate_min_items,
            "maxItems": self.__validate_max_items,
            "uniqueItems": self.__validate_unique_items,
            "pattern": validate.pattern,
        }
        # Get the function from switcher dictionary
        func = switcher.get(validation, None)
        # Execute the function
        if func is not None:
            result = func(component, value)
            if result:
                self.errors.add_error(result)

    def __validate_attachment_type(self, key: str, value, storage_type: str):
        is_url = storage_type == "url"
        """A switch for the different attachment input key"""
        switcher = {
            "storage": validate.storage,
            "name": validate.text,
            "url": validate.uri if is_url else validate.data_url_base64,
            "size": validate.number,
            "type": validate.text if is_url else validate.mime_type,
            "originalName": validate.text,
        }

        # Get the function from switcher dictionary
        # Key string looks something like this: "file: name" or
        # "file: url" so we need to get the substring after ' '
        function = switcher.get(key[key.rindex(" ") + 1 :], None)  # noqa: E203
        use_dict = [validate.text, validate.number, validate.mime_type]
        if function is not None:
            function_key = {"key": key} if function in use_dict else key

            result: Union[None, str] = function(function_key, value)  # type: ignore  # noqa: E501
            if result:
                self.errors.add_error(result)

    def __validate_unique(self, component: dict, value):
        """Validate if a value is unique"""
        if component["validate"]["unique"]:
            # NOTE: if the request input body has an url for the form,
            # this will only check requests with the exact same url set as
            # their form, so even if there is a request with a form set to a
            # json blob that matches the json blob of the form url, this will
            # not count it as a request with the same form.

            if self.database is None or self.request_create_or_update_request is None:
                raise HTTPException(
                    status_code=http_status.HTTP_500_INTERNAL_SERVER_ERROR,
                    detail="Add database Session to validate()",
                )
            component_property = component["key"]
            # get all requests with the same form
            query = self.database.query(models.Request).filter(
                models.Request.form_uuid == self.request_create_or_update_request.form_uuid
            )
            result = self.database.execute(query).scalars().all()
            # than loop through all requests, get component['key'] value and
            # check if that value matches the input value
            for request in result:
                # print(request.id) # PRINT FOR DEBUGGING
                try:
                    request_form_input_dict = json.loads(request.form_input)
                    if component_property in request_form_input_dict:
                        request_value = request_form_input_dict.get(component_property)
                        # print(requestValue) # PRINT FOR DEBUGGING
                        if request_value == value:
                            self.errors.add_error(
                                str(component_property) + f" moet uniek zijn, er bestaat al een "
                                f"verzoek met deze waarde: {value}."
                            )
                            break
                except ValueError:
                    continue

    def __validate_exclusive_minimum(self, component: dict, value):
        """Validation for exclusive minimum"""
        if component["validate"]["exclusiveMinimum"] and int(value) <= int(
            component["validate"]["exclusiveMinimum"]
        ):
            self.errors.add_error(
                str(component["key"])
                + " is te laag, "
                + str(component["key"])
                + " heeft een exclusief minimum waarde van "
                + str(component["validate"]["exclusiveMinimum"])
                + "."
            )

    def __validate_exclusive_maximum(self, component: dict, value):
        """Validation for exclusive maximum"""
        if component["validate"]["exclusiveMaximum"] and int(value) >= int(
            component["validate"]["exclusiveMaximum"]
        ):
            self.errors.add_error(
                str(component["key"])
                + " is te hoog, "
                + str(component["key"])
                + " heeft een exclusief maximum waarde van "
                + str(component["validate"]["exclusiveMaximum"])
                + "."
            )

    def __validate_multiple_of(self, component: dict, value):
        """Validation for multipleOf"""
        if not int(value) % int(component["validate"]["multipleOf"]) == 0:
            self.errors.add_error(str(component["key"]) + " is geen multipleOf 2.")

    def __validate_min_items(self, component: dict, value):
        """Validation for min items"""
        if component["validate"]["minItems"] and len(value) < int(
            component["validate"]["maxItems"]
        ):
            self.errors.add_error(
                str(component["key"])
                + " heeft te wijnig items, "
                + str(component["key"])
                + " heeft een minimum van "
                + str(component["validate"]["minItems"])
                + " items."
            )

    def __validate_max_items(self, component: dict, value):
        """Validation for max items"""
        if component["validate"]["maxItems"] and len(value) > int(
            component["validate"]["minItems"]
        ):
            self.errors.add_error(
                str(component["key"])
                + " heeft te veel items, "
                + str(component["key"])
                + " heeft een maximum van "
                + str(component["validate"]["maxItems"])
                + " items."
            )

    def __validate_unique_items(self, component: dict, value):
        """Validation for unique items"""
        if isinstance(value, list):
            if len(set(value)) != len(value):
                self.errors.add_error(
                    str(component["key"]) + f" {str(value)} de items zijn niet uniek."
                )

    def __validate_uuid(self, component: dict, value):
        """Validation for component type UUID"""
        regex_result = re.match(
            r"^([0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12})$",  # noqa: E501
            value,
        )
        if not regex_result:
            self.errors.add_error(str(component["key"]) + f": {str(value)} is geen geldige UUID.")

    def __validate_month(self, component: dict, value):
        """Validation for component type month"""
        # checks if the value as an int is not between 1 and 12
        if int(value) < 1 or int(value) > 12:
            self.errors.add_error(str(component["key"]) + f": {str(value)} is geen geldige maand.")

    def __validate_week(self, component: dict, value):
        """Validation for component type week"""
        # checks if the value as an int is not between 1 and 52
        if int(value) < 1 or int(value) > 52:
            self.errors.add_error(str(component["key"]) + f": {str(value)} is geen geldige week.")

    def __validate_money(self, component: dict, value):
        """Validation for component type money. expect; ISO CODE | spatie | Integer"""  # noqa: E501
        string = (str(value)).split(" ")
        if len(string) == 2 and string[1].isnumeric():
            try:
                # babel.numbers.format_currency does not actually \
                # ever raise any exceptions !
                babel.numbers.format_currency(Decimal(string[1]), string[0])
                return
            except DecimalException:
                self.errors.add_error(
                    str(component["key"]) + f": {str(value)} is geen geldige currency, "
                    f"ISO CODE | spatie | Integer."
                )
        self.errors.add_error(
            str(component["key"]) + f": {str(value)} is geen geldige currency, "
            f"ISO CODE | spatie | Integer."
        )

    def __validate_attachment(self, component: dict, value: Union[list, Any]):
        """Validation for component type file"""
        if isinstance(value, list):
            for file in value:
                self.__validate_file(component, file)
        else:
            self.errors.add_error(
                vsc.ERROR_MESSAGE_ATTACHMENT_VALUE_NOT_VALID.format(
                    component["key"], value, vsc.get_type_name(value)
                )
            )

    def __validate_file(self, component: dict, file: Union[dict, Any]):
        """Validation for file"""
        if not isinstance(file, dict):
            self.errors.add_error(
                vsc.ERROR_MESSAGE_ATTACHMENT_VALUE_NOT_DICT.format(
                    component["key"], file, vsc.get_type_name(file)
                )
            )
            return

        # check dict for keys & values
        keys = ["storage", "name", "url", "size", "type", "originalName"]
        supported_storage_types = ["base64", "url"]
        if all(x in file for x in keys):
            error_amount = self.errors.error_amount

            # check for valid storage types
            storage_type = get_storage_type(file)
            if storage_type not in supported_storage_types:
                self.errors.add_error(
                    vsc.ERROR_MESSAGE_FILE_STORAGE_TYPE_NOT_VALID.format(
                        component["key"], storage_type
                    )
                )

            # for loop file key: str, value, storage_type: str)
            for key in file.keys():
                if storage_type in supported_storage_types:
                    self.__validate_attachment_type(
                        key=f'{str(component["key"])}: {key}',
                        value=file.get(key),
                        storage_type=storage_type,
                    )

            if self.errors.error_amount != error_amount:
                if file.get("url") and len(file["url"]) > 30:
                    file["url"] = f'{str(file["url"][0:30])}...'
                self.errors.add_error(
                    vsc.ERROR_MESSAGE_FILE_NOT_VALID.format(component["key"], file)
                )
        else:
            if file.get("url") and len(file["url"]) > 30:
                file["url"] = f'{str(file["url"][0:30])}...'
            self.errors.add_error(
                vsc.ERROR_MESSAGE_FILE_KEY_NOT_EXISTS.format(component["key"], file, keys)
            )

    def __validate_color(self, component: dict, value):
        """Validation for component type color. Is validated on hex color code"""  # noqa: E501
        regex_result = re.match(r"^#(?:[0-9a-fA-F]{1,2}){3}$", value)
        if not regex_result:
            self.errors.add_error(
                str(component["key"]) + f": {str(value)} is geen geldige hex kleur."
            )

    def __validate_float(self, component: dict, value):
        """Validation for component type float"""
        try:
            float(value)
        except ValueError:
            self.errors.add_error(str(component["key"]) + f": {str(value)} is geen geldige float.")

    def __validate_double(self, component: dict, value):
        """Validation for component type double"""
        string_number = (str(value)).split(".")
        if len(string_number) != 2:
            self.errors.add_error(
                str(component["key"]) + f": {str(value)} is geen geldige double."
            )

    def __validate_integer(self, component: dict, value):
        """Validation for component type integer"""
        try:
            int(value)
        except ValueError:
            self.errors.add_error(
                str(component["key"]) + f": {str(value)} is geen geldige integer."
            )

    def __validate_int32(self, component: dict, value):
        """Validation for component type int32"""
        # TODO CGA-1844
        # if not np.int32(value):
        #   self.errors.add_error(
        #        str(component["key"])
        #        + f": {str(value)} is geen geldige int32."
        #    )

    def __validate_int64(self, component: dict, value):
        """Validation for component type int64"""
        # TODO CGA-1844
        # if not np.int64(value):
        #    self.errors.add_error(
        #        str(component["key"])
        #        + f": {str(value)} is geen geldige int64."
        #    )

    def __validate_hostname(self, component: dict, value):
        """Validation for component type hostname"""
        if not validators.domain(value):
            self.errors.add_error(
                str(component["key"]) + f": {str(value)} is geen geldige hostname."
            )

    def __validate_ipv4(self, component: dict, value):
        """Validation for component type ipv4"""
        try:
            ipaddress.IPv4Address(value)
        except ValueError:
            self.errors.add_error(str(component["key"]) + f": {str(value)} is geen geldige ipv4.")

    def __validate_ipv6(self, component: dict, value):
        """Validation for component type ipv6"""
        try:
            ipaddress.IPv6Address(value)
        except ValueError:
            self.errors.add_error(str(component["key"]) + f": {str(value)} is geen geldige ipv6.")

    def __validate_byte(self, component: dict, value):
        """Validation for component type byte"""
        regex_result = re.match(r"^[0-1]{8}$", value)
        if not regex_result:
            self.errors.add_error(str(component["key"]) + f": {str(value)} is geen geldige byte.")

    def __validate_binary(self, component: dict, value):
        """Validation for component type binary"""
        regex_result = re.match(r"^([0-1]{8} ?)*$", value)
        if not regex_result or len(value.replace(" ", "")) % 8 != 0:
            self.errors.add_error(
                str(component["key"]) + f": {str(value)} is geen geldige binary."
            )

    def __validate_kenteken(self, component: dict, value):
        """Validation for component type kenteken -- numbers and letters"""
        regex_result = re.match(r"^[a-zA-Z0-9-]*$", value)
        if not regex_result:
            self.errors.add_error(
                str(component["key"]) + f": {str(value)} is geen geldige kenteken."
            )

    def __validate_bsn(self, component: dict, value):
        """Validation for component type bsn"""
        regex_result = re.match(r"^[0-9]{8,9}$", value)
        if not regex_result:
            self.errors.add_error(str(component["key"]) + f": {str(value)} is geen geldige bsn.")

    def __validate_kvk(self, component: dict, value):
        """Validation for component type kvk"""
        regex_result = re.match(r"^[0-9]{12}$", value)
        if not regex_result:
            self.errors.add_error(str(component["key"]) + f": {str(value)} is geen geldige kvk.")

    def __validate_rsin(self, component: dict, value):
        """Validation for component type rsin"""
        regex_result = re.match(r"^[A-Z]{2}[0-9]{9}[B][0-9]{2}$", value)
        if not regex_result:
            self.errors.add_error(str(component["key"]) + f": {str(value)} is geen geldige rsin.")

    def __validate_gemeente_code(self, component: dict, value):
        """Validation for component type gemeentecode"""
        regex_result = re.match(r"^[0-9]{1,4}$", value)
        if not regex_result:
            self.errors.add_error(
                str(component["key"]) + f": {str(value)} is geen geldige gemeentecode."
            )
