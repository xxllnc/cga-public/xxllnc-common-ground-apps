import boto3
from app import schemas
from app.core import config
from exxellence_shared_cga.core.db_session_handler import AppInstanceIdHandler
from loguru import logger
from typing import IO, Literal

client = boto3.client(
    "s3",
    endpoint_url=config.AWS_S3_ENDPOINT_URL,
    region_name=config.AWS_REGION_NAME,
    aws_access_key_id=config.AWS_ACCESS_KEY_ID,
    aws_secret_access_key=config.AWS_SECRET_ACCESS_KEY,
)

app_instance_id_handler = AppInstanceIdHandler(
    app_id=config.APP_ID,
    controlpanel_url=config.CONTROLPANEL_URL,
    eco_client_id=config.AUTH0_ECO_CLIENT_ID,
    eco_client_secret=config.AUTH0_ECO_CLIENT_SECRET,
    eco_audience=config.AUTH0_ECO_AUDIENCE,
    eco_domain=config.AUTH0_ECO_DOMAIN,
    redis_host=config.REDIS_HOST,
    redis_password=config.REDIS_PASSWORD,
)

bucket = config.BUCKET_NAME


def _get_key(organization_url: str, unique_filename: str) -> str:
    app_instance_id = app_instance_id_handler.get_app_instance_id(organization_url)
    return f"{app_instance_id}/{unique_filename}"


# Made this function to be able to mock it in pytest.
# Possible a bug in pytest requiring this construction?
def _upload_stream_to_s3(
    organization_url: str, unique_filename: str, stream: IO
) -> schemas.FileUploadResponse:

    key = _get_key(organization_url, unique_filename)

    stream.seek(0)
    data = stream.read()
    content_type = getattr(stream, "content_type", "")
    storage_class: Literal["REDUCED_REDUNDANCY", "STANDARD"] = (
        "REDUCED_REDUNDANCY" if getattr(stream, "reproducible", False) else "STANDARD"
    )

    response = client.put_object(
        Bucket=bucket, Key=key, Body=data, ContentType=content_type, StorageClass=storage_class
    )

    response_metadata = response.get("ResponseMetadata")
    if not response_metadata or response_metadata.get("HTTPStatusCode") != 200:
        logger.error(response_metadata)

    head_object_response = client.head_object(Bucket=bucket, Key=key)
    size = head_object_response.get("ContentLength", 0)

    base_url = f"{organization_url}{config.BASE_PATH_V1}/requests/file"

    return schemas.FileUploadResponse(url=f"{base_url}/{key}", size=size)


def upload_stream_to_s3(
    organization_url: str, unique_filename: str, stream: IO
) -> schemas.FileUploadResponse:

    # Call this function to be able to mock it in pytest
    return _upload_stream_to_s3(organization_url, unique_filename, stream)


def delete_object(organization_url: str, instance_id: str, unique_filename: str):

    if instance_id == app_instance_id_handler.get_app_instance_id(organization_url):
        client.delete_object(Bucket=bucket, Key=_get_key(organization_url, unique_filename))


def get_object(organization_url: str, instance_id: str, unique_filename: str):
    if instance_id == app_instance_id_handler.get_app_instance_id(organization_url):
        response = client.get_object(
            Bucket=bucket, Key=_get_key(organization_url, unique_filename)
        )

        return response["Body"]
