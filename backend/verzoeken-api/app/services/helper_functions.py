from requests import Response as RequestResponse
from typing import Any, Union


def get_storage_type(file: dict[str, Any]) -> Union[str, None]:
    return file.get("storage")


def get_config_item(form_resource: Union[bool, RequestResponse], key: str) -> Union[str, None]:
    if not isinstance(form_resource, RequestResponse):
        return None

    form_object = form_resource.json()
    if form_object and "config" in form_object and isinstance(form_object["config"], list):
        config_items: list = form_object["config"]

        item = _get_config_item_value(config_items, key)
        return item["value"] if item else None

    return None


def _get_config_item_value(config_items: list, item_name: str) -> Union[Any, None]:
    config_item = next(
        (item for item in config_items if item["key"] == item_name),
        None,
    )
    return config_item if config_item else None
