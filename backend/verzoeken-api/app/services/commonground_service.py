"""This commonground_service module is used to communicate with other
commonground components / api's"""

# TODO CGAT-127: refactor this service

import requests as apiRequests
from app.api.api_v1.routers.tests import CONTENT_TYPE_VALUE_APPLICATION_JSON
from app.core import config
from loguru import logger
from requests import Response
from typing import Final, Optional, Union
from urllib.parse import ParseResult, urlparse
from validators import url  # type: ignore

DEFAULT_REQUEST_HEADERS: Final = {
    "Authorization": "",
    "Content-Type": CONTENT_TYPE_VALUE_APPLICATION_JSON,
    "Content-Crs": "EPSG:4326",
}


def _validate_url(value: str) -> bool:
    """
    Return whether or not the given value is a valid URL. Return ``True`` if
    the value is a valid URL, otherwise return ``False``.

    :param value: The value to validate.
    :type value: Any
    :return: True if validation was successful, False otherwise.
    :rtype: bool
    """
    if not (value and not value.isspace()):
        return False

    if not url(value):  # type: ignore
        return False

    return True


def _validate_body(body: dict) -> bool:
    """
    Return whether or not the given value is a valid body. Return ``True`` if
    the value is set, otherwise return ``False``.

    :param body: The component contains the fieldkey the value belongs to.
    :type body: dict
    :return: True if validation was successful, False otherwise.
    :rtype: bool
    """
    return isinstance(body, dict)


def create_resource(url: str, body: dict) -> Union[bool, Response]:
    """
    Create api call (POST method) to a commonground notification component.

    :param url: URL for the create call.
    :type url: str
    :param body: A JSON serializable Python object to send in the body
                 of the Request.
    :type body: dict
    :return: False when no call is send, a Response otherwise.
    :rtype: bool or requests.Response
    """
    logger.info(f"Create api call to a commonground notification component url: {url}")

    if not _validate_url(url):
        logger.error("No create api call send, because url is not valid.")
        return False

    if not _validate_body(body):
        logger.error("No create api call send, because body is not valid.")
        return False

    logger.info("Send create api call to a commonground notification component.")

    return apiRequests.post(url=url, headers=DEFAULT_REQUEST_HEADERS, json=body, verify=True)


def create_multipart_upload(url: str, files: dict) -> Union[bool, Response]:
    """
    Create multipart upload api call (POST method) to a commonground component.

    :param url: URL for the multipart upload call.
    :type url: str
    :param files: Dictionary for multipart encoding upload.
    :type files: dict
    :return: False when no call is send, a Response otherwise.
    :rtype: bool or requests.Response
    """
    logger.info(f"Create multipart upload api call to a commonground component url: {url}")

    parsed_url: ParseResult = urlparse(url)
    request_headers: Final = {
        "Authorization": "",
        "origin": f"{parsed_url.scheme}://{parsed_url.netloc}",
    }

    if not _validate_url(url):
        logger.error("No multipart upload api api call send, because url is not valid.")
        return False

    if not _validate_body(files):
        logger.error("No multipart upload api api call send, because body is not valid.")
        return False

    logger.info("Send multipart upload apicall to a commonground component.")

    return apiRequests.post(
        url=url, files=files, headers=request_headers, verify=config.SSL_CERT_FILE
    )


def get_resource(url: str, organization_url: Optional[str] = None) -> Union[bool, Response]:
    """
    Get api call to a commonground component

    :param url: URL for the get call.
    :type url: str
    :return: False when no call is send, a Response otherwise.
    :rtype: bool or requests.Response
    """
    logger.info(f"Get api call to a commonground component url {url}.")

    if not _validate_url(url):
        logger.error("No get call send, because url is not valid.")
        return False

    organization_url = organization_url if organization_url is not None else ""
    headers = {**DEFAULT_REQUEST_HEADERS, "organization-url": organization_url}

    return apiRequests.get(url=url, headers=headers, data={}, verify=config.SSL_CERT_FILE)
