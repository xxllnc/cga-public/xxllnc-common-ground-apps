import pytest
from app.core import config
from app.services.notification_service import NotifyActions, notify
from typing import Final, Union

URL: Final = f"{config.NOTIFICATION_URL}"


# ---------------------------------------------
# Test the notify function
# ---------------------------------------------
@pytest.mark.parametrize(
    "action_input, expect_result",
    [
        ("", False),
        ("    ", False),
        ("create", False),
        ("CREATE", False),
        (NotifyActions.CREATE, True),
    ],
)
def test_notify(action_input: Union[NotifyActions, str], expect_result: bool):
    """Test the notify function."""

    result = notify(action_input, "1", {}, URL)  # type: ignore

    assert result is expect_result
