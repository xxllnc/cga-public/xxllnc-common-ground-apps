import pytest
from app.core import config
from app.services.commonground_service import create_resource, get_resource
from typing import Final

NOTIFICATION_URL: Final = f"{config.NOTIFICATION_URL}"


# ---------------------------------------------
# Test the create_resource function with
# a valid and invalid notification url.
# ---------------------------------------------
@pytest.mark.parametrize(
    "notification_url_input, expect_result",
    [
        ("", False),
        ("    ", False),
        ("blablabla", False),
        ("//", False),
        ("http:///a", False),
        ("127.0.0.1", False),
        (None, False),
        ("http://localhost", True),
        ("https://exxellence.vcap.me/verzoeken", True),
        ("https://5748cff6-31e1-47d3-83d7-c080f24d94fd.bus.koppel.app/webhook", True),
        ("https://b0e19bf3-8dbd-46d2-b487-346ee3c0a0ca.api-test.nl/api/v1/zaken", True),
        (
            "https://b0e19bf3-8dbd-46d2-b487-346ee3c0a0ca.api-test.nl/api/v1/zaken?identificatie=ZAAK-2022-0000000029",  # noqa: E501
            True,
        ),
    ],
)
def test_create_resource_validate_notification_url(
    notification_url_input: str, expect_result: bool
):
    result = create_resource(notification_url_input, {})
    assert result is expect_result


# ---------------------------------------------
# Test the create_resource function with
# a valid and invalid body.
# ---------------------------------------------
@pytest.mark.parametrize(
    "body_input, expect_result",
    [
        ("", False),
        ("    ", False),
        ("blablabla", False),
        ({}, True),
        ({"test": "test", "name": "naam"}, True),
    ],
)
def test_create_resource_validate_body(body_input: dict, expect_result: bool):
    result = create_resource("http://localhost", body_input)
    assert result is expect_result


# ---------------------------------------------
# Test the create_resource function with
# valid url and body
# ---------------------------------------------
def test_create_resource():
    result = create_resource("http://localhost", {})
    assert result is True


# ---------------------------------------------
# Test the get_resource function with
# a valid and invalid url.
# ---------------------------------------------
@pytest.mark.parametrize(
    "url_input, expect_result",
    [
        ("", False),
        ("    ", False),
        ("blablabla", False),
        ("//", False),
        ("http:///a", False),
        ("127.0.0.1", False),
        (None, False),
        ("http://localhost", True),
        ("https://exxellence.vcap.me/verzoeken", True),
        (
            "https://5748cff6-31e1-47d3-83d7-c080f24d94fd.bus.koppel.app/webhook",  # noqa: E501
            True,
        ),
    ],
)
def test_get_resource_validate_url(url_input: str, expect_result: bool):
    result = get_resource(url_input)
    assert result is expect_result


# ---------------------------------------------
# Test the get_resource function
# ---------------------------------------------
def test_get_resource():
    response = get_resource("http://localhost")
    assert response is True
