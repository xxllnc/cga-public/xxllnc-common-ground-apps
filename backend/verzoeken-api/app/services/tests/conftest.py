import pytest
from typing import Any, Optional, Text


@pytest.fixture(autouse=True)
def fixture_mock_commonground_service_post(monkeypatch):
    """Mock app.services.commonground_service.apiRequests.post
    for all tests."""

    # always return True
    def fake_request(
        url: Text,
        headers: Any,
        verify: Any,
        data: Optional[Any] = None,
        json: Optional[Any] = None,
        files: Optional[Any] = None,
    ):
        return True

    # Application of the monkeypatch to replace
    # app.services.commonground_service.apiRequests.post
    # with the behavior of fake_request defined above.
    monkeypatch.setattr(
        "app.services.commonground_service.apiRequests.post",
        fake_request,
    )


@pytest.fixture(autouse=True)
def fixture_mock_commonground_service_request_get(monkeypatch):
    """Mock app.services.commonground_service.apiRequests.get
    for all tests."""

    # always return True
    def fake_request(
        url: Text,
        headers: Any,
        verify: Any,
        data: Optional[Any] = None,
        json: Optional[Any] = None,
    ):
        return True

    # Application of the monkeypatch to replace
    # app.services.commonground_service.apiRequests.get
    # with the behavior of fake_request defined above.
    monkeypatch.setattr(
        "app.services.commonground_service.apiRequests.get",
        fake_request,
    )
