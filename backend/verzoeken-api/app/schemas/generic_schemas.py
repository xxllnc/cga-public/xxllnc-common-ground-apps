import uuid as py_uuid
from pydantic import BaseModel
from pydantic.fields import Field
from typing import Optional


class FilterableId(BaseModel):
    """FilterableId"""

    id: Optional[py_uuid.UUID]


class FileUploadResponse(BaseModel):
    url: str = Field(..., title="Link to file")
    size: int = Field(..., title="The filesize")
