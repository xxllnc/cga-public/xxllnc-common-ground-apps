"""Module for the request schema"""

from __future__ import annotations

import datetime
from app.db.models import StatusEnum
from app.schemas.generic_schemas import FilterableId
from app.schemas.to_optional import to_optional
from exxellence_shared_cga.core.types import SortOrder
from pydantic import BaseModel, Field
from typing import Optional
from uuid import UUID


class RequestBase(BaseModel):
    form_uuid: str = Field(..., title="The uuid of the form that is used")
    status: StatusEnum = Field(..., title="The status of the request")
    form_input: dict = Field(
        ...,
        title="formInput: Submitted data",
        description="The formInput contains everything the user " "has entered in the form",
    )

    class Config:
        allow_population_by_field_name = True
        fields = {"form_uuid": "formUuid", "form_input": "formInput"}


class Request(RequestBase):
    """Properties to return to client as response body"""

    id: UUID = Field(..., title="The uuid of the request")
    registration_date: datetime.date = Field(
        ...,
        title="The registrationDate of the request",
        alias="registrationDate",
    )

    class Config:
        orm_mode = True
        extra = "forbid"
        allow_population_by_field_name = True


class RequestUpdate(to_optional(RequestBase)):  # type: ignore
    """Make fields optional to support partial update (patch)"""

    pass


RequestUpdate.update_forward_refs()


class RequestFilterableFields(FilterableId):
    """Class for the fields that are filterable"""

    registration_date: Optional[str] = Field(alias="registrationDate")

    class Config:
        extra = "forbid"
        allow_population_by_field_name = True


class RequestSortableFields(BaseModel):
    """Class for the fields that are sortable"""

    id: Optional[SortOrder]
    registration_date: Optional[SortOrder] = Field(alias="registrationDate")
    status: Optional[SortOrder]

    class Config:
        allow_population_by_field_name = True
        extra = "forbid"
