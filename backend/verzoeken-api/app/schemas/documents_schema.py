import uuid as py_uuid
from pydantic import BaseModel, Field
from typing import Optional


class DocumentId(BaseModel):

    id: Optional[py_uuid.UUID] = Field(..., title="The uuid of the request")


class Document(BaseModel):
    request_id: Optional[int] = Field(None, title="Unique filename")
    name: Optional[str] = Field(None, title="Unique filename")
    original_name: Optional[str] = Field(None, title="Original filename")
    url: Optional[str] = Field(None, title="url to file")
    size: Optional[str] = Field(None, title="File size")
    type: Optional[str] = Field(None, title="File type")
    storage: Optional[str] = Field(None, title="storage. url or base64")

    class Config:
        allow_population_by_field_name = True
        fields = {"original_name": "originalName"}
