# generated by datamodel-codegen:
#   filename:  https://development.zaaksysteem.nl/api/v2/document/openapi/openapi30.json
#   timestamp: 2022-12-20T08:09:24+00:00

from __future__ import annotations

from datetime import date, datetime
from enum import Enum
from pydantic import BaseModel, Extra, Field
from typing import Any, Dict, List, Optional, Union
from uuid import UUID


class Integrity(Enum):
    field_0 = "0"
    field_1 = "1"


class ApiV2DocumentCreateDocumentPostRequest(BaseModel):
    document_file: bytes
    case_uuid: UUID
    document_uuid: Optional[UUID] = None
    directory_uuid: Optional[UUID] = None
    magic_string: Optional[List[str]] = Field(None, unique_items=True)
    replaces: Optional[UUID] = None


class ApiV2FileCreateFilePostRequest(BaseModel):
    file: bytes
    file_uuid: Optional[UUID] = None


class ApiV2DocumentCreateDocumentFromAttachmentPostRequest(BaseModel):
    class Config:
        extra = Extra.forbid

    attachment_uuid: UUID


class OutputFormat(Enum):
    """
    The output format of the document, original or pdf
    """

    original = "original"
    pdf = "pdf"


class ApiV2DocumentCreateDocumentFromMessagePostRequest(BaseModel):
    class Config:
        extra = Extra.forbid

    message_uuid: UUID = Field(..., description="The message UUID.")
    output_format: Optional[OutputFormat] = Field(
        None, description="The output format of the document, original or pdf"
    )


class Filter(BaseModel):
    relationships_parent_id: Optional[Union[UUID, str]] = Field(
        None, alias="relationships.parent.id"
    )
    fulltext: Optional[str] = None


class ApiV2DocumentCreateDocumentFromFilePostRequest(BaseModel):
    class Config:
        extra = Extra.forbid

    file_uuid: UUID = Field(
        ..., description="UUID of the filestore to which the document is linked."
    )
    case_uuid: Optional[UUID] = Field(
        None, description="UUID of the case to which the document is linked."
    )
    skip_intake: Optional[bool] = Field(
        None,
        description="Indicates whether the generated document should show up on the intake",
    )
    document_uuid: Optional[UUID] = Field(None, description="UUID of the document to be created")
    directory_uuid: Optional[UUID] = Field(
        None, description="UUID of the directory in which the document is created."
    )
    magic_string: Optional[List[str]] = Field(
        None, description="Magic strings associated with document."
    )


class Sort(Enum):
    attributes_name = "attributes.name"
    attributes_extension = "attributes.extension"
    attributes_description = "attributes.description"
    attributes_last_modified_date_time = "attributes.last_modified_date_time"
    _attributes_name = "-attributes.name"
    _attributes_extension = "-attributes.extension"
    _attributes_description = "-attributes.description"
    _attributes_last_modified_date_time = "-attributes.last_modified_date_time"


class ApiV2DocumentAddDocumentToCasePostRequest(BaseModel):
    class Config:
        extra = Extra.forbid

    document_uuid: UUID = Field(..., title="Internal identifier of the document")
    case_uuid: UUID = Field(
        ..., title="Internal identifier the case to which the document is attached."
    )
    document_label_uuids: Optional[List[UUID]] = Field(
        [],
        title="List of internal identifiers of document_labels/magic_strings used to label the "
        + "document.",
    )


class Origin(Enum):
    Uitgaand = "Uitgaand"
    Intern = "Intern"
    Inkomend = "Inkomend"


class DocumentCategory(Enum):
    Aangifte = "Aangifte"
    Aanmaning = "Aanmaning"
    Aanmelding = "Aanmelding"
    Aanvraag = "Aanvraag"
    Advies = "Advies"
    Afbeelding = "Afbeelding"
    Afmelding = "Afmelding"
    Afspraak = "Afspraak"
    Agenda = "Agenda"
    Akte = "Akte"
    Bankgarantie = "Bankgarantie"
    Begroting = "Begroting"
    Bekendmaking = "Bekendmaking"
    Beleidsdocument = "Beleidsdocument"
    Benoeming = "Benoeming"
    Berekening = "Berekening"
    Beroepschrift = "Beroepschrift"
    Beschikking = "Beschikking"
    Besluit = "Besluit"
    Besluitenlijst = "Besluitenlijst"
    Bestek = "Bestek"
    Bestemmingsplan = "Bestemmingsplan"
    Betaalafspraak = "Betaalafspraak"
    Betalingsherinnering = "Betalingsherinnering"
    Bevestiging = "Bevestiging"
    Bezwaarschrift = "Bezwaarschrift"
    Brochure = "Brochure"
    Catalogus = "Catalogus"
    Checklist = "Checklist"
    Circulaire = "Circulaire"
    Declaratie = "Declaratie"
    Dwangbevel = "Dwangbevel"
    Factuur = "Factuur"
    Film = "Film"
    Foto = "Foto"
    Garantiebewijs = "Garantiebewijs"
    Geluidsfragment = "Geluidsfragment"
    Gespreksverslag = "Gespreksverslag"
    Gids = "Gids"
    Grafiek = "Grafiek"
    Herinnering = "Herinnering"
    Identificatiebewijs = "Identificatiebewijs"
    Kaart = "Kaart"
    Kennisgeving = "Kennisgeving"
    Klacht = "Klacht"
    Lastgeving = "Lastgeving"
    Mededeling = "Mededeling"
    Melding = "Melding"
    Norm = "Norm"
    Nota = "Nota"
    Notitie = "Notitie"
    Offerte = "Offerte"
    Ontvangstbevestiging = "Ontvangstbevestiging"
    Ontwerp = "Ontwerp"
    Opdracht = "Opdracht"
    Overeenkomst = "Overeenkomst"
    Pakket_Van_Eisen = "Pakket Van Eisen"
    Persbericht = "Persbericht"
    Plan = "Plan"
    Plan_Van_Aanpak = "Plan Van Aanpak"
    Polis = "Polis"
    Procesbeschrijving = "Procesbeschrijving"
    Proces_verbaal = "Proces-verbaal"
    Rapport = "Rapport"
    Regeling = "Regeling"
    Register = "Register"
    Rooster = "Rooster"
    Ruimtelijk_plan = "Ruimtelijk plan"
    Sollicitatiebrief = "Sollicitatiebrief"
    Statistische_opgave = "Statistische opgave"
    Taxatierapport = "Taxatierapport"
    Technische_tekening = "Technische tekening"
    Tekening = "Tekening"
    Uitnodiging = "Uitnodiging"
    Uitspraak = "Uitspraak"
    Uittreksel = "Uittreksel"
    Vergaderverslag = "Vergaderverslag"
    Vergunning = "Vergunning"
    Verklaring = "Verklaring"
    Verordening = "Verordening"
    Verslag = "Verslag"
    Verslag_van_bevindingen = "Verslag van bevindingen"
    Verspreidingslijst = "Verspreidingslijst"
    Verweerschrift = "Verweerschrift"
    Verzoek = "Verzoek"
    Verzoekschrift = "Verzoekschrift"
    Voordracht = "Voordracht"
    Voorschrift = "Voorschrift"
    Voorstel = "Voorstel"
    Wet = "Wet"
    Zienswijze = "Zienswijze"


class Meta(BaseModel):
    last_modified_datetime: Optional[datetime] = None
    created_datetime: Optional[datetime] = None
    document_number: Optional[int] = None


class Data(BaseModel):
    id: Optional[UUID] = None
    type: Optional[str] = Field(None, example="case")


class Meta1(BaseModel):
    display_number: Optional[float] = Field(None, example=1740)


class Case(BaseModel):
    """
    Case this document is related to
    """

    data: Optional[Data] = None
    meta: Optional[Meta1] = None


class Data1(BaseModel):
    id: UUID
    type: str = Field(..., example="directory")


class Directory(BaseModel):
    """
    Directory this document is part of (map / folder / directory)
    """

    data: Data1


class Data2(BaseModel):
    id: UUID
    type: str = Field(..., example="subject")


class Meta2(BaseModel):
    display_name: Optional[str] = Field(None, example="Admin")


class ModifiedBy(BaseModel):
    """
    Subject making the last change to this document
    """

    data: Optional[Data2] = None
    meta: Optional[Meta2] = None


class Data3(Data2):
    pass


class Meta3(Meta2):
    pass


class LockedBy(BaseModel):
    """
    Subject making the last change to this document
    """

    data: Data3
    meta: Optional[Meta3] = None


class Data4(Data2):
    pass


class Meta4(Meta2):
    pass


class CreatedBy(BaseModel):
    """
    Subject created this document
    """

    data: Data4
    meta: Optional[Meta4] = None


class Data5(BaseModel):
    id: UUID
    type: str = Field(..., example="file")


class File(BaseModel):
    """
    File this document is related to
    """

    data: Data5


class Relationships(BaseModel):
    class Config:
        extra = Extra.forbid

    case: Optional[Case] = Field(None, description="Case this document is related to")
    directory: Optional[Directory] = Field(
        None,
        description="Directory this document is part of (map / folder / directory)",
    )
    modified_by: Optional[ModifiedBy] = Field(
        None, description="Subject making the last change to this document"
    )
    locked_by: Optional[LockedBy] = Field(
        None, description="Subject making the last change to this document"
    )
    created_by: CreatedBy = Field(..., description="Subject created this document")
    file: File = Field(..., description="File this document is related to")


class Type(Enum):
    danslist2018 = "danslist2018"


class Archive(BaseModel):
    """
    contains the archive properties of the file.
    """

    type: Optional[Type] = None
    is_archivable: Optional[bool] = None


class VirusStatus(Enum):
    pending = "pending"
    ok = "ok"


class Security(BaseModel):
    """
    contains the security properties of the file.
    """

    virus_status: Optional[VirusStatus] = None


class State(Enum):
    pending = "pending"
    active = "active"
    rejected = "rejected"
    deleted = "deleted"


class PendingType(Enum):
    filename_exists = "filename_exists"
    no_permission = "no_permission"


class Status(BaseModel):
    """
    Status of the document
    """

    state: Optional[State] = None
    pending_type: Optional[PendingType] = None
    reason: Optional[str] = Field(None, description="In case of rejected, this is the reason why")


class Lock(BaseModel):
    since: Optional[datetime] = Field(None, description="(e.g. locked) since")


class Publish(BaseModel):
    status: Optional[bool] = Field(None, example=True)
    destinations: Optional[List[str]] = None


class Meta5(BaseModel):
    last_modified_datetime: Optional[datetime] = None
    created_datetime: Optional[datetime] = None
    summary: Optional[str] = Field(None, example="parkingpermit.pdf")
    table_id: Optional[int] = Field(None, example=1234)


class Relationships1(BaseModel):
    document: Optional[Dict[str, Any]] = Field(
        None, description="Document this storage item links to, if available"
    )
    thumbnail: Optional[Dict[str, Any]] = Field(None, description="Related thumbnail")
    created_by: Optional[Dict[str, Any]] = Field(
        None, description="Subject created this entry in the file"
    )


class Type1(Enum):
    md5 = "md5"


class Hash(BaseModel):
    type: Optional[Type1] = None
    value: Optional[str] = Field(None, example="9b36b2e89df94bc458d629499d38cf86")


class Archive1(Archive):
    pass


class Security1(Security):
    pass


class Attributes1(BaseModel):
    name: Optional[str] = Field(
        None,
        description="Fullname of the file with extension",
        example="parkingpermit.pdf",
    )
    basename: Optional[str] = Field(
        None, description="Name without extension", example="parkingpermit"
    )
    extension: Optional[Any] = Field(None, description="Extension of this file", example="pdf")
    hash: Optional[Hash] = None
    archive: Optional[Archive1] = None
    security: Optional[Security1] = None
    filesize: Optional[float] = None
    mimetype: Optional[str] = None


class FileEntity(BaseModel):
    type: Optional[str] = Field(None, example="file")
    id: Optional[UUID] = None
    meta: Optional[Meta5] = None
    relationships: Optional[Relationships1] = None
    attributes: Optional[Attributes1] = None


class Download(BaseModel):
    class Config:
        extra = Extra.forbid

    href: str = Field(
        ...,
        description="Download link for document.",
        example="/api/v2/document/download_document?id=79e15217-760c-4e15-8123-414965798d60",
    )


class Meta6(BaseModel):
    content_type: Optional[str] = Field(None, alias="content-type", example="application/pdf")


class Preview(BaseModel):
    class Config:
        extra = Extra.forbid

    href: str = Field(
        ...,
        description="preview link for document.",
        example="/api/v2/document/preview_document?id=79e15217-760c-4e15-8123-414965798d60",
    )
    meta: Meta6


class Meta7(BaseModel):
    content_type: Optional[str] = Field(None, alias="content-type", example="image/jpeg")


class Thumbnail(BaseModel):
    class Config:
        extra = Extra.forbid

    href: str = Field(
        ...,
        description="thumbnail link for document.",
        example="/api/v2/document/thumbnail_document?id=79e15217-760c-4e15-8123-414965798d60",
    )
    meta: Meta7


class Links(BaseModel):
    class Config:
        extra = Extra.forbid

    download: Optional[Download] = None
    preview: Optional[Preview] = None
    thumbnail: Optional[Thumbnail] = None


class EntryType(Enum):
    """
    Type of document entry
    """

    document = "document"
    directory = "directory"


class Employee(BaseModel):
    name: Optional[str] = Field(None, description="Assigned employee name", example="Ina Taker")


class Role(BaseModel):
    name: Optional[str] = Field(None, description="Assigned role name", example="Document Intaker")


class Department(BaseModel):
    name: Optional[str] = Field(None, description="Assigned department name", example="Backoffice")


class Assignment(BaseModel):
    employee: Optional[Employee] = None
    role: Optional[Role] = None
    department: Optional[Department] = None


class Attributes2(BaseModel):
    class Config:
        extra = Extra.forbid

    name: str = Field(
        ...,
        description="Name of the document_entry. For type document, it is the basename(name "
        + "without extension).",
        example="parkingpermit.pdf or Niew_map",
    )
    entry_type: EntryType = Field(..., description="Type of document entry", example="directory")
    description: str = Field(
        ..., description="Description about directory entry. Valid only for document."
    )
    extension: str = Field(
        ...,
        description="Extension of directory_entry. Valid only for document.",
        example="pdf",
    )
    mimetype: str = Field(
        ...,
        description="Mimetype of directory_entry. Valid only for document",
        example="application/pdf",
    )
    accepted: bool = Field(
        ..., description="Accepted status of directory_entry. Valid only for document."
    )
    document_number: Optional[int] = Field(
        None,
        description="Table id for document in file table. Valid only for document.",
    )
    last_modified_date_time: datetime = Field(
        ..., description="Last modified date time. Valid only for document."
    )
    rejection_reason: str = Field(
        ...,
        description="Optional rejection reason for document after case assignment",
        example="Document is missing vital information.",
    )
    rejected_by_display_name: str = Field(
        ...,
        description="Optional display name of person who gave a rejection_reason",
        example="Ad Min",
    )
    assignment: Assignment


class Data6(BaseModel):
    id: Optional[UUID] = Field(None, example="b48bd549-d7c3-44cb-a02e-8e633e684360")
    type: Optional[str] = Field(None, example="directory")


class Directory1(BaseModel):
    """
    Reference to the directory object when directory_entry is of type directory.
    """

    data: Data6


class Data7(BaseModel):
    id: Optional[UUID] = Field(None, example="b48bd549-d7c3-44cb-a02e-8e633e684360")
    type: Optional[str] = Field(None, example="document")


class Document(BaseModel):
    """
    Reference to the document object when directory_entry is of type document.
    """

    data: Data7


class Data8(BaseModel):
    id: Optional[UUID] = Field(None, example="b48bd549-d7c3-44cb-a02e-8e633e684360")
    type: Optional[str] = Field(None, example="case")


class Meta8(Meta1):
    pass


class Case1(BaseModel):
    """
    Case this directory_entry is related to
    """

    data: Data8
    meta: Meta8


class Data9(Data6):
    pass


class Meta9(BaseModel):
    display_name: Optional[str] = Field(None, example="Niew map 1")


class Parent(BaseModel):
    """
    Parent directory this document_entry.
    """

    data: Data9
    meta: Meta9


class Data10(BaseModel):
    id: Optional[UUID] = Field(None, example="b48bd549-d7c3-44cb-a02e-8e633e684360")
    type: Optional[str] = Field(None, example="subject")


class Meta10(Meta2):
    pass


class ModifiedBy1(BaseModel):
    """
    Subject making the last change to this document
    """

    data: Data10
    meta: Meta10


class Relationships2(BaseModel):
    class Config:
        extra = Extra.forbid

    directory: Optional[Directory1] = Field(
        None,
        description="Reference to the directory object when directory_entry is of type directory.",
    )
    document: Optional[Document] = Field(
        None,
        description="Reference to the document object when directory_entry is of type document.",
    )
    case: Optional[Case1] = Field(None, description="Case this directory_entry is related to")
    parent: Optional[Parent] = Field(None, description="Parent directory this document_entry.")
    modified_by: Optional[ModifiedBy1] = Field(
        None, description="Subject making the last change to this document"
    )


class DirectoryEntryEntity(BaseModel):
    class Config:
        extra = Extra.forbid

    links: Links
    type: str = Field(..., example="directory_entry")
    id: UUID
    attributes: Attributes2
    relationships: Relationships2


class Attributes3(BaseModel):
    name: str


class Data11(Data6):
    pass


class Parent1(BaseModel):
    """
    Parent directory.
    """

    data: Optional[Data11] = None


class Relationships3(BaseModel):
    class Config:
        extra = Extra.forbid

    parent: Optional[Parent1] = Field(None, description="Parent directory.")


class DirectoryEntity(BaseModel):
    class Config:
        extra = Extra.forbid

    type: str = Field(..., example="directory")
    id: UUID
    attributes: Attributes3
    relationships: Relationships3


class Attributes4(Attributes3):
    pass


class DocumentLabelEntity(BaseModel):
    class Config:
        extra = Extra.forbid

    type: str = Field(..., example="document_label")
    id: UUID
    attributes: Attributes4


class DocumentConfidentiality(Enum):
    Openbaar = "Openbaar"
    Beperkt_openbaar = "Beperkt openbaar"
    Intern = "Intern"
    Zaakvertrouwelijk = "Zaakvertrouwelijk"
    Vertrouwelijk = "Vertrouwelijk"
    Confidentieel = "Confidentieel"
    Geheim = "Geheim"
    Zeer_geheim = "Zeer geheim"


class ApiV2DocumentDeleteDocumentPostRequest(BaseModel):
    class Config:
        extra = Extra.forbid

    document_uuid: UUID = Field(..., title="Internal identifier of the document")
    reason: Optional[str] = Field(None, title="Optional human readable reason for deletion")


class ApiV2DocumentRejectAssignedDocumentPostRequest(BaseModel):
    class Config:
        extra = Extra.forbid

    document_uuid: UUID = Field(..., title="Internal identifier of the document")
    rejection_reason: str = Field(..., title="User-specified reason for rejecting the document")


class ApiV2DocumentAssignDocumentToUserPostRequest(BaseModel):
    class Config:
        extra = Extra.forbid

    document_uuid: UUID = Field(..., title="Internal identifier of the document")
    user_uuid: UUID = Field(..., title="Internal identifier of the user")


class ApiV2DocumentAssignDocumentToRolePostRequest(BaseModel):
    class Config:
        extra = Extra.forbid

    document_uuid: UUID = Field(..., title="Internal identifier of the document")
    group_uuid: UUID = Field(..., title="Internal identifier of the group")
    role_uuid: UUID = Field(..., title="Internal identifier of the role")


class EditorType(Enum):
    zoho = "zoho"
    msonline = "msonline"


class ApiV2DocumentUpdateDocumentFromExternalEditorPostRequest(BaseModel):
    document_uuid: str
    content: bytes
    zoho_authentication_token: str


class ApiV2DocumentCreateDirectoryPostRequest(BaseModel):
    case_uuid: UUID = Field(..., title="The Case UUID")
    name: str = Field(..., title="The name of the new directory.")
    parent_uuid: Optional[UUID] = Field(None, title="Optional UUID of the parent directory.")
    directory_uuid: Optional[UUID] = Field(
        None, title="Optional UUID of the directory to be created."
    )


class ApiV2DocumentApplyLabelsPostRequest(BaseModel):
    class Config:
        extra = Extra.forbid

    document_uuids: List[UUID] = Field(
        ...,
        description="UUID's of documents you want to apply labels.",
        unique_items=True,
    )
    document_label_uuids: List[UUID] = Field(
        ...,
        description="UUID's of document_labels(uuid of zaaktype_kenmerk).",
        unique_items=True,
    )


class ApiV2DocumentRemoveLabelsPostRequest(BaseModel):
    class Config:
        extra = Extra.forbid

    document_uuids: List[UUID] = Field(
        ..., description="UUID's of documents you want to remove labels."
    )
    document_label_uuids: List[UUID] = Field(
        ..., description="UUID's of document_labels(uuid of zaaktype_kenmerk)."
    )


class ApiV2DocumentMoveToDirectoryPostRequest(BaseModel):
    source_documents: Optional[List[UUID]] = Field(
        None, description="UUID's of documents to be moved."
    )
    source_directories: Optional[List[UUID]] = Field(
        None, description="UUID's of directories to be moved."
    )
    destination_directory_uuid: Optional[List[UUID]] = Field(
        None, description="UUID of destination directory"
    )


class ApiV2DocumentAcceptDocumentPostRequest(BaseModel):
    document_uuid: UUID


class ApiV2DocumentUpdateDocumentPostRequest(BaseModel):
    class Config:
        extra = Extra.forbid

    document_uuid: UUID = Field(..., title="Internal identifier of the document")
    basename: Optional[str] = Field(None, title="New basename for the document")
    description: Optional[str] = Field(None, title="New description for the document.")
    document_category: Optional[DocumentCategory] = None
    origin: Optional[Origin] = Field(None, title="New origin/direction for the document")
    origin_date: Optional[date] = Field(None, title="New origin date for the document.")
    confidentiality: Optional[DocumentConfidentiality] = None


class Metadata(BaseModel):
    status: Optional[str] = Field(None, example="original")
    origin: Optional[Origin] = None
    origin_date: Optional[date] = None
    appearance: Optional[str] = None
    confidentiality: Optional[DocumentConfidentiality] = None
    structure: Optional[str] = None
    document_category: Optional[DocumentCategory] = None
    pronom_format: Optional[str] = None
    description: Optional[str] = None


class Attributes(BaseModel):
    class Config:
        extra = Extra.forbid

    basename: Optional[str] = Field(
        None, description="Basename of the file.", example="parkingpermit"
    )
    extension: Optional[str] = Field(None, description="Extension of the file.", example=".pdf")
    name: str = Field(
        ...,
        description="Fullname of the file with extension",
        example="parkingpermit.pdf",
    )
    mimetype: str = Field(..., description="Mime type of the file", example="application/pdf")
    filesize: int = Field(..., description="Size of the file", example=1234)
    md5: str = Field(..., description="md5 of the file", example="4a1f7892d2bde95g6y1ea428b99f955")
    archive: Archive = Field(..., description="contains the archive properties of the file.")
    security: Security = Field(..., description="contains the security properties of the file.")
    accepted: bool = Field(..., description="Accepted status of the document")
    current_version: Optional[int] = Field(
        None, description="Version number, an increasing value of 1-9"
    )
    status: Optional[Status] = Field(None, description="Status of the document")
    lock: Optional[Lock] = None
    metadata: Optional[Metadata] = None
    publish: Optional[Publish] = None
    tags: Optional[List[str]] = Field(
        None, description="List of tags, in case of zaaksysteem: magic strings"
    )
    integrity_check_successful: Optional[bool] = Field(
        None,
        description="true if the integrity of file is verified, else false. Shown in response only"
        + "if query_parameter 'integrity' is set.",
    )


class DocumentEntity1(BaseModel):
    class Config:
        extra = Extra.forbid

    type: str = Field(..., example="document")
    id: UUID
    meta: Meta
    relationships: Relationships
    attributes: Attributes
