from __future__ import annotations

import uuid as py_uuid
from app.db.models import PaymentStatusEnum
from app.schemas.generic_schemas import FilterableId
from app.schemas.to_optional import to_optional
from datetime import date
from exxellence_shared_cga.core.types import SortOrder
from pydantic import BaseModel, Field
from typing import Optional


class StatusBase(BaseModel):

    payment_status: Optional[PaymentStatusEnum] = Field(
        ..., title="The payment status of the request", alias="paymentStatus"
    )
    request_status: Optional[str] = Field(
        ..., title="The status of the request", alias="requestStatus"
    )

    class Config:
        allow_population_by_field_name = True


class Status(StatusBase):

    payment_status_date: Optional[date] = Field(
        ..., title="Payment Date of the request", alias="paymentStatusDate"
    )
    request_status_date: Optional[date] = Field(
        ...,
        title="Date of the status of the request",
        alias="requestStatusDate",
    )

    class Config:
        orm_mode = True


class StatusRequestId(Status):

    request_id: py_uuid.UUID = Field(
        ...,
        title="The unique uuid of the request linked to the status",
        alias="requestId",
    )


class StatusDetails(StatusRequestId):

    uuid: py_uuid.UUID = Field(..., title="The unique uuid of the status", alias="id")

    class Config:
        orm_mode = True


class StatusCreateRequest(to_optional(StatusRequestId)):  # type: ignore
    pass


class StatusFilterableFields(FilterableId):

    payment_status: Optional[PaymentStatusEnum]
    payment_status_date: Optional[date]
    request_status_date: Optional[date]
    request_status: Optional[str]

    class Config:
        extra = "forbid"


class StatusSortableFields(BaseModel):

    uuid: Optional[SortOrder]
    payment_status: Optional[SortOrder]
    payment_status_date: Optional[SortOrder]
    request_status: Optional[SortOrder]
    request_status_date: Optional[SortOrder]

    class Config:
        extra = "forbid"
