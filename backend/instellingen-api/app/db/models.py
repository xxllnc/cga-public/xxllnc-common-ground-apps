import datetime
import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as pg
from .session import Base
from sqlalchemy.sql.sqltypes import DateTime

utcnow = sa.literal_column("TIMEZONE('utc', CURRENT_TIMESTAMP)")


class Setting(Base):
    __tablename__ = "setting"
    default_sort_field = "id"

    sid = sa.Column(sa.Integer, primary_key=True, index=True)
    id = sa.Column(
        sa.String,
        nullable=False,
        unique=True,
        index=True,
        server_default=sa.func.setting_generate_id(),
    )
    uuid = sa.Column(
        pg.UUID(as_uuid=True),
        nullable=False,
        unique=True,
        server_default=sa.func.gen_random_uuid(),
    )
    name = sa.Column(sa.String, index=True)
    value = sa.Column(sa.String)
    description = sa.Column(sa.String)
    regex = sa.Column(sa.String)
    last_modified = sa.Column(DateTime, default=datetime.datetime.utcnow)
    setting_type_id = sa.Column(
        sa.Integer,
        sa.ForeignKey("setting_type.sid"),
        index=True,
        nullable=False,
    )


class SettingType(Base):
    __tablename__ = "setting_type"
    default_sort_field = "name"

    sid = sa.Column(sa.Integer, primary_key=True, index=True)
    uuid = sa.Column(
        pg.UUID(as_uuid=True),
        nullable=False,
        unique=True,
        server_default=sa.func.gen_random_uuid(),
    )
    name = sa.Column(sa.String, index=True)
    description = sa.Column(sa.String)
