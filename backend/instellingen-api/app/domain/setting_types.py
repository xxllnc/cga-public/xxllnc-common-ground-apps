from app import schemas
from app.db import models
from exxellence_shared_cga.domain.base import CRUDBase


class CRUDSettingTypes(
    CRUDBase[
        models.SettingType,
        schemas.SettingTypeCreate,
        schemas.SettingTypeUpdate,
    ]
):
    pass


setting_types = CRUDSettingTypes(models.SettingType, models)
