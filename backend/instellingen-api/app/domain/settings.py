import sass
import sqlalchemy as sa
from app.db import models
from app.schemas import setting_schemas
from exxellence_shared_cga.domain.base import CRUDBase
from sqlalchemy.orm import Session


class CRUDSettings(
    CRUDBase[
        models.Setting,
        setting_schemas.SettingCreate,
        setting_schemas.SettingUpdate,
    ]
):
    def get_css(self, db: Session):

        query = sa.select(models.Setting).where(models.Setting.name == "cssOverride")
        result = db.execute(query).scalars().first()

        try:
            return sass.compile(string=result.value) if result.value else ""
        except sass.CompileError:
            return ""


settings = CRUDSettings(models.Setting, models)
