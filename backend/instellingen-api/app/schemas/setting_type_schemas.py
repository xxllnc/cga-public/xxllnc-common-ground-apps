from __future__ import annotations

import uuid as py_uuid
from app.schemas.generic_schemas import FilterableId
from app.schemas.to_optional import to_optional
from exxellence_shared_cga.core.types import SortOrder
from pydantic import BaseModel, Field
from typing import Optional


# Setting
class SettingTypeBase(BaseModel):
    name: str = Field(..., title="Name of the setting")
    description: Optional[str] = Field(..., title="Description of the setting")

    class Config:
        allow_population_by_field_name = True


class SettingType(SettingTypeBase):
    sid: int = Field(..., title="The unique id of the setting", alias="id")
    uuid: py_uuid.UUID = Field(..., title="The unique uuid of the setting")

    class Config:
        orm_mode = True
        allow_population_by_field_name = True
        extra = "allow"


class SettingTypeCreate(SettingTypeBase):

    pass


class SettingTypeUpdate(to_optional(SettingTypeCreate)):  # type: ignore

    pass


SettingTypeUpdate.update_forward_refs()


class SettingTypeFilterableFields(FilterableId):
    name: Optional[str]
    descriptoin: Optional[str]
    q: Optional[str]

    class Config:
        extra = "forbid"


class SettingTypeSortableFields(BaseModel):
    sid: Optional[SortOrder] = Field(alias="id")
    name: Optional[SortOrder]
    descriptoin: Optional[SortOrder]

    class Config:
        allow_population_by_field_name = True
        extra = "forbid"
