from __future__ import annotations

import uuid as py_uuid
from app.schemas.generic_schemas import FilterableId
from app.schemas.setting_type_schemas import SettingType
from app.schemas.to_optional import to_optional
from datetime import datetime
from exxellence_shared_cga.core.types import SortOrder
from pydantic import BaseModel, Field
from typing import Optional


# Setting
class SettingBase(BaseModel):
    name: str = Field(..., title="Name of the setting")
    value: str = Field(..., title="Value of the setting")
    description: Optional[str] = Field(..., title="Description of the setting")
    regex: Optional[str] = Field(..., title="Regex of the setting")

    setting_type_id: Optional[int] = Field(
        None, title="id of the type of the setting", alias="settingTypeId"
    )

    class Config:
        allow_population_by_field_name = True


class PublicSetting(SettingBase):
    id: str = Field(..., title="The unique id of the setting")
    uuid: py_uuid.UUID = Field(..., title="The unique uuid of the setting")

    class Config:
        orm_mode = True
        allow_population_by_field_name = True


class Setting(PublicSetting):
    last_modified: Optional[datetime] = Field(
        None, title="Date of last modification", alias="lastModified"
    )
    setting_type: Optional[SettingType] = Field(
        None, title="type of the setting", alias="settingType"
    )

    class Config:
        orm_mode = True
        allow_population_by_field_name = True
        extra = "allow"


class SettingCreate(SettingBase):

    pass


class SettingUpdate(to_optional(SettingCreate)):  # type: ignore

    pass


SettingUpdate.update_forward_refs()


class SettingPublicFilterableFields(FilterableId):
    name: Optional[str]

    class Config:
        extra = "forbid"


class SettingPublicSortableFields(BaseModel):
    id: Optional[SortOrder]
    name: Optional[SortOrder]

    class Config:
        extra = "forbid"


class SettingFilterableFields(SettingPublicFilterableFields):
    value: Optional[str]
    regex: Optional[str]
    description: Optional[str]
    q: Optional[str]

    class Config:
        extra = "forbid"


class SettingSortableFields(SettingPublicSortableFields):
    value: Optional[SortOrder]
    regex: Optional[SortOrder]
    SettingTypeCreate: Optional[SortOrder]
    last_modified: Optional[SortOrder]

    class Config:
        extra = "forbid"
