import uuid
from app import domain, schemas
from app.core.auth0 import get_user_scopes
from app.db.session import get_db
from exxellence_shared_cga.core.rest_helpers import SearchParameters
from exxellence_shared_cga.core.types import GenericId, IdType
from fastapi import APIRouter, Depends, Response
from sqlalchemy.orm import Session
from typing import List, Union

router = r = APIRouter()


def validate_search_parameters():
    return SearchParameters(
        filter_options_schema=schemas.SettingTypeFilterableFields,
        sort_options_schema=schemas.SettingTypeSortableFields,
    )


@r.get(
    "/settingTypes/{id}",
    response_model=schemas.SettingType,
    responses={404: {"description": "setting_type not found"}},
    dependencies=[Depends(get_user_scopes)],
)
def get_setting_type(id: Union[int, uuid.UUID], db=Depends(get_db)):
    "Retrieve a single custom field, given its unique id"
    id_type = IdType.uuid if isinstance(id, uuid.UUID) else IdType.sid
    setting_type = domain.setting_types.get(db=db, id=GenericId(id=id, type=id_type))

    return setting_type


# Get a list of settingss.
@r.get(
    "/settingTypes",
    response_model=List[schemas.SettingType],
    dependencies=[Depends(get_user_scopes)],
)
def get_settings(
    response: Response,
    database: Session = Depends(get_db),
    search_parameters: dict = Depends(validate_search_parameters()),
):

    search_result = domain.setting_types.get_multi(db=database, query=None, **search_parameters)
    response.headers["Content-Range"] = search_result.as_content_range("results")

    return search_result.result


@r.put(
    "/settingTypes/{id}",
    response_model=schemas.SettingType,
    responses={
        404: {"description": "setting_type not found"},
        409: {"description": "Conflict. setting_type not updated."},
    },
    dependencies=[Depends(get_user_scopes)],
)
def update_setting_type(
    id: Union[int, uuid.UUID],
    setting_type: schemas.SettingTypeUpdate,
    db=Depends(get_db),
):
    "Update an setting_type"
    id_type = IdType.uuid if isinstance(id, uuid.UUID) else IdType.sid
    return domain.setting_types.update(
        db=db,
        id=GenericId(id=id, type=id_type),
        obj_in=setting_type,
    )
