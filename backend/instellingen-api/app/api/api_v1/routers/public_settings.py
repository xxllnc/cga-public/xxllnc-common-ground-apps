from app import domain, schemas
from app.db.session import get_db, get_db_with_org_url_from_query_string
from exxellence_shared_cga.core.rest_helpers import SearchParameters
from fastapi import APIRouter, Depends, Response
from sqlalchemy.orm import Session
from typing import List

router = r = APIRouter()


class CssTextResponse(Response):
    media_type = "text/css"


def validate_search_parameters():
    return SearchParameters(
        filter_options_schema=schemas.SettingPublicFilterableFields,
        sort_options_schema=schemas.SettingPublicSortableFields,
    )


@r.get("/public/settings", response_model=List[schemas.PublicSetting])
def get_public_settings(
    response: Response,
    db: Session = Depends(get_db),
    search_parameters: dict = Depends(validate_search_parameters()),
):

    search_result = domain.settings.get_multi(db=db, query=None, **search_parameters)

    response.headers["Content-Range"] = search_result.as_content_range("results")

    return search_result.result


@r.get("/public/settings/css", response_class=CssTextResponse)
def get_css(db: Session = Depends(get_db_with_org_url_from_query_string)):

    return domain.settings.get_css(db)
