#!/usr/bin/env python3

import datetime
from app.db import models
from collections import namedtuple
from sqlalchemy.orm import Session

# Declaring namedtuple() as return value
MockData = namedtuple(
    "MockData",
    [
        "setting_type1",
        "setting_type2",
        "setting_type3",
        "setting1",
        "setting2",
    ],
)


#  Same data als initial_data.
def add_mock_data_to_db(db: Session) -> MockData:

    setting_type1 = models.SettingType(
        name="general settings",
        description="general settings",
    )

    setting_type2 = models.SettingType(
        name="form settings",
        description="settings for the forms",
    )

    setting_type3 = models.SettingType(
        name="not in use",
        description="This type is not in use",
    )
    db.add(setting_type1)
    db.add(setting_type2)
    db.add(setting_type3)
    db.flush()

    setting1 = models.Setting(
        name="organisatienaam",
        description="Naam van de organisatie",
        regex=".*",
        value="Xxllnc",
        last_modified=datetime.datetime(2021, 5, 11, 00, 1, 00),
        setting_type_id=setting_type1.sid,
    )
    setting2 = models.Setting(
        name="urlAfsluitenKnop",
        description="De standaard url die wordt gebruikt voor de "
        "afsluitknop van het formulier. Deze waarde kan in het formulier "
        "worden overschreven door in het formulier een instelling "
        "urlAfsluitenKnop toe te voegen",
        regex=".*",
        value="https://xxllnc.nl/",
        last_modified=datetime.datetime(2021, 5, 11, 00, 1, 00),
        setting_type_id=setting_type2.sid,
    )

    db.add(setting1)
    db.add(setting2)

    db.flush()

    return MockData(
        setting_type1,
        setting_type2,
        setting_type3,
        setting1,
        setting2,
    )
