from app.api.api_v1.routers.tests.test_mock_data_settings import add_mock_data_to_db
from app.core.config import BASE_PATH_V1
from app.db import models
from fastapi import status as http_status
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session
from typing import Final

# The api endpoint
url: Final = f"{BASE_PATH_V1}/settingTypes"


def _asser_setting_type(response: dict, expected: models.SettingType):

    assert response["id"] == expected.sid
    assert response["uuid"] == str(expected.uuid)
    assert response["name"] == expected.name
    assert response["description"] == expected.description


# ---------------------------------------------
# Testing an API with GET requests
# ---------------------------------------------
def test_get_settings_type_list(
    client: TestClient,
    test_db: Session,
):
    """Test get all the setting types"""
    add_mock_data_to_db(test_db)

    response = client.get(url)

    assert response.status_code == http_status.HTTP_200_OK
    headers = response.headers
    assert len(headers) == 3
    assert headers["content-type"] == "application/json"
    assert headers["content-range"] == "results 0-5/1"

    res = response.json()
    assert len(res) == 6


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# ---------------------------------------------
def test_put_method_setting_type(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    # Test update of a setting type
    body = {
        "name": "new name",
        "description": mock.setting_type1.description,
    }

    response = client.put(
        f"{url}/{mock.setting_type1.uuid}",
        json=body,
    )

    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 4
    assert res["name"] == "new name"


# ---------------------------------------------
# Get "/settingtypes/{id}"
# ---------------------------------------------
def test_get_setting_type_by_id(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    response = client.get(
        f"{url}/{mock.setting_type1.uuid}",
    )

    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 4

    # Check if id is generated.
    assert res["id"] is not None
    assert res["name"] == mock.setting_type1.name
    assert res["description"] == mock.setting_type1.description
