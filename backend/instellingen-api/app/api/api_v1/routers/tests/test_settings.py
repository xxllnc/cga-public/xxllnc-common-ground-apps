import pytest
from app.api.api_v1.routers.tests.test_mock_data_settings import add_mock_data_to_db
from app.core.config import BASE_PATH_V1
from fastapi import status as http_status
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session
from typing import Final

url: Final = f"{BASE_PATH_V1}/settings"


# ---------------------------------------------
# Testing an API with GET requests
# ---------------------------------------------
def test_get_settings_list(
    client: TestClient,
    test_db: Session,
):
    """Test get all the settings"""
    add_mock_data_to_db(test_db)

    response = client.get(url)
    assert response.status_code == http_status.HTTP_200_OK
    headers = response.headers
    assert len(headers) == 3
    assert headers["content-type"] == "application/json"
    assert headers["content-range"] == "results 0-6/7"

    res = response.json()
    assert len(res) == 7

    # Check if id is generated.
    assert len(res[0]) == 9
    assert res[0]["id"] is not None

    assert len(res[1]) == 9
    assert res[1]["id"] is not None


# ---------------------------------------------
# Get "/setting/{id}"
# ---------------------------------------------
def test_get_setting_by_id(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    for id in [mock.setting1.id, mock.setting1.uuid]:
        response = client.get(f"{url}/{id}")

        assert response.status_code == http_status.HTTP_200_OK
        res = response.json()
        assert len(res) == 9

        # Check if id is generated and two fields.
        assert res["id"] is not None
        assert res["name"] == mock.setting1.name
        assert res["description"] == mock.setting1.description


@pytest.mark.parametrize("id", ["1234", 1234, "39a0ca90-5ca8-11ec-bf63-0242ac130002"])
def test_get_setting_by__unknown_id(client: TestClient, test_db: Session, id):
    add_mock_data_to_db(test_db)

    response = client.get(f"{url}/{id}")

    assert response.status_code == http_status.HTTP_404_NOT_FOUND
