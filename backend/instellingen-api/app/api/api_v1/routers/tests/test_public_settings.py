import pytest
import sqlalchemy as sa
from app.core.config import BASE_PATH_V1
from app.db import models
from fastapi import status as http_status
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session
from typing import Final, Optional

url: Final = f"{BASE_PATH_V1}/public/settings"


def _assert_status_ok(setting: dict, expected):
    assert setting["uuid"] == str(expected.uuid)
    assert setting["id"] == expected.id
    assert setting["name"] == expected.name
    assert setting["value"] == expected.value
    assert setting["description"] == expected.description
    assert setting["regex"] == expected.regex
    assert setting["settingTypeId"] == expected.setting_type_id


# ---------------------------------------------
# Testing an API with GET requests
# ---------------------------------------------
def test_get_settings(
    client: TestClient,
    test_db: Session,
):
    """Test get all the settings"""
    settings = (
        test_db.execute(sa.select(models.Setting).order_by(models.Setting.id)).scalars().all()
    )

    response = client.get(url)

    assert response.status_code == http_status.HTTP_200_OK

    headers = response.headers
    assert len(headers) == 3
    assert headers["content-type"] == "application/json"
    assert headers["content-range"] == "results 0-4/5"

    res = response.json()
    assert len(res) == 5

    for idx, setting in enumerate(settings):
        _assert_status_ok(res[idx], setting)


# ---------------------------------------------
# Testing an API with GET requests
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameters, expected_order",
    [
        ("", [0, 1, 2, 3, 4]),
        ('?sort=["id","ASC"]', None),
        ('?sort=["name","ASC"]', [0, 1, 4, 3, 2]),
        ('?sort=["name","DESC"]', [2, 3, 4, 1, 0]),
        ("?filter={}", [0, 1, 2, 3, 4]),
        ('?filter={"name":"does_not_exist"}', []),
        ('?filter={"name":"cssOverride"}', [0]),
        ('?filter={"name":"organisatienaam"}', [1]),
        ('?filter={"name":"urlAfsluitenKnop"}', [2]),
    ],
)
def test_get_forms_list_with_query_parameters(
    client: TestClient,
    test_db: Session,
    query_parameters: str,
    expected_order: Optional[list[int]],
):

    response = client.get(f"{url}{query_parameters}")

    settings = (
        test_db.execute(sa.select(models.Setting).order_by(models.Setting.id)).scalars().all()
    )
    result_dict: dict = {}
    for idx, setting in enumerate(settings):
        result_dict[idx] = setting

    assert response.status_code == http_status.HTTP_200_OK

    res = response.json()
    expected_length = len(expected_order) if expected_order is not None else 5

    assert len(res) == expected_length

    if expected_order is not None:
        for idx, order_no in enumerate(expected_order):
            _assert_status_ok(res[idx], result_dict[order_no])


@pytest.mark.parametrize(
    "query_parameters",
    [
        '?sort=["value","ASC"]',
        '?sort=["regex","ASC"]',
        '?sort=["SettingTypeCreate","ASC"]',
        '?sort=["last_modified","ASC"]',
        '?sort=["unkownfield","DESC"]',
        '?sort=["_","ASC"]',
        f'?sort=[{None},"ASC"]',
        '?sort=["name","unkown"]',
        '?sort=["name","order"]',
        '?sort=["name","_"]',
        '?sort=["name","LEFT"]',
        '?sort=["name","asc"]',
        '?sort=["name","AsC"]',
        '?sort=["name","desc"]',
        '?sort=["name","DeSc"]',
        f'?sort=["name", {None}]',
        '?filter={"value":"something"}',
        '?filter={"regex":"something"}',
        '?filter={"description":"something"}',
        '?filter={"q":"question"}',
        '?filter={""}',
        '?filter={"aaaaaaaaaaaaa"}',
        '?filter={"aaaaaaaaaaaaa":}',
        "?filter={:}",
        '?filter={:""}',
    ],
)
def test_get_forms_list_with_invalid_query_parameters(
    client: TestClient,
    query_parameters: str,
):

    response = client.get(f"{url}{query_parameters}")
    assert response.status_code == http_status.HTTP_400_BAD_REQUEST


# ---------------------------------------------
# Testing get_css
# ---------------------------------------------
def test_get_css(client: TestClient):

    response = client.get(f"{url}/css?orgUrl=https://www.example.com")

    assert response.status_code == http_status.HTTP_200_OK

    headers = response.headers
    assert len(headers) == 2
    assert headers["content-type"] == "text/css; charset=utf-8"

    assert response.text is not None
