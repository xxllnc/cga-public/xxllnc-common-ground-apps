import uuid
from app.core.auth0 import get_user_scopes
from app.db.session import get_db
from app.domain import settings
from app.schemas import setting_schemas
from exxellence_shared_cga.core.rest_helpers import SearchParameters
from exxellence_shared_cga.core.types import GenericId, IdType
from fastapi import APIRouter, Depends, Response
from sqlalchemy.orm import Session
from typing import List, Union

router = r = APIRouter()


def validate_search_parameters():
    return SearchParameters(
        filter_options_schema=setting_schemas.SettingFilterableFields,
        sort_options_schema=setting_schemas.SettingSortableFields,
    )


@r.get(
    "/settings/{id}",
    response_model=setting_schemas.Setting,
    responses={404: {"description": "setting not found"}},
    dependencies=[Depends(get_user_scopes)],
)
def get_setting(id: Union[uuid.UUID, str], db=Depends(get_db)):
    "Retrieve a single custom field, given its unique id"
    id_type = IdType.uuid if isinstance(id, uuid.UUID) else IdType.id
    setting = settings.get(db=db, id=GenericId(id=id, type=id_type))

    return setting


# Get a list of settingss.
@r.get(
    "/settings",
    response_model=List[setting_schemas.Setting],
    dependencies=[Depends(get_user_scopes)],
)
def get_settings(
    response: Response,
    db: Session = Depends(get_db),
    search_parameters: dict = Depends(validate_search_parameters()),
):

    search_result = settings.get_multi(db=db, query=None, **search_parameters)

    response.headers["Content-Range"] = search_result.as_content_range("results")

    return search_result.result


@r.put(
    "/settings/{id}",
    response_model=setting_schemas.Setting,
    responses={
        404: {"description": "setting not found"},
        409: {"description": "Conflict. setting_type not updated."},
    },
    dependencies=[Depends(get_user_scopes)],
)
def update_setting(
    id: Union[uuid.UUID, str],
    setting: setting_schemas.SettingUpdate,
    db=Depends(get_db),
):
    "Update an setting_type"
    id_type = IdType.uuid if isinstance(id, uuid.UUID) else IdType.id
    return settings.update(db=db, id=GenericId(id=id, type=id_type), obj_in=setting)
