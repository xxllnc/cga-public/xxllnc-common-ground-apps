"""CSS setting

Revision ID: 1b1e1f5b62c2
Revises: e7e3cd09da52
Create Date: 2022-01-11 08:45:10.835745+01:00

"""
import datetime
import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as pg
from alembic import op
from sqlalchemy import orm, text
from sqlalchemy.orm import DeclarativeMeta, declarative_base
from sqlalchemy.sql.sqltypes import DateTime

# revision identifiers, used by Alembic.
revision = "1b1e1f5b62c2"
down_revision = "e7e3cd09da52"
branch_labels = None
depends_on = None

Base: DeclarativeMeta = declarative_base()


class Setting(Base):
    __tablename__ = "Setting"
    default_sort_field = "id"

    sid = sa.Column(sa.Integer, primary_key=True, index=True)
    id = sa.Column(
        sa.String,
        nullable=False,
        unique=True,
        index=True,
        server_default=sa.func.setting_generate_id(),
    )
    uuid = sa.Column(
        pg.UUID(as_uuid=True),
        nullable=False,
        unique=True,
        server_default=sa.func.gen_random_uuid(),
    )
    name = sa.Column(sa.String, index=True)
    value = sa.Column(sa.String)
    description = sa.Column(sa.String)
    regex = sa.Column(sa.String)
    last_modified = sa.Column(DateTime, default=datetime.datetime.utcnow)
    setting_type_id = sa.Column(
        sa.Integer,
        sa.ForeignKey("SettingType.sid"),
        index=True,
        nullable=False,
    )


class SettingType(Base):
    __tablename__ = "SettingType"
    default_sort_field = "name"

    sid = sa.Column(sa.Integer, primary_key=True, index=True)
    uuid = sa.Column(
        pg.UUID(as_uuid=True),
        nullable=False,
        unique=True,
        server_default=sa.func.gen_random_uuid(),
    )
    name = sa.Column(sa.String, index=True)
    description = sa.Column(sa.String)


def upgrade():

    bind = op.get_bind()
    session = orm.Session(bind=bind)

    session.execute(text('truncate table "Setting" cascade'))
    session.execute(text('truncate table "SettingType" cascade'))

    setting_type_general = SettingType(
        name="Algemene instellingen",
        description="Algemene instellingen",
    )

    setting_type_css = SettingType(
        name="CSS instellingen",
        description="CSS instellingen",
    )

    setting_type_form = SettingType(
        name="Formulier instellingen",
        description="Algemene instellingen voor formulieren",
    )

    session.add(setting_type_general)
    session.add(setting_type_css)
    session.add(setting_type_form)
    session.flush()
    session.commit()

    session.add(
        Setting(
            name="cssOverride",
            description="Specifieke css voor de organisatie",
            regex=".*",
            value="",
            setting_type_id=setting_type_css.sid,
        )
    )

    session.add(
        Setting(
            name="organisatienaam",
            description="Naam van de organisatie",
            regex=".*",
            value="Xxllnc",
            setting_type_id=setting_type_general.sid,
        )
    )

    session.add(
        Setting(
            name="urlAfsluitenKnop",
            description="De standaard url die wordt gebruikt voor de "
            "afsluitknop van het formulier. Deze waarde kan in het formulier "
            "worden overschreven door in het formulier een instelling "
            "urlAfsluitenKnop toe te voegen",
            regex=".*",
            value="https://xxllnc.nl/",
            setting_type_id=setting_type_form.sid,
        )
    )

    session.flush()
    session.commit()
