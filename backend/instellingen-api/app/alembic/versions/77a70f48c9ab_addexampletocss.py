"""AddExampleToCSS

Revision ID: 77a70f48c9ab
Revises: 1b1e1f5b62c2
Create Date: 2022-01-11 15:45:03.493138+01:00

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.orm import DeclarativeMeta, declarative_base

# revision identifiers, used by Alembic.
revision = "77a70f48c9ab"
down_revision = "1b1e1f5b62c2"
branch_labels = None
depends_on = None

Base: DeclarativeMeta = declarative_base()

css_example = """
$primary: #0f4958 !default;
$secondary: #fff !default;

.btn {
  background-color: $primary;
}

.btn-secondary {
  background-color: $secondary;
}
"""


class Setting(Base):
    __tablename__ = "Setting"

    sid = sa.Column(sa.Integer, primary_key=True, index=True)
    name = sa.Column(sa.String, index=True)
    value = sa.Column(sa.String)


def upgrade():
    connection = op.get_bind()
    connection.execute(
        sa.update(Setting).values(value=css_example).where(Setting.name == "cssOverride")
    )


def downgrade():
    connection = op.get_bind()
    connection.execute(sa.update(Setting).values(value="").where(Setting.name == "cssOverride"))
