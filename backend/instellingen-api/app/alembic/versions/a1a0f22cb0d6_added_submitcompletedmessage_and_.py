"""Added submitCompletedMessage and organizationId to settings

Revision ID: a1a0f22cb0d6
Revises: bc35a8e1afb8
Create Date: 2022-12-06 08:56:09.029954+01:00

"""
import datetime
import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as pg
from alembic import op
from sqlalchemy import orm
from sqlalchemy.orm import DeclarativeMeta, declarative_base
from sqlalchemy.sql.sqltypes import DateTime

# revision identifiers, used by Alembic.
revision = "a1a0f22cb0d6"
down_revision = "bc35a8e1afb8"
branch_labels = None
depends_on = None

Base: DeclarativeMeta = declarative_base()


class Setting(Base):
    __tablename__ = "setting"
    default_sort_field = "id"

    sid = sa.Column(sa.Integer, primary_key=True, index=True)
    id = sa.Column(
        sa.String,
        nullable=False,
        unique=True,
        index=True,
        server_default=sa.func.setting_generate_id(),
    )
    uuid = sa.Column(
        pg.UUID(as_uuid=True),
        nullable=False,
        unique=True,
        server_default=sa.func.gen_random_uuid(),
    )
    name = sa.Column(sa.String, index=True)
    value = sa.Column(sa.String)
    description = sa.Column(sa.String)
    regex = sa.Column(sa.String)
    last_modified = sa.Column(DateTime, default=datetime.datetime.utcnow)
    setting_type_id = sa.Column(
        sa.Integer,
        sa.ForeignKey("setting_type.sid"),
        index=True,
        nullable=False,
    )


class SettingType(Base):
    __tablename__ = "setting_type"
    default_sort_field = "name"

    sid = sa.Column(sa.Integer, primary_key=True, index=True)
    uuid = sa.Column(
        pg.UUID(as_uuid=True),
        nullable=False,
        unique=True,
        server_default=sa.func.gen_random_uuid(),
    )
    name = sa.Column(sa.String, index=True)
    description = sa.Column(sa.String)


def upgrade():
    bind = op.get_bind()
    session = orm.Session(bind=bind)

    setting_type_general: SettingType = (
        bind.execute(sa.select(SettingType).where(SettingType.name == "Algemene instellingen"))
        .scalars()
        .first()
    )

    setting_type_form: SettingType = (
        bind.execute(sa.select(SettingType).where(SettingType.name == "Formulier instellingen"))
        .scalars()
        .first()
    )

    session.add(
        Setting(
            name="submitCompletedMessage",
            description="Het standaard bericht die wordt gebruikt voor het "
            "submitbericht van het formulier. Deze waarde kan in het formulier "
            "worden overschreven door in het formulier een instelling "
            "submitCompletedMessage toe te voegen",
            regex=".*",
            value="",
            setting_type_id=setting_type_form,
        )
    )

    session.add(
        Setting(
            name="organization",
            description="Het organization id die wordt gebruikt bij DigiId",
            regex=".*",
            value="",
            setting_type_id=setting_type_general,
        )
    )

    session.flush()
    session.commit()


def downgrade():
    bind = op.get_bind()
    session = orm.Session(bind=bind)

    submitCompletedMessage: Setting = (
        bind.execute(sa.select(Setting).where(Setting.name == "submitCompletedMessage"))
        .scalars()
        .first()
    )

    organization: Setting = (
        bind.execute(sa.select(Setting).where(Setting.name == "organization")).scalars().first()
    )
    session.delete(submitCompletedMessage)
    session.delete(organization)
    session.flush()
    session.commit()
