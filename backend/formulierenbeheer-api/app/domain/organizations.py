"""Module for css CRUD calls"""

import sqlalchemy as sa
from app import schemas
from app.db import models
from app.resources import error_messages
from exxellence_shared_cga.core.types import (
    FilterOptions,
    ProcessError,
    RangeOptions,
    SearchResult,
    SortOptions,
    SortOrder,
    UnsignedInt,
)
from exxellence_shared_cga.domain.base import CRUDBase
from fastapi import status as http_status
from sqlalchemy.orm import Session


class CRUDStatus(
    CRUDBase[
        models.Organization,
        schemas.OrganizationCreate,
        schemas.OrganizationUpdate,
    ]
):
    def get_organization_by_url(self, url: str, db: Session) -> schemas.OrganizationId:

        query = sa.select(models.Organization).join(models.Url).where(models.Url.url == url)

        # execute the query
        organization = db.execute(query).scalars().first()

        if not organization:
            raise ProcessError(
                loc={"source": "path", "field": "url"},
                msg=error_messages.ORGANIZATION_NOT_FOUND_WITH.format(url),
                status_code=http_status.HTTP_404_NOT_FOUND,
            )
        return schemas.OrganizationId(id=organization.id)

    def get_organizations(
        self,
        database: Session,
        sort_options: (SortOptions | None),
        range_options: (RangeOptions | None),
        filter_options: (FilterOptions | None),
    ) -> SearchResult:

        query = sa.select(models.Organization)

        # Only two filters supported, so no generic function is used
        if filter_options:

            filter_on_name = filter_options.options.get("name")
            if filter_on_name:
                query = query.where(self.model.name == filter_on_name)

            filter_on_url = filter_options.options.get("url")
            if filter_on_url:
                query = query.where(models.Url.url == filter_on_url).join(models.Url)

        # Always sort on name, this is nessesary for lazy loading the urls
        if sort_options and sort_options.order == SortOrder.desc:
            query = query.order_by(models.Organization.name.desc())
        else:
            query = query.order_by(models.Organization.name.asc())

        # Add the range to the query
        start = UnsignedInt(0)
        end = UnsignedInt(99)
        if range_options:
            start = range_options.start
            end = range_options.end

        query = query.limit(end - start + 1).offset(start)

        # execute the query
        result = database.execute(query).unique().scalars().all()

        if not result:
            raise ProcessError(
                loc={"source": "path", "field": "id"},
                msg=error_messages.FORM_NOT_FOUND_WITH.format(id),
                status_code=http_status.HTTP_404_NOT_FOUND,
            )
        # get the total number of organizations
        total_results = (
            database.execute(
                sa.select(models.Organization)
                .with_only_columns(sa.func.count(sa.distinct(self.model.sid)))
                .order_by(None)
                .group_by(None)
            )
            .scalars()
            .one()
        )

        range_end = UnsignedInt(start + total_results - 1)

        return SearchResult(
            result=result,
            start=start,
            end=range_end if range_end >= UnsignedInt(0) else None,
            total=total_results,
        )


organizations = CRUDStatus(models.Organization, models)
