from app import schemas
from app.db import models
from app.domain.organizations import organizations
from exxellence_shared_cga.core.types import GenericId, IdType
from exxellence_shared_cga.domain.base import CRUDBase
from sqlalchemy.orm import Session
from typing import cast


class CRUDSettingTypes(
    CRUDBase[
        models.Url,
        schemas.UrlCreate,
        schemas.UrlUpdate,
    ]
):
    def create_url(
        self, db: Session, url_add_request: schemas.UrlCreateRequest
    ) -> schemas.UrlCreate:

        organization_id = GenericId(id=url_add_request.organization_id, type=IdType.id)
        organization = organizations.get(db=db, id=organization_id)

        url = schemas.UrlCreate(
            url=url_add_request.url,
            primary=url_add_request.primary,
            organizationId=cast(int, organization.sid),
        )
        return self.create(db=db, obj_in=url)


urls = CRUDSettingTypes(models.Url, models)
