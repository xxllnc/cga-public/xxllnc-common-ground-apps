import sqlalchemy as sa
from app.resources import error_messages
from exxellence_shared_cga.core.types import ProcessError
from fastapi import status as http_status


def handle_db_exception(exception: sa.exc.IntegrityError, create_or_update):
    loc = {"source": "body", "field": "name"}
    msg = f"Kan geen record {create_or_update}"
    if "duplicate key value violates unique constraint" in exception.args[0]:

        if "DETAIL:  Key (name, organization_sid)=" in exception.args[0]:

            loc["field"] = "name, organization_uuid"
            msg = (
                error_messages.FORM_COMBINATION_NAME_CREATOR_ORGANIZATION_UUID_ALREADY_EXIST_ERROR
            )

        if "DETAIL:  Key (slug, organization_sid)=" in exception.args[0]:

            loc["field"] = "slug, organization_uuid"
            msg = (
                error_messages.FORM_COMBINATION_SLUG_CREATOR_ORGANIZATION_UUID_ALREADY_EXIST_ERROR
            )

        raise ProcessError(loc, msg, http_status.HTTP_409_CONFLICT) from exception
