import sqlalchemy as sa
from app.core.auth0 import AccessUser
from app.db import models
from app.resources import error_messages
from exxellence_shared_cga.core.types import ProcessError
from fastapi import status as http_status
from sqlalchemy import func
from sqlalchemy.orm import Session, joinedload
from uuid import UUID


def _execute_query(database: Session, select_statement, id):

    select_statement = select_statement.options(  # type: ignore
        joinedload(models.FormManagement.form_details),
        joinedload(models.FormManagement.organization),
    )

    form_management = database.execute(select_statement).scalars().first()

    if not form_management:
        raise ProcessError(
            loc={"source": "path", "field": "id"},
            msg=error_messages.FORM_NOT_FOUND_WITH.format(id),
            status_code=http_status.HTTP_404_NOT_FOUND,
        )

    return form_management


def get_form_management_by_id(database: Session, id: UUID):

    select_statement = sa.select(models.FormManagement).where(models.FormManagement.id == id)

    return _execute_query(database, select_statement, id)


def get_form_management_by_slug(database: Session, slug: str, current_user: AccessUser):

    select_statement = (
        sa.select(models.FormManagement)
        .where(
            sa.and_(
                func.lower(models.FormManagement.slug) == func.lower(slug),
                models.Organization.id == current_user.organization_uuid,
            )
        )
        .join(models.Organization)
    )

    return _execute_query(database, select_statement, slug)
