import datetime
import sqlalchemy as sa
from .db_exception_and_error_handler import handle_db_exception
from .helper_functions import get_form_management_by_id
from app import schemas
from app.core.auth0 import AccessUser
from app.db import models
from app.db.models import Rights
from app.resources import error_messages
from exxellence_shared_cga.core.types import GenericId, IdType, ProcessError
from exxellence_shared_cga.domain.base import CRUDBase
from fastapi import status as http_status
from loguru import logger
from sqlalchemy.orm import Session
from typing import Union
from uuid import UUID


class CRUDSettingTypes(
    CRUDBase[
        models.FormManagement,
        schemas.FormAddShareRequest,
        schemas.FormUpdateShareRequest,
    ]
):
    def _convert_db_object_to_Form(self, db_form: models.FormManagement) -> schemas.Form:

        # First convert to dict and do it this way or else the test fails
        form = db_form.__dict__
        form["organization_uuid"] = db_form.organization.id
        form["organization_name"] = db_form.organization.name
        return schemas.Form(**form)

    def get_share_by_id(self, database: Session, id: UUID) -> schemas.Form:

        return self._convert_db_object_to_Form(get_form_management_by_id(database, id))

    def add_new_share(
        self,
        database: Session,
        add_share_request: schemas.FormAddShareRequest,
        current_user: AccessUser,
    ) -> schemas.Form:

        # Get the details of the form that is shared
        form_management = get_form_management_by_id(database, add_share_request.id)

        is_owner = (
            form_management.rights == models.Rights.OWNER
            and form_management.organization.id == current_user.organization_uuid
        )
        is_private = form_management.form_details.private is True
        if not is_owner and is_private:
            raise ProcessError(
                loc={"source": "path", "field": "id"},
                msg=error_messages.FORM_NOT_SHARED.format(form_management.id),
                status_code=http_status.HTTP_404_NOT_FOUND,
            )

        # get the organization to add to the form share
        organization: models.Organization = (
            database.execute(
                sa.select(models.Organization).where(
                    models.Organization.id == add_share_request.organization_uuid
                )
            )
            .scalars()
            .first()
        )

        if not organization:
            raise ProcessError(
                loc={"source": "path", "field": "id"},
                msg=error_messages.ORGANIZATION_NOT_FOUND_WITH.format(
                    form_management.organization.id
                ),
                status_code=http_status.HTTP_404_NOT_FOUND,
            )

        publish_date = add_share_request.publish_date
        status = add_share_request.status

        if status is None:
            status = models.Status.INACTIVE if publish_date is None else models.Status.ACTIVE

        if add_share_request.status == models.Status.ACTIVE and not publish_date:
            publish_date = datetime.datetime.today()  # type: ignore

        def _get_value(field: str):
            value_from_request = getattr(add_share_request, field)
            return value_from_request if value_from_request else getattr(form_management, field)

        add_form_management = self.model(
            name=_get_value("name"),
            slug=_get_value("slug"),
            config=add_share_request.config,
            publish_date=publish_date,
            status=status,
            rights=add_share_request.rights,
            form_details=form_management.form_details,
            organization=organization,
        )

        try:
            database.add(add_form_management)
            database.flush()
            database.commit()
        except sa.exc.IntegrityError as exception:

            logger.error(f"create_form exception: {exception}")
            handle_db_exception(exception, "aanmaken")

        return self._convert_db_object_to_Form(add_form_management)

    def _get_form_and_check_owner_or_is_own_form(
        self,
        db: Session,
        id: UUID,
        org_id: UUID,
    ):

        form_to_update = get_form_management_by_id(db, id)
        own_share = form_to_update.organization.id == org_id

        if not own_share:

            form_sid = form_to_update.form_details_sid
            is_owner = (
                sa.select(self.model)
                .where(
                    sa.and_(
                        self.model.form_details_sid == form_sid,
                        self.model.rights == models.Rights.OWNER,
                        models.Organization.id == org_id,
                    )
                )
                .join(models.Organization)
            )

            if not db.execute(is_owner).first():
                raise ProcessError(
                    loc={"source": "path", "field": "id"},
                    msg=error_messages.FORM_NOT_SHARED.format(id),
                    status_code=http_status.HTTP_404_NOT_FOUND,
                )
        return form_to_update

    def update_share(
        self,
        db: Session,
        id: UUID,
        update_request: schemas.FormUpdateShareRequest,
        current_user: AccessUser,
    ) -> Union[schemas.Form, None]:

        loc = {"source": "body"}
        msg = error_messages.FORM_NOT_SHARED.format(id)
        status_code = http_status.HTTP_422_UNPROCESSABLE_ENTITY

        form_to_update = self._get_form_and_check_owner_or_is_own_form(
            db, id, current_user.organization_uuid
        )

        own_form = form_to_update.organization.id == current_user.organization_uuid

        i_am_editing_my_own_rights = (
            own_form
            and update_request.rights.value != form_to_update.rights.value  # type: ignore # noqa: E501
            and form_to_update.rights != Rights.OWNER.value
        )

        if i_am_editing_my_own_rights:
            raise ProcessError(loc, msg, status_code)

        if not own_form:
            # Raise error when one of these fields are changed
            for field in ["name", "config", "slug", "status", "publish_date"]:

                value = getattr(update_request, field)
                not_None_or_empty = value is not None and value != []

                if not_None_or_empty and str(value) != str(getattr(form_to_update, field)):
                    raise ProcessError(loc, msg, status_code)

        updated_form = self.update(
            db=db,
            id=GenericId(id=id, type=IdType.id),
            obj_in=update_request,
            db_obj_to_change=form_to_update,
        )

        return self._convert_db_object_to_Form(updated_form)

    def delete_share(
        self,
        db: Session,
        id: UUID,
        current_user: AccessUser,
    ) -> Union[schemas.Form, None]:

        form_to_delete = self._get_form_and_check_owner_or_is_own_form(
            db, id, current_user.organization_uuid
        )

        self.delete(db=db, id=GenericId(id=id, type=IdType.id))

        return self._convert_db_object_to_Form(form_to_delete)


share_forms = CRUDSettingTypes(models.FormManagement, models)
