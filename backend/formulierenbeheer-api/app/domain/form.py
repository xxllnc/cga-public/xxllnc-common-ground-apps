import datetime
import sqlalchemy as sa
from .db_exception_and_error_handler import handle_db_exception
from .helper_functions import get_form_management_by_id, get_form_management_by_slug
from app import schemas
from app.core.auth0 import AccessUser
from app.db import models
from app.db.models import FormDetails, FormManagement, Organization, Rights, Status, Url
from app.resources import error_messages
from enum import Enum
from exxellence_shared_cga.core.types import (
    FilterOptions,
    GenericId,
    IdType,
    ProcessError,
    RangeOptions,
    SearchResult,
    SortOptions,
)
from exxellence_shared_cga.domain.base import CRUDBase
from fastapi import status as http_status
from loguru import logger
from slugify.main import UniqueSlugify  # type: ignore
from sqlalchemy import func
from sqlalchemy.orm import Session, aliased
from sqlalchemy.sql.expression import false
from typing import List, Literal, Optional, Union
from uuid import UUID


class FormAction(Enum):
    update = "update"
    delete = "delete"


class CRUDForm(
    CRUDBase[
        FormManagement,
        FormManagement,
        schemas.FormUpdateRequest,
    ]
):
    """Class for form CRUD calls"""

    def _convert_db_object_to_FormWithDetails(
        self, db_form: models.FormManagement
    ) -> schemas.FormWithDetails:

        # First convert to dict and do it this way or else the test fails
        form = db_form.__dict__
        form["form"] = db_form.form_details.form
        form["private"] = db_form.form_details.private
        form["author"] = db_form.form_details.author
        form["organization_uuid"] = db_form.organization.id
        form["organization_name"] = db_form.organization.name

        return schemas.FormWithDetails(**form)

    Field = Literal["id", "slug", "url_url", "org_uuid"]

    def _match(self, field: Field, value: Union[str, UUID]):
        fields = {
            "id": (self.model.id == value),
            "slug": (func.lower(self.model.slug) == func.lower(value)),
            "url_url": (Url.url == value),
            "org_uuid": (Organization.id == value),
        }
        return fields[field]

    def _select_base_query(
        self,
        organization_uuid: Optional[UUID],
        get_active_forms_on_org_url: Optional[str],
        extra_columns: List = [],
    ):

        if organization_uuid is None and get_active_forms_on_org_url is None:
            raise ProcessError(
                loc={
                    "source": "header",
                    "field": "organization_uuid or organization_url",
                },
                msg=error_messages.ORGANIZATION_NOT_FOUND_WITH.format(organization_uuid),
                status_code=http_status.HTTP_404_NOT_FOUND,
            )

        own_forms = (
            sa.select(self.model.form_details_sid)  # type: ignore
            .select_from(self.model)
            .where(
                sa.and_(
                    Organization.id == organization_uuid,
                    Organization.sid == Url.organization_id,
                )
            )
            .join(Organization)
            .join(Url)
        )
        author = self._get_author_sub_query()

        # use this select style, using [] is depricated and gives warnings.
        # but new style gives false positive typing errors therefore the type: ignore is added # noqa: E501
        select_statement = sa.select(
            FormManagement.id,  # type: ignore
            FormManagement.name,
            FormManagement.slug,  # type: ignore
            FormManagement.config,
            FormManagement.publish_date,
            FormManagement.status,
            FormManagement.rights,  # type: ignore
            (Organization.id).label("organization_uuid"),  # type: ignore # noqa: E501
            (Organization.name).label("organization_name"),  # type: ignore # noqa: E501
            FormDetails.form,  # type: ignore
            FormDetails.private,  # type: ignore
            Url.url,
            author.scalar_subquery().label("author"),
            *extra_columns,
        )

        if get_active_forms_on_org_url:
            select_statement = select_statement.where(
                sa.and_(
                    self._match("url_url", get_active_forms_on_org_url),
                    self.model.publish_date <= datetime.date.today(),
                    self.model.status == Status.ACTIVE,
                )
            )
        if organization_uuid:
            select_statement = select_statement.where(
                sa.or_(
                    sa.and_(
                        self._match("org_uuid", organization_uuid),
                    ),
                    sa.and_(
                        Organization.id != organization_uuid,
                        self.model.rights == "OWNER",
                        FormDetails.private == false(),
                        self.model.form_details_sid.not_in(own_forms),  # type: ignore # noqa: E501
                    ),
                )
            )

        select_statement = select_statement.join(FormDetails)
        select_statement = select_statement.join(
            Organization, Organization.sid == FormManagement.organization_sid
        )
        select_statement = select_statement.join(Url)

        return select_statement

    def _get_author_sub_query(self):
        form_details = aliased(FormDetails)
        organizations = aliased(Organization)
        return (
            sa.select(organizations.name)
            .select_from(form_details)
            .join(organizations, organizations.sid == form_details.author, isouter=True)
            .where(form_details.sid == FormDetails.sid)
            .scalar_subquery()
        )

    def _shares_sub_query(
        self,
        id: Union[UUID, str],
        organization_uuid: Optional[UUID] = None,
        get_active_forms_on_org_url: Optional[str] = None,
    ):
        shares_form_details_sid = sa.select(self.model.form_details_sid).join(Organization)
        if isinstance(id, UUID):
            shares_form_details_sid = shares_form_details_sid.where(self._match("id", id))
        else:

            shares_form_details_sid = shares_form_details_sid.where(self._match("slug", id))
            if get_active_forms_on_org_url:
                shares_form_details_sid = shares_form_details_sid.where(
                    self._match("url_url", get_active_forms_on_org_url)
                )
            if organization_uuid:
                shares_form_details_sid = shares_form_details_sid.where(
                    self._match("org_uuid", organization_uuid)
                )
        result = (
            sa.select(
                func.json_agg(
                    func.json_build_object(
                        "id",
                        self.model.id,
                        "name",
                        self.model.name,
                        "slug",
                        self.model.slug,
                        "config",
                        self.model.config,
                        "publishDate",
                        self.model.publish_date,
                        "status",
                        self.model.status,
                        "organizationUuid",
                        Organization.id,
                        "organizationName",
                        Organization.name,
                        "rights",
                        self.model.rights,
                    )
                )
            )
            .select_from(self.model)
            .where(
                self.model.form_details_sid
                == shares_form_details_sid.distinct().scalar_subquery()  # type: ignore # noqa: E501
            )
            .join(Organization)
        )

        return result

    def get_form(
        self,
        database: Session,
        id: Union[UUID, str],
        organization_uuid: Optional[UUID] = None,
        get_active_forms_on_org_url: Optional[str] = None,
    ):
        if get_active_forms_on_org_url is None:
            shares = self._shares_sub_query(
                id=id,
                organization_uuid=organization_uuid,
                get_active_forms_on_org_url=get_active_forms_on_org_url,
            )

            select_statement = self._select_base_query(
                organization_uuid,
                get_active_forms_on_org_url,
                [shares.scalar_subquery().label("shares")],  # type: ignore
            )
        else:
            # Ignore shares
            select_statement = self._select_base_query(
                organization_uuid,
                get_active_forms_on_org_url,
                [],
            )

        select_statement = (
            select_statement.where(self._match("id", id))
            if isinstance(id, UUID)
            else select_statement.where(self._match("slug", id))
        ).select_from(self.model)

        result = database.execute(select_statement).mappings().first()

        if not result:
            raise ProcessError(
                loc={"source": "path", "field": "id"},
                msg=error_messages.FORM_NOT_FOUND_WITH.format(id),
                status_code=http_status.HTTP_404_NOT_FOUND,
            )

        return result

    def get_forms_list(
        self,
        db: Session,
        sort_options: Optional[SortOptions],
        range_options: Optional[RangeOptions],
        filter_options: Optional[FilterOptions],
        organization_uuid: Optional[UUID] = None,
        get_active_forms_on_org_url: Optional[str] = None,
    ) -> SearchResult:
        """Basic CRUD get collection call"""

        # Return the following forms:
        # - All the (in)active public/private forms for the organization.
        #   There are no specific requirements on the access rights.
        # - All the (in)active public forms for organizations with
        #   access right 'Owner'.
        #
        # Note: there can only be one owner for a form

        # Ophalen van al je eigen formulieren. Het maakt dus niet uit
        # of het formulier actief of inactief is. Ook maakt het niet uit
        # of het formulier public of private is. Ook maakt het niet uit
        # welke rol de organisatie heeft (owner, editor of viewer).

        # Als je geen eigenaar, editor of viewer bent wil je ook alle
        # overige publieke formulieren tonen en dan alleen het formulier
        # met rol owner. Het maakt hier niet uit of het formulier inactief
        # of actief is.

        # Er kunnen formulieren zijn waarbij meer dan een owner aanwezig is.
        # Toon al deze owners. Geldt alleen voor publieke formulieren.
        # Maakt niet uit of het actief of inactief is.

        select_statement = self._select_base_query(
            organization_uuid, get_active_forms_on_org_url
        ).select_from(self.model)

        updated_filter: dict = {}
        if filter_options:
            for key, value in filter_options.options.items():
                if key == "private":
                    select_statement = select_statement.where(  # type: ignore
                        FormDetails.private == value
                    )
                else:
                    updated_filter[key] = value

        response = self.get_multi(
            db=db,
            query=select_statement,
            sort_options=sort_options,
            range_options=range_options,
            filter_options=FilterOptions(options=updated_filter),
            return_scalars=False,
        )

        return response

    def create_form(
        self,
        database: Session,
        form_create_request: schemas.FormCreateRequest,
        current_user: AccessUser,
    ) -> schemas.FormWithDetails:
        """Basic CRUD create call"""

        # First check if organization exists
        organization = (
            database.execute(
                sa.select(Organization).where(
                    self._match("org_uuid", current_user.organization_uuid)
                )
            )
            .scalars()
            .first()
        )

        if not organization:
            raise ProcessError(
                loc={"source": "token", "field": "organization_uuid"},
                msg=error_messages.ORGANIZATION_NOT_FOUND_WITH.format(
                    current_user.organization_uuid
                ),
                status_code=http_status.HTTP_404_NOT_FOUND,
            )

        # First add the form details to the db
        form_details = FormDetails(
            form=form_create_request.form,
            private=form_create_request.private,
            author=organization.sid,
        )
        try:

            database.add(form_details)
            database.flush()
            database.commit()
        except sa.exc.IntegrityError as exception:

            logger.error(f"create_form exception: {exception}")

            raise ProcessError(
                loc={"source": "body", "field": "name"},
                msg=error_messages.FORM_NOT_CREATED,
                status_code=http_status.HTTP_409_CONFLICT,
            ) from exception

        # generate a slug with the name that is slugable
        custom_slugify = UniqueSlugify()
        slug = custom_slugify(form_create_request.name)

        # see if there is a slug
        if len(slug) <= 0:
            database.delete(form_details)
            database.flush()
            database.commit()
            raise ProcessError(
                loc={"source": "body", "field": "name"},
                msg=error_messages.INPUT_NOT_ALLOWED.format("name"),
                status_code=http_status.HTTP_422_UNPROCESSABLE_ENTITY,
            )

        config_create = (
            form_create_request.config
            if form_create_request.config
            else [{"key": "urlAfsluitenKnop", "value": "https://xxllnc.nl/"}]
        )

        publish_date = form_create_request.publish_date
        if form_create_request.status == Status.ACTIVE and not publish_date:
            publish_date = datetime.datetime.today()

        # Now create new row in FormManagement
        form_management = self.model(
            name=form_create_request.name,
            slug=slug,
            config=config_create,
            publish_date=publish_date,
            status=form_create_request.status,
            organization=organization,
            rights=Rights.OWNER,
            form_details=form_details,
        )

        try:
            database.add(form_management)
            database.flush()
            database.commit()
        except sa.exc.IntegrityError as exception:

            logger.error(f"create_form exception: {exception}")
            database.delete(form_details)
            handle_db_exception(exception, "aanmaken")

        database.refresh(form_management)

        return schemas.FormWithDetails(
            form=form_management.form_details.form,
            private=form_management.form_details.private,
            author=form_management.organization.name,
            organization_name=form_management.organization.name,
            organization_uuid=form_management.organization.id,
            **form_management.__dict__,
        )

    def _get_form_and_check_rights(
        self,
        db: Session,
        id: Union[UUID, str],
        current_user: AccessUser,
        action: FormAction,
    ):

        form_to_update = (
            get_form_management_by_id(db, id)
            if isinstance(id, UUID)
            else get_form_management_by_slug(db, id, current_user)
        )

        own_form = form_to_update.organization.id == current_user.organization_uuid
        owner = form_to_update.rights == Rights.OWNER
        editor = form_to_update.rights == Rights.EDITOR
        viewer = form_to_update.rights == Rights.VIEWER

        if (
            (action.update and not own_form or not (owner or editor))
            or (action.delete and not own_form)
            and not viewer
        ):
            raise ProcessError(
                loc={"source": "path", "field": "id"},
                msg=error_messages.NOT_YOUR_FORM.format(id),
                status_code=http_status.HTTP_403_FORBIDDEN,
            )
        return form_to_update

    def _set_value_to_update(
        self,
        field_name: str,
        dict_with_input_values: dict,
        can_be_null: bool,
        form_to_update,
    ):

        if field_name in dict_with_input_values:

            value = dict_with_input_values.get(field_name)
            if value is not None or (value is None and can_be_null):

                setattr(form_to_update, field_name, value)

            else:

                raise ProcessError(
                    loc={"source": "body", "field": field_name},
                    msg=error_messages.INVALID_INPUT.format(field_name),
                    status_code=http_status.HTTP_422_UNPROCESSABLE_ENTITY,
                )

    def update_form(
        self,
        database: Session,
        id: Union[UUID, str],
        form_update_request: schemas.FormUpdateRequest,
        current_user: AccessUser,
    ) -> schemas.FormWithDetails:

        form_to_update = self._get_form_and_check_rights(
            database, id, current_user, FormAction.update
        )

        # make sure this slug is slugable
        if form_update_request.slug:
            custom_slugify = UniqueSlugify()
            form_update_request.slug = custom_slugify(form_update_request.slug)

        dict_with_input_values = form_update_request.dict(by_alias=False, exclude_unset=True)

        #  update form and private in form_details if they are set
        for column in ["form", "private"]:
            can_be_null = models.FormDetails.__table__.columns[column].nullable
            self._set_value_to_update(
                field_name=column,
                dict_with_input_values=dict_with_input_values,
                can_be_null=can_be_null,
                form_to_update=form_to_update.form_details,
            )

        if dict_with_input_values.get("private") is not None:
            form_to_update.form_details.private = form_update_request.private

        for column in self.model.__table__.columns.keys():
            self._set_value_to_update(
                field_name=column,
                dict_with_input_values=dict_with_input_values,
                can_be_null=self.model.__table__.columns[column].nullable,
                form_to_update=form_to_update,
            )

        try:
            database.add(form_to_update)
            database.commit()
        except sa.exc.IntegrityError as exception:
            logger.error(f"update_form exception: {exception}")
            handle_db_exception(exception, "wijzigen")

        return self._convert_db_object_to_FormWithDetails(form_to_update)

    def delete_form(
        self,
        db: Session,
        id: Union[UUID, str],
        current_user: AccessUser,
    ) -> schemas.FormWithDetails | None:

        form_to_delete = self._get_form_and_check_rights(db, id, current_user, FormAction.delete)

        select_shares = sa.select(self.model.sid).where(  # type: ignore
            self.model.form_details_sid == form_to_delete.form_details_sid
        )
        no_of_shares = (
            db.execute(select_shares.with_only_columns(func.count()).order_by(None))
            .scalars()
            .one()
        )

        if no_of_shares > 1:
            raise ProcessError(
                loc={"source": "path", "field": "id"},
                msg=error_messages.FORM_NOT_DELETED,
                status_code=http_status.HTTP_409_CONFLICT,
            )

        self.delete(db=db, id=GenericId(id=form_to_delete.id, type=IdType.id))

        deleted_form = self._convert_db_object_to_FormWithDetails(form_to_delete)

        sub_stmt = db.query(FormManagement.form_details_sid)
        statement = sa.delete(FormDetails).where(FormDetails.sid.not_in(sub_stmt))
        db.execute(statement, execution_options={"synchronize_session": "fetch"})
        db.commit()

        return deleted_form


form = CRUDForm(FormManagement, models)
