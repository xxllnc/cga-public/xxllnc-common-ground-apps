#!/usr/bin/env python3

"""Module for creating initial data for the database"""  # pragma: no cover

from app.api.api_v1.routers.tests.helper import (
    org1,
    org2,
    org3,
    org4,
    org5,
    org6,
    url1,
    url2,
    url3,
    url4,
    url5,
    url6,
    url7,
)
from app.db import models  # pragma: no cover
from app.db.models import Status  # pragma: no cover
from app.db.session import write_session  # pragma: no cover
from datetime import date  # pragma: no cover
from sqlalchemy import text  # pragma: no cover
from sqlalchemy.orm import Session  # pragma: no cover


def truncate_tables() -> None:  # pragma: no cover
    """Truncate tables"""

    database: Session = write_session()
    database.execute(text('truncate table "form_management" cascade'))
    database.execute(text('truncate table "form_details" cascade'))
    database.execute(text('truncate table "organization" cascade'))
    database.commit()


def restart_sequences_with_1() -> None:  # pragma: no cover
    """Restart the sequences with 1"""

    db: Session = write_session()
    db.execute(text('ALTER SEQUENCE "form_details_sid_seq" RESTART WITH 1'))
    db.execute(text('ALTER SEQUENCE "form_management_sid_seq" RESTART WITH 1'))
    db.execute(text('ALTER SEQUENCE "organization_sid_seq" RESTART WITH 1'))
    db.execute(text('ALTER SEQUENCE "url_sid_seq" RESTART WITH 1'))
    db.commit()


def create_initial_data() -> None:  # pragma: no cover
    """Creates initial data for database"""

    database: Session = write_session()

    organization1 = models.Organization(**org1)
    organization2 = models.Organization(**org2)
    organization3 = models.Organization(**org3)
    organization4 = models.Organization(**org4)
    organization5 = models.Organization(**org5)
    organization6 = models.Organization(**org6)

    database.add(organization1)
    database.add(organization2)
    database.add(organization3)
    database.add(organization4)
    database.add(organization5)
    database.add(organization6)

    database.flush()

    details1 = models.FormDetails(form={"field": "value"}, private=False, author=organization1.sid)
    details2 = models.FormDetails(form={"field": "value"}, private=True, author=organization1.sid)
    details3 = models.FormDetails(form={"field": "value"}, private=False, author=organization3.sid)

    database.add(details1)
    database.add(details2)
    database.add(details3)

    database.flush()

    url_1 = models.Url(
        **url1,
        organization_id=organization1.sid,
    )
    url_2 = models.Url(
        **url2,
        organization_id=organization2.sid,
    )
    url_3 = models.Url(
        **url3,
        organization_id=organization3.sid,
    )
    url_4 = models.Url(
        **url4,
        organization_id=organization4.sid,
    )
    url_5 = models.Url(
        **url5,
        organization_id=organization5.sid,
    )
    url_6 = models.Url(
        **url6,
        organization_id=organization6.sid,
    )
    url_7 = models.Url(
        **url7,
        organization_id=organization6.sid,
    )
    database.add(url_1)
    database.add(url_2)
    database.add(url_3)
    database.add(url_4)
    database.add(url_5)
    database.add(url_6)
    database.add(url_7)

    form1 = models.FormManagement(
        name="formulier a",
        slug="formulier-a",
        config=[],
        publish_date=None,
        status=Status.INACTIVE,
        organization=organization1,
        rights=models.Rights.OWNER,
        form_details=details1,
    )
    form2 = models.FormManagement(
        name="formulier b",
        slug="formulier-b",
        config=[],
        publish_date=date(2022, 2, 10),
        status=Status.ACTIVE,
        organization=organization1,
        rights=models.Rights.OWNER,
        form_details=details2,
    )
    form3 = models.FormManagement(
        name="formulier C",
        slug="formulier-C",
        config=[],
        publish_date=date(2022, 2, 10),
        status=Status.ACTIVE,
        organization=organization3,
        rights=models.Rights.OWNER,
        form_details=details3,
    )

    database.add(form1)
    database.add(form2)
    database.add(form3)

    database.flush()
    database.commit()


if __name__ == "__main__":  # pragma: no cover
    print("First remove any existing data")
    truncate_tables()
    print("Restart the sequences with 1")
    restart_sequences_with_1()
    print("Creating initial data")
    create_initial_data()
    print("Initial data created")
