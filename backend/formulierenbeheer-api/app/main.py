"""The main file"""

import logging
import os
import sys
import uvicorn
from app.api.api_v1.routers import active_forms, forms, organizations, security, share_forms, url
from app.core.config import BASE_PATH, BASE_PATH_V1, PROJECT_NAME
from exxellence_shared_cga.core.exceptions import (
    shared_process_error_handler,
    shared_semantic_error_handler,
    shared_validation_exception_handler,
)
from exxellence_shared_cga.core.logging import InterceptHandler
from exxellence_shared_cga.core.types import ProcessError, SemanticError
from exxellence_shared_cga.middleware.CheckContentTypeMiddleware import CheckContentTypeMiddleware
from fastapi import FastAPI, Request
from fastapi.exceptions import RequestValidationError
from loguru import logger
from pydantic import ValidationError
from typing import Final, Union

Exe = Union[RequestValidationError, ValidationError]
API_DESCRIPTION: Final = """
formulierenbeheer-api can be used to make, edit, add and delete new forms.
To use this api it is required to provide a valid Auth0 jwt-token
"""

app = FastAPI(
    title=PROJECT_NAME,
    description=API_DESCRIPTION,
    version="0.0.1",
    contact={
        "name": "Xxllnc developer website",
        "url": "https://gitlab.com/xxllnc/cga/xxllnc-common-ground-apps/-/tree/development/backend/formulierenbeheer-api",  # noqa: E501
    },
    license_info={
        "name": "Xxllnc: Licensed under the EUPL-1.2-or-later",
        "url": "https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12",
    },
    docs_url=f"{BASE_PATH}/docs",
    redoc_url=f"{BASE_PATH}/redoc",
    openapi_url=BASE_PATH,
)


@app.middleware("http")
async def log_middle(request: Request, call_next):

    logger.trace(f"{request.method} {request.url}")

    logger.trace("Params:")
    for name, value in request.path_params.items():
        logger.trace(f"\t{name}: {value}")

    logger.trace("Headers:")
    for name, value in request.headers.items():
        logger.trace(f"\t{name}: {value}")

    return await call_next(request)


app.add_middleware(CheckContentTypeMiddleware)


@app.exception_handler(RequestValidationError)
@app.exception_handler(ValidationError)
async def validation_exception_handler(request: Request, exc: Exe):

    handler = shared_validation_exception_handler()
    return handler(exc)


@app.exception_handler(SemanticError)
async def semantic_error_handler(request: Request, exc: SemanticError):
    handler = shared_semantic_error_handler()
    return handler(request, exc)


@app.exception_handler(ProcessError)
async def process_error_handler(request: Request, exc: ProcessError):
    handler = shared_process_error_handler()
    return handler(exc)


# Routers
app.include_router(active_forms.router, prefix=BASE_PATH_V1, tags=["Active forms"])
app.include_router(forms.router, prefix=BASE_PATH_V1, tags=["Forms catalogue"])
app.include_router(share_forms.router, prefix=BASE_PATH_V1, tags=["Share interface"])
app.include_router(organizations.router, prefix=BASE_PATH_V1, tags=["Organizations interface"])
app.include_router(url.router, prefix=BASE_PATH_V1, tags=["Url interface"])
app.include_router(security.router, prefix=BASE_PATH_V1, tags=["security.txt"])

# Get log level.
LOG_LEVEL = logging.getLevelName(os.environ.get("LOG_LEVEL", "INFO"))
LOGGERS = ("uvicorn.asgi", "uvicorn.access", "uvicorn.error")

# Configure root logger.
logging.root.handlers = [InterceptHandler()]
logging.root.setLevel(LOG_LEVEL)

# Remove every other logger's handlers and propagate to root logger
for name in LOGGERS:
    logging_logger = logging.getLogger(name)
    logging_logger.handlers = []
    logging_logger.setLevel(LOG_LEVEL)
    logging_logger.propagate = True

# Set format
logger.configure(handlers=[{"sink": sys.stdout, "level": LOG_LEVEL}])

if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", reload=True, port=8883)
