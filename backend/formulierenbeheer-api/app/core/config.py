import os
import sys
from dotenv import load_dotenv
from typing import Final, Union

if "pytest" in sys.modules:
    load_dotenv(".env.test")


def to_int(value: Union[str, None], default_value: int) -> int:
    try:
        return int(value) if value else default_value
    except ValueError:
        return default_value


# The title of the API
PROJECT_NAME: Final[str] = "xxllnc-formulierenbeheer"
BASE_PATH: Final[str] = "/formulierenbeheer/api"
BASE_PATH_V1: Final[str] = f"{BASE_PATH}/v1"

#############################################
# SQLAlchemy settings
#############################################
SQLALCHEMY_DATABASE_READ_URI: Final[str] = os.environ.get(
    "DATABASE_READ_URL",
    "postgresql://formscatalogue:formscatalogue123@localhost/formscatalogue",
)
SQLALCHEMY_DATABASE_WRITE_URI: Final[str] = os.environ.get(
    "DATABASE_WRITE_URL",
    "postgresql://formscatalogue:formscatalogue123@localhost/formscatalogue",
)
# If set to True, all database queries will be logged as they are executed.
SQLALCHEMY_ECHO: Final[bool] = os.environ.get("DATABASE_ECHO", "False").upper() == "TRUE"
SQLALCHEMY_POOL_SIZE: Final[int] = to_int(os.environ.get("SQLALCHEMY_POOL_SIZE", None), 5)
SQLALCHEMY_POOL_MAX_OVERFLOW: Final[int] = to_int(
    os.environ.get("SQLALCHEMY_POOL_MAX_OVERFLOW", None), 10
)


# Flag to enable or disable audit logging
AUDIT_LOGGING: Final = bool(os.environ.get("AUDIT_LOGGING", True))

# OIDC client-id, used to verify if we're the audience of access tokens
OIDC_CLIENT_ID = os.environ["OIDC_CLIENT_ID"]

# Auth0 Domain
DOMAIN = os.environ["DOMAIN"]
