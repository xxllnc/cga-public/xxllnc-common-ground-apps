import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as pg
import uuid
from .session import Base
from enum import Enum
from sqlalchemy.orm import relationship


class Status(str, Enum):
    ACTIVE = "ACTIVE"
    INACTIVE = "INACTIVE"


class Rights(str, Enum):
    VIEWER = "VIEWER"
    EDITOR = "EDITOR"
    OWNER = "OWNER"


class FormManagement(Base):
    __tablename__ = "form_management"
    default_sort_field = "name"

    sid = sa.Column(sa.Integer, primary_key=True, index=True)
    id = sa.Column(pg.UUID(as_uuid=True), nullable=False, index=True, default=uuid.uuid4)
    form_details_sid = sa.Column(sa.Integer, sa.ForeignKey("form_details.sid"), nullable=False)
    form_details = relationship("FormDetails")
    name = sa.Column(sa.String(255), index=True, nullable=False)
    slug = sa.Column(sa.String(255), index=True, nullable=False)
    config = sa.Column(sa.JSON, nullable=True)
    publish_date = sa.Column(sa.Date, nullable=True)
    status = sa.Column(sa.Enum(Status), nullable=False)
    rights = sa.Column(sa.Enum(Rights), nullable=False)
    organization_sid = sa.Column(sa.Integer, sa.ForeignKey("organization.sid"), nullable=False)
    organization = relationship("Organization")
    sa.UniqueConstraint(
        "form_management_sid",
        "organization_sid",
        name="unique_columns_per_organization",
    )
    sa.UniqueConstraint(
        "name",
        "organization_sid",
        name="uc_name_organization",
    )
    sa.UniqueConstraint(
        "slug",
        "organization_sid",
        name="uc_slug_organization",
    )

    fulltext_fields = [
        {"modelName": "FormManagement", "fields": ["name", "slug"]},
        {"modelName": "Organization", "fields": ["name"]},
    ]


class FormDetails(Base):
    __tablename__ = "form_details"
    default_sort_field = "id"

    sid = sa.Column(
        sa.Integer,
        sa.Sequence("form_details_sid_seq", metadata=Base.metadata),
        primary_key=True,
        index=True,
    )
    form = sa.Column(sa.JSON, nullable=False)
    private = sa.Column(sa.BOOLEAN, nullable=False, default=True)
    author = sa.Column(sa.Integer, sa.ForeignKey("organization.sid"), nullable=False)


class Organization(Base):
    __tablename__ = "organization"
    __table_args__ = (sa.UniqueConstraint("name", "id", name="uc_name_id"),)
    default_sort_field = "name"

    sid = sa.Column(sa.Integer, primary_key=True, index=True)
    id = sa.Column(
        pg.UUID(as_uuid=True), nullable=False, index=True, unique=True, default=uuid.uuid4
    )
    name = sa.Column(sa.String, index=True, unique=True, nullable=False)
    urls = relationship("Url", lazy="joined", cascade="all,delete", backref="parent")


class Url(Base):
    __tablename__ = "url"
    default_sort_field = "url"
    sid = sa.Column(sa.Integer, primary_key=True, index=True)
    uuid = sa.Column(
        pg.UUID(as_uuid=True),
        nullable=False,
        unique=True,
        server_default=sa.func.gen_random_uuid(),
    )
    url = sa.Column(sa.String, index=True, unique=True, nullable=False)
    primary = sa.Column(sa.Boolean)
    organization_id = sa.Column(sa.Integer, sa.ForeignKey("organization.sid"), nullable=False)
