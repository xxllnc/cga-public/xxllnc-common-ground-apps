from app.core import config
from sqlalchemy import create_engine
from sqlalchemy.orm import DeclarativeMeta, declarative_base, sessionmaker

Base: DeclarativeMeta = declarative_base()


def _make_session(url: str):
    engine = create_engine(
        url=url,
        future=True,
        echo=config.SQLALCHEMY_ECHO,
        pool_size=config.SQLALCHEMY_POOL_SIZE,
        max_overflow=config.SQLALCHEMY_POOL_MAX_OVERFLOW,
    )

    return sessionmaker(bind=engine, future=True, autocommit=False, autoflush=False)


read_session = _make_session(config.SQLALCHEMY_DATABASE_READ_URI)
write_session = _make_session(config.SQLALCHEMY_DATABASE_WRITE_URI)


def get_read_db():

    db = read_session()
    try:
        yield db
    finally:
        db.close()


def get_write_db():

    db = write_session()
    try:
        yield db
    finally:
        db.close()
