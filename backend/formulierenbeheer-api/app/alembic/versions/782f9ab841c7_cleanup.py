"""Script is modified to get situation befor new model is added.
To make cleanup possible

Revision ID: 782f9ab841c7
Revises: 072ed69a2bad
Create Date: 2022-02-07 13:17:35.005162+01:00

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "782f9ab841c7"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    """Upgrade"""

    # The next lines are manually added
    op.create_table(
        "form",
        sa.Column("id", postgresql.UUID(), autoincrement=False, nullable=False),
        sa.Column("name", sa.VARCHAR(length=255), autoincrement=False, nullable=False),
        sa.Column("slug", sa.VARCHAR(length=255), autoincrement=False, nullable=False),
        sa.Column(
            "status",
            postgresql.ENUM("ACTIVE", "INACTIVE", name="status"),
            autoincrement=False,
            nullable=False,
        ),
        sa.Column("publish_date", sa.DATE(), autoincrement=False, nullable=True),
        sa.Column("sid", sa.INTEGER(), autoincrement=False, nullable=False),
        sa.Column(
            "form",
            postgresql.JSON(),
            autoincrement=False,
            nullable=True,
        ),
        sa.Column(
            "config",
            postgresql.JSON(),
            autoincrement=False,
            nullable=True,
        ),
        sa.Column("public", sa.BOOLEAN(), autoincrement=False, nullable=False),
        sa.Column(
            "creator_organization_uuid",
            postgresql.UUID(),
            autoincrement=False,
            nullable=False,
        ),
        sa.PrimaryKeyConstraint("sid", name="form_pkey"),
        sa.UniqueConstraint(
            "name",
            "creator_organization_uuid",
            name="uc_name_creator_organization_uuid",
        ),
        sa.UniqueConstraint(
            "slug",
            "creator_organization_uuid",
            name="uc_slug_creator_organization_uuid",
        ),
        postgresql_ignore_search_path=False,
    )
    op.create_index("ix_form_slug", "form", ["slug"], unique=False)
    op.create_index("ix_form_sid", "form", ["sid"], unique=False)
    op.create_index("ix_form_name", "form", ["name"], unique=False)
    op.create_index("ix_form_id", "form", ["id"], unique=False)
    op.create_table(
        "shared_with",
        sa.Column("sid", sa.INTEGER(), autoincrement=True, nullable=False),
        sa.Column("form_sid", sa.INTEGER(), autoincrement=False, nullable=False),
        sa.Column(
            "organization_uuid",
            postgresql.UUID(),
            autoincrement=False,
            nullable=False,
        ),
        sa.Column(
            "rights",
            postgresql.ENUM("VIEWER", "EDITOR", "OWNER", name="rights"),
            autoincrement=False,
            nullable=False,
        ),
        sa.Column(
            "organization_name",
            sa.VARCHAR(),
            autoincrement=False,
            nullable=False,
        ),
        sa.ForeignKeyConstraint(["form_sid"], ["form.sid"], name="shared_with_form_sid_fkey"),
        sa.PrimaryKeyConstraint("sid", name="shared_with_pkey"),
        sa.UniqueConstraint("form_sid", "organization_uuid", name="unique_columns_share_with"),
    )
    op.create_index("ix_shared_with_sid", "shared_with", ["sid"], unique=False)
    # ### end Alembic commands ###


def downgrade():
    """Downgrade"""

    pass
