"""make form none null

Revision ID: 58ce9901de10
Revises: 447dab94b7d6
Create Date: 2022-04-04 15:23:03.496980+02:00

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.orm import DeclarativeMeta, declarative_base

Base: DeclarativeMeta = declarative_base()

# revision identifiers, used by Alembic.
revision = "58ce9901de10"
down_revision = "447dab94b7d6"
branch_labels = None
depends_on = None


class FormDetails(Base):
    __tablename__ = "form_details"
    sid = sa.Column(
        sa.Integer,
        sa.Sequence("form_details_sid_seq", metadata=Base.metadata),
        primary_key=True,
        index=True,
    )
    form = sa.Column(sa.JSON, nullable=False)


def upgrade():
    """Upgrade"""
    # ### commands auto generated by Alembic - please adjust! ###
    # First make sure all forms get al value and then set nullable to False
    bind = op.get_bind()
    bind.execute(
        sa.update(FormDetails).where(FormDetails.form.is_(None)).values(form={"field": "value"})
    )

    op.alter_column("form_details", "form", nullable=False)
    # ### end Alembic commands ###


def downgrade():
    """Downgrade"""
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column("form_details", "form", nullable=True)
    # ### end Alembic commands ###
