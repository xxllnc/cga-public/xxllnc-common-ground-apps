"""FixOrganizationTableAndRelations


Revision ID: 447dab94b7d6
Revises: ebf2da213dea
Create Date: 2022-03-09 07:04:56.866429+01:00

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy import orm
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm import DeclarativeMeta, declarative_base

Base: DeclarativeMeta = declarative_base()

# revision identifiers, used by Alembic.
revision = "447dab94b7d6"
down_revision = "ebf2da213dea"
branch_labels = None
depends_on = None


class FormManagement(Base):
    __tablename__ = "form_management"
    sid = sa.Column(sa.Integer, primary_key=True, index=True)
    organization_uuid = sa.Column(postgresql.UUID(as_uuid=True), nullable=False)
    organization_name = sa.Column(sa.String(), nullable=False)
    organization_sid = sa.Column(sa.Integer)


class Organization(Base):
    __tablename__ = "organization"

    sid = sa.Column(
        sa.Integer,
        primary_key=True,
        index=True,
    )
    id = sa.Column(
        postgresql.UUID(as_uuid=True),
        nullable=False,
        index=True,
    )
    name = sa.Column(sa.String, index=True, nullable=False)
    url = sa.Column(sa.String, index=True, nullable=False)


def upgrade():
    """Upgrade"""
    # ### commands auto generated by Alembic - please adjust! ###

    # (Re)create organization(s) table
    op.create_table(
        "organization",
        sa.Column("sid", sa.Integer(), nullable=False),
        sa.Column("id", postgresql.UUID(as_uuid=True), nullable=False),
        sa.Column("name", sa.String(), nullable=False),
        sa.Column("url", sa.String(), nullable=False),
        sa.PrimaryKeyConstraint("sid"),
    )
    op.create_index(op.f("ix_organization_id"), "organization", ["id"], unique=False)
    op.create_index(op.f("ix_organization_name"), "organization", ["name"], unique=False)
    op.create_index(op.f("ix_organization_sid"), "organization", ["sid"], unique=False)
    op.create_index(op.f("ix_organization_url"), "organization", ["url"], unique=False)
    op.add_column(
        "form_management",
        sa.Column("organization_sid", sa.Integer(), nullable=True),
    )

    # Now select all unique organizations
    bind = op.get_bind()
    session = orm.Session(bind=bind)
    forms = bind.execute(
        sa.select(
            FormManagement.organization_uuid, FormManagement.organization_name  # type: ignore # noqa: E501
        ).group_by(FormManagement.organization_uuid, FormManagement.organization_name)
    ).fetchall()

    # Add fill new organization table and link organization to FormManagement
    for form in forms:
        organization = Organization(
            id=form.organization_uuid,
            name=form.organization_name,
            url="https://exxellence.vcap.me",
        )
        session.add(organization)
        session.commit()
        bind.execute(
            sa.update(FormManagement)
            .where(FormManagement.organization_uuid == form.organization_uuid)
            .values(organization_sid=organization.sid)
        )

    op.alter_column("form_management", "organization_sid", nullable=False)

    # Now cleanup changed and no longer used tables and columns

    op.drop_index("ix_organizations_organization_name", table_name="organizations")
    op.drop_index("ix_organizations_organization_url", table_name="organizations")
    op.drop_index("ix_organizations_sid", table_name="organizations")
    op.drop_index("ix_organizations_uuid", table_name="organizations")
    op.drop_table("organizations")

    op.drop_constraint("uc_name_organization_uuid", "form_management", type_="unique")
    op.drop_constraint("uc_slug_organization_uuid", "form_management", type_="unique")

    op.create_unique_constraint(
        constraint_name="uc_name_organization",
        table_name="form_management",
        columns=["name", "organization_sid"],
    )
    op.create_unique_constraint(
        constraint_name="uc_slug_organization",
        table_name="form_management",
        columns=["slug", "organization_sid"],
    )

    op.create_foreign_key(None, "form_management", "organization", ["organization_sid"], ["sid"])
    op.drop_column("form_management", "organization_name")
    op.drop_column("form_management", "organization_uuid")
    # ### end Alembic commands ###


def downgrade():
    """Downgrade"""
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "form_management",
        sa.Column(
            "organization_uuid",
            postgresql.UUID(),
            autoincrement=False,
            nullable=False,
        ),
    )
    op.add_column(
        "form_management",
        sa.Column(
            "organization_name",
            sa.VARCHAR(),
            autoincrement=False,
            nullable=False,
        ),
    )

    op.create_unique_constraint(
        "uc_slug_organization_uuid",
        "form_management",
        ["slug", "organization_uuid"],
    )
    op.create_unique_constraint(
        "uc_name_organization_uuid",
        "form_management",
        ["name", "organization_uuid"],
    )
    op.drop_column("form_management", "organization_sid")
    op.create_table(
        "organizations",
        sa.Column("sid", sa.INTEGER(), autoincrement=True, nullable=False),
        sa.Column("uuid", postgresql.UUID(), autoincrement=False, nullable=False),
        sa.Column(
            "organization_name",
            sa.VARCHAR(),
            autoincrement=False,
            nullable=False,
        ),
        sa.Column(
            "organization_url",
            sa.VARCHAR(),
            autoincrement=False,
            nullable=False,
        ),
        sa.PrimaryKeyConstraint("sid", name="organizations_pkey"),
    )
    op.create_index("ix_organizations_uuid", "organizations", ["uuid"], unique=False)
    op.create_index("ix_organizations_sid", "organizations", ["sid"], unique=False)
    op.create_index(
        "ix_organizations_organization_url",
        "organizations",
        ["organization_url"],
        unique=False,
    )
    op.create_index(
        "ix_organizations_organization_name",
        "organizations",
        ["organization_name"],
        unique=False,
    )
    op.drop_index(op.f("ix_organization_url"), table_name="organization")
    op.drop_index(op.f("ix_organization_sid"), table_name="organization")
    op.drop_index(op.f("ix_organization_name"), table_name="organization")
    op.drop_index(op.f("ix_organization_id"), table_name="organization")
    op.drop_table("organization")
    # ### end Alembic commands ###
