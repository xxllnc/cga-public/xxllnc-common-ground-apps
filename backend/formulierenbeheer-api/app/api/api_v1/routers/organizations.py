from app import domain, schemas
from app.core.auth0 import get_admin_scopes
from app.db.session import get_read_db, get_write_db
from exxellence_shared_cga.core.rest_helpers import SearchParameters
from exxellence_shared_cga.core.types import GenericId, IdType
from fastapi import APIRouter, Depends, Header, Response
from fastapi import status as http_status
from sqlalchemy.orm import Session
from uuid import UUID

router = r = APIRouter()


# validate the filter and sort parameters
def validate_search_parameters():
    return SearchParameters(
        filter_options_schema=schemas.OrgFilterableFields,
        sort_options_schema=schemas.OrgSortableFields,
    )


@r.get(
    "/organizations/url",
    response_model=schemas.OrganizationId,
    responses={404: {"description": "Organization not found"}},
    dependencies=[Depends(get_admin_scopes)],
)
def get_organization_uuid_by_url(db=Depends(get_read_db), organization_url: str = Header(...)):

    return domain.organizations.get_organization_by_url(organization_url, db)


@r.get(
    "/organizations",
    response_model=list[schemas.OrganizationReturn],
    dependencies=[Depends(get_admin_scopes)],
)
def get_organizations(
    response: Response,
    database: Session = Depends(get_read_db),
    search_parameters: dict = Depends(validate_search_parameters()),
):

    search_result = domain.organizations.get_organizations(database=database, **search_parameters)

    response.headers["Content-Range"] = search_result.as_content_range("results")

    return search_result.result


@r.put(
    "/organizations/{id}",
    response_model=schemas.OrganizationReturn,
    responses={
        http_status.HTTP_404_NOT_FOUND: {"description": "Organization not found"},
        http_status.HTTP_409_CONFLICT: {"description": "Conflict. Organization not updated."},
    },
    dependencies=[Depends(get_admin_scopes)],
)
def update_organization(
    id: UUID,
    organization_update_request: schemas.OrganizationUpdate,
    database: Session = Depends(get_write_db),
):

    updated_org = domain.organizations.update(
        db=database, id=GenericId(id=id, type=IdType.id), obj_in=organization_update_request
    )
    return {**updated_org.__dict__}


@r.post(
    "/organizations",
    response_model=schemas.OrganizationReturn,
    responses={
        http_status.HTTP_404_NOT_FOUND: {"description": "Organization not found"},
        http_status.HTTP_409_CONFLICT: {"description": "Conflict. Organization not updated."},
    },
    dependencies=[Depends(get_admin_scopes)],
)
def add_organization(
    organization_add_request: schemas.OrganizationCreate, database: Session = Depends(get_write_db)
):

    return domain.organizations.create(db=database, obj_in=organization_add_request)
