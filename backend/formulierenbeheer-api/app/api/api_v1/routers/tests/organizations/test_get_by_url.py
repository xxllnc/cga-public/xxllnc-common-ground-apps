from app.api.api_v1.routers.tests import HEADERS, ORG
from app.api.api_v1.routers.tests.organizations.test_mock_data import (
    add_organization_mock_data_to_db,
)
from app.core.config import BASE_PATH_V1
from app.resources import error_messages
from fastapi import status as http_status
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session


# ---------------------------------------------
# Test getting org_uuid by url
# ---------------------------------------------
def test_get_org_by_existing_url(client: TestClient, test_db: Session):
    mock = add_organization_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{ORG}/url", headers=HEADERS)

    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()

    assert len(res) == 1
    assert res["id"] == str(mock.organization1.id)


def test_get_org_by_none_existing_url(client: TestClient, test_db: Session):

    add_organization_mock_data_to_db(test_db)

    response = client.get(
        f"{BASE_PATH_V1}{ORG}/url", headers={"organization-url": "https://unknown.me"}
    )

    assert response.status_code == http_status.HTTP_404_NOT_FOUND
    res = response.json()

    assert res["message"] == error_messages.ORGANIZATION_NOT_FOUND_WITH.format(
        "https://unknown.me"
    )
