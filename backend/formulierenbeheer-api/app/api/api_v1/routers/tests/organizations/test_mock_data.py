#!/usr/bin/env python3
from app.api.api_v1.routers.tests.helper import org1, org2, org3, org4, url1, url2, url3, url4
from app.db import models
from collections import namedtuple
from sqlalchemy.orm import Session
from typing import Final

SUBMITTER: Final = "test@exxellence.nl"

# Declaring namedtuple() as return value

OrganizationMockData = namedtuple(
    "OrganizationMockData",
    ["organization1", "organization2", "organization3", "organization4"],
)


def add_organization_mock_data_to_db(
    db: Session,
) -> OrganizationMockData:

    organization1 = models.Organization(**org1)
    organization2 = models.Organization(**org2)
    organization3 = models.Organization(**org3)
    organization4 = models.Organization(**org4)

    db.add(organization1)
    db.add(organization2)
    db.add(organization3)
    db.add(organization4)
    db.flush()

    url_1 = models.Url(
        **url1,
        organization_id=organization1.sid,
    )
    url_2 = models.Url(
        **url2,
        organization_id=organization2.sid,
    )
    url_3 = models.Url(
        **url3,
        organization_id=organization3.sid,
    )
    url_4 = models.Url(
        **url4,
        organization_id=organization4.sid,
    )
    db.add(url_1)
    db.add(url_2)
    db.add(url_3)
    db.add(url_4)

    return OrganizationMockData(
        organization1,
        organization2,
        organization3,
        organization4,
    )
