import pytest
from app import schemas
from app.api.api_v1.routers.tests import (
    HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER,
    HTTP_METHOD_OVERRIDE_URI_PARAMETER,
    ORG,
)
from app.api.api_v1.routers.tests.asserts import assert_method_not_allowed
from app.api.api_v1.routers.tests.helper import empty_body
from app.api.api_v1.routers.tests.organizations.test_mock_data import (
    add_organization_mock_data_to_db,
)
from app.api.api_v1.routers.tests.parameters import http_method_override
from app.core.config import BASE_PATH_V1
from fastapi import status as http_status
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session
from typing import Final

SUPPORTED_METHODS: Final = ["get", "post", "put"]
UNSUPPORTED_METHODS: Final = [ele for ele in http_method_override if ele not in SUPPORTED_METHODS]


# ---------------------------------------------
# Testing an API with HTTP methods that are not supported.
# Reject request with HTTP response code 405 (Method not allowed).
# Supported HTTP methods: GET and PUT
# ---------------------------------------------
@pytest.mark.parametrize("http_method", UNSUPPORTED_METHODS)
@pytest.mark.parametrize("request_body", [None, empty_body])
def test_http_methods_not_supported(client: TestClient, http_method: str, request_body: str):
    response = client.request(http_method, f"{BASE_PATH_V1}{ORG}", json=request_body)
    assert_method_not_allowed(response, http_method)


# ---------------------------------------------
# Test HTTP Method Overriding for the http method GET
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("header", HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER)
def test_get_method_http_override(client: TestClient, test_db: Session, method: str, header: str):
    add_organization_mock_data_to_db(test_db)

    headers = {header: method}
    response = client.get(f"{BASE_PATH_V1}{ORG}", headers=headers)
    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 4


# ---------------------------------------------
# Test HTTP Method Overriding for the http method GET
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("parameter", HTTP_METHOD_OVERRIDE_URI_PARAMETER)
def test_get_method_http_override_uri_parameter(
    client: TestClient, test_db: Session, method: str, parameter: str
):
    add_organization_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{ORG}?{parameter}={method.upper()}")
    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 4


# ---------------------------------------------
# Test HTTP Method Overriding for the http method PUT
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("header", HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER)
def test_put_method_http_override(client: TestClient, test_db: Session, method: str, header: str):
    mock = add_organization_mock_data_to_db(test_db)

    body = schemas.OrganizationUpdate(name="test").json(by_alias=True)

    headers = {header: method}
    response = client.put(
        f"{BASE_PATH_V1}{ORG}/{mock.organization1.id}", json=body, headers=headers
    )
    assert response.status_code == http_status.HTTP_200_OK
    form = response.json()
    assert len(form) == 3


# ---------------------------------------------
# Test HTTP Method Overriding for the http method POST
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("parameter", HTTP_METHOD_OVERRIDE_URI_PARAMETER)
def test_put_method_http_override_uri_parameter(
    client: TestClient, test_db: Session, method: str, parameter: str
):
    mock = add_organization_mock_data_to_db(test_db)

    body = schemas.OrganizationUpdate(name="test").json(by_alias=True)

    response = client.put(
        f"{BASE_PATH_V1}{ORG}/{mock.organization1.id}?{parameter}={method.upper()}", json=body
    )
    assert response.status_code == http_status.HTTP_200_OK
    form = response.json()
    assert len(form) == 3


# ---------------------------------------------
# Test the unsupported http methods for HTTP Method Overriding.
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("unsupported_method", UNSUPPORTED_METHODS)
@pytest.mark.parametrize("header", HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER)
def test_http_method_overriding_for_unsupported_http_methods(
    client: TestClient, method: str, unsupported_method: str, header: str
):
    headers = {header: method}
    response = client.request(unsupported_method, f"{BASE_PATH_V1}{ORG}", headers=headers)
    assert_method_not_allowed(response, unsupported_method)


# ---------------------------------------------
# Test the unsupported http methods for HTTP Method Overriding.
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("unsupported_method", UNSUPPORTED_METHODS)
@pytest.mark.parametrize("parameter", HTTP_METHOD_OVERRIDE_URI_PARAMETER)
def test_http_method_overriding_for_unsupported_http_methods_uri_parameter(
    client: TestClient, method: str, unsupported_method: str, parameter: str
):
    response = client.request(
        unsupported_method, f"{BASE_PATH_V1}{ORG}?{parameter}={method.upper()}"
    )
    assert_method_not_allowed(response, unsupported_method)
