import pytest
from app.api.api_v1.routers.tests import CONTENT_TYPE_VALUE_APPLICATION_JSON, ORG, QUERY_PARAMETER
from app.api.api_v1.routers.tests.organizations.test_mock_data import (
    add_organization_mock_data_to_db,
)
from app.api.api_v1.routers.tests.parameters import (
    invalid_order_paramter,
    invalid_q_filter_values,
    invalid_sort_and_order,
    range_parameter,
    range_test_organization_with_end,
    sort_by_invalid_fields,
)
from app.api.api_v1.routers.tests.test_generic_functions import (
    test_get_list_range_with_invalid_start_end_combination,
)
from app.core.config import BASE_PATH_V1
from app.db import models
from fastapi import status as http_status
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session
from typing import Union


def _assert_found(organization: dict, expected: models.Organization):
    organizations = expected.__dict__
    assert organization["name"] == organizations["name"]
    assert organization["id"] == str(organizations["id"])


# ---------------------------------------------
# Testing an API with GET requests
# ---------------------------------------------
def test_get_forms_list(client: TestClient, test_db: Session):
    mock = add_organization_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{ORG}")
    res = response.json()
    assert response.status_code == http_status.HTTP_200_OK
    headers = response.headers
    assert len(headers) == 3
    assert headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert headers["content-range"] == "results 0-3/4"

    expected_order = [4, 2, 3, 1]
    assert len(res) == len(expected_order)
    for index, no in enumerate(expected_order):
        _assert_found(res[index], getattr(mock, f"organization{no}"))


# ---------------------------------------------
# Testing an API with GET requests.
# Sort by valid fields.
# ---------------------------------------------
@pytest.mark.parametrize(
    "sort_query_parameter_value, expected_order",
    [
        ('["name","ASC"]', [4, 2, 3, 1]),
        ('["name","DESC"]', [1, 3, 2, 4]),
    ],
)
def test_get_forms_list_sort_by_valid_field_asc(
    client: TestClient,
    test_db: Session,
    sort_query_parameter_value: str,
    expected_order: Union[str, list[int]],
):
    mock = add_organization_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{ORG}?sort={sort_query_parameter_value}")

    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 4
    for index, organizations in enumerate(res):
        assert len(organizations) == 3
        _assert_found(organizations, getattr(mock, f"organization{expected_order[index]}"))


# ---------------------------------------------
# Testing an API with GET requests.
# Sort by invalid fields.
# ---------------------------------------------
@pytest.mark.parametrize(QUERY_PARAMETER, sort_by_invalid_fields)
@pytest.mark.parametrize("order", ["ASC", "DESC"])
def test_get_forms_list_sort_by_invalid_field(
    client: TestClient, query_parameter: str, order: str
):
    response = client.get(f'{BASE_PATH_V1}{ORG}?sort=["{query_parameter}","{order}"]')
    assert response.status_code == http_status.HTTP_400_BAD_REQUEST


# ---------------------------------------------
# Testing an API with GET requests.
# Sort by invalid order.
# ---------------------------------------------
@pytest.mark.parametrize(QUERY_PARAMETER, invalid_order_paramter)
def test_get_forms_list_sort_by_invalid_order(client: TestClient, query_parameter: str):
    response = client.get(f'{BASE_PATH_V1}{ORG}?sort=["name","{query_parameter}"]')
    assert response.status_code == http_status.HTTP_400_BAD_REQUEST


# ---------------------------------------------
# Testing an API with GET requests.
# Test invalid sort and order combinations.
# ---------------------------------------------
@pytest.mark.parametrize(QUERY_PARAMETER, invalid_sort_and_order)
def test_get_forms_list_invalid_sort_order_combinations(client: TestClient, query_parameter: str):
    response = client.get(f"{BASE_PATH_V1}{ORG}?sort={query_parameter}")

    assert response.status_code == http_status.HTTP_400_BAD_REQUEST
    res = response.json()
    assert (
        res["detail"] == f"Invalid sort order: {query_parameter}. sort; needs to be a "
        "JSON list with two elements: The field to sort by, and the order "
        "(ASC or DESC). Example: `[field, sortOrder]` where sortOrder is ASC "
        "or DESC"
    )


# ---------------------------------------------
# Testing an API with GET requests.
# Test start range without end range.
# Throw ValueError because the end range is not set.
# ---------------------------------------------
@pytest.mark.parametrize(QUERY_PARAMETER, range_parameter)
def test_get_forms_list_range_with_start_without_end(client: TestClient, query_parameter: str):
    with pytest.raises(ValueError):
        client.get(f"{BASE_PATH_V1}{ORG}?{query_parameter}")


# ---------------------------------------------
# Testing an API with GET requests.
# Test start range with an end range.
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter, response_length, header_content_range", range_test_organization_with_end
)
def test_get_forms_list_range_with_start_with_end(
    client: TestClient,
    test_db: Session,
    query_parameter: str,
    response_length: int,
    header_content_range: str,
):
    add_organization_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{ORG}?{query_parameter}")

    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == int(f"{response_length}")

    headers = response.headers
    assert len(headers) == 3
    assert headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert headers["content-range"] == f"{header_content_range}"


# ---------------------------------------------
# Testing an API with GET requests.
# Test invalid start range with end range combinations.
# Throw ValueError
# ---------------------------------------------
def test_get_forms_list_range_with_invalid_start_end_combination(client: TestClient):
    test_get_list_range_with_invalid_start_end_combination(client)


# ---------------------------------------------
# Testing an API with GET requests.
# Test end range without start range.
# Throw ValueError because the start range is not set.
# ---------------------------------------------
@pytest.mark.parametrize(QUERY_PARAMETER, range_parameter)
def test_get_forms_list_range_with_end_without_start(client: TestClient, query_parameter: str):
    with pytest.raises(ValueError):
        client.get(f"{BASE_PATH_V1}{ORG}?{query_parameter}")


# ---------------------------------------------
# Testing an API with GET requests.
# Test fulltext search with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize(QUERY_PARAMETER, invalid_q_filter_values)
def test_get_forms_list_invalid_full_text_filter(client: TestClient, query_parameter: str):
    response = client.get(f"{BASE_PATH_V1}{ORG}?filter={query_parameter}")
    assert response.status_code == http_status.HTTP_400_BAD_REQUEST
