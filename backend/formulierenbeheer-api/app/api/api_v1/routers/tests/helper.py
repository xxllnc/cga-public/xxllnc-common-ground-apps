from app import schemas
from app.db import models
from typing import Final
from uuid import UUID

# org1 is also used in the auth token in test
org1: Final = {
    "id": UUID("0aa2beca-3819-4f49-aba2-2631e0bcbfb8"),
    "name": "xxllnc",
}
org2: Final = {
    "id": UUID("1f2b43c1-ed77-4fb2-bc4e-2f542d424a3b"),
    "name": "Exxellence2",
}
org3: Final = {
    "id": UUID("bf585e6d-e641-4ef7-be55-9d3a7054773a"),
    "name": "Zaakstad",
}
org4: Final = {
    "id": UUID("73cef5d6-ae65-4cb4-97b4-564b53e02606"),
    "name": "Acme Corporation",
}
org5: Final = {
    "id": UUID("0d495e11-bc5a-489e-8924-c26ca9f7df3d"),
    "name": "Exxellence Bulk",
}
org6: Final = {
    "id": UUID("623dfac9-3e8e-4ea5-be5c-7dc4a78f5868"),
    "name": "Zuiddrecht",
}

url1: Final = {
    "uuid": UUID("0aa2beca-3819-4f49-aba2-2631e0bcbfb8"),
    "url": "https://exxellence.vcap.me",
    "primary": True,
}
url2: Final = {
    "uuid": UUID("1f2b43c1-ed77-4fb2-bc4e-2f542d424a3b"),
    "url": "https://exxellence2.vcap.me",
    "primary": True,
}
url3: Final = {
    "uuid": UUID("bf585e6d-e641-4ef7-be55-9d3a7054773a"),
    "url": "https://zaakstad.vcap.me",
    "primary": True,
}
url4: Final = {
    "uuid": UUID("73cef5d6-ae65-4cb4-97b4-564b53e02606"),
    "url": "https://acme.vcap.me",
    "primary": True,
}
url5: Final = {
    "uuid": UUID("0d495e11-bc5a-489e-8924-c26ca9f7df3d"),
    "url": "https://bulk.vcap.me",
    "primary": True,
}
url6: Final = {
    "uuid": UUID("623dfac9-3e8e-4ea5-be5c-7dc4a78f5868"),
    "url": "https://zuiddrecht.vcap.me",
    "primary": True,
}
url7: Final = {
    "uuid": UUID("993dfac9-3e8e-4ea5-be5c-7dc4a78f5868"),
    "url": "https://oostdrecht.vcap.me",
    "primary": True,
}


empty_body: dict = {}


def convert_db_form_to_dict(form: models.FormManagement) -> dict:
    form_management = form.__dict__
    form_details = form_management["form_details"].__dict__
    organization = form_management["organization"].__dict__

    return schemas.FormWithDetails(
        id=form_management["id"],
        form=form_details["form"],
        name=form_management["name"],
        slug=form_management["slug"],
        config=form_management["config"],
        status=form_management["status"],
        organization_name=organization["name"],
        organization_uuid=organization["id"],
        rights=form_management["rights"],
        private=form_details["private"],
        publish_date=form_management["publish_date"],
        author=form_details["author"],
    ).dict(by_alias=True)
