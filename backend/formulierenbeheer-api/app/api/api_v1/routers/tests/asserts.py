from app.api.api_v1.routers.tests import CONTENT_TYPE_VALUE_APPLICATION_JSON
from app.db import models
from app.resources import error_messages
from fastapi import status as http_status
from requests.models import Response


def assert_method_not_allowed(response: Response, http_method: str):
    assert response.status_code == http_status.HTTP_405_METHOD_NOT_ALLOWED
    if http_method != "head":
        res = response.json()
        assert len(res) == 1
        assert res["detail"] == error_messages.METHOD_NOT_ALLOWED


def assert_found_range(response: Response, with_range: bool):
    assert response.status_code == http_status.HTTP_200_OK

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 3 if with_range else 2
    assert response_headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert response_headers["content-length"] is not None
    if with_range:
        assert response_headers["content-range"] is not None


# check if returned form matches form from db
def assert_found(form: dict, expected: models.FormManagement):
    form_management = expected.__dict__
    form_details = form_management["form_details"].__dict__
    organization = form_management["organization"].__dict__

    assert form["id"] == str(form_management["id"])
    assert form["name"] == form_management["name"]
    assert form["slug"] == form_management["slug"]
    if form_management["config"]:
        assert form["config"] == form_management["config"]
    else:
        assert type(form["config"]) is list
    assert form["status"] == form_management["status"]
    assert form["organizationUuid"] == str(organization["id"])
    assert form["organizationName"] == organization["name"]
    assert form["private"] == form_details["private"]
    if form_management["publish_date"]:
        assert form["publishDate"] == str(form_management["publish_date"])
    else:
        assert form["publishDate"] is None
