#!/usr/bin/env python3
from app.api.api_v1.routers.tests.helper import org1, org2, org3, org4, url1, url2, url3, url4
from app.db import models
from app.db.models import Status
from collections import namedtuple
from datetime import date
from sqlalchemy.orm import Session

# Declaring namedtuple() as return value
MockData = namedtuple(
    "MockData",
    [
        "form1_org1_owner",
        "form1_org2_viewer",
        "form2_public_org2_owner",
        "form3_org2_owner",
        "form3_org1_editor",
        "form4_org2_owner",
        "form4_org1_viewer",
        "form5_org3_owner",
        "form6_org1_owner",
        "form_details1",
        "form_details2",
        "form_details3",
        "form_details4",
        "form_details5",
    ],
)


#  Same data als initial_data.
def add_share_forms_mock_data_to_db(db: Session) -> MockData:
    organization1 = models.Organization(**org1)
    organization2 = models.Organization(**org2)
    organization3 = models.Organization(**org3)
    organization4 = models.Organization(**org4)

    db.add(organization1)
    db.add(organization2)
    db.add(organization3)
    db.add(organization4)

    db.flush()

    form_details1 = models.FormDetails(
        form={"description": "form 1, private, org 1 owner"},
        private=True,
        author=organization1.sid,
    )
    form_details2 = models.FormDetails(
        form={"description": "form 2, public, org 2 owner"},
        private=False,
        author=organization2.sid,
    )
    form_details3 = models.FormDetails(
        form={"description": "form 3, private, org 2 owner, org 1 editor"},
        private=True,
        author=organization2.sid,
    )
    form_details4 = models.FormDetails(
        form={"description": "form 4, private, org 2 owner, org 1 viewer"},
        private=True,
        author=organization2.sid,
    )
    form_details5 = models.FormDetails(
        form={"description": "form 5, private, org 3 owner"},
        private=True,
        author=organization3.sid,
    )
    form_details6 = models.FormDetails(
        form={"description": "form 6, private, org 1 only owner"},
        private=True,
        author=organization1.sid,
    )
    db.add(form_details1)
    db.add(form_details2)
    db.add(form_details3)
    db.add(form_details4)
    db.add(form_details5)
    db.add(form_details6)

    db.flush()

    url_1 = models.Url(
        **url1,
        organization_id=organization1.sid,
    )
    url_2 = models.Url(
        **url2,
        organization_id=organization2.sid,
    )
    url_3 = models.Url(
        **url3,
        organization_id=organization3.sid,
    )
    url_4 = models.Url(
        **url4,
        organization_id=organization4.sid,
    )
    db.add(url_1)
    db.add(url_2)
    db.add(url_3)
    db.add(url_4)

    form1_org1_owner = models.FormManagement(
        name="form 1 organization 1 owner",
        slug="form1-org1-owner",
        config=[],
        publish_date=date(2021, 8, 24),
        status=Status.ACTIVE,
        rights=models.Rights.OWNER,
        form_details=form_details1,
        organization=organization1,
    )
    form1_org2_viewer = models.FormManagement(
        name="form 1 organization 2 viewer",
        slug="form1-org2-viewer",
        config=[],
        publish_date=date(2021, 8, 24),
        status=Status.ACTIVE,
        rights=models.Rights.VIEWER,
        form_details=form_details1,
        organization=organization2,
    )
    form2_public_org2_owner = models.FormManagement(
        name="form 1 organization 1 owner",  # TODO CGA-2052
        slug="form2-org2-owner",
        config=[],
        publish_date=None,
        status=Status.INACTIVE,
        rights=models.Rights.OWNER,
        form_details=form_details2,
        organization=organization2,
    )
    form3_org2_owner = models.FormManagement(
        name="form 3 organization 2 owner",
        slug="form3-org2-owner",
        config=[],
        publish_date=date(2021, 8, 25),
        status=Status.ACTIVE,
        rights=models.Rights.OWNER,
        form_details=form_details3,
        organization=organization2,
    )
    form3_org1_editor = models.FormManagement(
        name="form 3 organization 1 editor",
        slug="form3-org1-editor",
        config=[],
        publish_date=date(2021, 8, 25),
        status=Status.ACTIVE,
        rights=models.Rights.EDITOR,
        form_details=form_details3,
        organization=organization1,
    )
    form4_org2_owner = models.FormManagement(
        name="form 4 organization 2 owner",
        slug="form4-org2-owner",
        config=[],
        publish_date=date(2021, 8, 25),
        status=Status.ACTIVE,
        rights=models.Rights.OWNER,
        form_details=form_details4,
        organization=organization2,
    )
    form4_org1_viewer = models.FormManagement(
        name="form 4 organization 1 viewer",
        slug="form4-org1-viewer",
        config=[],
        publish_date=date(2021, 8, 25),
        status=Status.ACTIVE,
        rights=models.Rights.VIEWER,
        form_details=form_details4,
        organization=organization1,
    )
    form5_org3_owner = models.FormManagement(
        name="form 5 organization 3 owner",
        slug="form5-org3-owner",
        config=[],
        publish_date=date(2021, 8, 25),
        status=Status.ACTIVE,
        rights=models.Rights.OWNER,
        form_details=form_details5,
        organization=organization3,
    )
    form6_org1_owner = models.FormManagement(
        name="form 6 organization 1 only owner",
        slug="form6-org1-owner",
        config=[],
        publish_date=date(2021, 8, 25),
        status=Status.ACTIVE,
        rights=models.Rights.OWNER,
        form_details=form_details6,
        organization=organization1,
    )

    db.add(form1_org1_owner)
    db.add(form1_org2_viewer)
    db.add(form2_public_org2_owner)
    db.add(form3_org2_owner)
    db.add(form3_org1_editor)
    db.add(form4_org2_owner)
    db.add(form4_org1_viewer)
    db.add(form5_org3_owner)
    db.add(form6_org1_owner)

    db.flush()

    return MockData(
        form1_org1_owner,
        form1_org2_viewer,
        form2_public_org2_owner,
        form3_org2_owner,
        form3_org1_editor,
        form4_org2_owner,
        form4_org1_viewer,
        form5_org3_owner,
        form6_org1_owner,
        form_details1,
        form_details2,
        form_details3,
        form_details4,
        form_details5,
    )
