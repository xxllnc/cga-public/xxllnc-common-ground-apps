import pytest
from app.api.api_v1.routers.tests import FORMS, SHARE
from app.api.api_v1.routers.tests.helper import org2
from app.api.api_v1.routers.tests.share_forms.test_mock_data import add_share_forms_mock_data_to_db
from app.core.config import BASE_PATH_V1
from app.db import models
from fastapi import status as http_status
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session


# ---------------------------------------------
# Delete "/forms/{id}"
# Test delete a shared form.
# ---------------------------------------------
@pytest.mark.parametrize(
    "rights", [models.Rights.EDITOR, models.Rights.OWNER, models.Rights.VIEWER]
)
def test_delete_shared_form(client: TestClient, test_db: Session, rights: str):
    mock = add_share_forms_mock_data_to_db(test_db)
    mock_form_details_id = mock.form6_org1_owner.id

    # check number of shares (1 expected)
    response = client.get(f"{BASE_PATH_V1}{FORMS}/{mock_form_details_id}")
    assert response.status_code == http_status.HTTP_200_OK
    form = response.json()
    assert isinstance(form["shares"], list)
    assert len(form["shares"]) == 1

    # add a new share
    body = {
        "organizationUuid": str(org2["id"]),
        "organizationName": org2["name"],
        "rights": rights,
        "id": str(mock_form_details_id),
    }
    response_post = client.post(f"{BASE_PATH_V1}{SHARE}", json=body)
    assert response_post.status_code == http_status.HTTP_200_OK

    # check number of shares (2 expected)
    response_get = client.get(f"{BASE_PATH_V1}{FORMS}/{mock_form_details_id}")
    assert response_get.status_code == http_status.HTTP_200_OK
    form1 = response_get.json()
    assert isinstance(form1["shares"], list)
    assert len(form1["shares"]) == 2

    # delete form
    response_delete = client.delete(
        f"{BASE_PATH_V1}{FORMS}/{mock_form_details_id}",
    )
    assert response_delete.status_code == http_status.HTTP_409_CONFLICT

    # check share
    response_get2 = client.get(f"{BASE_PATH_V1}{FORMS}/{mock_form_details_id}")
    assert response_get2.status_code == http_status.HTTP_200_OK
    form2 = response_get2.json()
    assert isinstance(form2["shares"], list)
    assert len(form2["shares"]) == 2
