import pytest
import re
import uuid
from app.api.api_v1.routers.tests import SHARE
from app.api.api_v1.routers.tests.helper import org2, org3, org4
from app.api.api_v1.routers.tests.share_forms.test_mock_data import add_share_forms_mock_data_to_db
from app.core.config import BASE_PATH_V1
from app.db import models
from app.resources import error_messages
from fastapi import status as code
from fastapi.testclient import TestClient
from requests.models import Response
from sqlalchemy.orm import Session
from typing import Final, Union

URL: Final[str] = f"{BASE_PATH_V1}{SHARE}"

# fields used to test te post and put operations
name = {"name": "the name"}
slug = {"slug": "the slug"}
rights_viewer = {"rights": models.Rights.VIEWER}
rights_editor = {"rights": models.Rights.EDITOR}
rights_owner = {"rights": models.Rights.OWNER}
config = {"config": [{"url": "https://xxllnc.nl"}]}
status_inactive = {"status": models.Status.INACTIVE, "publishDate": None}
status_active = {"status": models.Status.ACTIVE, "publishDate": "2022-03-02"}
name_and_slug = {**name, **slug}
name_slug_and_config: dict = {**name_and_slug, **config}
name_slug_and_status_inactive: dict = {**name_and_slug, **status_inactive}
name_slug_and_status_active = {**name_and_slug, **status_active}
name_and_rights_viewer = {**name, **rights_viewer}
name_and_rights_editor = {**name, **rights_editor}
name_and_rights_owner = {**name, **rights_owner}


def _camel_to_snake(name):
    name = re.sub("(.)([A-Z][a-z]+)", r"\1_\2", name)
    return re.sub("([a-z0-9])([A-Z])", r"\1_\2", name).lower()


def _get_expected_values_from_body(body: dict, expected_base: dict) -> dict:

    for key, value in body.items():
        expected_base[_camel_to_snake(key)] = value

    if not body.get("status"):
        expected_base["status"] = models.Status.INACTIVE

    if not body.get("publishDate"):
        expected_base["publish_date"] = None

    return expected_base


def _assert_get_share(response: Response, expected: dict, id: Union[uuid.UUID, None] = None):

    assert response.status_code == code.HTTP_200_OK

    res = response.json()

    assert res["organizationUuid"] == str(expected["organization_uuid"])
    assert res["organizationName"] == expected["organization_name"]
    assert res["rights"] == expected["rights"].value
    assert res["name"] == expected["name"]
    assert res["slug"] == expected["slug"]
    assert res["status"] == expected["status"].value
    if expected["publish_date"]:
        assert res["publishDate"] == str(expected["publish_date"])
    else:
        assert res["publishDate"] is None
    if id:
        assert res["id"] == str(id)


# ---------------------------------------------
# GET
# ---------------------------------------------
def test_get_share(client: TestClient, test_db: Session):

    mock = add_share_forms_mock_data_to_db(test_db)

    share = mock.form1_org1_owner

    response = client.get(f"{URL}/{share.id}")

    expected = share.__dict__
    expected["organization_uuid"] = expected["organization"].id
    expected["organization_name"] = expected["organization"].name

    _assert_get_share(response, expected, share.id)


@pytest.mark.parametrize(
    "id, expected_status",
    [
        ("58e51567-6fef-4305-97e2-a5f70762c2a0", code.HTTP_404_NOT_FOUND),
        ("nouuid", code.HTTP_422_UNPROCESSABLE_ENTITY),
        (1, code.HTTP_422_UNPROCESSABLE_ENTITY),
    ],
)
def test_get_unknown_share(client: TestClient, test_db: Session, id, expected_status):

    add_share_forms_mock_data_to_db(test_db)

    response = client.get(f"{URL}/{id}")

    assert response.status_code == expected_status


# ---------------------------------------------
# POST
# ---------------------------------------------


def test_add_new_share_to_forms_where_org_is_owner(client: TestClient, test_db: Session):

    mock = add_share_forms_mock_data_to_db(test_db)

    body = {
        "organizationUuid": str(org3["id"]),
        "organizationName": org3["name"],
        "rights": models.Rights.VIEWER,
        "id": str(mock.form1_org1_owner.id),
    }

    response = client.post(URL, json=body)

    expected = _get_expected_values_from_body(body, mock.form1_org1_owner.__dict__)

    _assert_get_share(response, expected)


def test_add_new_share_to_forms_where_org_already_has_rights(client: TestClient, test_db: Session):

    mock = add_share_forms_mock_data_to_db(test_db)

    body = {
        "organizationUuid": str(org2["id"]),
        "organizationName": org2["name"],
        "rights": models.Rights.VIEWER,
        "id": str(mock.form1_org1_owner.id),
    }

    response = client.post(URL, json=body)

    assert response.status_code == code.HTTP_409_CONFLICT
    assert (
        response.json()["message"]
        == error_messages.FORM_COMBINATION_NAME_CREATOR_ORGANIZATION_UUID_ALREADY_EXIST_ERROR
    )


@pytest.mark.parametrize(
    "optional_fields",
    [
        name,
        slug,
        config,
        status_inactive,
        status_active,
        name_and_slug,
        name_slug_and_config,
        name_slug_and_status_inactive,
        name_slug_and_status_active,
    ],
)
def test_add_new_share_to_public_form_org_has_no_owner_rights(
    client: TestClient, test_db: Session, optional_fields: dict
):

    mock = add_share_forms_mock_data_to_db(test_db)

    body = {
        "organizationUuid": str(org3["id"]),
        "organizationName": org3["name"],
        "rights": models.Rights.OWNER,
        "id": str(mock.form2_public_org2_owner.id),
    }

    for key, value in optional_fields.items():
        if value:
            body[key] = value

    expected = _get_expected_values_from_body(body, mock.form2_public_org2_owner.__dict__)

    response = client.post(URL, json=body)

    _assert_get_share(response, expected)


@pytest.mark.parametrize(
    "form_name",
    [
        "form3_org1_editor",
        "form4_org1_viewer",
        "form5_org3_owner",
    ],
)
def test_add_new_share_to_form_where_org_has_no_owner_rights(
    client: TestClient, test_db: Session, form_name: str
):

    mock = add_share_forms_mock_data_to_db(test_db)
    form = getattr(mock, form_name)

    body = {
        "organizationUuid": str(org4["id"]),
        "organizationName": org4["name"],
        "rights": models.Rights.VIEWER,
        "id": str(form.id),
    }

    response = client.post(URL, json=body)

    assert response.status_code == code.HTTP_404_NOT_FOUND
    assert response.json()["message"] == error_messages.FORM_NOT_SHARED.format(form.id)


# ---------------------------------------------
# PUT
# ---------------------------------------------
@pytest.mark.parametrize(
    "form, optional_fields",
    [
        ("form1_org2_viewer", {}),
        ("form1_org2_viewer", name),
        ("form1_org2_viewer", slug),
        ("form1_org2_viewer", rights_viewer),
        ("form1_org2_viewer", rights_editor),
        ("form1_org2_viewer", rights_owner),
        ("form1_org2_viewer", config),
        ("form1_org2_viewer", status_inactive),
        ("form1_org2_viewer", status_active),
        ("form1_org2_viewer", name_and_slug),
        ("form1_org2_viewer", name_slug_and_config),
        ("form1_org2_viewer", name_slug_and_status_inactive),
        ("form1_org2_viewer", name_slug_and_status_active),
        ("form1_org2_viewer", name_and_rights_viewer),
        ("form1_org2_viewer", name_and_rights_editor),
        ("form1_org2_viewer", name_and_rights_owner),
        ("form4_org1_viewer", name),
        ("form6_org1_owner", name),
        ("form6_org1_owner", rights_editor),
        ("form6_org1_owner", rights_viewer),
    ],
)
def test_update_share_where_organization_is_the_owner(
    client: TestClient, test_db: Session, form: str, optional_fields: dict
):

    mock = add_share_forms_mock_data_to_db(test_db)

    share = getattr(mock, form)
    body = {"id": str(share.id)}

    # add optional fields to body
    for key, value in optional_fields.items():
        if value:
            body[key] = value

    expected_base = share.__dict__
    expected_base["organization_uuid"] = share.organization.id
    expected_base["organization_name"] = share.organization.name

    expected = _get_expected_values_from_body(body, expected_base)

    response = client.put(f"{URL}/{share.id}", json=body)

    _assert_get_share(response, expected)


@pytest.mark.parametrize("form", ["form3_org2_owner", "form4_org2_owner", "form5_org3_owner"])
def test_update_share_where_organization_has_no_owner_rights(
    client: TestClient, test_db: Session, form: str
):

    mock = add_share_forms_mock_data_to_db(test_db)
    share = getattr(mock, form)
    share_id = str(share.id)
    body = {"id": share_id, "name": "new name"}

    response = client.put(f"{URL}/{share_id}", json=body)

    assert response.status_code == code.HTTP_404_NOT_FOUND


# ---------------------------------------------
# DELETE
# ---------------------------------------------
@pytest.mark.parametrize(
    "form",
    ["form1_org2_viewer", "form3_org1_editor", "form6_org1_owner"],
)
def test_delete_shares_where_organization_is_the_owner(
    client: TestClient, test_db: Session, form: str
):

    mock = add_share_forms_mock_data_to_db(test_db)

    delete = client.delete(f"{URL}/{getattr(mock, form).id}")

    assert delete.status_code == code.HTTP_200_OK


@pytest.mark.parametrize(
    "form",
    [
        "form2_public_org2_owner",
        "form3_org2_owner",
        "form4_org2_owner",
        "form5_org3_owner",
    ],
)
def test_delete_shares_where_organization_is_not_the_owner(
    client: TestClient, test_db: Session, form: str
):

    mock = add_share_forms_mock_data_to_db(test_db)

    delete = client.delete(f"{URL}/{getattr(mock, form).id}")

    assert delete.status_code == code.HTTP_404_NOT_FOUND
