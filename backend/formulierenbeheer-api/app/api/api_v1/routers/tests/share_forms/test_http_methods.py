import pytest
from app.api.api_v1.routers.tests import (
    HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER,
    HTTP_METHOD_OVERRIDE_URI_PARAMETER,
    SHARE,
)
from app.api.api_v1.routers.tests.asserts import assert_method_not_allowed
from app.api.api_v1.routers.tests.helper import empty_body, org3
from app.api.api_v1.routers.tests.parameters import http_method_override
from app.api.api_v1.routers.tests.share_forms.test_mock_data import add_share_forms_mock_data_to_db
from app.core.config import BASE_PATH_V1
from app.db import models
from fastapi import status as http_status
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session
from typing import Final

SUPPORTED_METHODS: Final = ["delete", "get", "post", "put"]
UNSUPPORTED_METHODS: Final = [ele for ele in http_method_override if ele not in SUPPORTED_METHODS]


def _add_share(client: TestClient, mock) -> str:
    body = {
        "organizationUuid": str(org3["id"]),
        "organizationName": org3["name"],
        "rights": models.Rights.VIEWER,
        "id": str(mock.form1_org1_owner.id),
    }

    response = client.post(f"{BASE_PATH_V1}{SHARE}", json=body)
    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    return res["id"]


# ---------------------------------------------
# Testing an API with HTTP methods that are not supported.
# Reject request with HTTP response code 405 (Method not allowed).
# Supported HTTP methods: DELETE, GET, POST and PUT
# ---------------------------------------------
@pytest.mark.parametrize("http_method", UNSUPPORTED_METHODS)
@pytest.mark.parametrize("request_body", [None, empty_body])
def test_http_methods_not_supported(client: TestClient, http_method: str, request_body: str):
    response = client.request(http_method, f"{BASE_PATH_V1}{SHARE}", json=request_body)
    assert_method_not_allowed(response, http_method)


# ---------------------------------------------
# Test HTTP Method Overriding for the http method GET
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("header", HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER)
def test_get_method_http_override(client: TestClient, test_db: Session, method: str, header: str):
    mock = add_share_forms_mock_data_to_db(test_db)
    share = mock.form1_org1_owner

    headers = {header: method}
    response = client.get(f"{BASE_PATH_V1}{SHARE}/{share.id}", headers=headers)
    assert response.status_code == http_status.HTTP_200_OK


# ---------------------------------------------
# Test HTTP Method Overriding for the http method GET
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("parameter", HTTP_METHOD_OVERRIDE_URI_PARAMETER)
def test_get_method_http_override_uri_parameter(
    client: TestClient, test_db: Session, method: str, parameter: str
):
    mock = add_share_forms_mock_data_to_db(test_db)
    share = mock.form1_org1_owner

    response = client.get(f"{BASE_PATH_V1}{SHARE}/{share.id}?{parameter}={method.upper()}")
    assert response.status_code == http_status.HTTP_200_OK


# ---------------------------------------------
# Test HTTP Method Overriding for the http method POST
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("header", HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER)
def test_post_method_http_override(client: TestClient, test_db: Session, method: str, header: str):
    mock = add_share_forms_mock_data_to_db(test_db)

    body = {
        "organizationUuid": str(org3["id"]),
        "organizationName": org3["name"],
        "rights": models.Rights.VIEWER,
        "id": str(mock.form1_org1_owner.id),
    }

    headers = {header: method}
    response = client.post(f"{BASE_PATH_V1}{SHARE}", json=body, headers=headers)
    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    client.delete(f'{BASE_PATH_V1}{SHARE}/{str(res["id"])}')


# ---------------------------------------------
# Test HTTP Method Overriding for the http method POST
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("parameter", HTTP_METHOD_OVERRIDE_URI_PARAMETER)
def test_post_method_http_override_uri_parameter(
    client: TestClient, test_db: Session, method: str, parameter: str
):
    mock = add_share_forms_mock_data_to_db(test_db)

    body = {
        "organizationUuid": str(org3["id"]),
        "organizationName": org3["name"],
        "rights": models.Rights.VIEWER,
        "id": str(mock.form1_org1_owner.id),
    }

    response = client.post(f"{BASE_PATH_V1}{SHARE}?{parameter}={method.upper()}", json=body)
    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    client.delete(f'{BASE_PATH_V1}{SHARE}/{str(res["id"])}')


# ---------------------------------------------
# Test HTTP Method Overriding for the http method PUT
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("header", HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER)
def test_put_method_http_override(client: TestClient, test_db: Session, method: str, header: str):
    mock = add_share_forms_mock_data_to_db(test_db)
    share = getattr(mock, "form1_org2_viewer")
    body = {"id": str(share.id)}

    headers = {header: method}
    response = client.put(f"{BASE_PATH_V1}{SHARE}/{share.id}", json=body, headers=headers)
    assert response.status_code == http_status.HTTP_200_OK


# ---------------------------------------------
# Test HTTP Method Overriding for the http method PUT
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("parameter", HTTP_METHOD_OVERRIDE_URI_PARAMETER)
def test_put_method_http_override_uri_parameter(
    client: TestClient, test_db: Session, method: str, parameter: str
):
    mock = add_share_forms_mock_data_to_db(test_db)
    share = getattr(mock, "form1_org2_viewer")
    body = {"id": str(share.id)}

    response = client.put(
        f"{BASE_PATH_V1}{SHARE}/{share.id}?{parameter}={method.upper()}", json=body
    )
    assert response.status_code == http_status.HTTP_200_OK


# ---------------------------------------------
# Test HTTP Method Overriding for the http method DELETE
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("header", HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER)
def test_delete_method_http_override(
    client: TestClient, test_db: Session, method: str, header: str
):
    mock = add_share_forms_mock_data_to_db(test_db)
    headers = {header: method}
    share_id: str = _add_share(client, mock)
    response = client.delete(f"{BASE_PATH_V1}{SHARE}/{share_id}", headers=headers)
    assert response.status_code == http_status.HTTP_200_OK


# ---------------------------------------------
# Test HTTP Method Overriding for the http method DELETE
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("parameter", HTTP_METHOD_OVERRIDE_URI_PARAMETER)
def test_delete_method_http_override_uri_parameter(
    client: TestClient, test_db: Session, method: str, parameter: str
):
    mock = add_share_forms_mock_data_to_db(test_db)
    share_id: str = _add_share(client, mock)
    response = client.delete(f"{BASE_PATH_V1}{SHARE}/{share_id}?{parameter}={method.upper()}")
    assert response.status_code == http_status.HTTP_200_OK


# ---------------------------------------------
# Test the unsupported http methods for HTTP Method Overriding.
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("unsupported_method", UNSUPPORTED_METHODS)
@pytest.mark.parametrize("header", HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER)
def test_http_method_overriding_for_unsupported_http_methods(
    client: TestClient, method: str, unsupported_method: str, header: str
):
    headers = {header: method}
    response = client.request(unsupported_method, f"{BASE_PATH_V1}{SHARE}", headers=headers)
    assert_method_not_allowed(response, unsupported_method)


# ---------------------------------------------
# Test the unsupported http methods for HTTP Method Overriding.
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("unsupported_method", UNSUPPORTED_METHODS)
@pytest.mark.parametrize("parameter", HTTP_METHOD_OVERRIDE_URI_PARAMETER)
def test_http_method_overriding_for_unsupported_http_methods_uri_parameter(
    client: TestClient, method: str, unsupported_method: str, parameter: str
):
    response = client.request(
        unsupported_method, f"{BASE_PATH_V1}{SHARE}?{parameter}={method.upper()}"
    )
    assert_method_not_allowed(response, unsupported_method)
