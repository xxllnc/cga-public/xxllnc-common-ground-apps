from app.api.api_v1.routers.tests import CONTENT_TYPE_VALUE_APPLICATION_JSON, URLS
from app.api.api_v1.routers.tests.urls.test_mock_data import add_url_mock_data_to_db
from app.core.config import BASE_PATH_V1
from fastapi import status as http_status
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session


# ---------------------------------------------
# Testing an API with GET requests
# ---------------------------------------------
def test_get_url_list(client: TestClient, test_db: Session):
    add_url_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{URLS}")

    assert response.status_code == http_status.HTTP_200_OK
    headers = response.headers
    assert len(headers) == 3
    assert headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert headers["content-range"] == "results 0-3/1"

    res = response.json()
    assert len(res) == 4


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# ---------------------------------------------
def test_post_method_url(client: TestClient, test_db: Session):
    mock = add_url_mock_data_to_db(test_db)

    # Test update of a setting type
    body = {
        "url": "https://test.xxllnc.nl",
        "primary": mock.url1.primary,
        "organizationId": str(mock.organization1.id),
    }

    response = client.post(f"{BASE_PATH_V1}{URLS}", json=body)

    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 3
    assert res["url"] == "https://test.xxllnc.nl"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# ---------------------------------------------
def test_put_method_url(client: TestClient, test_db: Session):
    mock = add_url_mock_data_to_db(test_db)

    # Test update of a setting type
    body = {
        "url": "new url",
        "primary": mock.url1.primary,
        "organizationId": mock.organization1.sid,
    }

    response = client.put(f"{BASE_PATH_V1}{URLS}/{mock.url1.uuid}", json=body)

    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 3
    assert res["url"] == "new url"


# ---------------------------------------------
# Get "/urls/{id}"
# ---------------------------------------------
def test_get_url_by_id(
    client: TestClient,
    test_db: Session,
):
    mock = add_url_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{URLS}/{mock.url1.uuid}")

    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 5

    # Check if id is generated.
    assert res["id"] is not None
    assert res["url"] == mock.url1.url
