from typing import Final

FORM_LIST: Final[list[int]] = [
    24,
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    14,
    17,
    18,
    20,
]
FORM_LIST_LEN: Final[int] = len(FORM_LIST)


range_parameter = [
    "range=[,0]",
    "range=[,1]",
    "range=[,4]",
    "range=[,100]",
    "range=[,]",
    "range=[,aa]",
    "range=[,-1]",
]

invalid_order_paramter = [
    "unkown",
    "order",
    "_",
    "LEFT",
    "asc",
    "AsC",
    "desc",
    "DeSc",
    None,
]

range_test_with_end = [
    ("range=[0,3]", 4, "results 0-3/1"),
    ("range=[1,2]", 2, "results 1-2/1"),
    ("range=[3,3]", 1, "results 3-3/1"),
]

range_test_organization_with_end = [
    ("range=[0,3]", 4, "results 0-3/4"),
    ("range=[1,2]", 2, "results 1-4/4"),
    ("range=[3,3]", 1, "results 3-6/4"),
]


invalid_q_filter_values = [
    '{""}',
    '{"aaaaaaaaaaaaa"}',
    '{"aaaaaaaaaaaaa":}',
    "{:}",
    '{:""}',
]

invalid_sort_and_order = [
    "[]",
    '[""]',
    '["name"]',
    '["unknownfield"]',
    "[,]",
    '[,"ASC"]',
    '[,"DESC"]',
    '[,"TEST"]',
    "[,,,]",
]

sort_by_invalid_fields = [
    "",
    "unkownfield",
    "_",
    None,
]

http_method_override = [
    "connect",
    "delete",
    "get",
    "head",
    "options",
    "patch",
    "post",
    "put",
    "trace",
    "foor",
]
