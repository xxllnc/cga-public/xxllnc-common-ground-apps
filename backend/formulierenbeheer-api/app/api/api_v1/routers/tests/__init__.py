from typing import Final

ACTIVE_FORMS: Final[str] = "/active/forms"
FORMS: Final[str] = "/forms"
ORG: Final[str] = "/organizations"
SHARE: Final[str] = "/share"
URLS: Final[str] = "/urls"

FORM_DETAILS_FIELDS: Final = 12
FORM_GET_ONE_RESPONSE_DETAILS_FIELDS: Final = 7
FORM_SHARES_FIELDS: Final = 9
FORM_LIST_FIELDS: Final = 12
FORM_ACTIVE_FORM_FIELDS: Final = 3
QUERY_PARAMETER = "query_parameter"
HEADERS: Final = {"organization-url": "https://exxellence.vcap.me"}

HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER: Final[list[str]] = [
    "X-HTTP-Method",
    "X-Method-Override",
    "X-HTTP-Method-Override",
]

HTTP_METHOD_OVERRIDE_URI_PARAMETER: Final[list[str]] = [
    x.lower() for x in HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER
]

CONTENT_TYPE_VALUE_APPLICATION_JSON: Final[str] = "application/json"
