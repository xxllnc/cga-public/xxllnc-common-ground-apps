import pytest
from app.api.api_v1.routers.tests import (
    ACTIVE_FORMS,
    HEADERS,
    HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER,
    HTTP_METHOD_OVERRIDE_URI_PARAMETER,
)
from app.api.api_v1.routers.tests.asserts import assert_method_not_allowed
from app.api.api_v1.routers.tests.helper import empty_body
from app.api.api_v1.routers.tests.parameters import http_method_override
from app.core.config import BASE_PATH_V1
from fastapi import status as http_status
from fastapi.testclient import TestClient
from typing import Final

SUPPORTED_METHODS: Final = ["get"]
UNSUPPORTED_METHODS: Final = [ele for ele in http_method_override if ele not in SUPPORTED_METHODS]


# ---------------------------------------------
# Testing an API with HTTP methods that are not supported.
# Reject request with HTTP response code 405 (Method not allowed).
# Supported HTTP methods: GET
# ---------------------------------------------
@pytest.mark.parametrize("method", UNSUPPORTED_METHODS)
@pytest.mark.parametrize("request_body", [None, empty_body])
def test_http_methods_not_supported(client: TestClient, method: str, request_body: str):
    response = client.request(
        method, f"{BASE_PATH_V1}{ACTIVE_FORMS}", json=request_body, headers=HEADERS
    )
    assert_method_not_allowed(response, method)


# ---------------------------------------------
# Test HTTP Method Overriding for the http method GET
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("header", HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER)
def test_get_method_http_override(client: TestClient, method: str, header: str):
    headers = {header: method}
    headers.update(HEADERS)
    response = client.get(f"{BASE_PATH_V1}{ACTIVE_FORMS}", headers=headers)
    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 0


# ---------------------------------------------
# Test HTTP Method Overriding for the http method GET
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("parameter", HTTP_METHOD_OVERRIDE_URI_PARAMETER)
def test_get_method_http_override_uri_parameter(client: TestClient, method: str, parameter: str):
    response = client.get(
        f"{BASE_PATH_V1}{ACTIVE_FORMS}?{parameter}={method.upper()}", headers=HEADERS
    )
    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 0


# ---------------------------------------------
# Test the unsupported http methods for HTTP Method Overriding.
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("unsupported_method", UNSUPPORTED_METHODS)
@pytest.mark.parametrize("header", HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER)
def test_http_method_overriding_for_unsupported_http_methods(
    client: TestClient, method: str, unsupported_method: str, header: str
):
    headers = {header: method}
    headers.update(HEADERS)
    response = client.request(unsupported_method, f"{BASE_PATH_V1}{ACTIVE_FORMS}", headers=headers)
    assert_method_not_allowed(response, unsupported_method)


# ---------------------------------------------
# Test the unsupported http methods for HTTP Method Overriding.
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("unsupported_method", UNSUPPORTED_METHODS)
@pytest.mark.parametrize("parameter", HTTP_METHOD_OVERRIDE_URI_PARAMETER)
def test_http_method_overriding_for_unsupported_http_methods_uri_parameter(
    client: TestClient, method: str, unsupported_method: str, parameter: str
):
    response = client.request(
        unsupported_method,
        f"{BASE_PATH_V1}{ACTIVE_FORMS}?{parameter}={method.upper()}",
        headers=HEADERS,
    )
    assert_method_not_allowed(response, unsupported_method)
