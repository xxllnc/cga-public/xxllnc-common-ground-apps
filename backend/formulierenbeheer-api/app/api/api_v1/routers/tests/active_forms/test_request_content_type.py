import pytest
import requests
from app.api.api_v1.routers.tests import ACTIVE_FORMS, CONTENT_TYPE_VALUE_APPLICATION_JSON, HEADERS
from app.api.api_v1.routers.tests.asserts import assert_found_range
from app.api.api_v1.routers.tests.forms.test_mock_data import add_mock_data_to_db
from app.core.config import BASE_PATH_V1
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session


# ---------------------------------------------
# Test get request without a request content type.
# A sender that generates a message containing a payload body SHOULD generate
# a Content-Type header field in that message unless the intended media type
# of the enclosed representation is unknown to the sender.
# ---------------------------------------------
def test_get_list_request_without_request_header_content_type(client: TestClient):
    response = client.get(f"{BASE_PATH_V1}{ACTIVE_FORMS}", headers=HEADERS)
    assert_found_range(response, True)


# ---------------------------------------------
# Test get request with a request content type application/json.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_accept",
    [
        "application/javascript",
        CONTENT_TYPE_VALUE_APPLICATION_JSON,  # allowed
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "not valid",
        "application/*",
        "*/*",
        "",
        None,
    ],
)
def test_get_list_request_with_request_header_content_type_application_json(
    client: TestClient, http_request_header_accept: str
):
    headers = {
        **HEADERS,
        "Accept": f"{http_request_header_accept}",
        "Content-Type": CONTENT_TYPE_VALUE_APPLICATION_JSON,
    }
    response = client.get(f"{BASE_PATH_V1}{ACTIVE_FORMS}", headers=headers)
    assert_found_range(response, True)


# ---------------------------------------------
# Test get request with an invalid request content type.
# The Content-Type header attribute specifies the format of the data in the
# request body so that receiver can parse it into appropriate format.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_content_type",
    [
        "application/javascript",
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "",
        "not valid",
        None,
    ],
)
def test_get_list_request_with_request_header_content_type_with_invalid_value(
    client: TestClient, http_request_header_content_type: str
):
    headers = {**HEADERS, "Content-Type": f"{http_request_header_content_type}"}
    response = client.get(f"{BASE_PATH_V1}{ACTIVE_FORMS}", headers=headers)
    assert_found_range(response, True)


# ---------------------------------------------
# Test get request without a request content type.
# A sender that generates a message containing a payload body SHOULD generate
# a Content-Type header field in that message unless the intended media type
# of the enclosed representation is unknown to the sender.
# ---------------------------------------------
def test_get_by_id_request_without_request_header_content_type(
    client: TestClient, test_db: Session
):
    mock = add_mock_data_to_db(test_db)
    response = client.get(f"{BASE_PATH_V1}{ACTIVE_FORMS}/{mock.form7.id}", headers=HEADERS)
    assert_found_range(response, False)


# ---------------------------------------------
# Test get request with a request content type application/json.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_accept",
    [
        "application/javascript",
        CONTENT_TYPE_VALUE_APPLICATION_JSON,  # allowed
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "not valid",
        "application/*",
        "*/*",
        "",
        None,
    ],
)
def test_get_by_id_request_with_request_header_content_type_application_json(
    client: TestClient, http_request_header_accept: str, test_db: Session
):
    mock = add_mock_data_to_db(test_db)

    headers = {
        **HEADERS,
        "Accept": f"{http_request_header_accept}",
        "Content-Type": CONTENT_TYPE_VALUE_APPLICATION_JSON,
    }

    response = client.get(f"{BASE_PATH_V1}{ACTIVE_FORMS}/{mock.form7.id}", headers=headers)
    assert_found_range(response, False)


# ---------------------------------------------
# Test get request with an invalid request content type.
# The Content-Type header attribute specifies the format of the data in the
# request body so that receiver can parse it into appropriate format.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_content_type",
    [
        "application/javascript",
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "",
        "not valid",
        None,
    ],
)
def test_get_by_id_request_with_request_header_content_type_with_invalid_value(
    client: TestClient, http_request_header_content_type: str, test_db: Session
):
    mock = add_mock_data_to_db(test_db)
    headers = {**HEADERS, "Content-Type": f"{http_request_header_content_type}"}
    response = client.get(f"{BASE_PATH_V1}{ACTIVE_FORMS}/{mock.form7.id}", headers=headers)
    assert_found_range(response, False)


# ---------------------------------------------
# Test get request with an invalid request content type.
# The Content-Type header attribute specifies the format of the data in the
# request body so that receiver can parse it into appropriate format.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_content_type",
    [
        " application/javascript",
        " application/octet-stream",
        " multipart/form-data",
        " text/html",
        " text/plain",
        " ",
        " not valid",
    ],
)
def test_get_by_id_request_with_request_header_content_type_with_invalid_value_2(
    client: TestClient, http_request_header_content_type: str, test_db: Session
):
    mock = add_mock_data_to_db(test_db)
    headers = {"Content-Type": f"{http_request_header_content_type}"}

    # requests.exceptions.InvalidHeader: Invalid return character or
    # leading space in header: Content-Type
    with pytest.raises(requests.exceptions.InvalidHeader):
        client.get(f"{BASE_PATH_V1}{ACTIVE_FORMS}/{mock.form1.id}", headers=headers)
