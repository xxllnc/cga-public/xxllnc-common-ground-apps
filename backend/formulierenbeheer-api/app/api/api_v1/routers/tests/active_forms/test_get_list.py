import pytest
from app.api.api_v1.routers.tests import (
    ACTIVE_FORMS,
    CONTENT_TYPE_VALUE_APPLICATION_JSON,
    FORM_ACTIVE_FORM_FIELDS,
    HEADERS,
)
from app.api.api_v1.routers.tests.forms.test_mock_data import add_mock_data_to_db
from app.core.config import BASE_PATH_V1
from fastapi import status as http_status
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session
from typing import Final

FORM_LIST: Final[list[int]] = [2, 3, 4, 7, 11, 14, 20]
FORM_LIST_LEN: Final[int] = len(FORM_LIST)


# ---------------------------------------------
# Testing an API with GET requests
# Test with query parameter sort.
# ---------------------------------------------
@pytest.mark.parametrize(
    "sort_query_parameter_value, expected_order",
    [
        ("", FORM_LIST),
        ('["id","ASC"]', None),
        ('["name","ASC"]', FORM_LIST),
        ('["slug","ASC"]', FORM_LIST),
        ('["id","DESC"]', None),
        ('["name","DESC"]', list(reversed(FORM_LIST))),
        ('["slug","DESC"]', list(reversed(FORM_LIST))),
    ],
)
def test_get_forms_list_with_query_parameter_sort(
    client: TestClient,
    test_db: Session,
    sort_query_parameter_value: str,
    expected_order: list[int] | None,
):
    mock = add_mock_data_to_db(test_db)

    response = client.get(
        f"{BASE_PATH_V1}{ACTIVE_FORMS}?sort={sort_query_parameter_value}", headers=HEADERS
    )

    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == len(expected_order) if expected_order is not None else FORM_LIST_LEN

    if expected_order is not None:
        for index, no in enumerate(expected_order):
            assert len(res[index]) == FORM_ACTIVE_FORM_FIELDS
            assert res[index]["name"] == getattr(mock, f"form{no}").name
            assert res[index]["slug"] == getattr(mock, f"form{no}").slug
            assert res[index]["id"] is not None


# ---------------------------------------------
# Testing an API with GET requests.
# Test with query parameter sort.
# Sort by invalid fields.
# ---------------------------------------------
@pytest.mark.parametrize(
    "sort_query_parameter_value",
    [
        '["status","ASC"]',
        '["publish_date","ASC"]',
        '["publishDate","ASC"]',
        '["","ASC"]',
        '["unkownfield","ASC"]',
        '["_","ASC"]',
        '["url","ASC"]',
        '["form","ASC"]',
        '["config","ASC"]',
        '["organization_id","DESC"]',
        '["creator_organization_uuid","DESC"]',
        '["organization_url","DESC"]',
        '["organizationId","DESC"]',
        '["organizationUuid","DESC"]',
        '["organizationUrl","DESC"]',
        f'[{None},"ASC"]',
        '["name","unkown"]',
        '["name","order"]',
        '["name","_"]',
        '["name","LEFT"]',
        '["name","asc"]',
        '["name","AsC"]',
        '["name","desc"]',
        '["name","DeSc"]',
        f'["name", {None}]',
    ],
)
def test_get_forms_list_with_invalid_query_parameter_sort(
    client: TestClient, sort_query_parameter_value: str
):
    response = client.get(
        f"{BASE_PATH_V1}{ACTIVE_FORMS}?sort={sort_query_parameter_value}", headers=HEADERS
    )
    assert response.status_code == http_status.HTTP_400_BAD_REQUEST


# ---------------------------------------------
# Testing an API with GET requests
# Test with query parameter filter.
# ---------------------------------------------
@pytest.mark.parametrize(
    "filter_query_parameter_value, expected_order",
    [
        ("", FORM_LIST),
        ("{}", FORM_LIST),
        ('{"q":"aaaaaaaaaaaaa"}', []),
        ('{"q":"formulier "}', FORM_LIST),  # form name
        ('{"q":"FoRmUliEr "}', FORM_LIST),  # form name
        ('{"q":"formulier g"}', [7]),  # form name
        ('{"q":"formulier-"}', FORM_LIST),  # slug
        ('{"q":"fOrMuLiEr-"}', FORM_LIST),  # slug
        ('{"q":"formulier-g"}', [7]),  # slug
        ('{"q":"formulier"}', FORM_LIST),  # form name and slug
        ('{"q":"formulieren"}', []),  # url
        ('{"q":"ACTIVE"}', []),
        ('{"q":"active"}', []),
        ('{"q":"INACTIVE"}', []),
        ('{"q":"t"}', []),
        ('{"q":"T"}', []),
        ('{"q":"xx"}', FORM_LIST),
        ('{"q":"Xx"}', FORM_LIST),
    ],
)
def test_get_forms_list_with_query_parameter_filter(
    client: TestClient,
    test_db: Session,
    filter_query_parameter_value: str,
    expected_order: list[int],
):
    mock = add_mock_data_to_db(test_db)

    response = client.get(
        f"{BASE_PATH_V1}{ACTIVE_FORMS}?filter={filter_query_parameter_value}", headers=HEADERS
    )

    assert response.status_code == http_status.HTTP_200_OK

    res = response.json()
    assert len(res) == len(expected_order)

    for index, no in enumerate(expected_order):
        assert len(res[index]) == FORM_ACTIVE_FORM_FIELDS
        assert res[index]["name"] == getattr(mock, f"form{no}").name
        assert res[index]["slug"] == getattr(mock, f"form{no}").slug
        assert res[index]["id"] is not None


# ---------------------------------------------
# Testing an API with GET requests.
# Test with query parameter filter.
# Filter by invalid fields.
# ---------------------------------------------
@pytest.mark.parametrize(
    "filter_query_parameter_value",
    [
        '{"status":"INACTIVE"}',
        '{"status":"ACTIVE"}',
        '{"publishedAt_lte":"2020-12-31T21:59:59.999Z"}',
        '{"publishedAt_gte":"2111-11-01T21:59:59.999Z"}',
        '{""}',
        '{"aaaaaaaaaaaaa"}',
        '{"aaaaaaaaaaaaa":}',
        "{:}",
        '{:""}',
    ],
)
def test_get_forms_list_with_invalid_query_parameter_filter(
    client: TestClient, filter_query_parameter_value: str
):
    response = client.get(
        f"{BASE_PATH_V1}{ACTIVE_FORMS}?filter={filter_query_parameter_value}", headers=HEADERS
    )
    assert response.status_code == http_status.HTTP_400_BAD_REQUEST


# ---------------------------------------------
# Testing an API with GET requests.
# Test start range with an invalid end range.
# ---------------------------------------------
def test_get_forms_list_range_with_start_with_invalid_end(client: TestClient, test_db: Session):
    add_mock_data_to_db(test_db)

    # test start with an empty end range
    response = client.get(f"{BASE_PATH_V1}{ACTIVE_FORMS}?range=[3,0]", headers=HEADERS)

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]["loc"]) == 1
    assert res["detail"][0]["loc"][0] == "end"
    assert res["detail"][0]["msg"] == "range end cannot be before start"
    assert res["detail"][0]["type"] == "assertion_error"

    # test range end cannot be before start
    response = client.get(f"{BASE_PATH_V1}{ACTIVE_FORMS}?range=[3,2]", headers=HEADERS)

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]["loc"]) == 1
    assert res["detail"][0]["loc"][0] == "end"
    assert res["detail"][0]["msg"] == "range end cannot be before start"
    assert res["detail"][0]["type"] == "assertion_error"

    # test end value is greater than or equal to 0
    response = client.get(f"{BASE_PATH_V1}{ACTIVE_FORMS}?range=[0,-1]", headers=HEADERS)

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]["loc"]) == 1
    assert res["detail"][0]["loc"][0] == "end"
    assert res["detail"][0]["msg"] == "ensure this value is greater than or equal to 0"
    assert res["detail"][0]["type"] == "value_error.number.not_ge"

    # test page size cannot exceed 100
    response = client.get(
        f"{BASE_PATH_V1}{ACTIVE_FORMS}?range=[1111111111111111111111111111111,1111111111111111111111111111111222222222222222222222222222222222222222222222]",  # noqa: E501
        headers=HEADERS,
    )

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]["loc"]) == 1
    assert res["detail"][0]["loc"][0] == "end"
    assert res["detail"][0]["msg"] == "page size cannot exceed 100"
    assert res["detail"][0]["type"] == "assertion_error"


# ---------------------------------------------
# Testing an API with GET requests.
# Test start range without end range.
# Throw ValueError because the end range is not set.
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter",
    [
        "?range=[0]",
        "?range=[1]",
        "?range=[4]",
        "?range=[100]",
        "?range=[]",
        "?range=[aa]",
        "?range=[-1]",
        "?range=[,]",
        "?range=[0,aabbcc]",
        "?range=[aa,bb]",
        "?range=[,0]",
        "?range=[,1]",
        "?range=[,4]",
        "?range=[,100]",
        "?range=[,]",
        "?range=[,aa]",
        "?range=[,-1]",
    ],
)
def test_get_forms_list_range_with_start_without_end(
    client: TestClient, test_db: Session, query_parameter: str
):
    add_mock_data_to_db(test_db)

    with pytest.raises(ValueError):
        client.get(f"{BASE_PATH_V1}{ACTIVE_FORMS}{query_parameter}", headers=HEADERS)


# ---------------------------------------------
# Testing an API with GET requests.
# Test start range with an end range.
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter, response_length, header_content_range",
    [
        ("?range=[0,3]", 4, f"results 0-3/{FORM_LIST_LEN}"),
        ("?range=[0,1]", 2, f"results 0-1/{FORM_LIST_LEN}"),
        ("?range=[1,1]", 1, f"results 1-1/{FORM_LIST_LEN}"),
    ],
)
def test_get_forms_list_range_with_start_with_end(
    client: TestClient,
    test_db: Session,
    query_parameter: str,
    response_length: int,
    header_content_range: str,
):
    add_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{ACTIVE_FORMS}{query_parameter}", headers=HEADERS)

    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == int(f"{response_length}")

    for index, no in enumerate(res):
        assert len(res[index]) == FORM_ACTIVE_FORM_FIELDS
        assert res[index]["name"] is not None
        assert res[index]["slug"] is not None
        assert res[index]["id"] is not None

    headers = response.headers
    assert len(headers) == 3
    assert headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert headers["content-range"] == f"{header_content_range}"
