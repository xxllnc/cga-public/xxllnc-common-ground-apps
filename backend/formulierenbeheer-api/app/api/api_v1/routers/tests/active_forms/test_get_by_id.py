import pytest
import uuid
from app.api.api_v1.routers.tests import ACTIVE_FORMS, FORM_DETAILS_FIELDS, HEADERS
from app.api.api_v1.routers.tests.forms.test_mock_data import add_mock_data_to_db
from app.api.api_v1.routers.tests.helper import convert_db_form_to_dict
from app.core.config import BASE_PATH_V1
from app.resources import error_messages
from fastapi import status as http_status
from fastapi.testclient import TestClient
from requests.models import Response
from sqlalchemy.orm import Session
from typing import Final, Union

ACTIVE_FORM_LIST: Final[list[int]] = [2, 3, 4, 7, 11, 14, 20]
IN_ACTIVE_FORM_LIST: Final[list[int]] = [1, 5, 6, 8, 9, 10, 18]


def _assert_not_found(response: Response, id: Union[uuid.UUID, str]):
    assert response.status_code == http_status.HTTP_404_NOT_FOUND
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["msg"] == error_messages.FORM_NOT_FOUND_WITH.format(id)
    assert res["detail"][0]["detail"] == error_messages.NOT_FOUND


def _assert_found(response: Response, expected: dict):
    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == FORM_DETAILS_FIELDS
    assert res["id"] != expected["id"]
    assert res["name"] == expected["name"]
    assert res["slug"] == expected["slug"]
    assert res["status"] == expected["status"]
    assert res["form"] == expected["form"]
    assert res["config"] == expected["config"]
    if res["config"]:
        assert res["config"] == expected["config"]
    else:
        assert type(res["config"]) is list
    assert res["publishDate"] == str(expected["publishDate"])
    assert res["rights"] == expected["rights"]
    assert res["organizationUuid"] == str(expected["organizationUuid"])
    assert res["organizationName"] == expected["organizationName"]
    assert res["private"] == expected["private"]
    assert res["author"] is not None
    assert "shares" not in res


# ---------------------------------------------
# Get "/active/forms/{id}"
# Retrieve a single form, given its unique id or slug
# Test status is inactive and/or (publishdate is not set or
# publishdate > actual date)
# ---------------------------------------------
@pytest.mark.parametrize("mock_id", IN_ACTIVE_FORM_LIST)
def test_get_form_inactive_forms(client: TestClient, test_db: Session, mock_id: int):
    mock = add_mock_data_to_db(test_db)

    test_form = getattr(mock, f"form{mock_id}")
    for id in [test_form.id, test_form.slug, test_form.slug.upper()]:
        response = client.get(f"{BASE_PATH_V1}{ACTIVE_FORMS}/{id}", headers=HEADERS)
        _assert_not_found(response, id)


# ---------------------------------------------
# Get "/active/forms/{id}"
# Retrieve a single form, given its unique id or slug
# Test status is active and publishdate <= actual date
# ---------------------------------------------
@pytest.mark.parametrize("mock_id", ACTIVE_FORM_LIST)
def test_get_form_active_forms(client: TestClient, test_db: Session, mock_id: int):
    mock = add_mock_data_to_db(test_db)

    test_form = getattr(mock, f"form{mock_id}")
    expected = convert_db_form_to_dict(test_form)
    for id in [test_form.id, test_form.slug, test_form.slug.upper()]:
        response = client.get(f"{BASE_PATH_V1}{ACTIVE_FORMS}/{id}", headers=HEADERS)
        _assert_found(response, expected)


# ---------------------------------------------
# Get "/forms/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_id_value",
    [
        "unknown",
        "0",
        "__",
        "<>",
        "111111111111111111111111111111111111111111111111111111111",
    ],
)
def test_get_forms_with_invalid_id_value(
    client: TestClient, test_db: Session, invalid_id_value: str
):
    add_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{ACTIVE_FORMS}/{invalid_id_value}", headers=HEADERS)
    _assert_not_found(response, invalid_id_value)
