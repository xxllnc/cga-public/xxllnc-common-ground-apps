import pytest
from app.api.api_v1.routers.tests import (
    CONTENT_TYPE_VALUE_APPLICATION_JSON,
    FORM_GET_ONE_RESPONSE_DETAILS_FIELDS,
    FORM_SHARES_FIELDS,
    FORMS,
)
from app.api.api_v1.routers.tests.forms.test_mock_data import add_mock_data_to_db
from app.core.config import BASE_PATH_V1
from app.db import models
from app.resources import error_messages
from fastapi import status as http_status
from fastapi.testclient import TestClient
from requests.models import Response
from sqlalchemy.orm import Session
from typing import Union


def _assert_found(response: Response, expected: models.FormManagement):
    form_management = expected.__dict__
    form_details = form_management["form_details"].__dict__
    organization = form_management["organization"].__dict__

    assert response.status_code == http_status.HTTP_200_OK
    form = response.json()
    assert len(form) == FORM_GET_ONE_RESPONSE_DETAILS_FIELDS

    assert form["id"] == str(form_management["id"])
    assert form["form"] == form_details["form"]
    assert form["private"] == form_details["private"]
    assert isinstance(form["shares"], list)
    assert len(form["shares"]) > 0
    assert form["author"] is not None
    assert form["name"] == form_management["name"]
    assert form["slug"] == form_management["slug"]
    assert "status" not in form
    assert "config" not in form
    assert "organizationUuid" not in form
    assert "organizationName" not in form
    assert "rights" not in form

    share = form["shares"][0]
    assert len(share) == FORM_SHARES_FIELDS
    assert share["id"] == str(form_management["id"])
    assert share["name"] == form_management["name"]
    assert share["slug"] == form_management["slug"]
    assert share["status"] == form_management["status"]
    if form_management["config"]:
        assert share["config"] == form_management["config"]
    else:
        assert type(share["config"]) is list
    assert share["organizationUuid"] == str(organization["id"])
    assert share["organizationName"] == organization["name"]
    if form_management["publish_date"]:
        assert share["publishDate"] == str(form_management["publish_date"])
    else:
        assert share["publishDate"] is None
    assert share["rights"] == form_management["rights"]
    assert "form" not in share
    assert "private" not in share
    assert "author" not in share
    assert "shares" not in share


def _assert_not_found(response: Response, id: str):
    assert response.status_code == http_status.HTTP_404_NOT_FOUND
    res = response.json()

    assert len(res["detail"]) == 1
    assert res["detail"][0]["detail"] == error_messages.NOT_FOUND
    # assert str(res["detail"][0]["msg"]) == error_messages.FORM_NOT_FOUND_WITH.format(str(id))


# ---------------------------------------------
# Get "/forms/{id | slug}"
# ---------------------------------------------
@pytest.mark.parametrize("headers", [None, {"Content-Type": CONTENT_TYPE_VALUE_APPLICATION_JSON}])
@pytest.mark.parametrize("id_or_slug", ["id", "slug"])
def test_get_forms_by_id_without_content_type(
    client: TestClient, test_db: Session, headers: Union[dict, None], id_or_slug: str
):
    mock = add_mock_data_to_db(test_db)

    for idx in range(1, 11):
        form = getattr(mock, f"form{idx}")
        form_id = form.id if id_or_slug == "id" else form.slug
        response = client.get(f"{BASE_PATH_V1}{FORMS}/{form_id}", headers=headers)
        _assert_found(response, form)


# ---------------------------------------------
# Get "/forms/{id}"
# Test ignore the query parameters.
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter",
    [
        "",
        "unknown",
        "0",
        "__",
        "<>",
        "id=1 AND 1::int=1",
        "?id=1 AND 1::int=1",
        "&id=1 AND 1::int=1",
        "id=1 UNION ALL SELECT NULL,version(),NULL LIMIT 1 OFFSET 1--",
        None,
    ],
)
def test_get_forms_with_addition_query_params(
    client: TestClient, test_db: Session, query_parameter: str
):
    mock = add_mock_data_to_db(test_db)

    for id in [mock.form1.id, mock.form1.slug]:
        response = client.get(f"{BASE_PATH_V1}{FORMS}/{id}?{query_parameter}")
        _assert_found(response, mock.form1)


# ---------------------------------------------
# Get "/forms/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_id_value",
    [
        "unknown",
        "0",
        "__",
        "<>",
        "111111111111111111111111111111111111111111111111111111111",
    ],
)
def test_get_forms_with_invalid_id_value(
    client: TestClient, test_db: Session, invalid_id_value: str
):
    add_mock_data_to_db(test_db)
    response = client.get(f"{BASE_PATH_V1}{FORMS}/{invalid_id_value}")
    _assert_not_found(response, invalid_id_value)


# ---------------------------------------------
# Get "/forms/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_id_value",
    [
        " AND 1::int=1",
        " UNION ALL SELECT NULL,version(),NULL LIMIT 1 OFFSET 1--",
        " or 1=1",
        " or = 1' or '1' = '1",
        "not valid",
    ],
)
def test_get_forms_with_invalid_id_value_2(
    client: TestClient, test_db: Session, invalid_id_value: str
):
    mock = add_mock_data_to_db(test_db)
    form_id: str = f"{str(mock.form1.id)}{invalid_id_value}"
    response = client.get(f"{BASE_PATH_V1}{FORMS}/{id}")
    _assert_not_found(response, form_id)


# ---------------------------------------------
# Get "/forms/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_id_value",
    [
        ">'\"()%26%25<acx><ScRiPt%20>jLeb(9968)</ScRiPt>",
        "text/plain",
        "application/*",
        "*/*",
    ],
)
def test_get_forms_with_invalid_id_value_3(
    client: TestClient, test_db: Session, invalid_id_value: str
):
    add_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{FORMS}/{invalid_id_value}")

    assert response.status_code == http_status.HTTP_404_NOT_FOUND
    res = response.json()
    assert len(res["detail"]) == 9
    assert res["detail"] == error_messages.NOT_FOUND


# ---------------------------------------------
# Get "/forms/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
def test_get_forms_with_invalid_id_value_4(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    invalid_id = "..;/"
    invalid_id_test = ".."

    test_id: str = f"{str(mock.form1.id)}{invalid_id_test}"

    response = client.get(f"{BASE_PATH_V1}{FORMS}/{mock.form1.id}{invalid_id}")

    _assert_not_found(response, test_id)
