#!/usr/bin/env python3

from app.api.api_v1.routers.tests.helper import org1, org2, org3, url1, url2, url3
from app.db import models
from app.db.models import Status
from collections import namedtuple
from datetime import date
from sqlalchemy.orm import Session

# Declaring namedtuple() as return value
MockData = namedtuple(
    "MockData",
    [
        "form1",
        "form2",
        "form3",
        "form4",
        "form5",
        "form6",
        "form7",
        "form8",
        "form9",
        "form10",
        "form11",
        "form12",
        "form13",
        "form14",
        "form15",
        "form16",
        "form17",
        "form18",
        "form19",
        "form20",
        "form21",
        "form22",
        "form23",
        "form24",
        "STATUS_INACTIVE",
        "STATUS_ACTIVE",
        "org_uuid1",
        "org_uuid2",
        "org_uuid3",
    ],
)


#  Same data als initial_data.
def add_mock_data_to_db(db: Session) -> MockData:

    organization1 = models.Organization(**org1)
    organization2 = models.Organization(**org2)
    organization3 = models.Organization(**org3)

    db.add(organization1)
    db.add(organization2)
    db.add(organization3)
    db.flush()

    # Add different details so each formmanagement has a unique form
    details1 = models.FormDetails(form={"field": "value"}, private=False, author=organization1.sid)
    details2 = models.FormDetails(form={"field": "value"}, private=False, author=organization1.sid)
    details3 = models.FormDetails(form={"field": "value"}, private=True, author=organization1.sid)
    details4 = models.FormDetails(form={"field": "value"}, private=True, author=organization1.sid)
    details5 = models.FormDetails(form={"field": "value"}, private=True, author=organization1.sid)
    details6 = models.FormDetails(form={"field": "value"}, private=True, author=organization1.sid)
    details7 = models.FormDetails(form={"field": "value"}, private=True, author=organization1.sid)
    details8 = models.FormDetails(form={"field": "value"}, private=True, author=organization1.sid)
    details9 = models.FormDetails(form={"field": "value"}, private=True, author=organization1.sid)
    details10 = models.FormDetails(form={"field": "value"}, private=True, author=organization1.sid)
    details11 = models.FormDetails(form={"field": "value"}, private=True, author=organization1.sid)
    details12 = models.FormDetails(
        form={"field": "value"}, private=False, author=organization1.sid
    )
    details13 = models.FormDetails(
        form={"field": "value"}, private=False, author=organization1.sid
    )
    details14 = models.FormDetails(
        form={"field": "value"}, private=False, author=organization1.sid
    )
    details15 = models.FormDetails(form={"field": "value"}, private=True, author=organization1.sid)
    details16 = models.FormDetails(
        form={"field": "value"}, private=False, author=organization1.sid
    )
    details17 = models.FormDetails(form={"field": "value"}, private=True, author=organization3.sid)

    db.add(details1)
    db.add(details2)
    db.add(details3)
    db.add(details4)
    db.add(details5)
    db.add(details6)
    db.add(details7)
    db.add(details8)
    db.add(details9)
    db.add(details10)
    db.add(details11)
    db.add(details12)
    db.add(details13)
    db.add(details14)
    db.add(details15)
    db.add(details16)
    db.add(details17)
    db.flush()
    url_1 = models.Url(
        **url1,
        organization_id=organization1.sid,
    )
    url_2 = models.Url(
        **url2,
        organization_id=organization2.sid,
    )
    url_3 = models.Url(
        **url3,
        organization_id=organization3.sid,
    )

    db.add(url_1)
    db.add(url_2)
    db.add(url_3)

    form1 = models.FormManagement(
        name="formulier a",
        slug="formulier-a",
        config=[],
        publish_date=None,
        status=Status.INACTIVE,
        rights=models.Rights.OWNER,
        form_details=details1,
        organization=organization1,
    )
    form2 = models.FormManagement(
        name="formulier b",
        slug="formulier-b",
        config=[],
        publish_date=date(2021, 8, 24),
        status=Status.ACTIVE,
        rights=models.Rights.OWNER,
        form_details=details2,
        organization=organization1,
    )
    form3 = models.FormManagement(
        name="formulier c",
        slug="formulier-c",
        config=[],
        publish_date=date(2021, 8, 24),
        status=Status.ACTIVE,
        rights=models.Rights.OWNER,
        form_details=details3,
        organization=organization1,
    )
    form4 = models.FormManagement(
        name="formulier d",
        slug="formulier-d",
        config=[],
        publish_date=date(2021, 8, 24),
        status=Status.ACTIVE,
        rights=models.Rights.OWNER,
        form_details=details4,
        organization=organization1,
    )
    form5 = models.FormManagement(
        name="formulier e",
        slug="formulier-e",
        config=[],
        publish_date=None,
        status=Status.INACTIVE,
        rights=models.Rights.OWNER,
        form_details=details5,
        organization=organization1,
    )
    form6 = models.FormManagement(
        name="formulier f2",
        slug="formulier-f",
        config=[],
        publish_date=date(2011, 11, 4),
        status=Status.INACTIVE,
        rights=models.Rights.OWNER,
        form_details=details6,
        organization=organization1,
    )
    form7 = models.FormManagement(
        name="formulier g2",
        slug="formulier-g",
        config=[],
        publish_date=date(2021, 8, 24),
        status=Status.ACTIVE,
        rights=models.Rights.OWNER,
        form_details=details7,
        organization=organization1,
    )
    form8 = models.FormManagement(
        name="formulier h2",
        slug="formulier-h",
        config=[],
        publish_date=date(2111, 11, 4),
        status=Status.ACTIVE,
        rights=models.Rights.OWNER,
        form_details=details8,
        organization=organization1,
    )
    form9 = models.FormManagement(
        name="formulier i2",
        slug="formulier-i",
        config=[],
        publish_date=date(2111, 11, 1),
        status=Status.INACTIVE,
        rights=models.Rights.OWNER,
        form_details=details9,
        organization=organization1,
    )
    form10 = models.FormManagement(
        name="formulier j2",
        slug="formulier-j",
        config=[],
        status=Status.INACTIVE,
        publish_date=date.today(),
        rights=models.Rights.OWNER,
        form_details=details10,
        organization=organization1,
    )

    # Multiple private forms for one form_details
    form11 = models.FormManagement(
        name="formulier k",
        slug="formulier-k",
        config=[],
        publish_date=date.today(),
        status=Status.ACTIVE,
        rights=models.Rights.OWNER,
        form_details=details11,
        organization=organization1,
    )
    form12 = models.FormManagement(
        name="formulier k1",
        slug="formulier-k1",
        config=[],
        publish_date=date.today(),
        status=Status.ACTIVE,
        rights=models.Rights.EDITOR,
        form_details=details11,
        organization=organization2,
    )
    form13 = models.FormManagement(
        name="formulier k2",
        slug="formulier-k2",
        config=[],
        publish_date=date.today(),
        status=Status.INACTIVE,
        rights=models.Rights.VIEWER,
        form_details=details11,
        organization=organization3,
    )

    # Multiple public forms for one form_details
    form14 = models.FormManagement(
        name="formulier l",
        slug="formulier-l",
        config=[],
        publish_date=date.today(),
        status=Status.ACTIVE,
        rights=models.Rights.OWNER,
        form_details=details12,  # public
        organization=organization1,
    )
    form15 = models.FormManagement(
        name="formulier l1",
        slug="formulier-l1",
        config=[],
        publish_date=date.today(),
        status=Status.ACTIVE,
        rights=models.Rights.EDITOR,
        form_details=details12,  # public
        organization=organization2,
    )
    form16 = models.FormManagement(
        name="formulier l2",
        slug="formulier-l2",
        config=[],
        publish_date=date.today(),
        status=Status.INACTIVE,
        rights=models.Rights.VIEWER,
        form_details=details12,  # public
        organization=organization3,
    )

    form17 = models.FormManagement(
        name="formulier m",
        slug="formulier-m",
        config=[],
        publish_date=date.today(),
        status=Status.ACTIVE,
        rights=models.Rights.OWNER,
        form_details=details13,  # public
        organization=organization2,
    )
    form18 = models.FormManagement(
        name="formulier n",
        slug="formulier-n",
        config=[],
        publish_date=date.today(),
        status=Status.INACTIVE,
        rights=models.Rights.OWNER,
        form_details=details14,  # public
        organization=organization3,
    )
    form19 = models.FormManagement(
        name="formulier o",
        slug="formulier-o",
        config=[],
        publish_date=date.today(),
        status=Status.ACTIVE,
        rights=models.Rights.OWNER,
        form_details=details15,  # private
        organization=organization3,
    )

    # Multiple public forms for one form_details
    form20 = models.FormManagement(
        name="formulier p",
        slug="formulier-p",
        config=[],
        publish_date=date.today(),
        status=Status.ACTIVE,
        rights=models.Rights.EDITOR,
        form_details=details16,  # public
        organization=organization1,
    )
    form21 = models.FormManagement(
        name="formulier p1",
        slug="formulier-p1",
        config=[],
        publish_date=date.today(),
        status=Status.ACTIVE,
        rights=models.Rights.OWNER,
        form_details=details16,  # public
        organization=organization2,
    )
    form22 = models.FormManagement(
        name="formulier p2",
        slug="formulier-p2",
        config=[],
        publish_date=date.today(),
        status=Status.INACTIVE,
        rights=models.Rights.VIEWER,
        form_details=details16,  # public
        organization=organization3,
    )
    form23 = models.FormManagement(
        name="formulier 23",
        slug="formulier-23",
        config=[],
        publish_date=date.today(),
        status=Status.INACTIVE,
        rights=models.Rights.OWNER,
        form_details=details17,  # private org 3
        organization=organization3,
    )
    form24 = models.FormManagement(
        name="formulier 24",
        slug="formulier-24",
        config=[],
        publish_date=date.today(),
        status=Status.INACTIVE,
        rights=models.Rights.EDITOR,
        form_details=details17,  # private org 3
        organization=organization1,
    )

    db.add(form1)
    db.add(form2)
    db.add(form3)
    db.add(form4)
    db.add(form5)
    db.add(form6)
    db.add(form7)
    db.add(form8)
    db.add(form9)
    db.add(form10)
    db.add(form11)
    db.add(form12)
    db.add(form13)
    db.add(form14)
    db.add(form15)
    db.add(form16)
    db.add(form17)
    db.add(form18)
    db.add(form19)
    db.add(form20)
    db.add(form21)
    db.add(form22)
    db.add(form23)
    db.add(form24)

    db.flush()

    return MockData(
        form1,
        form2,
        form3,
        form4,
        form5,
        form6,
        form7,
        form8,
        form9,
        form10,
        form11,
        form12,
        form13,
        form14,
        form15,
        form16,
        form17,
        form18,
        form19,
        form20,
        form21,
        form22,
        form23,
        form24,
        Status.INACTIVE,
        Status.ACTIVE,
        org_uuid1=org1["id"],
        org_uuid2=org2["id"],
        org_uuid3=org3["id"],
    )
