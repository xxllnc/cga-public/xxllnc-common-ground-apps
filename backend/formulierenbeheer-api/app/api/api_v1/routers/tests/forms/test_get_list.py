import pytest
from app.api.api_v1.routers.tests import (
    CONTENT_TYPE_VALUE_APPLICATION_JSON,
    FORM_LIST_FIELDS,
    FORMS,
)
from app.api.api_v1.routers.tests.asserts import assert_found
from app.api.api_v1.routers.tests.forms.test_mock_data import add_mock_data_to_db
from app.api.api_v1.routers.tests.parameters import (
    FORM_LIST,
    FORM_LIST_LEN,
    invalid_order_paramter,
    invalid_q_filter_values,
    invalid_sort_and_order,
    range_parameter,
)
from app.api.api_v1.routers.tests.test_generic_functions import (
    test_get_list,
    test_get_list_range_with_invalid_start_end_combination,
)
from app.core.config import BASE_PATH_V1
from fastapi import status as http_status
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session
from typing import Union


# ---------------------------------------------
# Testing the API with a GET requests to retreive all the forms.
# List is not emtpy.
# ---------------------------------------------
def test_get_method_list(client: TestClient, test_db: Session):
    add_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{FORMS}")
    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == FORM_LIST_LEN


# ---------------------------------------------
# Testing an API with GET requests
#
# Return the following forms:
# - All the (in)active public/private forms for the organization 'org1'.
#   There are no specific requirements on the access rights.
# - All the (in)active public forms for organizations with
#   access right 'Owner'.
# ---------------------------------------------
def test_get_forms_list(client: TestClient, test_db: Session):
    test_get_list(client, test_db)


# ---------------------------------------------
# Testing an API with GET requests.
# Sort by valid fields. Sort order is ascending
#
# Return the following forms:
# - All the (in)active public/private forms for the organization 'org1'.
#   There are no specific requirements on the access rights.
# - All the (in)active public forms for organizations with
#   access right 'Owner'.
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter_sort_value, expected_order",
    [
        ('["id","ASC"]', "impossible to determine"),
        ('["id","DESC"]', "impossible to determine"),
        ('["name","ASC"]', FORM_LIST),
        ('["name","DESC"]', FORM_LIST.reverse),
        ('["slug","ASC"]', FORM_LIST),
        ('["slug","DESC"]', FORM_LIST.reverse),
        ('["status","ASC"]', "impossible to determine"),
        ('["status","DESC"]', "impossible to determine"),
        ('["publish_date","ASC"]', "impossible to determine"),
        ('["publish_date","ASC"]', "impossible to determine"),
        ('["publishDate","ASC"]', "impossible to determine"),
        ('["publishDate","ASC"]', "impossible to determine"),
    ],
)
def test_get_forms_list_sort_by_valid_field_asc(
    client: TestClient,
    test_db: Session,
    query_parameter_sort_value: str,
    expected_order: Union[str, list[int]],
):
    mock = add_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{FORMS}?sort={query_parameter_sort_value}")
    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == FORM_LIST_LEN
    if isinstance(expected_order, list):
        for index, form in enumerate(res):
            assert len(form) == FORM_LIST_FIELDS
            assert_found(form, getattr(mock, f"form{expected_order[index]}"))


# ---------------------------------------------
# Testing an API with GET requests.
# Sort by invalid fields.
# ---------------------------------------------
@pytest.mark.parametrize(
    "sort_query_parameter_value",
    [
        "",
        "unkownfield",
        "_",
        "url",
        "form",
        "config",
        "organization_id",
        "creator_organization_uuid",
        "organization_url",
        "organizationId",
        "organizationUuid",
        "organizationUrl",
        None,
    ],
)
@pytest.mark.parametrize("order", ["ASC", "DESC"])
def test_get_forms_list_sort_by_invalid_field(
    client: TestClient, sort_query_parameter_value: str, order: str
):
    response = client.get(f'{BASE_PATH_V1}{FORMS}?sort=["{sort_query_parameter_value}","{order}"]')
    assert response.status_code == http_status.HTTP_400_BAD_REQUEST


# ---------------------------------------------
# Testing an API with GET requests.
# Sort by invalid order.
# ---------------------------------------------
@pytest.mark.parametrize("query_parameter_order_value", invalid_order_paramter)
def test_get_forms_list_sort_by_invalid_order(
    client: TestClient, query_parameter_order_value: str
):
    response = client.get(f'{BASE_PATH_V1}{FORMS}?sort=["name","{query_parameter_order_value}"]')
    assert response.status_code == http_status.HTTP_400_BAD_REQUEST


# ---------------------------------------------
# Testing an API with GET requests.
# Sort by status. Sort order is ascending
#
# Return the following forms:
# - All the (in)active public/private forms for the organization 'org1'.
#   There are no specific requirements on the access rights.
# - All the (in)active public forms for organizations with
#   access right 'Owner'.
# ---------------------------------------------
def test_get_forms_list_sort_by_status_asc(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    response = client.get(f'{BASE_PATH_V1}{FORMS}?sort=["status","ASC"]')

    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == FORM_LIST_LEN
    assert res[0]["status"] == mock.form2.status
    assert res[1]["status"] == mock.form3.status
    assert res[2]["status"] == mock.form4.status
    assert res[3]["status"] == mock.form7.status
    assert res[4]["status"] == mock.form8.status
    assert res[5]["status"] == mock.form11.status
    assert res[6]["status"] == mock.form14.status
    assert res[7]["status"] == mock.form17.status
    assert res[8]["status"] == mock.form20.status
    assert res[9]["status"] == mock.form1.status
    assert res[10]["status"] == mock.form5.status
    assert res[11]["status"] == mock.form6.status
    assert res[12]["status"] == mock.form9.status
    assert res[13]["status"] == mock.form10.status
    assert res[14]["status"] == mock.form18.status


# ---------------------------------------------
# Testing an API with GET requests.
# Sort by status. Sort order is descending
#
# Return the following forms:
# - All the (in)active public/private forms for the organization 'org1'.
#   There are no specific requirements on the access rights.
# - All the (in)active public forms for organizations with
#   access right 'Owner'.
# ---------------------------------------------
def test_get_forms_list_sort_by_status_desc(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    response = client.get(f'{BASE_PATH_V1}{FORMS}?sort=["status","DESC"]')

    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == FORM_LIST_LEN
    assert res[0]["status"] == mock.form1.status
    assert res[1]["status"] == mock.form5.status
    assert res[2]["status"] == mock.form6.status
    assert res[3]["status"] == mock.form9.status
    assert res[4]["status"] == mock.form10.status
    assert res[5]["status"] == mock.form18.status
    assert res[6]["status"] == mock.form24.status
    assert res[7]["status"] == mock.form2.status
    assert res[8]["status"] == mock.form3.status
    assert res[9]["status"] == mock.form4.status
    assert res[10]["status"] == mock.form7.status
    assert res[11]["status"] == mock.form8.status
    assert res[12]["status"] == mock.form11.status
    assert res[13]["status"] == mock.form14.status
    assert res[14]["status"] == mock.form17.status
    assert res[15]["status"] == mock.form20.status


# ---------------------------------------------
# Testing an API with GET requests.
# Test invalid sort and order combinations.
# ---------------------------------------------
@pytest.mark.parametrize("sort_query_parameter_value", invalid_sort_and_order)
def test_get_forms_list_invalid_sort_order_combinations(
    client: TestClient, sort_query_parameter_value: str
):
    response = client.get(f"{BASE_PATH_V1}{FORMS}?sort={sort_query_parameter_value}")

    assert response.status_code == http_status.HTTP_400_BAD_REQUEST
    res = response.json()
    assert (
        res["detail"] == f"Invalid sort order: {sort_query_parameter_value}. sort; needs to "
        "be a JSON list with two elements: The field to sort by, and the "
        "order (ASC or DESC). Example: `[field, sortOrder]` where sortOrder "
        "is ASC or DESC"
    )


# ---------------------------------------------
# Testing an API with GET requests.
# Test start range without end range.
# Throw ValueError because the end range is not set.
# ---------------------------------------------
@pytest.mark.parametrize("query_parameter", range_parameter)
def test_get_forms_list_range_with_start_without_end(
    client: TestClient, test_db: Session, query_parameter: str
):
    add_mock_data_to_db(test_db)

    with pytest.raises(ValueError):
        client.get(f"{BASE_PATH_V1}{FORMS}?{query_parameter}")


# ---------------------------------------------
# Testing an API with GET requests.
# Test start range with an end range.
#
# Return the following forms:
# - All the (in)active public/private forms for the organization 'org1'.
#   There are no specific requirements on the access rights.
# - All the (in)active public forms for organizations with
#   access right 'Owner'.
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter, response_length, header_content_range",
    [
        ("range=[0,3]", 4, f"results 0-3/{FORM_LIST_LEN}"),
        ("range=[1,2]", 2, f"results 1-2/{FORM_LIST_LEN}"),
        ("range=[3,3]", 1, f"results 3-3/{FORM_LIST_LEN}"),
    ],
)
def test_get_forms_list_range_with_start_with_end(
    client: TestClient,
    test_db: Session,
    query_parameter: str,
    response_length: int,
    header_content_range: str,
):
    add_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{FORMS}?{query_parameter}")
    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == int(f"{response_length}")

    headers = response.headers
    assert len(headers) == 3
    assert headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert headers["content-range"] == f"{header_content_range}"


# ---------------------------------------------
# Testing an API with GET requests.
# Test invalid start range with end range combinations.
# Throw ValueError
# ---------------------------------------------
def test_get_forms_list_range_with_invalid_start_end_combination(
    client: TestClient,
):
    test_get_list_range_with_invalid_start_end_combination(client)


# ---------------------------------------------
# Testing an API with GET requests.
# Test end range without start range.
# Throw ValueError because the start range is not set.
# ---------------------------------------------
@pytest.mark.parametrize("query_parameter", range_parameter)
def test_get_forms_list_range_with_end_without_start(
    client: TestClient, test_db: Session, query_parameter: str
):
    add_mock_data_to_db(test_db)

    with pytest.raises(ValueError):
        client.get(f"{BASE_PATH_V1}{FORMS}?{query_parameter}")


# ---------------------------------------------
# Testing an API with GET requests.
# Test fulltext search with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize("filter_query_parameter_value", invalid_q_filter_values)
def test_get_forms_list_invalid_full_text_filter(
    client: TestClient, test_db: Session, filter_query_parameter_value: str
):
    add_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{FORMS}?filter={filter_query_parameter_value}")

    assert response.status_code == http_status.HTTP_400_BAD_REQUEST


# ---------------------------------------------
# Testing an API with GET requests.
# Test filter option for field publish_date.
# ---------------------------------------------
@pytest.mark.parametrize(
    "filter_query_parameter, results, resultform",
    [
        (
            '{"publishedAt_gte":"2021-8-23T22:00:00.000Z","publishedAt_lte":"2021-8-24T21:59:59.999Z"}',  # One specific day/range # noqa: E501
            4,
            "formulier b",
        ),
        (
            '{"publishedAt_lte":"2020-12-31T21:59:59.999Z"}',  # Everything before 2021 # noqa: E501
            1,
            "formulier f2",
        ),
        (
            '{"publishedAt_gte":"2111-11-01T21:59:59.999Z"}',  # Everything after 2111-11-01 # noqa: E501
            1,
            "formulier h2",
        ),
    ],
)
def test_get_forms_list_filter_option_publish_date(
    client: TestClient,
    test_db: Session,
    filter_query_parameter: str,
    results: int,
    resultform: str,
):
    add_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{FORMS}?filter={filter_query_parameter}")
    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == results
    assert res[0]["name"] == resultform


# ---------------------------------------------
# Testing an API with GET requests.
# Test filter with fulltext search ('q') and filter options 'status' and
# 'private'.
#
# Return the following forms:
# - All the (in)active public/private forms for the organization 'org1'.
#   There are no specific requirements on the access rights.
# - All the (in)active public forms for organizations with
#   access right 'Owner'.
# ---------------------------------------------
@pytest.mark.parametrize(
    "filter_query_parameter_value, results",
    [
        ("{}", FORM_LIST_LEN),
        ('{"status":"INACTIVE"}', 7),
        ('{"status":"ACTIVE"}', 9),
        # ('{"status":""}', 8),  # TODO CGA-1385 fix DataError
        # ('{"status":"None"}', 8),  # TODO CGA-1385 fix DataError
        # ('{"status":"iNaCtIvE"}', 0),  # TODO CGA-1385 fix DataError
        # ('{"status":"aCtIvE"}', 0),  # TODO CGA-1385 fix DataError
        # ('{"status":"in"}', 0),  # TODO CGA-1385 fix DataError
        # ('{"status":"ac"}', 0),  # TODO CGA-1385 fix DataError
        # ('{"status":"a"}', 0),  # TODO CGA-1385 fix DataError
        ('{"private": false}', 6),
        ('{"private": true}', 10),
        ('{"q":"aaaaaaaaaaaaa"}', 0),
        ('{"q":"formulier "}', FORM_LIST_LEN),  # form name
        ('{"q":"FoRmUliEr "}', FORM_LIST_LEN),  # form name
        ('{"q":"formulier g"}', 1),  # form name
        ('{"q":"formulier-"}', FORM_LIST_LEN),  # slug
        ('{"q":"fOrMuLiEr-"}', FORM_LIST_LEN),  # slug
        ('{"q":"formulier-g"}', 1),  # slug
        ('{"q":"formulier"}', FORM_LIST_LEN),  # form name and slug
        ('{"q":"formulieren"}', 0),
        ('{"q":"ACTIVE"}', 0),
        ('{"q":"active"}', 0),
        ('{"q":"INACTIVE"}', 0),
        ('{"q":"t"}', 1),
        ('{"q":"T"}', 1),
        ('{"q":"","status":"INACTIVE"}', 7),
        ('{"q":"aaaaaaaaaaaaa","status":"INACTIVE"}', 0),
        ('{"q":"formulier ","status":"INACTIVE"}', 7),  # form name
        ('{"q":"FoRmUliEr ","status":"INACTIVE"}', 7),  # form name
        ('{"q":"formulier g","status":"INACTIVE"}', 0),  # form name
        ('{"q":"formulier-","status":"INACTIVE"}', 7),  # slug
        ('{"q":"fOrMuLiEr-","status":"INACTIVE"}', 7),  # slug
        ('{"q":"formulier-g","status":"INACTIVE"}', 0),  # slug
        (
            '{"q":"formulier","status":"INACTIVE"}',
            7,
        ),  # form name and slug
        ('{"q":"formulieren","status":"INACTIVE"}', 0),
        ('{"q":"ACTIVE","status":"INACTIVE"}', 0),
        ('{"q":"active","status":"INACTIVE"}', 0),
        ('{"q":"INACTIVE","status":"INACTIVE"}', 0),
        ('{"q":"t","status":"INACTIVE"}', 1),
        ('{"q":"T","status":"INACTIVE"}', 1),
        ('{"q":"","status":"ACTIVE"}', 9),
        ('{"q":"aaaaaaaaaaaaa","status":"ACTIVE"}', 0),
        ('{"q":"formulier ","status":"ACTIVE"}', 9),  # form name
        ('{"q":"FoRmUliEr ","status":"ACTIVE"}', 9),  # form name
        ('{"q":"formulier g","status":"ACTIVE"}', 1),  # form name
        ('{"q":"formulier-","status":"ACTIVE"}', 9),  # slug
        ('{"q":"fOrMuLiEr-","status":"ACTIVE"}', 9),  # slug
        ('{"q":"formulier-g","status":"ACTIVE"}', 1),  # slug
        (
            '{"q":"formulier","status":"ACTIVE"}',
            9,
        ),  # form name and slug
        ('{"q":"formulieren","status":"ACTIVE"}', 0),
        ('{"q":"ACTIVE","status":"ACTIVE"}', 0),
        ('{"q":"active","status":"ACTIVE"}', 0),
        ('{"q":"INACTIVE","status":"ACTIVE"}', 0),
        ('{"q":"t","status":"ACTIVE"}', 0),
        ('{"q":"T","status":"ACTIVE"}', 0),
        ('{"q":"formulier a","private":"false"}', 1),
        ('{"q":"formulier a","private":"true"}', 0),
        ('{"q":"formulier b","private":"false"}', 1),
        ('{"q":"formulier b","private":"true"}', 0),
        ('{"q":"formulier c","private":"false"}', 0),
        ('{"q":"formulier c","private":"true"}', 1),
        ('{"q":"formulier d","private":"false"}', 0),
        ('{"q":"formulier d","private":"true"}', 1),
        ('{"q":"formulier e","private":"false"}', 0),
        ('{"q":"formulier e","private":"true"}', 1),
        ('{"q":"formulier f2","private":"false"}', 0),
        ('{"q":"formulier f2","private":"true"}', 1),
        ('{"q":"formulier g2","private":"false"}', 0),
        ('{"q":"formulier g2","private":"true"}', 1),
        ('{"q":"formulier h2","private":"false"}', 0),
        ('{"q":"formulier h2","private":"true"}', 1),
        ('{"q":"formulier i2","private":"false"}', 0),
        ('{"q":"formulier i2","private":"true"}', 1),
        ('{"q":"formulier j2","private":"false"}', 0),
        ('{"q":"formulier j2","private":"true"}', 1),
        ('{"q":"formulier k","private":"false"}', 0),
        ('{"q":"formulier k","private":"true"}', 1),
        ('{"q":"formulier k1","private":"false"}', 0),  # org2
        ('{"q":"formulier k1","private":"true"}', 0),  # org2
        ('{"q":"formulier k2","private":"false"}', 0),  # org3
        ('{"q":"formulier k2","private":"true"}', 0),  # org3
        ('{"q":"formulier l","private":"false"}', 1),
        ('{"q":"formulier l","private":"true"}', 0),
        ('{"q":"formulier l1","private":"false"}', 0),  # org2
        ('{"q":"formulier l1","private":"true"}', 0),  # org2
        ('{"q":"formulier l2","private":"false"}', 0),  # org3
        ('{"q":"formulier l2","private":"true"}', 0),  # org3
        ('{"q":"formulier m","private":"false"}', 1),
        ('{"q":"formulier m","private":"true"}', 0),
        ('{"q":"formulier n","private":"false"}', 1),
        ('{"q":"formulier n","private":"true"}', 0),
        ('{"q":"formulier o","private":"false"}', 0),  # org3
        ('{"q":"formulier o","private":"true"}', 0),  # org3
        ('{"q":"formulier p","private":"false"}', 1),
        ('{"q":"formulier p","private":"true"}', 0),
        ('{"q":"formulier p1","private":"false"}', 0),  # org2
        ('{"q":"formulier p1","private":"true"}', 0),  # org2
        ('{"q":"formulier p2","private":"false"}', 0),  # org3
        ('{"q":"formulier p2","private":"true"}', 0),  # org3
        ('{"q":"formulier a","private":"false","status":"ACTIVE"}', 0),
        (
            '{"q":"formulier a","private":"false","status":"INACTIVE"}',
            1,
        ),
        ('{"q":"formulier a","private":"true","status":"ACTIVE"}', 0),
        ('{"q":"formulier a","private":"true","status":"INACTIVE"}', 0),
    ],
)
def test_get_forms_list_full_text_filter(
    client: TestClient, test_db: Session, filter_query_parameter_value: str, results: int
):
    add_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{FORMS}?filter={filter_query_parameter_value}")
    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == results
