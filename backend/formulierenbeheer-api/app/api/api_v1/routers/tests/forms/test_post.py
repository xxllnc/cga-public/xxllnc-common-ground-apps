import pytest
from app.api.api_v1.routers.tests import (
    CONTENT_TYPE_VALUE_APPLICATION_JSON,
    FORM_DETAILS_FIELDS,
    FORMS,
)
from app.api.api_v1.routers.tests.forms.test_mock_data import add_mock_data_to_db
from app.core.config import BASE_PATH_V1
from app.resources import error_messages
from app.resources.error_messages import FIELD_REQUIRED, VALUE_ERROR_MISSING
from datetime import date, datetime
from fastapi import status as http_status
from fastapi.testclient import TestClient
from requests.models import Response
from sqlalchemy.orm import Session
from typing import Union


def _assert_created(response: Response, expected: dict):
    assert response.status_code == http_status.HTTP_201_CREATED
    res = response.json()
    assert len(res) == FORM_DETAILS_FIELDS
    assert res["id"] is not None
    if "id" in expected:
        assert res["id"] != expected["id"]
    assert res["name"] == expected["name"]
    assert res["slug"] is not None
    assert res["slug"] == expected["name"].replace("_", "-")
    assert res["status"] == expected["status"]
    assert res["form"] == expected["form"]

    if "config" in expected and isinstance(expected["config"], list) and len(expected["config"]):
        assert res["config"] == expected["config"]
    else:
        assert res["config"] == [{"key": "urlAfsluitenKnop", "value": "https://xxllnc.nl/"}]

    if "publishDate" in expected and expected["publishDate"]:
        assert res["publishDate"] == expected["publishDate"]
    else:
        expected_date = str(datetime.today().date()) if res["status"] == "ACTIVE" else None
        assert res["publishDate"] == expected_date


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with all the required fields filled
# ---------------------------------------------
def test_post_forms_all_required_fields_set(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_all_required_fields_set",
        "status": mock.STATUS_ACTIVE,
        "form": {},
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)
    _assert_created(response, body)


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with all fields filled
# Ignore the following request fields.
# - id
# - slug
# - url (not part of the model)
# - organizationUuid
# ---------------------------------------------
def test_post_forms_all_fields_set(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "id": "1234567",
        "name": "test_post_forms_all_fields_set",
        "slug": "test_post_forms_all_fields_set slug",
        "status": mock.STATUS_ACTIVE,
        "form": {},
        "config": [],
        "organizationUuid": "69c08406-3984-4344-9b6d-6f4adec2a4b5",
        "publishDate": "2001-01-02",
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)
    _assert_created(response, body)


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with empty body.
# ---------------------------------------------
def test_post_forms_body_is_empty(client: TestClient):
    response = client.post(f"{BASE_PATH_V1}{FORMS}", json={})

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()

    assert len(res["detail"]) == 3
    expected_detail = [
        {
            "loc": {"source": "body", "field": "form"},
            "msg": FIELD_REQUIRED,
            "type": VALUE_ERROR_MISSING,
        },
        {
            "loc": {"source": "body", "field": "name"},
            "msg": FIELD_REQUIRED,
            "type": VALUE_ERROR_MISSING,
        },
        {
            "loc": {"source": "body", "field": "status"},
            "msg": FIELD_REQUIRED,
            "type": VALUE_ERROR_MISSING,
        },
    ]
    assert res["detail"] == expected_detail


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with all form values empty.
# ---------------------------------------------
def test_post_forms_all_fields_values_are_empty(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "id": "",
        "name": "",
        "slug": "",
        "status": "",
        "form": {},
        "config": [],
        "organizationId": "",  # ignore field
        "organizationUuid": "",  # ignore field
        "publishDate": "",
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res["detail"]) == 2
    assert res["detail"][0]["ctx"]["limit_value"] == 1
    assert res["detail"][0]["loc"]["field"] == "name"
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["msg"] == "ensure this value has at least 1 characters"
    assert res["detail"][0]["type"] == "value_error.any_str.min_length"

    assert len(res["detail"][1]["ctx"]["enum_values"]) == 2
    assert res["detail"][1]["ctx"]["enum_values"][0] == mock.STATUS_ACTIVE
    assert res["detail"][1]["ctx"]["enum_values"][1] == mock.STATUS_INACTIVE
    assert res["detail"][1]["loc"]["field"] == "status"
    assert res["detail"][1]["loc"]["source"] == "body"
    assert (
        res["detail"][1]["msg"]
        == "value is not a valid enumeration member; permitted: 'ACTIVE', 'INACTIVE'"  # noqa: E501
    )
    assert res["detail"][1]["type"] == "type_error.enum"


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new form with a prefilled id.
# Ignore the following request fields.
# - id
# ---------------------------------------------
@pytest.mark.parametrize(
    "id_value",
    [
        "..;/",
        "",
        "   ",
        "1233344",
        "72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
        None,
    ],
)
def test_post_forms_with_id(client: TestClient, test_db: Session, id_value: str):
    mock = add_mock_data_to_db(test_db)

    body = {
        "id": f"{id_value}",
        "name": "test_post_forms_with_id",
        "status": mock.STATUS_ACTIVE,
        "form": {},
        "publishDate": None,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)
    _assert_created(response, body)


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with missing required field name
# ---------------------------------------------
def test_post_forms_name_not_set(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "form": {},
        "status": mock.STATUS_ACTIVE,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "name"
    assert res["detail"][0]["msg"] == FIELD_REQUIRED
    assert res["detail"][0]["type"] == VALUE_ERROR_MISSING


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with empty field name
# ---------------------------------------------
def test_post_forms_name_is_empty(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "",
        "form": {},
        "status": mock.STATUS_ACTIVE,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["ctx"]["limit_value"] == 1
    assert res["detail"][0]["loc"]["field"] == "name"
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["msg"] == "ensure this value has at least 1 characters"
    assert res["detail"][0]["type"] == "value_error.any_str.min_length"


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with special chars
# ---------------------------------------------
@pytest.mark.parametrize(
    "name",
    [" ", "/", "-", "*"],
)
def test_post_forms_name_is_special_char(client: TestClient, test_db: Session, name: str):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": name,
        "form": {},
        "status": mock.STATUS_ACTIVE,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["loc"]["field"] == "name"
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["msg"] == "name heeft een waarde die niet wordt geaccepteerd"


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with name is none
# ---------------------------------------------
def test_post_forms_name_is_none(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body: dict = {
        "name": None,
        "form": {},
        "status": mock.STATUS_ACTIVE,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "name"
    assert res["detail"][0]["msg"] == "none is not an allowed value"
    assert res["detail"][0]["type"] == "type_error.none.not_allowed"


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with name that exceeds 255 kars.
# ---------------------------------------------
def test_post_forms_name_value_exceeds_max_length(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_name_value_exceeds_max_length 72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
        "status": mock.STATUS_ACTIVE,
        "form": {},
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res) == 1
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "name"
    assert res["detail"][0]["msg"] == "ensure this value has at most 255 characters"  # noqa: E501
    assert res["detail"][0]["ctx"]["limit_value"] == 255


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with multiple names.
# ---------------------------------------------
def test_post_forms_multiple_names(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_multiple_names 1",  # noqa: F601
        "name": "test_post_forms_multiple_names 2",  # noqa: F601
        "name": "test_post_forms_multiple_names 3",  # noqa: F601
        "status": mock.STATUS_ACTIVE,
        "form": {},
        "publishDate": None,
    }
    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)
    assert response.status_code == http_status.HTTP_201_CREATED
    res = response.json()
    assert len(res) == FORM_DETAILS_FIELDS
    assert res["id"] is not None
    assert res["name"] is not None
    assert res["slug"] is not None
    assert res["status"] == body["status"]
    assert res["form"] == body["form"]
    assert res["config"] is not None
    assert res["publishDate"] == str(datetime.today().date())


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new active form where combination name
# and creator_organization_uuid already exists.
# Combination name and creator_organization_uuid is unique.
# ---------------------------------------------
def test_post_forms_combination_name_organization_exists(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": mock.form1.name,
        "status": mock.STATUS_ACTIVE,
        "form": {},
        "publishDate": None,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    assert response.status_code == http_status.HTTP_409_CONFLICT
    res = response.json()
    assert len(res["detail"]) == 1
    assert (
        res["detail"][0]["msg"]
        == error_messages.FORM_COMBINATION_NAME_CREATOR_ORGANIZATION_UUID_ALREADY_EXIST_ERROR  # noqa: E501
    )


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new active form where combination name
# and creator_organization_uuid not exists.
# Combination name and creator_organization_uuid is unique.
# ---------------------------------------------
def test_post_forms_combination_name_organization_not_exists(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": mock.form10.name.replace(" ", "-"),
        "slug": mock.form10.slug.replace(" ", "-"),
        "status": mock.STATUS_ACTIVE,
        "form": {},
        "publishDate": None,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)
    _assert_created(response, body)


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new form with a prefilled slug.
# Slug will be ignored.
# ---------------------------------------------
def test_post_forms_with_slug(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_with_slug",
        "status": mock.STATUS_ACTIVE,
        "form": {},
        "slug": "formn6",
        "publishDate": None,
    }
    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)
    _assert_created(response, body)


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form without optional field slug.
# ---------------------------------------------
def test_post_forms_slug_not_set(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_slug_not_set",
        "form": {},
        "status": mock.STATUS_ACTIVE,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    _assert_created(response, body)


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with empty field slug
# Slug will be ignored.
# ---------------------------------------------
def test_post_forms_slug_is_empty(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_slug_is_empty",
        "slug": "",
        "form": {},
        "status": mock.STATUS_ACTIVE,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)
    _assert_created(response, body)


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with slug is none
# Slug will be ignored.
# ---------------------------------------------
def test_post_forms_slug_is_none(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body: dict = {
        "name": "test_post_forms_slug_is_none",
        "slug": None,
        "form": {},
        "status": mock.STATUS_ACTIVE,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    _assert_created(response, body)


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with a slug that exceeds 255 kars.
# Slug will be ignored.
# ---------------------------------------------
def test_post_forms_slug_exceeds_max_length(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_slug_exceeds_max_length",
        "status": mock.STATUS_ACTIVE,
        "form": {},
        "slug": "formulierrneww42345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
        "publishDate": None,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    _assert_created(response, body)


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with multiple slugs.
# Slug will be ignored.
# ---------------------------------------------
def test_post_forms_multiple_slugs(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_multiple_slugs",
        "slug": "test_post_forms_multiple_slugs 1",  # noqa: F601
        "slug": "test_post_forms_multiple_slugs 2",  # noqa: F601
        "slug": "test_post_forms_multiple_slugs 3",  # noqa: F601
        "status": mock.STATUS_ACTIVE,
        "form": {},
        "publishDate": None,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    _assert_created(response, body)


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form where combination slug
# and creator_organization_uuid already exists
# Combination slug and creator_organization_uuid is unique.
# ---------------------------------------------
def test_post_forms_combination_slug_organization_exists(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": mock.form6.slug.replace(" ", "-"),
        "slug": mock.form6.slug,
        "status": mock.STATUS_ACTIVE,
        "form": {},
        "publishDate": None,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    assert response.status_code == http_status.HTTP_409_CONFLICT
    res = response.json()
    assert len(res["detail"]) == 1
    assert (
        res["detail"][0]["msg"]
        == error_messages.FORM_COMBINATION_SLUG_CREATOR_ORGANIZATION_UUID_ALREADY_EXIST_ERROR  # noqa: E501
    )


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new active form where combination slug
# and creator_organization_uuid already exists.
# Combination slug and creator_organization_uuid is unique.
# ---------------------------------------------
def test_post_forms_combination_slug_organization_not_exists(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": mock.form10.slug.replace(" ", "-"),
        "slug": mock.form10.slug,
        "status": mock.STATUS_ACTIVE,
        "form": {},
        "publishDate": None,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)
    assert response.status_code == http_status.HTTP_409_CONFLICT


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new form with a prefilled url.
# Url will be ignored, because it is not part
# of the model.
# ---------------------------------------------
@pytest.mark.parametrize(
    "url_value",
    [
        "..;/",
        "",
        "   ",
        "1233344",
        "72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
        "http://localhost",
        None,
    ],
)
def test_post_forms_with_url(client: TestClient, test_db: Session, url_value: str):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_with_url",
        "status": mock.STATUS_ACTIVE,
        "form": {},
        "url": f"{url_value}",
        "publishDate": None,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    _assert_created(response, body)


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new form with status inactive.
# ---------------------------------------------
def test_post_forms_status_inactive(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_status_inactive",
        "status": mock.STATUS_INACTIVE,
        "form": {},
        "publishDate": None,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    _assert_created(response, body)


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new form with status active.
# ---------------------------------------------
def test_post_forms_status_active(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_status_active",
        "status": mock.STATUS_ACTIVE,
        "form": {},
        "publishDate": None,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    _assert_created(response, body)


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is set to application/json.
# Test add a new form with status active.
# ---------------------------------------------
def test_post_forms_status_active_with_content_type_json(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    headers = {
        "Content-Type": CONTENT_TYPE_VALUE_APPLICATION_JSON,
    }

    body = {
        "name": "test_post_forms_status_active_with_content_type_json",
        "status": mock.STATUS_ACTIVE,
        "form": {},
        "publishDate": None,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body, headers=headers)

    _assert_created(response, body)


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with missing required field status
# ---------------------------------------------
def test_post_forms_status_not_set(client: TestClient):

    body = {
        "name": "test_post_forms_status_not_set",
        "form": {},
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "status"
    assert res["detail"][0]["msg"] == FIELD_REQUIRED
    assert res["detail"][0]["type"] == VALUE_ERROR_MISSING


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with invalid status value.
# ---------------------------------------------
@pytest.mark.parametrize(
    "status_invalid_value",
    [
        "..;/",
        "",
        "   ",
        "1233344",
        "72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
        "http://localhost",
        "INVALID_ACTIVE",
        "active",
        "inactive",
        None,
    ],
)
def test_post_forms_status_invalid_value(
    client: TestClient, test_db: Session, status_invalid_value: str
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_status_invalid_value",
        "status": f"{status_invalid_value}",
        "form": {},
        "publishDate": None,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res) == 1
    assert len(res["detail"][0]["ctx"]["enum_values"]) == 2
    assert res["detail"][0]["ctx"]["enum_values"][0] == mock.STATUS_ACTIVE
    assert res["detail"][0]["ctx"]["enum_values"][1] == mock.STATUS_INACTIVE
    assert res["detail"][0]["loc"]["field"] == "status"
    assert res["detail"][0]["loc"]["source"] == "body"
    assert (
        res["detail"][0]["msg"]
        == "value is not a valid enumeration member; permitted: 'ACTIVE', 'INACTIVE'"  # noqa: E501
    )
    assert res["detail"][0]["type"] == "type_error.enum"


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with an empty form.
# ---------------------------------------------
def test_post_forms_form_empty(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_form_empty",
        "status": mock.STATUS_ACTIVE,
        "form": {},
        "config": [],
        "publishDate": "2001-01-01",
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    _assert_created(response, body)


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with missing required field form
# ---------------------------------------------
def test_post_forms_form_not_set(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_form_not_set",
        "status": mock.STATUS_ACTIVE,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "form"
    assert res["detail"][0]["msg"] == FIELD_REQUIRED
    assert res["detail"][0]["type"] == VALUE_ERROR_MISSING


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with an invalid form.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_form",
    [
        None,
    ],
)
def test_post_forms_form_not_valid(client: TestClient, test_db: Session, invalid_form: str):
    mock = add_mock_data_to_db(test_db)

    body = {
        "id": "1234567",
        "name": "test_post_forms_form_not_valid",
        "slug": "test_post_forms_form_not_valid slug",
        "status": mock.STATUS_ACTIVE,
        "form": invalid_form,
        "config": "{}",
        "organizationUuid": "69c08406-3984-4344-9b6d-6f4adec2a4b5",  # ignore
        "publishDate": None,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()

    assert len(res["detail"]) == 2
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "form" or "config"
    assert res["detail"][0]["msg"] == "none is not an allowed value" or "value is not a valid list"
    assert res["detail"][0]["type"] == "type_error.none.not_allowed" or "type_error.list"


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with missing required fields form and status
# ---------------------------------------------
def test_post_forms_status_and_form_not_set(client: TestClient):
    body = {
        "name": "test_post_forms_status_and_form_not_set",
    }
    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()

    expected_detail = [
        {
            "loc": {"source": "body", "field": "form"},
            "msg": FIELD_REQUIRED,
            "type": VALUE_ERROR_MISSING,
        },
        {
            "loc": {"source": "body", "field": "status"},
            "msg": FIELD_REQUIRED,
            "type": VALUE_ERROR_MISSING,
        },
    ]
    assert len(res["detail"]) == 2
    assert res["detail"] == expected_detail


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with missing required fields name and form
# ---------------------------------------------
def test_post_forms_name_and_form_not_set(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "status": mock.STATUS_ACTIVE,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()

    expected_detail = [
        {
            "loc": {"source": "body", "field": "form"},
            "msg": FIELD_REQUIRED,
            "type": VALUE_ERROR_MISSING,
        },
        {
            "loc": {"source": "body", "field": "name"},
            "msg": FIELD_REQUIRED,
            "type": VALUE_ERROR_MISSING,
        },
    ]
    assert len(res["detail"]) == 2
    assert res["detail"] == expected_detail


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with missing required fields name and status
# ---------------------------------------------
def test_post_forms_name_and_status_not_set(client: TestClient):
    body: dict = {"form": {}}

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res["detail"]) == 2
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "name"
    assert res["detail"][0]["msg"] == FIELD_REQUIRED
    assert res["detail"][0]["type"] == VALUE_ERROR_MISSING
    assert res["detail"][1]["loc"]["source"] == "body"
    assert res["detail"][1]["loc"]["field"] == "status"
    assert res["detail"][1]["msg"] == FIELD_REQUIRED
    assert res["detail"][1]["type"] == VALUE_ERROR_MISSING


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form without the required fields and with
# additional fields.
# ---------------------------------------------
def test_post_forms_without_required_fields_and_with_additional_fields(
    client: TestClient, test_db: Session
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "notvalid1": "formulier new 2",
        "notvalid2": mock.STATUS_ACTIVE,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    expected_detail = [
        {
            "loc": {"source": "body", "field": "form"},
            "msg": FIELD_REQUIRED,
            "type": VALUE_ERROR_MISSING,
        },
        {
            "loc": {"source": "body", "field": "name"},
            "msg": FIELD_REQUIRED,
            "type": VALUE_ERROR_MISSING,
        },
        {
            "loc": {"source": "body", "field": "status"},
            "msg": FIELD_REQUIRED,
            "type": VALUE_ERROR_MISSING,
        },
    ]
    assert len(res["detail"]) == 3
    assert res["detail"] == expected_detail


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with the required fields and with
# additional fields (url, organizationUrl, notvalid1, notvalid2)
# ---------------------------------------------
def test_post_forms_with_required_fields_and_with_additional_fields(
    client: TestClient, test_db: Session
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_with_required_fields_and_with_additional_fields",  # noqa: E501
        "url": "http://localhost",
        "status": mock.STATUS_ACTIVE,
        "form": {},
        "config": [],
        "organizationUrl": "http://localhost2",
        "publishDate": "2001-01-01",
        "notvalid1": "formulier new 2",
        "notvalid2": mock.STATUS_ACTIVE,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    _assert_created(response, body)


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with an organization id.
# The organization id is not a valid id.
# Ignore the organizationUuid request field.
# ---------------------------------------------
@pytest.mark.parametrize(
    "organization_value",
    [
        "",
        " ",
        "123",
    ],
)
def test_post_forms_organization_uuid_not_valid(
    client: TestClient, test_db: Session, organization_value: str
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "id": "1234567",
        "name": "test_post_forms_organization_uuid_not_valid",
        "slug": "test_post_forms_organization_uuid_not_valid slug",
        "status": mock.STATUS_ACTIVE,
        "form": {},
        "config": [],
        "organizationUuid": f"{organization_value}",
        "publishDate": None,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    _assert_created(response, body)


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with an organization id.
# The organization id value is None.
# Ignore the organizationUuid request field.
# ---------------------------------------------
def test_post_forms_organization_uuid_none(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "id": "1234567",
        "name": "test_post_forms_organization_uuid_none",
        "slug": "test_post_forms_organization_uuid_none slug",
        "status": mock.STATUS_ACTIVE,
        "form": {},
        "config": [],
        "organizationUuid": None,
        "publishDate": None,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)
    _assert_created(response, body)


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with an organization id.
# The organization id value is a valid id.
# Ignore the organizationUuid request field.
# ---------------------------------------------
@pytest.mark.parametrize(
    "organization_value",
    [
        "12345678-1234-4123-a123-1234567890ab",
        "00000000-0000-4000-a000-000000000000",
        "aaaaaaaa-aaaa-4aaa-aaaa-aaaaaaaaaaaa",
    ],
)
def test_post_forms_organization_uuid_valid(
    client: TestClient, test_db: Session, organization_value: str
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "id": "1234567",
        "name": "test_post_forms_organization_uuid_valid",
        "slug": "test_post_forms_organization_uuid_valid slug",
        "status": mock.STATUS_ACTIVE,
        "form": {},
        "config": [],
        "organizationUuid": f"{organization_value}",
        "publishDate": None,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)
    _assert_created(response, body)


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new form with a prefilled config.
# ---------------------------------------------
@pytest.mark.parametrize(
    "config_value",
    [
        [{"key": "urlAfsluitenKnop", "value": "https://www.xxllnc.nl/"}],
        [{}],
        [{"key": "something", "value": "theValue"}],
        [
            {
                "key": "urlAfsluitenKnop",
                "value": "72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
            }
        ],
        None,
    ],
)
def test_post_forms_config(client: TestClient, test_db: Session, config_value: list):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_config",
        "status": mock.STATUS_ACTIVE,
        "form": {},
        "config": config_value,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    _assert_created(response, body)


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new form with a prefilled organizationUrl.
# The organizationUrl will be ignored, because
# it is not part of the model.
# ---------------------------------------------
@pytest.mark.parametrize(
    "organization_url_value",
    [
        "..;/",
        "",
        "   ",
        "1233344",
        "72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
        "http://localhost",
        None,
    ],
)
def test_post_forms_organization_url(
    client: TestClient, test_db: Session, organization_url_value: str
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_organization_url",
        "status": mock.STATUS_INACTIVE,
        "form": {},
        "organizationUrl": f"{organization_url_value}",
    }

    response = client.post(
        f"{BASE_PATH_V1}{FORMS}",
        json=body,
    )

    _assert_created(response, body)


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new form with an empty publishDate.
# ---------------------------------------------
def test_post_forms_publish_date_is_empty(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_publish_date_is_empty",
        "status": mock.STATUS_INACTIVE,
        "form": {},
        "publishDate": "",
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)
    body["publishDate"] = None
    _assert_created(response, body)


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new form with publishDate set to None.
# ---------------------------------------------
def test_post_forms_publish_date_is_none(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_publish_date_is_none",
        "status": mock.STATUS_INACTIVE,
        "form": {},
        "publishDate": None,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    assert response.status_code == http_status.HTTP_201_CREATED
    _assert_created(response, body)


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new form with an invalid publishDate.
#
# https://fastapi.tiangolo.com/tutorial/extra-data-types/
# datetime.date:
# - Python datetime.date.
# - In requests and responses will be represented as a str in
#   ISO 8601 format, like: 2008-09-15.
#
# https://pydantic-docs.helpmanual.io/usage/types/#datetime-types
# date fields can be:
# - date, existing date object
# - int or float, see datetime
# - str, following formats work:
#   - YYYY-MM-DD
#   - int or float, see datetime
# ---------------------------------------------
@pytest.mark.parametrize(
    "publish_date_invalid_value",
    [
        "..;/",
        "0000-00-00",
        "0000-00-01",
        "0000-01-00",
        "0000-01-01",
        "01-01-2020",
        "2021-01-00",
        "2021-00-01",
        "0000-00-00",
        "2021-000-01",
        "2021-01-00",
        "2021-13-01",
        "2021-00-00",
        "2021-01-32",
        "2021-01",
        "2021-01-",
        "   ",
        "http://localhost",
        "None",
    ],
)
def test_post_forms_publish_date_invalid_value(
    client: TestClient, test_db: Session, publish_date_invalid_value: str
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_publish_date_invalid_value",
        "status": mock.STATUS_INACTIVE,
        "form": {},
        "publishDate": f"{publish_date_invalid_value}",
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "publishDate"
    assert res["detail"][0]["msg"] == "invalid date format"
    assert res["detail"][0]["type"] == "value_error.date"


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new form with a valid publishDate.
#
# https://fastapi.tiangolo.com/tutorial/extra-data-types/
# datetime.date:
# - Python datetime.date.
# - In requests and responses will be represented as a str in
#   ISO 8601 format, like: 2008-09-15.
#
# https://pydantic-docs.helpmanual.io/usage/types/#datetime-types
# date fields can be:
# - date, existing date object
# - int or float, see datetime
# - str, following formats work:
#   - YYYY-MM-DD
#   - int or float, see datetime
# ---------------------------------------------
@pytest.mark.parametrize(
    "publish_date_request, publish_date_response",
    [
        ("0001-01-01", "0001-01-01"),
        ("2001-01-01", "2001-01-01"),
        ("2021-12-31", "2021-12-31"),
        ("0", "1970-01-01"),
        ("2021", "1970-01-01"),
        ("1233344", "1970-01-15"),
        (
            "72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
            "9999-12-31",
        ),
        ("-0", "1970-01-01"),
        ("-1", "1969-12-31"),
        ("-30", "1969-12-31"),
        (0, "1970-01-01"),
        (2021, "1970-01-01"),
        (1233344, "1970-01-15"),
        (
            72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456,  # noqa: E501
            "9999-12-31",
        ),
        (-10, "1969-12-31"),
        (str(date(2001, 1, 1)), "2001-01-01"),
        (str(date(2021, 12, 20)), "2021-12-20"),
    ],
)
def test_post_forms_publish_date_valid_value(
    client: TestClient,
    test_db: Session,
    publish_date_request: Union[str, int, float],
    publish_date_response: str,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_publish_date_valid_value",
        "status": mock.STATUS_INACTIVE,
        "form": {},
        "publishDate": publish_date_request,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)
    body["publishDate"] = publish_date_response
    _assert_created(response, body)


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with all the required fields filled
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter",
    [
        "",
        "1",
        "2",
        "..;/",
        "",
        "   ",
        "1233344",
        "72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
        "http://localhost",
        "99",
        "-1",
        "99999999999999999999999999999999999999999999999999999999999999",
        "_",
        "aa",
        "unknown",
        "0",
        "__",
        "<>",
        "111111111111111111111111111111111111111111111111111111111",
        " AND 1::int=1",
        " UNION ALL SELECT NULL,version(),NULL LIMIT 1 OFFSET 1--",
        " or 1=1",
        " or = 1' or '1' = '1",
        "not valid",
        "..;/",
        "",
        "?id=1 AND 1::int=1",
        "?id=1 UNION ALL SELECT NULL,version(),NULL LIMIT 1 OFFSET 1--",
        ">'\"()%26%25<acx><ScRiPt%20>jLeb(9968)</ScRiPt>",
        "text/plain",
        "application/*",
        "*/*",
        "../form",
        None,
    ],
)
def test_post_forms_all_required_fields_set_with_query_param(
    client: TestClient, test_db: Session, query_parameter: str
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_all_required_fields_set_with_query_param",
        "status": mock.STATUS_INACTIVE,
        "form": {},
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}?{query_parameter}", json=body)

    _assert_created(response, body)
