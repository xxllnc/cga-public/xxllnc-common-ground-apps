import pytest
from app.api.api_v1.routers.tests import CONTENT_TYPE_VALUE_APPLICATION_JSON, FORMS
from app.api.api_v1.routers.tests.forms.test_get_by_id import _assert_found
from app.api.api_v1.routers.tests.forms.test_mock_data import add_mock_data_to_db
from app.api.api_v1.routers.tests.helper import convert_db_form_to_dict
from app.core.config import BASE_PATH_V1
from app.resources import error_messages
from app.resources.error_messages import FIELD_REQUIRED, VALUE_ERROR_MISSING
from datetime import date
from fastapi import status as http_status
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session
from typing import Union


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Test update all fields.
# Ignore the following request fields.
# - id
# - organizationUuid
# ---------------------------------------------
def test_put_forms_update_all_fields_on_id(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)
    mock_form_slug = "test-put-forms-update-all-fields-a"

    body = {
        "id": str(mock.form1.id),
        "name": "test_put_forms_update_all_fields_on_id",
        "slug": mock_form_slug,
        "status": mock.STATUS_ACTIVE,
        "form": {"put": "value"},
        "config": [{"key": "config", "value": "put_config"}],
        "organizationUuid": "c16ae581-8d6f-424e-bff3-3a70a1374f09",
        "publishDate": "2021-12-31",
    }

    response = client.put(f"{BASE_PATH_V1}{FORMS}/{mock.form1.id}", json=body)
    _assert_found(response, mock.form1)


def test_put_forms_update_all_fields_on_non_existing_id(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)
    mock_form_slug = "test-put-forms-update-all-fields-a"

    body = {
        "id": "c16ae581-8d6f-424e-bff3-3a70a1374f00",
        "name": "test_put_forms_update_all_fields_on_non_existing_id",
        "slug": mock_form_slug,
        "status": mock.STATUS_ACTIVE,
        "form": {"put": "value"},
        "config": [{"key": "config", "value": "put_config"}],
        "organizationUuid": "c16ae581-8d6f-424e-bff3-3a70a1374f09",
        "publishDate": "2021-12-31",
    }

    response = client.put(f"{BASE_PATH_V1}{FORMS}/c16ae581-8d6f-424e-bff3-3a70a1374f00", json=body)

    assert response.status_code == http_status.HTTP_404_NOT_FOUND


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Test update all fields.
# Ignore the following request fields.
# - id
# - organizationUuid
# ---------------------------------------------
def test_put_forms_update_all_fields_on_slug(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "id": str(mock.form1.id),
        "name": "test_put_forms_update_all_fields_on_slug",
        "slug": "test-put-forms-update-all-fields-on-slug",
        "status": mock.STATUS_ACTIVE,
        "form": {"put": "value"},
        "config": [{"key": "config", "value": "put_config"}],
        "organizationUuid": "c16ae581-8d6f-424e-bff3-3a70a1374f09",
        "publishDate": "2021-12-31",
    }

    response = client.put(f"{BASE_PATH_V1}{FORMS}/formulier-a", json=body)
    _assert_found(response, mock.form1)


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Test update with body is empty.
# Only update the following field:
# - organizationUuid
# ---------------------------------------------
def test_put_forms_body_is_empty(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    for id in [mock.form1.id, mock.form1.slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json={})
        _assert_found(response, mock.form1)


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Test update form with all form values empty.
# ---------------------------------------------
def test_put_forms_update_all_fields_values_are_empty(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "id": "",
        "name": "",
        "slug": "",
        "status": "",
        "form": {},
        "config": "",
        "organizationUuid": "",
        "publishDate": "",
    }

    for id in [mock.form1.id, mock.form1.slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json=body)

        assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
        res = response.json()

        expected_detail = [
            {
                "loc": {"source": "body", "field": "config"},
                "msg": "value is not a valid list",
                "type": "type_error.list",
            },
            {
                "loc": {"source": "body", "field": "slug"},
                "msg": "ensure this value has at least 1 characters",
                "type": "value_error.any_str.min_length",
                "ctx": {"limit_value": 1},
            },
            {
                "loc": {"source": "body", "field": "name"},
                "msg": "ensure this value has at least 1 characters",
                "type": "value_error.any_str.min_length",
                "ctx": {"limit_value": 1},
            },
            {
                "loc": {"source": "body", "field": "status"},
                "msg": "value is not a valid enumeration member; permitted: 'ACTIVE', 'INACTIVE'",
                "type": "type_error.enum",
                "ctx": {"enum_values": ["ACTIVE", "INACTIVE"]},
            },
        ]
        assert len(res["detail"]) == 4
        assert res["detail"] == expected_detail


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Update status and form.
# ---------------------------------------------
def test_put_forms_update_status_and_form(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    expected = convert_db_form_to_dict(mock.form1)
    body = {
        "status": mock.STATUS_INACTIVE,
        "form": {"test1": "value"},
    }
    expected["status"] = body["status"]
    expected["form"] = body["form"]

    for id in [mock.form1.id, mock.form1.slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json=body)
        _assert_found(response, mock.form1)


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Test without path parameter id and without a body.
# Reject request with HTTP response code 405 Method not allowed,
# because we do not want to update every resource in the entire collection
# of resource.
# # ---------------------------------------------
def test_put_forms_without_path_parameter_id_without_body(client: TestClient):
    response = client.put(f"{BASE_PATH_V1}{FORMS}")
    assert response.status_code == http_status.HTTP_405_METHOD_NOT_ALLOWED
    res = response.json()
    assert len(res) == 1
    assert res["detail"] == "Method Not Allowed"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Test without path parameter id and with an empty body.
# ---------------------------------------------
def test_put_forms_without_path_parameter_id_with_empty_body(client: TestClient):
    response = client.put(f"{BASE_PATH_V1}{FORMS}/", json={})
    assert response.status_code == http_status.HTTP_307_TEMPORARY_REDIRECT
    headers = response.headers
    assert len(headers) == 2
    assert headers["content-length"] == str(0)
    assert headers["Location"] is not None
    assert headers["Location"] == "http://testserver/formulierenbeheer/api/v1/forms"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Test with empty path parameter id and without a body.
# ---------------------------------------------
@pytest.mark.parametrize("invalid_id", [""])
def test_put_forms_with_empty_path_parameter_id_without_body(client: TestClient, invalid_id: str):
    response = client.put(f"{BASE_PATH_V1}{FORMS}/{invalid_id}")
    assert response.status_code == http_status.HTTP_307_TEMPORARY_REDIRECT
    headers = response.headers
    assert len(headers) == 2
    assert headers["content-length"] == str(0)
    assert headers["Location"] is not None
    assert headers["Location"] == "http://testserver/formulierenbeheer/api/v1/forms"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Test with invalid path parameter id and without a body.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_path_parameter_id_value",
    [
        "99",
        "-1",
        "99999999999999999999999999999999999999999999999999999999999999",
        "_",
        "aa",
        "unknown",
        "0",
        "__",
        "<>",
        "111111111111111111111111111111111111111111111111111111111",
        " AND 1::int=1",
        " UNION ALL SELECT NULL,version(),NULL LIMIT 1 OFFSET 1--",
        " or 1=1",
        " or = 1' or '1' = '1",
        "not valid",
    ],
)
def test_put_forms_with_invalid_path_parameter_id_without_body(
    client: TestClient, invalid_path_parameter_id_value: str
):
    response = client.put(f"{BASE_PATH_V1}{FORMS}/{invalid_path_parameter_id_value}")

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["loc"][0] == "body"
    assert res["detail"][0]["msg"] == FIELD_REQUIRED
    assert res["detail"][0]["type"] == VALUE_ERROR_MISSING

    headers = response.headers
    assert len(headers) == 2
    assert headers["content-length"] is not None
    assert headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Test with invalid path parameter id and with an empty body.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_path_parameter_id_value",
    [
        "99",
        "-1",
        "99999999999999999999999999999999999999999999999999999999999999",
        "_",
        "aa",
        "unknown",
        "0",
        "__",
        "<>",
        "111111111111111111111111111111111111111111111111111111111",
        " AND 1::int=1",
        " UNION ALL SELECT NULL,version(),NULL LIMIT 1 OFFSET 1--",
        " or 1=1",
        " or = 1' or '1' = '1",
        "not valid",
    ],
)
def test_put_forms_invalid_id_with_empty_body(
    client: TestClient, invalid_path_parameter_id_value: str
):
    response = client.put(f"{BASE_PATH_V1}{FORMS}/{invalid_path_parameter_id_value}", json={})
    assert response.status_code == http_status.HTTP_404_NOT_FOUND
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["msg"] == error_messages.FORM_NOT_FOUND_WITH.format(
        invalid_path_parameter_id_value
    )


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Test with invalid path parameter id and with an empty body.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_path_parameter_id_value",
    [
        "99",
        "-1",
        "99999999999999999999999999999999999999999999999999999999999999",
        "_",
        "aa",
        "unknown",
        "0",
        "__",
        "<>",
        "111111111111111111111111111111111111111111111111111111111",
        " AND 1::int=1",
        " UNION ALL SELECT NULL,version(),NULL LIMIT 1 OFFSET 1--",
        " or 1=1",
        " or = 1' or '1' = '1",
        "not valid",
    ],
)
def test_put_forms_invalid_id_with_empty_body_2(
    client: TestClient, test_db: Session, invalid_path_parameter_id_value: str
):
    mock = add_mock_data_to_db(test_db)

    id_value = f"{mock.form1.id}{invalid_path_parameter_id_value}"
    response = client.put(f"{BASE_PATH_V1}{FORMS}/{id_value}", json={})

    assert response.status_code == http_status.HTTP_404_NOT_FOUND
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["msg"] == error_messages.FORM_NOT_FOUND_WITH.format(id_value)


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form with a prefilled id.
# Id will be ignored.
# ---------------------------------------------
@pytest.mark.parametrize(
    "id_value",
    [
        "..;/",
        "",
        "   ",
        "1233344",
        "72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
        None,
    ],
)
def test_put_forms_update_id(client: TestClient, test_db: Session, id_value: str):
    mock = add_mock_data_to_db(test_db)

    expected = convert_db_form_to_dict(mock.form1)
    body = {"id": f"{id_value}", "form": {"field": "value"}}
    expected["form"] = body["form"]

    for id in [mock.form1.id, mock.form1.slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json=body)
        _assert_found(response, mock.form1)


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update form with empty name
# ---------------------------------------------
def test_put_forms_name_is_empty(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {"name": ""}

    for id in [mock.form1.id, mock.form1.slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json=body)

        assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
        res = response.json()
        assert len(res["detail"]) == 1
        assert res["detail"][0]["loc"]["source"] == "body"
        assert res["detail"][0]["loc"]["field"] == "name"
        assert res["detail"][0]["msg"] == "ensure this value has at least 1 characters"
        assert res["detail"][0]["type"] == "value_error.any_str.min_length"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update form with none nullable fields set to None
# ---------------------------------------------
@pytest.mark.parametrize(
    "id_or_slug, field",
    [
        ("id", "name"),
        ("slug", "name"),
        ("id", "slug"),
        ("slug", "slug"),
        ("id", "form"),
        ("slug", "form"),
        ("id", "status"),
        ("id", "rights"),
        ("id", "private"),
    ],
)
def test_put_forms_by_id_name_is_none(
    client: TestClient, test_db: Session, id_or_slug: str, field: str
):
    mock = add_mock_data_to_db(test_db)

    form_id = getattr(mock.form1, id_or_slug)
    body = {field: None}
    response = client.put(f"{BASE_PATH_V1}{FORMS}/{form_id}", json=body)

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()

    assert len(res["detail"]) == 1
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == field
    assert res["detail"][0]["msg"] == (
        f"{field} heeft geen waarde, dit is een verplicht veld " "en moet een waarde bevatten"
    )


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test put form with name that exceeds 255 kars.
# ---------------------------------------------
def test_put_forms_name_value_exceeds_max_length(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "formulier update 72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
    }

    for id in [mock.form1.id, mock.form1.slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json=body)

        assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
        res = response.json()
        assert len(res) == 1
        assert res["detail"][0]["loc"]["source"] == "body"
        assert res["detail"][0]["loc"]["field"] == "name"
        assert res["detail"][0]["msg"] == "ensure this value has at most 255 characters"
        assert res["detail"][0]["ctx"]["limit_value"] == 255


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update a form with multiple names.
# ---------------------------------------------
def test_put_forms_multiple_names(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "formulier update 9a",  # noqa: F601
        "name": "formulier update 9b",  # noqa: F601
        "name": "formulier update 9c",  # noqa: F601
    }

    for id in [mock.form1.id, mock.form1.slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json=body)

        assert response.status_code == http_status.HTTP_200_OK
        res = response.json()
        _assert_found(response, mock.form1)
        assert res["shares"][0]["name"] is not None
        assert res["shares"][0]["name"] == mock.form1.name


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update form name.
# ---------------------------------------------
def test_put_forms_update_name(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    expected = convert_db_form_to_dict(mock.form1)
    body = {"name": f"{mock.form1.name}a"}
    expected["name"] = body["name"]

    for id in [mock.form1.id, mock.form1.slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json=body)
        _assert_found(response, mock.form1)


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form where combination name
# and creator_organization_uuid already exists.
# Combination name and creator_organization_uuid is unique.
# ---------------------------------------------
def test_put_forms_by_id_name_organization_exists(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {"name": mock.form2.name}

    response = client.put(f"{BASE_PATH_V1}{FORMS}/{mock.form1.id}", json=body)

    assert response.status_code == http_status.HTTP_409_CONFLICT
    res = response.json()
    assert len(res["detail"]) == 1
    assert (
        res["detail"][0]["msg"]
        == error_messages.FORM_COMBINATION_NAME_CREATOR_ORGANIZATION_UUID_ALREADY_EXIST_ERROR
    )


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form where combination name
# and creator_organization_uuid not exists.
# Combination name and creator_organization_uuid is unique.
# ---------------------------------------------
def test_put_forms_by_id_name_organization_not_exists(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {"name": "formulier_not_exists"}

    response = client.put(f"{BASE_PATH_V1}{FORMS}/{mock.form1.id}", json=body)
    assert response.status_code == http_status.HTTP_200_OK


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form where combination name
# and creator_organization_uuid already exists.
# Combination name and creator_organization_uuid is unique.
# ---------------------------------------------
def test_put_forms_by_slug_name_organization_exists(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {"name": mock.form2.name}

    response = client.put(f"{BASE_PATH_V1}{FORMS}/{mock.form1.slug}", json=body)

    assert response.status_code == http_status.HTTP_409_CONFLICT
    res = response.json()
    assert len(res["detail"]) == 1
    assert (
        res["detail"][0]["msg"]
        == error_messages.FORM_COMBINATION_NAME_CREATOR_ORGANIZATION_UUID_ALREADY_EXIST_ERROR
    )


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form where combination name
# and creator_organization_uuid not exists.
# Combination name and creator_organization_uuid is unique.
# ---------------------------------------------
def test_put_forms_by_slug_name_organization_not_exists(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {"name": "formulier_not_exists"}

    response = client.put(f"{BASE_PATH_V1}{FORMS}/{mock.form1.slug}", json=body)
    assert response.status_code == http_status.HTTP_200_OK


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update slug.
# ---------------------------------------------
def test_put_forms_slug(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    mock_form1_slug = "test-put-forms-slug"

    expected = convert_db_form_to_dict(mock.form1)
    body = {"slug": f"{mock_form1_slug}"}
    expected["slug"] = body["slug"]

    for id in [mock.form1.id, mock_form1_slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json=body)
        _assert_found(response, mock.form1)


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update form without optional field slug.
# ---------------------------------------------
def test_put_forms_slug_not_set(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    expected = convert_db_form_to_dict(mock.form1)
    body: dict = {"form": {"field": "value"}}
    expected["form"] = body["form"]

    for id in [mock.form1.id, mock.form1.slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json=body)
        _assert_found(response, mock.form1)


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update form with empty slug
# ---------------------------------------------
def test_put_forms_by_id_slug_is_empty(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {"slug": ""}

    response = client.put(f"{BASE_PATH_V1}{FORMS}/{mock.form1.id}", json=body)

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "slug"
    assert res["detail"][0]["msg"] == "ensure this value has at least 1 characters"
    assert res["detail"][0]["type"] == "value_error.any_str.min_length"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update form with empty slug
# ---------------------------------------------
def test_put_forms_by_slug_slug_is_empty(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {"slug": ""}

    response = client.put(f"{BASE_PATH_V1}{FORMS}/{mock.form1.slug}", json=body)

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "slug"
    assert res["detail"][0]["msg"] == "ensure this value has at least 1 characters"
    assert res["detail"][0]["type"] == "value_error.any_str.min_length"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update a form with a slug that exceeds 255 kars.
# ---------------------------------------------
def test_put_forms_by_slug_slug_exceeds_max_length(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "slug": "formulierrneww42345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
    }

    response = client.put(f"{BASE_PATH_V1}{FORMS}/{mock.form2.slug}", json=body)

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res) == 1
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "slug"
    assert res["detail"][0]["msg"] == "ensure this value has at most 255 characters"
    assert res["detail"][0]["ctx"]["limit_value"] == 255


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update a form with a slug that exceeds 255 kars.
# ---------------------------------------------
def test_put_forms_by_id_slug_exceeds_max_length(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "slug": "formulierrneww42345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
    }

    response = client.put(f"{BASE_PATH_V1}{FORMS}/{mock.form1.id}", json=body)

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res) == 1
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "slug"
    assert res["detail"][0]["msg"] == "ensure this value has at most 255 characters"
    assert res["detail"][0]["ctx"]["limit_value"] == 255


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update a norm with multiple slugs.
# ---------------------------------------------
def test_put_forms_by_id_multiple_slugs(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    expected = convert_db_form_to_dict(mock.form1)

    body = {
        "slug": "test_post_forms_multiple_slugs 1",  # noqa: F601
        "slug": "test_post_forms_multiple_slugs 2",  # noqa: F601
        "slug": "test_post_forms_multiple_slugs 3",  # noqa: F601
    }
    expected["slug"] = "test-post-forms-multiple-slugs-3"

    response = client.put(f"{BASE_PATH_V1}{FORMS}/{mock.form1.id}", json=body)
    res = response.json()
    _assert_found(response, mock.form1)
    assert res["shares"][0]["slug"] != body["slug"]


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update a norm with multiple slugs.
# ---------------------------------------------
def test_put_forms_by_slug_multiple_slugs(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    expected = convert_db_form_to_dict(mock.form1)
    body = {
        "slug": "test_post_forms_multiple_slugs 1",  # noqa: F601
        "slug": "test_post_forms_multiple_slugs 2",  # noqa: F601
        "slug": "test_post_forms_multiple_slugs 3",  # noqa: F601
    }
    expected["slug"] = "test-post-forms-multiple-slugs-3"

    response = client.put(f"{BASE_PATH_V1}{FORMS}/{mock.form1.slug}", json=body)
    res = response.json()
    _assert_found(response, mock.form1)
    assert res["shares"][0]["slug"] != body["slug"]


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update a form where combination slug
# and creator_organization_uuid already exists.
# Combination slug and creator_organization_uuid is unique.
# ---------------------------------------------
def test_put_forms_by_id_slug_organization_exists(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {"slug": mock.form6.slug}

    response = client.put(f"{BASE_PATH_V1}{FORMS}/{mock.form1.id}", json=body)

    assert response.status_code == http_status.HTTP_409_CONFLICT
    res = response.json()
    assert len(res["detail"]) == 1
    assert (
        res["detail"][0]["msg"]
        == error_messages.FORM_COMBINATION_SLUG_CREATOR_ORGANIZATION_UUID_ALREADY_EXIST_ERROR
    )


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update a form where combination slug
# and creator_organization_uuid not exists.
# Combination slug and creator_organization_uuid is unique.
# ---------------------------------------------
def test_put_forms_by_id_slug_organization_not_exists(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {"slug": "slug_not_exists"}

    response = client.put(f"{BASE_PATH_V1}{FORMS}/{mock.form1.id}", json=body)
    assert response.status_code == http_status.HTTP_200_OK


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update a form where combination slug
# and creator_organization_uuid already exists.
# Combination slug and creator_organization_uuid is unique.
# ---------------------------------------------
def test_put_forms_by_slug_slug_organization_exists(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {"slug": mock.form6.slug}

    response = client.put(f"{BASE_PATH_V1}{FORMS}/{mock.form1.slug}", json=body)

    assert response.status_code == http_status.HTTP_409_CONFLICT
    res = response.json()
    assert len(res["detail"]) == 1
    assert (
        res["detail"][0]["msg"]
        == error_messages.FORM_COMBINATION_SLUG_CREATOR_ORGANIZATION_UUID_ALREADY_EXIST_ERROR
    )


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update a form where combination slug
# and creator_organization_uuid not exists.
# Combination slug and creator_organization_uuid is unique.
# ---------------------------------------------
def test_put_forms_by_slug_slug_organization_not_exists(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {"slug": "slug_not_exists"}

    response = client.put(f"{BASE_PATH_V1}{FORMS}/{mock.form1.slug}", json=body)
    assert response.status_code == http_status.HTTP_200_OK


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form with a prefilled url.
# Ignore the url, because it is not part
# of the model.
# ---------------------------------------------
@pytest.mark.parametrize(
    "url_value",
    [
        "..;/",
        "",
        "   ",
        "1233344",
        "11112272345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
        "http://localhost",
        None,
    ],
)
def test_put_forms_with_url(client: TestClient, test_db: Session, url_value: str):
    mock = add_mock_data_to_db(test_db)

    expected = convert_db_form_to_dict(mock.form1)
    body = {"url": f"{url_value}"}
    expected["url"] = body["url"]

    for id in [mock.form1.id, mock.form1.slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json=body)
        _assert_found(response, mock.form1)


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form with status inactive.
# ---------------------------------------------
def test_put_forms_status_inactive(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    expected = convert_db_form_to_dict(mock.form1)
    body = {"status": mock.STATUS_INACTIVE}
    expected["status"] = body["status"]

    for id in [mock.form1.id, mock.form1.slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json=body)
        _assert_found(response, mock.form1)


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form with status active.
# ---------------------------------------------
def test_put_forms_status_active(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    expected = convert_db_form_to_dict(mock.form1)
    body = {"status": mock.STATUS_ACTIVE}
    expected["status"] = body["status"]
    for id in [mock.form1.id, mock.form1.slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json=body)
        _assert_found(response, mock.form1)


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is set to application/json.
# Test update a form with status active.
# ---------------------------------------------
def test_put_forms_status_active_with_content_type_json(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)
    expected = convert_db_form_to_dict(mock.form1)
    body = {"status": mock.STATUS_ACTIVE}
    expected["status"] = body["status"]

    headers = {"Content-Type": CONTENT_TYPE_VALUE_APPLICATION_JSON}

    for id in [mock.form1.id, mock.form1.slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json=body, headers=headers)
        _assert_found(response, mock.form1)


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update form with invalid status value.
# ---------------------------------------------
@pytest.mark.parametrize(
    "status_invalid_value",
    [
        "..;/",
        "",
        "   ",
        "1233344",
        "72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
        "http://localhost",
        "INVALID_ACTIVE",
        "active",
        "inactive",
        None,
    ],
)
def test_put_forms_status_invalid_value(
    client: TestClient, test_db: Session, status_invalid_value: str
):
    mock = add_mock_data_to_db(test_db)

    body = {"status": f"{status_invalid_value}"}

    for id in [mock.form1.id, mock.form1.slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json=body)

        assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
        res = response.json()
        assert len(res) == 1
        assert len(res["detail"][0]["ctx"]["enum_values"]) == 2
        assert res["detail"][0]["ctx"]["enum_values"][0] == mock.STATUS_ACTIVE
        assert res["detail"][0]["ctx"]["enum_values"][1] == mock.STATUS_INACTIVE
        assert res["detail"][0]["loc"]["field"] == "status"
        assert res["detail"][0]["loc"]["source"] == "body"
        assert (
            res["detail"][0]["msg"]
            == "value is not a valid enumeration member; permitted: 'ACTIVE', 'INACTIVE'"
        )
        assert res["detail"][0]["type"] == "type_error.enum"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update form with empty forms field
# ---------------------------------------------
def test_put_forms_form_empty(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    expected = convert_db_form_to_dict(mock.form1)
    body: dict = {"form": {"field": "value"}}
    expected["form"] = body["form"]

    for id in [mock.form1.id, mock.form1.slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json=body)
        _assert_found(response, mock.form1)


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update form with additional fields.
# ---------------------------------------------
def test_put_forms_with_additional_fields(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "notvalid1": "formulier new 2",
        "notvalid2": mock.STATUS_ACTIVE,
    }

    for id in [mock.form1.id, mock.form1.slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json=body)
        _assert_found(response, mock.form1)


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update a form with an organization uuid.
# The organization uuid is not a valid uuid.
# Ignore the following request fields.
# - organizationUuid
# ---------------------------------------------
@pytest.mark.parametrize(
    "organization_value",
    ["", " ", "123"],
)
def test_put_forms_organization_uuid_not_valid(
    client: TestClient, test_db: Session, organization_value: str
):
    mock = add_mock_data_to_db(test_db)

    expected = convert_db_form_to_dict(mock.form1)
    body = {
        "organizationUuid": f"{organization_value}",
    }
    expected["organization_uuid"] = body["organizationUuid"]

    for id in [mock.form1.id, mock.form1.slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json=body)
        _assert_found(response, mock.form1)


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update a form with an organization uuid.
# The organizatin id is set to None.
# Ignore the following request fields.
# - organizationUuid
# ---------------------------------------------
def test_put_forms_organization_uuid_none(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    expected = convert_db_form_to_dict(mock.form1)

    body = {
        "organizationUuid": None,
    }
    expected["organization_uuid"] = body["organizationUuid"]

    for id in [mock.form1.id, mock.form1.slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json=body)
        _assert_found(response, mock.form1)


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update a form with an organization uuid.
# The organization uuid is a valid uuid.
# Ignore the following request fields.
# - organizationUuid
# ---------------------------------------------
@pytest.mark.parametrize(
    "organization_value",
    [
        "12345678-1234-4123-a123-1234567890ab",
        "00000000-0000-4000-a000-000000000000",
        "aaaaaaaa-aaaa-4aaa-aaaa-aaaaaaaaaaaa",
    ],
)
def test_put_forms_organization_uuid_valid(
    client: TestClient, test_db: Session, organization_value: str
):
    mock = add_mock_data_to_db(test_db)

    expected = convert_db_form_to_dict(mock.form1)

    body = {"organizationUuid": f"{organization_value}"}
    expected["organization_uuid"] = body["organizationUuid"]

    for id in [mock.form1.id, mock.form1.slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json=body)
        _assert_found(response, mock.form1)


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form with a prefilled config.
# ---------------------------------------------
@pytest.mark.parametrize(
    "config_value",
    [
        [{"key": "urlAfsluitenKnop", "value": "https://www.xxllnc.nl/"}],
        [{}],
        [{"key": "something", "value": "theValue"}],
        [
            {
                "key": "urlAfsluitenKnop",
                "value": "72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
            }
        ],
        None,
    ],
)
def test_put_forms_config(client: TestClient, test_db: Session, config_value: list):
    mock = add_mock_data_to_db(test_db)

    expected = convert_db_form_to_dict(mock.form1)

    body = {
        "name": "test_put_forms_config",
        "status": mock.STATUS_ACTIVE,
        "form": {"field": "value"},
        "config": config_value,
    }
    for field in body.keys():
        expected[field] = body[field]

    response = client.put(f"{BASE_PATH_V1}{FORMS}/{mock.form1.id}", json=body)
    _assert_found(response, mock.form1)


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form with a prefilled organizationUrl.
# The organizationUrl will be ignored, because
# it is not part of the model.
# ---------------------------------------------
@pytest.mark.parametrize(
    "organization_url_value",
    [
        "..;/",
        "",
        "   ",
        "1233344",
        "72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
        "http://localhost",
        None,
    ],
)
def test_put_forms_organization_url(
    client: TestClient, test_db: Session, organization_url_value: str
):
    mock = add_mock_data_to_db(test_db)

    expected = convert_db_form_to_dict(mock.form1)

    body = {
        "name": "test_put_forms_organization_url",
        "status": mock.STATUS_INACTIVE,
        "form": {"field": "value"},
        "organizationUrl": f"{organization_url_value}",
    }
    for field in body.keys():
        expected[field] = body[field]

    for id in [mock.form1.id, mock.form1.slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json=body)
        _assert_found(response, mock.form1)


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form with an empty publishDate.
# ---------------------------------------------
def test_put_forms_publish_date_is_empty(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    expected = convert_db_form_to_dict(mock.form1)

    body = {
        "name": "test_put_forms_publish_date_is_empty",
        "status": mock.STATUS_INACTIVE,
        "form": {"field": "value"},
        "publishDate": "",
    }
    for field in body.keys():
        expected[field] = body[field]

    expected["publishDate"] = None

    for id in [mock.form1.id, mock.form1.slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json=body)
        _assert_found(response, mock.form1)


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form with publishDate set to None.
# ---------------------------------------------
def test_put_forms_publish_date_is_none(client: TestClient, test_db: Session):

    mock = add_mock_data_to_db(test_db)

    expected = convert_db_form_to_dict(mock.form1)
    body = {
        "name": "test_put_forms_publish_date_is_none",
        "status": mock.STATUS_INACTIVE,
        "form": {"field": "value"},
        "publishDate": None,
    }
    for field in body.keys():
        expected[field] = body[field]

    for id in [mock.form1.id, mock.form1.slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json=body)
        _assert_found(response, mock.form1)


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form with an invalid publishDate.
#
# https://fastapi.tiangolo.com/tutorial/extra-data-types/
# datetime.date:
# - Python datetime.date.
# - In requests and responses will be represented as a str in
#   ISO 8601 format, like: 2008-09-15.
#
# https://pydantic-docs.helpmanual.io/usage/types/#datetime-types
# date fields can be:
# - date, existing date object
# - int or float, see datetime
# - str, following formats work:
#   - YYYY-MM-DD
#   - int or float, see datetime
# ---------------------------------------------
@pytest.mark.parametrize(
    "publish_date_invalid_value",
    [
        "..;/",
        "0000-00-00",
        "0000-00-01",
        "0000-01-00",
        "0000-01-01",
        "01-01-2020",
        "2021-01-00",
        "2021-00-01",
        "0000-00-00",
        "2021-000-01",
        "2021-01-00",
        "2021-13-01",
        "2021-00-00",
        "2021-01-32",
        "2021-01",
        "2021-01-",
        "   ",
        "http://localhost",
        "None",
    ],
)
def test_put_forms_publish_date_invalid_value(
    client: TestClient, test_db: Session, publish_date_invalid_value: str
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_put_forms_publish_date_invalid_value",
        "status": mock.STATUS_INACTIVE,
        "form": {"field": "value"},
        "publishDate": f"{publish_date_invalid_value}",
    }

    for id in [mock.form1.id, mock.form1.slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json=body)

        assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
        res = response.json()
        assert len(res["detail"]) == 1
        assert res["detail"][0]["loc"]["source"] == "body"
        assert res["detail"][0]["loc"]["field"] == "publishDate"
        assert res["detail"][0]["msg"] == "invalid date format"
        assert res["detail"][0]["type"] == "value_error.date"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form with a valid publishDate.
#
# https://fastapi.tiangolo.com/tutorial/extra-data-types/
# datetime.date:
# - Python datetime.date.
# - In requests and responses will be represented as a str in
#   ISO 8601 format, like: 2008-09-15.
#
# https://pydantic-docs.helpmanual.io/usage/types/#datetime-types
# date fields can be:
# - date, existing date object
# - int or float, see datetime
# - str, following formats work:
#   - YYYY-MM-DD
#   - int or float, see datetime
# ---------------------------------------------
@pytest.mark.parametrize(
    "publish_date_request, publish_date_response",
    [
        ("0001-01-01", "0001-01-01"),
        ("2001-01-01", "2001-01-01"),
        ("2021-12-31", "2021-12-31"),
        ("0", "1970-01-01"),
        ("2021", "1970-01-01"),
        ("1233344", "1970-01-15"),
        (
            "72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
            "9999-12-31",
        ),
        ("-0", "1970-01-01"),
        ("-1", "1969-12-31"),
        ("-30", "1969-12-31"),
        (0, "1970-01-01"),
        (2021, "1970-01-01"),
        (1233344, "1970-01-15"),
        (
            72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456,  # noqa: E501
            "9999-12-31",
        ),
        (-10, "1969-12-31"),
        (str(date(2001, 1, 1)), "2001-01-01"),
        (str(date(2021, 12, 20)), "2021-12-20"),
    ],
)
def test_put_forms_publish_date_valid_value(
    client: TestClient,
    test_db: Session,
    publish_date_request: Union[str, int, float],
    publish_date_response: str,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_put_forms_publish_date_valid_value",
        "status": mock.STATUS_INACTIVE,
        "form": {"field": "value"},
        "publishDate": f"{publish_date_request}",
    }

    for id in [mock.form1.id, mock.form1.slug]:
        response = client.put(f"{BASE_PATH_V1}{FORMS}/{id}", json=body)
        res = response.json()
        _assert_found(response, mock.form1)
        assert res["shares"][0]["publishDate"] == publish_date_response
