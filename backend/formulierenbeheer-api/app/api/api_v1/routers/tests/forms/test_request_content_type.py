import pytest
import requests
from app.api.api_v1.routers.tests import (
    CONTENT_TYPE_VALUE_APPLICATION_JSON,
    FORM_DETAILS_FIELDS,
    FORMS,
    HEADERS,
)
from app.api.api_v1.routers.tests.asserts import assert_found_range
from app.api.api_v1.routers.tests.forms.test_get_by_id import _assert_found
from app.api.api_v1.routers.tests.forms.test_mock_data import add_mock_data_to_db
from app.core.config import BASE_PATH_V1
from fastapi import status as http_status
from fastapi.testclient import TestClient
from requests.models import Response
from sqlalchemy.orm import Session


def _assert_415_unsupported_media_type(response: Response):
    assert response.status_code == http_status.HTTP_415_UNSUPPORTED_MEDIA_TYPE

    expected_body = {
        "detail": [
            {
                "loc": {"source": "header", "field": "Content-Type"},
                "msg": "Content-Type header must be application/json",
                "type": "value_error.missing",
            }
        ]
    }
    assert response.json() == expected_body

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test get request without a request content type.
# A sender that generates a message containing a payload body SHOULD generate
# a Content-Type header field in that message unless the intended media type
# of the enclosed representation is unknown to the sender.
# ---------------------------------------------
def test_get_list_request_without_request_header_content_type(client: TestClient):
    response = client.get(f"{BASE_PATH_V1}{FORMS}", headers=HEADERS)
    assert_found_range(response, True)


# ---------------------------------------------
# Test get request with a request content type application/json.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_accept",
    [
        "application/javascript",
        CONTENT_TYPE_VALUE_APPLICATION_JSON,  # allowed
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "not valid",
        "application/*",
        "*/*",
        "",
        None,
    ],
)
def test_get_list_request_with_request_header_content_type_application_json(
    client: TestClient, http_request_header_accept: str
):
    headers = {
        **HEADERS,
        "Accept": f"{http_request_header_accept}",
        "Content-Type": CONTENT_TYPE_VALUE_APPLICATION_JSON,
    }
    response = client.get(f"{BASE_PATH_V1}{FORMS}", headers=headers)
    assert_found_range(response, True)


# ---------------------------------------------
# Test get request with an invalid request content type.
# The Content-Type header attribute specifies the format of the data in the
# request body so that receiver can parse it into appropriate format.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_content_type",
    [
        "application/javascript",
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "",
        "not valid",
        None,
    ],
)
def test_get_list_request_with_request_header_content_type_with_invalid_value(
    client: TestClient, http_request_header_content_type: str
):
    headers = {**HEADERS, "Content-Type": f"{http_request_header_content_type}"}
    response = client.get(f"{BASE_PATH_V1}{FORMS}", headers=headers)
    assert_found_range(response, True)


# ---------------------------------------------
# Test get request without a request content type.
# A sender that generates a message containing a payload body SHOULD generate
# a Content-Type header field in that message unless the intended media type
# of the enclosed representation is unknown to the sender.
# ---------------------------------------------
def test_get_by_id_request_without_request_header_content_type(
    client: TestClient, test_db: Session
):
    mock = add_mock_data_to_db(test_db)
    response = client.get(f"{BASE_PATH_V1}{FORMS}/{mock.form1.id}", headers=HEADERS)
    assert_found_range(response, False)


# ---------------------------------------------
# Test get request with a request content type application/json.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_accept",
    [
        "application/javascript",
        CONTENT_TYPE_VALUE_APPLICATION_JSON,  # allowed
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "not valid",
        "application/*",
        "*/*",
        "",
        None,
    ],
)
def test_get_by_id_request_with_request_header_content_type_application_json(
    client: TestClient, http_request_header_accept: str, test_db: Session
):
    mock = add_mock_data_to_db(test_db)

    headers = {
        **HEADERS,
        "Accept": f"{http_request_header_accept}",
        "Content-Type": CONTENT_TYPE_VALUE_APPLICATION_JSON,
    }

    response = client.get(f"{BASE_PATH_V1}{FORMS}/{mock.form1.id}", headers=headers)
    assert_found_range(response, False)


# ---------------------------------------------
# Test get request with an invalid request content type.
# The Content-Type header attribute specifies the format of the data in the
# request body so that receiver can parse it into appropriate format.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_content_type",
    [
        "application/javascript",
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "",
        "not valid",
        None,
    ],
)
def test_get_by_id_request_with_request_header_content_type_with_invalid_value(
    client: TestClient, http_request_header_content_type: str, test_db: Session
):
    mock = add_mock_data_to_db(test_db)
    headers = {**HEADERS, "Content-Type": f"{http_request_header_content_type}"}
    response = client.get(f"{BASE_PATH_V1}{FORMS}/{mock.form1.id}", headers=headers)
    assert_found_range(response, False)


# ---------------------------------------------
# Test get request with an invalid request content type.
# The Content-Type header attribute specifies the format of the data in the
# request body so that receiver can parse it into appropriate format.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_content_type",
    [
        " application/javascript",
        " application/octet-stream",
        " multipart/form-data",
        " text/html",
        " text/plain",
        " ",
        " not valid",
    ],
)
def test_get_by_id_request_with_request_header_content_type_with_invalid_value_2(
    client: TestClient, http_request_header_content_type: str, test_db: Session
):
    mock = add_mock_data_to_db(test_db)
    headers = {"Content-Type": f"{http_request_header_content_type}"}

    # requests.exceptions.InvalidHeader: Invalid return character or
    # leading space in header: Content-Type
    with pytest.raises(requests.exceptions.InvalidHeader):
        client.get(f"{BASE_PATH_V1}{FORMS}/{mock.form1.id}", headers=headers)


# ---------------------------------------------
# Test post request without a request content type.
# The body contains JSON data.
# Content type is not mandatory.
# A sender that generates a message containing a payload body SHOULD generate
# a Content-Type header field in that message unless the intended media type
# of the enclosed representation is unknown to the sender.
# The missing content type is ignored.The response Content-Type is always 'application/json'.
# ---------------------------------------------
def test_post_request_without_request_header_content_type(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_request_without_request_header_content_type",
        "status": mock.STATUS_INACTIVE,
        "form": {},
        "publishDate": None,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)

    assert response.status_code == http_status.HTTP_201_CREATED
    res = response.json()
    assert len(res) == FORM_DETAILS_FIELDS

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test post request with a request content type application/json.
# The body contains JSON data.
# The Accept header is ignored. the response Content-Type is always 'application/json'.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_accept",
    [
        "application/javascript",
        CONTENT_TYPE_VALUE_APPLICATION_JSON,  # allowed
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "not valid",
        "application/*",
        "*/*",
        "",
        None,
    ],
)
def test_post_request_with_request_header_content_type_application_json(
    client: TestClient, http_request_header_accept: str, test_db: Session
):
    mock = add_mock_data_to_db(test_db)

    headers = {
        "Accept": f"{http_request_header_accept}",
        "Content-Type": CONTENT_TYPE_VALUE_APPLICATION_JSON,
    }

    body = {
        "name": "formulier new 1",
        "status": mock.STATUS_INACTIVE,
        "form": {},
        "publishDate": None,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", headers=headers, json=body)

    assert response.status_code == http_status.HTTP_201_CREATED
    res = response.json()
    assert len(res) == FORM_DETAILS_FIELDS

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test post request with an invalid request content type.
# The body contains JSON data.
# The Content-Type header attribute specifies the format of the data in the
# request body so that receiver can parse it into appropriate format.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_content_type",
    [
        "application/javascript",
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "not valid",
        "",
        None,
    ],
)
def test_post_request_with_request_header_content_type_with_invalid_value(
    client: TestClient, http_request_header_content_type: str, test_db: Session
):
    mock = add_mock_data_to_db(test_db)

    headers = {"Content-Type": f"{http_request_header_content_type}"}

    body = {
        "name": "test_post_request_with_request_header_content_type_with_invalid_value",
        "status": mock.STATUS_INACTIVE,
        "form": {},
        "publishDate": None,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", headers=headers, json=body)

    _assert_415_unsupported_media_type(response)


# ---------------------------------------------
# Test put request without a request content type.
# The body contains JSON data.
# Content type is not mandatory.
# A sender that generates a message containing a payload body SHOULD generate
# a Content-Type header field in that message unless the intended media type
# of the enclosed representation is unknown to the sender.
# ---------------------------------------------
def test_put_request_without_request_header_content_type(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body: dict = {
        "status": mock.STATUS_INACTIVE,
        "form": {"test_put_method": "value"},
    }

    response = client.put(f"{BASE_PATH_V1}{FORMS}/{mock.form1.id}", json=body)
    _assert_found(response, mock.form1)

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test put request with a request content type application/json.
# The body contains JSON data.
# The Accept header is ignored. the response Content-Type is always 'application/json'.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_accept",
    [
        "application/javascript",
        CONTENT_TYPE_VALUE_APPLICATION_JSON,  # allowed
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "not valid",
        "application/*",
        "*/*",
        "",
        None,
    ],
)
def test_put_request_with_request_header_content_type_application_json(
    client: TestClient, http_request_header_accept: str, test_db: Session
):
    mock = add_mock_data_to_db(test_db)

    headers = {
        "Accept": f"{http_request_header_accept}",
        "Content-Type": CONTENT_TYPE_VALUE_APPLICATION_JSON,
    }

    body: dict = {
        "status": mock.STATUS_INACTIVE,
        "form": {"test_put_method": "value"},
    }

    response = client.put(f"{BASE_PATH_V1}{FORMS}/{mock.form1.id}", headers=headers, json=body)
    _assert_found(response, mock.form1)

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test put request with an invalid request content type.
# The body contains JSON data.
# The Content-Type header attribute specifies the format of the data in the
# request body so that receiver can parse it into appropriate format.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_content_type",
    [
        "application/javascript",
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "not valid",
        None,
    ],
)
def test_put_request_with_request_header_content_type_with_invalid_value(
    client: TestClient, http_request_header_content_type: str, test_db: Session
):
    mock = add_mock_data_to_db(test_db)

    headers = {"Content-Type": f"{http_request_header_content_type}"}

    body: dict = {
        "status": mock.STATUS_INACTIVE,
        "form": {"test_put_method": "value"},
    }

    response = client.put(f"{BASE_PATH_V1}{FORMS}/{mock.form1.id}", headers=headers, json=body)

    _assert_415_unsupported_media_type(response)


# ---------------------------------------------
# Test delete request without a request content type.
# The content type header is ignored. the response Content-Type is always 'application/json'.
# ---------------------------------------------
def test_delete_request_without_request_header_content_type(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    response = client.delete(f"{BASE_PATH_V1}{FORMS}/{mock.form1.id}")

    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 1

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test delete request with a request content type application/json.
# The Accept header is ignored. the response Content-Type is always 'application/json'.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_accept",
    [
        "application/javascript",
        CONTENT_TYPE_VALUE_APPLICATION_JSON,  # allowed
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "not valid",
        "application/*",
        "*/*",
        "",
        None,
    ],
)
def test_delete_request_with_request_header_content_type_application_json(
    client: TestClient, http_request_header_accept: str, test_db: Session
):
    mock = add_mock_data_to_db(test_db)

    headers = {
        "Accept": f"{http_request_header_accept}",
        "Content-Type": CONTENT_TYPE_VALUE_APPLICATION_JSON,
    }

    response = client.delete(f"{BASE_PATH_V1}{FORMS}/{mock.form1.id}", headers=headers)

    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 1

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test delete request with an invalid request content type.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_content_type",
    [
        "application/javascript",
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "",
        "not valid",
        None,
    ],
)
def test_delete_request_with_request_header_content_type_with_invalid_value(
    client: TestClient, http_request_header_content_type: str, test_db: Session
):
    mock = add_mock_data_to_db(test_db)

    headers = {
        "Content-Type": f"{http_request_header_content_type}",
    }

    response = client.delete(f"{BASE_PATH_V1}{FORMS}/{mock.form1.id}", headers=headers)

    _assert_415_unsupported_media_type(response)
