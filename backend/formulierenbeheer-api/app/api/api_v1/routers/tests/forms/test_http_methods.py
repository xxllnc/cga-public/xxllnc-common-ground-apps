import pytest
from app.api.api_v1.routers.tests import (
    FORM_DETAILS_FIELDS,
    FORMS,
    HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER,
    HTTP_METHOD_OVERRIDE_URI_PARAMETER,
)
from app.api.api_v1.routers.tests.asserts import assert_method_not_allowed
from app.api.api_v1.routers.tests.forms.test_get_by_id import _assert_found
from app.api.api_v1.routers.tests.forms.test_mock_data import add_mock_data_to_db
from app.api.api_v1.routers.tests.helper import empty_body
from app.api.api_v1.routers.tests.parameters import http_method_override
from app.core.config import BASE_PATH_V1
from app.resources import error_messages
from fastapi import status as http_status
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session
from typing import Final

SUPPORTED_METHODS: Final = ["delete", "get", "post", "put"]
UNSUPPORTED_METHODS: Final = [ele for ele in http_method_override if ele not in SUPPORTED_METHODS]


def _add_form(client: TestClient, mock) -> str:
    body: dict = {
        "name": "form_test_override",
        "status": mock.STATUS_ACTIVE,
        "form": {},
        "publish_date": None,
    }

    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body)
    assert response.status_code == http_status.HTTP_201_CREATED
    res = response.json()
    assert len(res) == FORM_DETAILS_FIELDS
    return res["id"]


# ---------------------------------------------
# Testing an API with HTTP methods that are not supported.
# Reject request with HTTP response code 405 (Method not allowed).
# Supported HTTP methods: DELETE, GET, POST and PUT
# ---------------------------------------------
@pytest.mark.parametrize("http_method", UNSUPPORTED_METHODS)
@pytest.mark.parametrize("request_body", [None, empty_body])
def test_http_methods_not_supported(client: TestClient, http_method: str, request_body: str):
    response = client.request(http_method, f"{BASE_PATH_V1}{FORMS}", json=request_body)
    assert_method_not_allowed(response, http_method)


# ---------------------------------------------
# Test HTTP Method Overriding for the http method GET
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("header", HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER)
def test_get_method_http_override(client: TestClient, method: str, header: str):
    headers = {header: method}
    response = client.get(f"{BASE_PATH_V1}{FORMS}", headers=headers)
    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 0


# ---------------------------------------------
# Test HTTP Method Overriding for the http method GET
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("parameter", HTTP_METHOD_OVERRIDE_URI_PARAMETER)
def test_get_method_http_override_uri_parameter(client: TestClient, method: str, parameter: str):
    response = client.get(f"{BASE_PATH_V1}{FORMS}?{parameter}={method.upper()}")
    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 0


# ---------------------------------------------
# Test HTTP Method Overriding for the http method POST
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("header", HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER)
def test_post_method_http_override(client: TestClient, test_db: Session, method: str, header: str):
    mock = add_mock_data_to_db(test_db)

    FORM_NAME: Final[str] = "formulier new 2"
    suffix: int = 0
    body: dict = {
        "name": FORM_NAME,
        "status": mock.STATUS_ACTIVE,
        "form": {},
        "publish_date": None,
    }

    headers = {header: method}
    suffix += 1
    body["name"] = FORM_NAME + str(suffix)
    response = client.post(f"{BASE_PATH_V1}{FORMS}", json=body, headers=headers)
    assert response.status_code == http_status.HTTP_201_CREATED
    res = response.json()
    assert len(res) == FORM_DETAILS_FIELDS


# ---------------------------------------------
# Test HTTP Method Overriding for the http method POST
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("parameter", HTTP_METHOD_OVERRIDE_URI_PARAMETER)
def test_post_method_http_override_uri_parameter(
    client: TestClient, test_db: Session, method: str, parameter: str
):
    mock = add_mock_data_to_db(test_db)

    FORM_NAME: Final[str] = "formulier new 2"
    suffix: int = 0
    body: dict = {
        "name": FORM_NAME,
        "status": mock.STATUS_ACTIVE,
        "form": {},
        "publish_date": None,
    }

    suffix += 1
    body["name"] = FORM_NAME + str(suffix)
    response = client.post(f"{BASE_PATH_V1}{FORMS}?{parameter}={method.upper()}", json=body)
    assert response.status_code == http_status.HTTP_201_CREATED
    res = response.json()
    assert len(res) == FORM_DETAILS_FIELDS


# ---------------------------------------------
# Test HTTP Method Overriding for the http method PUT
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("header", HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER)
def test_put_method_http_override(client: TestClient, test_db: Session, method: str, header: str):
    mock = add_mock_data_to_db(test_db)
    body: dict = {"status": mock.STATUS_ACTIVE, "form": {"field": "value"}}

    headers = {header: method}
    response = client.put(f"{BASE_PATH_V1}{FORMS}/{mock.form1.id}", json=body, headers=headers)
    _assert_found(response, mock.form1)


# ---------------------------------------------
# Test HTTP Method Overriding for the http method PUT
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("parameter", HTTP_METHOD_OVERRIDE_URI_PARAMETER)
def test_put_method_http_override_uri_parameter(
    client: TestClient, test_db: Session, method: str, parameter: str
):
    mock = add_mock_data_to_db(test_db)
    body: dict = {"status": mock.STATUS_ACTIVE, "form": {"field": "value"}}

    response = client.put(
        f"{BASE_PATH_V1}{FORMS}/{mock.form1.id}?{parameter}={method.upper()}", json=body
    )
    _assert_found(response, mock.form1)


# ---------------------------------------------
# Test HTTP Method Overriding for the http method DELETE
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("header", HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER)
def test_delete_method_http_override(
    client: TestClient, test_db: Session, method: str, header: str
):
    mock = add_mock_data_to_db(test_db)
    headers = {header: method}
    form_id: str = _add_form(client, mock)
    response = client.delete(f"{BASE_PATH_V1}{FORMS}/{form_id}", headers=headers)
    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 1
    assert res["message"] == error_messages.FORM_IS_DELETED.format(form_id)


# ---------------------------------------------
# Test HTTP Method Overriding for the http method DELETE
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("parameter", HTTP_METHOD_OVERRIDE_URI_PARAMETER)
def test_delete_method_http_override_uri_parameter(
    client: TestClient, test_db: Session, method: str, parameter: str
):
    mock = add_mock_data_to_db(test_db)
    form_id: str = _add_form(client, mock)
    response = client.delete(f"{BASE_PATH_V1}{FORMS}/{form_id}?{parameter}={method.upper()}")
    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 1
    assert res["message"] == error_messages.FORM_IS_DELETED.format(form_id)


# ---------------------------------------------
# Test the unsupported http methods for HTTP Method Overriding.
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("unsupported_method", UNSUPPORTED_METHODS)
@pytest.mark.parametrize("header", HTTP_METHOD_OVERRIDE_HTTP_REQUEST_HEADER)
def test_http_method_overriding_for_unsupported_http_methods(
    client: TestClient, method: str, unsupported_method: str, header: str
):
    headers = {header: method}
    response = client.request(unsupported_method, f"{BASE_PATH_V1}{FORMS}", headers=headers)
    assert_method_not_allowed(response, unsupported_method)


# ---------------------------------------------
# Test the unsupported http methods for HTTP Method Overriding.
# ---------------------------------------------
@pytest.mark.parametrize("method", http_method_override)
@pytest.mark.parametrize("unsupported_method", UNSUPPORTED_METHODS)
@pytest.mark.parametrize("parameter", HTTP_METHOD_OVERRIDE_URI_PARAMETER)
def test_http_method_overriding_for_unsupported_http_methods_uri_parameter(
    client: TestClient, method: str, unsupported_method: str, parameter: str
):
    response = client.request(
        unsupported_method, f"{BASE_PATH_V1}{FORMS}?{parameter}={method.upper()}"
    )
    assert_method_not_allowed(response, unsupported_method)
