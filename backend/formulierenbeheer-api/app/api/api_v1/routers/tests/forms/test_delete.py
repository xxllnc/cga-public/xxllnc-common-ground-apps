import pytest
import uuid
from app.api.api_v1.routers.tests import CONTENT_TYPE_VALUE_APPLICATION_JSON, FORMS
from app.api.api_v1.routers.tests.forms.test_mock_data import add_mock_data_to_db
from app.api.api_v1.routers.tests.share_forms.test_mock_data import add_share_forms_mock_data_to_db
from app.core.config import BASE_PATH_V1
from app.resources import error_messages
from fastapi import status as http_status
from fastapi.testclient import TestClient
from requests.models import Response
from sqlalchemy.orm import Session
from typing import Union


def _assert_not_found(response: Response, id: Union[uuid.UUID, str]):
    assert response.status_code == http_status.HTTP_404_NOT_FOUND
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["msg"] == error_messages.FORM_NOT_FOUND_WITH.format(id)
    assert res["detail"][0]["detail"] == error_messages.NOT_FOUND


def _assert_deleted(response: Response, id: Union[uuid.UUID, str]):
    assert response.status_code == http_status.HTTP_200_OK
    res = response.json()
    assert len(res) == 1
    assert res["message"] == error_messages.FORM_IS_DELETED.format(id)


def _assert_forbidden(response: Response, id: Union[uuid.UUID, str]):
    assert response.status_code == http_status.HTTP_403_FORBIDDEN
    res = response.json()
    assert len(res) == 2
    assert res["detail"][0]["msg"] == error_messages.NOT_YOUR_FORM.format(id)
    assert res["detail"][0]["detail"] == error_messages.FORBIDDEN


def _assert_conflict(response: Response, id: Union[uuid.UUID, str]):
    assert response.status_code == http_status.HTTP_409_CONFLICT
    res = response.json()
    assert len(res) == 2
    assert res["detail"][0]["msg"] == error_messages.FORM_NOT_DELETED
    assert res["detail"][0]["detail"] == error_messages.CONFLICT


# ---------------------------------------------
# Delete "/forms/{id}"
# ---------------------------------------------
@pytest.mark.parametrize("mock_id", [1, 2, 3, 4, 5, 6, 7, 8, 9])
@pytest.mark.parametrize("id_or_slug", ["id", "slug"])
@pytest.mark.parametrize("headers", [None, {"Content-Type": CONTENT_TYPE_VALUE_APPLICATION_JSON}])
def test_delete_forms_without_content_type(
    client: TestClient, test_db: Session, mock_id: str, id_or_slug: str, headers: dict
):
    mock = add_mock_data_to_db(test_db)
    form = getattr(mock, f"form{mock_id}")
    form_id = form.id if id_or_slug == "id" else form.slug

    response = (
        client.delete(f"{BASE_PATH_V1}{FORMS}/{form_id}")
        if headers is None
        else client.delete(f"{BASE_PATH_V1}{FORMS}/{form_id}", headers=headers)
    )

    _assert_deleted(response, form_id)


# ---------------------------------------------
# Delete "/forms/{id}"
# Test delete form twice.
# ---------------------------------------------
def test_delete_forms_twice(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    for id in [mock.form1.id, mock.form2.slug]:
        response1 = client.delete(f"{BASE_PATH_V1}{FORMS}/{id}")
        response2 = client.delete(f"{BASE_PATH_V1}{FORMS}/{id}")

        _assert_deleted(response1, id)
        _assert_not_found(response2, id)


# ---------------------------------------------
# Delete "/forms/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_path_parameter_id_value",
    [
        "99",
        "-1",
        "99999999999999999999999999999999999999999999999999999999999999",
        "_",
        "aa",
        "unknown",
        "0",
        "__",
        "<>",
        "111111111111111111111111111111111111111111111111111111111",
        " AND 1::int=1",
        " UNION ALL SELECT NULL,version(),NULL LIMIT 1 OFFSET 1--",
        " or 1=1",
        " or = 1' or '1' = '1",
        "not valid",
    ],
)
def test_delete_forms_with_invalid_path_parameter_id_value(
    client: TestClient, test_db: Session, invalid_path_parameter_id_value: str
):
    add_mock_data_to_db(test_db)

    response = client.delete(f"{BASE_PATH_V1}{FORMS}/{invalid_path_parameter_id_value}")

    _assert_not_found(response, invalid_path_parameter_id_value)


# ---------------------------------------------
# Delete "/forms/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_path_parameter_id_value",
    [
        ">'\"()%26%25<acx><ScRiPt%20>jLeb(9968)</ScRiPt>",
        "text/plain",
        "application/*",
        "*/*",
    ],
)
def test_delete_forms_with_invalid_path_parameter_id_value_2(
    client: TestClient, test_db: Session, invalid_path_parameter_id_value: str
):
    add_mock_data_to_db(test_db)

    response = client.delete(f"{BASE_PATH_V1}{FORMS}/{invalid_path_parameter_id_value}")

    assert response.status_code == http_status.HTTP_404_NOT_FOUND
    res = response.json()
    assert len(res) == 1
    assert res["detail"] == error_messages.NOT_FOUND


# ---------------------------------------------
# Delete "/forms/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_path_parameter_id_value",
    [
        "..;/",
        "",
        "?id=1 AND 1::int=1",
        "?id=1 UNION ALL SELECT NULL,version(),NULL LIMIT 1 OFFSET 1--",
    ],
)
def test_delete_forms_with_invalid_path_parameter_id_value_3(
    client: TestClient, test_db: Session, invalid_path_parameter_id_value: str
):
    add_mock_data_to_db(test_db)

    response = client.delete(f"{BASE_PATH_V1}{FORMS}/{invalid_path_parameter_id_value}")

    # A 307 Temporary Redirect message is an HTTP response status code
    # indicating that the requested resource has been temporarily moved
    # to another URI, as indicated by the special Location header
    # returned within the response.
    assert response.status_code == http_status.HTTP_307_TEMPORARY_REDIRECT
    headers = response.headers
    assert len(headers) == 2
    assert headers["content-length"] == str(0)
    assert headers["Location"] is not None


# ---------------------------------------------
# Delete "/forms/{id}"
# Test delete an unshared form.
# ---------------------------------------------
def test_delete_unshared_form(client: TestClient, test_db: Session):
    mock = add_share_forms_mock_data_to_db(test_db)
    mock_form_details_id = mock.form6_org1_owner.id

    # check number of shares (1 expected)
    response = client.get(f"{BASE_PATH_V1}{FORMS}/{mock_form_details_id}")
    assert response.status_code == http_status.HTTP_200_OK
    form = response.json()
    assert isinstance(form["shares"], list)
    assert len(form["shares"]) == 1

    # delete form
    response_delete = client.delete(f"{BASE_PATH_V1}{FORMS}/{mock_form_details_id}")
    _assert_deleted(response_delete, mock_form_details_id)

    # check if form exist
    response_get = client.get(f"{BASE_PATH_V1}{FORMS}/{mock_form_details_id}")
    _assert_not_found(response_get, mock_form_details_id)


# ---------------------------------------------
# Delete "/forms/{id}"
# Test delete an form not in my organization.
# ---------------------------------------------
def test_delete_form_not_in_my_organization(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)
    mock_form_details_id = mock.form23.id

    # delete form
    response_delete = client.delete(f"{BASE_PATH_V1}{FORMS}/{mock_form_details_id}")
    _assert_forbidden(response_delete, mock_form_details_id)


# ---------------------------------------------
# Delete "/forms/{id}"
# Test delete an form not in my organization.
# ---------------------------------------------
def test_delete_form_not_in_my_organization_but_shared_with_my_organization(
    client: TestClient, test_db: Session
):
    mock = add_mock_data_to_db(test_db)
    mock_form_details_id = mock.form24.id

    # delete form
    response_delete = client.delete(f"{BASE_PATH_V1}{FORMS}/{mock_form_details_id}")
    _assert_conflict(response_delete, mock_form_details_id)
