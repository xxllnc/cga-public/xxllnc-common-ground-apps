import pytest
from app.api.api_v1.routers.tests import (
    ACTIVE_FORMS,
    CONTENT_TYPE_VALUE_APPLICATION_JSON,
    FORM_LIST_FIELDS,
    FORMS,
    HEADERS,
    ORG,
    SHARE,
    URLS,
)
from app.api.api_v1.routers.tests.asserts import assert_found, assert_method_not_allowed
from app.api.api_v1.routers.tests.forms.test_mock_data import add_mock_data_to_db
from app.api.api_v1.routers.tests.helper import empty_body
from app.api.api_v1.routers.tests.parameters import FORM_LIST, FORM_LIST_LEN
from app.core.config import BASE_PATH_V1
from fastapi import status as http_status
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session


# ---------------------------------------------
# Testing an API with GET requests.
# Test invalid start range with end range combinations.
# Throw ValueError
# ---------------------------------------------
def test_get_list_range_with_invalid_start_end_combination(client: TestClient):

    # test start with an empty end range
    response = client.get(f"{BASE_PATH_V1}{FORMS}?range=[3,0]")

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]["loc"]) == 1
    assert res["detail"][0]["loc"][0] == "end"
    assert res["detail"][0]["msg"] == "range end cannot be before start"
    assert res["detail"][0]["type"] == "assertion_error"

    # test empty start with an empty end range
    with pytest.raises(ValueError):
        client.get(f"{BASE_PATH_V1}{FORMS}?range=[,]")

    # test range end cannot be before start
    response = client.get(f"{BASE_PATH_V1}{FORMS}?range=[3,2]")

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]["loc"]) == 1
    assert res["detail"][0]["loc"][0] == "end"
    assert res["detail"][0]["msg"] == "range end cannot be before start"
    assert res["detail"][0]["type"] == "assertion_error"

    # test end value is greater than or equal to 0
    response = client.get(f"{BASE_PATH_V1}{FORMS}?range=[0,-1]")

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]["loc"]) == 1
    assert res["detail"][0]["loc"][0] == "end"
    assert res["detail"][0]["msg"] == "ensure this value is greater than or equal to 0"
    assert res["detail"][0]["type"] == "value_error.number.not_ge"

    # test end value is not a valid integer
    with pytest.raises(ValueError):
        client.get(f"{BASE_PATH_V1}{FORMS}?range=[0,aabbcc]")

    # test start and end value are not a valid integer
    with pytest.raises(ValueError):
        client.get(f"{BASE_PATH_V1}{FORMS}?range=[aa,bb]")

    # test page size cannot exceed 100
    response = client.get(
        f"{BASE_PATH_V1}{FORMS}?range=[1111111111111111111111111111111,1111111111111111111111111111111222222222222222222222222222222222222222222222]"  # noqa: E501
    )

    assert response.status_code == http_status.HTTP_422_UNPROCESSABLE_ENTITY
    res = response.json()
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]["loc"]) == 1
    assert res["detail"][0]["loc"][0] == "end"
    assert res["detail"][0]["msg"] == "page size cannot exceed 100"
    assert res["detail"][0]["type"] == "assertion_error"


def test_get_list(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    response = client.get(f"{BASE_PATH_V1}{FORMS}")

    assert response.status_code == http_status.HTTP_200_OK
    headers = response.headers
    assert len(headers) == 3
    assert headers["content-type"] == CONTENT_TYPE_VALUE_APPLICATION_JSON
    assert headers["content-range"] == f"results 0-{FORM_LIST_LEN-1}/{FORM_LIST_LEN}"

    res = response.json()
    assert len(res) == FORM_LIST_LEN
    for index, form in enumerate(res):
        assert len(form) == FORM_LIST_FIELDS
        assert_found(form, getattr(mock, f"form{FORM_LIST[index]}"))


# ---------------------------------------------
# Testing an API for a given HTTP method without path parameter id.
# Reject request with HTTP response code 405 Method not allowed.
# ---------------------------------------------
@pytest.mark.parametrize("request_body", [None, empty_body])
@pytest.mark.parametrize(
    "url, http_method, request_headers, status_code",
    [
        (ACTIVE_FORMS, "delete", HEADERS, None),
        (ACTIVE_FORMS, "patch", HEADERS, None),
        (ACTIVE_FORMS, "post", HEADERS, None),
        (ACTIVE_FORMS, "put", HEADERS, None),
        (FORMS, "delete", None, None),
        (FORMS, "patch", None, None),
        (FORMS, "post", None, http_status.HTTP_422_UNPROCESSABLE_ENTITY),
        (FORMS, "put", None, None),
        (ORG, "delete", HEADERS, None),
        (ORG, "patch", HEADERS, None),
        (ORG, "post", HEADERS, http_status.HTTP_422_UNPROCESSABLE_ENTITY),
        (ORG, "put", HEADERS, None),
        (SHARE, "delete", None, None),
        (SHARE, "patch", None, None),
        (SHARE, "post", None, http_status.HTTP_422_UNPROCESSABLE_ENTITY),
        (SHARE, "put", None, None),
        (URLS, "delete", None, None),
        (URLS, "patch", None, None),
        (URLS, "post", None, http_status.HTTP_422_UNPROCESSABLE_ENTITY),
        (URLS, "put", None, None),
    ],
)
def test_http_method_without_required_path_parameter(
    client: TestClient,
    url: str,
    http_method: str,
    request_body: None | dict,
    request_headers,
    status_code: None | int,
):
    response = client.request(
        method=http_method, url=f"{BASE_PATH_V1}{url}", json=request_body, headers=request_headers
    )
    if status_code:
        assert response.status_code == status_code
    else:
        assert_method_not_allowed(response, http_method)
