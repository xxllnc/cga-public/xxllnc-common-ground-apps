from app import domain, schemas
from app.core.auth0 import get_admin_scopes, get_current_user
from app.db.session import get_read_db, get_write_db
from fastapi import APIRouter, Depends
from fastapi import status as http_status
from sqlalchemy.orm import Session
from uuid import UUID

router = r = APIRouter()


@r.get(
    "/share/{id}",
    response_model=schemas.Form,
    responses={http_status.HTTP_404_NOT_FOUND: {"description": "Share not found"}},
    dependencies=[Depends(get_admin_scopes)],
)
def get_share_by_id(id: UUID, db: Session = Depends(get_read_db)):

    return domain.share_forms.get_share_by_id(db, id)


@r.post(
    "/share",
    response_model=schemas.Form,
    status_code=http_status.HTTP_200_OK,
    responses={
        http_status.HTTP_404_NOT_FOUND: {"description": "Share not found"},
        http_status.HTTP_409_CONFLICT: {"description": "Conflict. Share not added."},
    },
    dependencies=[Depends(get_admin_scopes)],
)
def share_for_organization(
    add_share_request: schemas.FormAddShareRequest,
    db: Session = Depends(get_write_db),
    current_user=Depends(get_current_user),
):
    """
    Use this interface to share a form with an other organisation
    """

    return domain.share_forms.add_new_share(db, add_share_request, current_user)


@r.put(
    "/share/{id}",
    response_model=schemas.Form,
    responses={
        http_status.HTTP_404_NOT_FOUND: {"description": "Share not found"},
        http_status.HTTP_409_CONFLICT: {"description": "Conflict. Share not updated."},
    },
    dependencies=[Depends(get_admin_scopes)],
)
def update_share_for_organization(
    id: UUID,
    update_request: schemas.FormUpdateShareRequest,
    db=Depends(get_write_db),
    current_user=Depends(get_current_user),
):

    return domain.share_forms.update_share(db, id, update_request, current_user)


@r.delete(
    "/share/{id}",
    response_model=schemas.Form,
    responses={
        http_status.HTTP_404_NOT_FOUND: {"description": "share not found"},
        http_status.HTTP_409_CONFLICT: {"description": "Conflict. share not deleted."},
    },
    dependencies=[Depends(get_admin_scopes)],
)
def delete_share(id: UUID, db=Depends(get_write_db), current_user=Depends(get_current_user)):

    return domain.share_forms.delete_share(db, id, current_user)
