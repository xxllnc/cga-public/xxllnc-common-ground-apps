from app import domain, schemas
from app.core.auth0 import AccessUser, get_admin_scopes, get_current_user
from app.db.session import get_read_db, get_write_db
from app.resources import error_messages
from exxellence_shared_cga.core.rest_helpers import SearchParameters
from fastapi import APIRouter, Depends, Response
from fastapi import status as http_status
from sqlalchemy.orm import Session
from typing import List, Union
from uuid import UUID

router = r = APIRouter()


# validate the filter and sort parameters
def validate_search_parameters():
    return SearchParameters(
        filter_options_schema=schemas.FormFilterableFields,
        sort_options_schema=schemas.FormSortableFields,
    )


@r.get(
    "/forms/{id}",
    response_model=schemas.FormGetOneResponseDetails,
    responses={http_status.HTTP_404_NOT_FOUND: {"description": "Form not found"}},
    dependencies=[Depends(get_admin_scopes)],
)
def get_form_by_id(
    id: Union[UUID, str],
    database: Session = Depends(get_read_db),
    current_user: AccessUser = Depends(get_current_user),
):
    """Returns a single form, given its uuid or slug
    This method does not filter on active status or publish date.
    """

    form = domain.form.get_form(
        database=database,
        id=id,
        organization_uuid=current_user.organization_uuid,
    )

    return form


@r.get(
    "/forms",
    response_model=List[schemas.FormWithDetails],
    dependencies=[Depends(get_admin_scopes)],
)
def get_form_list(
    response: Response,
    database: Session = Depends(get_read_db),
    search_parameters: dict = Depends(validate_search_parameters()),
    current_user: AccessUser = Depends(get_current_user),
):
    """Returns a list of forms"""

    search_result = domain.form.get_forms_list(
        db=database,
        organization_uuid=current_user.organization_uuid,
        **search_parameters,
    )

    response.headers["Content-Range"] = search_result.as_content_range("results")

    return search_result.result


@r.post(
    "/forms",
    response_model=schemas.FormWithDetails,
    status_code=http_status.HTTP_201_CREATED,
    responses={
        http_status.HTTP_409_CONFLICT: {"description": "Conflict. Form not created."},
        http_status.HTTP_422_UNPROCESSABLE_ENTITY: {
            "description": "Unprocessable Entity. Form not created"
        },
    },
    dependencies=[Depends(get_admin_scopes)],
)
def create_form(
    form_create_request: schemas.FormCreateRequest,
    current_user: AccessUser = Depends(get_current_user),
    database: Session = Depends(get_write_db),
):

    form = domain.form.create_form(database, form_create_request, current_user)

    return form


@r.put(
    "/forms/{id}",
    response_model=schemas.FormGetOneResponseDetails,
    responses={
        http_status.HTTP_404_NOT_FOUND: {"description": "Form not found"},
        http_status.HTTP_409_CONFLICT: {"description": "Conflict. Form not updated."},
    },
    dependencies=[Depends(get_admin_scopes)],
)
def update_form(
    id: Union[UUID, str],
    form_update_request: schemas.FormUpdateRequest,
    current_user: AccessUser = Depends(get_current_user),
    database: Session = Depends(get_write_db),
):
    """Update a form, given its unique id or slug"""

    # TODO CGA-1377
    # * see create_form -> implement formio check
    # * check only valid when status is set to 'ACTIVE'?

    updated_form = domain.form.update_form(
        database,
        id,
        form_update_request,
        current_user,
    )
    form = domain.form.get_form(
        database=database,
        id=updated_form.id,
        organization_uuid=current_user.organization_uuid,
    )

    return form


@r.delete(
    "/forms/{id}",
    responses={
        http_status.HTTP_404_NOT_FOUND: {"description": "Form not found"},
        http_status.HTTP_409_CONFLICT: {"description": "Conflict. Form not deleted."},
        http_status.HTTP_403_FORBIDDEN: {"description": "Action not permitted."},
    },
    dependencies=[Depends(get_admin_scopes)],
)
def delete_form(
    id: Union[UUID, str],
    database: Session = Depends(get_write_db),
    current_user: AccessUser = Depends(get_current_user),
):

    domain.form.delete_form(database, id, current_user)
    return {"message": error_messages.FORM_IS_DELETED.format(id)}
