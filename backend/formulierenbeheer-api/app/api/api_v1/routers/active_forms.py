"""This module contains all the crud api calls for a form"""
import uuid
from app import domain, schemas
from app.db.session import get_read_db
from exxellence_shared_cga.core.rest_helpers import SearchParameters
from fastapi import APIRouter, Depends, Header, Response
from fastapi import status as http_status
from sqlalchemy.orm import Session
from typing import List, Union

router = r = APIRouter()


# validate the filter and sort parameters
def validate_search_parameters():
    return SearchParameters(
        filter_options_schema=schemas.ActiveFormFilterableFields,
        sort_options_schema=schemas.ActiveFormSortableFields,
    )


@r.get(
    "/active/forms/{id}",
    response_model=schemas.FormWithDetails,
    responses={http_status.HTTP_404_NOT_FOUND: {"description": "Form not found"}},
)
def get_active_form_by_id(
    id: Union[uuid.UUID, str],
    database: Session = Depends(get_read_db),
    organization_url: str = Header(...),
):
    """Returns a single form, given its unique id or slug.
    Used for calls from citizens without authentication.
    This method filters on active and the publish date.
    """

    form = domain.form.get_form(
        database=database,
        id=id,
        get_active_forms_on_org_url=organization_url,
    )
    return form


@r.get("/active/forms", response_model=List[schemas.ActiveFormFields])
def get_list_of_active_forms(
    response: Response,
    organization_url: str = Header(...),
    db: Session = Depends(get_read_db),
    search_parameters: dict = Depends(validate_search_parameters()),
):

    search_result = domain.form.get_forms_list(
        db=db,
        get_active_forms_on_org_url=organization_url,
        **search_parameters,
    )

    response.headers["Content-Range"] = search_result.as_content_range("results")

    return search_result.result
