import uuid
from app import domain, schemas
from app.core.auth0 import get_admin_scopes
from app.db.session import get_read_db, get_write_db
from exxellence_shared_cga.core.rest_helpers import SearchParameters
from exxellence_shared_cga.core.types import GenericId, IdType
from fastapi import APIRouter, Depends, Response
from fastapi import status as http_status
from sqlalchemy.orm import Session
from typing import List, Union

router = r = APIRouter()


def validate_search_parameters():
    return SearchParameters(
        filter_options_schema=schemas.UrlFilterableFields,
        sort_options_schema=schemas.UrlSortableFields,
    )


@r.get(
    "/urls/{id}",
    response_model=schemas.Url,
    responses={404: {"description": "url not found"}},
    dependencies=[Depends(get_admin_scopes)],
)
def get_url_by_id(id: Union[int, uuid.UUID], db=Depends(get_read_db)):

    id_type = IdType.uuid if isinstance(id, uuid.UUID) else IdType.sid
    return domain.urls.get(db=db, id=GenericId(id=id, type=id_type))


@r.get("/urls", response_model=List[schemas.Url], dependencies=[Depends(get_admin_scopes)])
def get_urls(
    response: Response,
    database: Session = Depends(get_read_db),
    search_parameters: dict = Depends(validate_search_parameters()),
):

    search_result = domain.urls.get_multi(db=database, query=None, **search_parameters)
    response.headers["Content-Range"] = search_result.as_content_range("results")

    return search_result.result


@r.put(
    "/urls/{id}",
    response_model=schemas.UrlUpdate,
    responses={
        http_status.HTTP_404_NOT_FOUND: {"description": "Url not found"},
        http_status.HTTP_409_CONFLICT: {"description": "Conflict. Url not updated."},
    },
    dependencies=[Depends(get_admin_scopes)],
)
def update_url(
    id: Union[int, uuid.UUID],
    url_update_request: schemas.UrlUpdate,
    database: Session = Depends(get_write_db),
):

    id_type = IdType.uuid if isinstance(id, uuid.UUID) else IdType.sid

    updated_url = domain.urls.update(
        db=database, id=GenericId(id=id, type=id_type), obj_in=url_update_request
    )
    return {**updated_url.__dict__}


@r.post(
    "/urls",
    response_model=schemas.UrlCreate,
    responses={
        http_status.HTTP_404_NOT_FOUND: {"description": "Url not found"},
        http_status.HTTP_409_CONFLICT: {"description": "Conflict. Url not updated."},
    },
    dependencies=[Depends(get_admin_scopes)],
)
def add_url(url_add_request: schemas.UrlCreateRequest, database: Session = Depends(get_write_db)):

    added_url = domain.urls.create_url(db=database, url_add_request=url_add_request)

    return {**added_url.__dict__}
