"""Module for the generic schema"""

import uuid as py_uuid
from pydantic import BaseModel
from typing import Optional


class FilterableId(BaseModel):
    """FilterableId"""

    id: Optional[py_uuid.UUID]
