from .url_schemas import Url
from app.schemas.to_optional import to_optional
from exxellence_shared_cga.core.types import SortOrder
from pydantic import BaseModel, Extra, Field
from typing import Optional
from uuid import UUID


class OrganizationId(BaseModel):
    id: UUID = Field(..., title="The unique id of the organization")


class OrganizationBase(OrganizationId):

    name: str = Field(..., title="The name of the organization")

    class Config:
        orm_mode = True


class OrganizationCreate(OrganizationBase):
    class Config:
        extra = Extra.ignore


class OrganizationUpdate(to_optional(OrganizationCreate)):  # type: ignore

    pass


class OrganizationReturn(OrganizationBase):
    urls: Optional[list[Url]] = Field(..., title="The urls of the organization")

    class Config:
        orm_mode = True


class OrgFilterableFields(BaseModel):

    name: Optional[str]
    url: Optional[str]

    class Config:
        extra = "forbid"


class OrgSortableFields(BaseModel):

    name: Optional[SortOrder]

    class Config:
        extra = "forbid"
