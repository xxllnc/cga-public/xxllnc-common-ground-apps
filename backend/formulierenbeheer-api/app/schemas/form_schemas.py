from __future__ import annotations

import uuid
from app.db.models import Rights, Status
from app.schemas.generic_schemas import FilterableId
from app.schemas.to_optional import to_optional
from datetime import date
from exxellence_shared_cga.core.types import SortOrder
from pydantic import BaseModel, Field, validator
from typing import Optional


class _Id(BaseModel):
    id: uuid.UUID = Field(..., title="The unique uuid of the form instance")


class _Name(BaseModel):
    name: str = Field(..., min_length=1, max_length=255, title="The name of the form")


class _Slug(BaseModel):
    slug: str = Field(..., min_length=1, max_length=255, title="The slug of the form")


class _FormDetails(BaseModel):
    form: dict = Field(..., title="The form contents containing the pages")
    private: Optional[bool] = Field(True, title="Indicates if form is public or private")
    author: str = Field(..., title="The original author of the form")


class _FormDetailsNoAuthor(BaseModel):
    form: dict = Field(..., title="The form contents containing the pages")
    private: Optional[bool] = Field(True, title="Indicates if form is public or private")


class _Config(BaseModel):
    config: list[dict] | None = Field([], title="The config of the form")

    def __init__(self, config: list | None = [], **data) -> None:
        config_value = [] if config is None else config
        super().__init__(config=config_value, **data)


class _Organization(BaseModel):
    organization_uuid: uuid.UUID = Field(..., title="The unique uuid of the organization")
    organization_name: str = Field(..., title="The name of the organization")


class _FormBase(_Name):
    status: Status = Field(..., title="The status of the form")
    rights: Optional[Rights] = Field(Rights.VIEWER, title="The form of the form")
    publish_date: Optional[date] = Field(None, title="The publish date of the form")

    @validator("publish_date", pre=True, always=False)
    def validate_publish_date(cls, val):
        """Validates the publish date"""
        if val == "":
            return None
        return val

    class Config:
        allow_population_by_field_name = True
        fields = {
            "publish_date": "publishDate",
            "organization_uuid": "organizationUuid",
            "organization_name": "organizationName",
        }


# TODO: Remove config and form from list and add it to FormDetails CGA-1828
class Form(_Id, _FormBase, _Slug, _Organization, _Config):
    """
    Shows the details of a Form, is used in the response of get, put and post
    """

    class Config:
        orm_mode = True


class FormWithDetails(Form, _FormDetails):
    """
    Also add the form and public element
    """

    pass


class FormGetOneResponseDetails(_Id, _Name, _Slug, _FormDetails):

    shares: list[Form] = Field(None, title="All the organisations the form is shared with")

    class Config:
        orm_mode = True


class ActiveFormFields(_Id, _Name, _Slug):
    class Config:
        orm_mode = True


class FormCreateRequest(_FormBase, _FormDetailsNoAuthor, _Config):
    """class to validate request input to create form"""

    pass


class _FormUpdateHelper(to_optional(_FormBase), _Slug, _FormDetails, _Config):  # type: ignore # noqa: E501
    """
    added this class because there is a strange error when make
    FormCreateRequest optional
    """

    pass


class FormUpdateRequest(to_optional(_FormUpdateHelper)):  # type: ignore
    """Make fields optional to support partial update (patch)"""

    pass


class FormAddShareRequest(
    to_optional(_FormBase),  # type: ignore
    to_optional(_Slug),  # type: ignore
    _Id,
    _Organization,
    _Config,
):
    pass


class FormUpdateShareRequest(
    to_optional(_FormBase),  # type: ignore
    to_optional(_Slug),  # type: ignore
    _Config,
):
    class Config:
        orm_mode = True


class ActiveFormFilterableFields(FilterableId):
    """Class for the active fields that are filterable"""

    q: Optional[str]

    class Config:
        extra = "forbid"


class ActiveFormSortableFields(BaseModel):
    """Class for the active fields that are sortable"""

    id: Optional[SortOrder]
    name: Optional[SortOrder]
    slug: Optional[SortOrder]

    class Config:
        allow_population_by_field_name = True
        extra = "forbid"


class FormFilterableFields(ActiveFormFilterableFields):
    """Class for the fields that are filterable"""

    status: Optional[str]
    publish_date_gte: Optional[str] = Field(alias="publishedAt_gte")
    publish_date_lte: Optional[str] = Field(alias="publishedAt_lte")
    private: Optional[bool]

    class Config:
        extra = "forbid"


class FormSortableFields(ActiveFormSortableFields):
    """Class for the fields that are sortable"""

    status: Optional[SortOrder]
    publish_date: Optional[SortOrder] = Field(alias="publishDate")
    author: Optional[str]

    class Config:
        allow_population_by_field_name = True
        extra = "forbid"
