import uuid as py_uuid
from app.schemas.to_optional import to_optional
from exxellence_shared_cga.core.types import SortOrder
from pydantic import BaseModel, Field
from typing import Optional


class UrlOrganizationId(BaseModel):
    organization_id: int = Field(..., title="the id of the organization", alias="organizationId")


class UrlBase(BaseModel):
    url: str = Field(..., title="An url of the organization")
    primary: bool = Field(..., title="if this is the primary url")

    class Config:
        allow_population_by_field_name = True


class Url(UrlBase, UrlOrganizationId):
    sid: int = Field(..., title="The unique id of the url", alias="id")
    uuid: py_uuid.UUID = Field(..., title="The unique uuid of the url")

    class Config:
        orm_mode = True
        allow_population_by_field_name = True
        extra = "allow"


class UrlCreateRequest(UrlBase):
    organization_id: py_uuid.UUID = Field(
        ..., title="the id of the organization", alias="organizationId"
    )


class UrlCreate(UrlBase, UrlOrganizationId):

    pass


class UrlUpdate(to_optional(UrlCreate)):  # type: ignore

    pass


class UrlFilterableFields(BaseModel):

    name: Optional[str]
    url: Optional[str]
    primary: Optional[str]
    organization_id: Optional[str]

    class Config:
        extra = "forbid"


class UrlSortableFields(BaseModel):

    name: Optional[SortOrder]
    url: Optional[SortOrder]

    class Config:
        extra = "forbid"
