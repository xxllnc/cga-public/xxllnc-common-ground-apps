from typing import Final

"""Module for the API error messages"""

# Common message
BAD_REQUEST: Final = "Bad Request"
CONFLICT: Final = "Conflict"
NOT_FOUND: Final = "Not Found"
FORBIDDEN: Final = "Forbidden"
INTERNAL_SERVER_ERROR: Final = "Internal Server Error"
METHOD_NOT_ALLOWED: Final = "Method Not Allowed"

FIELD_REQUIRED: Final[str] = "field required"
VALUE_ERROR_MISSING: Final[str] = "value_error.missing"
VALUE_IS_NOT_A_VALID_LIST: Final[str] = "value is not a valid list"

# Form messages
FORM_NOT_FOUND_WITH: Final = "Er is geen formulier gevonden met {}"
FORM_COMBINATION_NAME_CREATOR_ORGANIZATION_UUID_ALREADY_EXIST_ERROR: Final = "Voor deze organisatie bestaat er al een formulier met deze naam. Geef het formulier een unieke naam voor deze organisatie."  # noqa: E501
FORM_COMBINATION_SLUG_CREATOR_ORGANIZATION_UUID_ALREADY_EXIST_ERROR: Final = "Voor deze organisatie bestaat er al een formulier met deze verkorte naam. Geef het formulier een unieke verkorte naam voor deze organisatie."  # noqa: E501
FORM_NOT_CREATED: Final = "Formulier is niet aangemaakt. Er ging iets mis."
FORM_NOT_UPDATED: Final = "Formulier is niet gewijzigd. Er ging iets mis."
FORM_NOT_DELETED: Final = "Formulier is niet verwijderd. Er ging iets mis."
FORM_NOT_SHARED: Final = (
    "Formulier kan niet gedeeld worden. Er kan geen formulier gevonden worden met {}"
)
FORM_IS_DELETED: Final = "Formulier met id: {} is verwijderd."
ORGANIZATION_NOT_FOUND_WITH: Final = "Er is geen organisatie gevonden met {}"
INVALID_INPUT: Final = (
    "{} heeft geen waarde, dit is een verplicht veld en moet een waarde bevatten"
)
NOT_YOUR_FORM: Final = "U heeft onvoldoende rechten om formulier met id: {} te verwijderen"
INPUT_NOT_ALLOWED: Final = "{} heeft een waarde die niet wordt geaccepteerd"
