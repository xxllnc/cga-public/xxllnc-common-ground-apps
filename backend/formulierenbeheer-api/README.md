# formulierenbeheer-api

This api can be used to make, edit, add and delete new forms.
To use this api it is required to provide a valid Auth0 jwt-token

The api can be started using docker-compose as described in the readme.md
in the root of this repository
