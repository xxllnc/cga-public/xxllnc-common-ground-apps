import alembic.command
import logging
import pytest
from alembic.config import Config as AlembicConfig
from app.api.api_v1.routers.tests.helper import org1
from app.core import config
from app.core.auth0 import AccessUser, get_admin_scopes, get_current_user, get_user_scopes
from app.db.session import get_read_db, get_write_db
from app.main import app
from fastapi.testclient import TestClient
from sqlalchemy import create_engine, event, text
from sqlalchemy.engine.url import make_url
from sqlalchemy.orm import sessionmaker


def get_test_db_url() -> str:
    return f"{config.SQLALCHEMY_DATABASE_WRITE_URI}_test"


# create_database, drop_database, database_exists can be imported from
# sqlalchemy_utils once it supports SQLAlchemy 1.4
def _root_database_engine(url):
    pg_url = url.set(database="postgres")

    engine = create_engine(pg_url, isolation_level="AUTOCOMMIT")
    return engine


def create_database(database_url):
    url = make_url(database_url)

    with _root_database_engine(url).begin() as connection:
        connection.execute(text(f"CREATE DATABASE {url.database}"))


def drop_database(database_url):
    url = make_url(database_url)

    with _root_database_engine(url).begin() as connection:
        connection.execute(
            text(
                f"""
            SELECT pg_terminate_backend(pg_stat_activity.pid)
            FROM pg_stat_activity
            WHERE pg_stat_activity.datname = '{url.database}'
                AND pid <> pg_backend_pid();
            """
            )
        )
        connection.execute(text(f"DROP DATABASE {url.database}"))


def database_exists(database_url):
    url = make_url(database_url)

    with _root_database_engine(url).begin() as connection:
        result = connection.execute(
            text(f"SELECT 1 FROM pg_database WHERE datname='{url.database}'")
        )
        return bool(result.scalar())


@pytest.fixture
def test_db():
    """
    Modify the database session to automatically roll back after each test.
    This is to avoid tests affecting the database state of other tests.
    """
    # Connect to the test database
    engine = create_engine(get_test_db_url(), future=True)
    connection = engine.connect()

    # Run a parent transaction that can roll back all changes
    transaction = connection.begin()
    session = sessionmaker()(bind=connection)

    session.begin_nested()  # SAVEPOINT

    @event.listens_for(session, "after_transaction_end")
    def restart_savepoint(session, transaction):
        if transaction.nested and not transaction._parent.nested:
            session.begin_nested()

    yield session

    session.close()
    transaction.rollback()  # roll back to the SAVEPOINT
    connection.close()


@pytest.fixture(scope="session", autouse=True)
def create_test_db():
    """
    Create a test database and use it for the whole test session.
    """

    test_db_url = get_test_db_url()

    # Create the test database
    assert not database_exists(test_db_url), "Test database already exists. Aborting tests."

    create_database(test_db_url)

    alembic_logger = logging.getLogger("alembic.runtime.migration")
    alembic_logger.setLevel(logging.DEBUG)

    alembic_config = AlembicConfig("alembic.ini")
    alembic_config.set_main_option("sqlalchemy.url", test_db_url)
    alembic.command.upgrade(alembic_config, "head")

    # Run the tests
    yield

    # Drop the test database
    drop_database(test_db_url)


auth_token = {
    "sub": "94a3f2ca-350d-4456-aeb0-8fb6093c8810",
    "https://xxllnc.nl/name": "Test User",
    "scope": "openid profile email cga.admin",
    "org_id": "org_test",
    "https://xxllnc.nl/organization_uuid": org1["id"],
    "https://xxllnc.nl/organization_name": org1["name"],
}


@pytest.fixture(autouse=False)
def test_user() -> AccessUser:
    return override_get_current_user()


def override_get_current_user() -> AccessUser:
    return AccessUser.parse_obj(auth_token)


def override_get_scopes():
    return auth_token


@pytest.fixture()
def client(test_db):
    """
    Get a TestClient instance that reads/write to the test database
    """

    def get_test_db():
        yield test_db

    # Override the dependency and use the custom
    app.dependency_overrides[get_read_db] = get_test_db
    app.dependency_overrides[get_write_db] = get_test_db
    app.dependency_overrides[get_current_user] = override_get_current_user
    app.dependency_overrides[get_user_scopes] = override_get_scopes
    app.dependency_overrides[get_admin_scopes] = override_get_scopes

    yield TestClient(app)


@pytest.fixture(autouse=True)
def turn_off_audit_logging_during_tests():
    config.AUDIT_LOGGING = False
