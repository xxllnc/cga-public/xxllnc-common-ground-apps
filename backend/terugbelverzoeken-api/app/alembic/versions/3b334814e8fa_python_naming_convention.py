""" python naming convention

Revision ID: 3b334814e8fa
Revises: 1d4dd7b37325
Create Date: 2022-01-27 18:12:52.160530+01:00

"""
import sqlalchemy as sa
from alembic import op
from alembic_utils.pg_extension import PGExtension
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "3b334814e8fa"
down_revision = "1d4dd7b37325"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###

    op.rename_table("CustomField", "custom_field")
    op.drop_index("ix_CustomField_name", table_name="custom_field")
    op.drop_index("ix_CustomField_sid", table_name="custom_field")
    op.drop_index("ix_CustomField_sort_order", table_name="custom_field")

    op.create_index(op.f("ix_custom_field_name"), "custom_field", ["name"], unique=True)
    op.create_index(op.f("ix_custom_field_sid"), "custom_field", ["sid"], unique=False)
    op.create_index(
        op.f("ix_custom_field_sort_order"),
        "custom_field",
        ["sort_order"],
        unique=False,
    )
    # droped here as it depends on the CustomField_pkey
    op.drop_constraint("CustomFieldValues_custom_field_id_fkey", "CustomFieldValues")
    op.drop_constraint("CustomField_pkey", "custom_field")

    op.create_primary_key("custom_field_pkey", "custom_field", ["sid"])
    op.drop_constraint("CustomField_uuid_key", "custom_field")
    op.create_unique_constraint("custom_field_uuid_key", "custom_field", ["uuid"])

    op.rename_table("CustomFieldValues", "custom_field_values")
    op.drop_index(
        "ix_CustomFieldValues_callback_request_id",
        table_name="custom_field_values",
    )
    op.drop_index("ix_CustomFieldValues_sid", table_name="custom_field_values")

    op.create_index(
        op.f("ix_custom_field_values_callback_request_id"),
        "custom_field_values",
        ["callback_request_id"],
        unique=False,
    )
    op.create_index(
        op.f("ix_custom_field_values_sid"),
        "custom_field_values",
        ["sid"],
        unique=False,
    )

    op.drop_constraint("CustomFieldValues_pkey", "custom_field_values")
    op.create_primary_key("custom_field_values_pkey", "custom_field_values", ["sid"])
    op.drop_constraint("CustomFieldValues_callback_request_id_fkey", "custom_field_values")

    op.rename_table("CallbackRequest", "callback_request")

    op.drop_index("ix_CallbackRequest_contact", table_name="callback_request")
    op.drop_index("ix_CallbackRequest_id", table_name="callback_request")
    op.drop_index("ix_CallbackRequest_sid", table_name="callback_request")

    op.create_index(
        op.f("ix_callback_request_contact"),
        "callback_request",
        ["contact"],
        unique=False,
    )
    op.create_index(
        op.f("ix_callback_request_sid"),
        "callback_request",
        ["sid"],
        unique=False,
    )
    op.create_index(
        op.f("ix_callback_request_id"),
        "callback_request",
        ["id"],
        unique=True,
    )
    op.drop_constraint("CallbackRequest_pkey", "callback_request")
    op.create_primary_key("callback_request_pkey", "callback_request", ["sid"])
    op.drop_constraint("CallbackRequest_uuid_key", "callback_request")
    op.create_unique_constraint("callback_request_uuid_key", "callback_request", ["uuid"])

    op.drop_constraint("CallbackRequest_result_id_fkey", "callback_request")

    op.rename_table("Employee", "employee")

    op.drop_index("ix_Employee_email", table_name="employee")
    op.drop_index("ix_Employee_name", table_name="employee")
    op.drop_index("ix_Employee_sid", table_name="employee")

    op.create_index(
        op.f("ix_employee_email"),
        "employee",
        ["email"],
        unique=True,
    )
    op.create_index(
        op.f("ix_employee_name"),
        "employee",
        ["name"],
        unique=True,
    )
    op.create_index(
        op.f("ix_employee_sid"),
        "employee",
        ["sid"],
        unique=False,
    )
    op.drop_constraint("Employee_pkey", "employee")
    op.create_primary_key("employee_pkey", "employee", ["sid"])
    op.drop_constraint("Employee_uuid_key", "employee")
    op.create_unique_constraint("employee_uuid_key", "employee", ["uuid"])

    op.rename_table("Result", "result")

    op.drop_index("ix_Result_result", table_name="result")
    op.drop_index("ix_Result_sid", table_name="result")

    op.create_index(
        op.f("ix_result_result"),
        "result",
        ["result"],
        unique=True,
    )

    op.create_index(
        op.f("ix_result_sid"),
        "result",
        ["sid"],
        unique=False,
    )
    op.drop_constraint("Result_pkey", "result")
    op.create_primary_key("result_pkey", "result", ["sid"])
    op.drop_constraint("Result_uuid_key", "result")
    op.create_unique_constraint("result_uuid_key", "result", ["uuid"])

    # Create foreign keys last as both tables need to be renamed first
    op.create_foreign_key(
        "custom_field_values_callback_request_id_fkey",
        "custom_field_values",
        "callback_request",
        ["callback_request_id"],
        ["sid"],
    )

    op.create_foreign_key(
        "custom_field_values_custom_field_id_fkey",
        "custom_field_values",
        "custom_field",
        ["custom_field_id"],
        ["sid"],
    )
    op.create_foreign_key(
        "callback_request_result_id_fkey",
        "callback_request",
        "result",
        ["result_id"],
        ["sid"],
    )

    op.execute('ALTER SEQUENCE "Employee_sid_seq" RENAME TO "employee_sid_seq"')
    op.execute('ALTER SEQUENCE "Result_sid_seq" RENAME TO "result_sid_seq"')
    op.execute('ALTER SEQUENCE "CustomField_sid_seq" RENAME TO "custom_field_sid_seq"')
    op.execute(
        'ALTER SEQUENCE "CustomFieldValues_sid_seq" RENAME TO "custom_field_values_sid_seq"'  # noqa: E501
    )
    op.execute(
        'ALTER SEQUENCE "CallbackRequest_sid_seq" RENAME TO "callback_request_sid_seq"'  # noqa: E501
    )
    op.execute(
        'ALTER SEQUENCE "CallbackRequest_id_seq" RENAME TO "callback_request_id_seq"'  # noqa: E501
    )

    op.execute(
        """
    CREATE OR REPLACE FUNCTION callback_request_generate_id()
    RETURNS character varying
    LANGUAGE plpgsql
    AS $function$
                DECLARE
                v_result      varchar := 'TBV-';
                value_numeric numeric;
                begin
                    SELECT nextval('"callback_request_id_seq"') INTO value_numeric;\n IF value_numeric > 9999 THEN \n v_result := v_result || value_numeric;\n ELSE \n v_result := v_result || to_char(value_numeric, 'FM0000'); \n  END IF;\n return v_result;\n END;\n $function$\n; """  # noqa: E501
    )
    op.execute(
        "ALTER TABLE callback_request ALTER COLUMN id SET DEFAULT callback_request_generate_id();"  # noqa: E501
    )
    op.execute("DROP FUNCTION callbackrequest_generate_id")

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    public_uuid_ossp = PGExtension(schema="public", signature="uuid-ossp")
    op.create_entity(public_uuid_ossp)

    op.create_table(
        "Result",
        sa.Column(
            "sid",
            sa.INTEGER(),
            server_default=sa.text("nextval('\"Result_sid_seq\"'::regclass)"),
            autoincrement=True,
            nullable=False,
        ),
        sa.Column(
            "uuid",
            postgresql.UUID(),
            server_default=sa.text("gen_random_uuid()"),
            autoincrement=False,
            nullable=False,
        ),
        sa.Column("result", sa.VARCHAR(), autoincrement=False, nullable=True),
        sa.Column("default", sa.BOOLEAN(), autoincrement=False, nullable=True),
        sa.PrimaryKeyConstraint("sid", name="Result_pkey"),
        sa.UniqueConstraint("uuid", name="Result_uuid_key"),
        postgresql_ignore_search_path=False,
    )
    op.create_index("ix_Result_sid", "Result", ["sid"], unique=False)
    op.create_index("ix_Result_result", "Result", ["result"], unique=False)
    op.create_table(
        "CustomFieldValues",
        sa.Column(
            "sid",
            sa.INTEGER(),
            server_default=sa.text("nextval('\"CustomFieldValues_sid_seq\"'::regclass)"),
            autoincrement=True,
            nullable=False,
        ),
        sa.Column(
            "callback_request_id",
            sa.INTEGER(),
            autoincrement=False,
            nullable=True,
        ),
        sa.Column(
            "custom_field_id",
            sa.INTEGER(),
            autoincrement=False,
            nullable=False,
        ),
        sa.Column("value", sa.VARCHAR(), autoincrement=False, nullable=True),
        sa.ForeignKeyConstraint(
            ["callback_request_id"],
            ["CallbackRequest.sid"],
            name="CustomFieldValues_callback_request_id_fkey",
        ),
        sa.ForeignKeyConstraint(
            ["custom_field_id"],
            ["CustomField.sid"],
            name="CustomFieldValues_custom_field_id_fkey",
        ),
        sa.PrimaryKeyConstraint("sid", name="CustomFieldValues_pkey"),
    )
    op.create_index("ix_CustomFieldValues_sid", "CustomFieldValues", ["sid"], unique=False)
    op.create_index(
        "ix_CustomFieldValues_callback_request_id",
        "CustomFieldValues",
        ["callback_request_id"],
        unique=False,
    )
    op.create_table(
        "CustomField",
        sa.Column(
            "sid",
            sa.INTEGER(),
            server_default=sa.text("nextval('\"CustomField_sid_seq\"'::regclass)"),
            autoincrement=True,
            nullable=False,
        ),
        sa.Column(
            "uuid",
            postgresql.UUID(),
            server_default=sa.text("gen_random_uuid()"),
            autoincrement=False,
            nullable=False,
        ),
        sa.Column("name", sa.VARCHAR(), autoincrement=False, nullable=True),
        sa.Column("description", sa.VARCHAR(), autoincrement=False, nullable=True),
        sa.Column("required", sa.BOOLEAN(), autoincrement=False, nullable=True),
        sa.Column(
            "type",
            postgresql.ENUM("string", "text", "select", name="customfieldtypes"),
            autoincrement=False,
            nullable=False,
        ),
        sa.Column("sort_order", sa.INTEGER(), autoincrement=False, nullable=False),
        sa.Column(
            "options",
            postgresql.JSON(astext_type=sa.Text()),
            autoincrement=False,
            nullable=True,
        ),
        sa.PrimaryKeyConstraint("sid", name="CustomField_pkey"),
        sa.UniqueConstraint("uuid", name="CustomField_uuid_key"),
    )
    op.create_index(
        "ix_CustomField_sort_order",
        "CustomField",
        ["sort_order"],
        unique=False,
    )
    op.create_index("ix_CustomField_sid", "CustomField", ["sid"], unique=False)
    op.create_index("ix_CustomField_name", "CustomField", ["name"], unique=False)
    op.create_table(
        "CallbackRequest",
        sa.Column(
            "sid",
            sa.INTEGER(),
            server_default=sa.text("nextval('\"CallbackRequest_sid_seq\"'::regclass)"),
            autoincrement=True,
            nullable=False,
        ),
        sa.Column(
            "id",
            sa.VARCHAR(),
            server_default=sa.text("callbackrequest_generate_id()"),
            autoincrement=False,
            nullable=False,
        ),
        sa.Column(
            "uuid",
            postgresql.UUID(),
            server_default=sa.text("gen_random_uuid()"),
            autoincrement=False,
            nullable=False,
        ),
        sa.Column("contact", sa.VARCHAR(), autoincrement=False, nullable=True),
        sa.Column("subject", sa.VARCHAR(), autoincrement=False, nullable=True),
        sa.Column("summary", sa.VARCHAR(), autoincrement=False, nullable=True),
        sa.Column("phone", sa.VARCHAR(), autoincrement=False, nullable=True),
        sa.Column(
            "status",
            postgresql.ENUM("Open", "Afgehandeld", name="statusses"),
            autoincrement=False,
            nullable=False,
        ),
        sa.Column("result_id", sa.INTEGER(), autoincrement=False, nullable=True),
        sa.Column(
            "created_at",
            postgresql.TIMESTAMP(),
            autoincrement=False,
            nullable=True,
        ),
        sa.Column(
            "updated_at",
            postgresql.TIMESTAMP(),
            autoincrement=False,
            nullable=True,
        ),
        sa.Column("created_by_id", sa.VARCHAR(), autoincrement=False, nullable=True),
        sa.Column("created_by_name", sa.VARCHAR(), autoincrement=False, nullable=True),
        sa.Column("assigned_to_id", sa.VARCHAR(), autoincrement=False, nullable=True),
        sa.Column(
            "assigned_to_name",
            sa.VARCHAR(),
            autoincrement=False,
            nullable=True,
        ),
        sa.ForeignKeyConstraint(
            ["result_id"],
            ["Result.sid"],
            name="CallbackRequest_result_id_fkey",
        ),
        sa.PrimaryKeyConstraint("sid", name="CallbackRequest_pkey"),
        sa.UniqueConstraint("uuid", name="CallbackRequest_uuid_key"),
    )
    op.create_index("ix_CallbackRequest_sid", "CallbackRequest", ["sid"], unique=False)
    op.create_index("ix_CallbackRequest_id", "CallbackRequest", ["id"], unique=False)
    op.create_index(
        "ix_CallbackRequest_contact",
        "CallbackRequest",
        ["contact"],
        unique=False,
    )
    op.create_table(
        "Employee",
        sa.Column(
            "sid",
            sa.INTEGER(),
            server_default=sa.text("nextval('\"Employee_sid_seq\"'::regclass)"),
            autoincrement=True,
            nullable=False,
        ),
        sa.Column(
            "uuid",
            postgresql.UUID(),
            server_default=sa.text("gen_random_uuid()"),
            autoincrement=False,
            nullable=False,
        ),
        sa.Column("name", sa.VARCHAR(), autoincrement=False, nullable=True),
        sa.Column("email", sa.VARCHAR(), autoincrement=False, nullable=True),
        sa.PrimaryKeyConstraint("sid", name="Employee_pkey"),
        sa.UniqueConstraint("uuid", name="Employee_uuid_key"),
    )
    op.create_index("ix_Employee_sid", "Employee", ["sid"], unique=False)
    op.create_index("ix_Employee_name", "Employee", ["name"], unique=False)
    op.create_index("ix_Employee_email", "Employee", ["email"], unique=False)
    op.drop_index(op.f("ix_custom_field_values_sid"), table_name="custom_field_values")
    op.drop_index(
        op.f("ix_custom_field_values_callback_request_id"),
        table_name="custom_field_values",
    )
    op.drop_table("custom_field_values")
    op.drop_index(op.f("ix_callback_request_sid"), table_name="callback_request")
    op.drop_index(op.f("ix_callback_request_id"), table_name="callback_request")
    op.drop_index(op.f("ix_callback_request_contact"), table_name="callback_request")
    op.drop_table("callback_request")
    op.drop_index(op.f("ix_result_sid"), table_name="result")
    op.drop_index(op.f("ix_result_result"), table_name="result")
    op.drop_table("result")
    op.drop_index(op.f("ix_employee_sid"), table_name="employee")
    op.drop_index(op.f("ix_employee_name"), table_name="employee")
    op.drop_index(op.f("ix_employee_email"), table_name="employee")
    op.drop_table("employee")
    op.drop_index(op.f("ix_custom_field_sort_order"), table_name="custom_field")
    op.drop_index(op.f("ix_custom_field_sid"), table_name="custom_field")
    op.drop_index(op.f("ix_custom_field_name"), table_name="custom_field")
    op.drop_table("custom_field")
    # ### end Alembic commands ###
