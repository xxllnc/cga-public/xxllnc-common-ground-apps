"""Rename submitter columns

Revision ID: 02da3c5de99e
Revises: 8195220ac330
Create Date: 2021-09-23 16:23:36.330919+02:00

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = "02da3c5de99e"
down_revision = "8195220ac330"
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column("CallbackRequest", "submitter_id", new_column_name="created_by_id")
    op.alter_column("CallbackRequest", "submitter_name", new_column_name="created_by_name")


def downgrade():
    op.alter_column("CallbackRequest", "created_by_id", new_column_name="submitter_id")
    op.alter_column("CallbackRequest", "created_by_name", new_column_name="submitter_name")
