#!/usr/bin/env python3
from app.db import models
from app.db.session import SessionLocal
from datetime import datetime, timedelta
from sqlalchemy import text
from sqlalchemy.orm import Session


def truncate_tables() -> None:

    db: Session = SessionLocal()

    db.execute(text('truncate table "callback_request" cascade'))
    db.execute(text('truncate table "custom_field" cascade'))
    db.execute(text('truncate table "employee" cascade'))
    db.execute(text('truncate table "result"  cascade'))
    db.execute(text('truncate table "custom_field_values"  cascade'))

    db.commit()


def restart_sequences_with_1() -> None:

    db: Session = SessionLocal()

    db.execute(text('ALTER SEQUENCE "callback_request_id_seq" RESTART WITH 1'))
    db.execute(text('ALTER SEQUENCE "callback_request_sid_seq" RESTART WITH 1'))
    db.execute(text('ALTER SEQUENCE "custom_field_values_sid_seq" RESTART WITH 1'))
    db.execute(text('ALTER SEQUENCE "custom_field_sid_seq" RESTART WITH 1'))
    db.execute(text('ALTER SEQUENCE "employee_sid_seq" RESTART WITH 1'))
    db.execute(text('ALTER SEQUENCE "result_sid_seq" RESTART WITH 1'))

    db.commit()


def add_employees(db):
    # For now add the mock employees
    employees = {
        "number1": "auth0|60b8b386239e8b0070230c9b",
        "name1": "Admin account",
        "email1": "admin@exxellence.nl",
        "number2": "auth0|6141b11678862b0069362d25",
        "name2": "Automatischetest Medewerker afdeling Ö&€ßñ",
        "email2": "automatischetest@exxellence.nl",
    }
    for index in range(1, 3):
        key_name = "name" + str(index)
        key_email = "email" + str(index)
        employee = models.Employee(
            name=employees[key_name],
            email=employees[key_email],
        )
        db.add(employee)
        db.flush()
        db.commit()

    return employees


def create_initial_data() -> None:
    db: Session = SessionLocal()

    gebeld = models.Result(result="Gebeld", default=True)
    niet_bereikbaar = models.Result(result="Niet bereikbaar", default=False)
    db.add(gebeld)
    db.add(niet_bereikbaar)

    uitleg = models.CustomField(
        name="Uitleg",
        description="De uitleg",
        required=False,
        type=models.CustomFieldTypes.text,
        sort_order=2,
    )
    db.add(uitleg)
    telno = models.CustomField(
        name="Extra telefoonnummer",
        description="Een extra telefoonnummer",
        required=False,
        type=models.CustomFieldTypes.string,
        sort_order=1,
    )
    db.add(telno)

    db.flush()
    db.commit()

    # For now add the mock employees
    employees = add_employees(db)

    callback1 = models.CallbackRequest(
        contact="Mevrouw die contact opnam",
        subject="Vraag over voortgang van mijn aanvraag voor mijn nieuwbouw "
        + "woning in het nieuwbouw project Schönehaußen in Hengelo",
        summary="De aanvraag is nog niet in behandeling genomen. Zo gauw dat "
        + "wel het geval is, wordt door de afdeling Bouw & woningtoezicht "
        + "contact opgenomen met mevrouw",
        phone="06 - 123 456 78",
        status=models.Statusses.Open,
        assigned_to_id=employees["number1"],
        assigned_to_name=employees["name1"],
        created_by_id=employees["number2"],
        created_by_name=employees["email2"],
        created_at=datetime.now() - timedelta(days=0, seconds=65, hours=2, weeks=9),
    )

    callback2 = models.CallbackRequest(
        contact="Meneer die een vraag had",
        subject="Mag ik mijn vuilnis naast de container zetten als deze vol " + "is",
        summary="Nee, dat mag niet. U kunt u vuilnis in een nabij gelegen "
        + "container die niet vol zit doen. Als die er niet zijn, dan wordt u "
        + "geacht uw vuil thuis te bewaren tot uw container geleegd is",
        phone="06 - 987 564 32",
        status=models.Statusses.Open,
        assigned_to_id=employees["number2"],
        assigned_to_name=employees["name2"],
        created_by_id=employees["number1"],
        created_by_name=employees["email1"],
        created_at=datetime.now() - timedelta(days=3, seconds=0, hours=0, weeks=4),
    )

    callback3 = models.CallbackRequest(
        contact="Woning coöperatie Goed wonen",
        subject="Of de nieuwe brochure voor het aanpassen van een ouderen of "
        + "invalide woning al bij de gemeente binnen is",
        summary="Nagebeld of deze al in de  publieke ruimte ligt. Nog geen "
        + "antwoord terug ontvangen",
        phone="06 - 321 654 87",
        status=models.Statusses.Open,
        assigned_to_id=employees["number1"],
        assigned_to_name=employees["name1"],
        created_by_id=employees["number2"],
        created_by_name=employees["email2"],
        created_at=datetime.now() - timedelta(days=0, seconds=0, hours=0, weeks=2),
        result=gebeld,
    )

    callback4 = models.CallbackRequest(
        contact="Meneer Cо прашање",
        subject="Macedonische vraag over iets anders",
        summary="Wat is anders: geprobeerd terug te bellen, maar dat lukte "
        + "niet. Morgen nog een keer proberen",
        phone="06 - 321 614 12",
        status=models.Statusses.Afgehandeld,
        assigned_to_id=employees["number2"],
        assigned_to_name=employees["name2"],
        created_by_id=employees["number1"],
        created_by_name=employees["email1"],
        created_at=datetime.now() - timedelta(days=0, seconds=0, hours=0, weeks=1),
        result=niet_bereikbaar,
    )

    db.add(callback1)
    db.add(callback2)
    db.add(callback3)
    db.add(callback4)
    db.add(
        models.CallbackRequest(
            contact="Mevrouw Með sérstöku tilboði",
            subject="IJslandse met een bijzonder voorstel om een IJslandse "
            + "woning te mogen bouwen",
            summary="Afdeling Bouw & woning toezicht graag terugbellen om een "
            + "afspraak met deze mevrouw te maken.",
            phone="06 - 321 614 12",
            status=models.Statusses.Open,
            assigned_to_id=employees["number2"],
            assigned_to_name=employees["name2"],
            created_by_id=employees["number1"],
            created_by_name=employees["email1"],
            created_at=datetime.now() - timedelta(days=0, seconds=10, hours=1),
        )
    )

    db.flush()
    db.commit()

    db.add(
        models.CustomFieldValues(
            callback_request=callback1,
            custom_field=uitleg,
            value="Dit is de waarde van de extra uitleg",
        )
    )
    db.add(
        models.CustomFieldValues(
            callback_request=callback1,
            custom_field=telno,
            value="06 654 321 98",
        )
    )

    db.add(
        models.CustomFieldValues(
            callback_request=callback2,
            custom_field=uitleg,
            value="Een extra uitleg",
        )
    )

    db.add(
        models.CustomFieldValues(
            callback_request=callback3,
            custom_field=telno,
            value="06 654 321 98",
        )
    )

    db.commit()


if __name__ == "__main__":
    print("First remove any existing data")
    truncate_tables()
    print("RESTART sequences")
    restart_sequences_with_1()
    print("Creating initial data")
    create_initial_data()
    print("Initial data created")
