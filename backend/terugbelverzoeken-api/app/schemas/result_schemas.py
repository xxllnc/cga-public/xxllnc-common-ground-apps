from __future__ import annotations

import uuid as py_uuid
from app.schemas.generic_schemas import FilterableId
from exxellence_shared_cga.core.types import SortOrder
from pydantic import BaseModel, Field
from typing import Optional


# RESULT
class ResultBase(BaseModel):
    result: str = Field(..., title="The result name. this is an alias of result", alias="name")
    default: bool = Field(False, title="Default value")


class Result(ResultBase):
    sid: int = Field(..., title="Unique id of the result", alias="id")
    uuid: py_uuid.UUID = Field(..., title="The unique uuid of the result")

    class Config:
        orm_mode = True
        allow_population_by_field_name = True


class ResultFilterableFields(FilterableId):
    result: Optional[str]

    class Config:
        extra = "forbid"


class ResultSortableFields(BaseModel):
    sid: Optional[SortOrder] = Field(alias="id")
    result: Optional[SortOrder] = Field(alias="name")

    class Config:
        extra = "forbid"
