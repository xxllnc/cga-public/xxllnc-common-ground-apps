from __future__ import annotations

import uuid as py_uuid
from app.schemas.generic_schemas import FilterableId
from app.schemas.to_optional import to_optional
from exxellence_shared_cga.core.types import SortOrder
from pydantic import BaseModel, Field
from typing import Optional


# Employee
class EmployeeBase(BaseModel):
    name: str = Field(..., title="The name of the employee")
    email: str = Field(..., title="The email of the employee")


# Properties to return to client
class Employee(EmployeeBase):
    sid: int = Field(..., title="Unique id of the employee", alias="id")
    uuid: py_uuid.UUID = Field(..., title="The unique uuid of the employee")

    class Config:
        orm_mode = True
        allow_population_by_field_name = True


#  Make fields optional to support partial update (patch)
class EmployeeUpdate(to_optional(EmployeeBase)):  # type: ignore
    pass


class EmployeeFilterableFields(FilterableId):
    name: Optional[str]
    email: Optional[str]
    q: Optional[str]

    class Config:
        extra = "forbid"


class EmployeeSortableFields(BaseModel):
    sid: Optional[SortOrder] = Field(alias="id")
    name: Optional[SortOrder]
    email: Optional[SortOrder]

    class Config:
        extra = "forbid"
