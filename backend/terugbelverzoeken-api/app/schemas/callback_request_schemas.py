from __future__ import annotations

import uuid as py_uuid
from app.db.models import Statusses
from app.schemas.custom_field_schemas import CustomFieldsList
from app.schemas.generic_schemas import FilterableId
from datetime import datetime
from exxellence_shared_cga.core.types import SortOrder
from pydantic import BaseModel, Field
from typing import Optional


# Callback Request
class CallbackRequestBase(BaseModel):
    assigned_to_name: Optional[str] = Field(
        None,
        title="Name of employee that is assigned to the callback request",
        alias="assignedToName",
    )
    created_by_name: Optional[str] = Field(
        None, title="Name of the submitter", alias="createdByName"
    )
    contact: str = Field(..., title="Contact")
    subject: str = Field(..., title="Subject")
    summary: Optional[str] = Field(None, title="Summary")
    phone: Optional[str] = Field(None, title="Phone")

    class Config:
        allow_population_by_field_name = True


class CallbackRequestCreatedById(BaseModel):
    created_by_id: Optional[str] = Field(None, title="Id of the submitter", alias="createdById")


class CallbackRequestAssignedToId(BaseModel):
    assigned_to_id: Optional[str] = Field(
        None,
        title="Id of employee that is assigned to the callback request",
        alias="assignedToId",
    )


# Callback Request: create mapping to db model
class CallbackRequestCreate(
    CallbackRequestBase,
    CallbackRequestCreatedById,
    CallbackRequestAssignedToId,
):
    pass


# Callback Request: Create mapping for http request containing CustomFieldsList
class CallbackRequestCreateRequest(CallbackRequestCreate, CustomFieldsList):
    class Config:
        allow_population_by_field_name = True


# Callback Request: update mapping to db model
class CallbackRequestUpdate(CallbackRequestCreate):
    status: Statusses = Field(..., title="Status of the callback request")
    result_id: Optional[int] = Field(None, title="Result ID", alias="resultId")


# Callback Request: Create mapping for http request containing CustomFieldsList
class CallbackRequestUpdateRequest(CallbackRequestUpdate, CustomFieldsList):
    class Config:
        allow_population_by_field_name = True


class CallbackRequest(CallbackRequestBase):
    id: str = Field(..., title="The unique id of the callbackRequest")
    uuid: py_uuid.UUID = Field(..., title="The unique uuid of the callbackRequest")
    status: Statusses = Field(..., title="Status of the callback request")
    result_id: Optional[int] = Field(None, title="Result id", alias="resultId")
    created_at: datetime = Field(..., title="Creation Date", alias="createdAt")
    updated_at: Optional[datetime] = Field(None, title="Change Date", alias="updatedAt")

    class Config:
        orm_mode = True
        allow_population_by_field_name = True


class CallbackRequestDetails(CallbackRequest, CallbackRequestAssignedToId, CustomFieldsList):
    class Config:
        orm_mode = True
        allow_population_by_field_name = True
        extra = "allow"


class CallbackRequestFilterableFields(FilterableId):
    contact: Optional[str]
    assigned_to_name: Optional[int] = Field(alias="assignedToName")
    assignedToMe: Optional[bool]
    status: Optional[Statusses]
    result_id: Optional[int] = Field(alias="resultId")
    created_at_gte: Optional[str] = Field(alias="createdAt_gte")
    created_at_lte: Optional[str] = Field(alias="createdAt_lte")
    created_by_name: Optional[str] = Field(alias="createdByName")
    createdByMe: Optional[bool]
    q: Optional[str]

    class Config:
        allow_population_by_field_name = True
        extra = "forbid"


class CallbackRequestSortableFields(BaseModel):
    sid: Optional[SortOrder] = Field(alias="id")
    contact: Optional[SortOrder]
    created_by_name: Optional[SortOrder] = Field(alias="createdByName")
    subject: Optional[SortOrder]
    phone: Optional[SortOrder]
    created_at: Optional[SortOrder] = Field(alias="createdAt")
    updated_at: Optional[SortOrder] = Field(alias="updatedAt")
    status: Optional[SortOrder]
    assigned_to_name: Optional[SortOrder] = Field(alias="assignedToName")
    Kanaal: Optional[SortOrder]
    name: Optional[SortOrder]
    result: Optional[SortOrder]

    class Config:
        extra = "allow"
