from app import domain, schemas
from pydantic.main import create_model
from typing import Optional


# Dynamicaly create a new detailmodel and include the custom fields
def CallbackRequestDetailsWithCustomFields(org_uuid: Optional[str]):

    custom_field_model = domain.custom_field.get_custom_field_model(org_uuid)

    return create_model(
        "CallbackRequestDetails",
        __base__=schemas.CallbackRequestDetails,
        **custom_field_model,
    )


# Dynamicaly create a new model and include the custom fields
def CallbackRequestWithCustomFields(org_uuid: Optional[str]):

    custom_field_model = domain.custom_field.get_custom_field_model(org_uuid)

    return create_model(
        "CallbackRequest",
        __base__=schemas.CallbackRequest,
        **custom_field_model,
    )


# Dynamically create model to enable filtering on all custom fields
def CallbackRequestFilterableFieldsWithCustomFields(org_uuid: Optional[str]):

    custom_field_model = domain.custom_field.get_custom_field_model(org_uuid)

    return create_model(
        "CallbackRequestFilterableFields",
        __base__=schemas.CallbackRequestFilterableFields,
        **custom_field_model,
    )


# Dynamically create model to enable sorting on all custom fields
def CallbackRequestSortableFieldsWithCustomFields(org_uuid: Optional[str]):

    custom_field_model = domain.custom_field.get_custom_field_model(org_uuid)

    return create_model(
        "CallbackRequestSortableFields",
        __base__=schemas.CallbackRequestSortableFields,
        **custom_field_model,
    )
