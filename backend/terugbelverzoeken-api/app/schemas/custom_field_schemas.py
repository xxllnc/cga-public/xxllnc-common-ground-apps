from __future__ import annotations

import uuid as py_uuid
from app.db.models import CustomFieldTypes
from app.schemas.generic_schemas import FilterableId
from app.schemas.to_optional import to_optional
from exxellence_shared_cga.core.types import SortOrder
from pydantic import BaseModel, Field


class CustomFieldSortOrder(BaseModel):
    sort_order: int = Field(
        ...,
        title="sort order",
        description="Integer representing the sort order of the field.",
        alias="sortOrder",
    )


class Options(BaseModel):
    id: str = Field(..., title="Name")
    name: str = Field(..., title="Value")


# CustomField
class CustomFieldBase(BaseModel):
    name: str = Field(..., title="The name of the custom field")
    description: str | None = Field(None, title="description of the custom field")
    required: bool | None = Field(None, title="True if field is required")
    archived: bool | None = Field(None, title="True if field is archived")


class CustomFieldCreate(CustomFieldBase):
    type: CustomFieldTypes = Field(..., title="Type of field string or text")
    options: list[Options] | None = Field(
        None,
        title="The options in json for the list type",
    )

    class Config:
        allow_population_by_field_name = True


# Make fields optional to support partial update (patch)
# Field type is double because there was a forwardref error in to_optional
class CustomFieldCreateUpdate(
    to_optional(CustomFieldBase),  # type: ignore
    to_optional(CustomFieldSortOrder),  # type: ignore
):
    type: CustomFieldTypes | None = Field(None, title="Type of field string or text")
    options: list[Options] | None = Field(
        None,
        title="The options in json for the list type",
    )


class CustomFieldWithId(CustomFieldCreate):
    sid: int = Field(..., title="Unique id of the custom field", alias="id")


# Properties shared by models stored in DB
class CustomField(CustomFieldWithId, CustomFieldSortOrder):
    uuid: py_uuid.UUID = Field(..., title="The unique uuid of the custom field")

    class Config:
        orm_mode = True
        allow_population_by_field_name = True


class CustomFieldsWithValue(CustomFieldWithId, CustomFieldSortOrder):
    value: str | None = Field(
        None,
        title="The value for the contact moment for the given custom field",
    )

    class Config:
        orm_mode = True
        allow_population_by_field_name = True


class CustomFieldsList(BaseModel):
    customFields: list[CustomFieldsWithValue] | None = Field(
        None, title="CustomFields with the values for the given"
    )


class CustomFieldFilterableFields(FilterableId):
    name: str | None
    archived: bool | None

    class Config:
        extra = "forbid"


class CustomFieldSortableFields(BaseModel):
    sid: SortOrder | None = Field(alias="id")
    sort_order: SortOrder | None = Field(alias="sortOrder")

    class Config:
        extra = "forbid"


# CustomFieldValues
class CustomFieldValues(BaseModel):
    value: str | None = Field(None, title="Value")
    custom_field: CustomField | None = Field(None, title="Custom field")

    class Config:
        orm_mode = True
