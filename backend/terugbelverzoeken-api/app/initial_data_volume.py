#!/usr/bin/env python3
import random
from app.db import models
from app.db.session import SessionLocal
from app.initial_data import restart_sequences_with_1, truncate_tables
from datetime import datetime, timedelta
from sqlalchemy.orm import Session


def add_employees(db):
    # For now add the mock employees
    employees = {
        "number1": "auth0|61653f751c278900682dcd0e",
        "name1": "Admin voor de bulk testen",
        "email1": "cga.admin.bulk@exxellence.nl",
        "number2": "auth0|61653f93edcd510070ea3b7b",
        "name2": "User voor de bulk testen",
        "email2": "cga.user.bulk@exxellence.nl",
    }
    for index in range(1, 3):
        key_name = "name" + str(index)
        key_email = "email" + str(index)
        employee = models.Employee(
            name=employees[key_name],
            email=employees[key_email],
        )
        db.add(employee)
        db.flush()
        db.commit()

    return employees


def create_initial_data() -> None:
    db: Session = SessionLocal()

    gebeld = models.Result(result="Gebeld", default=True)
    niet_bereikbaar = models.Result(result="Niet bereikbaar", default=False)
    db.add(gebeld)
    db.add(niet_bereikbaar)

    uitleg = models.CustomField(
        name="Uitleg",
        description="De uitleg",
        required=False,
        type=models.CustomFieldTypes.text,
        sort_order=2,
    )
    db.add(uitleg)
    telno = models.CustomField(
        name="Extra telefoonnummer",
        description="Een extra telefoonnummer",
        required=False,
        type=models.CustomFieldTypes.string,
        sort_order=1,
    )
    db.add(telno)

    # For now add the mock employees
    employees = add_employees(db)

    db.flush()
    db.commit()

    # Bulk add records
    status = [
        models.Statusses.Open,
        models.Statusses.Open,
        models.Statusses.Open,
        models.Statusses.Open,
        models.Statusses.Open,
        models.Statusses.Open,
        models.Statusses.Open,
        models.Statusses.Open,
        models.Statusses.Open,
        models.Statusses.Open,
        models.Statusses.Afgehandeld,
    ]
    max_records = 9999  # 1000000
    years_of_load = 60 / 365
    days_in_week = 7
    weeks_in_year = 52
    hours_in_day = 24

    seconds_in_hour = 60 * 60
    seconds_in_day = seconds_in_hour * hours_in_day  # ex. 3600 * 24 = 86400
    seconds_in_week = seconds_in_day * days_in_week  # ex. 85300 * 7 = 604800
    # Calculating the first times to subtract
    seconds_interval_into_past = round(years_of_load * weeks_in_year * seconds_in_week)
    seconds_interval = round(seconds_interval_into_past / max_records)
    # ex. (0.2 * 52 * 60 * 60 * 24 * 7) / 100 = > every 62899 second or
    # every 0.73 day or every 17.2 hours
    print("Seconds interval: " + str(seconds_interval))
    print("Seconds interval into past: " + str(seconds_interval_into_past))
    print("Max rexords: " + str(max_records))
    print("Years of load: " + str(years_of_load))
    print("Weeks in year: " + str(weeks_in_year))
    print("Seconds in week: " + str(seconds_in_week))
    (
        weeks_calculated,
        days_calculated,
        hours_calculated,
        seconds_calculated,
    ) = calc_time_to_subtract(
        seconds_interval_into_past,
        seconds_in_week,
        seconds_in_day,
        seconds_in_hour,
    )

    for index in range(1, max_records + 1):
        print(
            "Time interval (recordnr "
            + str(index)
            + "): weeks="
            + str(weeks_calculated)
            + " days="
            + str(days_calculated)
            + " hours="
            + str(hours_calculated)
            + " seconds="
            + str(seconds_calculated)
        )
        str_index = str(index)
        employee_index = str(random.choice(range(1, 2)))
        employee_number = "number" + employee_index
        employee_name = "name" + employee_index
        db.add(
            models.CallbackRequest(
                contact="Mevrouw nummer " + str_index,
                subject="Onderwerp nummer [" + str_index + "]",
                summary="Samenvatting nr "
                + str_index
                + ". Het interval is "
                + str(seconds_interval)
                + ". Dit is een terugbelverzoek van "
                + str(seconds_calculated)
                + " seconden, "
                + str(days_calculated)
                + " dagen, "
                + str(hours_calculated)
                + " uren & "
                + str(weeks_calculated)
                + " weken geleden.",
                phone="06 - " + str(random.choice(range(10000000, 99999999))),
                status=random.choice(status),
                assigned_to_id=employees[employee_number],
                assigned_to_name=employees[employee_name],
                created_by_id=employees["number2"],
                created_by_name=employees["email2"],
                created_at=datetime.now()
                - timedelta(
                    seconds=seconds_calculated,
                    hours=hours_calculated,
                    days=days_calculated,
                    weeks=weeks_calculated,
                ),
            )
        )
        # Calculate the new time calculateds
        if index < max_records:
            seconds_interval_into_past = (max_records - index) * seconds_interval
            (
                weeks_calculated,
                days_calculated,
                hours_calculated,
                seconds_calculated,
            ) = calc_time_to_subtract(
                seconds_interval_into_past,
                seconds_in_week,
                seconds_in_day,
                seconds_in_hour,
            )

    db.flush()
    db.commit()


def calc_time_to_subtract(
    seconds_interval_into_past,
    seconds_in_week,
    seconds_in_day,
    seconds_in_hour,
):
    hours_calculated = 0
    days_calculated = 0
    weeks_calculated = 0
    seconds_calculated = seconds_interval_into_past
    if seconds_calculated > seconds_in_week:
        weeks_calculated = seconds_calculated / seconds_in_week
        str_seconds = repr(weeks_calculated)
        weeks_as_str, str_seconds = str_seconds.split(".")
        weeks_calculated = float(weeks_as_str)
        seconds_calculated = seconds_calculated - (weeks_calculated * seconds_in_week)
    if seconds_calculated > seconds_in_day:
        days_calculated = seconds_calculated / seconds_in_day
        str_seconds = repr(days_calculated)
        days_calculated_as_str, str_seconds = str_seconds.split(".")
        days_calculated = float(days_calculated_as_str)
        seconds_calculated = seconds_calculated - (days_calculated * seconds_in_day)
    if seconds_calculated > seconds_in_hour:
        hours_calculated = seconds_calculated / seconds_in_hour
        str_seconds = repr(hours_calculated)
        hours_calculated_as_str, str_seconds = str_seconds.split(".")
        hours_calculated = float(hours_calculated_as_str)
        seconds_calculated = seconds_calculated - (hours_calculated * seconds_in_hour)
    return (
        weeks_calculated,
        days_calculated,
        hours_calculated,
        seconds_calculated,
    )


if __name__ == "__main__":
    print("First remove any existing data")
    truncate_tables()
    print("RESTART sequences")
    restart_sequences_with_1()
    print("Creating initial data")
    create_initial_data()
    print("Initial data created")
