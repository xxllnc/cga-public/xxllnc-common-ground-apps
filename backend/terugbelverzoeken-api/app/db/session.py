from app.core import config
from exxellence_shared_cga.core.db_session_handler import DBSessionHandler
from fastapi import Header
from sqlalchemy import create_engine
from sqlalchemy.orm import DeclarativeMeta, declarative_base, sessionmaker

Base: DeclarativeMeta = declarative_base()
engine_local = create_engine(
    config.SQLALCHEMY_DATABASE_URI, future=True, echo=config.SQLALCHEMY_ECHO
)
SessionLocal = sessionmaker(bind=engine_local, future=True, autocommit=False, autoflush=False)

db_session_handler = DBSessionHandler(
    app_id=config.APP_ID,
    controlpanel_url=config.CONTROLPANEL_URL,
    sqlalchemy_database_uri=config.SQLALCHEMY_DATABASE_URI,
    eco_client_id=config.AUTH0_ECO_CLIENT_ID,
    eco_client_secret=config.AUTH0_ECO_CLIENT_SECRET,
    eco_audience=config.AUTH0_ECO_AUDIENCE,
    eco_domain=config.AUTH0_ECO_DOMAIN,
    redis_host=config.REDIS_HOST,
    redis_password=config.REDIS_PASSWORD,
    sqlalchemy_echo=config.SQLALCHEMY_ECHO,
    sqlalchemy_pool_size=config.SQLALCHEMY_POOL_SIZE,
    sqlalchemy_pool_max_overflow=config.SQLALCHEMY_POOL_MAX_OVERFLOW,
    use_app_instance_id=config.USE_APP_INSTANCE_ID,
)


def get_db(organization_url: str = Header(...)):

    db = db_session_handler.get_session(organization_url=organization_url)

    try:
        yield db
    finally:
        db.close()
