import datetime
import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as pg
from .session import Base
from enum import Enum
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import DateTime

utcnow = sa.literal_column("TIMEZONE('utc', CURRENT_TIMESTAMP)")


class Statusses(Enum):
    Open = "Open"
    Afgehandeld = "Afgehandeld"


class CallbackRequest(Base):
    __tablename__ = "callback_request"
    default_sort_field = "sid"

    sid = sa.Column(sa.Integer, primary_key=True, index=True)
    id = sa.Column(
        sa.String,
        nullable=False,
        unique=True,
        index=True,
        server_default=sa.func.callback_request_generate_id(),
    )
    uuid = sa.Column(
        pg.UUID(as_uuid=True),
        nullable=False,
        unique=True,
        server_default=sa.func.gen_random_uuid(),
    )
    assigned_to_id = sa.Column(sa.String)
    assigned_to_name = sa.Column(sa.String)
    contact = sa.Column(sa.String, index=True)
    created_by_id = sa.Column(sa.String)
    created_by_name = sa.Column(sa.String)
    subject = sa.Column(sa.String)
    summary = sa.Column(sa.String)
    phone = sa.Column(sa.String)
    status = sa.Column(pg.ENUM(Statusses), nullable=False, default=Statusses.Open)
    result_id = sa.Column(sa.Integer, sa.ForeignKey("result.sid"))
    result = relationship("Result")
    created_at = sa.Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = sa.Column(
        DateTime,
        default=datetime.datetime.utcnow,
        onupdate=datetime.datetime.utcnow,
    )
    custom_fields = relationship(
        "CustomFieldValues",
        back_populates="callback_request",
        cascade_backrefs=False,  # https://docs.sqlalchemy.org/en/14/orm/session_api.html # noqa: E501
    )

    fulltext_fields = [
        {
            "modelName": "CallbackRequest",
            "fields": [
                "id",
                "contact",
                "subject",
                "summary",
                "phone",
                "created_by_name",
                "assigned_to_name",
            ],
        },
        {"modelName": "CustomFieldValues", "fields": ["value"]},
        {"modelName": "Result", "fields": ["result"]},
    ]


class Result(Base):
    __tablename__ = "result"
    default_sort_field = "result"

    sid = sa.Column(sa.Integer, primary_key=True, index=True)
    uuid = sa.Column(
        pg.UUID(as_uuid=True),
        nullable=False,
        unique=True,
        server_default=sa.func.gen_random_uuid(),
    )
    result = sa.Column(sa.String, unique=True, index=True)
    default = sa.Column(sa.Boolean)


class CustomFieldTypes(Enum):
    string = "string"
    text = "text"
    select = "select"


class CustomField(Base):
    __tablename__ = "custom_field"
    default_sort_field = "sort_order"

    sid = sa.Column(sa.Integer, primary_key=True, index=True)
    uuid = sa.Column(
        pg.UUID(as_uuid=True),
        nullable=False,
        unique=True,
        server_default=sa.func.gen_random_uuid(),
    )
    name = sa.Column(sa.String, unique=True, index=True)
    description = sa.Column(sa.String)
    required = sa.Column(sa.Boolean, default=False)
    type = sa.Column(pg.ENUM(CustomFieldTypes), nullable=False)
    sort_order = sa.Column(sa.Integer, index=True, nullable=False)
    callback_requests = relationship(
        "CustomFieldValues",
        back_populates="custom_field",
        cascade_backrefs=False,  # https://docs.sqlalchemy.org/en/14/orm/session_api.html # noqa: E501
    )
    options = sa.Column(sa.JSON)
    archived = sa.Column(sa.Boolean, default=False, nullable=False)


class CustomFieldValues(Base):
    __tablename__ = "custom_field_values"
    default_sort_field = "sid"

    sid = sa.Column(sa.Integer, primary_key=True, index=True)
    callback_request_id = sa.Column(sa.Integer, sa.ForeignKey("callback_request.sid"), index=True)
    custom_field_id = sa.Column(sa.Integer, sa.ForeignKey("custom_field.sid"), nullable=False)
    value = sa.Column(sa.String)
    custom_field = relationship(
        "CustomField",
        back_populates="callback_requests",
    )
    callback_request = relationship(
        "CallbackRequest",
        back_populates="custom_fields",
    )
    sa.UniqueConstraint("callback_request_id", "custom_field_id")


class Employee(Base):
    __tablename__ = "employee"
    default_sort_field = "name"

    sid = sa.Column(sa.Integer, primary_key=True, index=True)
    uuid = sa.Column(
        pg.UUID(as_uuid=True),
        nullable=False,
        unique=True,
        server_default=sa.func.gen_random_uuid(),
    )
    name = sa.Column(sa.String, unique=True, index=True)
    email = sa.Column(sa.String, unique=True, index=True)

    fulltext_fields = [
        {
            "modelName": "Employee",
            "fields": ["name", "email"],
        },
    ]
