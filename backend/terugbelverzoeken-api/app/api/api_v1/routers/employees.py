import logging
import uuid
from app import domain, schemas
from app.core.auth0 import get_admin_scopes, get_user_scopes
from app.db.session import get_db
from exxellence_shared_cga.core.rest_helpers import SearchParameters
from exxellence_shared_cga.core.types import GenericId, IdType
from fastapi import APIRouter, Depends, Response
from typing import List, Union

logger = logging.getLogger(__name__)
router = r = APIRouter()

validate_search_parameters = SearchParameters(
    filter_options_schema=schemas.EmployeeFilterableFields,
    sort_options_schema=schemas.EmployeeSortableFields,
)


@r.get(
    "/employees/{id}",
    response_model=schemas.Employee,
    responses={404: {"description": "Employee not found"}},
    dependencies=[Depends(get_user_scopes)],
)
def get_employee(
    id: Union[int, uuid.UUID],
    db=Depends(get_db),
):
    "Retrieve a single custom field, given its unique id"
    id_type = IdType.uuid if isinstance(id, uuid.UUID) else IdType.sid
    employee = domain.employee.get(db=db, id=GenericId(id=id, type=id_type))

    return employee


@r.get(
    "/employees",
    response_model=List[schemas.Employee],
    dependencies=[Depends(get_user_scopes)],
)
def get_employees(
    response: Response,
    db=Depends(get_db),
    search_parameters: dict = Depends(validate_search_parameters),
):
    search_result = domain.employee.get_multi(db=db, query=None, **search_parameters)

    response.headers["Content-Range"] = search_result.as_content_range("results")

    return search_result.result


@r.post(
    "/employees",
    response_model=schemas.Employee,
    responses={409: {"description": "Conflict. Employee not added."}},
    dependencies=[Depends(get_admin_scopes)],
)
def create_employee(
    employee: schemas.EmployeeBase,
    db=Depends(get_db),
):
    employee = domain.employee.create(db=db, obj_in=employee)
    return employee


@r.put(
    "/employees/{id}",
    response_model=schemas.Employee,
    responses={
        404: {"description": "Employee not found"},
        409: {"description": "Conflict. Employee not updated."},
    },
    dependencies=[Depends(get_admin_scopes)],
)
@r.patch(
    "/employees/{id}",
    response_model=schemas.Employee,
    responses={
        404: {"description": "Employee not found"},
        409: {"description": "Conflict. Employee not updated."},
    },
    dependencies=[Depends(get_admin_scopes)],
)
def update_employee(
    id: Union[int, uuid.UUID],
    employee: schemas.EmployeeUpdate,
    db=Depends(get_db),
):
    "Update an employee"
    id_type = IdType.uuid if isinstance(id, uuid.UUID) else IdType.sid
    updated_org = domain.employee.update(
        db=db,
        id=GenericId(id=id, type=id_type),
        obj_in=employee,
    )

    return updated_org


@r.delete(
    "/employees/{id}",
    response_model=schemas.Employee,
    responses={
        404: {"description": "Employee not found"},
        409: {"description": "Conflict. Employee not deleted."},
    },
    dependencies=[Depends(get_admin_scopes)],
)
def delete_employee(
    id: Union[uuid.UUID, int],
    db=Depends(get_db),
):
    "Delete employee"
    id_type = IdType.uuid if isinstance(id, uuid.UUID) else IdType.sid
    return domain.employee.delete(db=db, id=GenericId(id=id, type=id_type))
