import pytest
from app import schemas
from app.api.api_v1.routers.tests.test_mock_data import add_mock_data_to_db
from app.db import models
from fastapi.testclient import TestClient


def _asser_employee_request(response: dict, expected_employee: models.Employee):
    "Use this function to check if the respone matches the db mock"

    expected = schemas.Employee.from_orm(expected_employee).dict(by_alias=True)

    assert response["id"] == expected["id"]
    assert response["name"] == expected["name"]
    assert response["email"] == expected["email"]


# Test route with sorting and range
@pytest.mark.parametrize(
    "http_method, request_parameters, expected_result, status_code",
    [
        ("get", "/54321", [], 404),
        ("get", '?sort=["id","ASC"]', ["1", "2", "3", "4"], 200),
        ("get", '?sort=["id","DESC"]', ["4", "3", "2", "1"], 200),
        ("get", '?filter={"name":"Niek"}', ["2"], 200),
        ("get", '?filter={"email":"w.bouwmeester@exxellence.nl"}', ["3"], 200),
        ("get", "?range=[0,1]", ["1", "2"], 200),
        ("get", "?range=[2,2]", ["4"], 200),
        ("get", '?sort=["id","DESC"]&range=[1,2]', ["3", "2"], 200),
    ],
)
def test_employee_requests(
    client: TestClient,
    test_db,
    http_method: str,
    request_parameters: str,
    expected_result: list[str],
    status_code: int,
):
    mock = add_mock_data_to_db(test_db)

    call = getattr(client, http_method, None)
    assert call is not None

    response = call(f"/terugbelverzoeken/api/v1/employees{request_parameters}")

    assert response.status_code == status_code

    if status_code == 200:
        res = response.json()
        expected_length = len(expected_result)

        assert len(res) == expected_length

        callback_requests = [getattr(mock, f"employee{nr}") for nr in expected_result]
        for index in range(expected_length):
            _asser_employee_request(res[index], callback_requests[index])
