import pytest
from app import schemas
from app.api.api_v1.routers.tests.test_mock_data import add_mock_data_to_db
from app.db import models
from fastapi.testclient import TestClient
from typing import Final, Union

url: Final = "/terugbelverzoeken/api/v1/callbackRequests"


def _asser_callback_request(response: dict, expected_request: models.CallbackRequest):
    "Use this function to check if the respone matches the db mock"
    expected = (
        schemas.CallbackRequestWithCustomFields("org_test")
        .from_orm(expected_request)
        .dict(by_alias=True)
    )
    assert response["id"] == expected["id"]
    assert response["uuid"] == str(expected["uuid"])
    assert response["assignedToName"] == expected["assignedToName"]
    assert response["createdByName"] == expected["createdByName"]
    assert response["contact"] == expected["contact"]
    assert response["subject"] == expected["subject"]
    assert response["summary"] == expected["summary"]
    assert response["phone"] == expected["phone"]
    assert response["status"] == expected["status"].value
    assert response["createdAt"] == expected["createdAt"].isoformat()
    assert response["updatedAt"] == expected["updatedAt"].isoformat()


# Test route with sorting and range
@pytest.mark.parametrize(
    "request_parameters, expected_result, expected_length",
    [
        ("", ["1", "2", "3", "4", "5", "6", "7", "8"], None),
        ('sort=["id","ASC"]', ["1", "2", "3", "4", "5", "6", "7", "8"], None),
        ('sort=["id","DESC"]', ["8", "7", "6", "5", "4", "3", "2", "1"], None),
        ('sort=["contact","ASC"]', ["2", "5", "4", "7", "8", "6", "3", "1"], None),
        ('sort=["contact","DESC"]', ["1", "3", "6", "7", "8", "4", "5", "2"], None),
        ('sort=["assignedToName","ASC"]', ["1", "2", "7", "3", "8", "5", "4", "6"], None),
        ('sort=["assignedToName","DESC"]', ["4"], 8),
        ('sort=["createdByName","ASC"]', ["1", "2", "3", "5", "8", "4", "6", "7"], None),
        ('sort=["createdByName","DESC"]', ["7"], 8),
        ("range=[0,2]", ["1", "2", "3"], None),
        ("range=[3,5]", ["4", "5", "6"], None),
        ('filter={"contact":"Gabriëlle"}', ["5"], None),
        ('filter={"contact":"Niek"}', ["7", "8"], None),
        ('filter={"createdByName":"Erwin"}', ["1", "2"], 5),
        ('filter={"createdByName":"Onbekend"}', [], None),
        ('filter={"createdByMe":"True"}', ["1", "2"], 5),
        ('filter={"createdByMe":"False"}', [], 8),
        ('filter={"assignedToMe":"True"}', ["1"], None),
        ('filter={"assignedToMe":"False"}', [], 8),
        ('filter={"createdAt_gte":"2021-05-12T00:01:00.000Z"}', ["4", "5", "6", "7", "8"], None),
        ('filter={"createdAt_lte":"2021-05-11T00:01:00.000Z"}', ["1", "2"], None),
        ('range=[0,4]&sort=["contact","DESC"]&filter={"contact":"Gabriëlle"}', ["5"], None),
        ('filter={"q":"paspoort"}', ["5"], None),
        ('filter={"q":"ebel"}', ["2", "3"], None),
        ('filter={"q":"Gabriëlle"}', ["5"], None),
        ('filter={"q":"Will"}', ["3", "4", "5", "6"], None),
    ],
)
def test_get_many_callback_requests(
    client: TestClient,
    test_db,
    request_parameters: str,
    expected_result: list[str],
    expected_length: Union[int, None],
):
    mock = add_mock_data_to_db(test_db)

    response = client.get(f"{url}?{request_parameters}")

    assert response.status_code == 200

    res = response.json()
    length = expected_length if expected_length else len(expected_result)

    assert len(res) == length

    callback_requests = [getattr(mock, f"cb{nr}") for nr in expected_result]
    for index in range(len(expected_result)):
        _asser_callback_request(res[index], callback_requests[index])


# ---------------------------------------------
#                      Get /{id}
# ---------------------------------------------
def test_get_callback_request_by_id(client: TestClient, test_db):

    mock = add_mock_data_to_db(test_db)

    # test both id and uuid
    for id in [mock.cb1.id, mock.cb1.uuid]:
        response = client.get(f"{url}/{id}")

        assert response.status_code == 200
        res = response.json()
        _asser_callback_request(res, mock.cb1)


# ---------------------------------------------
#                      Post
# ---------------------------------------------
def test_post_callbackrequest(client: TestClient, test_db):

    mock = add_mock_data_to_db(test_db)
    body = {
        "contact": "Timo Tester",
        "subject": "Testen van API",
        "summary": "Hoe werkt het testen?",
        "phone": "06 123 465 78",
        "assignedToId": mock.employee1.sid,
        "assignedToName": mock.employee1.name,
    }
    response = client.post(url, json=body)

    assert response.status_code == 200
    res = response.json()
    assert res["id"] is not None
    assert res["createdByName"] == "Test User"
    for field in body:
        if field != "assignedToId":
            assert res[field] == body[field]


def test_post_callbackrequest_with_custom_field(client: TestClient, test_db):

    mock = add_mock_data_to_db(test_db)
    body = {
        "contact": "Timo Tester",
        "subject": "Testen van API",
        "summary": "Hoe werkt het testen?",
        "phone": "06 123 465 78",
        "assignedToId": mock.employee1.sid,
        "assignedToName": mock.employee1.name,
        "customFields": [
            {
                "id": mock.custom_field1.sid,
                "name": mock.custom_field1.name,
                "value": "Dit is een test hallo!",
                "type": "string",
                "required": mock.custom_field1.required,
                "sort_order": 1,
            }
        ],
    }

    response = client.post(url, json=body)

    assert response.status_code == 200
    res = response.json()
    assert res["id"] is not None
    assert res["createdByName"] == "Test User"


# ---------------------------------------------
#                      Delete /{id}
# ---------------------------------------------
def test_delete_callbackrequest(client: TestClient, test_db):

    mock = add_mock_data_to_db(test_db)
    # test both id and uuid
    for id in [mock.cb1.id, mock.cb2.uuid]:
        response = client.delete(f"{url}/{id}")

        assert response.status_code == 200
    # test both id and uuid
    for id in [mock.cb3.id, mock.cb4.uuid]:
        response = client.delete(f"{url}/{id}")

        assert response.status_code == 200


def test_delete_callbackrequest_twice(client: TestClient, test_db):
    mock = add_mock_data_to_db(test_db)
    response1 = client.delete(f"{url}/{mock.cb1.id}")
    response2 = client.delete(f"{url}/{mock.cb1.id}")

    assert response1.status_code == 200
    assert response2.status_code == 404


# # ---------------------------------------------
# #                      PUT /{id}
# # ---------------------------------------------
def test_put_callbackrequest_with_id(client: TestClient, test_db):
    mock = add_mock_data_to_db(test_db)

    response = client.put(
        f"{url}/{mock.cb2.id}",
        json={
            "id": mock.cb2.id,
            "contact": "Tim Tester",
            "subject": "Vraag over het resultaaten",
            "summary": "Wat is het resultaat",
            "phone": "06 - 987 564 31",
            "status": "Open",
        },
    )

    assert response.status_code == 200
    res = response.json()

    assert res["contact"] == "Tim Tester"
    assert res["subject"] == "Vraag over het resultaaten"


def test_put_callbackrequest_with_id_update_employee(client: TestClient, test_db):

    mock = add_mock_data_to_db(test_db)

    response = client.put(
        f"{url}/{mock.cb3.uuid}",
        json={
            "id": str(mock.cb3.id),
            "contact": "Erwin",
            "subject": "Vraag over het resultaaten",
            "summary": "Wat is het resultaat",
            "phone": "06 - 987 564 31",
            "assignedToId": mock.employee1.sid,
            "assignedToName": mock.employee1.name,
            "status": "Open",
        },
    )

    assert response.status_code == 200
    res = response.json()
    assert res["assignedToName"] == mock.employee1.name


def test_put_callbackrequest_with_id_update_statatus(client: TestClient, test_db):
    mock = add_mock_data_to_db(test_db)
    response = client.put(
        f"{url}/{mock.cb1.id}",
        json={
            "id": str(mock.cb1.id),
            "contact": "Erwin",
            "subject": "Vraag over het resultaaten",
            "summary": "Wat is het resultaat",
            "phone": "06 - 987 564 31",
            "status": "Afgehandeld",
        },
    )

    assert response.status_code == 200
    res = response.json()
    assert res["status"] == "Afgehandeld"


def test_put_callbackrequest_with_id_update_result(client: TestClient, test_db):
    mock = add_mock_data_to_db(test_db)
    response = client.put(
        f"{url}/{mock.cb1.id}",
        json={
            "id": str(mock.cb1.id),
            "contact": "Erwin",
            "subject": "Vraag over het resultaaten",
            "summary": "Wat is het resultaat",
            "phone": "06 - 987 564 31",
            "status": "Afgehandeld",
            "resultId": mock.result1.sid,
        },
    )

    assert response.status_code == 200
    res = response.json()
    assert res["resultId"] == mock.result1.sid
