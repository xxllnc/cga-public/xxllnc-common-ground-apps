import pytest
from app import schemas
from app.api.api_v1.routers.tests.test_mock_data import add_mock_data_to_db
from app.db import models
from fastapi.testclient import TestClient
from typing import Final, Union

url: Final = "/terugbelverzoeken/api/v1/results"


def _asser_result_request(response: dict, expected_result: models.Result):
    "Use this function to check if the respone matches the db mock"

    expected = schemas.Result.from_orm(expected_result).dict(by_alias=True)
    assert response["id"] == expected["id"]
    assert response["uuid"] == str(expected["uuid"])
    assert response["name"] == expected["name"]
    assert response["default"] == expected["default"]


# Test route with sorting and range
@pytest.mark.parametrize(
    "http_method, request_parameters, json, expected_result, status_code",
    [
        ("get", "/54321", None, [], 404),
        ("get", '?sort=["id","ASC"]', None, ["1", "2", "3"], 200),
        ("get", '?sort=["id","DESC"]', None, ["3", "2", "1"], 200),
        ("get", '?filter={"result":"Gebeld"}', None, ["1"], 200),
        ("get", "?range=[0,1]", None, ["1", "2"], 200),
        ("get", '?sort=["id","DESC"]&range=[1,1]', None, ["2"], 200),
        ("post", "", {"name": "TestResult 1", "default": True}, [], 200),
    ],
)
def test_result_requests(
    client: TestClient,
    test_db,
    http_method: str,
    request_parameters: str,
    json: Union[dict[str, str], None],
    expected_result: list[str],
    status_code: int,
):
    mock = add_mock_data_to_db(test_db)

    expected_length = len(expected_result)
    call = getattr(client, http_method, None)
    assert call is not None

    response = call(f"{url}{request_parameters}", json=json)

    assert response.status_code == status_code

    if status_code == 200 and expected_length > 0:
        res = response.json()

        assert len(res) == expected_length

        callback_requests = [getattr(mock, f"result{nr}") for nr in expected_result]
        for index in range(expected_length):
            _asser_result_request(res[index], callback_requests[index])


# ---------------------------------------------
#                      Get /{uuid || id}
# ---------------------------------------------
def test_get_result_by_id_and_uuid(client: TestClient, test_db):

    mock = add_mock_data_to_db(test_db)

    response_id = client.get(f"{url}/{mock.result1.sid}")
    assert response_id.status_code == 200
    _asser_result_request(response_id.json(), mock.result1)

    response_uuid = client.get(f"{url}/{mock.result1.uuid}")
    assert response_uuid.status_code == 200
    _asser_result_request(response_uuid.json(), mock.result1)


# ---------------------------------------------
#                      Delete /{uuid || id}
# ---------------------------------------------
def test_delete_result(client: TestClient, test_db):

    mock = add_mock_data_to_db(test_db)

    response = client.delete(f"{url}/{mock.result3.sid}")
    assert response.status_code == 200
