#!/usr/bin/env python3

from app.db import models
from collections import namedtuple
from datetime import datetime
from sqlalchemy.orm import Session

# Declaring namedtuple() as return value
MockData = namedtuple(
    "MockData",
    [
        "result1",
        "result2",
        "result3",
        "custom_field1",
        "custom_field2",
        "employee1",
        "employee2",
        "employee3",
        "employee4",
        "cb1",
        "cb2",
        "cb3",
        "cb4",
        "cb5",
        "cb6",
        "cb7",
        "cb8",
        "submitter_a",
        "submitter_b",
    ],
)


def add_mock_data_to_db(db: Session) -> MockData:

    result1 = models.Result(result="Gebeld", default=True)
    result2 = models.Result(result="Niet bereikbaar", default=False)
    result3 = models.Result(result="Te verwijderen", default=False)

    db.add(result1)
    db.add(result2)
    db.add(result3)

    custom_field1 = models.CustomField(
        name="Kanaal",
        description="Vul het kanaal in",
        required=False,
        type=models.CustomFieldTypes.text,
        sort_order=1,
    )
    custom_field2 = models.CustomField(
        name="Telefoonnummer",
        description="Vul een telefoonnummer in",
        required=False,
        type=models.CustomFieldTypes.string,
        sort_order=2,
    )
    db.add(custom_field1)
    db.add(custom_field2)

    employee1 = models.Employee(name="Erwin", email="e.platenkamp@exxellence.nl")
    employee2 = models.Employee(name="Niek", email="n.bruins@exxellence.nl")
    employee3 = models.Employee(name="Wouter", email="w.bouwmeester@exxellence.nl")
    employee4 = models.Employee(name="Willem", email="w.dekker@exxellence.nl")

    db.add(employee1)
    db.add(employee2)
    db.add(employee3)
    db.add(employee4)

    db.flush()

    submitter_a = {
        "id": "94a3f2ca-350d-4456-aeb0-8fb6093c8810",
        "name": "Erwin",
    }
    submitter_b = {"id": "id54321BA", "name": "Willem"}

    cb1 = models.CallbackRequest(
        assigned_to_id=submitter_a["id"],
        assigned_to_name=submitter_a["name"],
        contact="Wouter",
        subject="Vraag over de voortgang",
        summary="Wat is de voortgang",
        phone="06 - 987 564 32",
        status=models.Statusses.Open,
        created_at=datetime(2021, 5, 10, 00, 1, 00),
        created_by_id=submitter_a["id"],
        created_by_name=submitter_a["name"],
    )
    cb2 = models.CallbackRequest(
        assigned_to_id=employee1.sid,
        assigned_to_name=employee1.name,
        contact="Erwin",
        subject="Vraag over het testen",
        summary="Wat is het testen",
        phone="06 - 456 789 45",
        status=models.Statusses.Afgehandeld,
        created_at=datetime(2021, 5, 11, 00, 1, 00),
        created_by_id=submitter_a["id"],
        created_by_name=submitter_a["name"],
        result=result1,
    )
    cb3 = models.CallbackRequest(
        assigned_to_id=employee2.sid,
        assigned_to_name=employee2.name,
        contact="Willem",
        subject="Vraag over het testen 2",
        summary="Wat is het testen 2",
        phone="06 - 987 456 12",
        status=models.Statusses.Afgehandeld,
        created_at=datetime(2021, 5, 11, 00, 2, 00),
        created_by_id=submitter_a["id"],
        created_by_name=submitter_a["name"],
        result=result1,
    )
    cb4 = models.CallbackRequest(
        assigned_to_id=employee3.sid,
        assigned_to_name=employee3.name,
        contact="Irene",
        subject="Vraag over subsidie",
        summary="Wat is subsidiëren?",
        phone="06 - 132 456 78",
        status=models.Statusses.Open,
        created_at=datetime(2021, 5, 12, 00, 1, 00),
        created_by_id=submitter_b["id"],
        created_by_name=submitter_b["name"],
    )
    cb5 = models.CallbackRequest(
        assigned_to_id=employee4.sid,
        assigned_to_name=employee4.name,
        contact="Gabriëlle",
        subject="Aanvraag paspoort",
        summary="Kan ik twee paspoorten aanvragen",
        phone="06 - 741 852 65",
        status=models.Statusses.Open,
        created_at=datetime(2021, 5, 13, 00, 1, 00),
        created_by_id=submitter_a["id"],
        created_by_name=submitter_a["name"],
    )
    cb6 = models.CallbackRequest(
        assigned_to_id=employee3.sid,
        assigned_to_name=employee3.name,
        contact="Stijn",
        subject="Vraag over het testen 4",
        summary="Tester de test",
        phone="06 - 167 943 45",
        status=models.Statusses.Afgehandeld,
        created_at=datetime(2021, 5, 13, 00, 2, 00),
        created_by_id=submitter_b["id"],
        created_by_name=submitter_b["name"],
        result=result2,
    )
    cb7 = models.CallbackRequest(
        assigned_to_id=employee1.sid,
        assigned_to_name=employee1.name,
        contact="Niek",
        subject="De eerste vraag",
        summary="Dit is de eerste vraag",
        phone="06 - 123 456 98",
        status=models.Statusses.Open,
        created_at=datetime(2021, 5, 14, 00, 1, 00),
    )
    cb8 = models.CallbackRequest(
        assigned_to_id=employee2.sid,
        assigned_to_name=employee2.name,
        contact="Niek",
        subject="Wat is de voorgang van de eerste vraag",
        summary="Dit is de vraag over de tweede vraag",
        phone="06 - 123 456 98",
        status=models.Statusses.Open,
        created_at=datetime(2021, 5, 14, 00, 3, 00),
        created_by_id=submitter_a["id"],
        created_by_name=submitter_a["name"],
    )

    db.add(cb1)
    db.add(cb2)
    db.add(cb3)
    db.add(cb4)
    db.add(cb5)
    db.add(cb6)
    db.add(cb7)
    db.add(cb8)

    db.flush()

    # Add custom field values for contactmoments
    for cb in [cb1, cb2, cb3, cb4, cb5, cb7, cb8]:
        for field in [custom_field1, custom_field2]:
            db.add(
                models.CustomFieldValues(
                    callback_request=cb,
                    custom_field=field,
                    value=f"Veld {field.name} contact {cb.contact}",
                )
            )

    db.flush()

    return MockData(
        result1,
        result2,
        result3,
        custom_field1,
        custom_field2,
        employee1,
        employee2,
        employee3,
        employee4,
        cb1,
        cb2,
        cb3,
        cb4,
        cb5,
        cb6,
        cb7,
        cb8,
        submitter_a,
        submitter_b,
    )
