import pytest
from fastapi.testclient import TestClient


@pytest.mark.parametrize(
    "http_method, route, expected_status_code",
    [
        ("get", "callbackRequests", 200),
        ("get", "customFields", 200),
        ("get", "results", 200),
        ("get", "callbackRequests/89", 404),
        ("get", "callbackRequests/5d2425a6-6d17-422d-8c5f-f585db932f5e", 404),
        ("get", "customFields/89", 404),
        ("get", "customFields/5d2425a6-6d17-422d-8c5f-f585db932f5e", 404),
        ("get", "results/89", 404),
        ("get", "results/5d2425a6-6d17-422d-8c5f-f585db932f5e", 404),
        ("post", "callbackRequests", 422),
        ("post", "customFields", 422),
        ("post", "results", 422),
        ("post", "callbackRequests/1", 405),
        ("post", "customFields/1", 405),
        ("post", "results/1", 405),
        ("put", "callbackRequests", 405),
        ("put", "customFields", 405),
        ("put", "results", 405),
        ("put", "callbackRequests/1", 422),
        ("put", "customFields/1", 422),
        ("put", "results/1", 422),
        ("delete", "callbackRequests/1", 404),
        ("delete", "customFields/1", 404),
        ("delete", "results/1", 404),
    ],
)
def test_routers_without_data_in_db(
    client: TestClient,
    http_method,
    route,
    expected_status_code,
):
    call = getattr(client, http_method, None)
    assert call is not None

    response = call(f"/terugbelverzoeken/api/v1/{route}")
    assert response.status_code == expected_status_code
