import uuid
from app import domain, schemas
from app.core.auth0 import get_admin_scopes, get_user_scopes
from app.db.session import get_db
from exxellence_shared_cga.core.rest_helpers import SearchParameters
from exxellence_shared_cga.core.types import GenericId, IdType
from fastapi import APIRouter, Depends, Response
from typing import List, Union

router = r = APIRouter()

validate_search_parameters = SearchParameters(
    filter_options_schema=schemas.ResultFilterableFields,
    sort_options_schema=schemas.ResultSortableFields,
)


@r.get(
    "/results/{id}",
    response_model=schemas.Result,
    responses={404: {"description": "Result not found"}},
    dependencies=[Depends(get_user_scopes)],
)
def get_custom_field(
    id: Union[int, uuid.UUID],
    db=Depends(get_db),
):
    "Retrieve a single custom field, given its unique id"
    id_type = IdType.uuid if isinstance(id, uuid.UUID) else IdType.sid
    return domain.result.get(db=db, id=GenericId(id=id, type=id_type))


@r.get(
    "/results",
    response_model=List[schemas.Result],
    dependencies=[Depends(get_user_scopes)],
)
def get_results(
    response: Response,
    db=Depends(get_db),
    search_parameters: dict = Depends(validate_search_parameters),
):
    search_result = domain.result.get_multi(db=db, query=None, **search_parameters)

    response.headers["Content-Range"] = search_result.as_content_range("results")

    return search_result.result


@r.post(
    "/results",
    response_model=schemas.Result,
    responses={409: {"description": "Conflict. Result not added."}},
    dependencies=[Depends(get_admin_scopes)],
)
def create_result(
    result: schemas.ResultBase,
    db=Depends(get_db),
):

    return domain.result.create(db=db, obj_in=result)


@r.put(
    "/results/{id}",
    response_model=schemas.Result,
    responses={
        404: {"description": "Result not found"},
        409: {"description": "Conflict. Result not updated."},
    },
    dependencies=[Depends(get_admin_scopes)],
)
def update_result(
    id: Union[int, uuid.UUID],
    result: schemas.ResultBase,
    db=Depends(get_db),
):
    "Update an result"
    id_type = IdType.uuid if isinstance(id, uuid.UUID) else IdType.sid
    updated = domain.result.update(
        db=db,
        id=GenericId(id=id, type=id_type),
        obj_in=result,
    )

    return updated


@r.delete(
    "/results/{id}",
    response_model=schemas.Result,
    responses={
        404: {"description": "Result not found"},
        409: {"description": "Conflict. Result not deleted."},
    },
    dependencies=[Depends(get_admin_scopes)],
)
def delete_result(
    id: Union[int, uuid.UUID],
    db=Depends(get_db),
):
    "Delete result"
    id_type = IdType.uuid if isinstance(id, uuid.UUID) else IdType.sid
    result = domain.result.delete(db=db, id=GenericId(id=id, type=id_type))

    return result
