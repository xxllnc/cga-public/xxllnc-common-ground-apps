import logging
import uuid
from app import domain, schemas
from app.core.auth0 import get_admin_scopes, get_user_scopes
from app.db.models import CustomFieldTypes
from app.db.session import get_db
from exxellence_shared_cga.core.rest_helpers import SearchParameters
from exxellence_shared_cga.core.types import GenericId, IdType
from fastapi import APIRouter, Depends, Response
from sqlalchemy.orm import Session
from typing import List, Union

logger = logging.getLogger(__name__)
router = r = APIRouter()

validate_search_parameters = SearchParameters(
    filter_options_schema=schemas.CustomFieldFilterableFields,
    sort_options_schema=schemas.CustomFieldSortableFields,
)


@r.get(
    "/customFields/{id}",
    response_model=schemas.CustomField,
    responses={404: {"description": "CustomField not found"}},
    dependencies=[Depends(get_user_scopes)],
)
def get_custom_field(
    id: Union[int, uuid.UUID],
    db: Session = Depends(get_db),
):
    "Retrieve a single custom field, given its unique id"
    id_type = IdType.uuid if isinstance(id, uuid.UUID) else IdType.sid

    custom_field = domain.custom_field.get(db=db, id=GenericId(id=id, type=id_type))

    return custom_field


@r.get(
    "/customFields",
    response_model=List[schemas.CustomField],
    dependencies=[Depends(get_user_scopes)],
)
def get_custom_fields(
    response: Response,
    search_parameters: dict = Depends(validate_search_parameters),
    db: Session = Depends(get_db),
):

    search_result = domain.custom_field.get_multi(db=db, query=None, **search_parameters)

    response.headers["Content-Range"] = search_result.as_content_range("results")

    return search_result.result


@r.post(
    "/customFields",
    response_model=schemas.CustomField,
    responses={409: {"description": "Conflict. CustomField not added."}},
    dependencies=[Depends(get_admin_scopes)],
)
def create_custom_field(
    custom_field: schemas.CustomFieldCreate,
    db: Session = Depends(get_db),
):

    return domain.custom_field.create(db=db, custom_field=custom_field)


@r.put(
    "/customFields/{id}",
    response_model=schemas.CustomField,
    responses={
        404: {"description": "CustomField not found"},
        409: {"description": "Conflict. CustomField not updated."},
    },
    dependencies=[Depends(get_admin_scopes)],
)
@r.patch(
    "/customFields/{id}",
    response_model=schemas.CustomField,
    responses={
        404: {"description": "CustomField not found"},
        409: {"description": "Conflict. CustomField not updated."},
    },
    dependencies=[Depends(get_admin_scopes)],
)
def update_custom_field(
    id: Union[int, uuid.UUID],
    custom_field: schemas.CustomFieldCreateUpdate,
    db: Session = Depends(get_db),
):
    "Update an customField"
    id_type = IdType.uuid if isinstance(id, uuid.UUID) else IdType.sid

    if custom_field.type not in [CustomFieldTypes.select]:
        custom_field.options = None

    return domain.custom_field.update_custom_field(
        db=db, id=GenericId(id=id, type=id_type), obj_in=custom_field
    )


@r.delete(
    "/customFields/{id}",
    response_model=schemas.CustomField,
    responses={
        404: {"description": "CustomField not found"},
        409: {"description": "Conflict. CustomField not deleted."},
    },
    dependencies=[Depends(get_admin_scopes)],
)
def delete_custom_field(
    id: Union[int, uuid.UUID],
    db: Session = Depends(get_db),
):
    "Delete customField"
    id_type = IdType.uuid if isinstance(id, uuid.UUID) else IdType.sid

    return domain.custom_field.delete(db=db, id=GenericId(id=id, type=id_type))
