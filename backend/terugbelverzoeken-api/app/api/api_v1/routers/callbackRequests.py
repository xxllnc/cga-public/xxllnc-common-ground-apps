import sqlalchemy as sa
from app import domain, schemas
from app.core.auth0 import AccessUser, get_current_user, get_user_scopes
from app.db import models
from app.db.session import get_db
from exxellence_shared_cga.core.rest_helpers import SearchParameters
from exxellence_shared_cga.core.types import FilterOptions, GenericId, IdType
from fastapi import APIRouter, Depends, Response
from sqlalchemy.orm import Session
from typing import Any
from uuid import UUID

router = r = APIRouter()


# validate the filter and sort parameters
def validate_search_parameters():
    return SearchParameters(
        filter_options_schema=schemas.CallbackRequestFilterableFieldsWithCustomFields(None),
        sort_options_schema=schemas.CallbackRequestSortableFieldsWithCustomFields(None),
    )


def _handle_filter_by_me(search_parameters_input: dict, user_id: str) -> dict:
    """
    This function checks if there is a filter createdByMe or assigedToMe.
    If it is there and it is True then the filter fields
    created_by_id and/or assigned_to_id is set to the user id
    """

    filter_options: FilterOptions | None = search_parameters_input.get("filter_options")
    if filter_options:
        if filter_options.options.get("createdByMe") is True:
            filter_options.options["created_by_id"] = user_id
        if filter_options.options.get("createdByMe") is not None:
            filter_options.options.pop("createdByMe")

        if filter_options.options.get("assignedToMe") is True:
            filter_options.options["assigned_to_id"] = user_id
        if filter_options.options.get("assignedToMe") is not None:
            filter_options.options.pop("assignedToMe")

    search_parameters_input["filter_options"] = filter_options
    return search_parameters_input


def _get_custom_fields(db: Session):
    query = sa.select(models.CustomField).where(models.CustomField.archived == sa.false())
    return domain.custom_field.get_multi(
        db=db,
        query=query,
        sort_options=None,
        range_options=None,
        filter_options=None,
    )


# Get one callbackRequest by id or uuid
@r.get(
    "/callbackRequests/{id}",
    response_model=schemas.CallbackRequestDetailsWithCustomFields(None) | Any,  # type: ignore # noqa: E501
    responses={404: {"description": "Callback request not found"}},
    dependencies=[Depends(get_user_scopes)],
)
def get_callback_request(
    id: UUID | str,
    db=Depends(get_db),
    current_user: AccessUser = Depends(get_current_user),
):

    custom_fields = _get_custom_fields(db=db)

    "Retrieve a single callbackrequest, given its unique identifier"
    id_type = IdType.uuid if isinstance(id, UUID) else IdType.id
    callback_request = domain.callbackRequest.get_callback_request(
        db=db,
        org_uuid=current_user.organization_uuid,
        custom_fields=custom_fields.result,
        id=GenericId(id=id, type=id_type),
    )

    return schemas.CallbackRequestDetailsWithCustomFields(current_user.organization_uuid).from_orm(
        callback_request
    )


# Get a list of callbackRequests.
@r.get(
    "/callbackRequests",
    response_model=list[Any | schemas.CallbackRequestWithCustomFields(None)],  # type: ignore # noqa: E501
    dependencies=[Depends(get_user_scopes)],
)
def get_callback_requests(
    response: Response,
    db=Depends(get_db),
    current_user: AccessUser = Depends(get_current_user),
    search_parameters_input: dict = Depends(validate_search_parameters()),
):

    custom_fields = _get_custom_fields(db=db)

    handled_search_parameters = _handle_filter_by_me(search_parameters_input, current_user.user_id)
    search_result = domain.callbackRequest.get_callbackRequests(
        db=db,
        custom_fields=custom_fields.result,
        **handled_search_parameters,
    )

    response.headers["Content-Range"] = search_result.as_content_range("results")

    return_val: list[schemas.CallbackRequestWithCustomFields(current_user.organization_uuid)] = []  # type: ignore # noqa: E501
    for row in search_result.result:
        return_val.append(
            schemas.CallbackRequestWithCustomFields(current_user.organization_uuid).from_orm(row)
        )

    return return_val


# Add a new callbackRequest.
@r.post(
    "/callbackRequests",
    response_model=schemas.CallbackRequestDetails,
    responses={409: {"description": "Conflict. callback not added."}},
    dependencies=[Depends(get_user_scopes)],
)
def create_callback_request(
    callback_request_create_request: schemas.CallbackRequestCreateRequest,
    db=Depends(get_db),
    current_user: AccessUser = Depends(get_current_user),
):
    callback_request = domain.callbackRequest.create_callback_request(
        db=db,
        org_uuid=current_user.organization_uuid,
        current_user=current_user,
        callback_request_create_request=callback_request_create_request,
    )
    return callback_request


# Update a callbackRequest.
@r.put(
    "/callbackRequests/{id}",
    response_model=schemas.CallbackRequestDetails,
    responses={
        404: {"description": "Callback not found"},
        409: {"description": "Conflict. Callback not updated."},
    },
    dependencies=[Depends(get_user_scopes)],
)
@r.patch(
    "/callbackRequests/{id}",
    response_model=schemas.CallbackRequestDetails,
    responses={
        404: {"description": "Callback not found"},
        409: {"description": "Conflict. Callback not updated."},
    },
    dependencies=[Depends(get_user_scopes)],
)
def update_callback_request(
    id: UUID | str,
    callback_request_update_request: schemas.CallbackRequestUpdateRequest,
    db=Depends(get_db),
    current_user: AccessUser = Depends(get_current_user),
):

    "Update the callbackRequest"
    id_type = IdType.uuid if isinstance(id, UUID) else IdType.id
    updated = domain.callbackRequest.update_callback_request(
        db=db,
        org_uuid=current_user.organization_uuid,
        id=GenericId(id=id, type=id_type),
        callback_request_update_request=callback_request_update_request,
    )

    return updated


# Delete a callbackRequest.
@r.delete(
    "/callbackRequests/{id}",
    response_model=schemas.CallbackRequestDetails,
    responses={
        404: {"description": "CallbackRequest not found"},
        409: {"description": "Conflict. CallbackRequest not deleted."},
    },
    dependencies=[Depends(get_user_scopes)],
)
def delete_callback_request(
    id: UUID | str,
    db=Depends(get_db),
):
    "Delete CallbackRequest"
    id_type = IdType.uuid if isinstance(id, UUID) else IdType.id
    callback_request = domain.callbackRequest.delete(db=db, id=GenericId(id=id, type=id_type))

    return callback_request
