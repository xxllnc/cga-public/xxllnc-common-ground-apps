from app import schemas
from app.db import models
from exxellence_shared_cga.domain.base import CRUDBase


class CRUDResult(CRUDBase[models.Result, schemas.ResultBase, schemas.ResultBase]):
    pass


result = CRUDResult(models.Result, models)
