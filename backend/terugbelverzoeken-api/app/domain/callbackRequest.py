import sqlalchemy as sa
from app import schemas
from app.db import models
from app.domain.create_base_query_with_custom_fields import create_base_query_with_custom_fields
from exxellence_shared_cga.core.types import (
    FilterOptions,
    GenericId,
    IdType,
    RangeOptions,
    SearchResult,
    SemanticError,
    SemanticErrorType,
    SortOptions,
)
from exxellence_shared_cga.domain.base import CRUDBase
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session
from typing import cast


class CRUDCallbackRequest(
    CRUDBase[
        models.CallbackRequest,
        schemas.CallbackRequestBase,
        schemas.CallbackRequestBase,
    ]
):
    def get_callback_request(
        self,
        db: Session,
        org_uuid: str,
        custom_fields: list[schemas.CustomField],
        id: GenericId,
    ) -> schemas.CallbackRequestDetails:

        # First get the details of the CallbackRequest
        query, query_filter = create_base_query_with_custom_fields(custom_fields, None)
        if id.type == IdType.uuid:
            query_filter["uuid"] = id.id
        else:
            query_filter["id"] = id.id

        callback_requests = self.get_multi(
            db=db,
            query=query,
            sort_options=None,
            range_options=None,
            filter_options=FilterOptions(options=query_filter),
            return_scalars=False,
        )

        if callback_requests.total <= 0:
            raise SemanticError(
                loc={"source": "path", "field": "id"},
                msg="Record not found",
                type=SemanticErrorType.not_found,
            )

        callback_request = callback_requests.result[0]

        return self.getCustomFieldsWithValues(db, org_uuid, callback_request.sid, callback_request)

    def get_callbackRequests(
        self,
        db: Session,
        custom_fields: list[schemas.CustomField],
        sort_options: SortOptions | None,
        range_options: RangeOptions | None,
        filter_options: FilterOptions | None,
    ) -> SearchResult:

        query, query_filter = create_base_query_with_custom_fields(custom_fields, filter_options)
        return self.get_multi(
            db=db,
            query=query,
            sort_options=sort_options,
            range_options=range_options,
            filter_options=FilterOptions(options=query_filter),
            return_scalars=False,
        )

    def createOrUpdateCustomFieldValues(
        self,
        db: Session,
        callback_request_id: int,
        custom_fields: list[schemas.CustomFieldsWithValue] | None,
    ):

        if custom_fields:
            for custom_field in custom_fields:
                query = sa.select(models.CustomFieldValues).filter(
                    sa.and_(
                        models.CustomFieldValues.callback_request_id == callback_request_id,
                        models.CustomFieldValues.custom_field_id == custom_field.sid,
                    )
                )
                custom_field_value = db.execute(query).scalars().first()

                if not custom_field_value:
                    custom_field_value = models.CustomFieldValues(
                        callback_request_id=callback_request_id,
                        custom_field_id=custom_field.sid,
                        value=custom_field.value,
                    )

                else:
                    custom_field_value.value = custom_field.value

                db.add(custom_field_value)
            db.commit()

    def create_callback_request(
        self,
        db: Session,
        org_uuid,
        current_user,
        callback_request_create_request: schemas.CallbackRequestCreateRequest,
    ):

        # 1 Create the callbackRequest
        callback_request_create = schemas.CallbackRequestCreate.parse_obj(
            callback_request_create_request.dict()
        )

        callback_request_create.created_by_id = current_user.user_id
        callback_request_create.created_by_name = current_user.user_name
        callback_request_fields = callback_request_create.dict(exclude_unset=True)

        db_callback_request = models.CallbackRequest(**callback_request_fields)

        try:
            db.add(db_callback_request)
            db.flush()  # generate uuid used in audit log
            db.commit()
        except IntegrityError as e:
            raise SemanticError(
                loc={"source": "body", "field": "name"},
                msg="Callback request with that name already exists " + "in given time frame",
                type=SemanticErrorType.not_unique,
            ) from e
        db.refresh(db_callback_request)

        # 2 Insert the custom_field values
        sid = cast(int, db_callback_request.sid)
        self.createOrUpdateCustomFieldValues(db, sid, callback_request_create_request.customFields)

        return self.getCustomFieldsWithValues(db, org_uuid, sid, db_callback_request)

    def getCustomFieldsWithValues(
        self,
        db: Session,
        org_uuid: str,
        id: int,
        callback_request: models.CallbackRequest,
    ) -> schemas.CallbackRequestDetails:
        # Then get the custom fields with the values for the selected
        # CallbackRequest
        custom_fields_with_values = (
            sa.select(
                models.CustomField.sid,
                models.CustomField.name,
                models.CustomField.description,
                models.CustomField.required,
                models.CustomField.type,
                models.CustomField.options,
                models.CustomField.sort_order,
                models.CustomFieldValues.value,
            )
            .outerjoin(
                models.CustomFieldValues,
                sa.and_(
                    models.CustomField.sid == models.CustomFieldValues.custom_field_id,
                    models.CustomFieldValues.callback_request_id == id,
                ),
            )
            .where(models.CustomField.archived == sa.false())
        )

        result = db.execute(custom_fields_with_values)

        #  Convert the orm to a pydantic dict
        custom_fields = []
        for row in result:
            custom_fields.append(schemas.CustomFieldsWithValue.from_orm(row))

        # Combine the details and the custom_fields to one object
        return_value = schemas.CallbackRequestDetailsWithCustomFields(org_uuid).from_orm(
            callback_request
        )  # noqa: E501
        # custom functions to get sortorder info

        def get_sortorder(customfield):
            return customfield.sort_order

        custom_fields.sort(key=get_sortorder)
        return_value.customFields = custom_fields

        return return_value

    def update_callback_request(
        self,
        db: Session,
        org_uuid: str,
        id: GenericId,
        callback_request_update_request: schemas.CallbackRequestUpdateRequest,
    ):

        # 1 get the callbackrequest from the db and update it
        db_callback_request = self.get(db=db, id=id)
        if db_callback_request is None:
            raise SemanticError(
                loc={"source": "path", "field": "id"},
                msg="Record not found",
                type=SemanticErrorType.not_found,
            )

        callback_request = schemas.CallbackRequestUpdate.parse_obj(
            callback_request_update_request.dict()
        )

        update_data = callback_request.dict(exclude_unset=True)

        for key, value in update_data.items():
            setattr(db_callback_request, key, value)

        try:
            db.add(db_callback_request)
            db.commit()
        except IntegrityError as e:
            raise SemanticError(
                loc={"source": "body", "field": "name"},
                msg="Unable to update CallbackRequest",
                type=SemanticErrorType.not_unique,
            ) from e
        # Is deze direct nodig?
        db.refresh(db_callback_request)

        # 2 Update or insert the custom_field values
        sid = cast(int, db_callback_request.sid)
        self.createOrUpdateCustomFieldValues(
            db,
            sid,
            callback_request_update_request.customFields,
        )

        return self.getCustomFieldsWithValues(db, org_uuid, sid, db_callback_request)


callbackRequest = CRUDCallbackRequest(models.CallbackRequest, models)
