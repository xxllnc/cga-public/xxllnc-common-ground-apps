from app import schemas
from app.db import models
from exxellence_shared_cga.domain.base import CRUDBase


class CRUDEmployee(CRUDBase[models.Employee, schemas.EmployeeBase, schemas.EmployeeBase]):
    pass


employee = CRUDEmployee(models.Employee, models)
