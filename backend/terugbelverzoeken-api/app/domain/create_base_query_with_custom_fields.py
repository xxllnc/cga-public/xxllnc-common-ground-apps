import sqlalchemy as sa
from app import schemas
from app.db import models
from copy import copy
from exxellence_shared_cga.core.types import FilterOptions
from typing import Any, Tuple


def create_base_query_with_custom_fields(
    custom_fields: list[schemas.CustomField],
    filter_options: FilterOptions | None,
) -> Tuple[sa.sql.expression.Select, dict]:
    # Basic select fields
    select_fields: list[Any] = [
        models.CallbackRequest.sid,
        models.CallbackRequest.id,
        models.CallbackRequest.uuid,
        models.CallbackRequest.assigned_to_name,
        models.CallbackRequest.assigned_to_id,
        models.CallbackRequest.contact,
        models.CallbackRequest.created_by_name,
        models.CallbackRequest.subject,
        models.CallbackRequest.summary,
        models.CallbackRequest.phone,
        models.CallbackRequest.status,
        models.CallbackRequest.result_id,
        models.CallbackRequest.created_at,
        models.CallbackRequest.updated_at,
    ]

    # An array of fieldnames to check if filter/sort is on customField
    field_names: list[str] = []
    # Loop trough custom fields and add them as subquery to select_fields
    for field in custom_fields:
        field_names.append(field.name)
        select_fields.append(
            sa.select(models.CustomFieldValues.value)
            .where(
                sa.and_(
                    models.CustomFieldValues.custom_field_id == field.sid,
                    models.CustomFieldValues.callback_request_id == models.CallbackRequest.sid,
                )
            )
            .scalar_subquery()
            .correlate(models.CallbackRequest)
            .label(field.name)
        )

    query = sa.select(*select_fields).outerjoin(models.CustomFieldValues)

    # Apply filter for custom fields
    # First create a new filter dict and
    # then loop trough all the filter options
    query_filter: dict = {}
    if filter_options:

        # When fulltext search also join Rusult so it is possible to search on the result
        if "q" in filter_options.options:
            query = query.outerjoin(models.Result)

        options = copy(filter_options.options)

        # Loop trough all the fields and
        # check if filter matches custom field
        for key, value in options.items():
            if key in field_names:
                # Filter matches a custom field so join customFieldValues
                # And add the field id and value to the query
                index = field_names.index(key)
                query = query.filter(
                    sa.and_(
                        models.CustomFieldValues.custom_field_id == custom_fields[index].sid,
                        models.CustomFieldValues.value == value,
                    )
                )
            # Filter doesn't match custom field
            # so add it to default filters
            else:
                query_filter[key] = value

    query = query.distinct()
    return query, query_filter
