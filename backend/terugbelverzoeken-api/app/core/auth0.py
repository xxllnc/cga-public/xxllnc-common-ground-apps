from app.core import config
from fastapi.param_functions import Depends
from fastapi_cloudauth.auth0 import Auth0  # type: ignore
from pydantic.fields import Field
from pydantic.main import BaseModel


class AccessUser(BaseModel):
    user_id: str = Field(alias="sub")
    user_name: str = Field(alias="https://xxllnc.nl/name")
    scope: str
    org_id: str
    organization_uuid: str = Field(alias="https://xxllnc.nl/organization_uuid")


auth = Auth0(domain=config.DOMAIN, customAPI=config.OIDC_CLIENT_ID, scope_key="scope")


def get_current_user(
    current_user: AccessUser = Depends(auth.claim(AccessUser)),
):
    return current_user


def get_user_scopes(
    scopes=Depends(auth.scope(["cga.user"])),
):
    return scopes


def get_admin_scopes(
    scopes=Depends(auth.scope(["cga.admin"])),
):
    return scopes
