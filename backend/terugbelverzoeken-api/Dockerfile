FROM python:3.11-slim

ARG UNAME=callbackrequests
ARG UID=1000
ARG GID=1000

RUN groupadd --gid $GID $UNAME \
  && useradd --create-home --home-dir /app \
  --uid $UID --gid $GID \
  --shell /bin/bash $UNAME
WORKDIR /app

RUN apt-get update && \
  apt-get install -y postgresql-client && \
  apt-get install -y git && \
  apt-get install -y curl && \
  apt-get install -y build-essential && \
  apt-get install -y python-dev && \
  apt-get install -y cmake

# Install Poetry
RUN curl -sSL https://install.python-poetry.org | POETRY_HOME=/opt/poetry python && \
  cd /usr/local/bin && \
  ln -s /opt/poetry/bin/poetry && \
  poetry config virtualenvs.create false

# Copy poetry.lock* in case it doesn't exist in the repo
COPY pyproject.toml poetry.lock* ./

# Allow installing dev dependencies to run tests
ARG INSTALL_DEV=false
RUN bash -c "if [ $INSTALL_DEV == 'true' ] ; then poetry install --no-root ; else poetry install --no-root --without dev ; fi"

COPY . .

ENV PYTHONDONTWRITEBYTECODE=1

USER $UID