from exxellence_shared_cga.core.types import SemanticError, SemanticErrorType
from typing import Optional

"""Module for the API error messages"""


def not_found_error(semantic_error: Optional[SemanticError], message: str):
    """Raise an SemanticError with http status 404 (not found)"""
    raise SemanticError(
        loc={"source": "", "field": ""},
        msg=message,
        type=SemanticErrorType.not_found,
    )


def not_unique_error(semantic_error: SemanticError, message: str):
    """Raise an SemanticError with http status 409 (conflict)"""
    raise SemanticError(
        loc={"source": "", "field": ""},
        msg=message,
        type=SemanticErrorType.not_unique,
    )
