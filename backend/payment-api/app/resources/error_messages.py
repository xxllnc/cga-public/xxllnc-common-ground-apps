from typing import Final

"""Module for the API error messages"""

# Common message
BAD_REQUEST: Final = "Bad Request"
CONFLICT: Final = "Conflict"
NOT_FOUND: Final = "Not Found"
INTERNAL_SERVER_ERROR: Final = "Internal Server Error"

# Payment messages

PAYMENT_NOT_CREATED: Final = "Betaling is niet aangemaakt. Er ging iets mis."

# Payment state messages

PAYMENT_STATE_NOT_CREATED: Final = "Betalings status is niet aangemaakt. Er ging iets mis."
