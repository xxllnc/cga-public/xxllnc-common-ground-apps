from app import domain, schemas
from app.core.auth0 import get_admin_scopes
from app.db.session import get_db
from app.resources import error_messages, errors
from datetime import date
from exxellence_shared_cga.core.rest_helpers import SearchParameters
from exxellence_shared_cga.core.types import SemanticError
from fastapi import APIRouter, Depends, Response, status
from sqlalchemy.orm import Session

router = r = APIRouter()


def validate_search_parameters():
    return SearchParameters(
        filter_options_schema=schemas.PaymentStateFilterableFields,  # type: ignore # noqa: E501
        sort_options_schema=schemas.PaymentStateSortableFields,  # type: ignore # noqa: E501
    )


# Get a list of payment states.
@r.get(
    "/paymentStates",
    response_model=list[schemas.PaymentState],
    dependencies=[Depends(get_admin_scopes)],
)
def get_paymentstate(
    response: Response,
    database: Session = Depends(get_db),
    search_parameters: dict = Depends(validate_search_parameters()),
):

    search_result = domain.payment_states.get_multi(db=database, query=None, **search_parameters)
    response.headers["Content-Range"] = search_result.as_content_range("results")

    return search_result.result


@r.post(
    "/paymentStates",
    response_model=schemas.PaymentState,
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_409_CONFLICT: {"description": "Conflict. Payment state not created."},
    },
    dependencies=[Depends(get_admin_scopes)],
)
def create_paymentstate(
    payment_state_create_request: schemas.PaymentStateCreate,
    database: Session = Depends(get_db),
):

    try:
        payment_state = domain.payment_states.create(
            db=database,
            obj_in=schemas.PaymentStateCreate(
                payment_id=payment_state_create_request.payment_id,
                code=payment_state_create_request.code,
                message=payment_state_create_request.message,
                set_date=date.today(),
            ),
        )
    except SemanticError as semantic_error:
        errors.not_unique_error(semantic_error, error_messages.PAYMENT_STATE_NOT_CREATED)

    return payment_state
