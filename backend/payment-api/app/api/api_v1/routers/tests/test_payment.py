from . import HEADERS
from app.api.api_v1.routers.tests.test_mock_data_payments import add_mock_data_to_db
from app.core.config import BASE_PATH_V1
from fastapi import status as http_status
from fastapi.testclient import TestClient
from requests.models import Response
from sqlalchemy.orm import Session
from typing import Final

# The api endpoint
PAYMENTS: Final = f"{BASE_PATH_V1}/payments"


def assert_payment_created(response: Response, body: dict):
    assert response.status_code == http_status.HTTP_201_CREATED
    res = response.json()
    print(res)
    assert len(res) == 7
    assert res["id"] is not None
    assert res["uuid"] is not None
    assert res["orderNumber"] == body["order_number"]
    assert res["amount"] == body["amount"]
    assert res["currency"] == body["currency"]
    assert res["description"] == body["description"]
    assert res["redirectUrl"] is not None

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == "application/json"
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Testing an API with POST requests
# ---------------------------------------------
def test_post(client: TestClient, test_db: Session):
    add_mock_data_to_db(test_db)

    body = {
        "order_number": "t4",
        "amount": "12",
        "currency": "eur",
        "description": "pay for order x",
        "accept_url": "https://exxellence.vcap.me/payments/api/docs",
        "decline_url": "string",
        "exception_url": "string",
        "cancel_url": "string",
    }

    response = client.post(PAYMENTS, json=body, headers=HEADERS)
    assert_payment_created(response, body)
