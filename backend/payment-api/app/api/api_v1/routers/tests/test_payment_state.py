from . import HEADERS
from app.api.api_v1.routers.tests.test_mock_data_payments import add_mock_data_to_db
from app.core.config import BASE_PATH_V1
from fastapi import status as http_status
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session
from typing import Final

# The api endpoint
PAYMENT_STATES: Final = f"{BASE_PATH_V1}/paymentStates"


# ---------------------------------------------
# Testing an API with GET requests
# Test get all the payment states
# ---------------------------------------------
def test_get_payment_states_list(client: TestClient, test_db: Session):
    add_mock_data_to_db(test_db)

    response = client.get(PAYMENT_STATES, headers=HEADERS)

    assert response.status_code == http_status.HTTP_200_OK
    headers = response.headers
    assert len(headers) == 3
    assert headers["content-type"] == "application/json"
    assert headers["content-range"] == "results 0-0/1"

    res = response.json()
    assert len(res) == 1


# ---------------------------------------------
# Testing an API with POST requests
# ---------------------------------------------
def test_post(client: TestClient, test_db: Session):
    mock = add_mock_data_to_db(test_db)

    body = {"message": "test_message", "code": "test_code", "payment_id": mock.payment.sid}

    response = client.post(PAYMENT_STATES, json=body, headers=HEADERS)
    assert response.status_code == http_status.HTTP_201_CREATED
