#!/usr/bin/env python3

import datetime
from app.core import constant
from app.db import models
from collections import namedtuple
from sqlalchemy.orm import Session

# Declaring namedtuple() as return value
MockData = namedtuple(
    "MockData",
    ["payment", "payment_state"],
)


#  Same data als initial_data.
def add_mock_data_to_db(db: Session) -> MockData:

    payment = models.Payment(
        order_number="b4", amount="121", currency="eur", description="order 1 of 1,00"
    )

    db.add(payment)
    db.flush()

    payment_state = models.PaymentState(
        code="urlAfsluitenKnop",
        message="De standaard url die wordt gebruikt voor de "
        "afsluitknop van het formulier. Deze waarde kan in het formulier "
        "worden overschreven door in het formulier een instelling "
        "urlAfsluitenKnop toe te voegen",
        set_date=datetime.datetime(2021, 5, 11, 00, 1, 00),
        payment_id=payment.sid,
    )

    db.add(payment_state)

    db.flush()

    setting1 = models.Setting(
        name=constant.SETTING_MERCHANT_ID,
        value="demo",
        description="De identificatie van de handelaar van de ogone account. "
        "Dit is account naam waarmee je op de ogone website inlogd",
    )
    setting2 = models.Setting(
        name=constant.SETTING_SHA_IN,
        value="secret_in",
        description="Dit is de sleutel waarmee de berichten naar ogone worden versleuteld.",
    )
    setting3 = models.Setting(
        name=constant.SETTING_SHA_OUT,
        value="secret_out",
        description="Dit is de sleutel waarmee de berichten vanuit ogone worden versleuteld.",
    )
    setting4 = models.Setting(
        name=constant.SETTING_URL,
        value="https://secure.ogone.com/ncol/test/orderstandard_utf8.asp?",
        description="De url voor aanroepen naar ogone.",
    )

    db.add(setting1)
    db.add(setting2)
    db.add(setting3)
    db.add(setting4)

    db.flush()

    return MockData(payment, payment_state)
