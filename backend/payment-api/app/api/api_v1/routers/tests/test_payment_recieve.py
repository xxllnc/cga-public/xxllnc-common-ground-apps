from . import HEADERS
from app.api.api_v1.routers.tests.test_mock_data_payments import add_mock_data_to_db
from app.core.config import BASE_PATH_V1
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session
from typing import Final

# The api endpoint
PAYMENT_RECIEVE: Final = f"{BASE_PATH_V1}/PaymentRecieve"


# ---------------------------------------------
# Testing an API with POST requests
# ---------------------------------------------
def test_post(client: TestClient, test_db: Session):
    add_mock_data_to_db(test_db)

    url = f"{PAYMENT_RECIEVE}?amount=121&orderID=b4&SHASIGN=97E765A42AA349F10AFD2BDCFB41789FB929E127E01C175529B4189526A55DC8C9F5228F1D894B8CAD5A55947A8F4588B6F19E76671699E40290D692648F8A89"  # noqa: E501
    response = client.get(url, headers=HEADERS)

    assert response.content == b'"found"'
