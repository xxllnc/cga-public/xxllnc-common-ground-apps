import hashlib
import uuid as py_uuid
from app import domain, schemas
from app.core import constant
from app.db.session import get_db
from app.resources import error_messages, errors
from datetime import date
from exxellence_shared_cga.core.types import SemanticError
from fastapi import APIRouter, Depends
from fastapi import status as http_status
from sqlalchemy.orm import Session
from typing import cast
from urllib import parse

router = r = APIRouter()


def _build_redirect_url(payment: schemas.PaymentIn, database: Session) -> str:
    result: str = domain.settings.get_setting_value(constant.SETTING_URL, database)

    result = _appendParameter(result, constant.PAYMENT_OGONE_AMOUNT, payment.amount, True)
    result = _appendParameter(result, constant.PAYMENT_OGONE_CURRENCY, payment.currency, True)
    result = _appendParameter(result, constant.PAYMENT_OGONE_COM, payment.description, True)

    if payment.accept_url is not None:
        result = _appendParameter(
            result, constant.PAYMENT_OGONE_ACCEPT_URL, payment.accept_url, True
        )
    if payment.cancel_url is not None:
        result = _appendParameter(
            result, constant.PAYMENT_OGONE_CANCEL_URL, payment.cancel_url, True
        )
    if payment.decline_url is not None:
        result = _appendParameter(
            result, constant.PAYMENT_OGONE_DECLINE_URL, payment.decline_url, True
        )
    if payment.exception_url is not None:
        result = _appendParameter(
            result, constant.PAYMENT_OGONE_EXCEPTION_URL, payment.exception_url, True
        )

    result = _appendParameter(
        result, constant.PAYMENT_OGONE_LANGUAGE, constant.PAYMENT_OGONE_LANGUAGE_NL, False
    )

    result = _appendParameter(
        result,
        constant.PAYMENT_OGONE_PSPID,
        domain.settings.get_setting_value(constant.SETTING_MERCHANT_ID, database),
        False,
    )

    result = _appendParameter(
        result, constant.PAYMENT_OGONE_RL, constant.PAYMENT_OGONE_RL_NCOL_2_0, False
    )
    result = _appendParameter(result, constant.PAYMENT_OGONE_ORDER_ID, payment.order_number, False)

    result = _appendParameter(
        result, constant.PAYMENT_OGONE_SHASIGN, _build_sha(payment, database), False
    )

    return result


def _build_sha(payment: schemas.PaymentIn, database: Session) -> str:
    signature: str = domain.settings.get_setting_value(constant.SETTING_SHA_IN, database)

    m = hashlib.sha512()
    if payment.accept_url is not None:
        value: str = "ACCEPTURL=" + payment.accept_url + signature
        m.update(value.encode("UTF-8"))
    value = "AMOUNT=" + payment.amount + signature
    m.update(value.encode("UTF-8"))
    if payment.cancel_url is not None:
        value = "CANCELURL=" + payment.cancel_url + signature
        m.update(value.encode("UTF-8"))
    value = "COM=" + payment.description + signature
    m.update(value.encode("UTF-8"))
    value = "CURRENCY=" + payment.currency + signature
    m.update(value.encode("UTF-8"))

    if payment.decline_url is not None:
        value = "DECLINEURL=" + payment.decline_url + signature
        m.update(value.encode("UTF-8"))
    if payment.exception_url is not None:
        value = "EXCEPTIONURL=" + payment.exception_url + signature
        m.update(value.encode("UTF-8"))
    value = "LANGUAGE=nl_NL" + signature
    m.update(value.encode("UTF-8"))
    value = "ORDERID=" + payment.order_number + signature
    m.update(value.encode("UTF-8"))
    value = (
        "PSPID="
        + domain.settings.get_setting_value(constant.SETTING_MERCHANT_ID, database)
        + signature
    )
    m.update(value.encode("UTF-8"))

    return m.hexdigest().upper()


def _appendParameter(
    current_url: str, parameter_name: str, parameter_value: str, encode: bool
) -> str:
    if current_url.endswith("?"):
        seperator: str = ""
    else:
        seperator = "&"

    if encode:
        return "".join(
            [
                current_url,
                seperator,
                parameter_name,
                "=",
                parse.quote_plus(parameter_value),
            ]
        )
    else:
        return "".join([current_url, seperator, parameter_name, "=", parameter_value])


@r.post(
    "/payments",
    response_model=schemas.Payment,
    status_code=http_status.HTTP_201_CREATED,
    responses={
        http_status.HTTP_409_CONFLICT: {"description": "Conflict. Payment not created."},
    },
)
def create_payment(
    payment_create_request: schemas.PaymentIn,
    database: Session = Depends(get_db),
):

    try:
        payment = domain.payments.create(
            db=database,
            obj_in=schemas.PaymentCreate(
                order_number=payment_create_request.order_number,
                amount=payment_create_request.amount,
                currency=payment_create_request.currency,
                description=payment_create_request.description,
            ),
        )
    except SemanticError as semantic_error:
        errors.not_unique_error(semantic_error, error_messages.PAYMENT_NOT_CREATED)

    try:
        domain.payment_states.create(
            db=database,
            obj_in=schemas.PaymentStateCreate(
                payment_id=cast(int, payment.sid),
                code="Created",
                message="Payment redirect url has been created",
                set_date=date.today(),
            ),
        )
    except SemanticError as semantic_error:
        errors.not_unique_error(semantic_error, error_messages.PAYMENT_NOT_CREATED)

    redirect_url: str = _build_redirect_url(payment_create_request, database)

    response: schemas.Payment = schemas.Payment(
        order_number=cast(str, payment.order_number),
        amount=cast(str, payment.amount),
        currency=cast(str, payment.currency),
        description=cast(str, payment.description),
        uuid=cast(py_uuid.UUID, payment.uuid),
        id=cast(str, payment.id),
        redirect_url=cast(str, redirect_url),
    )

    return response
