import hashlib
from app import domain, schemas
from app.core.constant import SETTING_SHA_OUT
from app.db.session import get_db
from datetime import date
from exxellence_shared_cga.core.types import SemanticError, SemanticErrorType
from fastapi import APIRouter, Depends, Header, Request, status
from sqlalchemy.orm import Session
from starlette.datastructures import QueryParams

router = r = APIRouter()


def _check_sha(value: str, sha: str):

    hash = hashlib.sha512()
    hash.update(value.encode("UTF-8"))
    if hash.hexdigest().upper() != sha:

        raise SemanticError(
            loc={"source": "path", "field": "id"},
            msg="SHA does not match given parameters",
            type=SemanticErrorType.invalid_input,
        )


@r.get(
    "/PaymentRecieve",
    response_model=None,
    responses={status.HTTP_404_NOT_FOUND: {"description": "payment not found"}},
)
def process_payment(
    amount: str,
    orderID: str,
    SHASIGN: str,
    request: Request,
    database: Session = Depends(get_db),
    organization_url: str = Header(...),
):

    parms: QueryParams = request.query_params
    sha: str = SHASIGN
    order: str = orderID
    amount_value: str = amount
    status: str = ""
    sha_out: str = domain.settings.get_setting_value(SETTING_SHA_OUT, database)
    sha_target: str = ""
    message: str = ""

    for parm in sorted(parms.keys(), key=lambda i: i[0].lower()):
        message = f"{message}&{parm.upper()}={parms.get(parm)}"
        if parm == "STATUS":
            status = parms.get(parm, "unknown")
        if parm != "SHASIGN":
            sha_target = f"{sha_target}{parm.upper()}={parms.get(parm)}{sha_out}"

    _check_sha(sha_target, sha)
    payment = domain.payments.get_payment(order, amount_value, database)

    if payment is not None:

        domain.payment_states.create(
            db=database,
            obj_in=schemas.PaymentStateCreate(
                payment_id=payment.sid,
                code=status,
                message=message,
                set_date=date.today(),
            ),
        )
        return "found"

    else:
        return "not found"
