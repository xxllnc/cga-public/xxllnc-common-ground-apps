import uuid
from app import domain, schemas
from app.core.auth0 import get_admin_scopes, get_user_scopes
from app.db.session import get_db
from exxellence_shared_cga.core.rest_helpers import SearchParameters
from exxellence_shared_cga.core.types import GenericId, IdType
from fastapi import APIRouter, Depends, Response
from sqlalchemy.orm import Session
from typing import Union

router = r = APIRouter()


def validate_search_parameters():
    return SearchParameters(
        filter_options_schema=schemas.SettingFilterableFields,  # type: ignore # noqa: E501
        sort_options_schema=schemas.SettingSortableFields,  # type: ignore # noqa: E501
    )


@r.get(
    "/settings/{id}",
    response_model=schemas.Setting,
    responses={404: {"description": "setting not found"}},
    dependencies=[Depends(get_user_scopes)],
)
def get_setting(id: Union[uuid.UUID, str], db=Depends(get_db)):
    "Retrieve a single setting, given its unique id"
    id_type = IdType.uuid if isinstance(id, uuid.UUID) else IdType.id
    setting = domain.settings.get(db=db, id=GenericId(id=id, type=id_type))

    return setting


# Get a list of settings.
@r.get(
    "/settings",
    response_model=list[schemas.Setting],
    dependencies=[Depends(get_admin_scopes)],
)
def get_settings(
    response: Response,
    database: Session = Depends(get_db),
    search_parameters: dict = Depends(validate_search_parameters()),
):

    search_result = domain.settings.get_multi(db=database, query=None, **search_parameters)
    response.headers["Content-Range"] = search_result.as_content_range("results")

    return search_result.result


@r.put(
    "/settings/{id}",
    response_model=schemas.Setting,
    responses={
        404: {"description": "setting not found"},
        409: {"description": "Conflict. setting not updated."},
    },
    dependencies=[Depends(get_admin_scopes)],
)
def update_setting_type(
    id: Union[int, uuid.UUID],
    setting_type: schemas.SettingUpdate,
    db=Depends(get_db),
):
    "Update an setting"
    id_type = IdType.uuid if isinstance(id, uuid.UUID) else IdType.sid
    return domain.settings.update(
        db=db,
        id=GenericId(id=id, type=id_type),
        obj_in=setting_type,
    )
