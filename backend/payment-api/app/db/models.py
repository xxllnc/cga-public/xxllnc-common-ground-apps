import datetime
import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as pg
from .session import Base
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import DateTime

utcnow = sa.literal_column("TIMEZONE('utc', CURRENT_TIMESTAMP)")


class Payment(Base):
    __tablename__ = "payment"
    default_sort_field = "id"

    sid = sa.Column(sa.Integer, primary_key=True, index=True)
    id = sa.Column(
        sa.String,
        nullable=False,
        unique=True,
        index=True,
        server_default=sa.func.payment_generate_id(),
    )
    uuid = sa.Column(
        pg.UUID(as_uuid=True),
        nullable=False,
        unique=True,
        server_default=sa.func.gen_random_uuid(),
    )
    order_number = sa.Column(sa.String, unique=True)
    amount = sa.Column(sa.String)
    currency = sa.Column(sa.String)
    description = sa.Column(sa.String)


class PaymentState(Base):
    __tablename__ = "payment_state"
    default_sort_field = "sid"

    sid = sa.Column(sa.Integer, primary_key=True, index=True)
    uuid = sa.Column(
        pg.UUID(as_uuid=True),
        nullable=False,
        unique=True,
        server_default=sa.func.gen_random_uuid(),
    )
    payment_id = sa.Column(
        sa.Integer,
        sa.ForeignKey("payment.sid"),
        index=True,
        nullable=False,
    )
    payment = relationship("Payment")
    code = sa.Column(sa.String, index=True)
    message = sa.Column(sa.String)
    set_date = sa.Column(DateTime, default=datetime.datetime.utcnow)


class Setting(Base):
    __tablename__ = "setting"
    default_sort_field = "id"

    sid = sa.Column(sa.Integer, primary_key=True, index=True)
    id = sa.Column(
        sa.String,
        nullable=False,
        unique=True,
        index=True,
        server_default=sa.func.setting_generate_id(),
    )
    uuid = sa.Column(
        pg.UUID(as_uuid=True),
        nullable=False,
        unique=True,
        server_default=sa.func.gen_random_uuid(),
    )
    name = sa.Column(sa.String, index=True)
    value = sa.Column(sa.String)
    description = sa.Column(sa.String)
