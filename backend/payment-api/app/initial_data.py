#!/usr/bin/env python3

"""Module for creating initial data for the database"""  # pragma: no cover


from app.core import constant  # pragma: no cover
from app.db import models  # pragma: no cover
from app.db.session import SessionLocal
from sqlalchemy import text  # pragma: no cover
from sqlalchemy.orm import Session  # pragma: no cover


def truncate_tables() -> None:  # pragma: no cover
    """Truncate tables"""

    database: Session = SessionLocal()
    database.execute(text('truncate table "payment" cascade'))
    database.execute(text('truncate table "payment_state" cascade'))
    database.execute(text('truncate table "setting" cascade'))
    database.commit()


def restart_sequences_with_1() -> None:  # pragma: no cover
    """Restart the sequences with 1"""

    db: Session = SessionLocal()
    db.execute(text('ALTER SEQUENCE "payment_sid_seq" RESTART WITH 1'))
    db.execute(text('ALTER SEQUENCE "setting_sid_seq" RESTART WITH 1'))
    db.execute(text('ALTER SEQUENCE "payment_state_sid_seq" RESTART WITH 1'))
    db.commit()


def create_initial_data() -> None:  # pragma: no cover
    """Creates initial data for database"""

    database: Session = SessionLocal()

    setting1 = models.Setting(
        name=constant.SETTING_MERCHANT_ID,
        value="See passwordmanager",
        description="De identificatie van de handelaar van de ogone account. "
        "Dit is account naam waarmee je op de ogone website inlogd",
    )
    setting2 = models.Setting(
        name=constant.SETTING_SHA_IN,
        value="See passwordmanager",
        description="Dit is de sleutel waarmee de berichten naar ogone worden versleuteld.",
    )
    setting3 = models.Setting(
        name=constant.SETTING_SHA_OUT,
        value="See passwordmanager",
        description="Dit is de sleutel waarmee de berichten vanuit ogone worden versleuteld.",
    )
    setting4 = models.Setting(
        name=constant.SETTING_URL,
        value="https://secure.ogone.com/ncol/test/orderstandard_utf8.asp?",
        description="De url voor aanroepen naar ogone.",
    )

    database.add(setting1)
    database.add(setting2)
    database.add(setting3)
    database.add(setting4)

    database.flush()
    database.commit()


if __name__ == "__main__":  # pragma: no cover
    print("First remove any existing data")
    truncate_tables()
    print("Restart the sequences with 1")
    restart_sequences_with_1()
    print("Creating initial data")
    create_initial_data()
    print("Initial data created")
