from __future__ import annotations

import uuid as py_uuid
from app.schemas.generic_schemas import FilterableId
from app.schemas.to_optional import to_optional
from datetime import date
from exxellence_shared_cga.core.types import SortOrder
from pydantic import BaseModel, Field
from typing import Optional


# payment
class PaymentStateBase(BaseModel):
    payment_id: int = Field(..., title="the id of the payment the status is for")
    code: str = Field(..., title="The code of the status", Alias="orderNumber")
    message: Optional[str] = Field(None, title="Message of the status")
    set_date: Optional[date] = Field(None, title="Date the status was set")

    class Config:
        allow_population_by_field_name = True
        fields = {"payment_id": "paymentId", "set_date": "setDate"}


class PaymentState(PaymentStateBase):
    uuid: py_uuid.UUID = Field(..., title="The unique uuid of the payment")
    set_date: date = Field(..., title="the date the status was added")

    class Config:
        orm_mode = True
        allow_population_by_field_name = True
        extra = "forbid"


class PaymentStateCreate(PaymentStateBase):

    pass


class PaymentStateUpdate(to_optional(PaymentStateCreate)):  # type: ignore

    pass


PaymentStateUpdate.update_forward_refs()


class PaymentStateFilterableFields(FilterableId):
    code: Optional[str]
    message: Optional[str]
    set_date: Optional[str]
    q: Optional[str]

    class Config:
        extra = "forbid"


class PaymentStateSortableFields(BaseModel):
    sid: Optional[SortOrder] = Field(alias="id")
    code: Optional[SortOrder]
    message: Optional[SortOrder]
    set_date: Optional[SortOrder]

    class Config:
        allow_population_by_field_name = True
        extra = "forbid"
