from __future__ import annotations

import uuid as py_uuid
from app.schemas.generic_schemas import FilterableId
from app.schemas.to_optional import to_optional
from exxellence_shared_cga.core.types import SortOrder
from pydantic import BaseModel, Field
from typing import Optional


class SettingBase(BaseModel):

    name: str = Field(..., title="Name of the setting")
    value: Optional[str] = Field(None, title="Value of the setting")
    description: Optional[str] = Field(None, title="Description of the setting")

    class Config:
        allow_population_by_field_name = True


class Setting(SettingBase):
    uuid: py_uuid.UUID = Field(..., title="The uuid of the setting")

    class Config:
        orm_mode = True
        allow_population_by_field_name = True
        extra = "forbid"


class SettingCreate(SettingBase):

    pass


class SettingUpdate(to_optional(SettingCreate)):  # type: ignore

    pass


SettingUpdate.update_forward_refs()


class SettingFilterableFields(FilterableId):
    name: Optional[str]
    value: Optional[str]
    description: Optional[str]
    q: Optional[str]

    class Config:
        extra = "forbid"


class SettingSortableFields(BaseModel):
    name: Optional[SortOrder] = Field(alias="id")
    value: Optional[SortOrder]
    description: Optional[SortOrder]

    class Config:
        allow_population_by_field_name = True
        extra = "forbid"
