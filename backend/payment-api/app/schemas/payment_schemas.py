from __future__ import annotations

import uuid as py_uuid
from pydantic import BaseModel, Field
from typing import Optional


# Payment
class PaymentBase(BaseModel):
    order_number: str = Field(..., title="Order number of the payment")
    amount: str = Field(..., title="Amount of the payment")
    currency: str = Field(..., title="Currency of the payment")
    description: str = Field(..., title="Description of the payment")

    class Config:
        allow_population_by_field_name = True
        fields = {"order_number": "orderNumber"}


class PaymentIn(PaymentBase):
    accept_url: Optional[str] = Field(
        None, title="Url the user is send to in case of a successful payment transaction"
    )
    decline_url: Optional[str] = Field(
        None, title="Url the user is send to when payment transaction is declined"
    )
    exception_url: Optional[str] = Field(
        None,
        title="Url the user is send to when an exception happens during the payment transaction",
    )
    cancel_url: Optional[str] = Field(
        None, title="Url the user is send to when the payment transaction is cancelled"
    )

    class Config:
        extra = "forbid"
        allow_population_by_field_name = True
        fields = {
            "accept_url": "acceptUrl",
            "decline_url": "declineUrl",
            "exception_url": "exceptionUrl",
            "cancel_url": "cancelUrl",
        }


class PaymentCreate(PaymentBase):

    pass


class Payment(PaymentBase):
    id: str = Field(..., title="The unique id of the payment")
    uuid: py_uuid.UUID = Field(..., title="The uuid of the payment")
    redirect_url: str = Field(..., title="The url to redirect the user to")

    class Config:
        extra = "forbid"
        allow_population_by_field_name = True
        fields = {"redirect_url": "redirectUrl"}
