import sqlalchemy as sa
from app.db import models
from app.db.models import Payment
from app.schemas import payment_schemas
from exxellence_shared_cga.domain.base import CRUDBase
from sqlalchemy.orm import Session


class CRUDPayments(
    CRUDBase[
        models.Payment,
        payment_schemas.PaymentCreate,
        payment_schemas.Payment,
    ]
):
    """Class for payment CRUD calls"""

    def get_payment(self, order: str, amount: str, database: Session):
        select_statement = sa.select(self.model).where(
            sa.and_(Payment.order_number == order, Payment.amount == amount)
        )

        return database.execute(select_statement).scalars().first()


payments = CRUDPayments(models.Payment, models)
