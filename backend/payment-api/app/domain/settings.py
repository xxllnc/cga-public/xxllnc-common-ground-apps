import sqlalchemy as sa
from app import schemas
from app.db import models
from app.db.models import Setting
from exxellence_shared_cga.domain.base import CRUDBase
from sqlalchemy.orm import Session


class CRUDSetting(
    CRUDBase[
        models.Setting,
        schemas.SettingCreate,
        schemas.SettingUpdate,
    ]
):
    def get_setting_value(self, name: str, database: Session) -> str:
        select_statement = sa.select(self.model).where(Setting.name == name)

        setting = database.execute(select_statement).scalars().first()
        return setting.value


settings = CRUDSetting(models.Setting, models)
