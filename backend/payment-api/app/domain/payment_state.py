from app import schemas
from app.db import models
from exxellence_shared_cga.domain.base import CRUDBase


class CRUDPaymentState(
    CRUDBase[
        models.PaymentState,
        schemas.PaymentStateCreate,
        schemas.PaymentStateUpdate,
    ]
):
    pass


payment_states = CRUDPaymentState(models.PaymentState, models)
