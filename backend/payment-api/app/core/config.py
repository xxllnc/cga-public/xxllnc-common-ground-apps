import os
import sys
from dotenv import load_dotenv
from typing import Final, Union

if "pytest" in sys.modules:
    load_dotenv(".env.test")


def to_int(value: Union[str, None], default_value: int) -> int:
    try:
        return int(value) if value else default_value
    except ValueError:
        return default_value


# The title of the API
PROJECT_NAME: Final[str] = "xxllnc-payments"
BASE_PATH: Final[str] = "/payments/api"
BASE_PATH_V1: Final[str] = f"{BASE_PATH}/v1"
APP_ID: Final[str] = os.environ.get("APP_ID", "")
USE_APP_INSTANCE_ID: Final[bool] = os.environ.get("USE_APP_INSTANCE_ID", "False").upper() == "TRUE"

#############################################
# SQLAlchemy settings
#############################################
SQLALCHEMY_DATABASE_URI: Final[str] = os.environ.get(
    "DATABASE_URL",
    "postgresql://payments:payments123@localhost:5432/payments",
)


# If set to True, all database queries will be logged as they are executed.
SQLALCHEMY_ECHO: Final[bool] = os.environ.get("DATABASE_ECHO", "False").upper() == "TRUE"
SQLALCHEMY_POOL_SIZE: Final[int] = to_int(os.environ.get("SQLALCHEMY_POOL_SIZE", None), 5)
SQLALCHEMY_POOL_MAX_OVERFLOW: Final[int] = to_int(
    os.environ.get("SQLALCHEMY_POOL_MAX_OVERFLOW", None), 10
)

# Future: OIDC parameters should be looked up in some kind of table
# so we can handle customer logins using their own SSO solutions

# Auth0 Domain
DOMAIN = os.environ["DOMAIN"]

# OIDC client-id, used to verify if we're the audience of access tokens
OIDC_CLIENT_ID = os.environ["OIDC_CLIENT_ID"]

# Refresh OIDC every
JWK_REFRESH_INTERVAL = int(os.environ.get("JWK_REFRESH_INTERVAL", "300"))

# Maximum number of results from a search. To retrieve more, use paging.
MAX_PAGE_SIZE = int(os.environ.get("MAX_PAGE_SIZE", "100"))

# Flag to enable or disable audit logging
AUDIT_LOGGING = bool(os.environ.get("AUDIT_LOGGING", True))

#############################################
# Auth0 controlpanel token settings
#############################################
AUTH0_ECO_CLIENT_ID: Final[str] = os.environ.get("AUTH0_ECO_CLIENT_ID", "")
AUTH0_ECO_CLIENT_SECRET: Final[str] = os.environ.get("AUTH0_ECO_CLIENT_SECRET", "")
AUTH0_ECO_AUDIENCE: Final[str] = os.environ.get("AUTH0_ECO_AUDIENCE", "")
AUTH0_ECO_DOMAIN: Final[str] = os.environ.get("AUTH0_ECO_DOMAIN", "")

#############################################
# Auth0 redis server settings
#############################################
REDIS_HOST: Final[str] = os.environ.get("REDIS_HOST", "redis")
REDIS_PASSWORD: Final[str | None] = os.environ.get("REDIS_PASSWORD", None)

CONTROLPANEL_URL: Final[str] = os.environ.get("CONTROLPANEL_URL", "")
