"""This module contains all the constants for the payment-api"""

from typing import Final

# The constants for settings
SETTING_URL: Final = "url"
SETTING_SHA_OUT: Final = (
    "shaUit"  # Passphrase for the data returned by Ogone for order data validation
)
SETTING_SHA_IN: Final = "shaIn"  # Passphrase for the data sent to Ogone for order data validation
SETTING_MERCHANT_ID: Final = "handelaarId"  # The PSPID of your account with Ogone

# The constants for Ogone

# URL of the web page to display to the customer when the payment has been authorised, stored,
# accepted or is waiting to be accepted.
PAYMENT_OGONE_ACCEPT_URL: Final = "accepturl"

# URL of the web page to display to the customer when he cancels the payment.
PAYMENT_OGONE_CANCEL_URL: Final = "cancelurl"

# URL of the web page to display to the customer when the payment result is uncertain.
PAYMENT_OGONE_EXCEPTION_URL: Final = "exceptionurl"

# URL of the web page to show the customer when the acquirer declines the authorisation more
# than the maximum permissible number of times
PAYMENT_OGONE_DECLINE_URL: Final = "declineurl"

PAYMENT_OGONE_AMOUNT: Final = "amount"  # amount to be paid
PAYMENT_OGONE_CURRENCY: Final = "currency"  # ISO alpha order currency code
PAYMENT_OGONE_COM: Final = "com"  # order description
PAYMENT_OGONE_LANGUAGE: Final = "language"
PAYMENT_OGONE_LANGUAGE_NL: Final = "nl_NL"
PAYMENT_OGONE_PSPID: Final = "pspid"  # Name in Ogone
PAYMENT_OGONE_RL: Final = "RL"  # release version Ogone
PAYMENT_OGONE_RL_NCOL_2_0: Final = "ncol_2.0"
PAYMENT_OGONE_ORDER_ID: Final = "orderID"  # unique order number
PAYMENT_OGONE_SHASIGN: Final = "SHASign"  # Unique character string for order data validation
