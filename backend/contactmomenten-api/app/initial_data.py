#!/usr/bin/env python3

import random
from app.db import models
from app.db.session import SessionLocal
from datetime import datetime, timedelta
from sqlalchemy import text
from sqlalchemy.orm import Session

channel = ["Balie", "Telefoon", "Chat & E-mail", "Internet", "Post"]


def truncate_tables() -> None:

    db: Session = SessionLocal()

    db.execute(text('truncate table "contact_moment" cascade'))
    db.execute(text('truncate table "custom_field" cascade'))
    db.execute(text('truncate table "custom_field_values"  cascade'))

    db.commit()


def restart_sequences_with_1() -> None:

    db: Session = SessionLocal()

    db.execute(text('ALTER SEQUENCE "contact_moment_id_seq" RESTART WITH 1'))
    db.execute(text('ALTER SEQUENCE "contact_moment_sid_seq" RESTART WITH 1'))
    db.execute(text('ALTER SEQUENCE "custom_field_values_sid_seq" RESTART WITH 1'))
    db.execute(text('ALTER SEQUENCE "custom_field_sid_seq" RESTART WITH 1'))

    db.commit()


def create_initial_data() -> None:
    db: Session = SessionLocal()

    kanaal = models.CustomField(
        name="Kanaal",
        description="Vul het kanaal in",
        required=False,
        type=models.CustomFieldTypes.text,
        sort_order=1,
    )
    db.add(kanaal)

    telno = models.CustomField(
        name="Telefoonnummer",
        description="Vul een telefoonnummer in",
        required=False,
        type=models.CustomFieldTypes.string,
        sort_order=2,
    )
    db.add(telno)

    db.flush()
    db.commit()

    cm1 = models.ContactMoment(
        contact="Afdeling receptie [Mevrouw van de Reçeption] ",
        created_by_id="auth0|6141b11678862b0069362d25",
        created_by_name="automatischetest@exxellence.nl",
        created_at=datetime.now() - timedelta(days=0, seconds=65, hours=2, weeks=9),
    )
    cm2 = models.ContactMoment(
        contact="Woning coöperatie Goed wonen",
        created_by_id="auth0|6141b11678862b0069362d25",
        created_by_name="automatischetest@exxellence.nl",
        created_at=datetime.now() - timedelta(days=3, seconds=65, hours=0, weeks=4),
    )
    cm3 = models.ContactMoment(
        contact="Meneer de Wit-Cо прашање",
        created_by_id="auth0|60b8b386239e8b0070230c9b",
        created_by_name="admin@exxellence.nl",
        created_at=datetime.now() - timedelta(days=3, seconds=0, hours=0, weeks=4),
    )
    cm4 = models.ContactMoment(
        contact="Bouwbedrijf Schöne Häuße",
        created_by_id="auth0|6141b11678862b0069362d25",
        created_by_name="automatischetest@exxellence.nl",
        created_at=datetime.now() - timedelta(days=0, seconds=0, hours=0, weeks=2),
    )
    cm5 = models.ContactMoment(
        contact="Mevrouw die contact opnam",
        created_by_id="auth0|60b8b386239e8b0070230c9b",
        created_by_name="admin@exxellence.nl",
        created_at=datetime.now() - timedelta(days=0, seconds=0, hours=0, weeks=1),
    )
    cm6 = models.ContactMoment(
        contact="Club Dans & zo met veel sterren dansers",
        created_by_id="auth0|6141b11678862b0069362d25",
        created_by_name="automatischetest@exxellence.nl",
        created_at=datetime.now() - timedelta(days=0, seconds=10, hours=1, weeks=0),
    )

    db.add(cm1)
    db.add(cm2)
    db.add(cm3)
    db.add(cm4)
    db.add(cm5)
    db.add(cm6)

    db.flush()
    db.commit()

    # Add custom field values for contactmoments
    for cm in [cm1, cm2, cm3, cm4, cm5]:
        db.add(
            models.CustomFieldValues(
                contact_moment=cm,
                custom_field=kanaal,
                value=random.choice(channel),
            )
        )
        db.add(
            models.CustomFieldValues(
                contact_moment=cm,
                custom_field=telno,
                value="06" + str(random.choice(range(10000000, 99999999))),
            )
        )

    db.add(
        models.CustomFieldValues(
            contact_moment=cm6,
            custom_field=kanaal,
            value="Chat & E-mail",
        )
    )
    db.add(
        models.CustomFieldValues(
            contact_moment=cm6,
            custom_field=telno,
            value="06 - 12345678",
        )
    )

    db.commit()


if __name__ == "__main__":
    print("First remove any existing data")
    truncate_tables()
    print("RESTART sequences")
    restart_sequences_with_1()
    print("Creating initial data")
    create_initial_data()
    print("Initial data created")
