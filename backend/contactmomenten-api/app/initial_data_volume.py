#!/usr/bin/env python3

from app.db import models
from app.db.session import SessionLocal
from app.initial_data import restart_sequences_with_1, truncate_tables
from datetime import datetime, timedelta
from sqlalchemy.orm import Session

channel = ["Balie", "Telefoon", "Chat & E-mail", "Internet", "Post"]


def calculate_time_to_substract(
    seconds_interval_into_past,
    seconds_in_week,
    seconds_in_day,
    seconds_in_hour,
):
    hours_calculated = 0
    days_calculated = 0
    weeks_calculated = 0
    seconds_calculated = seconds_interval_into_past
    if seconds_calculated > seconds_in_week:
        weeks_calculated = seconds_calculated / seconds_in_week
        str_seconds = repr(weeks_calculated)
        weeks_as_str, str_seconds = str_seconds.split(".")
        weeks_calculated = float(weeks_as_str)
        seconds_calculated = seconds_calculated - (weeks_calculated * seconds_in_week)
    if seconds_calculated > seconds_in_day:
        days_calculated = seconds_calculated / seconds_in_day
        str_seconds = repr(days_calculated)
        days_calculated_as_str, str_seconds = str_seconds.split(".")
        days_calculated = float(days_calculated_as_str)
        seconds_calculated = seconds_calculated - (days_calculated * seconds_in_day)
    if seconds_calculated > seconds_in_hour:
        hours_calculated = seconds_calculated / seconds_in_hour
        str_seconds = repr(hours_calculated)
        hours_calculated_as_str, str_seconds = str_seconds.split(".")
        hours_calculated = float(hours_calculated_as_str)
        seconds_calculated = seconds_calculated - (hours_calculated * seconds_in_hour)
    return (
        weeks_calculated,
        days_calculated,
        hours_calculated,
        seconds_calculated,
    )


def create_initial_data() -> None:
    db: Session = SessionLocal()

    kanaal = models.CustomField(
        name="Kanaal",
        description="Vul het kanaal in",
        required=False,
        type=models.CustomFieldTypes.text,
        sort_order=1,
    )
    db.add(kanaal)

    telno = models.CustomField(
        name="Telefoonnummer",
        description="Vul een telefoonnummer in",
        required=False,
        type=models.CustomFieldTypes.string,
        sort_order=2,
    )
    db.add(telno)

    db.flush()
    db.commit()

    # Bulk add records

    max_records = 9999  # 1000000
    years_of_load = 60 / 365
    days_in_week = 7
    weeks_in_year = 52
    hours_in_day = 24

    seconds_in_hour = 60 * 60
    seconds_in_day = seconds_in_hour * hours_in_day  # ex. 3600 * 24 = 86400
    seconds_in_week = seconds_in_day * days_in_week  # ex. 85300 * 7 = 604800
    # Calculating the first times to subtract
    seconds_interval_into_past = round(years_of_load * weeks_in_year * seconds_in_week)
    seconds_interval = round(seconds_interval_into_past / max_records)
    # ex. (0.2 * 52 * 60 * 60 * 24 * 7) / 100 = > every 62899 second or
    # every 0.73 day or every 17.2 hours
    print("Seconds _interval: " + str(seconds_interval))
    print("Seconds _interval into past: " + str(seconds_interval_into_past))
    print("Max rexords: " + str(max_records))
    print("Years of load: " + str(years_of_load))
    print("Weeks in year: " + str(weeks_in_year))
    print("Seconds in week: " + str(seconds_in_week))
    (
        weeks_calculated,
        days_calculated,
        hours_calculated,
        seconds_calculated,
    ) = calculate_time_to_substract(
        seconds_interval_into_past,
        seconds_in_week,
        seconds_in_day,
        seconds_in_hour,
    )

    for index in range(1, max_records + 1):
        print(
            f"Time _interval (recordnr {index}): weeks={weeks_calculated} "
            f"days={days_calculated} hours={hours_calculated} "
            f"seconds={seconds_calculated}"
        )

        db.add(
            models.ContactMoment(
                contact=f"Mevrouw nummer {index}",
                created_at=datetime.now()
                - timedelta(
                    seconds=seconds_calculated,
                    hours=hours_calculated,
                    days=days_calculated,
                    weeks=weeks_calculated,
                ),
            )
        )

        # Calculate the new time calculateds
        if index < max_records:
            seconds_interval_into_past = (max_records - index) * seconds_interval
            (
                weeks_calculated,
                days_calculated,
                hours_calculated,
                seconds_calculated,
            ) = calculate_time_to_substract(
                seconds_interval_into_past,
                seconds_in_week,
                seconds_in_day,
                seconds_in_hour,
            )

    db.flush()
    db.commit()


if __name__ == "__main__":
    print("First remove any existing data")
    truncate_tables()
    print("RESTART sequences")
    restart_sequences_with_1()
    print("Creating initial data")
    create_initial_data()
    print("Initial data created")
