from app import domain, schemas
from pydantic.main import create_model
from sqlalchemy.orm import Session


# Dynamicaly create a new detailmodel and include the custom fields
def ContactMomentDetailsWithCustomFields(db: Session | None):

    custom_field_model = domain.custom_field.get_custom_field_model(db)
    return create_model(
        "ContactMomentDetails",
        __base__=schemas.ContactMomentDetails,
        **custom_field_model,
    )


def ContactMomentWithCustomFields(db: Session | None):

    custom_field_model = domain.custom_field.get_custom_field_model(db)
    return create_model("ContactMoment", __base__=schemas.ContactMoment, **custom_field_model)


def ContactMomentFilterableFieldsWithCustomFields(db: Session | None):

    custom_field_model = domain.custom_field.get_custom_field_model(db)
    return create_model(
        "ContactMomentFilterableFields",
        __base__=schemas.ContactMomentFilterableFields,
        **custom_field_model,
    )


def ContactMomentSortableFieldsWithCustomFields(db: Session | None):

    custom_field_model = domain.custom_field.get_custom_field_model(db)
    return create_model(
        "ContactMomentSortableFields",
        __base__=schemas.ContactMomentSortableFields,
        **custom_field_model,
    )
