import uuid as py_uuid
from pydantic import BaseModel, Field
from typing import List, Optional, Union


class FilterableId(BaseModel):
    sid: Optional[Union[str, List[str], List[int]]] = Field(alias="id")
    uuid: Optional[py_uuid.UUID]
