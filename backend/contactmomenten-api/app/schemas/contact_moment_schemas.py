from __future__ import annotations

import uuid as py_uuid
from app.schemas.custom_field_schemas import CustomFieldsList, FilterableId
from app.schemas.to_optional import to_optional
from datetime import datetime
from exxellence_shared_cga.core.types import SortOrder
from pydantic import BaseModel, Field
from typing import Optional


# Contact Moment
class ContactMomentBase(BaseModel):
    contact: str = Field(..., title="Contact")

    class Config:
        allow_population_by_field_name = True


class ContactMomentSubmitterId(BaseModel):
    created_by_id: Optional[str] = Field(None, title="Id of the submitter", alias="createdById")


class ContactMomentSubmitterName(BaseModel):
    created_by_name: Optional[str] = Field(
        None, title="Name of the submitter", alias="createdByName"
    )


class ContactMomentCreate(
    ContactMomentBase,
    ContactMomentSubmitterId,
    ContactMomentSubmitterName,
):
    pass


class ContactMomentCreateRequest(ContactMomentBase, CustomFieldsList):
    class Config:
        allow_population_by_field_name = True


class ContactMomentUpdate(ContactMomentBase):
    result_id: Optional[int] = Field(None, title="Result ID", alias="resultId")


class ContactMomentUpdateRequest(
    to_optional(ContactMomentUpdate), CustomFieldsList  # type: ignore
):
    class Config:
        allow_population_by_field_name = True


class ContactMoment(ContactMomentBase, ContactMomentSubmitterName):
    id: str = Field(..., title="The unique id of the contact moment")
    uuid: py_uuid.UUID = Field(..., title="The unique uuid of the result")
    created_at: datetime = Field(..., title="Creation Date", alias="createdAt")
    updated_at: Optional[datetime] = Field(None, title="Change Date", alias="updatedAt")

    class Config:
        orm_mode = True
        allow_population_by_field_name = True
        extra = "allow"


class ContactMomentDetails(ContactMoment, CustomFieldsList):
    class Config:
        orm_mode = True
        allow_population_by_field_name = True


class ContactMomentFilterableFields(FilterableId):
    contact: Optional[str]
    created_at_gte: Optional[str] = Field(alias="createdAt_gte")
    created_at_lte: Optional[str] = Field(alias="createdAt_lte")
    created_by_name: Optional[str] = Field(alias="createdByName")
    createdByMe: Optional[bool]
    q: Optional[str]

    class Config:
        allow_population_by_field_name = True
        extra = "forbid"


class ContactMomentSortableFields(BaseModel):
    sid: Optional[SortOrder] = Field(alias="id")
    contact: Optional[SortOrder]
    created_by_name: Optional[SortOrder] = Field(alias="createdByName")
    created_at: Optional[SortOrder] = Field(alias="createdAt")
    updated_at: Optional[SortOrder] = Field(alias="updatedAt")
    Kanaal: Optional[SortOrder]
    name: Optional[SortOrder]

    class Config:
        extra = "allow"
