import sqlalchemy as sa
import uuid
from app import domain, schemas
from app.core.auth0 import AccessUser, get_current_user, get_user_scopes
from app.db import models
from app.db.session import get_db
from exxellence_shared_cga.core.rest_helpers import SearchParameters
from exxellence_shared_cga.core.types import FilterOptions, GenericId, IdType
from fastapi import APIRouter, Depends, Response
from sqlalchemy.orm import Session
from typing import Any, Final

router = r = APIRouter()


def validate_search_parameters():
    return SearchParameters(
        filter_options_schema=schemas.ContactMomentFilterableFieldsWithCustomFields(None),
        sort_options_schema=schemas.ContactMomentSortableFieldsWithCustomFields(None),
    )


def _handle_filter_created_by_me(search_parameters_input: dict, user_id: str) -> dict:
    """
    This function checks if there is a filter createdByMe. If it is there
    and it is True then the filter field created_by_id is set to the user id
    """

    filter_options: FilterOptions | None = search_parameters_input.get("filter_options")
    if filter_options and filter_options.options.get("createdByMe") is not None:
        if filter_options.options.get("createdByMe") is True:
            filter_options.options["created_by_id"] = user_id

        filter_options.options.pop("createdByMe")
        search_parameters_input["filter_options"] = filter_options

    return search_parameters_input


def _get_custom_fields(db: Session):
    query = sa.select(models.CustomField).where(models.CustomField.archived == sa.false())
    return domain.custom_field.get_multi(
        db=db,
        query=query,
        sort_options=None,
        range_options=None,
        filter_options=None,
    )


# Get one contact moment by id.
@r.get(
    "/contacts/{id}",
    response_model=schemas.ContactMomentDetailsWithCustomFields(None) | Any,  # type: ignore # noqa: E501
    responses={404: {"description": "Contact moment not found"}},
    dependencies=[Depends(get_user_scopes)],
)
def get_contact_moment(
    id: uuid.UUID | str,
    current_user: AccessUser = Depends(get_current_user),
    db: Session = Depends(get_db),
):

    org_uuid: Final = current_user.organization_uuid
    custom_fields = _get_custom_fields(db=db)

    "Retrieve a single contact moment, given its unique identifier"
    type_of_id = IdType.uuid if isinstance(id, uuid.UUID) else IdType.id
    contact_moment = domain.contact_moment.get_contact_moment(
        db=db,
        org_uuid=org_uuid,
        custom_fields=custom_fields.result,
        id=GenericId(id=id, type=type_of_id),
    )

    return schemas.ContactMomentDetailsWithCustomFields(db=db).from_orm(contact_moment)


# Get a list of contact moments.
@r.get(
    "/contacts",
    response_model=list[schemas.ContactMomentWithCustomFields(None) | Any],  # type: ignore # noqa: E501
    dependencies=[Depends(get_user_scopes)],
)
def get_contact_moments(
    response: Response,
    current_user: AccessUser = Depends(get_current_user),
    db: Session = Depends(get_db),
    search_parameters_input: dict = Depends(validate_search_parameters()),
):

    custom_fields = _get_custom_fields(db=db)

    handled_search_parameters = _handle_filter_created_by_me(
        search_parameters_input, current_user.user_id
    )
    search_result = domain.contact_moment.get_contact_moments(
        db=db,
        custom_fields=custom_fields.result,
        **handled_search_parameters,
    )

    response.headers["Content-Range"] = search_result.as_content_range("results")
    return_val: list[schemas.ContactMomentWithCustomFields(db=db)] = []  # type: ignore # noqa: E501
    for contact in search_result.result:
        return_val.append(schemas.ContactMomentWithCustomFields(db=db).from_orm(contact))

    return return_val


# Add a new contact moment.
@r.post(
    "/contacts",
    response_model=schemas.ContactMomentDetails,
    responses={409: {"description": "Conflict. contact moment not added."}},
    dependencies=[Depends(get_user_scopes)],
)
def create_contact_moments(
    contact_moment_create_request: schemas.ContactMomentCreateRequest,
    current_user: AccessUser = Depends(get_current_user),
    db: Session = Depends(get_db),
):

    return domain.contact_moment.create_contact_moment(
        db=db,
        current_user=current_user,
        org_uuid=current_user.organization_uuid,
        contact_moment_create_request=contact_moment_create_request,
    )


# Update/Patch a contact moment.
@r.put(
    "/contacts/{id}",
    response_model=schemas.ContactMomentDetails,
    responses={
        404: {"description": "Contact moment not found"},
        409: {"description": "Conflict. Contact moment not updated."},
    },
    dependencies=[Depends(get_user_scopes)],
)
@r.patch(
    "/contacts/{id}",
    response_model=schemas.ContactMomentDetails,
    responses={
        404: {"description": "Contact moment not found"},
        409: {"description": "Conflict. Contact moment not updated."},
    },
    dependencies=[Depends(get_user_scopes)],
)
def update_contact_moment(
    id: uuid.UUID | str,
    contact_moment_update_request: schemas.ContactMomentUpdateRequest,
    current_user: AccessUser = Depends(get_current_user),
    db: Session = Depends(get_db),
):

    "Update the contact moment"
    type_of_id = IdType.uuid if isinstance(id, uuid.UUID) else IdType.id

    updated = domain.contact_moment.update_contact_moment(
        db=db,
        org_uuid=current_user.organization_uuid,
        id=GenericId(id=id, type=type_of_id),
        contact_moment_update_request=contact_moment_update_request,
    )

    return updated


# Delete a contact moment.
@r.delete(
    "/contacts/{id}",
    response_model=schemas.ContactMomentDetails,
    responses={
        404: {"description": "Contact moment not found"},
        409: {"description": "Conflict. Contact moment not deleted."},
    },
    dependencies=[Depends(get_user_scopes)],
)
def delete_contact_moment(
    id: uuid.UUID | str,
    db: Session = Depends(get_db),
):
    "Delete contact moment"
    type_of_id = IdType.uuid if isinstance(id, uuid.UUID) else IdType.id

    contact_moments = domain.contact_moment.delete(db=db, id=GenericId(id=id, type=type_of_id))

    return contact_moments
