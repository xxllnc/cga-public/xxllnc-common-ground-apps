import pytest
from app import schemas
from app.api.api_v1.routers.tests.test_mock_data import add_mock_data_to_db
from app.db import models
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session
from typing import Final

url: Final = "/contactmomenten/api/v1/contacts"
json_content_type: Final = {"Content-Type": "application/json"}


def _asser_contactmoment(
    db: Session, response: dict, expected_contactmoment: models.ContactMoment
):

    expected = (
        schemas.ContactMomentWithCustomFields(db=db)
        .from_orm(expected_contactmoment)
        .dict(by_alias=True)
    )
    assert response["id"] == expected["id"]
    assert response["uuid"] == str(expected["uuid"])
    assert response["createdByName"] == expected["createdByName"]
    assert response["contact"] == expected["contact"]
    assert response["createdAt"] == expected["createdAt"].isoformat()
    assert response["updatedAt"] == expected["updatedAt"].isoformat()
    assert response["Inkomend/Uitgaand"] == expected["Inkomend/Uitgaand"]
    assert response["Telefoonnummer"] == expected["Telefoonnummer"]
    assert response["Onderwerp"] == expected["Onderwerp"]
    assert response["Samenvatting"] == expected["Samenvatting"]
    assert response["Kanaal"] == expected["Kanaal"]


# Test route with sorting and range
@pytest.mark.parametrize(
    "request_parameters, expected_result, expected_length, status_code",
    [
        ("", ["1", "2", "3", "4", "5", "6", "7", "8"], None, 200),
        ("/123456", [], None, 404),
        ("/976bb079-f22d-40f9-b139-8e96b4baa82w", [], None, 404),
        ('?sort=["id","ASC"]', ["1", "2", "3", "4", "5", "6", "7", "8"], None, 200),
        ('?sort=["id","DESC"]', ["8", "7", "6", "5", "4", "3", "2", "1"], None, 200),
        ('?sort=["contact","ASC"]', ["2", "5", "4", "7", "8", "6", "3", "1"], None, 200),
        ('?sort=["contact","DESC"]', ["1", "3", "6", "7", "8", "4", "5", "2"], None, 200),
        ('?sort=["createdByName","ASC"]', ["1", "3", "4", "5", "8", "2", "7", "6"], None, 200),
        ('?sort=["createdByName","DESC"]', ["6"], 8, 200),
        ("?range=[0,2]", ["1", "2", "3"], None, 200),
        ("?range=[3,5]", ["4", "5", "6"], None, 200),
        ('?filter={"contact":"Gabriëlle"}', ["5"], None, 200),
        ('?filter={"contact":"Niek"}', ["7", "8"], None, 200),
        ('?filter={"createdByName":"Erwin"}', ["1", "3"], 5, 200),
        ('?filter={"createdByName":"Onbekend"}', [], None, 200),
        ('?filter={"createdByMe":"True"}', ["1", "3"], 5, 200),
        ('?filter={"createdByMe":"False"}', [], 8, 200),
        ('?filter={"createdAt_gte":"2021-05-12T00:01:00.000Z"}', ["2"], None, 200),
        ('?filter={"createdAt_lte":"2021-05-11T00:01:00.000Z"}', ["1", "3"], 7, 200),
        ('?range=[0,4]&sort=["contact","DESC"]&filter={"contact":"Gabriëlle"}', ["5"], None, 200),
        ('?filter={"q":"win"}', ["1", "2", "3", "4", "5", "8"], 6, 200),
        ('?filter={"q":"wil"}', ["2", "3", "7"], 3, 200),
    ],
)
def test_get_many_contactmoments(
    client: TestClient,
    test_db,
    request_parameters: str,
    expected_result: list[str],
    expected_length: int | None,
    status_code: int,
):

    mock = add_mock_data_to_db(test_db)
    response = client.get(f"{url}{request_parameters}")

    assert response.status_code == status_code

    length = expected_length if expected_length else len(expected_result)
    if status_code == 200 and length > 0:

        res = response.json()
        assert len(res) == length

        contactmoments = [getattr(mock, f"cm{nr}") for nr in expected_result]
        for index in range(len(expected_result)):
            _asser_contactmoment(
                db=test_db, response=res[index], expected_contactmoment=contactmoments[index]
            )


# test route with no contacts in db
def test_contact_moment_no_id_empty_list(client: TestClient):

    response = client.get(url)

    assert response.status_code == 200
    res = response.json()
    assert len(res) == 0


# ---------------------------------------------
#                      Get /{id}
# ---------------------------------------------
def test_get_contact_by_id(client: TestClient, test_db):

    mock = add_mock_data_to_db(test_db)

    response1 = client.get(f"{url}/{mock.cm5.id}")
    assert response1.status_code == 200
    _asser_contactmoment(db=test_db, response=response1.json(), expected_contactmoment=mock.cm5)

    response2 = client.get(f"{url}/{mock.cm4.uuid}")
    assert response2.status_code == 200
    _asser_contactmoment(db=test_db, response=response2.json(), expected_contactmoment=mock.cm4)


# ---------------------------------------------
#                      Post
# ---------------------------------------------
def test_post_contact(client: TestClient, test_db):

    mock = add_mock_data_to_db(test_db)

    custom_field1 = schemas.CustomFieldsWithValue.from_orm(mock.custom_field1)
    custom_field1.value = "Telefoon"

    custom_field2 = schemas.CustomFieldsWithValue.from_orm(mock.custom_field2)
    custom_field2.value = "06 123 123 12"

    cm_create = schemas.ContactMomentCreateRequest(
        contact="Karel",
        customFields=[custom_field1, custom_field2],
    ).json(by_alias=True)

    response = client.post(url, headers=json_content_type, data=cm_create)

    assert response.status_code == 200
    res = response.json()
    assert res["id"] is not None
    assert res["contact"] == "Karel"
    assert res["createdByName"] == "Test User"


def test_post_contact_without_custom_field(client: TestClient, test_db):

    add_mock_data_to_db(test_db)

    cm_create = schemas.ContactMomentCreateRequest(contact="Klaas").json(  # type: ignore
        by_alias=True
    )

    response = client.post(url, headers=json_content_type, data=cm_create)

    assert response.status_code == 200
    res = response.json()
    assert res["id"] is not None
    assert res["contact"] == "Klaas"


def test_post_with_only_contact(client: TestClient):

    json = {"contact": "Niemand"}
    response = client.post(url, json=json)

    assert response.status_code == 200


def test_post_contact_with_name(client: TestClient, test_db):

    add_mock_data_to_db(test_db)

    json = {"name": "Niemand"}
    response = client.post(url, json=json)

    assert response.status_code == 422


def test_post_duplicate_contact(client: TestClient, test_db):

    mock = add_mock_data_to_db(test_db)

    custom_field1 = schemas.CustomFieldsWithValue.from_orm(mock.custom_field1)
    custom_field1.value = "Telefoon"

    custom_field2 = schemas.CustomFieldsWithValue.from_orm(mock.custom_field2)
    custom_field2.value = "06 123 123 12"

    cm_create = schemas.ContactMomentCreateRequest(
        contact="Karel",
        customFields=[custom_field1, custom_field2],
    ).json(by_alias=True)

    client.post(url, headers=json_content_type, data=cm_create)
    response = client.post(url, headers=json_content_type, data=cm_create)

    assert response.status_code == 200


# ---------------------------------------------
#                      Delete /{id}
# ---------------------------------------------
def test_delete_contact(client: TestClient, test_db):

    mock = add_mock_data_to_db(test_db)

    response1 = client.delete(f"{url}/{mock.cm1.id}")
    assert response1.status_code == 200

    response2 = client.delete(f"{url}/{mock.cm2.uuid}")
    assert response2.status_code == 200

    response3 = client.delete(f"{url}/321654")
    assert response3.status_code == 404

    response4 = client.delete(f"{url}/invalid_id")
    assert response4.status_code == 404


# ---------------------------------------------
#                      PUT /{id}
# ---------------------------------------------
def test_put_contact_with_id(client: TestClient, test_db):

    mock = add_mock_data_to_db(test_db)

    custom_field1 = schemas.CustomFieldsWithValue.from_orm(mock.custom_field1)
    custom_field1.value = "Telefoon"

    custom_field2 = schemas.CustomFieldsWithValue.from_orm(mock.custom_field2)
    custom_field2.value = "06 123 123 12"

    cm_update = schemas.ContactMomentCreateRequest(
        contact="Karel",
        customFields=[custom_field1, custom_field2],
    ).json(by_alias=True)

    response = client.put(f"{url}/{mock.cm1.id}", headers=json_content_type, data=cm_update)

    assert response.status_code == 200
    res = response.json()
    assert res["id"] == str(mock.cm1.id)
    assert res["contact"] == "Karel"
    assert res["customFields"] is not None


def test_put_contact_with_uuid(client: TestClient, test_db):

    mock = add_mock_data_to_db(test_db)

    custom_field1 = schemas.CustomFieldsWithValue.from_orm(mock.custom_field1)
    custom_field1.value = "Telefoon"

    custom_field2 = schemas.CustomFieldsWithValue.from_orm(mock.custom_field2)
    custom_field2.value = "06 123 123 12"

    cm_update = schemas.ContactMomentCreateRequest(
        contact="Karel",
        customFields=[custom_field1, custom_field2],
    ).json(by_alias=True)

    response = client.put(f"{url}/{mock.cm1.uuid}", headers=json_content_type, data=cm_update)

    assert response.status_code == 200
    res = response.json()
    assert res["uuid"] == str(mock.cm1.uuid)
    assert res["contact"] == "Karel"
    assert res["customFields"] is not None


def test_put_contact_with_id_but_no_body(client: TestClient):

    response = client.put(f"{url}/1")
    assert response.status_code == 422


def test_put_contact_with_invalid_id(client: TestClient):

    response = client.put(f"{url}/invalidId", json={"contact": "Erwin"})

    assert response.status_code == 404


def test_put_contact_with_unknown_id(client: TestClient):

    response = client.put(f"{url}/1", json={"contact": "Erwin"})

    assert response.status_code == 404


# ---------------------------------------------
#                      405
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_method, status_code", [("delete", 405), ("put", 405), ("post", 422)]
)
def test_result_requests(client: TestClient, http_method: str, status_code: int):

    call = getattr(client, http_method, None)
    assert call is not None

    response = call(f"{url}")
    assert response.status_code == status_code
