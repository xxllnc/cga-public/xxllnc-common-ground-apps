from app.api.api_v1.routers.tests.test_mock_data import add_mock_data_to_db
from fastapi.testclient import TestClient

url = "/contactmomenten/api/v1/customFields"


# Test get result with a single result
def test_get_custom_fields(client: TestClient, test_db):

    add_mock_data_to_db(test_db)
    response = client.get(url)
    assert response.status_code == 200
    assert len(response.json()) == 7


# ---------------------------------------------
#                    Get /{uuid || id}
# ---------------------------------------------
def test_get_custom_field_by_id_and_uuid(client: TestClient, test_db):

    mock = add_mock_data_to_db(test_db)

    response_id = client.get(f"{url}/{mock.custom_field1.sid}")
    assert response_id.status_code == 200
    res = response_id.json()
    assert res["id"] == mock.custom_field1.sid
    assert res["uuid"] == str(mock.custom_field1.uuid)

    response_uuid = client.get(f"{url}/{mock.custom_field2.uuid}")
    assert response_uuid.status_code == 200
    res = response_uuid.json()
    assert res["id"] == mock.custom_field2.sid
    assert res["uuid"] == str(mock.custom_field2.uuid)


# Test create customfield
def test_create_custom_field(client: TestClient):

    response = client.post(
        url,
        json={
            "name": "test",
            "description": "Test 123",
            "required": True,
            "type": "string",
        },
    )

    assert response.status_code == 200
    res = response.json()

    assert res["id"] is not None
    assert res["name"] == "test"
    assert res["description"] == "Test 123"
    assert res["required"] is True
    assert res["type"] == "string"


# test put customfield
def test_update_custom_field(client: TestClient, test_db):

    mock = add_mock_data_to_db(test_db)

    response = client.put(
        f"{url}/{mock.custom_field1.sid}",
        json={
            "name": "uitleg",
            "description": "testtest",
            "required": False,
            "type": "text",
            "sort_order": 1,
        },
    )

    assert response.status_code == 200
    res = response.json()

    assert res["id"] == mock.custom_field1.sid
    assert res["name"] == "uitleg"
    assert res["description"] == "testtest"
    assert res["required"] is False
    assert res["type"] == "text"


# Test put customfield with invalid types
def test_update_custom_field_with_invalid_types(client: TestClient, test_db):
    mock = add_mock_data_to_db(test_db)

    response = client.put(
        f"{url}/{mock.custom_field1.sid}",
        json={
            "name": 1234,
            "description": 1234,
            "required": False,
            "type": 1232,
        },
    )

    assert response.status_code == 422


# test patch custom_field
def test_patch_custom_field(client: TestClient, test_db):

    mock = add_mock_data_to_db(test_db)

    response = client.patch(
        f"{url}/{mock.custom_field2.sid}",
        json={"sortOrder": 4},
    )

    assert response.status_code == 200
    res = response.json()

    assert res["id"] == mock.custom_field2.sid
    # Expect sortOrder to be set to the highest possible sort_order that is 2
    assert res["sortOrder"] == 4
