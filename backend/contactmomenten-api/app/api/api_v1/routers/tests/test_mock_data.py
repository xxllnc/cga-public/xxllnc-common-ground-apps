#!/usr/bin/env python3
import datetime
from app.db import models
from collections import namedtuple
from sqlalchemy.orm import Session

# Declaring namedtuple() as return value
MockData = namedtuple(
    "MockData",
    [
        "custom_field1",
        "custom_field2",
        "cm1",
        "cm2",
        "cm3",
        "cm4",
        "cm5",
        "cm6",
        "cm7",
        "cm8",
        "submitter_a",
        "submitter_b",
    ],
)


def add_mock_data_to_db(db: Session) -> MockData:

    custom_field1 = models.CustomField(
        name="Formulier",
        description="Vul het formulier nummer in",
        required=False,
        type=models.CustomFieldTypes.text,
        sort_order=5,
    )
    db.add(custom_field1)

    custom_field2 = models.CustomField(
        name="Kantoor",
        description="Vul het kantoor in",
        required=False,
        type=models.CustomFieldTypes.string,
        sort_order=6,
    )
    db.add(custom_field2)
    db.flush()

    submitter_a = {
        "id": "94a3f2ca-350d-4456-aeb0-8fb6093c8810",
        "name": "Erwin",
    }
    submitter_b = {"id": "id54321BA", "name": "Willem"}

    cm1 = models.ContactMoment(
        contact="Wouter",
        created_at=datetime.datetime(2021, 5, 11, 00, 1, 00),
        created_by_id=submitter_a["id"],
        created_by_name=submitter_a["name"],
    )
    cm2 = models.ContactMoment(
        contact="Erwin",
        created_at=datetime.datetime(2021, 5, 12, 00, 1, 00),
        created_by_id=submitter_b["id"],
        created_by_name=submitter_b["name"],
    )
    cm3 = models.ContactMoment(
        contact="Willem",
        created_at=datetime.datetime(2021, 5, 10, 00, 00, 58),
        created_by_id=submitter_a["id"],
        created_by_name=submitter_a["name"],
    )
    cm4 = models.ContactMoment(
        contact="Irene",
        created_at=datetime.datetime(2021, 5, 9, 00, 00, 57),
        created_by_id=submitter_a["id"],
        created_by_name=submitter_a["name"],
    )
    cm5 = models.ContactMoment(
        contact="Gabriëlle",
        created_at=datetime.datetime(2021, 5, 8, 00, 00, 56),
        created_by_id=submitter_a["id"],
        created_by_name=submitter_a["name"],
    )
    cm6 = models.ContactMoment(
        contact="Stijn",
        created_at=datetime.datetime(2021, 5, 7, 00, 00, 55),
    )
    cm7 = models.ContactMoment(
        contact="Niek",
        created_at=datetime.datetime(2021, 5, 6, 00, 00, 54),
        created_by_id=submitter_b["id"],
        created_by_name=submitter_b["name"],
    )
    cm8 = models.ContactMoment(
        contact="Niek",
        created_at=datetime.datetime(2021, 5, 5, 00, 00, 53),
        created_by_id=submitter_a["id"],
        created_by_name=submitter_a["name"],
    )

    db.add(cm1)
    db.add(cm2)
    db.add(cm3)
    db.add(cm4)
    db.add(cm5)
    db.add(cm6)
    db.add(cm7)
    db.add(cm8)

    db.flush()

    # Add custom field values for contactmoments
    for cm in [cm1, cm2, cm3, cm4, cm5, cm7, cm8]:
        for field in [custom_field1, custom_field2]:
            db.add(
                models.CustomFieldValues(
                    contact_moment=cm,
                    custom_field=field,
                    value=f"value for {field.name} for contact {cm.contact}",
                )
            )

    db.flush()

    return MockData(
        custom_field1,
        custom_field2,
        cm1,
        cm2,
        cm3,
        cm4,
        cm5,
        cm6,
        cm7,
        cm8,
        submitter_a,
        submitter_b,
    )
