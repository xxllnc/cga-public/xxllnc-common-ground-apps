import pytest
from fastapi.testclient import TestClient


@pytest.mark.parametrize(
    "http_method, route, expected_status_code",
    [
        ("get", "contacts", 200),
        ("get", "customFields", 200),
        ("get", "contacts/89", 404),
        ("get", "contacts/5d2425a6-6d17-422d-8c5f-f585db932f5e", 404),
        ("get", "customFields/89", 404),
        ("get", "customFields/5d2425a6-6d17-422d-8c5f-f585db932f5e", 404),
        ("post", "contacts", 422),
        ("post", "customFields", 422),
        ("post", "contacts/1", 405),
        ("post", "customFields/1", 405),
        ("put", "contacts", 405),
        ("put", "customFields", 405),
        ("put", "contacts/1", 422),
        ("put", "customFields/1", 422),
        ("delete", "contacts/1", 404),
        ("delete", "customFields/1", 200),
    ],
)
def test_routers_without_data_in_db(
    client: TestClient,
    http_method,
    route,
    expected_status_code,
):
    call = getattr(client, http_method, None)
    assert call is not None

    response = call(f"/contactmomenten/api/v1/{route}")
    assert response.status_code == expected_status_code
