"""RenameSubmitterColumns

Revision ID: 7c2bb11c71bc
Revises: 59f232d3558b
Create Date: 2021-09-23 12:42:17.395465+02:00

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = "7c2bb11c71bc"
down_revision = "59f232d3558b"
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column("ContactMoment", "submitter_id", new_column_name="created_by_id")
    op.alter_column("ContactMoment", "submitter_name", new_column_name="created_by_name")


def downgrade():
    op.alter_column("ContactMoment", "created_by_id", new_column_name="submitter_id")
    op.alter_column("ContactMoment", "created_by_name", new_column_name="submitter_name")
