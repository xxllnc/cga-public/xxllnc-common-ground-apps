import sqlalchemy as sa
from app import schemas
from app.db import models
from app.domain.create_base_query_with_custom_fields import create_base_query_with_custom_fields
from exxellence_shared_cga.core.types import (
    FilterOptions,
    GenericId,
    IdType,
    RangeOptions,
    SearchResult,
    SemanticError,
    SemanticErrorType,
    SortOptions,
)
from exxellence_shared_cga.domain.base import CRUDBase
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session
from typing import cast


class CRUDContactMoment(
    CRUDBase[
        models.ContactMoment,
        schemas.ContactMomentDetails,
        schemas.ContactMomentDetails,
    ]
):
    def get_contact_moment(
        self,
        db: Session,
        org_uuid,
        custom_fields: list[schemas.CustomField],
        id: GenericId,
    ) -> schemas.ContactMomentDetails:

        # First get the details of the contact moment
        query, query_filter = create_base_query_with_custom_fields(custom_fields, None)
        if id.type == IdType.uuid:
            query_filter["uuid"] = id.id
        else:
            query_filter["id"] = id.id

        contact_moments = self.get_multi(
            db=db,
            query=query,
            sort_options=None,
            range_options=None,
            filter_options=FilterOptions(options=query_filter),
            return_scalars=False,
        )

        if contact_moments.total <= 0:
            raise SemanticError(
                loc={"source": "path", "field": "id"},
                msg="Record not found",
                type=SemanticErrorType.not_found,
            )

        contact_moment = contact_moments.result[0]

        return_value = self.getCustomFieldsWithValues(
            db, org_uuid, contact_moment.sid, contact_moment
        )

        return return_value

    def get_contact_moments(
        self,
        db: Session,
        custom_fields: list[schemas.CustomField],
        sort_options: SortOptions | None,
        range_options: RangeOptions | None,
        filter_options: FilterOptions | None,
    ) -> SearchResult:

        query, query_filter = create_base_query_with_custom_fields(custom_fields, filter_options)

        return self.get_multi(
            db=db,
            query=query,
            sort_options=sort_options,
            range_options=range_options,
            filter_options=FilterOptions(options=query_filter),
            return_scalars=False,
        )

    def createOrUpdateCustomFieldValues(
        self,
        db: Session,
        contact_moment_id: int,
        custom_fields: list[schemas.CustomFieldsWithValue] | None,
    ):

        if custom_fields:
            for custom_field in custom_fields:
                query = sa.select(models.CustomFieldValues).filter(
                    sa.and_(
                        models.CustomFieldValues.contact_moment_id == contact_moment_id,
                        models.CustomFieldValues.custom_field_id == custom_field.sid,
                    )
                )
                custom_field_value = db.execute(query).scalars().first()

                if not custom_field_value:
                    custom_field_value = models.CustomFieldValues(
                        contact_moment_id=contact_moment_id,
                        custom_field_id=custom_field.sid,
                        value=custom_field.value,
                    )

                else:
                    custom_field_value.value = custom_field.value

                db.add(custom_field_value)
            db.commit()

    def create_contact_moment(
        self,
        db: Session,
        current_user,
        org_uuid: str,
        contact_moment_create_request: schemas.ContactMomentCreateRequest,
    ):

        # 1 Create the contact moment and add the submitter
        contact_moment_create = schemas.ContactMomentCreate.parse_obj(
            contact_moment_create_request.dict()
        )
        contact_moment_create.created_by_id = current_user.user_id
        contact_moment_create.created_by_name = current_user.user_name

        contact_moment_fields = contact_moment_create.dict(exclude_unset=True)
        db_contact_moment = models.ContactMoment(**contact_moment_fields)

        try:
            db.add(db_contact_moment)
            db.flush()  # generate uuid used in audit log
            db.commit()
        except IntegrityError as e:
            raise SemanticError(
                loc={"source": "body", "field": "name"},
                msg="Contact moment with that name already exists " + "in given time frame",
                type=SemanticErrorType.not_unique,
            ) from e
        db.refresh(db_contact_moment)

        # 2 Insert the custom_field values
        sid = cast(int, db_contact_moment.sid)  # Silence type checker for seeing this as Column
        self.createOrUpdateCustomFieldValues(db, sid, contact_moment_create_request.customFields)

        return self.getCustomFieldsWithValues(db, org_uuid, sid, db_contact_moment)

    def getCustomFieldsWithValues(
        self,
        db: Session,
        org_uuid: str,
        id: int,
        contact_moment: models.ContactMoment,
    ) -> schemas.ContactMomentDetails:
        # Then get the custom fields with values
        # for the selected contact moment
        custom_fields_with_values = (
            sa.select(
                models.CustomField.sid,
                models.CustomField.name,
                models.CustomField.description,
                models.CustomField.required,
                models.CustomField.type,
                models.CustomField.options,
                models.CustomField.sort_order,
                models.CustomField.archived,
                models.CustomFieldValues.value,
            )
            .outerjoin(
                models.CustomFieldValues,
                sa.and_(
                    models.CustomField.sid == models.CustomFieldValues.custom_field_id,
                    models.CustomFieldValues.contact_moment_id == id,
                ),
            )
            .where(models.CustomField.archived == sa.false())
        )

        result = db.execute(custom_fields_with_values)

        #  Convert the orm to a pydantic dict
        custom_fields = []
        for row in result:
            custom_fields.append(schemas.CustomFieldsWithValue.from_orm(row))

        # Combine the details and the custom_fields to one object
        return_value = schemas.ContactMomentDetailsWithCustomFields(org_uuid).from_orm(
            contact_moment
        )
        # custom functions to get sortorder info

        def get_sortorder(customfield):
            return customfield.sort_order

        custom_fields.sort(key=get_sortorder)

        return_value.customFields = custom_fields

        return return_value

    def update_contact_moment(
        self,
        db: Session,
        org_uuid: str,
        id: GenericId,
        contact_moment_update_request: schemas.ContactMomentUpdateRequest,
    ):

        # 1 update the contact moment
        db_contact_moment = self.get(db=db, id=id)
        if db_contact_moment is None:
            raise SemanticError(
                loc={"source": "path", "field": "id"},
                msg="Record not found",
                type=SemanticErrorType.not_found,
            )

        contact_moment = schemas.ContactMomentUpdate.parse_obj(
            contact_moment_update_request.dict()
        )

        update_data = contact_moment.dict(exclude_unset=True)

        for key, value in update_data.items():
            setattr(db_contact_moment, key, value)

        try:
            db.add(db_contact_moment)
            db.commit()
        except IntegrityError as e:
            raise SemanticError(
                loc={"source": "body", "field": "name"},
                msg="Unable to update contact moment",
                type=SemanticErrorType.not_unique,
            ) from e
        # Is deze direct nodig?
        db.refresh(db_contact_moment)

        # 2 Update or insert the custom_field values
        sid = cast(int, db_contact_moment.sid)  # Silence type checker for seeing this as Column
        self.createOrUpdateCustomFieldValues(db, sid, contact_moment_update_request.customFields)

        return self.getCustomFieldsWithValues(db, org_uuid, sid, db_contact_moment)


contact_moment = CRUDContactMoment(models.ContactMoment, models)
