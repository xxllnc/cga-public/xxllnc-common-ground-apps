import sqlalchemy as sa
from app import schemas
from app.db import models
from exxellence_shared_cga.core.types import GenericId
from exxellence_shared_cga.domain.base import CRUDBase
from loguru import logger
from sqlalchemy import asc
from sqlalchemy.orm import Session
from typing import Any

custom_field_model: dict | None = None


class CRUDCustomField(
    CRUDBase[
        models.CustomField,
        schemas.CustomFieldCreateUpdate,
        schemas.CustomFieldCreateUpdate,
    ]
):
    def _update_sort_order(
        self,
        db: Session,
        id: GenericId,
        obj_in: schemas.CustomFieldCreateUpdate | dict[str, Any],
    ):

        # Check if sort_order is changed in put or patch
        if isinstance(obj_in, schemas.CustomFieldCreateUpdate) and obj_in.sort_order is not None:
            db_obj = self.get(db=db, id=id)

            #  When new sort_order is changed, change sort_order for all fields
            if db_obj and db_obj.sort_order != obj_in.sort_order:  # type: ignore # noqa: E501
                old_sort_order = db_obj.sort_order
                new_sort_order = obj_in.sort_order

                # Get all custom_fields from db
                select_statement = sa.select(models.CustomField).order_by(
                    asc(models.CustomField.sort_order)
                )
                # Keep track of highest sort_order
                highest_sort_order = 0

                # use index to change sorting of fields
                # all fields that are in the range of the new and old
                # sort_order get a new sort_order based on the index
                for index, field in enumerate(
                    db.execute(select_statement).scalars().all(), start=1
                ):
                    highest_sort_order = index
                    if new_sort_order < old_sort_order:
                        field.sort_order = (
                            index
                            if field.sort_order < new_sort_order
                            or field.sort_order > old_sort_order
                            else index + 1
                        )
                    else:
                        field.sort_order = (
                            index
                            if field.sort_order < old_sort_order
                            or field.sort_order > new_sort_order
                            else index - 1
                        )
                db.commit()
                # This is done to prevent a gap at the start of
                # the sort_order sequence
                if new_sort_order <= 0:
                    obj_in.sort_order = 1
                # This is done to prefent a gap between at the end of
                # the sort_order sequence
                if new_sort_order > highest_sort_order:
                    obj_in.sort_order = highest_sort_order

    def get_custom_field_model(self, db: Session | None):

        if not db:
            return {}

        global custom_field_model
        if not custom_field_model:
            custom_field_model = {}
            try:
                query = sa.select(self.model).where(self.model.archived == sa.false())
                custom_fields = self.get_multi(
                    db=db,  # type: ignore
                    query=query,
                    sort_options=None,
                    range_options=None,
                    filter_options=None,
                )

                for field in custom_fields.result:
                    custom_field_model[field.name] = (str | None, None)
            except Exception as e:
                logger.error(
                    "Something went wrong getting the CustomFields. No exception is thrown"
                )
                logger.error(e)

        return custom_field_model

    def reset_custom_field_model(self):

        global custom_field_model
        custom_field_model = None

    def create(
        self,
        db: Session,
        custom_field: schemas.CustomFieldCreate,
    ) -> models.CustomField:

        # Get the max sort_order and add 1 or set to 1 if it is the first field
        max_sort_order = db.query(sa.func.max(models.CustomField.sort_order)).scalar()
        sort_order = (max_sort_order + 1) if max_sort_order is not None else 1

        obj_with_sort_order = schemas.CustomFieldCreateUpdate(**custom_field.dict())
        obj_with_sort_order.sort_order = sort_order

        ret_val = super().create(db, obj_with_sort_order)
        self.reset_custom_field_model()
        return ret_val

    def update_custom_field(
        self,
        db: Session,
        *,
        id: GenericId,
        obj_in: schemas.CustomFieldCreateUpdate | dict[str, Any],
    ) -> models.CustomField | None:

        self._update_sort_order(db, id, obj_in)
        ret_val = super().update(db=db, id=id, obj_in=obj_in)
        self.reset_custom_field_model()
        return ret_val

    def delete(self, db: Session, *, id: GenericId) -> models.CustomField | None:

        ret_val = super().delete(db, id=id)
        self.reset_custom_field_model()
        return ret_val


custom_field = CRUDCustomField(models.CustomField, models)
