import sqlalchemy as sa
from app import schemas
from app.db import models
from copy import copy
from exxellence_shared_cga.core.types import FilterOptions
from typing import Any, Tuple


def create_base_query_with_custom_fields(
    custom_fields: list[schemas.CustomField],
    filter_options: FilterOptions | None,
) -> Tuple[sa.sql.expression.Select, dict]:
    # Basic select fields
    select_fields: list[Any] = [
        models.ContactMoment.sid,
        models.ContactMoment.id,
        models.ContactMoment.uuid,
        models.ContactMoment.contact,
        models.ContactMoment.created_by_name,
        models.ContactMoment.created_at,
        models.ContactMoment.updated_at,
    ]

    # An array of fieldnames to check if filter/sort is on customField
    field_names: list[str] = []
    # Loop trough custom fields and add them as subquery to select_fields
    for field in custom_fields:
        field_names.append(field.name)
        select_fields.append(
            sa.select(models.CustomFieldValues.value)
            .where(
                sa.and_(
                    models.CustomFieldValues.custom_field_id == field.sid,
                    models.CustomFieldValues.contact_moment_id == models.ContactMoment.sid,
                )
            )
            .scalar_subquery()
            .correlate(models.ContactMoment)
            .label(field.name)
        )

    query = sa.select(*select_fields).outerjoin(models.CustomFieldValues).distinct()

    # Apply filter for custom fields
    # First create a new filter dict and
    # then loop trough all the filter options
    query_filter: dict = {}
    if filter_options:
        options = copy(filter_options.options)

        # Loop trough all the fields and
        # check if filter matches custom field
        for key, value in options.items():
            if key in field_names:
                # Filter matches a custom field so join customFieldValues
                # And add the field id and value to the query
                index = field_names.index(key)

                query = query.filter(
                    sa.and_(
                        models.CustomFieldValues.custom_field_id == custom_fields[index].sid,
                        models.CustomFieldValues.value == value,
                    )
                )
            # Filter doesn't match custom field
            # so add it to default filters
            else:
                query_filter[key] = value

    return query, query_filter
