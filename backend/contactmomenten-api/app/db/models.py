import datetime
import enum
import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as pg
from .session import Base
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import DateTime

utcnow = sa.literal_column("TIMEZONE('utc', CURRENT_TIMESTAMP)")


class ContactMoment(Base):
    __tablename__ = "contact_moment"
    default_sort_field = "id"

    sid = sa.Column(sa.Integer, primary_key=True, index=True)
    id = sa.Column(
        sa.String,
        nullable=False,
        unique=True,
        index=True,
        server_default=sa.func.contact_moment_generate_id(),
    )
    uuid = sa.Column(
        pg.UUID(as_uuid=True),
        nullable=False,
        unique=True,
        server_default=sa.func.gen_random_uuid(),
    )
    contact = sa.Column(sa.String, index=True)
    created_by_id = sa.Column(sa.String)
    created_by_name = sa.Column(sa.String)
    created_at = sa.Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = sa.Column(
        DateTime,
        default=datetime.datetime.utcnow,
        onupdate=datetime.datetime.utcnow,
    )
    custom_fields = relationship(
        "CustomFieldValues",
        back_populates="contact_moment",
        cascade_backrefs=False,  # https://docs.sqlalchemy.org/en/14/orm/session_api.html # noqa: E501
    )

    fulltext_fields = [
        {"modelName": "ContactMoment", "fields": ["id", "contact", "created_by_name"]},
        {"modelName": "CustomFieldValues", "fields": ["value"]},
    ]


class CustomFieldTypes(enum.Enum):
    string = "string"
    text = "text"
    select = "select"


class CustomField(Base):
    __tablename__ = "custom_field"
    default_sort_field = "sort_order"

    sid = sa.Column(sa.Integer, primary_key=True, index=True)
    uuid = sa.Column(
        pg.UUID(as_uuid=True),
        nullable=False,
        unique=True,
        server_default=sa.func.gen_random_uuid(),
    )
    name = sa.Column(sa.String, unique=True, index=True)
    description = sa.Column(sa.String)
    required = sa.Column(sa.Boolean, default=False)
    type = sa.Column(pg.ENUM(CustomFieldTypes), nullable=False)
    sort_order = sa.Column(sa.Integer, index=True, nullable=False)
    contact_moments = relationship(
        "CustomFieldValues",
        back_populates="custom_field",
        cascade_backrefs=False,  # https://docs.sqlalchemy.org/en/14/orm/session_api.html # noqa: E501
    )
    options = sa.Column(sa.JSON)
    archived = sa.Column(sa.Boolean, default=False, nullable=False)


class CustomFieldValues(Base):
    __tablename__ = "custom_field_values"
    default_sort_field = "sid"

    sid = sa.Column(sa.Integer, primary_key=True, index=True)
    contact_moment_id = sa.Column(sa.Integer, sa.ForeignKey("contact_moment.sid"), index=True)
    custom_field_id = sa.Column(sa.Integer, sa.ForeignKey("custom_field.sid"), nullable=False)
    value = sa.Column(sa.String)
    custom_field = relationship("CustomField", back_populates="contact_moments")
    contact_moment = relationship("ContactMoment", back_populates="custom_fields")
    sa.UniqueConstraint("contact_moment_id", "custom_field_id")
