#!/usr/bin/env python3

"""Module for creating initial data for the database"""
from app.db import models
from app.db.session import SessionLocal
from sqlalchemy import text
from sqlalchemy.orm import Session

if __name__ == "__main__":

    database: Session = SessionLocal()

    print("First remove any existing data and restart the sequences with 1")

    database.execute(text('truncate table "setting" cascade'))
    database.execute(text('ALTER SEQUENCE "setting_sid_seq" RESTART WITH 1'))
    database.commit()

    print("Add initial data")

    database.add(models.Setting(name="appId", value="3fa85f64-5717-4562-b3fc-2c963f66afa6"))
    database.add(
        models.Setting(name="appInstanceId", value="8cfd13aa-37bb-4a2f-b6d4-149eecc636c7")
    )

    database.flush()
    database.commit()

    print("Initial data added")
