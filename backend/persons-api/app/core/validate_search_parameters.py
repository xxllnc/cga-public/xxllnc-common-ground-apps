from fastapi import Query
from typing import Optional

search_description = """
    Het is alleen mogelijk om te zoeken op een van de volgende combinaties:
    - Burgerservicenummer
    - postcode met huisnummer van de verblijfsplaats
    - geslachtsnaam met geboortedatum van de persoon
    """


class validate:
    def __call__(
        self,
        burgerservicenummer: Optional[str] = Query(None),
        verblijfplaats__postcode: Optional[str] = Query(None),
        verblijfplaats__huisnummer: Optional[int] = Query(None),
        naam__geslachtsnaam: Optional[str] = Query(None),
        geboorte__datum: Optional[str] = Query(None),
    ):

        params = {
            "burgerservicenummer": burgerservicenummer,
            "verblijfplaats__postcode": verblijfplaats__postcode,
            "verblijfplaats__huisnummer": verblijfplaats__huisnummer,
            "naam__geslachtsnaam": naam__geslachtsnaam,
            "geboorte__datum": geboorte__datum,
        }

        filtered = {k: v for k, v in params.items() if v is not None}

        return filtered if filtered else None
