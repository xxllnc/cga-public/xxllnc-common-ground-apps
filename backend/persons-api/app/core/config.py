import os
import sys
from dotenv import load_dotenv
from typing import Final, Union

if "pytest" in sys.modules:
    load_dotenv(".env.test")


def to_int(value: Union[str, None], default_value: int) -> int:
    try:
        return int(value) if value else default_value
    except ValueError:
        return default_value


# The title of the API
PROJECT_NAME: Final[str] = "xxllnc-persons"
BASE_PATH: Final[str] = "/persons/api"
BASE_PATH_V1: Final[str] = f"{BASE_PATH}/v1"
APP_ID: Final[str] = os.environ.get("APP_ID", "")
USE_APP_INSTANCE_ID: Final[bool] = os.environ.get("USE_APP_INSTANCE_ID", "False").upper() == "TRUE"

HAALCENTRAAL_BRP_URL: Final[str] = os.environ["HAALCENTRAAL_BRP_URL"]
AUDITLOGGER_URL: Final[str] = os.environ["AUDITLOGGER_URL"]


X_API_KEY: Final = os.environ["X_API_KEY"]

# Auth0 Domain
DOMAIN: Final = os.environ["DOMAIN"]

# OIDC client-id, used to verify if we're the audience of access tokens
OIDC_CLIENT_ID: Final = os.environ["OIDC_CLIENT_ID"]


AUTH0_ECO_DOMAIN: Final[str] = os.environ.get("AUTH0_ECO_DOMAIN", "")
EVENT_URL: Final[str] = os.environ.get("EVENT_URL", "")

#############################################
# Auth0 event server settings
#############################################
AUTH0_EVENT_CLIENT_ID: Final[str] = os.environ.get("AUTH0_EVENT_CLIENT_ID", "")
AUTH0_EVENT_CLIENT_SECRET: Final[str] = os.environ.get("AUTH0_EVENT_CLIENT_SECRET", "")
AUTH0_EVENT_AUDIENCE: Final[str] = os.environ.get("AUTH0_EVENT_AUDIENCE", "")

#############################################
# Auth0 audit server settings
#############################################
AUTH0_AUDITLOG_CLIENT_ID: Final[str] = os.environ.get("AUTH0_AUDITLOG_CLIENT_ID", "")
AUTH0_AUDITLOG_CLIENT_SECRET: Final[str] = os.environ.get("AUTH0_AUDITLOG_CLIENT_SECRET", "")
AUTH0_AUDITLOG_AUDIENCE: Final[str] = os.environ.get("AUTH0_AUDITLOG_AUDIENCE", "")

#############################################
# Auth0 redis server settings
#############################################
REDIS_HOST: Final[str] = os.environ.get("REDIS_HOST", "redis")
REDIS_PASSWORD: Final[str | None] = os.environ.get("REDIS_PASSWORD", None)

#############################################
# SQLAlchemy settings
#############################################
SQLALCHEMY_DATABASE_URI: Final[str] = os.environ.get(
    "DATABASE_URL",
    "postgresql://persons:persons123@localhost:5432/persons",
)
# If set to True, all database queries will be logged as they are executed.
SQLALCHEMY_ECHO: Final[bool] = os.environ.get("DATABASE_ECHO", "False").upper() == "TRUE"
SQLALCHEMY_POOL_SIZE: Final[int] = to_int(os.environ.get("SQLALCHEMY_POOL_SIZE", None), 5)
SQLALCHEMY_POOL_MAX_OVERFLOW: Final[int] = to_int(
    os.environ.get("SQLALCHEMY_POOL_MAX_OVERFLOW", None), 10
)
# Flag to enable or disable audit logging
AUDIT_LOGGING = bool(os.environ.get("AUDIT_LOGGING", True))

#############################################
# Auth0 controlpanel token settings
#############################################
AUTH0_ECO_CLIENT_ID: Final[str] = os.environ.get("AUTH0_ECO_CLIENT_ID", "")
AUTH0_ECO_CLIENT_SECRET: Final[str] = os.environ.get("AUTH0_ECO_CLIENT_SECRET", "")
AUTH0_ECO_AUDIENCE: Final[str] = os.environ.get("AUTH0_ECO_AUDIENCE", "")

CONTROLPANEL_URL: Final[str] = os.environ.get("CONTROLPANEL_URL", "")
