import httpx
import ssl
from app import schemas
from app.core import config
from app.core.auth0 import AccessUser, get_current_user, get_user_scopes
from collections import namedtuple
from exxellence_shared_cga.access_token.auth0_adapter import Auth0Adapter
from fastapi import APIRouter, Depends
from starlette.background import BackgroundTask
from starlette.responses import StreamingResponse
from typing import Final

router = r = APIRouter()

auth0 = Auth0Adapter(
    config.AUTH0_AUDITLOG_CLIENT_ID,
    config.AUTH0_AUDITLOG_CLIENT_SECRET,
    config.AUTH0_AUDITLOG_AUDIENCE,
    f"https://{config.AUTH0_ECO_DOMAIN}",
    config.REDIS_HOST,
    config.REDIS_PASSWORD,
)

# Python 3.10 requires to add ssl.PROTOCOL_TLS so it is required to add context to httpx
context = ssl.SSLContext(ssl.PROTOCOL_TLS)
client = httpx.AsyncClient(base_url=config.AUDITLOGGER_URL, verify=context)


AppIds = namedtuple("AppIds", ["app_id", "app_instance_id"])
app_id_cache: dict[str, AppIds] = {
    "default": AppIds(
        "3fa85f64-5717-4562-b3fc-2c963f66afa6", "8cfd13aa-37bb-4a2f-b6d4-149eecc636c7"
    )
}


@r.get(
    "/auditlog",
    response_model=list[schemas.AuditLogOut],
    responses={404: {"description": "No auditlog found"}},
    dependencies=[Depends(get_user_scopes)],
)
async def get_auditlog_by_bsn(user: AccessUser = Depends(get_current_user)):

    global client

    access_token = auth0.get_access_token()
    headers: Final = {
        "content-type": "application/json",
        "authorization": f"Bearer {access_token}",
    }

    params = {
        "filter": f'{{"userId": "{user.user_name}"}}',
        "sort": '["timestamp","DESC"]',
        "range": "[0,99]",
    }
    req = client.build_request(method="GET", url="/auditlog", params=params, headers=headers)
    r = await client.send(req, stream=True)
    return StreamingResponse(
        r.aiter_raw(),
        background=BackgroundTask(r.aclose),
        headers=r.headers,  # type: ignore
    )
