import pytest
from app.core.config import BASE_PATH_V1
from app.main import app
from fastapi.testclient import TestClient
from httpx import AsyncClient


@pytest.fixture(params=["asyncio"])
def anyio_backend(request):
    return request.param


@pytest.mark.parametrize(
    "http_method, expected_status_code",
    [
        ("post", 405),
        ("put", 405),
        ("patch", 405),
        ("delete", 405),
        ("head", 405),
        ("options", 405),
    ],
)
@pytest.mark.anyio
def test_common_unused_methods(client: TestClient, http_method, expected_status_code):
    call = getattr(client, http_method, None)
    assert call is not None

    response = call(f"http://test:8887{BASE_PATH_V1}/persons/999993847")
    assert response.status_code == expected_status_code


@pytest.mark.anyio
async def test_async_find_persons():

    async with AsyncClient(app=app, base_url=f"http://test:8887{BASE_PATH_V1}") as ac:
        response = await ac.get(
            "/persons",
            params={
                "verblijfplaats__postcode": "2551XS",
                "verblijfplaats__huisnummer": 31,
            },
        )
    assert response.status_code == 200


@pytest.mark.anyio
async def test_async_get_person():

    async with AsyncClient(app=app, base_url=f"http://test:8887{BASE_PATH_V1}") as ac:
        response = await ac.get("/persons/999993847")
    assert response.status_code == 200
