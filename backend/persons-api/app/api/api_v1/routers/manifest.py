import json
from app import schemas
from exxellence_shared_cga.core.rest_helpers import SearchParameters
from fastapi import APIRouter, Response

router = r = APIRouter()

manfest = {
    "version": "1.0",
    "name": "cga-persons-manifest",
    "events": [
        {
            "object": "person",
            "action": "search",
            "label": "Gezocht naar persoon.",
            "description": "Er is gezocht naar persoon.",
            "auditlog": {"message": "Er is gezocht naar persoon: {event.data.person.bsn}."},
        }
    ],
    "roles": [
        {
            "label": "Xxllnc CGA Users",
            "description": "Gebruiker van XXLLNC.",
        },
    ],
}


class ManifestResponse(Response):
    media_type = "application/json"


def validate_search_parameters():
    return SearchParameters(
        filter_options_schema=schemas.SettingPublicFilterableFields,
        sort_options_schema=schemas.SettingPublicSortableFields,
    )


@r.get("/manifest", response_class=ManifestResponse)
def get_manifest():

    return json.dumps(manfest)
