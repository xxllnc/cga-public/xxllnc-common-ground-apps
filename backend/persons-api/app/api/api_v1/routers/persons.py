import asyncio
import httpx
import json
import ssl
import sys
from app import domain, schemas
from app.core import config
from app.core.auth0 import AccessUser, get_current_user, get_user_scopes
from app.core.validate_search_parameters import validate
from app.db import models
from collections import namedtuple
from datetime import datetime
from exxellence_shared_cga.access_token.auth0_adapter import Auth0Adapter
from exxellence_shared_cga.events.event_client import EventClient
from exxellence_shared_cga.events.event_schemas import Event, EventInfo, Source
from fastapi import APIRouter, Depends
from loguru import logger
from sqlalchemy.orm import Session
from starlette.background import BackgroundTask
from starlette.responses import StreamingResponse
from typing import Final, cast
from uuid import UUID

router = r = APIRouter()

auth0 = Auth0Adapter(
    config.AUTH0_EVENT_CLIENT_ID,
    config.AUTH0_EVENT_CLIENT_SECRET,
    config.AUTH0_EVENT_AUDIENCE,
    f"https://{config.AUTH0_ECO_DOMAIN}",
    config.REDIS_HOST,
    config.REDIS_PASSWORD,
)

event_client = EventClient(config.EVENT_URL)

# Python 3.10 requires to add ssl.PROTOCOL_TLS so it is required to add context to httpx
context = ssl.SSLContext(ssl.PROTOCOL_TLS)
client = httpx.AsyncClient(base_url=config.HAALCENTRAAL_BRP_URL, verify=context)

headers: Final = {"X-API-KEY": config.X_API_KEY}

AppIds = namedtuple("AppIds", ["app_id", "app_instance_id"])
app_id_cache: dict[str, AppIds] = {
    "default": AppIds(
        "3fa85f64-5717-4562-b3fc-2c963f66afa6", "8cfd13aa-37bb-4a2f-b6d4-149eecc636c7"
    )
}


def _get_app_ids(current_user: AccessUser, db: Session) -> AppIds:

    if current_user.org_id and current_user.org_id in app_id_cache:
        logger.info("App and app instance id is already available in cache so return this one")
        return app_id_cache[current_user.org_id]

    logger.info("App and app instance id is not in cache get it from the db")

    app_id = None
    app_instance_id = None

    result = domain.settings.get_multi(db=db)

    for row in result.result:
        row = cast(models.Setting, row)
        if row.name == "appId":
            app_id = row.value
        if row.name == "appInstanceId":
            app_instance_id = row.value

    # For now use default apIds when None is found
    # TODO if the provisioner is working than the appIds will be set and then delete this default
    # https://exxellence.atlassian.net/browse/CGA-2510
    if app_id is None or app_instance_id is None:
        return_value = app_id_cache["default"]
    else:
        return_value = AppIds(app_id, app_instance_id)

    if current_user.org_id:
        app_id_cache[current_user.org_id] = return_value

    return return_value


async def _post_event_for_audit_log(search_parameters_input: dict, current_user: AccessUser):

    global auth0, event_client

    # Dont post event in test or when client id is not set.
    # This is done to make it backwards compatible
    if "unittest" in sys.modules.keys() or config.AUTH0_EVENT_CLIENT_ID == "":
        return

    # TODO use _get_app_ids when provisioner is ready
    # https://exxellence.atlassian.net/browse/CGA-2510
    # app_ids = _get_app_ids(current_user=current_user, db=db)
    # For now always use default app ids
    app_ids = app_id_cache["default"]

    organizationId = (
        current_user.organization_uuid
        if current_user.organization_uuid
        else "ba3ca947-b5cc-421b-a27f-8754707a3b34"
    )

    # TODO: use pydantic model
    event = Event(
        version=1,
        timestamp=datetime.utcnow(),
        source=Source(
            appId=UUID(app_ids.app_id),
            organizationId=UUID(organizationId),
            userId=current_user.user_name,
            appInstanceId=UUID(app_ids.app_instance_id),
            displayName=f"{current_user.user_name}",
            requestId=None,
        ),
        event=EventInfo(object="string", action="string"),
        resource="person/BSN",
        content=search_parameters_input,
    )

    # To get the right date format first convert the pydantic model to json and than make it a dict
    event_dict = json.loads(event.json())

    access_token = auth0.get_access_token()

    asyncio.create_task(event_client.post_event(event_dict, access_token))

    logger.info("Audit log event is posted")


@r.get(
    "/persons",
    responses={404: {"description": "No person found"}},
    dependencies=[Depends(get_user_scopes)],
)
async def find_persons(
    search_parameters_input=Depends(validate()),
    current_user: AccessUser = Depends(get_current_user),
):
    """
    Het is alleen mogelijk om te zoeken op een van de volgende combinaties:
    - postcode met huisnummer van de verblijfsplaats
    - geslachtsnaam met geboortedatum van de persoon
    """

    global client, headers

    asyncio.create_task(
        _post_event_for_audit_log(
            search_parameters_input=search_parameters_input, current_user=current_user
        )
    )

    req = client.build_request(
        method="GET", url="", params=search_parameters_input, headers=headers
    )
    r = await client.send(req, stream=True)
    return StreamingResponse(
        r.aiter_raw(),
        background=BackgroundTask(r.aclose),
        headers=r.headers,  # type: ignore
    )


@r.get(
    "/persons/{bsn}",
    response_model=schemas.IngeschrevenPersoonHal,
    responses={404: {"description": "No person found"}},
    dependencies=[Depends(get_user_scopes)],
)
async def get_person_by_bsn(bsn: int, current_user: AccessUser = Depends(get_current_user)):

    global client, headers

    asyncio.create_task(
        _post_event_for_audit_log(
            search_parameters_input={"getPersonByBSN": bsn}, current_user=current_user
        )
    )

    req = client.build_request(method="GET", url=f"/{bsn}", headers=headers)
    r = await client.send(req, stream=True)
    return StreamingResponse(
        r.aiter_raw(),
        background=BackgroundTask(r.aclose),
        headers=r.headers,  # type: ignore
    )
