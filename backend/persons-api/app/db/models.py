import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as pg
from .session import Base


class Setting(Base):
    __tablename__ = "setting"
    default_sort_field = "id"

    sid = sa.Column(sa.Integer, primary_key=True, index=True)
    id = sa.Column(
        sa.String,
        nullable=False,
        unique=True,
        index=True,
        server_default=sa.func.setting_generate_id(),
    )
    uuid = sa.Column(
        pg.UUID(as_uuid=True),
        nullable=False,
        unique=True,
        server_default=sa.func.gen_random_uuid(),
    )
    name = sa.Column(sa.String, index=True)
    value = sa.Column(sa.String)
    description = sa.Column(sa.String)
