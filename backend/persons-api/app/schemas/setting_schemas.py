from __future__ import annotations

import uuid as py_uuid
from app.schemas.generic_schemas import FilterableId
from app.schemas.to_optional import to_optional
from exxellence_shared_cga.core.types import SortOrder
from pydantic import BaseModel, Field
from typing import Optional


# Setting
class SettingBase(BaseModel):
    name: str = Field(..., title="Name of the setting")
    value: str = Field(..., title="Value of the setting")
    description: Optional[str] = Field(..., title="Description of the setting")

    class Config:
        allow_population_by_field_name = True


class PublicSetting(SettingBase):
    id: str = Field(..., title="The unique id of the setting")
    uuid: py_uuid.UUID = Field(..., title="The unique uuid of the setting")

    class Config:
        orm_mode = True
        allow_population_by_field_name = True


class Setting(PublicSetting):
    class Config:
        orm_mode = True
        allow_population_by_field_name = True
        extra = "forbid"


class SettingCreate(SettingBase):

    pass


class SettingUpdate(to_optional(SettingCreate)):  # type: ignore

    pass


SettingUpdate.update_forward_refs()


class SettingPublicFilterableFields(FilterableId):
    name: Optional[str]

    class Config:
        extra = "forbid"


class SettingPublicSortableFields(BaseModel):
    id: Optional[SortOrder]
    name: Optional[SortOrder]

    class Config:
        extra = "forbid"


class SettingFilterableFields(SettingPublicFilterableFields):
    value: Optional[str]
    description: Optional[str]
    q: Optional[str]

    class Config:
        extra = "forbid"


class SettingSortableFields(SettingPublicSortableFields):
    value: Optional[SortOrder]
    SettingTypeCreate: Optional[SortOrder]

    class Config:
        extra = "forbid"
