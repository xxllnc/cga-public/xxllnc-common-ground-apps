generate a new model by:

```bash
datamodel-codegen --url https://raw.githubusercontent.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen/master/specificatie/genereervariant/openapi.yaml --output generated_schemas.py --target-python-version 3.10 --validation --wrap-string-literal --use-schema-description --reuse-model
```

auditlog schema's

```bash
datamodel-codegen --url https://auditlog-dev.xxllnc.nl/api --output generated_auditlog_schemas.py --target-python-version 3.10 --validation --wrap-string-literal --use-schema-description --reuse-model
```

Make sure poetry install is run and the right enviroment is selected

For more information see https://github.com/koxudaxi/datamodel-code-generator

Add # type: ignore to generated file to ignore type checks
