from app.db import models
from app.schemas import setting_schemas
from exxellence_shared_cga.domain.base import CRUDBase


class CRUDSettings(
    CRUDBase[
        models.Setting,
        setting_schemas.SettingCreate,
        setting_schemas.SettingUpdate,
    ]
):
    pass


settings = CRUDSettings(models.Setting, models)
