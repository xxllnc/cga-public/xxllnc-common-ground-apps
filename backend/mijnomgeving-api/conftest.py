import pytest
from app.core.auth0 import AccessUser, get_admin_scopes, get_current_user, get_user_scopes
from app.main import app
from fastapi.testclient import TestClient

auth_token = {
    "https://xxllnc.nl/name": "999993847",
    "iss": "https://xxllnc-dev.eu.auth0.com/",
    "sub": "oauth2|DigiD-MijnOmgeving|999993847",
    "aud": ["https://cga-dev.exxellence.nl/api", "https://xxllnc-dev.eu.auth0.com/userinfo"],
    "scope": "openid profile email",
}


@pytest.fixture(autouse=False)
def test_user() -> AccessUser:
    return override_get_current_user()


def override_get_current_user() -> AccessUser:
    return AccessUser.parse_obj(auth_token)


def override_get_scopes():
    return auth_token


@pytest.fixture()
def client():
    """
    Get a TestClient instance that reads/write to the test database
    """

    # Override the dependency and use the custom
    app.dependency_overrides[get_current_user] = override_get_current_user
    app.dependency_overrides[get_user_scopes] = override_get_scopes
    app.dependency_overrides[get_admin_scopes] = override_get_scopes

    yield TestClient(app)
