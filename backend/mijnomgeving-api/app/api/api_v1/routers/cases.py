"""Module for all cases api routes"""
from app import schemas
from app.core.auth0 import get_user_scopes
from app.services import zaaksysteem_service
from fastapi import APIRouter, Depends, Response

router = r = APIRouter()


@r.get(
    "/cases", response_model=list[schemas.CaseResponse], dependencies=[Depends(get_user_scopes)]
)
def get_cases_for_user(response: Response):

    cases = zaaksysteem_service.get_cases()

    response.headers["Content-Range"] = f"results 0-{len(cases)}/{len(cases)}"

    return cases


@r.get(
    "/cases/{id}",
    response_model=schemas.CaseDetailsResponse,
    dependencies=[Depends(get_user_scopes)],
)
def get_detailed_case_for_user(id: str):

    return zaaksysteem_service.get_detailed_case(id)
