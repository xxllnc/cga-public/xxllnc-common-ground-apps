import httpx
import requests
import ssl
from app import schemas
from app.core import config
from app.core.auth0 import get_user_scopes
from exxellence_shared_cga.core.rest_helpers import SearchParameters
from exxellence_shared_cga.core.types import FilterOptions, ProcessError
from fastapi import APIRouter, Depends, Form, Response, UploadFile
from fastapi import status as http_status
from starlette.background import BackgroundTask
from starlette.responses import StreamingResponse
from typing import cast

router = r = APIRouter()

cert = (
    (config.KOPPELAPP_MIJNOMGEVING_CERT, config.KOPPELAPP_MIJNOMGEVING_KEY)
    if config.KOPPELAPP_MIJNOMGEVING_CERT and config.KOPPELAPP_MIJNOMGEVING_KEY
    else None
)
# Python 3.10 requires to add ssl.PROTOCOL_TLS_CLIENT so it is required to add context to httpx
context = ssl.SSLContext(ssl.PROTOCOL_TLS)
client = httpx.AsyncClient(base_url=config.KOPPELAPP_URL, cert=cert, verify=context)


# validate the filter and sort parameters
def validate_search_parameters():
    return SearchParameters(
        filter_options_schema=schemas.CaseFilterableFields,
        sort_options_schema=schemas.CaseSortableFields,
    )


@r.get(
    "/documents",
    response_model=list[schemas.FileEntity],
    status_code=http_status.HTTP_200_OK,
    dependencies=[Depends(get_user_scopes)],
)
def get_document_meta_data_for_case(
    response: Response,
    search_parameters: dict = Depends(validate_search_parameters()),
):

    # case_uuid is required in CaseFilterableFields, it can be used without additional checks
    filter_options = cast(FilterOptions, search_parameters.get("filter_options"))
    case_uuid = filter_options.options.get("case_uuid")

    document_response = requests.get(
        f"{config.KOPPELAPP_URL}/api/v2/document/search_document",
        params={"case_uuid": case_uuid},
        cert=cert,
    )
    if document_response.status_code != 200:
        raise ProcessError(
            loc={"source": "backend"},
            msg=document_response.json(),
            status_code=http_status.HTTP_422_UNPROCESSABLE_ENTITY,
        )

    response.headers["Content-Range"] = "results 0-1/1"

    return document_response.json().get("data", [])


#  TODO: CGA-3640
@r.get("/documents/download/{id}", dependencies=[Depends(get_user_scopes)])
async def get_download_document(id: str):

    global client

    req = client.build_request(
        method="GET", url="/api/v2/document/download_document", params={"id": id}
    )
    r = await client.send(req, stream=True)
    return StreamingResponse(
        r.aiter_raw(),
        background=BackgroundTask(r.aclose),
        headers=r.headers,  # type: ignore
    )


@r.post(
    "/documents",
    response_model=None,
    status_code=http_status.HTTP_204_NO_CONTENT,
    dependencies=[Depends(get_user_scopes)],
)
def post_document(file: UploadFile, case_uuid: str = Form()):

    form_data = {"case_uuid": case_uuid}
    files = {"document_file": (file.filename, file.file, file.content_type)}

    response = requests.post(
        f"{config.KOPPELAPP_URL}/api/v2/document/create_document",
        data=form_data,
        files=files,
        cert=cert,
    )
    if response.status_code != 200:
        raise ProcessError(
            loc={"source": "backend"},
            msg=response.json(),
            status_code=http_status.HTTP_422_UNPROCESSABLE_ENTITY,
        )

    return None
