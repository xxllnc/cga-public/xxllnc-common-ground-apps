import json
import pytest
from fastapi.testclient import TestClient
from pathlib import Path
from requests.models import Response
from typing import Final
from unittest.mock import Mock

url: Final = "/mijnomgeving/api/v1/cases"


def _mock_koppelapp_response(mocker: Mock, return_value: dict, status_code: int):
    mock_response = Mock(spec=Response)
    mock_response.json.return_value = return_value
    mock_response.status_code = status_code

    mocker.patch("app.services.zaaksysteem_service.requests.get", return_value=mock_response)


# ------------------------------------------------------------------------------------------
# Test get_cases_for_user
# ------------------------------------------------------------------------------------------
def test_get_cases_for_user_with_401_from_koppelapp(client: TestClient, mocker: Mock):

    _mock_koppelapp_response(mocker=mocker, return_value={}, status_code=401)

    response = client.get(url)

    assert response.status_code == 422


def test_get_cases_for_user(client: TestClient, mocker: Mock):

    with open(Path(__file__).absolute().parent / "mocks/search.json") as file:
        return_value = json.load(file)
        file.close()

    _mock_koppelapp_response(mocker=mocker, return_value=return_value, status_code=200)

    response = client.get(url)

    assert response.status_code == 200
    data = response.json()
    assert len(data) == 10
    assert len(data[0]) == 7


# ------------------------------------------------------------------------------------------
# Test get_detailed_case_for_user
# ------------------------------------------------------------------------------------------
def test_get_detailed_case_for_user_401_from_koppelapp(client: TestClient, mocker: Mock):

    _mock_koppelapp_response(mocker=mocker, return_value={}, status_code=401)

    response = client.get(f"{url}/cf82fae8-dee5-474d-9a80-ecaf7db16bf3")

    assert response.status_code == 422


def test_get_detailed_case_for_user(client: TestClient, mocker: Mock):

    with open(Path(__file__).absolute().parent / "mocks/get_case.json") as file:
        return_value = json.load(file)
        file.close()

    _mock_koppelapp_response(mocker=mocker, return_value=return_value, status_code=200)

    response = client.get(f"{url}/cf82fae8-dee5-474d-9a80-ecaf7db16bf3")

    assert response.status_code == 200
    data = response.json()
    assert len(data) == 45
    assert data["identificatie"] == "502"


# ------------------------------------------------------------------------------------------
# Generic tests
# ------------------------------------------------------------------------------------------
@pytest.mark.parametrize("http_method", ["post", "put", "delete", "patch", "head", "options"])
def test_if_unsupported_methods_are_blocked(client: TestClient, http_method):

    call = getattr(client, http_method, None)
    assert call is not None

    response = call(url)
    assert response.status_code == 405
