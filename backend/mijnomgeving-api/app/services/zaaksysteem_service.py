import json
import requests
from app import schemas
from app.core import config
from exxellence_shared_cga.core.types import ProcessError
from fastapi import status as http_status
from types import SimpleNamespace
from typing import Any, Final

case_base_url: Final[str] = "/api/v2/cm/case"

two_way_cert: Final = (config.KOPPELAPP_MIJNOMGEVING_CERT, config.KOPPELAPP_MIJNOMGEVING_KEY)


def get_cases():

    resp = get_zaaksysteem_response(f"{case_base_url}/search", {"page": 1, "page_size": 10}, {})

    case_responses = []
    for key in resp.get("data", []):
        response_object = json.loads(json.dumps(key), object_hook=lambda d: SimpleNamespace(**d))
        case_responses.append(
            schemas.CaseResponse(
                id=response_object.id,
                identificatie=response_object.attributes.number,
                omschrijving=str(response_object.attributes.subject),
                zaaktype_naam=str(response_object.attributes.case_type_title),
                status_naam=response_object.attributes.status,
                registratiedatum=response_object.attributes.registration_date,
                einddatum=response_object.attributes.completion_date,
            )
        )

    return case_responses


def get_case_type(id: str):
    return get_zaaksysteem_response(
        "/api/v2/cm/case_type/get_active_version", {"case_type_uuid": id}, {}
    )


def get_detailed_case(id: str):

    case_response = get_zaaksysteem_response(f"{case_base_url}/get_case", {"case_uuid": id}, {})

    case_object = json.loads(
        json.dumps(case_response.get("data", [])), object_hook=lambda d: SimpleNamespace(**d)
    )
    attributes = case_object.attributes

    case_type_id = str(case_object.relationships.case_type.data.id)
    case_type = get_case_type(case_type_id)

    return schemas.CaseDetailsResponse(
        id=case_object.id,
        identificatie=attributes.number,
        omschrijving=str(attributes.public_summary),
        zaaktype_naam=case_type["data"]["meta"]["summary"],
        status_naam=attributes.status,
        registratiedatum=attributes.registration_date,
        einddatum=attributes.completion_date,
        einddatumGepland=attributes.target_completion_date,
        resultaat=attributes.result,
        archiefstatus=attributes.archival_state,
        deelzaken=[],
        eigenschappen=[],
        rollen=[],
        zaakinformatieobjecten=[],
        zaakobjecten=[],
    )


# TODO: Add error handling https://xxllnc.atlassian.net/browse/CGA-4127
def get_zaaksysteem_response(url: str, params: dict[Any, Any], files: dict[Any, Any]):
    hostname = config.KOPPELAPP_URL
    response = requests.get(
        hostname + url,
        params=params,
        files=files,
        cert=two_way_cert,
    )
    if response.status_code != 200:
        raise ProcessError(
            loc={"source": "backend"},
            msg=response.json(),
            status_code=http_status.HTTP_422_UNPROCESSABLE_ENTITY,
        )
    return response.json()
