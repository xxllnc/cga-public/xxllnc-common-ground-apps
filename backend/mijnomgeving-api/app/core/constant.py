"""This module contains all the constants for the mijnomgeving-api"""

from typing import Final

# The config item constants
CONFIG_ITEM_SEND_NOTIFICATION: Final = "sendNotification"
CONFIG_ITEM_NOTIFICATION_URL: Final = "notificationUrl"
CONFIG_ITEM_CASE_SYSTEM: Final = "zaaksysteem"
CONFIG_ITEM_CASE_TYPE: Final = "zaaktype"
