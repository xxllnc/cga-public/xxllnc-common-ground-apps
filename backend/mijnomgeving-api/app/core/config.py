import os
import sys
from dotenv import load_dotenv
from typing import Final

if "pytest" in sys.modules:
    load_dotenv(".env.test")

# The title of the API
PROJECT_NAME: Final[str] = "xxllnc-mijnomgeving"
BASE_PATH: Final[str] = "/mijnomgeving/api"
BASE_PATH_V1: Final[str] = f"{BASE_PATH}/v1"
APP_ID: Final[str] = os.environ.get("APP_ID", "")
USE_APP_INSTANCE_ID: Final[bool] = os.environ.get("USE_APP_INSTANCE_ID", "False").upper() == "TRUE"

#############################################
# Auth0 settings
#   Domain: auth0 domain
#   OIDC client-id, used to verify if we're the audience of access tokens
#############################################
DOMAIN = os.environ["DOMAIN"]
OIDC_CLIENT_ID = os.environ["OIDC_CLIENT_ID"]

#############################################
# KOPPELAPP_URL
#############################################
KOPPELAPP_URL: Final[str] = os.environ.get("KOPPELAPP_URL", "")
KOPPELAPP_MIJNOMGEVING_KEY: Final[str] = os.environ.get("KOPPELAPP_MIJNOMGEVING_KEY", "")
KOPPELAPP_MIJNOMGEVING_CERT: Final[str] = os.environ.get("KOPPELAPP_MIJNOMGEVING_CERT", "")
