import logging
import os
import sys
import uvicorn
from app.api.api_v1.routers import cases, documents, security
from app.core.config import BASE_PATH, BASE_PATH_V1, PROJECT_NAME
from exxellence_shared_cga.core.exceptions import (
    shared_semantic_error_handler,
    shared_validation_exception_handler,
)
from exxellence_shared_cga.core.logging import InterceptHandler
from exxellence_shared_cga.core.types import SemanticError
from fastapi import FastAPI, Request
from fastapi import status as http_status
from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse
from loguru import logger
from starlette.middleware.base import BaseHTTPMiddleware
from typing import Final


class CheckContentTypeMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request: Request, call_next):
        if request.method in {"POST", "PUT", "PATCH", "DELETE"}:

            content_type = request.headers.get("Content-Type")

            if content_type is not None and content_type != "application/json":

                path = request.get("path", "")
                file_upload = "multipart/form-data" in content_type and "requests/upload" in path

                if not file_upload:

                    return JSONResponse(
                        status_code=http_status.HTTP_415_UNSUPPORTED_MEDIA_TYPE,
                        content={
                            "detail": [
                                {
                                    "loc": {"source": "header", "field": "Content-Type"},
                                    "msg": "Content-Type header must be application/json",
                                    "type": "value_error.missing",
                                }
                            ]
                        },
                    )
        return await call_next(request)


API_DESCRIPTION: Final = """
mijnomgeving-api can be used to get cases from a backend
To use this api it can be required to provide a valid Auth0 jwt-token
"""

app = FastAPI(
    title=PROJECT_NAME,
    description=API_DESCRIPTION,
    version="0.0.1",
    contact={
        "name": "Xxllnc developer website",
        "url": "https://gitlab.com/xxllnc/cga/xxllnc-common-ground-apps/-/tree/development/backend/mijnomgeving-api",  # noqa: E501
    },
    license_info={
        "name": "Xxllnc: Licensed under the EUPL-1.2-or-later",
        "url": "https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12",
    },
    docs_url=f"{BASE_PATH}/docs",
    redoc_url=f"{BASE_PATH}/redoc",
    openapi_url=BASE_PATH,
)

#  TODO: CGA-3640
# app.add_middleware(CheckContentTypeMiddleware)


@app.middleware("http")
async def log_middle(request: Request, call_next):
    logger.trace(f"{request.method} {request.url}")
    logger.trace("Params:")
    for name, value in request.path_params.items():
        logger.trace(f"\t{name}: {value}")
    logger.trace("Headers:")
    for name, value in request.headers.items():
        logger.trace(f"\t{name}: {value}")

    response = await call_next(request)
    return response


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exc: RequestValidationError):
    hander = shared_validation_exception_handler()
    return hander(exc)


@app.exception_handler(SemanticError)
async def semantic_error_handler(request: Request, exc: SemanticError):
    hander = shared_semantic_error_handler()
    return hander(request, exc)


# Routers
app.include_router(cases.router, prefix=BASE_PATH_V1, tags=["Cases"])
app.include_router(documents.router, prefix=BASE_PATH_V1, tags=["Documents"])
app.include_router(security.router, prefix=BASE_PATH_V1, tags=["security.txt"])

# Get log level.
LOG_LEVEL = logging.getLevelName(os.environ.get("LOG_LEVEL", "INFO"))
LOGGERS = ("uvicorn.asgi", "uvicorn.access", "uvicorn.error")

# Configure root logger.
logging.root.handlers = [InterceptHandler()]
logging.root.setLevel(LOG_LEVEL)

# Remove every other logger's handlers and propagate to root logger
for name in LOGGERS:
    logging_logger = logging.getLogger(name)
    logging_logger.handlers = []
    logging_logger.setLevel(LOG_LEVEL)
    logging_logger.propagate = True

# Set format
logger.configure(handlers=[{"sink": sys.stdout, "level": LOG_LEVEL}])


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", reload=True, port=8890)
