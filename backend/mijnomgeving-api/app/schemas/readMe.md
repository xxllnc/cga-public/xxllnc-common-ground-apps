Make sure poetry install is run and the right enviroment is selected

For more information see https://github.com/koxudaxi/datamodel-code-generator

If nessesary add # type: ignore to generated file to ignore type checks

first cd to the schema directory

```bash
cd backend/mijnomgeving-api/app/schemas
```

generate a new case management model by:

```bash
datamodel-codegen --url https://development.zaaksysteem.nl/api/v2/cm/openapi/openapi30.json --output generated_case_management_schemas.py --openapi-scopes paths --target-python-version 3.10 --reuse-model --use-schema-description
```

generate a new Communication model by:

```bash
datamodel-codegen --url https://development.zaaksysteem.nl/api/v2/communication/openapi/openapi30.json --output generated_communication_schemas.py --openapi-scopes paths --target-python-version 3.10 --reuse-model --use-schema-description
```

generate a new Document model by:

```bash
datamodel-codegen --url https://development.zaaksysteem.nl/api/v2/document/openapi/openapi30.json --output generated_document_schemas.py --openapi-scopes paths --target-python-version 3.10 --reuse-model --use-schema-description
```

# openzaak

generate a new openzaak model by:

```bash
datamodel-codegen --url https://raw.githubusercontent.com/VNG-Realisatie/zaken-api/master/src/openapi.yaml --output generated_openzaak_schemas.py --openapi-scopes paths --target-python-version 3.10 --reuse-model --use-schema-description
```
