from app.schemas.generated_openzaak_schemas import PatchedZaak  # type: ignore
from datetime import date
from exxellence_shared_cga.core.types import SortOrder
from pydantic import BaseModel
from pydantic.fields import Field


class Documents(BaseModel):
    url: str = Field(..., title="Url to document")
    title: str = Field(..., title="Document title")


class CaseResponse(BaseModel):
    id: str = Field(..., title="Id of the case")
    identificatie: str = Field(..., title="Human readble identification")
    omschrijving: str = Field(..., title="Case description")
    zaaktype_naam: str = Field(..., title="Case type")
    registratiedatum: date = Field(..., title="Registration Date")
    einddatum: date | None = Field(None, title="Date the case is closed")
    status_naam: str = Field(..., title="The status of the case")


class CaseDetailsResponse(PatchedZaak, CaseResponse):
    pass


class CaseFilterableFields(BaseModel):
    case_uuid: str

    class Config:
        extra = "forbid"


class CaseSortableFields(BaseModel):

    id: SortOrder | None

    class Config:
        extra = "forbid"
