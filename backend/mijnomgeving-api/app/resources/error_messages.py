from typing import Final

"""Module for the API error messages"""

# Common message
BAD_REQUEST: Final = "Bad Request"
CONFLICT: Final = "Conflict"
NOT_FOUND: Final = "Not Found"
INTERNAL_SERVER_ERROR: Final = "Internal Server Error"
METHOD_NOT_ALLOWED: Final[str] = "Method Not Allowed"

FIELD_REQUIRED: Final[str] = "field required"
VALUE_ERROR_MISSING: Final[str] = "value_error.missing"

# Request messages
REQUEST_NOT_FOUND_WITH: Final = "Er is geen verzoek gevonden met {}"
REQUEST_REGISTRATION_DATE_NOT_VALID: Final = "registrationDate: {} is geen geldige datum, geef een geldige datum op, die een d-m-Y of Y-m-d formaat heeft."  # noqa: E501
REQUEST_NOT_CREATED: Final = "Verzoek is niet aangemaakt. Er ging iets mis."
REQUEST_NOT_UPDATED: Final = "Verzoek is niet gewijzigd. Er ging iets mis."
REQUEST_NOT_DELETED: Final = "Verzoek is niet verwijderd. Er ging iets mis."
REQUEST_IS_DELETED: Final = "Verzoek met id: {} is verwijderd."
REQUEST_NAME_ALREADY_EXIST_ERROR: Final = (
    "Er bestaat al een formulier met deze naam. Geef het formulier een unieke naam."  # noqa: E501
)


# File
FILE_IS_DELETED: Final = "Bestand met uuid: {} en naam: {} is verwijderd."
FILE_NOT_DELETED: Final = "Bestand is niet verwijderd. Er ging iets mis."

# Status messages
STATUS_NOT_FOUND_WITH: Final = "Er is geen status gevonden met {}"
STATUS_NOT_CREATED: Final = "Status is niet aangemaakt. Er ging iets mis."
STATUS_NOT_UPDATED: Final = "Status is niet gewijzigd. Er ging iets mis."
STATUS_NOT_DELETED: Final = "Status is niet verwijderd. Er ging iets mis."
STATUS_IS_DELETED: Final = "Status met id: {} is verwijderd."

DOCUMENT_IS_DELETED: Final = "Document met id: {} is verwijderd."
DOCUMENT_NOT_FOUND_WITH: Final = "Er zijn geen documenten gevonden met request id {}"
