import { NextFunction, Request, Response } from 'express'

const defaultMethodsToDeny = ['HEAD', 'PUT', 'DELETE', 'TRACE', 'CONNECT', 'PATCH']
const defaultMethodsToAllow = ['GET', 'POST', 'OPTIONS']

const methodBlockerMiddleware = ({ allow, deny } = { allow: defaultMethodsToAllow, deny: defaultMethodsToDeny }) =>
  (req: Request, res: Response, next: NextFunction): Response | void => {

    if (allow.includes(req.method))
      return next()

    console.log(
      `Denied request because the HTTP-Method is not ${deny.includes(req.method) ? 'allowed' : 'recognized'}: ${req.method}`
    )

    return res.status(418).json({
      status: 'error',
      message: 'Er heeft zich een onbekende fout voorgedaan.',
    })
  }

export default methodBlockerMiddleware