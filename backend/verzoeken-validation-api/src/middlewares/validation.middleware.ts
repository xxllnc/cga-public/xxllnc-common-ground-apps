import { plainToClass } from 'class-transformer'
import { validate, ValidationError } from 'class-validator'
import { RequestHandler } from 'express'
import { HttpException } from '@exceptions/HttpException'

const getAllNestedErrors = (error: ValidationError) => {
  if (error.constraints) {
    return Object.values(error.constraints)
  }
  return error.children.map(getAllNestedErrors).join(',')
}

export const validationMiddleware = (
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  type: any,
  value: string | 'body' | 'query' | 'params' = 'body',
  skipMissingProperties = false,
  whitelist = true,
  forbidNonWhitelisted = true,
): RequestHandler => (req, res, next) => {
  // eslint-disable-next-line  @typescript-eslint/no-unsafe-argument
  const obj = plainToClass(type, req[String(value)])
  void validate(obj, { skipMissingProperties, whitelist, forbidNonWhitelisted }).then((errors: ValidationError[]) => {
    if (errors.length > 0) {
      const message = errors.map(getAllNestedErrors).join(', ')
      next(new HttpException(400, message))
    } else {
      next()
    }
  })
}
