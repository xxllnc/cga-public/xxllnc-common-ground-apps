import { FormValidation } from '@/interfaces/validation.interface'
import initFormIO from '@/utils/initFormio'
import { HttpException } from '@exceptions/HttpException'
import { NextFunction, Request, Response } from 'express'

const Formio = initFormIO()

export const formValidationMiddleware = (req: Request, _res: Response, next: NextFunction) => {
  const formData = req.body as FormValidation

  if (!formData?.form || !formData?.formInput?.data)
    return next(new HttpException(400, 'No formData'))

  // Create the form, then check validity.
  void Formio.Formio.createForm(formData.form, {
    server: true,
    noDefaults: true,
    hooks: {
      setDataValue: (value: unknown, _key: string, _data: Record<string, unknown>) => value
    },
  })
    .then(form => {
      // Set the validation config.
      form.validator.config = {
        form: formData.form,
        submission: formData.formInput,
      }

      // Set the submission data
      form.data = formData.formInput.data

      // Perform calculations and conditions.
      form.checkConditions()
      form.clearOnHide()
      form.calculateValue()

      // Reset the data
      form.data = {}

      // Set the value to the submission.
      form.setValue(formData.formInput, { sanitize: true })

      // Check the validity of the form.
      form.checkAsyncValidity(null, true)
        .then(valid => {
          if (!valid) {
            const details = []
            form.errors.forEach(error => error.messages.forEach(message => details.push(message)))

            next(new HttpException(400, 'ValidationError', { name: 'ValidationError', details }))
          }
          next()
        })
        .catch(next)
    })
}
