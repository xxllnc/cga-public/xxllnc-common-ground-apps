import { HttpException } from '@exceptions/HttpException'
import { logger } from '@utils/logger'
import { NextFunction, Request, Response } from 'express'

const errorMiddleware = (error: HttpException, req: Request, res: Response, next: NextFunction): void => {
  try {
    const status: number = error.status || 500
    const message: string = error.message || 'Something went wrong'
    const details: Record<string, unknown> = error?.details

    if (process.env.NODE_ENV !== 'test') {
      console.error(`[${req.method}] ${req.path} >> StatusCode:: ${status}, Message:: ${message}`)
      logger.error(`[${req.method}] ${req.path} >> StatusCode:: ${status}, Message:: ${message}`)
    }
    res.status(status).json(details ? details : { message })
  } catch (err) {
    next(err)
  }
}

export default errorMiddleware
