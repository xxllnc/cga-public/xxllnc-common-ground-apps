import { IsObject } from 'class-validator'

export class FormValidationDto {
  @IsObject()
  public form: Record<string, unknown>

  @IsObject()
  public formInput: Record<string, unknown>
}
