import App from '@/app'
import { ValidationController } from '@controllers/validation.controller'
import { Server } from 'http'
import request from 'supertest'
import {
  basicForm, checkbox, currencyField, dateTime, dayField, emailField,
  formWithShowConditionOnFields, numberField, phoneField, radiobutton,
  selectBoxes, selectField, textArea, textField, time, urlField
} from './testdata'
import { formWithAdvancedShowConditions } from './testdata/formWithAdvancedShowConditions'

const url = '/validation-api/validate'
let server: Server
const app = new App([ValidationController])

// Set onlyAvailableItems to true
const selectFieldOnlyItems = { ...selectField, validate: { ...selectField.validate, onlyAvailableItems: true } }
const radiobuttonOnlyItems = { ...radiobutton, validate: { ...radiobutton.validate, onlyAvailableItems: true } }

// Add regulare expression to textField
const textFieldWithRegExp = {
  ...textField, validate: { ...textField.validate, pattern: '^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$' }
}

// Add json validation
const textFieldWithJSON = {
  ...textField, validate: {
    ...textField.validate,
    json: { if: [{ '===': [{ var: 'input' }, 'Bob'] }, true, 'Your name must be \'Bob\'!'] }
  }
}


beforeEach((done) => {
  server = app.getServer().listen(4000, () => {
    request.agent(server)
    done()
  })
})

afterEach((done) => { server && server.close(done) })

describe('Testing validate', () => {

  it('HTTP method put should return status code 418 (teapot).', async () =>
    request(server).put(url)
      .then(response => { expect(response.statusCode).toBe(418) })
  )

  describe('test validation with incorrect input', () => {
    test.each`
    input
    ${{ form: undefined }}
    ${{ form: {}, formInput: undefined }}
    ${{ form: {}, formInput: {} }}
    ${{ form: {}, formInput: { data: undefined } }}
    `('Should get error on validation of input: $input',
      async ({ input }: { input: object }) =>
        request(server).post(url).send(input)
          .then(response => {
            expect(response.statusCode).toBe(400)
          }))
  })

  // These tests validate to 204
  // ${[numberField]}          | ${{ number: 'three' }}                               | ${400}
  // ${[checkbox]}             | ${{ checkbox: 32 }}                                  | ${400}
  // ${[dateTime]}             | ${{ datetime: '2022-05-13' }}                        | ${400}
  // ${[dateTime]}             | ${{ datetime: 'something' }}                         | ${400}
  // ${[time]}                 | ${{ time: 'null' }}                                  | ${400}
  // ${[currencyField]}        | ${{ currency: '50Euro' }}                            | ${400}
  describe('Test basicForm with different fields and values', () => {
    test.each`
    fields                    | data                                                 | statusCode
    ${[checkbox]}             | ${{ checkbox: true }}                                | ${204}
    ${[checkbox]}             | ${{ checkbox: false }}                               | ${204}
    ${[currencyField]}        | ${{ currency: 12 }}                                  | ${204}
    ${[dateTime]}             | ${{ datetime: '2022-05-13T12:00:00+02:00' }}         | ${204}
    ${[dateTime]}             | ${{ datetime: '2022-05-13T12:00:00' }}               | ${204}
    ${[dateTime]}             | ${{ datetime: '2022-05-13T12:00' }}                  | ${204}
    ${[dayField]}             | ${{ day: '01/01/2000' }}                             | ${204}
    ${[dayField]}             | ${{ day: '0101/2000' }}                              | ${400}
    ${[emailField]}           | ${{ email: 'me@xxllnc.nl' }}                         | ${204}
    ${[emailField]}           | ${{ email: 'mexxllnc.nl' }}                          | ${400}
    ${[emailField]}           | ${{ email: 'me@xxllnc' }}                            | ${400}
    ${[numberField]}          | ${{ number: 3 }}                                     | ${204}
    ${[phoneField]}           | ${{ phoneNumber: '(061) 234-5678' }}                 | ${204}
    ${[phoneField]}           | ${{ phoneNumber: '(061) 234-567' }}                  | ${400}
    ${[radiobutton]}          | ${{ radio1: 'optie2' }}                              | ${204}
    ${[radiobutton]}          | ${{ radio1: 'nonExistingOption' }}                   | ${204}
    ${[radiobuttonOnlyItems]} | ${{ radio1: 'nonExistingOption' }}                   | ${400}
    ${[selectBoxes]}          | ${{ selectBoxes1: { optie1: true, optie2: false } }} | ${204}
    ${[selectBoxes]}          | ${{ selectBoxes1: { optie1: true, optie2: true } }}  | ${204}
    ${[selectField]}          | ${{ select1: 'optie1' }}                             | ${204}
    ${[selectField]}          | ${{ select1: 'nonExistingOption' }}                  | ${204}
    ${[selectFieldOnlyItems]} | ${{ select1: 'nonExistingOption' }}                  | ${400}
    ${[textArea]}             | ${{ textArea: 'val' }}                               | ${204}
    ${[textArea]}             | ${{ textArea: null }}                                | ${204}
    ${[textField]}            | ${{ textField: 'val' }}                              | ${204}
    ${[textField]}            | ${{ textField: null }}                               | ${400}
    ${[textFieldWithJSON]}    | ${{ textField: 'Bob' }}                              | ${204}
    ${[textFieldWithJSON]}    | ${{ textField: 'Rob' }}                              | ${400}
    ${[textFieldWithRegExp]}  | ${{ textField: 'me@domain.com' }}                    | ${204}
    ${[textFieldWithRegExp]}  | ${{ textField: 'me@domaincom' }}                     | ${400}
    ${[time]}                 | ${{ time: '08:49:00' }}                              | ${204}
    ${[time]}                 | ${{ time: '32:49:00' }}                              | ${204}
    ${[time]}                 | ${{ time: null }}                                    | ${400}
    ${[urlField]}             | ${{ url: 'http://xxllnc.nl' }}                       | ${204}
    ${[urlField]}             | ${{ url: 'http://xxllncnl' }}                        | ${400}
    ${[urlField]}             | ${{ url: 'htt://xxllncnl' }}                         | ${400}
    `('Should get $statusCode on validation of data: $data',
      async ({ fields, data, statusCode }: { fields: object[], data: object, statusCode: number }) => {

        const form = basicForm
        form.components[0].components = fields
        const formData = {
          form,
          formInput: { data }
        }

        return request(server).post(url).send(formData).then(response => {
          expect(response.statusCode).toBe(statusCode)
        })

      })
  })

  describe('test form with fields with conditions', () => {
    test.each`
    data                                           | statusCode
    ${{ select1: 'showFieldA', fieldA: 'test' }}   | ${204}
    ${{ select1: 'showFieldA', fieldB: 'test' }}   | ${400}
    ${{ select1: 'showFieldB', fieldB: 'test' }}   | ${204}
    ${{ select1: 'showFieldB', fieldA: 'test' }}   | ${400}
    `('Should get $statusCode on validation of data: $data',
      async ({ data, statusCode }: { data: object, statusCode: number }) => {

        const form = formWithShowConditionOnFields
        const formData = {
          form,
          formInput: { data }
        }

        return request(server).post(url).send(formData).then(response => {
          expect(response.statusCode).toBe(statusCode)
        })

      })
  })

  describe('test form with fields with advancedconditions', () => {
    test.each`
    data                                                  | statusCode
    ${{ selectanoption: 'option1', textField: 'test' }}   | ${204}
    ${{ selectanoption: 'option1', textField: null }}     | ${400}
    ${{ selectanoption: 'option2', textField: 'test' }}   | ${204}
    ${{ selectanoption: 'option2', textField: null }}     | ${400}
    ${{ selectanoption: 'option3', textField: 'test' }}   | ${204}
    ${{ selectanoption: 'option3', textField: null }}     | ${204}
    `('Should get $statusCode on validation of data: $data',
      async ({ data, statusCode }: { data: object, statusCode: number }) => {

        const form = formWithAdvancedShowConditions
        const formData = {
          form,
          formInput: { data }
        }

        return request(server).post(url).send(formData).then(response => {
          expect(response.statusCode).toBe(statusCode)
        })

      })
  })

})
