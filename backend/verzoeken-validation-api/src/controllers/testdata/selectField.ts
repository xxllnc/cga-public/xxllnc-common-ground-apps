export const selectField = {
  label: 'Select',
  labelPosition: 'top',
  widget: 'choicesjs',
  labelWidth: '',
  labelMargin: '',
  placeholder: '',
  description: '',
  tooltip: '',
  customClass: '',
  tabindex: '',
  hidden: false,
  hideLabel: false,
  uniqueOptions: false,
  autofocus: false,
  disabled: false,
  tableView: true,
  modalEdit: false,
  multiple: false,
  dataSrc: 'values',
  data: {
    values: [
      {
        label: 'optie 1',
        value: 'optie1'
      },
      {
        label: 'optie 2',
        value: 'optie2'
      }
    ],
    resource: '',
    json: '',
    url: '',
    custom: ''
  },
  dataType: '',
  idPath: 'id',
  valueProperty: '',
  limit: 100,
  template: '<span>{{ item.label }}</span>',
  refreshOn: '',
  refreshOnBlur: '',
  clearOnRefresh: false,
  searchEnabled: true,
  selectThreshold: 0.3,
  readOnlyValue: false,
  customOptions: {},
  useExactSearch: false,
  persistent: true,
  protected: false,
  dbIndex: false,
  encrypted: false,
  clearOnHide: true,
  customDefaultValue: '',
  calculateValue: '',
  calculateServer: false,
  allowCalculateOverride: false,
  validateOn: 'change',
  validate: {
    required: false,
    onlyAvailableItems: false,
    customMessage: '',
    custom: '',
    customPrivate: false,
    json: '',
    strictDateValidation: false,
    multiple: false,
    unique: false
  },
  errorLabel: '',
  errors: '',
  key: 'select1',
  tags: [],
  properties: {},
  conditional: {
    show: null,
    when: null,
    eq: '',
    json: ''
  },
  customConditional: '',
  logic: [],
  attributes: {},
  overlay: {
    style: '',
    page: '',
    left: '',
    top: '',
    width: '',
    height: ''
  },
  type: 'select',
  indexeddb: {
    filter: {}
  },
  selectFields: '',
  searchField: '',
  searchDebounce: 0.3,
  minSearch: 0,
  filter: '',
  redrawOn: '',
  input: true,
  prefix: '',
  suffix: '',
  unique: false,
  dataGridLabel: false,
  showCharCount: false,
  showWordCount: false,
  allowMultipleMasks: false,
  addons: [],
  lazyLoad: true,
  authenticate: false,
  ignoreCache: false,
  fuseOptions: {
    include: 'score',
    threshold: 0.3
  },
  id: 'e8qfjn7',
  defaultValue: ''
}