import { FormValidationDto } from '@/dtos/FormValidation.dto'
import { formValidationMiddleware } from '@/middlewares/formValidation.middleware'
import { validationMiddleware } from '@middlewares/validation.middleware'
import { Body, Controller, HttpCode, Post, UseBefore } from 'routing-controllers'
import { OpenAPI } from 'routing-controllers-openapi'

@Controller('/validation-api')
export class ValidationController {
  @Post('/validate')
  @HttpCode(204)
  @UseBefore(validationMiddleware(FormValidationDto, 'body'))
  @UseBefore(formValidationMiddleware)
  @OpenAPI({ summary: 'Validate a formio form' })
  validateForm(@Body({ required: true }) _formData: FormValidationDto): null {
    return null
  }
}
