import { HttpError } from 'routing-controllers'

export class HttpException extends HttpError {
  public status: number
  public message: string
  public details?: Record<string, unknown>

  constructor(status: number, message: string, details?: Record<string, unknown>) {
    super(status, message)
    this.status = status
    this.message = message
    this.details = details
  }
}
