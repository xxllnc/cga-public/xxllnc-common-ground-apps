import { API_LOG_FORMAT, CREDENTIALS, NODE_ENV, ORIGIN, PORT } from '@config'
import errorMiddleware from '@middlewares/error.middleware'
import methodBlockerMiddleware from '@middlewares/methodBlocker.middleware'
import { logger } from '@utils/logger'
import { validationMetadatasToSchemas } from 'class-validator-jsonschema'
import compression from 'compression'
import cookieParser from 'cookie-parser'
import express, { Application } from 'express'
import helmet from 'helmet'
import hpp from 'hpp'
import morgan from 'morgan'
import 'reflect-metadata'
import { getMetadataArgsStorage, useExpressServer } from 'routing-controllers'
import { routingControllersToSpec } from 'routing-controllers-openapi'
import swaggerUi from 'swagger-ui-express'

class App {
  public app: express.Application
  public env: string
  public port: string | number

  // eslint-disable-next-line @typescript-eslint/ban-types
  constructor(Controllers: Function[]) {
    this.app = express()
    this.env = NODE_ENV || 'production'
    this.port = PORT || 3000

    this.initializeMiddlewares()
    this.initializeRoutes(Controllers)
    this.initializeSwagger(Controllers)
    this.initializeErrorHandling()
  }

  public listen(): void {
    this.app.listen(this.port, () => {
      logger.info('=================================')
      logger.info(`======= ENV: ${this.env} =======`)
      logger.info(`🚀 App listening on the port ${this.port}`)
      logger.info('=================================')
    })
  }

  public getServer(): Application {
    return this.app
  }

  private initializeMiddlewares() {
    this.app.use(morgan(API_LOG_FORMAT))
    this.app.use(hpp())
    this.app.use(helmet())
    this.app.use(compression())
    this.app.use(express.json())
    this.app.use(express.urlencoded({ extended: true }))
    this.app.use(cookieParser())
    this.app.use(methodBlockerMiddleware())
  }

  // eslint-disable-next-line @typescript-eslint/ban-types
  private initializeRoutes(controllers: Function[]) {
    useExpressServer(this.app, {
      cors: {
        origin: ORIGIN,
        credentials: CREDENTIALS,
      },
      controllers,
      defaultErrorHandler: false,
    })
  }

  // eslint-disable-next-line @typescript-eslint/ban-types
  private initializeSwagger(controllers: Function[]) {
    const schemas = validationMetadatasToSchemas()

    const routingControllersOptions = {
      controllers,
    }

    const storage = getMetadataArgsStorage()
    const spec = routingControllersToSpec(storage, routingControllersOptions, {
      components: {
        schemas,
        securitySchemes: {
          basicAuth: {
            scheme: 'basic',
            type: 'http',
          },
        },
      },
      info: {
        description: 'This service can be used to validate formio form input. For validation formiojs is used',
        title: 'Verzoeken validation service',
        version: '1.0.0',
      },
    })

    this.app.use('/validation-api/docs', swaggerUi.serve, swaggerUi.setup(spec))
  }

  private initializeErrorHandling() {
    this.app.use(errorMiddleware)
  }
}

export default App
