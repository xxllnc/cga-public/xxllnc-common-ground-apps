export interface FormValidation {
  form: Record<string, unknown>
  formInput: { data: Record<string, unknown> }
}
