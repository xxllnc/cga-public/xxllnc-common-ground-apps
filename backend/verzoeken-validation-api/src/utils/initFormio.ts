import { VM } from 'vm2'

class DummyClass { }
const dummyFunction = () => {
  // do nothing
}

interface FormIoForm {
  validator: { config: unknown }
  checkConditions: () => void
  clearOnHide: () => void
  calculateValue: () => void
  data: Record<string, unknown>
  setValue: (...args: unknown[]) => void
  checkAsyncValidity: (...args: unknown[]) => Promise<boolean>
  errors: { messages: Record<string, unknown>[] }[]
}
interface FormioInterface {
  Formio: {
    createForm: (form: Record<string, unknown>, options: unknown) => Promise<FormIoForm>
  }
  Displays: { displays: { [key: string]: { prototype: { onChange: unknown } } } }
  Utils: { Evaluator: { noeval: unknown, evaluator: unknown } }
}

/*****************************************************************************************
* To initiate formiojs it is nessesary to first set some globals.                        *
* This is needed because formiojs is designed to run in the browser and it is depending  *
* on these globals                                                                       *
*****************************************************************************************/
const initFormIO = (): FormioInterface => {
  // Define a few global noop placeholder shims and import the component classes
  const navigator: { [key: string]: string } = { userAgent: '' }
  global.Text = DummyClass
  global.HTMLElement = DummyClass
  global.HTMLCanvasElement = DummyClass
  global.navigator = navigator
  global.document = {
    createElement: () => ({}),
    cookie: '',
    getElementsByTagName: () => [],
    documentElement: {
      style: [],
      firstElementChild: {
        appendChild: dummyFunction
      },
    },
  }
  global.window = {
    addEventListener: dummyFunction,
    Event: dummyFunction,
    navigator
  }
  global.btoa = (str: unknown) => str instanceof Buffer ? str.toString('base64') : Buffer.from(str.toString(), 'binary').toString('base64')
  global.self = global

  global.self = global

  // import after setting all globals
  const Formio = require('formiojs/formio.form.js') as FormioInterface
  global.Formio = Formio.Formio

  // Remove onChange events from all renderer displays.
  Object.values(Formio.Displays.displays).forEach(display => display.prototype.onChange = dummyFunction)


  const vm = new VM({
    timeout: 250,
    sandbox: {
      result: null,
    },
    fixAsync: true,
  })

  Formio.Utils.Evaluator.noeval = true
  Formio.Utils.Evaluator.evaluator = (func, args) => () => {
    let result: string | null = null
    try {
      vm.freeze(args, 'args')
      result = vm.run(
        `result = (function({${Object.keys((args as string)).join(',')}}) {${(func as string)}})(args);`
      ) as string
    } catch (err) {
      // do nothing
    }
    return result
  }

  return Formio
}

export default initFormIO