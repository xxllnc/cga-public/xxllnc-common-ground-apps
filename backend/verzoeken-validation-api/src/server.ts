import App from '@/app'
import { ValidationController } from '@controllers/validation.controller'
import validateEnv from '@utils/validateEnv'

validateEnv()

const app = new App([ValidationController])
app.listen()
