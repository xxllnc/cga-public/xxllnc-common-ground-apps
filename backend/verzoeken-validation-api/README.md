# Formiojs-validation-api

This app can be used to validate form user input by sending the form input and the form to this api

## Development

First install all the needed packages by running:

```sh
yarn install
```

Then start the api:

```sh
yarn start
```

And goto http://localhost:8889/api-docs to see the api documentation

## Test

To run the available test, type:

```sh
yarn test
```
